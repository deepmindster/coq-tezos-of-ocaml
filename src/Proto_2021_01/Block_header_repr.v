Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Constants_repr.
Require TezosOfOCaml.Proto_2021_01.Fitness_repr.
Require TezosOfOCaml.Proto_2021_01.Nonce_hash.

Module contents.
  Record record : Set := Build {
    priority : int;
    seed_nonce_hash : option Nonce_hash.t;
    proof_of_work_nonce : bytes }.
  Definition with_priority priority (r : record) :=
    Build priority r.(seed_nonce_hash) r.(proof_of_work_nonce).
  Definition with_seed_nonce_hash seed_nonce_hash (r : record) :=
    Build r.(priority) seed_nonce_hash r.(proof_of_work_nonce).
  Definition with_proof_of_work_nonce proof_of_work_nonce (r : record) :=
    Build r.(priority) r.(seed_nonce_hash) proof_of_work_nonce.
End contents.
Definition contents := contents.record.

Module protocol_data.
  Record record : Set := Build {
    contents : contents;
    signature : Signature.t }.
  Definition with_contents contents (r : record) :=
    Build contents r.(signature).
  Definition with_signature signature (r : record) :=
    Build r.(contents) signature.
End protocol_data.
Definition protocol_data := protocol_data.record.

Module t.
  Record record : Set := Build {
    shell : Block_header.shell_header;
    protocol_data : protocol_data }.
  Definition with_shell shell (r : record) :=
    Build shell r.(protocol_data).
  Definition with_protocol_data protocol_data (r : record) :=
    Build r.(shell) protocol_data.
End t.
Definition t := t.record.

Definition block_header : Set := t.

Definition raw : Set := Block_header.t.

Definition shell_header : Set := Block_header.shell_header.

Definition raw_encoding : Data_encoding.t Block_header.t :=
  Block_header.encoding.

Definition shell_header_encoding : Data_encoding.t Block_header.shell_header :=
  Block_header.shell_header_encoding.

Definition contents_encoding : Data_encoding.encoding contents :=
  (let arg := Data_encoding.def "block_header.alpha.unsigned_contents" in
  fun (eta : Data_encoding.encoding contents) => arg None None eta)
    (Data_encoding.conv
      (fun (function_parameter : contents) =>
        let '{|
          contents.priority := priority;
            contents.seed_nonce_hash := seed_nonce_hash;
            contents.proof_of_work_nonce := proof_of_work_nonce
            |} := function_parameter in
        (priority, proof_of_work_nonce, seed_nonce_hash))
      (fun (function_parameter : int * bytes * option Nonce_hash.t) =>
        let '(priority, proof_of_work_nonce, seed_nonce_hash) :=
          function_parameter in
        {| contents.priority := priority;
          contents.seed_nonce_hash := seed_nonce_hash;
          contents.proof_of_work_nonce := proof_of_work_nonce |}) None
      (Data_encoding.obj3
        (Data_encoding.req None None "priority" Data_encoding.uint16)
        (Data_encoding.req None None "proof_of_work_nonce"
          (Data_encoding.Fixed.bytes_value
            Constants_repr.proof_of_work_nonce_size))
        (Data_encoding.opt None None "seed_nonce_hash" Nonce_hash.encoding))).

Definition protocol_data_encoding : Data_encoding.encoding protocol_data :=
  (let arg := Data_encoding.def "block_header.alpha.signed_contents" in
  fun (eta : Data_encoding.encoding protocol_data) => arg None None eta)
    (Data_encoding.conv
      (fun (function_parameter : protocol_data) =>
        let '{|
          protocol_data.contents := contents;
            protocol_data.signature := signature
            |} := function_parameter in
        (contents, signature))
      (fun (function_parameter : contents * Signature.t) =>
        let '(contents, signature) := function_parameter in
        {| protocol_data.contents := contents;
          protocol_data.signature := signature |}) None
      (Data_encoding.merge_objs contents_encoding
        (Data_encoding.obj1
          (Data_encoding.req None None "signature" Signature.encoding)))).

Definition raw_value (function_parameter : t) : Block_header.t :=
  let '{| t.shell := shell; t.protocol_data := protocol_data |} :=
    function_parameter in
  let protocol_data :=
    Data_encoding.Binary.to_bytes_exn protocol_data_encoding protocol_data in
  {| Block_header.t.shell := shell;
    Block_header.t.protocol_data := protocol_data |}.

Definition unsigned_encoding
  : Data_encoding.encoding (Block_header.shell_header * contents) :=
  Data_encoding.merge_objs Block_header.shell_header_encoding contents_encoding.

Definition encoding : Data_encoding.encoding t :=
  (let arg := Data_encoding.def "block_header.alpha.full_header" in
  fun (eta : Data_encoding.encoding t) => arg None None eta)
    (Data_encoding.conv
      (fun (function_parameter : t) =>
        let '{| t.shell := shell; t.protocol_data := protocol_data |} :=
          function_parameter in
        (shell, protocol_data))
      (fun (function_parameter : Block_header.shell_header * protocol_data) =>
        let '(shell, protocol_data) := function_parameter in
        {| t.shell := shell; t.protocol_data := protocol_data |}) None
      (Data_encoding.merge_objs Block_header.shell_header_encoding
        protocol_data_encoding)).

Definition max_header_length : int :=
  let fake_shell :=
    {| Block_header.shell_header.level := 0;
      Block_header.shell_header.proto_level := 0;
      Block_header.shell_header.predecessor := Block_hash.zero;
      Block_header.shell_header.timestamp := Time.of_seconds 0;
      Block_header.shell_header.validation_passes := 0;
      Block_header.shell_header.operations_hash := Operation_list_list_hash.zero;
      Block_header.shell_header.fitness := Fitness_repr.from_int64 0;
      Block_header.shell_header.context := Context_hash.zero |} in
  let fake_contents :=
    {| contents.priority := 0; contents.seed_nonce_hash := Some Nonce_hash.zero;
      contents.proof_of_work_nonce :=
        Bytes.make Constants_repr.proof_of_work_nonce_size "0" % char |} in
  Data_encoding.Binary.length encoding
    {| t.shell := fake_shell;
      t.protocol_data :=
        {| protocol_data.contents := fake_contents;
          protocol_data.signature := Signature.zero |} |}.

Definition hash_raw : Block_header.t -> Block_hash.t := Block_header.hash_value.

Definition hash_value (function_parameter : t) : Block_hash.t :=
  let '{| t.shell := shell; t.protocol_data := protocol_data |} :=
    function_parameter in
  Block_header.hash_value
    {| Block_header.t.shell := shell;
      Block_header.t.protocol_data :=
        Data_encoding.Binary.to_bytes_exn protocol_data_encoding protocol_data
      |}.
