Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_2021_01.Environment.S.

Parameter Fr_t : Set.

Parameter Fr : S.PRIME_FIELD (t := Fr_t).

Parameter Fq12_t : Set.

Parameter Fq12 : S.FIELD (t := Fq12_t).

Parameter Included_PAIRING_G1_t : Set.

Parameter Included_PAIRING_G2_t : Set.

Parameter Included_PAIRING :
  S.PAIRING (Gt_t := Fq12.(S.FIELD.t))
    (G1_t := Included_PAIRING_G1_t) (G1_Scalar_t := Fr.(S.PRIME_FIELD.t))
    (G2_t := Included_PAIRING_G2_t) (G2_Scalar_t := Fr.(S.PRIME_FIELD.t)).

Definition Gt := Included_PAIRING.(S.PAIRING.Gt).

Definition G1 := Included_PAIRING.(S.PAIRING.G1).

Definition G2 := Included_PAIRING.(S.PAIRING.G2).

Definition miller_loop :
  list (G1.(S.CURVE.t) * G2.(S.CURVE.t)) -> Fq12.(S.FIELD.t) :=
  Included_PAIRING.(S.PAIRING.miller_loop).

Definition final_exponentiation_opt :
  Fq12.(S.FIELD.t) -> option Fq12.(S.FIELD.t) :=
  Included_PAIRING.(S.PAIRING.final_exponentiation_opt).

Definition pairing : G1.(S.CURVE.t) -> G2.(S.CURVE.t) -> Fq12.(S.FIELD.t) :=
  Included_PAIRING.(S.PAIRING.pairing).
