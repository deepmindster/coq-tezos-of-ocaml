Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.

Axiom of_notation_to_notation
  : forall time,
    let notation := Time.to_notation time in
    Time.of_notation notation =
    if String.equal notation "out_of_range" then
      None
    else
      Some time.
