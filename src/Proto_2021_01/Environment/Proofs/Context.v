Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.

Lemma raw_mem_get ctxt key :
  Context.raw_mem ctxt key =
  match Context.raw_get ctxt key with
  | None => false
  | Some _ => true
  end.
Admitted.

Lemma raw_get_set ctxt key value :
  Context.raw_get (Context.raw_set ctxt key value) key =
  Some value.
Admitted.

Lemma raw_get_remove_rec ctxt key :
  Context.raw_get (Context.raw_remove_rec ctxt key) key =
  None.
Admitted.
