Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_2021_01.Environment.Error_monad.
Require Proto_2021_01.Environment.Lwt.

Module stream.
  Record record {next shutdown : Set} : Set := Build {
    next : next;
    shutdown : shutdown }.
  Arguments record : clear implicits.
  Definition with_next {t_next t_shutdown} next
    (r : record t_next t_shutdown) :=
    Build t_next t_shutdown next r.(shutdown).
  Definition with_shutdown {t_next t_shutdown} shutdown
    (r : record t_next t_shutdown) :=
    Build t_next t_shutdown r.(next) shutdown.
End stream.
Definition stream_skeleton := stream.record.

Reserved Notation "'stream".

Inductive t (o : Set) : Set :=
| OkStream : 'stream o -> t o
| Unauthorized : option (list Error_monad._error) -> t o
| Error : option (list Error_monad._error) -> t o
| Ok : o -> t o
| Not_found : option (list Error_monad._error) -> t o
| Forbidden : option (list Error_monad._error) -> t o
| Created : option string -> t o
| Conflict : option (list Error_monad._error) -> t o
| No_content : t o

where "'stream" := (fun (t_a : Set) =>
  stream_skeleton (unit -> Lwt.t (option t_a)) (unit -> unit)).

Definition stream := 'stream.

Arguments OkStream {_}.
Arguments Unauthorized {_}.
Arguments Error {_}.
Arguments Ok {_}.
Arguments Not_found {_}.
Arguments Forbidden {_}.
Arguments Created {_}.
Arguments Conflict {_}.
Arguments No_content {_}.

Parameter _return : forall {o : Set}, o -> Lwt.t (t o).

Parameter return_stream : forall {o : Set}, stream o -> Lwt.t (t o).

Parameter not_found : forall {o : Set}, Lwt.t (t o).

Parameter fail : forall {a : Set}, list Error_monad._error -> Lwt.t (t a).
