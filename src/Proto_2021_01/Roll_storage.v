Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Constants_repr.
Require TezosOfOCaml.Proto_2021_01.Constants_storage.
Require TezosOfOCaml.Proto_2021_01.Contract_repr.
Require TezosOfOCaml.Proto_2021_01.Cycle_repr.
Require TezosOfOCaml.Proto_2021_01.Level_repr.
Require TezosOfOCaml.Proto_2021_01.Manager_repr.
Require TezosOfOCaml.Proto_2021_01.Misc.
Require TezosOfOCaml.Proto_2021_01.Raw_context.
Require TezosOfOCaml.Proto_2021_01.Roll_repr.
Require TezosOfOCaml.Proto_2021_01.Seed_repr.
Require TezosOfOCaml.Proto_2021_01.Seed_storage.
Require TezosOfOCaml.Proto_2021_01.Storage.
Require TezosOfOCaml.Proto_2021_01.Storage_sigs.
Require TezosOfOCaml.Proto_2021_01.Tez_repr.

(** Init function; without side-effects in Coq *)
Definition init_module : unit :=
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "contract.manager.consume_roll_change" "Consume roll change"
      "Change is not enough to consume a roll."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Not enough change to consume a roll."
                  CamlinternalFormatBasics.End_of_format)
                "Not enough change to consume a roll."))) Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Consume_roll_change" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Consume_roll_change" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "contract.manager.no_roll_for_delegate" "No roll for delegate"
      "Delegate has no roll."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "Delegate has no roll."
                  CamlinternalFormatBasics.End_of_format)
                "Delegate has no roll."))) Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "No_roll_for_delegate" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "No_roll_for_delegate" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "contract.manager.no_roll_snapshot_for_cycle" "No roll snapshot for cycle"
      "A snapshot of the rolls distribution does not exist for this cycle."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (c : Cycle_repr.cycle) =>
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "A snapshot of the rolls distribution does not exist for cycle "
                  (CamlinternalFormatBasics.Alpha
                    CamlinternalFormatBasics.End_of_format))
                "A snapshot of the rolls distribution does not exist for cycle %a")
              Cycle_repr.pp c))
      (Data_encoding.obj1
        (Data_encoding.req None None "cycle" Cycle_repr.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "No_roll_snapshot_for_cycle" then
            let 'c := cast Cycle_repr.t payload in
            Some c
          else None
        end)
      (fun (c : Cycle_repr.cycle) =>
        Build_extensible "No_roll_snapshot_for_cycle" Cycle_repr.cycle c) in
  Error_monad.register_error_kind Error_monad.Permanent
    "contract.manager.unregistered_delegate" "Unregistered delegate"
    "A contract cannot be delegated to an unregistered delegate"
    (Some
      (fun (ppf : Format.formatter) =>
        fun (k : Signature.public_key_hash) =>
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal
                "The provided public key (with hash "
                (CamlinternalFormatBasics.Alpha
                  (CamlinternalFormatBasics.String_literal
                    ") is not registered as valid delegate key."
                    CamlinternalFormatBasics.End_of_format)))
              "The provided public key (with hash %a) is not registered as valid delegate key.")
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp) k))
    (Data_encoding.obj1
      (Data_encoding.req None None "hash"
        Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Unregistered_delegate" then
          let 'k := cast Signature.public_key_hash payload in
          Some k
        else None
      end)
    (fun (k : Signature.public_key_hash) =>
      Build_extensible "Unregistered_delegate" Signature.public_key_hash k).

Definition get_contract_delegate
  (ctxt : Raw_context.t) (contract : Contract_repr.t)
  : M=? (option Signature.public_key_hash) :=
  Storage.Contract.Delegate.(Storage_sigs.Indexed_data_storage.get_option) ctxt
    contract.

Definition delegate_pubkey
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  : M=? Signature.public_key :=
  let=? function_parameter :=
    Storage.Contract.Manager.(Storage_sigs.Indexed_data_storage.get_option) ctxt
      (Contract_repr.implicit_contract delegate) in
  match function_parameter with
  | (None | Some (Manager_repr.Hash _)) =>
    Error_monad.fail
      (Build_extensible "Unregistered_delegate" Signature.public_key_hash
        delegate)
  | Some (Manager_repr.Public_key pk) => Error_monad._return pk
  end.

Definition clear_cycle (ctxt : Raw_context.t) (cycle : Cycle_repr.t)
  : M=? Raw_context.t :=
  let=? index :=
    Storage.Roll.Snapshot_for_cycle.(Storage_sigs.Indexed_data_storage.get) ctxt
      cycle in
  let=? ctxt :=
    Storage.Roll.Snapshot_for_cycle.(Storage_sigs.Indexed_data_storage.delete)
      ctxt cycle in
  let=? ctxt :=
    Storage.Roll.Last_for_snapshot.(Storage_sigs.Indexed_data_storage.delete)
      (ctxt, cycle) index in
  Error_monad.op_gtpipeeq
    (Storage.Roll.Owner.(Storage_sigs.Indexed_data_snapshotable_storage.delete_snapshot)
      ctxt (cycle, index)) Error_monad.ok.

Definition fold {A : Set}
  (ctxt : Raw_context.t)
  (f : Roll_repr.roll -> Signature.public_key -> A -> M=? A) (init_value : A)
  : M=? A :=
  let=? last := Storage.Roll.Next.(Storage_sigs.Single_data_storage.get) ctxt in
  let fix loop (ctxt : Raw_context.t) (roll : Roll_repr.roll) (acc_value : A)
    {struct roll} : M=? A :=
    if Roll_repr.op_eq roll last then
      Error_monad._return acc_value
    else
      let=? function_parameter :=
        Storage.Roll.Owner.(Storage_sigs.Indexed_data_snapshotable_storage.get_option)
          ctxt roll in
      match function_parameter with
      | None => loop ctxt (Roll_repr.succ roll) acc_value
      | Some delegate =>
        let=? acc_value := f roll delegate acc_value in
        loop ctxt (Roll_repr.succ roll) acc_value
      end in
  loop ctxt Roll_repr.first init_value.

Definition snapshot_rolls_for_cycle
  (ctxt : Raw_context.t) (cycle : Cycle_repr.t) : M=? Raw_context.t :=
  let=? index :=
    Storage.Roll.Snapshot_for_cycle.(Storage_sigs.Indexed_data_storage.get) ctxt
      cycle in
  let=? ctxt :=
    Storage.Roll.Snapshot_for_cycle.(Storage_sigs.Indexed_data_storage.set) ctxt
      cycle (index +i 1) in
  let=? ctxt :=
    Storage.Roll.Owner.(Storage_sigs.Indexed_data_snapshotable_storage.snapshot_value)
      ctxt (cycle, index) in
  let=? last := Storage.Roll.Next.(Storage_sigs.Single_data_storage.get) ctxt in
  Storage.Roll.Last_for_snapshot.(Storage_sigs.Indexed_data_storage.init_value)
    (ctxt, cycle) index last.

Definition freeze_rolls_for_cycle (ctxt : Raw_context.t) (cycle : Cycle_repr.t)
  : M=? Raw_context.t :=
  let=? max_index :=
    Storage.Roll.Snapshot_for_cycle.(Storage_sigs.Indexed_data_storage.get) ctxt
      cycle in
  let=? seed_value := Storage.Seed.For_cycle.(Storage.FOR_CYCLE.get) ctxt cycle
    in
  let rd :=
    Seed_repr.initialize_new seed_value [ Bytes.of_string "roll_snapshot" ] in
  let seq := Seed_repr.sequence_value rd 0 in
  let selected_index :=
    Int32.to_int
      (Pervasives.fst (Seed_repr.take_int32 seq (Int32.of_int max_index))) in
  let=? ctxt :=
    Storage.Roll.Snapshot_for_cycle.(Storage_sigs.Indexed_data_storage.set) ctxt
      cycle selected_index in
  Error_monad.fold_left_s
    (fun (ctxt : Raw_context.t) =>
      fun (index : int) =>
        if index =i selected_index then
          Error_monad._return ctxt
        else
          let= ctxt :=
            Storage.Roll.Owner.(Storage_sigs.Indexed_data_snapshotable_storage.delete_snapshot)
              ctxt (cycle, index) in
          Storage.Roll.Last_for_snapshot.(Storage_sigs.Indexed_data_storage.delete)
            (ctxt, cycle) index) ctxt (Misc.op_minusminusgt 0 (max_index -i 1)).

Module Random.
  Definition int32_to_bytes (i : int32) : bytes :=
    let b_value := Bytes.make 4 "0" % char in
    let '_ := TzEndian.set_int32 b_value 0 i in
    b_value.
  
  Definition level_random
    (seed_value : Seed_repr.seed) (use : string) (level : Level_repr.t)
    : Seed_repr.t :=
    let position := level.(Level_repr.t.cycle_position) in
    Seed_repr.initialize_new seed_value
      [
        Bytes.of_string
          (Pervasives.op_caret "level " (Pervasives.op_caret use ":"));
        int32_to_bytes position
      ].
  
  Definition owner
    (c : Raw_context.t) (kind_value : string) (level : Level_repr.t)
    (offset : int) : M=? Signature.public_key :=
    let cycle := level.(Level_repr.t.cycle) in
    let=? random_seed := Seed_storage.for_cycle c cycle in
    let rd := level_random random_seed kind_value level in
    let sequence_value := Seed_repr.sequence_value rd (Int32.of_int offset) in
    let=? index :=
      Storage.Roll.Snapshot_for_cycle.(Storage_sigs.Indexed_data_storage.get) c
        cycle in
    let=? bound :=
      Storage.Roll.Last_for_snapshot.(Storage_sigs.Indexed_data_storage.get)
        (c, cycle) index in
    let fix loop (sequence_value : Seed_repr.sequence)
      : M=? Signature.public_key :=
      let '(roll, sequence_value) := Roll_repr.random sequence_value bound in
      let=? function_parameter :=
        Storage.Roll.Owner.(Storage_sigs.Indexed_data_snapshotable_storage.Snapshot).(Storage_sigs.Indexed_data_storage.get_option)
          c ((cycle, index), roll) in
      match function_parameter with
      | None => loop sequence_value
      | Some delegate => Error_monad._return delegate
      end in
    let= snapshot_exists :=
      Storage.Roll.Owner.(Storage_sigs.Indexed_data_snapshotable_storage.snapshot_exists)
        c (cycle, index) in
    let=? '_ :=
      return=
        (Error_monad.error_unless snapshot_exists
          (Build_extensible "No_roll_snapshot_for_cycle" Cycle_repr.t cycle)) in
    loop sequence_value.
End Random.

Definition baking_rights_owner
  (c : Raw_context.t) (level : Level_repr.t) (priority : int)
  : M=? Signature.public_key := Random.owner c "baking" level priority.

Definition endorsement_rights_owner
  (c : Raw_context.t) (level : Level_repr.t) (slot : int)
  : M=? Signature.public_key := Random.owner c "endorsement" level slot.

Definition count_rolls
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash) : M=? int :=
  let=? function_parameter :=
    Storage.Roll.Delegate_roll_list.(Storage_sigs.Indexed_data_storage.get_option)
      ctxt delegate in
  match function_parameter with
  | None => Error_monad._return 0
  | Some head_roll =>
    let fix loop (acc_value : int) (roll : int32) {struct roll} : M=? int :=
      let=? function_parameter :=
        Storage.Roll.Successor.(Storage_sigs.Indexed_data_storage.get_option)
          ctxt roll in
      match function_parameter with
      | None => Error_monad._return acc_value
      | Some next => loop (Pervasives.succ acc_value) next
      end in
    loop 1 head_roll
  end.

Definition get_change
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  : M=? Tez_repr.t :=
  Error_monad.op_gtpipeeqquestion
    (Storage.Roll.Delegate_change.(Storage_sigs.Indexed_data_storage.get_option)
      ctxt delegate) (fun x_1 => Option.value x_1 Tez_repr.zero).

Module Delegate.
  Definition fresh_roll (ctxt : Raw_context.t) : M=? (int32 * Raw_context.t) :=
    let=? roll := Storage.Roll.Next.(Storage_sigs.Single_data_storage.get) ctxt
      in
    let=? ctxt :=
      Storage.Roll.Next.(Storage_sigs.Single_data_storage.set) ctxt
        (Roll_repr.succ roll) in
    return=? (roll, ctxt).
  
  Definition get_limbo_roll (ctxt : Raw_context.t)
    : M=? (int32 * Raw_context.t) :=
    let=? function_parameter :=
      Storage.Roll.Limbo.(Storage_sigs.Single_data_storage.get_option) ctxt in
    match function_parameter with
    | None =>
      let=? '(roll, ctxt) := fresh_roll ctxt in
      let=? ctxt :=
        Storage.Roll.Limbo.(Storage_sigs.Single_data_storage.init_value) ctxt
          roll in
      return=? (roll, ctxt)
    | Some roll => Error_monad._return (roll, ctxt)
    end.
  
  Definition consume_roll_change
    (ctxt : Raw_context.context) (delegate : Signature.public_key_hash)
    : M=? Raw_context.t :=
    let tokens_per_roll := Constants_storage.tokens_per_roll ctxt in
    let=? change :=
      Storage.Roll.Delegate_change.(Storage_sigs.Indexed_data_storage.get) ctxt
        delegate in
    let=? new_change :=
      return=
        (Error_monad.record_trace
          (Build_extensible "Consume_roll_change" unit tt)
          (Tez_repr.op_minusquestion change tokens_per_roll)) in
    Storage.Roll.Delegate_change.(Storage_sigs.Indexed_data_storage.set) ctxt
      delegate new_change.
  
  Definition recover_roll_change
    (ctxt : Raw_context.context) (delegate : Signature.public_key_hash)
    : M=? Raw_context.t :=
    let tokens_per_roll := Constants_storage.tokens_per_roll ctxt in
    let=? change :=
      Storage.Roll.Delegate_change.(Storage_sigs.Indexed_data_storage.get) ctxt
        delegate in
    let=? new_change :=
      return= (Tez_repr.op_plusquestion change tokens_per_roll) in
    Storage.Roll.Delegate_change.(Storage_sigs.Indexed_data_storage.set) ctxt
      delegate new_change.
  
  Definition pop_roll_from_delegate
    (ctxt : Raw_context.context) (delegate : Signature.public_key_hash)
    : M=? (int32 * Raw_context.t) :=
    let=? ctxt := recover_roll_change ctxt delegate in
    let=? limbo_head :=
      Storage.Roll.Limbo.(Storage_sigs.Single_data_storage.get_option) ctxt in
    let=? function_parameter :=
      Storage.Roll.Delegate_roll_list.(Storage_sigs.Indexed_data_storage.get_option)
        ctxt delegate in
    match function_parameter with
    | None => Error_monad.fail (Build_extensible "No_roll_for_delegate" unit tt)
    | Some roll =>
      let=? ctxt :=
        Storage.Roll.Owner.(Storage_sigs.Indexed_data_snapshotable_storage.delete)
          ctxt roll in
      let=? successor_roll :=
        Storage.Roll.Successor.(Storage_sigs.Indexed_data_storage.get_option)
          ctxt roll in
      let= ctxt :=
        Storage.Roll.Delegate_roll_list.(Storage_sigs.Indexed_data_storage.set_option)
          ctxt delegate successor_roll in
      let= ctxt :=
        Storage.Roll.Successor.(Storage_sigs.Indexed_data_storage.set_option)
          ctxt roll limbo_head in
      let= ctxt :=
        Storage.Roll.Limbo.(Storage_sigs.Single_data_storage.init_set) ctxt roll
        in
      return=? (roll, ctxt)
    end.
  
  Definition create_roll_in_delegate
    (ctxt : Raw_context.context) (delegate : Signature.public_key_hash)
    (delegate_pk : Signature.public_key) : M=? Raw_context.t :=
    let=? ctxt := consume_roll_change ctxt delegate in
    let=? delegate_head :=
      Storage.Roll.Delegate_roll_list.(Storage_sigs.Indexed_data_storage.get_option)
        ctxt delegate in
    let=? '(roll, ctxt) := get_limbo_roll ctxt in
    let=? ctxt :=
      Storage.Roll.Owner.(Storage_sigs.Indexed_data_snapshotable_storage.init_value)
        ctxt roll delegate_pk in
    let=? limbo_successor :=
      Storage.Roll.Successor.(Storage_sigs.Indexed_data_storage.get_option) ctxt
        roll in
    let= ctxt :=
      Storage.Roll.Limbo.(Storage_sigs.Single_data_storage.set_option) ctxt
        limbo_successor in
    let= ctxt :=
      Storage.Roll.Successor.(Storage_sigs.Indexed_data_storage.set_option) ctxt
        roll delegate_head in
    Error_monad.op_gtpipeeq
      (Storage.Roll.Delegate_roll_list.(Storage_sigs.Indexed_data_storage.init_set)
        ctxt delegate roll) Error_monad.ok.
  
  Definition ensure_inited
    (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
    : M=? Raw_context.t :=
    let= function_parameter :=
      Storage.Roll.Delegate_change.(Storage_sigs.Indexed_data_storage.mem) ctxt
        delegate in
    match function_parameter with
    | true => Error_monad._return ctxt
    | false =>
      Storage.Roll.Delegate_change.(Storage_sigs.Indexed_data_storage.init_value)
        ctxt delegate Tez_repr.zero
    end.
  
  Definition is_inactive
    (ctxt : Raw_context.t) (delegate : Signature.public_key_hash) : M=? bool :=
    let= inactive :=
      Storage.Contract.Inactive_delegate.(Storage_sigs.Data_set_storage.mem)
        ctxt (Contract_repr.implicit_contract delegate) in
    if inactive then
      Error_monad._return inactive
    else
      let=? function_parameter :=
        Storage.Contract.Delegate_desactivation.(Storage_sigs.Indexed_data_storage.get_option)
          ctxt (Contract_repr.implicit_contract delegate) in
      match function_parameter with
      | Some last_active_cycle =>
        let '{| Level_repr.t.cycle := current_cycle |} :=
          Raw_context.current_level ctxt in
        return=? (Cycle_repr.op_lt last_active_cycle current_cycle)
      | None => return=? false
      end.
  
  Definition add_amount
    (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
    (amount : Tez_repr.t) : M=? Raw_context.t :=
    let=? ctxt := ensure_inited ctxt delegate in
    let tokens_per_roll := Constants_storage.tokens_per_roll ctxt in
    let=? change :=
      Storage.Roll.Delegate_change.(Storage_sigs.Indexed_data_storage.get) ctxt
        delegate in
    let=? change := return= (Tez_repr.op_plusquestion amount change) in
    let=? ctxt :=
      Storage.Roll.Delegate_change.(Storage_sigs.Indexed_data_storage.set) ctxt
        delegate change in
    let=? delegate_pk := delegate_pubkey ctxt delegate in
    let fix loop (ctxt : Raw_context.context) (change : Tez_repr.t)
      {struct change} : M=? Raw_context.context :=
      if Tez_repr.op_lt change tokens_per_roll then
        Error_monad._return ctxt
      else
        let=? change :=
          return= (Tez_repr.op_minusquestion change tokens_per_roll) in
        let=? ctxt := create_roll_in_delegate ctxt delegate delegate_pk in
        loop ctxt change in
    let=? inactive := is_inactive ctxt delegate in
    if inactive then
      Error_monad._return ctxt
    else
      let=? ctxt := loop ctxt change in
      let=? rolls :=
        Storage.Roll.Delegate_roll_list.(Storage_sigs.Indexed_data_storage.get_option)
          ctxt delegate in
      match rolls with
      | None => Error_monad._return ctxt
      | Some _ =>
        Error_monad.op_gtpipeeq
          (Storage.Active_delegates_with_rolls.(Storage_sigs.Data_set_storage.add)
            ctxt delegate) Error_monad.ok
      end.
  
  Definition remove_amount
    (ctxt : Raw_context.context) (delegate : Signature.public_key_hash)
    (amount : Tez_repr.t) : M=? Raw_context.t :=
    let tokens_per_roll := Constants_storage.tokens_per_roll ctxt in
    let fix loop (ctxt : Raw_context.context) (change : Tez_repr.t)
      {struct change} : M=? (Raw_context.context * Tez_repr.t) :=
      if Tez_repr.op_lteq amount change then
        Error_monad._return (ctxt, change)
      else
        let=? '(_, ctxt) := pop_roll_from_delegate ctxt delegate in
        let=? change :=
          return= (Tez_repr.op_plusquestion change tokens_per_roll) in
        loop ctxt change in
    let=? change :=
      Storage.Roll.Delegate_change.(Storage_sigs.Indexed_data_storage.get) ctxt
        delegate in
    let=? inactive := is_inactive ctxt delegate in
    let=? '(ctxt, change) :=
      if inactive then
        Error_monad._return (ctxt, change)
      else
        let=? '(ctxt, change) := loop ctxt change in
        let=? rolls :=
          Storage.Roll.Delegate_roll_list.(Storage_sigs.Indexed_data_storage.get_option)
            ctxt delegate in
        match rolls with
        | None =>
          let= ctxt :=
            Storage.Active_delegates_with_rolls.(Storage_sigs.Data_set_storage.del)
              ctxt delegate in
          return=? (ctxt, change)
        | Some _ => Error_monad._return (ctxt, change)
        end in
    let=? change := return= (Tez_repr.op_minusquestion change amount) in
    Storage.Roll.Delegate_change.(Storage_sigs.Indexed_data_storage.set) ctxt
      delegate change.
  
  Definition set_inactive
    (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
    : M=? Raw_context.t :=
    let=? ctxt := ensure_inited ctxt delegate in
    let tokens_per_roll := Constants_storage.tokens_per_roll ctxt in
    let=? change :=
      Storage.Roll.Delegate_change.(Storage_sigs.Indexed_data_storage.get) ctxt
        delegate in
    let= ctxt :=
      Storage.Contract.Inactive_delegate.(Storage_sigs.Data_set_storage.add)
        ctxt (Contract_repr.implicit_contract delegate) in
    let= ctxt :=
      Storage.Active_delegates_with_rolls.(Storage_sigs.Data_set_storage.del)
        ctxt delegate in
    let fix loop (ctxt : Raw_context.t) (change : Tez_repr.t) {struct change}
      : M=? (Raw_context.t * Tez_repr.t) :=
      let=? function_parameter :=
        Storage.Roll.Delegate_roll_list.(Storage_sigs.Indexed_data_storage.get_option)
          ctxt delegate in
      match function_parameter with
      | None => Error_monad._return (ctxt, change)
      | Some _roll =>
        let=? '(_, ctxt) := pop_roll_from_delegate ctxt delegate in
        let=? change :=
          return= (Tez_repr.op_plusquestion change tokens_per_roll) in
        loop ctxt change
      end in
    let=? '(ctxt, change) := loop ctxt change in
    Storage.Roll.Delegate_change.(Storage_sigs.Indexed_data_storage.set) ctxt
      delegate change.
  
  Definition set_active
    (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
    : M=? Raw_context.t :=
    let=? inactive := is_inactive ctxt delegate in
    let current_cycle := (Raw_context.current_level ctxt).(Level_repr.t.cycle)
      in
    let preserved_cycles := Constants_storage.preserved_cycles ctxt in
    let=? current_expiration :=
      Storage.Contract.Delegate_desactivation.(Storage_sigs.Indexed_data_storage.get_option)
        ctxt (Contract_repr.implicit_contract delegate) in
    let expiration :=
      match current_expiration with
      | None => Cycle_repr.add current_cycle (1 +i (2 *i preserved_cycles))
      | Some current_expiration =>
        let delay :=
          if inactive then
            1 +i (2 *i preserved_cycles)
          else
            1 +i preserved_cycles in
        let updated := Cycle_repr.add current_cycle delay in
        Cycle_repr.max current_expiration updated
      end in
    let= ctxt :=
      Storage.Contract.Delegate_desactivation.(Storage_sigs.Indexed_data_storage.init_set)
        ctxt (Contract_repr.implicit_contract delegate) expiration in
    if Pervasives.not inactive then
      Error_monad._return ctxt
    else
      let=? ctxt := ensure_inited ctxt delegate in
      let tokens_per_roll := Constants_storage.tokens_per_roll ctxt in
      let=? change :=
        Storage.Roll.Delegate_change.(Storage_sigs.Indexed_data_storage.get)
          ctxt delegate in
      let= ctxt :=
        Storage.Contract.Inactive_delegate.(Storage_sigs.Data_set_storage.del)
          ctxt (Contract_repr.implicit_contract delegate) in
      let=? delegate_pk := delegate_pubkey ctxt delegate in
      let fix loop (ctxt : Raw_context.context) (change : Tez_repr.t)
        {struct change} : M=? Raw_context.context :=
        if Tez_repr.op_lt change tokens_per_roll then
          Error_monad._return ctxt
        else
          let=? change :=
            return= (Tez_repr.op_minusquestion change tokens_per_roll) in
          let=? ctxt := create_roll_in_delegate ctxt delegate delegate_pk in
          loop ctxt change in
      let=? ctxt := loop ctxt change in
      let=? rolls :=
        Storage.Roll.Delegate_roll_list.(Storage_sigs.Indexed_data_storage.get_option)
          ctxt delegate in
      match rolls with
      | None => Error_monad._return ctxt
      | Some _ =>
        Error_monad.op_gtpipeeq
          (Storage.Active_delegates_with_rolls.(Storage_sigs.Data_set_storage.add)
            ctxt delegate) Error_monad.ok
      end.
End Delegate.

Module Contract.
  Definition add_amount
    (c : Raw_context.t) (contract : Contract_repr.t) (amount : Tez_repr.t)
    : M=? Raw_context.t :=
    let=? function_parameter := get_contract_delegate c contract in
    match function_parameter with
    | None => Error_monad._return c
    | Some delegate => Delegate.add_amount c delegate amount
    end.
  
  Definition remove_amount
    (c : Raw_context.t) (contract : Contract_repr.t) (amount : Tez_repr.t)
    : M=? Raw_context.t :=
    let=? function_parameter := get_contract_delegate c contract in
    match function_parameter with
    | None => Error_monad._return c
    | Some delegate => Delegate.remove_amount c delegate amount
    end.
End Contract.

Definition init_value (ctxt : Raw_context.t) : M=? Raw_context.t :=
  Storage.Roll.Next.(Storage_sigs.Single_data_storage.init_value) ctxt
    Roll_repr.first.

Definition init_first_cycles (ctxt : Raw_context.context) : M=? Raw_context.t :=
  let preserved := Constants_storage.preserved_cycles ctxt in
  let=? ctxt :=
    Error_monad.fold_left_s
      (fun (ctxt : Raw_context.t) =>
        fun (c : int) =>
          let cycle := Cycle_repr.of_int32_exn (Int32.of_int c) in
          let=? ctxt :=
            Storage.Roll.Snapshot_for_cycle.(Storage_sigs.Indexed_data_storage.init_value)
              ctxt cycle 0 in
          let=? ctxt := snapshot_rolls_for_cycle ctxt cycle in
          freeze_rolls_for_cycle ctxt cycle) ctxt
      (Misc.op_minusminusgt 0 preserved) in
  let cycle := Cycle_repr.of_int32_exn (Int32.of_int (preserved +i 1)) in
  let=? ctxt :=
    Storage.Roll.Snapshot_for_cycle.(Storage_sigs.Indexed_data_storage.init_value)
      ctxt cycle 0 in
  let=? ctxt := snapshot_rolls_for_cycle ctxt cycle in
  let cycle := Cycle_repr.of_int32_exn (Int32.of_int (preserved +i 2)) in
  Storage.Roll.Snapshot_for_cycle.(Storage_sigs.Indexed_data_storage.init_value)
    ctxt cycle 0.

Definition snapshot_rolls (ctxt : Raw_context.context) : M=? Raw_context.t :=
  let current_level := Raw_context.current_level ctxt in
  let preserved := Constants_storage.preserved_cycles ctxt in
  let cycle :=
    Cycle_repr.add current_level.(Level_repr.t.cycle) (preserved +i 2) in
  snapshot_rolls_for_cycle ctxt cycle.

Definition cycle_end
  (ctxt : Raw_context.context) (last_cycle : Cycle_repr.cycle)
  : M=? Raw_context.t :=
  let preserved := Constants_storage.preserved_cycles ctxt in
  let=? ctxt :=
    match Cycle_repr.sub last_cycle preserved with
    | None => Error_monad._return ctxt
    | Some cleared_cycle => clear_cycle ctxt cleared_cycle
    end in
  let frozen_roll_cycle := Cycle_repr.add last_cycle (preserved +i 1) in
  let=? ctxt := freeze_rolls_for_cycle ctxt frozen_roll_cycle in
  Storage.Roll.Snapshot_for_cycle.(Storage_sigs.Indexed_data_storage.init_value)
    ctxt (Cycle_repr.succ (Cycle_repr.succ frozen_roll_cycle)) 0.

Definition update_tokens_per_roll
  (ctxt : Raw_context.context) (new_tokens_per_roll : Tez_repr.t)
  : M=? Raw_context.context :=
  let constants := Raw_context.constants ctxt in
  let old_tokens_per_roll :=
    constants.(Constants_repr.parametric.tokens_per_roll) in
  let= ctxt :=
    Raw_context.patch_constants ctxt
      (fun (constants : Constants_repr.parametric) =>
        Constants_repr.parametric.with_tokens_per_roll new_tokens_per_roll
          constants) in
  let decrease := Tez_repr.op_lt new_tokens_per_roll old_tokens_per_roll in
  let=? abs_diff :=
    return=
      (if decrease then
        Tez_repr.op_minusquestion old_tokens_per_roll new_tokens_per_roll
      else
        Tez_repr.op_minusquestion new_tokens_per_roll old_tokens_per_roll) in
  Storage.Delegates.(Storage_sigs.Data_set_storage.fold) ctxt
    (Pervasives.Ok ctxt)
    (fun (pkh : Signature.public_key_hash) =>
      fun (ctxt_opt : M? Raw_context.context) =>
        let=? ctxt := return= ctxt_opt in
        let=? rolls := count_rolls ctxt pkh in
        let=? amount :=
          return= (Tez_repr.op_starquestion abs_diff (Int64.of_int rolls)) in
        if decrease then
          Delegate.add_amount ctxt pkh amount
        else
          Delegate.remove_amount ctxt pkh amount).
