Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Alpha_context.
Require TezosOfOCaml.Proto_2021_01.Baking.
Require TezosOfOCaml.Proto_2021_01.Misc.
Require TezosOfOCaml.Proto_2021_01.Services_registration.

Module info.
  Record record : Set := Build {
    balance : Alpha_context.Tez.t;
    frozen_balance : Alpha_context.Tez.t;
    frozen_balance_by_cycle :
      Alpha_context.Cycle.Map.(S.MAP.t) Alpha_context.Delegate.frozen_balance;
    staking_balance : Alpha_context.Tez.t;
    delegated_contracts : list Alpha_context.Contract.t;
    delegated_balance : Alpha_context.Tez.t;
    deactivated : bool;
    grace_period : Alpha_context.Cycle.t;
    voting_power : int32 }.
  Definition with_balance balance (r : record) :=
    Build balance r.(frozen_balance) r.(frozen_balance_by_cycle)
      r.(staking_balance) r.(delegated_contracts) r.(delegated_balance)
      r.(deactivated) r.(grace_period) r.(voting_power).
  Definition with_frozen_balance frozen_balance (r : record) :=
    Build r.(balance) frozen_balance r.(frozen_balance_by_cycle)
      r.(staking_balance) r.(delegated_contracts) r.(delegated_balance)
      r.(deactivated) r.(grace_period) r.(voting_power).
  Definition with_frozen_balance_by_cycle frozen_balance_by_cycle
    (r : record) :=
    Build r.(balance) r.(frozen_balance) frozen_balance_by_cycle
      r.(staking_balance) r.(delegated_contracts) r.(delegated_balance)
      r.(deactivated) r.(grace_period) r.(voting_power).
  Definition with_staking_balance staking_balance (r : record) :=
    Build r.(balance) r.(frozen_balance) r.(frozen_balance_by_cycle)
      staking_balance r.(delegated_contracts) r.(delegated_balance)
      r.(deactivated) r.(grace_period) r.(voting_power).
  Definition with_delegated_contracts delegated_contracts (r : record) :=
    Build r.(balance) r.(frozen_balance) r.(frozen_balance_by_cycle)
      r.(staking_balance) delegated_contracts r.(delegated_balance)
      r.(deactivated) r.(grace_period) r.(voting_power).
  Definition with_delegated_balance delegated_balance (r : record) :=
    Build r.(balance) r.(frozen_balance) r.(frozen_balance_by_cycle)
      r.(staking_balance) r.(delegated_contracts) delegated_balance
      r.(deactivated) r.(grace_period) r.(voting_power).
  Definition with_deactivated deactivated (r : record) :=
    Build r.(balance) r.(frozen_balance) r.(frozen_balance_by_cycle)
      r.(staking_balance) r.(delegated_contracts) r.(delegated_balance)
      deactivated r.(grace_period) r.(voting_power).
  Definition with_grace_period grace_period (r : record) :=
    Build r.(balance) r.(frozen_balance) r.(frozen_balance_by_cycle)
      r.(staking_balance) r.(delegated_contracts) r.(delegated_balance)
      r.(deactivated) grace_period r.(voting_power).
  Definition with_voting_power voting_power (r : record) :=
    Build r.(balance) r.(frozen_balance) r.(frozen_balance_by_cycle)
      r.(staking_balance) r.(delegated_contracts) r.(delegated_balance)
      r.(deactivated) r.(grace_period) voting_power.
End info.
Definition info := info.record.

Definition info_encoding : Data_encoding.encoding info :=
  Data_encoding.conv
    (fun (function_parameter : info) =>
      let '{|
        info.balance := balance;
          info.frozen_balance := frozen_balance_value;
          info.frozen_balance_by_cycle := frozen_balance_by_cycle;
          info.staking_balance := staking_balance;
          info.delegated_contracts := delegated_contracts;
          info.delegated_balance := delegated_balance;
          info.deactivated := deactivated;
          info.grace_period := grace_period;
          info.voting_power := voting_power
          |} := function_parameter in
      (balance, frozen_balance_value, frozen_balance_by_cycle, staking_balance,
        delegated_contracts, delegated_balance, deactivated, grace_period,
        voting_power))
    (fun (function_parameter :
      Alpha_context.Tez.t * Alpha_context.Tez.t *
        Alpha_context.Cycle.Map.(S.MAP.t) Alpha_context.Delegate.frozen_balance
        * Alpha_context.Tez.t * list Alpha_context.Contract.t *
        Alpha_context.Tez.t * bool * Alpha_context.Cycle.t * int32) =>
      let
        '(balance, frozen_balance_value, frozen_balance_by_cycle,
          staking_balance, delegated_contracts, delegated_balance, deactivated,
          grace_period, voting_power) := function_parameter in
      {| info.balance := balance; info.frozen_balance := frozen_balance_value;
        info.frozen_balance_by_cycle := frozen_balance_by_cycle;
        info.staking_balance := staking_balance;
        info.delegated_contracts := delegated_contracts;
        info.delegated_balance := delegated_balance;
        info.deactivated := deactivated; info.grace_period := grace_period;
        info.voting_power := voting_power |}) None
    (Data_encoding.obj9
      (Data_encoding.req None None "balance" Alpha_context.Tez.encoding)
      (Data_encoding.req None None "frozen_balance" Alpha_context.Tez.encoding)
      (Data_encoding.req None None "frozen_balance_by_cycle"
        Alpha_context.Delegate.frozen_balance_by_cycle_encoding)
      (Data_encoding.req None None "staking_balance" Alpha_context.Tez.encoding)
      (Data_encoding.req None None "delegated_contracts"
        (Data_encoding.list_value None Alpha_context.Contract.encoding))
      (Data_encoding.req None None "delegated_balance"
        Alpha_context.Tez.encoding)
      (Data_encoding.req None None "deactivated" Data_encoding.bool_value)
      (Data_encoding.req None None "grace_period" Alpha_context.Cycle.encoding)
      (Data_encoding.req None None "voting_power" Data_encoding.int32_value)).

Module S.
  Definition raw_path : RPC_path.path Updater.rpc_context Updater.rpc_context :=
    RPC_path.op_div (RPC_path.op_div RPC_path.open_root "context") "delegates".
  
  Module list_query.
    Record record : Set := Build {
      active : bool;
      inactive : bool }.
    Definition with_active active (r : record) :=
      Build active r.(inactive).
    Definition with_inactive inactive (r : record) :=
      Build r.(active) inactive.
  End list_query.
  Definition list_query := list_query.record.
  
  Definition list_query_value : RPC_query.t list_query :=
    RPC_query.seal
      (RPC_query.op_pipeplus
        (RPC_query.op_pipeplus
          (RPC_query.query_value
            (fun (active : bool) =>
              fun (inactive : bool) =>
                {| list_query.active := active; list_query.inactive := inactive
                  |}))
          (RPC_query.flag None "active"
            (fun (t_value : list_query) => t_value.(list_query.active))))
        (RPC_query.flag None "inactive"
          (fun (t_value : list_query) => t_value.(list_query.inactive)))).
  
  Definition list_delegate
    : RPC_service.service Updater.rpc_context Updater.rpc_context list_query
      unit (list Signature.public_key_hash) :=
    RPC_service.get_service (Some "Lists all registered delegates.")
      list_query_value
      (Data_encoding.list_value None
        Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
      raw_path.
  
  Definition path
    : RPC_path.path Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) :=
    RPC_path.op_divcolon raw_path
      Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.rpc_arg).
  
  Definition info_value
    : RPC_service.service Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) unit unit info :=
    RPC_service.get_service (Some "Everything about a delegate.")
      RPC_query.empty info_encoding path.
  
  Definition balance
    : RPC_service.service Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) unit unit
      Alpha_context.Tez.t :=
    RPC_service.get_service
      (Some
        "Returns the full balance of a given delegate, including the frozen balances.")
      RPC_query.empty Alpha_context.Tez.encoding
      (RPC_path.op_div path "balance").
  
  Definition frozen_balance_value
    : RPC_service.service Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) unit unit
      Alpha_context.Tez.t :=
    RPC_service.get_service
      (Some
        "Returns the total frozen balances of a given delegate, this includes the frozen deposits, rewards and fees.")
      RPC_query.empty Alpha_context.Tez.encoding
      (RPC_path.op_div path "frozen_balance").
  
  Definition frozen_balance_by_cycle
    : RPC_service.service Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) unit unit
      (Alpha_context.Cycle.Map.(S.MAP.t) Alpha_context.Delegate.frozen_balance) :=
    RPC_service.get_service
      (Some
        "Returns the frozen balances of a given delegate, indexed by the cycle by which it will be unfrozen")
      RPC_query.empty Alpha_context.Delegate.frozen_balance_by_cycle_encoding
      (RPC_path.op_div path "frozen_balance_by_cycle").
  
  Definition staking_balance
    : RPC_service.service Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) unit unit
      Alpha_context.Tez.t :=
    RPC_service.get_service
      (Some
        "Returns the total amount of tokens delegated to a given delegate. This includes the balances of all the contracts that delegate to it, but also the balance of the delegate itself and its frozen fees and deposits. The rewards do not count in the delegated balance until they are unfrozen.")
      RPC_query.empty Alpha_context.Tez.encoding
      (RPC_path.op_div path "staking_balance").
  
  Definition delegated_contracts
    : RPC_service.service Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) unit unit
      (list Alpha_context.Contract.t) :=
    RPC_service.get_service
      (Some "Returns the list of contracts that delegate to a given delegate.")
      RPC_query.empty
      (Data_encoding.list_value None Alpha_context.Contract.encoding)
      (RPC_path.op_div path "delegated_contracts").
  
  Definition delegated_balance
    : RPC_service.service Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) unit unit
      Alpha_context.Tez.t :=
    RPC_service.get_service
      (Some
        "Returns the balances of all the contracts that delegate to a given delegate. This excludes the delegate's own balance and its frozen balances.")
      RPC_query.empty Alpha_context.Tez.encoding
      (RPC_path.op_div path "delegated_balance").
  
  Definition deactivated
    : RPC_service.service Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) unit unit bool :=
    RPC_service.get_service
      (Some
        "Tells whether the delegate is currently tagged as deactivated or not.")
      RPC_query.empty Data_encoding.bool_value
      (RPC_path.op_div path "deactivated").
  
  Definition grace_period
    : RPC_service.service Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) unit unit
      Alpha_context.Cycle.t :=
    RPC_service.get_service
      (Some
        "Returns the cycle by the end of which the delegate might be deactivated if she fails to execute any delegate action. A deactivated delegate might be reactivated (without loosing any rolls) by simply re-registering as a delegate. For deactivated delegates, this value contains the cycle by which they were deactivated.")
      RPC_query.empty Alpha_context.Cycle.encoding
      (RPC_path.op_div path "grace_period").
  
  Definition voting_power
    : RPC_service.service Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) unit unit int32 :=
    RPC_service.get_service
      (Some "The number of rolls in the vote listings for a given delegate")
      RPC_query.empty Data_encoding.int32_value
      (RPC_path.op_div path "voting_power").
End S.

Definition begin_register (function_parameter : unit) : unit :=
  let '_ := function_parameter in
  let '_ :=
    Services_registration.register0 S.list_delegate
      (fun (ctxt : Alpha_context.t) =>
        fun (q : S.list_query) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            let= delegates := Alpha_context.Delegate.list_value ctxt in
            match q with
            | {| S.list_query.active := true; S.list_query.inactive := false |}
              =>
              Error_monad.filter_s
                (fun (pkh : Signature.public_key_hash) =>
                  Error_monad.op_gtpipeeqquestion
                    (Alpha_context.Delegate.deactivated ctxt pkh) Pervasives.not)
                delegates
            | {| S.list_query.active := false; S.list_query.inactive := true |}
              =>
              Error_monad.filter_s
                (fun (pkh : Signature.public_key_hash) =>
                  Alpha_context.Delegate.deactivated ctxt pkh) delegates
            | _ => Error_monad._return delegates
            end) in
  let '_ :=
    Services_registration.register1 S.info_value
      (fun (ctxt : Alpha_context.t) =>
        fun (pkh : Signature.public_key_hash) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              let=? balance := Alpha_context.Delegate.full_balance ctxt pkh in
              let=? frozen_balance_value :=
                Alpha_context.Delegate.frozen_balance_value ctxt pkh in
              let= frozen_balance_by_cycle :=
                Alpha_context.Delegate.frozen_balance_by_cycle ctxt pkh in
              let=? staking_balance :=
                Alpha_context.Delegate.staking_balance ctxt pkh in
              let= delegated_contracts :=
                Alpha_context.Delegate.delegated_contracts ctxt pkh in
              let=? delegated_balance :=
                Alpha_context.Delegate.delegated_balance ctxt pkh in
              let=? deactivated := Alpha_context.Delegate.deactivated ctxt pkh
                in
              let=? grace_period := Alpha_context.Delegate.grace_period ctxt pkh
                in
              let=? voting_power :=
                Alpha_context.Vote.get_voting_power_free ctxt pkh in
              return=?
                {| info.balance := balance;
                  info.frozen_balance := frozen_balance_value;
                  info.frozen_balance_by_cycle := frozen_balance_by_cycle;
                  info.staking_balance := staking_balance;
                  info.delegated_contracts := delegated_contracts;
                  info.delegated_balance := delegated_balance;
                  info.deactivated := deactivated;
                  info.grace_period := grace_period;
                  info.voting_power := voting_power |}) in
  let '_ :=
    Services_registration.register1 S.balance
      (fun (ctxt : Alpha_context.t) =>
        fun (pkh : Signature.public_key_hash) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              Alpha_context.Delegate.full_balance ctxt pkh) in
  let '_ :=
    Services_registration.register1 S.frozen_balance_value
      (fun (ctxt : Alpha_context.t) =>
        fun (pkh : Signature.public_key_hash) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              Alpha_context.Delegate.frozen_balance_value ctxt pkh) in
  let '_ :=
    Services_registration.register1 S.frozen_balance_by_cycle
      (fun (ctxt : Alpha_context.t) =>
        fun (pkh : Signature.public_key_hash) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              Error_monad.op_gtpipeeq
                (Alpha_context.Delegate.frozen_balance_by_cycle ctxt pkh)
                Error_monad.ok) in
  let '_ :=
    Services_registration.register1 S.staking_balance
      (fun (ctxt : Alpha_context.t) =>
        fun (pkh : Signature.public_key_hash) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              Alpha_context.Delegate.staking_balance ctxt pkh) in
  let '_ :=
    Services_registration.register1 S.delegated_contracts
      (fun (ctxt : Alpha_context.t) =>
        fun (pkh : Signature.public_key_hash) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              Error_monad.op_gtpipeeq
                (Alpha_context.Delegate.delegated_contracts ctxt pkh)
                Error_monad.ok) in
  let '_ :=
    Services_registration.register1 S.delegated_balance
      (fun (ctxt : Alpha_context.t) =>
        fun (pkh : Signature.public_key_hash) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              Alpha_context.Delegate.delegated_balance ctxt pkh) in
  let '_ :=
    Services_registration.register1 S.deactivated
      (fun (ctxt : Alpha_context.t) =>
        fun (pkh : Signature.public_key_hash) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              Alpha_context.Delegate.deactivated ctxt pkh) in
  let '_ :=
    Services_registration.register1 S.grace_period
      (fun (ctxt : Alpha_context.t) =>
        fun (pkh : Signature.public_key_hash) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              Alpha_context.Delegate.grace_period ctxt pkh) in
  Services_registration.register1 S.voting_power
    (fun (ctxt : Alpha_context.t) =>
      fun (pkh : Signature.public_key_hash) =>
        fun (function_parameter : unit) =>
          let '_ := function_parameter in
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Alpha_context.Vote.get_voting_power_free ctxt pkh).

Definition list_value {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (op_staroptstar : option bool)
  : option bool -> unit ->
  M= (Error_monad.shell_tzresult (list Signature.public_key_hash)) :=
  let active :=
    match op_staroptstar with
    | Some op_starsthstar => op_starsthstar
    | None => true
    end in
  fun (op_staroptstar : option bool) =>
    let inactive :=
      match op_staroptstar with
      | Some op_starsthstar => op_starsthstar
      | None => false
      end in
    fun (function_parameter : unit) =>
      let '_ := function_parameter in
      RPC_context.make_call0 S.list_delegate ctxt block
        {| S.list_query.active := active; S.list_query.inactive := inactive |}
        tt.

Definition info_value {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (pkh : Signature.public_key_hash)
  : M= (Error_monad.shell_tzresult info) :=
  RPC_context.make_call1 S.info_value ctxt block pkh tt tt.

Definition balance {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (pkh : Signature.public_key_hash)
  : M= (Error_monad.shell_tzresult Alpha_context.Tez.t) :=
  RPC_context.make_call1 S.balance ctxt block pkh tt tt.

Definition frozen_balance_value {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (pkh : Signature.public_key_hash)
  : M= (Error_monad.shell_tzresult Alpha_context.Tez.t) :=
  RPC_context.make_call1 S.frozen_balance_value ctxt block pkh tt tt.

Definition frozen_balance_by_cycle {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (pkh : Signature.public_key_hash)
  : M=
    (Error_monad.shell_tzresult
      (Alpha_context.Cycle.Map.(S.MAP.t) Alpha_context.Delegate.frozen_balance)) :=
  RPC_context.make_call1 S.frozen_balance_by_cycle ctxt block pkh tt tt.

Definition staking_balance {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (pkh : Signature.public_key_hash)
  : M= (Error_monad.shell_tzresult Alpha_context.Tez.t) :=
  RPC_context.make_call1 S.staking_balance ctxt block pkh tt tt.

Definition delegated_contracts {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (pkh : Signature.public_key_hash)
  : M= (Error_monad.shell_tzresult (list Alpha_context.Contract.t)) :=
  RPC_context.make_call1 S.delegated_contracts ctxt block pkh tt tt.

Definition delegated_balance {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (pkh : Signature.public_key_hash)
  : M= (Error_monad.shell_tzresult Alpha_context.Tez.t) :=
  RPC_context.make_call1 S.delegated_balance ctxt block pkh tt tt.

Definition deactivated {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (pkh : Signature.public_key_hash)
  : M= (Error_monad.shell_tzresult bool) :=
  RPC_context.make_call1 S.deactivated ctxt block pkh tt tt.

Definition grace_period {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (pkh : Signature.public_key_hash)
  : M= (Error_monad.shell_tzresult Alpha_context.Cycle.t) :=
  RPC_context.make_call1 S.grace_period ctxt block pkh tt tt.

Definition voting_power {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (pkh : Signature.public_key_hash)
  : M= (Error_monad.shell_tzresult int32) :=
  RPC_context.make_call1 S.voting_power ctxt block pkh tt tt.

Definition requested_levels
  (default : Alpha_context.Level.t * option Alpha_context.Timestamp.t)
  (ctxt : Alpha_context.context) (cycles : list Alpha_context.Cycle.t)
  (levels : list Alpha_context.Raw_level.t)
  : M? (list (Alpha_context.Level.t * option Alpha_context.Timestamp.t)) :=
  match (levels, cycles) with
  | ([], []) => return? [ default ]
  | (levels, cycles) =>
    let levels :=
      List.sort_uniq Alpha_context.Level.compare
        (List.concat
          (cons
            (List.map
              (let arg := Alpha_context.Level.from_raw ctxt in
              fun (eta : Alpha_context.Raw_level.t) => arg None eta) levels)
            (List.map (Alpha_context.Level.levels_in_cycle ctxt) cycles))) in
    Error_monad.map
      (fun (level : Alpha_context.Level.t) =>
        let current_level := Alpha_context.Level.current ctxt in
        if Alpha_context.Level.op_lteq level current_level then
          return? (level, None)
        else
          let? timestamp := Baking.earlier_predecessor_timestamp ctxt level in
          return? (level, (Some timestamp))) levels
  end.

Module Baking_rights.
  Module t.
    Record record : Set := Build {
      level : Alpha_context.Raw_level.t;
      delegate : Signature.public_key_hash;
      priority : int;
      timestamp : option Alpha_context.Timestamp.t }.
    Definition with_level level (r : record) :=
      Build level r.(delegate) r.(priority) r.(timestamp).
    Definition with_delegate delegate (r : record) :=
      Build r.(level) delegate r.(priority) r.(timestamp).
    Definition with_priority priority (r : record) :=
      Build r.(level) r.(delegate) priority r.(timestamp).
    Definition with_timestamp timestamp (r : record) :=
      Build r.(level) r.(delegate) r.(priority) timestamp.
  End t.
  Definition t := t.record.
  
  Definition encoding : Data_encoding.encoding t :=
    Data_encoding.conv
      (fun (function_parameter : t) =>
        let '{|
          t.level := level;
            t.delegate := delegate;
            t.priority := priority;
            t.timestamp := timestamp
            |} := function_parameter in
        (level, delegate, priority, timestamp))
      (fun (function_parameter :
        Alpha_context.Raw_level.t * Signature.public_key_hash * int *
          option Alpha_context.Timestamp.t) =>
        let '(level, delegate, priority, timestamp) := function_parameter in
        {| t.level := level; t.delegate := delegate; t.priority := priority;
          t.timestamp := timestamp |}) None
      (Data_encoding.obj4
        (Data_encoding.req None None "level" Alpha_context.Raw_level.encoding)
        (Data_encoding.req None None "delegate"
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
        (Data_encoding.req None None "priority" Data_encoding.uint16)
        (Data_encoding.opt None None "estimated_time"
          Alpha_context.Timestamp.encoding)).
  
  Module S.
    Definition custom_root
      : RPC_path.path Updater.rpc_context Updater.rpc_context :=
      RPC_path.op_div (RPC_path.op_div RPC_path.open_root "helpers")
        "baking_rights".
    
    Module baking_rights_query.
      Record record : Set := Build {
        levels : list Alpha_context.Raw_level.t;
        cycles : list Alpha_context.Cycle.t;
        delegates : list Signature.public_key_hash;
        max_priority : option int;
        all : bool }.
      Definition with_levels levels (r : record) :=
        Build levels r.(cycles) r.(delegates) r.(max_priority) r.(all).
      Definition with_cycles cycles (r : record) :=
        Build r.(levels) cycles r.(delegates) r.(max_priority) r.(all).
      Definition with_delegates delegates (r : record) :=
        Build r.(levels) r.(cycles) delegates r.(max_priority) r.(all).
      Definition with_max_priority max_priority (r : record) :=
        Build r.(levels) r.(cycles) r.(delegates) max_priority r.(all).
      Definition with_all all (r : record) :=
        Build r.(levels) r.(cycles) r.(delegates) r.(max_priority) all.
    End baking_rights_query.
    Definition baking_rights_query := baking_rights_query.record.
    
    Definition baking_rights_query_value : RPC_query.t baking_rights_query :=
      RPC_query.seal
        (RPC_query.op_pipeplus
          (RPC_query.op_pipeplus
            (RPC_query.op_pipeplus
              (RPC_query.op_pipeplus
                (RPC_query.op_pipeplus
                  (RPC_query.query_value
                    (fun (levels : list Alpha_context.Raw_level.t) =>
                      fun (cycles : list Alpha_context.Cycle.t) =>
                        fun (delegates : list Signature.public_key_hash) =>
                          fun (max_priority : option int) =>
                            fun (all : bool) =>
                              {| baking_rights_query.levels := levels;
                                baking_rights_query.cycles := cycles;
                                baking_rights_query.delegates := delegates;
                                baking_rights_query.max_priority := max_priority;
                                baking_rights_query.all := all |}))
                  (RPC_query.multi_field None "level"
                    Alpha_context.Raw_level.rpc_arg
                    (fun (t_value : baking_rights_query) =>
                      t_value.(baking_rights_query.levels))))
                (RPC_query.multi_field None "cycle" Alpha_context.Cycle.rpc_arg
                  (fun (t_value : baking_rights_query) =>
                    t_value.(baking_rights_query.cycles))))
              (RPC_query.multi_field None "delegate"
                Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.rpc_arg)
                (fun (t_value : baking_rights_query) =>
                  t_value.(baking_rights_query.delegates))))
            (RPC_query.opt_field None "max_priority" RPC_arg.int_value
              (fun (t_value : baking_rights_query) =>
                t_value.(baking_rights_query.max_priority))))
          (RPC_query.flag None "all"
            (fun (t_value : baking_rights_query) =>
              t_value.(baking_rights_query.all)))).
    
    Definition baking_rights
      : RPC_service.service Updater.rpc_context Updater.rpc_context
        baking_rights_query unit (list t) :=
      RPC_service.get_service
        (Some
          "Retrieves the list of delegates allowed to bake a block.\nBy default, it gives the best baking priorities for bakers that have at least one opportunity below the 64th priority for the next block.\nParameters `level` and `cycle` can be used to specify the (valid) level(s) in the past or future at which the baking rights have to be returned. When asked for (a) whole cycle(s), baking opportunities are given by default up to the priority 8.\nParameter `delegate` can be used to restrict the results to the given delegates. If parameter `all` is set, all the baking opportunities for each baker at each level are returned, instead of just the first one.\nReturns the list of baking slots. Also returns the minimal timestamps that correspond to these slots. The timestamps are omitted for levels in the past, and are only estimates for levels later that the next block, based on the hypothesis that all predecessor blocks were baked at the first priority.")
        baking_rights_query_value (Data_encoding.list_value None encoding)
        custom_root.
  End S.
  
  Definition baking_priorities
    (ctxt : Alpha_context.context) (max_prio : int)
    (function_parameter : Alpha_context.Level.t * option Time.t)
    : M=? (list t) :=
    let '(level, pred_timestamp) := function_parameter in
    let=? contract_list := Baking.baking_priorities ctxt level in
    let fix loop
      (l_value : Misc.lazy_list_t Signature.public_key) (acc_value : list t)
      (priority : int) {struct priority} : M=? (list t) :=
      if priority >i max_prio then
        Error_monad._return (List.rev acc_value)
      else
        let 'Misc.LCons pk next := l_value in
        let delegate :=
          Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.hash_value) pk in
        let=? timestamp :=
          match pred_timestamp with
          | None => return= Error_monad.ok_none
          | Some pred_timestamp =>
            return=
              (let? t_value := Baking.minimal_time ctxt priority pred_timestamp
                in
              return? (Some t_value))
          end in
        let acc_value :=
          cons
            {| t.level := level.(Alpha_context.Level.t.level);
              t.delegate := delegate; t.priority := priority;
              t.timestamp := timestamp |} acc_value in
        let=? l_value := next tt in
        loop l_value acc_value (priority +i 1) in
    loop contract_list nil 0.
  
  Definition baking_priorities_of_delegates
    (ctxt : Alpha_context.context) (all : bool) (max_prio : int)
    (delegates : list (Signature.public_key * Signature.public_key_hash))
    (function_parameter : Alpha_context.Level.t * option Time.t)
    : M=? (list t) :=
    let '(level, pred_timestamp) := function_parameter in
    let=? contract_list := Baking.baking_priorities ctxt level in
    let fix loop
      (l_value : Misc.lazy_list_t Signature.public_key) (acc_value : list t)
      (priority : int)
      (delegates : list (Signature.public_key * Signature.public_key_hash))
      {struct priority} : M=? (list t) :=
      match delegates with
      | [] => Error_monad._return (List.rev acc_value)
      | cons _ _ =>
        if priority >i max_prio then
          Error_monad._return (List.rev acc_value)
        else
          let 'Misc.LCons pk next := l_value in
          let=? l_value := next tt in
          match
            List.partition
              (fun (function_parameter :
                Signature.public_key * Signature.public_key_hash) =>
                let '(pk', _) := function_parameter in
                Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.equal) pk pk')
              delegates with
          | ([], _) => loop l_value acc_value (priority +i 1) delegates
          | (cons (_, delegate) _, delegates') =>
            let=? timestamp :=
              match pred_timestamp with
              | None => return= Error_monad.ok_none
              | Some pred_timestamp =>
                return=
                  (let? t_value :=
                    Baking.minimal_time ctxt priority pred_timestamp in
                  return? (Some t_value))
              end in
            let acc_value :=
              cons
                {| t.level := level.(Alpha_context.Level.t.level);
                  t.delegate := delegate; t.priority := priority;
                  t.timestamp := timestamp |} acc_value in
            let delegates'' :=
              if all then
                delegates
              else
                delegates' in
            loop l_value acc_value (priority +i 1) delegates''
          end
      end in
    loop contract_list nil 0 delegates.
  
  Definition remove_duplicated_delegates (rights : list t) : list t :=
    List.rev
      (Pervasives.fst
        (List.fold_left
          (fun (function_parameter :
            list t *
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH._Set).(S.INDEXES_SET.t))
            =>
            let '(acc_value, previous) := function_parameter in
            fun (r_value : t) =>
              if
                Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH._Set).(S.INDEXES_SET.mem)
                  r_value.(t.delegate) previous
              then
                (acc_value, previous)
              else
                ((cons r_value acc_value),
                  (Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH._Set).(S.INDEXES_SET.add)
                    r_value.(t.delegate) previous)))
          (nil,
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH._Set).(S.INDEXES_SET.empty))
          rights)).
  
  Definition register (function_parameter : unit) : unit :=
    let '_ := function_parameter in
    Services_registration.register0 S.baking_rights
      (fun (ctxt : Alpha_context.t) =>
        fun (q : S.baking_rights_query) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            let=? levels :=
              return=
                (requested_levels
                  ((Alpha_context.Level.succ ctxt
                    (Alpha_context.Level.current ctxt)),
                    (Some (Alpha_context.Timestamp.current ctxt))) ctxt
                  q.(S.baking_rights_query.cycles)
                  q.(S.baking_rights_query.levels)) in
            let max_priority :=
              match q.(S.baking_rights_query.max_priority) with
              | Some max => max
              | None =>
                match q.(S.baking_rights_query.cycles) with
                | [] => 64
                | cons _ _ => 8
                end
              end in
            match q.(S.baking_rights_query.delegates) with
            | [] =>
              let=? rights :=
                Error_monad.map_s (baking_priorities ctxt max_priority) levels
                in
              let rights :=
                if q.(S.baking_rights_query.all) then
                  rights
                else
                  List.map remove_duplicated_delegates rights in
              return=? (List.concat rights)
            | (cons _ _) as delegates =>
              let= delegates :=
                Lwt_list.filter_map_s
                  (fun (delegate : Alpha_context.public_key_hash) =>
                    let= function_parameter :=
                      Alpha_context.Contract.get_manager_key ctxt delegate in
                    match function_parameter with
                    | Pervasives.Ok pk => Lwt._return (Some (pk, delegate))
                    | Pervasives.Error _ => Lwt.return_none
                    end) delegates in
              Error_monad.op_gtpipeeqquestion
                (Error_monad.map_s
                  (fun (level : Alpha_context.Level.t * option Time.t) =>
                    baking_priorities_of_delegates ctxt
                      q.(S.baking_rights_query.all) max_priority delegates level)
                  levels) List.concat
            end).
  
  Definition get {A : Set}
    (ctxt : RPC_context.simple A)
    (op_staroptstar : option (list Alpha_context.Raw_level.t))
    : option (list Alpha_context.Cycle.t) ->
    option (list Signature.public_key_hash) -> option bool -> option int -> A ->
    M= (Error_monad.shell_tzresult (list t)) :=
    let levels :=
      match op_staroptstar with
      | Some op_starsthstar => op_starsthstar
      | None => nil
      end in
    fun (op_staroptstar : option (list Alpha_context.Cycle.t)) =>
      let cycles :=
        match op_staroptstar with
        | Some op_starsthstar => op_starsthstar
        | None => nil
        end in
      fun (op_staroptstar : option (list Signature.public_key_hash)) =>
        let delegates :=
          match op_staroptstar with
          | Some op_starsthstar => op_starsthstar
          | None => nil
          end in
        fun (op_staroptstar : option bool) =>
          let all :=
            match op_staroptstar with
            | Some op_starsthstar => op_starsthstar
            | None => false
            end in
          fun (max_priority : option int) =>
            fun (block : A) =>
              RPC_context.make_call0 S.baking_rights ctxt block
                {| S.baking_rights_query.levels := levels;
                  S.baking_rights_query.cycles := cycles;
                  S.baking_rights_query.delegates := delegates;
                  S.baking_rights_query.max_priority := max_priority;
                  S.baking_rights_query.all := all |} tt.
End Baking_rights.

Module Endorsing_rights.
  Module t.
    Record record : Set := Build {
      level : Alpha_context.Raw_level.t;
      delegate : Signature.public_key_hash;
      slots : list int;
      estimated_time : option Time.t }.
    Definition with_level level (r : record) :=
      Build level r.(delegate) r.(slots) r.(estimated_time).
    Definition with_delegate delegate (r : record) :=
      Build r.(level) delegate r.(slots) r.(estimated_time).
    Definition with_slots slots (r : record) :=
      Build r.(level) r.(delegate) slots r.(estimated_time).
    Definition with_estimated_time estimated_time (r : record) :=
      Build r.(level) r.(delegate) r.(slots) estimated_time.
  End t.
  Definition t := t.record.
  
  Definition encoding : Data_encoding.encoding t :=
    Data_encoding.conv
      (fun (function_parameter : t) =>
        let '{|
          t.level := level;
            t.delegate := delegate;
            t.slots := slots;
            t.estimated_time := estimated_time
            |} := function_parameter in
        (level, delegate, slots, estimated_time))
      (fun (function_parameter :
        Alpha_context.Raw_level.t * Signature.public_key_hash * list int *
          option Time.t) =>
        let '(level, delegate, slots, estimated_time) := function_parameter in
        {| t.level := level; t.delegate := delegate; t.slots := slots;
          t.estimated_time := estimated_time |}) None
      (Data_encoding.obj4
        (Data_encoding.req None None "level" Alpha_context.Raw_level.encoding)
        (Data_encoding.req None None "delegate"
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
        (Data_encoding.req None None "slots"
          (Data_encoding.list_value None Data_encoding.uint16))
        (Data_encoding.opt None None "estimated_time"
          Alpha_context.Timestamp.encoding)).
  
  Module S.
    Definition custom_root
      : RPC_path.path Updater.rpc_context Updater.rpc_context :=
      RPC_path.op_div (RPC_path.op_div RPC_path.open_root "helpers")
        "endorsing_rights".
    
    Module endorsing_rights_query.
      Record record : Set := Build {
        levels : list Alpha_context.Raw_level.t;
        cycles : list Alpha_context.Cycle.t;
        delegates : list Signature.public_key_hash }.
      Definition with_levels levels (r : record) :=
        Build levels r.(cycles) r.(delegates).
      Definition with_cycles cycles (r : record) :=
        Build r.(levels) cycles r.(delegates).
      Definition with_delegates delegates (r : record) :=
        Build r.(levels) r.(cycles) delegates.
    End endorsing_rights_query.
    Definition endorsing_rights_query := endorsing_rights_query.record.
    
    Definition endorsing_rights_query_value
      : RPC_query.t endorsing_rights_query :=
      RPC_query.seal
        (RPC_query.op_pipeplus
          (RPC_query.op_pipeplus
            (RPC_query.op_pipeplus
              (RPC_query.query_value
                (fun (levels : list Alpha_context.Raw_level.t) =>
                  fun (cycles : list Alpha_context.Cycle.t) =>
                    fun (delegates : list Signature.public_key_hash) =>
                      {| endorsing_rights_query.levels := levels;
                        endorsing_rights_query.cycles := cycles;
                        endorsing_rights_query.delegates := delegates |}))
              (RPC_query.multi_field None "level"
                Alpha_context.Raw_level.rpc_arg
                (fun (t_value : endorsing_rights_query) =>
                  t_value.(endorsing_rights_query.levels))))
            (RPC_query.multi_field None "cycle" Alpha_context.Cycle.rpc_arg
              (fun (t_value : endorsing_rights_query) =>
                t_value.(endorsing_rights_query.cycles))))
          (RPC_query.multi_field None "delegate"
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.rpc_arg)
            (fun (t_value : endorsing_rights_query) =>
              t_value.(endorsing_rights_query.delegates)))).
    
    Definition endorsing_rights
      : RPC_service.service Updater.rpc_context Updater.rpc_context
        endorsing_rights_query unit (list t) :=
      RPC_service.get_service
        (Some
          "Retrieves the delegates allowed to endorse a block.\nBy default, it gives the endorsement slots for delegates that have at least one in the next block.\nParameters `level` and `cycle` can be used to specify the (valid) level(s) in the past or future at which the endorsement rights have to be returned. Parameter `delegate` can be used to restrict the results to the given delegates.\nReturns the list of endorsement slots. Also returns the minimal timestamps that correspond to these slots. The timestamps are omitted for levels in the past, and are only estimates for levels later that the next block, based on the hypothesis that all predecessor blocks were baked at the first priority.")
        endorsing_rights_query_value (Data_encoding.list_value None encoding)
        custom_root.
  End S.
  
  Definition endorsement_slots
    (ctxt : Alpha_context.context)
    (function_parameter : Alpha_context.Level.t * option Time.t)
    : M=? (list t) :=
    let '(level, estimated_time) := function_parameter in
    let=? rights := Baking.endorsement_rights ctxt level in
    return=?
      (Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.fold)
        (fun (delegate : Signature.public_key_hash) =>
          fun (function_parameter : Alpha_context.public_key * list int * bool)
            =>
            let '(_, slots, _) := function_parameter in
            fun (acc_value : list t) =>
              cons
                {| t.level := level.(Alpha_context.Level.t.level);
                  t.delegate := delegate; t.slots := slots;
                  t.estimated_time := estimated_time |} acc_value) rights nil).
  
  Definition register (function_parameter : unit) : unit :=
    let '_ := function_parameter in
    Services_registration.register0 S.endorsing_rights
      (fun (ctxt : Alpha_context.t) =>
        fun (q : S.endorsing_rights_query) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            let=? levels :=
              return=
                (requested_levels
                  ((Alpha_context.Level.current ctxt),
                    (Some (Alpha_context.Timestamp.current ctxt))) ctxt
                  q.(S.endorsing_rights_query.cycles)
                  q.(S.endorsing_rights_query.levels)) in
            let=? rights := Error_monad.map_s (endorsement_slots ctxt) levels in
            let rights := List.concat rights in
            match q.(S.endorsing_rights_query.delegates) with
            | [] => return=? rights
            | (cons _ _) as delegates =>
              return=?
                (let is_requested (p_value : t) : bool :=
                  List._exists
                    (Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.equal)
                      p_value.(t.delegate)) delegates in
                List.filter is_requested rights)
            end).
  
  Definition get {A : Set}
    (ctxt : RPC_context.simple A)
    (op_staroptstar : option (list Alpha_context.Raw_level.t))
    : option (list Alpha_context.Cycle.t) ->
    option (list Signature.public_key_hash) -> A ->
    M= (Error_monad.shell_tzresult (list t)) :=
    let levels :=
      match op_staroptstar with
      | Some op_starsthstar => op_starsthstar
      | None => nil
      end in
    fun (op_staroptstar : option (list Alpha_context.Cycle.t)) =>
      let cycles :=
        match op_staroptstar with
        | Some op_starsthstar => op_starsthstar
        | None => nil
        end in
      fun (op_staroptstar : option (list Signature.public_key_hash)) =>
        let delegates :=
          match op_staroptstar with
          | Some op_starsthstar => op_starsthstar
          | None => nil
          end in
        fun (block : A) =>
          RPC_context.make_call0 S.endorsing_rights ctxt block
            {| S.endorsing_rights_query.levels := levels;
              S.endorsing_rights_query.cycles := cycles;
              S.endorsing_rights_query.delegates := delegates |} tt.
End Endorsing_rights.

Module Endorsing_power.
  Definition endorsing_power
    (ctxt : Alpha_context.context)
    (function_parameter : Alpha_context.packed_operation * Chain_id.t)
    : M=? int :=
    let '(operation, chain_id) := function_parameter in
    let 'Alpha_context.Operation_data data :=
      operation.(Alpha_context.packed_operation.protocol_data) in
    match data.(Alpha_context.protocol_data.contents) with
    | Alpha_context.Single (Alpha_context.Endorsement _) =>
      let=? '(_, slots, _) :=
        Baking.check_endorsement_rights ctxt chain_id
          {|
            Alpha_context.operation.shell :=
              operation.(Alpha_context.packed_operation.shell);
            Alpha_context.operation.protocol_data := data |} in
      return=? (List.length slots)
    | _ => Pervasives.failwith "Operation is not an endorsement"
    end.
  
  Module S.
    Definition endorsing_power
      : RPC_service.service Updater.rpc_context Updater.rpc_context unit
        (Alpha_context.Operation.packed * Chain_id.t) int :=
      RPC_service.post_service
        (Some
          "Get the endorsing power of an endorsement, that is, the number of slots that the endorser has")
        RPC_query.empty
        (Data_encoding.obj2
          (Data_encoding.req None None "endorsement_operation"
            Alpha_context.Operation.encoding)
          (Data_encoding.req None None "chain_id" Chain_id.encoding))
        Data_encoding.int31
        (RPC_path.op_div RPC_path.open_root "endorsing_power").
  End S.
  
  Definition register (function_parameter : unit) : unit :=
    let '_ := function_parameter in
    Services_registration.register0 S.endorsing_power
      (fun (ctxt : Alpha_context.t) =>
        fun (function_parameter : unit) =>
          let '_ := function_parameter in
          fun (function_parameter : Alpha_context.Operation.packed * Chain_id.t)
            =>
            let '(op, chain_id) := function_parameter in
            endorsing_power ctxt (op, chain_id)).
  
  Definition get {A : Set}
    (ctxt : RPC_context.simple A) (block : A)
    (op : Alpha_context.Operation.packed) (chain_id : Chain_id.t)
    : M= (Error_monad.shell_tzresult int) :=
    RPC_context.make_call0 S.endorsing_power ctxt block tt (op, chain_id).
End Endorsing_power.

Module Required_endorsements.
  Definition required_endorsements
    (ctxt : Alpha_context.context) (block_delay : Alpha_context.Period.t)
    : int := Baking.minimum_allowed_endorsements ctxt block_delay.
  
  Module S.
    Module t.
      Record record : Set := Build {
        block_delay : Alpha_context.Period.t }.
      Definition with_block_delay block_delay (r : record) :=
        Build block_delay.
    End t.
    Definition t := t.record.
    
    Definition required_endorsements_query : RPC_query.t t :=
      RPC_query.seal
        (RPC_query.op_pipeplus
          (RPC_query.query_value
            (fun (block_delay : Alpha_context.Period.t) =>
              {| t.block_delay := block_delay |}))
          (RPC_query.field_value None "block_delay" Alpha_context.Period.rpc_arg
            Alpha_context.Period.zero
            (fun (t_value : t) => t_value.(t.block_delay)))).
    
    Definition required_endorsements
      : RPC_service.service Updater.rpc_context Updater.rpc_context t unit int :=
      RPC_service.get_service
        (Some
          "Minimum number of endorsements for a block to be valid, given a delay of the block's timestamp with respect to the minimum time to bake at the block's priority")
        required_endorsements_query Data_encoding.int31
        (RPC_path.op_div RPC_path.open_root "required_endorsements").
  End S.
  
  Definition register (function_parameter : unit) : unit :=
    let '_ := function_parameter in
    Services_registration.register0 S.required_endorsements
      (fun (ctxt : Alpha_context.t) =>
        fun (function_parameter : S.t) =>
          let '{| S.t.block_delay := block_delay |} := function_parameter in
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Error_monad._return (required_endorsements ctxt block_delay)).
  
  Definition get {A : Set}
    (ctxt : RPC_context.simple A) (block : A)
    (block_delay : Alpha_context.Period.t)
    : M= (Error_monad.shell_tzresult int) :=
    RPC_context.make_call0 S.required_endorsements ctxt block
      {| S.t.block_delay := block_delay |} tt.
End Required_endorsements.

Module Minimal_valid_time.
  Definition minimal_valid_time
    (ctxt : Alpha_context.context) (priority : int) (endorsing_power : int)
    : M? Time.t := Baking.minimal_valid_time ctxt priority endorsing_power.
  
  Module S.
    Module t.
      Record record : Set := Build {
        priority : int;
        endorsing_power : int }.
      Definition with_priority priority (r : record) :=
        Build priority r.(endorsing_power).
      Definition with_endorsing_power endorsing_power (r : record) :=
        Build r.(priority) endorsing_power.
    End t.
    Definition t := t.record.
    
    Definition minimal_valid_time_query : RPC_query.t t :=
      RPC_query.seal
        (RPC_query.op_pipeplus
          (RPC_query.op_pipeplus
            (RPC_query.query_value
              (fun (priority : int) =>
                fun (endorsing_power : int) =>
                  {| t.priority := priority;
                    t.endorsing_power := endorsing_power |}))
            (RPC_query.field_value None "priority" RPC_arg.int_value 0
              (fun (t_value : t) => t_value.(t.priority))))
          (RPC_query.field_value None "endorsing_power" RPC_arg.int_value 0
            (fun (t_value : t) => t_value.(t.endorsing_power)))).
    
    Definition minimal_valid_time
      : RPC_service.service Updater.rpc_context Updater.rpc_context t unit
        Time.t :=
      RPC_service.get_service
        (Some
          "Minimal valid time for a block given a priority and an endorsing power.")
        minimal_valid_time_query Time.encoding
        (RPC_path.op_div RPC_path.open_root "minimal_valid_time").
  End S.
  
  Definition register (function_parameter : unit) : unit :=
    let '_ := function_parameter in
    Services_registration.register0 S.minimal_valid_time
      (fun (ctxt : Alpha_context.t) =>
        fun (function_parameter : S.t) =>
          let '{|
            S.t.priority := priority;
              S.t.endorsing_power := endorsing_power
              |} := function_parameter in
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Lwt._return (minimal_valid_time ctxt priority endorsing_power)).
  
  Definition get {A : Set}
    (ctxt : RPC_context.simple A) (block : A) (priority : int)
    (endorsing_power : int) : M= (Error_monad.shell_tzresult Time.t) :=
    RPC_context.make_call0 S.minimal_valid_time ctxt block
      {| S.t.priority := priority; S.t.endorsing_power := endorsing_power |} tt.
End Minimal_valid_time.

Definition register (function_parameter : unit) : unit :=
  let '_ := function_parameter in
  let '_ := begin_register tt in
  let '_ := Baking_rights.register tt in
  let '_ := Endorsing_rights.register tt in
  let '_ := Endorsing_power.register tt in
  let '_ := Required_endorsements.register tt in
  Minimal_valid_time.register tt.

Definition endorsement_rights
  (ctxt : Alpha_context.context) (level : Alpha_context.Level.t)
  : M=? (list Signature.public_key_hash) :=
  let=? l_value := Endorsing_rights.endorsement_slots ctxt (level, None) in
  return=?
    (List.map
      (fun (function_parameter : Endorsing_rights.t) =>
        let '{| Endorsing_rights.t.delegate := delegate |} := function_parameter
          in
        delegate) l_value).

Definition baking_rights
  (ctxt : Alpha_context.context) (max_priority : option int)
  : M=?
    (Alpha_context.Raw_level.t *
      list (Signature.public_key_hash * option Alpha_context.Timestamp.t)) :=
  let max :=
    match max_priority with
    | None => 64
    | Some m => m
    end in
  let level := Alpha_context.Level.current ctxt in
  let=? l_value := Baking_rights.baking_priorities ctxt max (level, None) in
  return=?
    (level.(Alpha_context.Level.t.level),
      (List.map
        (fun (function_parameter : Baking_rights.t) =>
          let '{|
            Baking_rights.t.delegate := delegate;
              Baking_rights.t.timestamp := timestamp
              |} := function_parameter in
          (delegate, timestamp)) l_value)).

Definition endorsing_power
  (ctxt : Alpha_context.context)
  (operation : Alpha_context.packed_operation * Chain_id.t) : M=? int :=
  Endorsing_power.endorsing_power ctxt operation.

Definition required_endorsements
  (ctxt : Alpha_context.context) (delay : Alpha_context.Period.t) : int :=
  Required_endorsements.required_endorsements ctxt delay.

Definition minimal_valid_time
  (ctxt : Alpha_context.context) (priority : int) (endorsing_power : int)
  : M? Time.t :=
  Minimal_valid_time.minimal_valid_time ctxt priority endorsing_power.
