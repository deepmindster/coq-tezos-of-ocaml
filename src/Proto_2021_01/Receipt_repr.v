Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Contract_repr.
Require TezosOfOCaml.Proto_2021_01.Cycle_repr.
Require TezosOfOCaml.Proto_2021_01.Tez_repr.

Inductive balance : Set :=
| Contract : Contract_repr.t -> balance
| Rewards : Signature.public_key_hash -> Cycle_repr.t -> balance
| Fees : Signature.public_key_hash -> Cycle_repr.t -> balance
| Deposits : Signature.public_key_hash -> Cycle_repr.t -> balance.

Definition balance_encoding : Data_encoding.encoding balance :=
  (let arg := Data_encoding.def "operation_metadata.alpha.balance" in
  fun (eta : Data_encoding.encoding balance) => arg None None eta)
    (Data_encoding.union None
      [
        Data_encoding.case_value "Contract" None (Data_encoding.Tag 0)
          (Data_encoding.obj2
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "contract"))
            (Data_encoding.req None None "contract"
              Contract_repr.encoding))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Contract c => Some (tt, c)
            | _ => None
            end)
          (fun (function_parameter : unit * Contract_repr.contract) =>
            let '(_, c) := function_parameter in
            Contract c);
        Data_encoding.case_value "Rewards" None (Data_encoding.Tag 1)
          (Data_encoding.obj4
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "freezer"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "rewards"))
            (Data_encoding.req None None "delegate"
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
            (Data_encoding.req None None "cycle" Cycle_repr.encoding))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Rewards d l_value => Some (tt, tt, d, l_value)
            | _ => None
            end)
          (fun (function_parameter :
            unit * unit * Signature.public_key_hash * Cycle_repr.cycle)
            =>
            let '(_, _, d, l_value) := function_parameter in
            Rewards d l_value);
        Data_encoding.case_value "Fees" None (Data_encoding.Tag 2)
          (Data_encoding.obj4
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "freezer"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "fees"))
            (Data_encoding.req None None "delegate"
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
            (Data_encoding.req None None "cycle" Cycle_repr.encoding))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Fees d l_value => Some (tt, tt, d, l_value)
            | _ => None
            end)
          (fun (function_parameter :
            unit * unit * Signature.public_key_hash * Cycle_repr.cycle)
            =>
            let '(_, _, d, l_value) := function_parameter in
            Fees d l_value);
        Data_encoding.case_value "Deposits" None (Data_encoding.Tag 3)
          (Data_encoding.obj4
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "freezer"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "deposits"))
            (Data_encoding.req None None "delegate"
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
            (Data_encoding.req None None "cycle" Cycle_repr.encoding))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Deposits d l_value => Some (tt, tt, d, l_value)
            | _ => None
            end)
          (fun (function_parameter :
            unit * unit * Signature.public_key_hash * Cycle_repr.cycle)
            =>
            let '(_, _, d, l_value) := function_parameter in
            Deposits d l_value)
      ]).

Inductive balance_update : Set :=
| Debited : Tez_repr.t -> balance_update
| Credited : Tez_repr.t -> balance_update.

Definition balance_update_encoding : Data_encoding.encoding balance_update :=
  (let arg := Data_encoding.def "operation_metadata.alpha.balance_update" in
  fun (eta : Data_encoding.encoding balance_update) => arg None None eta)
    (Data_encoding.obj1
      (Data_encoding.req None None "change"
        (Data_encoding.conv
          (fun (function_parameter : balance_update) =>
            match function_parameter with
            | Credited v => Tez_repr.to_mutez v
            | Debited v => Int64.neg (Tez_repr.to_mutez v)
            end)
          (Data_encoding.Json.wrap_error
            (fun (v : int64) =>
              if v <i64 0 then
                match Tez_repr.of_mutez (Int64.neg v) with
                | Some v => Debited v
                | None => Pervasives.failwith "Qty.of_mutez"
                end
              else
                match Tez_repr.of_mutez v with
                | Some v => Credited v
                | None => Pervasives.failwith "Qty.of_mutez"
                end)) None Data_encoding.int64_value))).

Inductive update_origin : Set :=
| Block_application : update_origin
| Protocol_migration : update_origin.

Definition update_origin_encoding : Data_encoding.encoding update_origin :=
  (let arg := Data_encoding.def "operation_metadata.alpha.update_origin" in
  fun (eta : Data_encoding.encoding update_origin) => arg None None eta)
    (Data_encoding.obj1
      (Data_encoding.req None None "origin"
        (Data_encoding.union None
          [
            Data_encoding.case_value "Block_application" None
              (Data_encoding.Tag 0) (Data_encoding.constant "block")
              (fun (function_parameter : update_origin) =>
                match function_parameter with
                | Block_application => Some tt
                | _ => None
                end)
              (fun (function_parameter : unit) =>
                let '_ := function_parameter in
                Block_application);
            Data_encoding.case_value "Protocol_migration" None
              (Data_encoding.Tag 1) (Data_encoding.constant "migration")
              (fun (function_parameter : update_origin) =>
                match function_parameter with
                | Protocol_migration => Some tt
                | _ => None
                end)
              (fun (function_parameter : unit) =>
                let '_ := function_parameter in
                Protocol_migration)
          ]))).

Definition balance_updates : Set :=
  list (balance * balance_update * update_origin).

Definition balance_updates_encoding
  : Data_encoding.encoding (list (balance * balance_update * update_origin)) :=
  (let arg := Data_encoding.def "operation_metadata.alpha.balance_updates" in
  fun (eta :
    Data_encoding.encoding (list (balance * balance_update * update_origin))) =>
    arg None None eta)
    (Data_encoding.list_value None
      (Data_encoding.conv
        (fun (function_parameter : balance * balance_update * update_origin) =>
          let '(balance, balance_update, update_origin) := function_parameter in
          ((balance, balance_update), update_origin))
        (fun (function_parameter : (balance * balance_update) * update_origin)
          =>
          let '((balance, balance_update), update_origin) := function_parameter
            in
          (balance, balance_update, update_origin)) None
        (Data_encoding.merge_objs
          (Data_encoding.merge_objs balance_encoding balance_update_encoding)
          update_origin_encoding))).

Definition cleanup_balance_updates {A B : Set}
  (balance_updates : list (A * balance_update * B))
  : list (A * balance_update * B) :=
  List.filter
    (fun (function_parameter : A * balance_update * B) =>
      match function_parameter with
      | (_, (Credited update | Debited update), _) =>
        Pervasives.not (Tez_repr.equal update Tez_repr.zero)
      end) balance_updates.
