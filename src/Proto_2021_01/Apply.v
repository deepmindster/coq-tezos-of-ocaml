Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Alpha_context.
Require TezosOfOCaml.Proto_2021_01.Amendment.
Require TezosOfOCaml.Proto_2021_01.Apply_results.
Require TezosOfOCaml.Proto_2021_01.Baking.
Require TezosOfOCaml.Proto_2021_01.Blinded_public_key_hash.
Require TezosOfOCaml.Proto_2021_01.Michelson_v1_gas.
Require TezosOfOCaml.Proto_2021_01.Michelson_v1_primitives.
Require TezosOfOCaml.Proto_2021_01.Nonce_hash.
Require TezosOfOCaml.Proto_2021_01.Script_interpreter.
Require TezosOfOCaml.Proto_2021_01.Script_ir_translator.
Require TezosOfOCaml.Proto_2021_01.Script_typed_ir.
Require TezosOfOCaml.Proto_2021_01.Storage.

Module Invalid_commitment.
  Record record : Set := Build {
    expected : bool }.
  Definition with_expected expected (r : record) :=
    Build expected.
End Invalid_commitment.
Definition Invalid_commitment := Invalid_commitment.record.

Module Inconsistent_double_endorsement_evidence.
  Record record : Set := Build {
    delegate1 : Signature.public_key_hash;
    delegate2 : Signature.public_key_hash }.
  Definition with_delegate1 delegate1 (r : record) :=
    Build delegate1 r.(delegate2).
  Definition with_delegate2 delegate2 (r : record) :=
    Build r.(delegate1) delegate2.
End Inconsistent_double_endorsement_evidence.
Definition Inconsistent_double_endorsement_evidence :=
  Inconsistent_double_endorsement_evidence.record.

Module Too_early_double_endorsement_evidence.
  Record record : Set := Build {
    level : Alpha_context.Raw_level.t;
    current : Alpha_context.Raw_level.t }.
  Definition with_level level (r : record) :=
    Build level r.(current).
  Definition with_current current (r : record) :=
    Build r.(level) current.
End Too_early_double_endorsement_evidence.
Definition Too_early_double_endorsement_evidence :=
  Too_early_double_endorsement_evidence.record.

Module Outdated_double_endorsement_evidence.
  Record record : Set := Build {
    level : Alpha_context.Raw_level.t;
    last : Alpha_context.Raw_level.t }.
  Definition with_level level (r : record) :=
    Build level r.(last).
  Definition with_last last (r : record) :=
    Build r.(level) last.
End Outdated_double_endorsement_evidence.
Definition Outdated_double_endorsement_evidence :=
  Outdated_double_endorsement_evidence.record.

Module Invalid_double_baking_evidence.
  Record record : Set := Build {
    hash1 : Block_hash.t;
    level1 : Int32.t;
    hash2 : Block_hash.t;
    level2 : Int32.t }.
  Definition with_hash1 hash1 (r : record) :=
    Build hash1 r.(level1) r.(hash2) r.(level2).
  Definition with_level1 level1 (r : record) :=
    Build r.(hash1) level1 r.(hash2) r.(level2).
  Definition with_hash2 hash2 (r : record) :=
    Build r.(hash1) r.(level1) hash2 r.(level2).
  Definition with_level2 level2 (r : record) :=
    Build r.(hash1) r.(level1) r.(hash2) level2.
End Invalid_double_baking_evidence.
Definition Invalid_double_baking_evidence :=
  Invalid_double_baking_evidence.record.

Module Inconsistent_double_baking_evidence.
  Record record : Set := Build {
    delegate1 : Signature.public_key_hash;
    delegate2 : Signature.public_key_hash }.
  Definition with_delegate1 delegate1 (r : record) :=
    Build delegate1 r.(delegate2).
  Definition with_delegate2 delegate2 (r : record) :=
    Build r.(delegate1) delegate2.
End Inconsistent_double_baking_evidence.
Definition Inconsistent_double_baking_evidence :=
  Inconsistent_double_baking_evidence.record.

Module Too_early_double_baking_evidence.
  Record record : Set := Build {
    level : Alpha_context.Raw_level.t;
    current : Alpha_context.Raw_level.t }.
  Definition with_level level (r : record) :=
    Build level r.(current).
  Definition with_current current (r : record) :=
    Build r.(level) current.
End Too_early_double_baking_evidence.
Definition Too_early_double_baking_evidence :=
  Too_early_double_baking_evidence.record.

Module Outdated_double_baking_evidence.
  Record record : Set := Build {
    level : Alpha_context.Raw_level.t;
    last : Alpha_context.Raw_level.t }.
  Definition with_level level (r : record) :=
    Build level r.(last).
  Definition with_last last (r : record) :=
    Build r.(level) last.
End Outdated_double_baking_evidence.
Definition Outdated_double_baking_evidence :=
  Outdated_double_baking_evidence.record.

Module Invalid_activation.
  Record record : Set := Build {
    pkh : Ed25519.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.t) }.
  Definition with_pkh pkh (r : record) :=
    Build pkh.
End Invalid_activation.
Definition Invalid_activation := Invalid_activation.record.

Module Not_enough_endorsements_for_priority.
  Record record : Set := Build {
    required : int;
    priority : int;
    endorsements : int;
    timestamp : Time.t }.
  Definition with_required required (r : record) :=
    Build required r.(priority) r.(endorsements) r.(timestamp).
  Definition with_priority priority (r : record) :=
    Build r.(required) priority r.(endorsements) r.(timestamp).
  Definition with_endorsements endorsements (r : record) :=
    Build r.(required) r.(priority) endorsements r.(timestamp).
  Definition with_timestamp timestamp (r : record) :=
    Build r.(required) r.(priority) r.(endorsements) timestamp.
End Not_enough_endorsements_for_priority.
Definition Not_enough_endorsements_for_priority :=
  Not_enough_endorsements_for_priority.record.

(** Init function; without side-effects in Coq *)
Definition init_module : unit :=
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "operation.wrong_endorsement_predecessor" "Wrong endorsement predecessor"
      "Trying to include an endorsement in a block that is not the successor of the endorsed one"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : Block_hash.t * Block_hash.t) =>
            let '(e, p_value) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "Wrong predecessor "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal ", expected "
                      (CamlinternalFormatBasics.Alpha
                        CamlinternalFormatBasics.End_of_format))))
                "Wrong predecessor %a, expected %a") Block_hash.pp p_value
              Block_hash.pp e))
      (Data_encoding.obj2
        (Data_encoding.req None None "expected" Block_hash.encoding)
        (Data_encoding.req None None "provided" Block_hash.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Wrong_endorsement_predecessor" then
            let '(e, p_value) := cast (Block_hash.t * Block_hash.t) payload in
            Some (e, p_value)
          else None
        end)
      (fun (function_parameter : Block_hash.t * Block_hash.t) =>
        let '(e, p_value) := function_parameter in
        Build_extensible "Wrong_endorsement_predecessor"
          (Block_hash.t * Block_hash.t) (e, p_value)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "operation.wrong_voting_period" "Wrong voting period"
      "Trying to include a proposal or ballot meant for another voting period"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : int32 * int32) =>
            let '(e, p_value) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "Wrong voting period "
                  (CamlinternalFormatBasics.Int32 CamlinternalFormatBasics.Int_d
                    CamlinternalFormatBasics.No_padding
                    CamlinternalFormatBasics.No_precision
                    (CamlinternalFormatBasics.String_literal ", current is "
                      (CamlinternalFormatBasics.Int32
                        CamlinternalFormatBasics.Int_d
                        CamlinternalFormatBasics.No_padding
                        CamlinternalFormatBasics.No_precision
                        CamlinternalFormatBasics.End_of_format))))
                "Wrong voting period %ld, current is %ld") p_value e))
      (Data_encoding.obj2
        (Data_encoding.req None None "current_index" Data_encoding.int32_value)
        (Data_encoding.req None None "provided_index" Data_encoding.int32_value))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Wrong_voting_period" then
            let '(e, p_value) := cast (int32 * int32) payload in
            Some (e, p_value)
          else None
        end)
      (fun (function_parameter : int32 * int32) =>
        let '(e, p_value) := function_parameter in
        Build_extensible "Wrong_voting_period" (int32 * int32) (e, p_value)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Branch
      "operation.duplicate_endorsement" "Duplicate endorsement"
      "Two endorsements received from same delegate"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (k : Signature.public_key_hash) =>
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Duplicate endorsement from delegate "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal
                      " (possible replay attack)."
                      CamlinternalFormatBasics.End_of_format)))
                "Duplicate endorsement from delegate %a (possible replay attack).")
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp_short) k))
      (Data_encoding.obj1
        (Data_encoding.req None None "delegate"
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Duplicate_endorsement" then
            let 'k := cast Signature.public_key_hash payload in
            Some k
          else None
        end)
      (fun (k : Signature.public_key_hash) =>
        Build_extensible "Duplicate_endorsement" Signature.public_key_hash k) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "operation.invalid_endorsement_level" "Unexpected level in endorsement"
      "The level of an endorsement is inconsistent with the  provided block hash."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Unexpected level in endorsement."
                  CamlinternalFormatBasics.End_of_format)
                "Unexpected level in endorsement."))) Data_encoding.unit_value
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_endorsement_level" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Invalid_endorsement_level" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "block.invalid_commitment" "Invalid commitment in block header"
      "The block header has invalid commitment."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (expected : bool) =>
            if expected then
              Format.fprintf ppf
                (CamlinternalFormatBasics.Format
                  (CamlinternalFormatBasics.String_literal
                    "Missing seed's nonce commitment in block header."
                    CamlinternalFormatBasics.End_of_format)
                  "Missing seed's nonce commitment in block header.")
            else
              Format.fprintf ppf
                (CamlinternalFormatBasics.Format
                  (CamlinternalFormatBasics.String_literal
                    "Unexpected seed's nonce commitment in block header."
                    CamlinternalFormatBasics.End_of_format)
                  "Unexpected seed's nonce commitment in block header.")))
      (Data_encoding.obj1
        (Data_encoding.req None None "expected" Data_encoding.bool_value))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_commitment" then
            let '{| Invalid_commitment.expected := expected |} :=
              cast Invalid_commitment payload in
            Some expected
          else None
        end)
      (fun (expected : bool) =>
        Build_extensible "Invalid_commitment" Invalid_commitment
          {| Invalid_commitment.expected := expected |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "internal_operation_replay" "Internal operation replay"
      "An internal operation was emitted twice by a script"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : Alpha_context.packed_internal_operation) =>
            let
              'Alpha_context.Internal_operation {|
                Alpha_context.internal_operation.nonce := nonce_value |} :=
              function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "Internal operation "
                  (CamlinternalFormatBasics.Int CamlinternalFormatBasics.Int_d
                    CamlinternalFormatBasics.No_padding
                    CamlinternalFormatBasics.No_precision
                    (CamlinternalFormatBasics.String_literal
                      " was emitted twice by a script"
                      CamlinternalFormatBasics.End_of_format)))
                "Internal operation %d was emitted twice by a script")
              nonce_value)) Alpha_context.Operation.internal_operation_encoding
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Internal_operation_replay" then
            let 'op := cast Alpha_context.packed_internal_operation payload in
            Some op
          else None
        end)
      (fun (op : Alpha_context.packed_internal_operation) =>
        Build_extensible "Internal_operation_replay"
          Alpha_context.packed_internal_operation op) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "block.invalid_double_endorsement_evidence"
      "Invalid double endorsement evidence"
      "A double-endorsement evidence is malformed"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Malformed double-endorsement evidence"
                  CamlinternalFormatBasics.End_of_format)
                "Malformed double-endorsement evidence"))) Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_double_endorsement_evidence" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Invalid_double_endorsement_evidence" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "block.inconsistent_double_endorsement_evidence"
      "Inconsistent double endorsement evidence"
      "A double-endorsement evidence is inconsistent  (two distinct delegates)"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            Signature.public_key_hash * Signature.public_key_hash) =>
            let '(delegate1, delegate2) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Inconsistent double-endorsement evidence  (distinct delegate: "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal " and "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.Char_literal ")" % char
                          CamlinternalFormatBasics.End_of_format)))))
                "Inconsistent double-endorsement evidence  (distinct delegate: %a and %a)")
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp_short)
              delegate1
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp_short)
              delegate2))
      (Data_encoding.obj2
        (Data_encoding.req None None "delegate1"
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
        (Data_encoding.req None None "delegate2"
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Inconsistent_double_endorsement_evidence" then
            let '{|
              Inconsistent_double_endorsement_evidence.delegate1 := delegate1;
                Inconsistent_double_endorsement_evidence.delegate2 := delegate2
                |} := cast Inconsistent_double_endorsement_evidence payload in
            Some (delegate1, delegate2)
          else None
        end)
      (fun (function_parameter :
        Signature.public_key_hash * Signature.public_key_hash) =>
        let '(delegate1, delegate2) := function_parameter in
        Build_extensible "Inconsistent_double_endorsement_evidence"
          Inconsistent_double_endorsement_evidence
          {| Inconsistent_double_endorsement_evidence.delegate1 := delegate1;
            Inconsistent_double_endorsement_evidence.delegate2 := delegate2 |})
    in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Branch
      "block.unrequired_double_endorsement_evidence"
      "Unrequired double endorsement evidence"
      "A double-endorsement evidence is unrequired"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "A valid double-endorsement operation cannot  be applied: the associated delegate  has previously been denounced in this cycle."
                  CamlinternalFormatBasics.End_of_format)
                "A valid double-endorsement operation cannot  be applied: the associated delegate  has previously been denounced in this cycle.")))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Unrequired_double_endorsement_evidence" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Unrequired_double_endorsement_evidence" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "block.too_early_double_endorsement_evidence"
      "Too early double endorsement evidence"
      "A double-endorsement evidence is in the future"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            Alpha_context.Raw_level.t * Alpha_context.Raw_level.t) =>
            let '(level, current) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "A double-endorsement evidence is in the future  (current level: "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal
                      ", endorsement level: "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.Char_literal ")" % char
                          CamlinternalFormatBasics.End_of_format)))))
                "A double-endorsement evidence is in the future  (current level: %a, endorsement level: %a)")
              Alpha_context.Raw_level.pp current Alpha_context.Raw_level.pp
              level))
      (Data_encoding.obj2
        (Data_encoding.req None None "level" Alpha_context.Raw_level.encoding)
        (Data_encoding.req None None "current" Alpha_context.Raw_level.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Too_early_double_endorsement_evidence" then
            let '{|
              Too_early_double_endorsement_evidence.level := level;
                Too_early_double_endorsement_evidence.current := current
                |} := cast Too_early_double_endorsement_evidence payload in
            Some (level, current)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Raw_level.t * Alpha_context.Raw_level.t) =>
        let '(level, current) := function_parameter in
        Build_extensible "Too_early_double_endorsement_evidence"
          Too_early_double_endorsement_evidence
          {| Too_early_double_endorsement_evidence.level := level;
            Too_early_double_endorsement_evidence.current := current |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "block.outdated_double_endorsement_evidence"
      "Outdated double endorsement evidence"
      "A double-endorsement evidence is outdated."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            Alpha_context.Raw_level.t * Alpha_context.Raw_level.t) =>
            let '(level, last) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "A double-endorsement evidence is outdated  (last acceptable level: "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal
                      ", endorsement level: "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.Char_literal ")" % char
                          CamlinternalFormatBasics.End_of_format)))))
                "A double-endorsement evidence is outdated  (last acceptable level: %a, endorsement level: %a)")
              Alpha_context.Raw_level.pp last Alpha_context.Raw_level.pp level))
      (Data_encoding.obj2
        (Data_encoding.req None None "level" Alpha_context.Raw_level.encoding)
        (Data_encoding.req None None "last" Alpha_context.Raw_level.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Outdated_double_endorsement_evidence" then
            let '{|
              Outdated_double_endorsement_evidence.level := level;
                Outdated_double_endorsement_evidence.last := last
                |} := cast Outdated_double_endorsement_evidence payload in
            Some (level, last)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Raw_level.t * Alpha_context.Raw_level.t) =>
        let '(level, last) := function_parameter in
        Build_extensible "Outdated_double_endorsement_evidence"
          Outdated_double_endorsement_evidence
          {| Outdated_double_endorsement_evidence.level := level;
            Outdated_double_endorsement_evidence.last := last |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "block.invalid_double_baking_evidence" "Invalid double baking evidence"
      "A double-baking evidence is inconsistent  (two distinct level)"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            Block_hash.t * Int32.t * Block_hash.t * Int32.t) =>
            let '(hash1, level1, hash2, level2) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Invalid double-baking evidence (hash: "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal " and "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal ", levels: "
                          (CamlinternalFormatBasics.Int32
                            CamlinternalFormatBasics.Int_d
                            CamlinternalFormatBasics.No_padding
                            CamlinternalFormatBasics.No_precision
                            (CamlinternalFormatBasics.String_literal " and "
                              (CamlinternalFormatBasics.Int32
                                CamlinternalFormatBasics.Int_d
                                CamlinternalFormatBasics.No_padding
                                CamlinternalFormatBasics.No_precision
                                (CamlinternalFormatBasics.Char_literal
                                  ")" % char
                                  CamlinternalFormatBasics.End_of_format)))))))))
                "Invalid double-baking evidence (hash: %a and %a, levels: %ld and %ld)")
              Block_hash.pp hash1 Block_hash.pp hash2 level1 level2))
      (Data_encoding.obj4
        (Data_encoding.req None None "hash1" Block_hash.encoding)
        (Data_encoding.req None None "level1" Data_encoding.int32_value)
        (Data_encoding.req None None "hash2" Block_hash.encoding)
        (Data_encoding.req None None "level2" Data_encoding.int32_value))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_double_baking_evidence" then
            let '{|
              Invalid_double_baking_evidence.hash1 := hash1;
                Invalid_double_baking_evidence.level1 := level1;
                Invalid_double_baking_evidence.hash2 := hash2;
                Invalid_double_baking_evidence.level2 := level2
                |} := cast Invalid_double_baking_evidence payload in
            Some (hash1, level1, hash2, level2)
          else None
        end)
      (fun (function_parameter :
        Block_hash.t * Int32.t * Block_hash.t * Int32.t) =>
        let '(hash1, level1, hash2, level2) := function_parameter in
        Build_extensible "Invalid_double_baking_evidence"
          Invalid_double_baking_evidence
          {| Invalid_double_baking_evidence.hash1 := hash1;
            Invalid_double_baking_evidence.level1 := level1;
            Invalid_double_baking_evidence.hash2 := hash2;
            Invalid_double_baking_evidence.level2 := level2 |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "block.inconsistent_double_baking_evidence"
      "Inconsistent double baking evidence"
      "A double-baking evidence is inconsistent  (two distinct delegates)"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            Signature.public_key_hash * Signature.public_key_hash) =>
            let '(delegate1, delegate2) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Inconsistent double-baking evidence  (distinct delegate: "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal " and "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.Char_literal ")" % char
                          CamlinternalFormatBasics.End_of_format)))))
                "Inconsistent double-baking evidence  (distinct delegate: %a and %a)")
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp_short)
              delegate1
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp_short)
              delegate2))
      (Data_encoding.obj2
        (Data_encoding.req None None "delegate1"
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
        (Data_encoding.req None None "delegate2"
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Inconsistent_double_baking_evidence" then
            let '{|
              Inconsistent_double_baking_evidence.delegate1 := delegate1;
                Inconsistent_double_baking_evidence.delegate2 := delegate2
                |} := cast Inconsistent_double_baking_evidence payload in
            Some (delegate1, delegate2)
          else None
        end)
      (fun (function_parameter :
        Signature.public_key_hash * Signature.public_key_hash) =>
        let '(delegate1, delegate2) := function_parameter in
        Build_extensible "Inconsistent_double_baking_evidence"
          Inconsistent_double_baking_evidence
          {| Inconsistent_double_baking_evidence.delegate1 := delegate1;
            Inconsistent_double_baking_evidence.delegate2 := delegate2 |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Branch
      "block.unrequired_double_baking_evidence"
      "Unrequired double baking evidence"
      "A double-baking evidence is unrequired"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "A valid double-baking operation cannot  be applied: the associated delegate  has previously been denounced in this cycle."
                  CamlinternalFormatBasics.End_of_format)
                "A valid double-baking operation cannot  be applied: the associated delegate  has previously been denounced in this cycle.")))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Unrequired_double_baking_evidence" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Unrequired_double_baking_evidence" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "block.too_early_double_baking_evidence"
      "Too early double baking evidence"
      "A double-baking evidence is in the future"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            Alpha_context.Raw_level.t * Alpha_context.Raw_level.t) =>
            let '(level, current) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "A double-baking evidence is in the future  (current level: "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal ", baking level: "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.Char_literal ")" % char
                          CamlinternalFormatBasics.End_of_format)))))
                "A double-baking evidence is in the future  (current level: %a, baking level: %a)")
              Alpha_context.Raw_level.pp current Alpha_context.Raw_level.pp
              level))
      (Data_encoding.obj2
        (Data_encoding.req None None "level" Alpha_context.Raw_level.encoding)
        (Data_encoding.req None None "current" Alpha_context.Raw_level.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Too_early_double_baking_evidence" then
            let '{|
              Too_early_double_baking_evidence.level := level;
                Too_early_double_baking_evidence.current := current
                |} := cast Too_early_double_baking_evidence payload in
            Some (level, current)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Raw_level.t * Alpha_context.Raw_level.t) =>
        let '(level, current) := function_parameter in
        Build_extensible "Too_early_double_baking_evidence"
          Too_early_double_baking_evidence
          {| Too_early_double_baking_evidence.level := level;
            Too_early_double_baking_evidence.current := current |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "block.outdated_double_baking_evidence" "Outdated double baking evidence"
      "A double-baking evidence is outdated."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            Alpha_context.Raw_level.t * Alpha_context.Raw_level.t) =>
            let '(level, last) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "A double-baking evidence is outdated  (last acceptable level: "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal ", baking level: "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.Char_literal ")" % char
                          CamlinternalFormatBasics.End_of_format)))))
                "A double-baking evidence is outdated  (last acceptable level: %a, baking level: %a)")
              Alpha_context.Raw_level.pp last Alpha_context.Raw_level.pp level))
      (Data_encoding.obj2
        (Data_encoding.req None None "level" Alpha_context.Raw_level.encoding)
        (Data_encoding.req None None "last" Alpha_context.Raw_level.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Outdated_double_baking_evidence" then
            let '{|
              Outdated_double_baking_evidence.level := level;
                Outdated_double_baking_evidence.last := last
                |} := cast Outdated_double_baking_evidence payload in
            Some (level, last)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Raw_level.t * Alpha_context.Raw_level.t) =>
        let '(level, last) := function_parameter in
        Build_extensible "Outdated_double_baking_evidence"
          Outdated_double_baking_evidence
          {| Outdated_double_baking_evidence.level := level;
            Outdated_double_baking_evidence.last := last |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "operation.invalid_activation" "Invalid activation"
      "The given key and secret do not correspond to any existing preallocated contract"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (pkh : Ed25519.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.t)) =>
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Invalid activation. The public key "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal
                      " does not match any commitment."
                      CamlinternalFormatBasics.End_of_format)))
                "Invalid activation. The public key %a does not match any commitment.")
              Ed25519.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp) pkh))
      (Data_encoding.obj1
        (Data_encoding.req None None "pkh"
          Ed25519.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_activation" then
            let '{| Invalid_activation.pkh := pkh |} :=
              cast Invalid_activation payload in
            Some pkh
          else None
        end)
      (fun (pkh : Ed25519.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.t)) =>
        Build_extensible "Invalid_activation" Invalid_activation
          {| Invalid_activation.pkh := pkh |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "block.multiple_revelation"
      "Multiple revelations were included in a manager operation"
      "A manager operation should not contain more than one revelation"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Multiple revelations were included in a manager operation"
                  CamlinternalFormatBasics.End_of_format)
                "Multiple revelations were included in a manager operation")))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Multiple_revelation" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Multiple_revelation" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "gas_exhausted.init_deserialize"
      "Not enough gas for initial deserialization of script expressions"
      "Gas limit was not high enough to deserialize the transaction parameters or origination script code or initial storage, making the operation impossible to parse within the provided gas bounds."
      None Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Gas_quota_exceeded_init_deserialize" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Gas_quota_exceeded_init_deserialize" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "operation.inconsistent_sources" "Inconsistent sources in operation pack"
      "The operation pack includes operations from different sources."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.pp_print_string ppf
              "The operation pack includes operations from different sources."))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Inconsistent_sources" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Inconsistent_sources" unit tt) in
  Error_monad.register_error_kind Error_monad.Permanent
    "operation.not_enough_endorsements_for_priority"
    "Not enough endorsements for priority"
    "The block being validated does not include the required minimum number of endorsements for this priority."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (function_parameter : int * int * int * Time.t) =>
          let '(required, endorsements, priority, timestamp) :=
            function_parameter in
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal
                "Wrong number of endorsements ("
                (CamlinternalFormatBasics.Int CamlinternalFormatBasics.Int_i
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.No_precision
                  (CamlinternalFormatBasics.String_literal ") for priority ("
                    (CamlinternalFormatBasics.Int CamlinternalFormatBasics.Int_i
                      CamlinternalFormatBasics.No_padding
                      CamlinternalFormatBasics.No_precision
                      (CamlinternalFormatBasics.String_literal "), "
                        (CamlinternalFormatBasics.Int
                          CamlinternalFormatBasics.Int_i
                          CamlinternalFormatBasics.No_padding
                          CamlinternalFormatBasics.No_precision
                          (CamlinternalFormatBasics.String_literal
                            " are expected at "
                            (CamlinternalFormatBasics.Alpha
                              CamlinternalFormatBasics.End_of_format))))))))
              "Wrong number of endorsements (%i) for priority (%i), %i are expected at %a")
            endorsements priority required Time.pp_hum timestamp))
    (Data_encoding.obj4
      (Data_encoding.req None None "required" Data_encoding.int31)
      (Data_encoding.req None None "endorsements" Data_encoding.int31)
      (Data_encoding.req None None "priority" Data_encoding.int31)
      (Data_encoding.req None None "timestamp" Time.encoding))
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Not_enough_endorsements_for_priority" then
          let '{|
            Not_enough_endorsements_for_priority.required := required;
              Not_enough_endorsements_for_priority.priority := priority;
              Not_enough_endorsements_for_priority.endorsements := endorsements;
              Not_enough_endorsements_for_priority.timestamp := timestamp
              |} := cast Not_enough_endorsements_for_priority payload in
          Some (required, endorsements, priority, timestamp)
        else None
      end)
    (fun (function_parameter : int * int * int * Time.t) =>
      let '(required, endorsements, priority, timestamp) := function_parameter
        in
      Build_extensible "Not_enough_endorsements_for_priority"
        Not_enough_endorsements_for_priority
        {| Not_enough_endorsements_for_priority.required := required;
          Not_enough_endorsements_for_priority.priority := priority;
          Not_enough_endorsements_for_priority.endorsements := endorsements;
          Not_enough_endorsements_for_priority.timestamp := timestamp |}).

Definition apply_manager_operation_content
  (ctxt : Alpha_context.t) (mode : Script_ir_translator.unparsing_mode)
  (payer : Alpha_context.Contract.t) (source : Alpha_context.Contract.t)
  (chain_id : Chain_id.t) (internal : bool)
  (operation : Alpha_context.manager_operation)
  : M=?
    (Alpha_context.context * Apply_results.successful_manager_operation_result *
      list Alpha_context.packed_internal_operation) :=
  let before_operation := ctxt in
  let=? '_ := Alpha_context.Contract.must_exist ctxt source in
  let=? ctxt :=
    return=
      (Alpha_context.Gas.consume ctxt Michelson_v1_gas.Cost_of.manager_operation)
    in
  match operation with
  | Alpha_context.Reveal _ =>
    Error_monad._return
      (ctxt,
        (Apply_results.Reveal_result
          {|
            Apply_results.successful_manager_operation_result.Reveal_result.consumed_gas
              := Alpha_context.Gas.consumed before_operation ctxt |}), nil)
  |
    Alpha_context.Transaction {|
      Alpha_context.manager_operation.Transaction.amount := amount;
        Alpha_context.manager_operation.Transaction.parameters := parameters;
        Alpha_context.manager_operation.Transaction.entrypoint := entrypoint;
        Alpha_context.manager_operation.Transaction.destination := destination
        |} =>
    let=? ctxt := Alpha_context.Contract.spend ctxt source amount in
    let=? '(ctxt, maybe_burn_balance_update, allocated_destination_contract) :=
      match Alpha_context.Contract.is_implicit destination with
      | None => Error_monad._return (ctxt, nil, false)
      | Some _ =>
        let=? function_parameter :=
          Alpha_context.Contract.allocated ctxt destination in
        match function_parameter with
        | true => Error_monad._return (ctxt, nil, false)
        | false =>
          Lwt._return
            (let? '(ctxt, origination_burn) :=
              Alpha_context.Fees.origination_burn ctxt in
            return?
              (ctxt,
                [
                  ((Alpha_context.Receipt.Contract payer),
                    (Alpha_context.Receipt.Debited
                      origination_burn),
                    Alpha_context.Receipt.Block_application)
                ], true))
        end
      end in
    let=? ctxt := Alpha_context.Contract.credit ctxt destination amount in
    let=? '(ctxt, script) := Alpha_context.Contract.get_script ctxt destination
      in
    match script with
    | None =>
      Lwt._return
        (let? ctxt :=
          let? '_ :=
            match entrypoint with
            | "default" => Error_monad.ok_unit
            | entrypoint =>
              Error_monad.error_value
                (Build_extensible "No_such_entrypoint" string entrypoint)
            end in
          let? '(arg, ctxt) :=
            Alpha_context.Script.force_decode_in_context ctxt parameters in
          let cost_arg := Alpha_context.Script.deserialized_cost arg in
          let? ctxt := Alpha_context.Gas.consume ctxt cost_arg in
          match Micheline.root arg with
          | Micheline.Prim _ Michelson_v1_primitives.D_Unit [] _ => return? ctxt
          | _ =>
            Error_monad.error_value
              (Build_extensible "Bad_contract_parameter"
                Alpha_context.Contract.contract destination)
          end in
        let result_value :=
          Apply_results.Transaction_result
            {|
              Apply_results.successful_manager_operation_result.Transaction_result.storage
                := None;
              Apply_results.successful_manager_operation_result.Transaction_result.lazy_storage_diff
                := None;
              Apply_results.successful_manager_operation_result.Transaction_result.balance_updates
                :=
                Pervasives.op_at
                  (Alpha_context.Receipt.cleanup_balance_updates
                    [
                      ((Alpha_context.Receipt.Contract source),
                        (Alpha_context.Receipt.Debited amount),
                        Alpha_context.Receipt.Block_application);
                      ((Alpha_context.Receipt.Contract destination),
                        (Alpha_context.Receipt.Credited amount),
                        Alpha_context.Receipt.Block_application)
                    ]) maybe_burn_balance_update;
              Apply_results.successful_manager_operation_result.Transaction_result.originated_contracts
                := nil;
              Apply_results.successful_manager_operation_result.Transaction_result.consumed_gas
                := Alpha_context.Gas.consumed before_operation ctxt;
              Apply_results.successful_manager_operation_result.Transaction_result.storage_size
                := Z.zero;
              Apply_results.successful_manager_operation_result.Transaction_result.paid_storage_size_diff
                := Z.zero;
              Apply_results.successful_manager_operation_result.Transaction_result.allocated_destination_contract
                := allocated_destination_contract |} in
        return? (ctxt, result_value, nil))
    | Some script =>
      let=? '(parameter, ctxt) :=
        return= (Alpha_context.Script.force_decode_in_context ctxt parameters)
        in
      let cost_parameter := Alpha_context.Script.deserialized_cost parameter in
      let=? ctxt := return= (Alpha_context.Gas.consume ctxt cost_parameter) in
      let step_constants :=
        {| Script_interpreter.step_constants.source := source;
          Script_interpreter.step_constants.payer := payer;
          Script_interpreter.step_constants.self := destination;
          Script_interpreter.step_constants.amount := amount;
          Script_interpreter.step_constants.chain_id := chain_id |} in
      let=? '{|
        Script_interpreter.execution_result.ctxt := ctxt;
          Script_interpreter.execution_result.storage := storage_value;
          Script_interpreter.execution_result.lazy_storage_diff :=
            lazy_storage_diff;
          Script_interpreter.execution_result.operations := operations
          |} :=
        Script_interpreter.execute None ctxt mode step_constants script
          entrypoint parameter internal in
      let=? ctxt :=
        Alpha_context.Contract.update_script_storage ctxt destination
          storage_value lazy_storage_diff in
      let=? '(ctxt, new_size, paid_storage_size_diff, fees) :=
        Alpha_context.Fees.record_paid_storage_space ctxt destination in
      let=? originated_contracts :=
        Alpha_context.Contract.originated_from_current_nonce before_operation
          ctxt in
      let result_value :=
        Apply_results.Transaction_result
          {|
            Apply_results.successful_manager_operation_result.Transaction_result.storage
              := Some storage_value;
            Apply_results.successful_manager_operation_result.Transaction_result.lazy_storage_diff
              := lazy_storage_diff;
            Apply_results.successful_manager_operation_result.Transaction_result.balance_updates
              :=
              Alpha_context.Receipt.cleanup_balance_updates
                [
                  ((Alpha_context.Receipt.Contract payer),
                    (Alpha_context.Receipt.Debited fees),
                    Alpha_context.Receipt.Block_application);
                  ((Alpha_context.Receipt.Contract source),
                    (Alpha_context.Receipt.Debited amount),
                    Alpha_context.Receipt.Block_application);
                  ((Alpha_context.Receipt.Contract destination),
                    (Alpha_context.Receipt.Credited amount),
                    Alpha_context.Receipt.Block_application)
                ];
            Apply_results.successful_manager_operation_result.Transaction_result.originated_contracts
              := originated_contracts;
            Apply_results.successful_manager_operation_result.Transaction_result.consumed_gas
              := Alpha_context.Gas.consumed before_operation ctxt;
            Apply_results.successful_manager_operation_result.Transaction_result.storage_size
              := new_size;
            Apply_results.successful_manager_operation_result.Transaction_result.paid_storage_size_diff
              := paid_storage_size_diff;
            Apply_results.successful_manager_operation_result.Transaction_result.allocated_destination_contract
              := allocated_destination_contract |} in
      return=? (ctxt, result_value, operations)
    end
  |
    Alpha_context.Origination {|
      Alpha_context.manager_operation.Origination.delegate := delegate;
        Alpha_context.manager_operation.Origination.script := script;
        Alpha_context.manager_operation.Origination.credit := credit;
        Alpha_context.manager_operation.Origination.preorigination :=
          preorigination
        |} =>
    let=? '(unparsed_storage, ctxt) :=
      return=
        (Alpha_context.Script.force_decode_in_context ctxt
          script.(Alpha_context.Script.t.storage)) in
    let=? ctxt :=
      return=
        (Alpha_context.Gas.consume ctxt
          (Alpha_context.Script.deserialized_cost unparsed_storage)) in
    let=? '(unparsed_code, ctxt) :=
      return=
        (Alpha_context.Script.force_decode_in_context ctxt
          script.(Alpha_context.Script.t.code)) in
    let=? ctxt :=
      return=
        (Alpha_context.Gas.consume ctxt
          (Alpha_context.Script.deserialized_cost unparsed_code)) in
    let=? '(Script_ir_translator.Ex_script parsed_script, ctxt) :=
      Script_ir_translator.parse_script None ctxt false internal script in
    let 'existT _ __Ex_script_'b [ctxt, parsed_script] :=
      existT (A := Set)
        (fun __Ex_script_'b =>
          [Alpha_context.context ** Script_typed_ir.script __Ex_script_'b]) _
        [ctxt, parsed_script] in
    let=? '(to_duplicate, ctxt) :=
      return=
        (Script_ir_translator.collect_lazy_storage ctxt
          parsed_script.(Script_typed_ir.script.storage_type)
          parsed_script.(Script_typed_ir.script.storage)) in
    let to_update := Script_ir_translator.no_lazy_storage_id in
    let=? '(storage_value, lazy_storage_diff, ctxt) :=
      Script_ir_translator.extract_lazy_storage_diff ctxt
        Script_ir_translator.Optimized false to_duplicate to_update
        parsed_script.(Script_typed_ir.script.storage_type)
        parsed_script.(Script_typed_ir.script.storage) in
    let=? '(storage_value, ctxt) :=
      Script_ir_translator.unparse_data ctxt Script_ir_translator.Optimized
        parsed_script.(Script_typed_ir.script.storage_type) storage_value in
    let storage_value :=
      Alpha_context.Script.lazy_expr_value
        (Micheline.strip_locations storage_value) in
    let script := Alpha_context.Script.t.with_storage storage_value script in
    let=? ctxt := Alpha_context.Contract.spend ctxt source credit in
    let=? '(ctxt, contract) :=
      match preorigination with
      | Some contract =>
        let '_ :=
          (* ❌ Assert instruction is not handled. *)
          assert unit internal in
        return=? (ctxt, contract)
      | None =>
        return= (Alpha_context.Contract.fresh_contract_from_current_nonce ctxt)
      end in
    let=? ctxt :=
      Alpha_context.Contract.originate ctxt contract credit
        (script, lazy_storage_diff) delegate in
    let=? '(ctxt, origination_burn) :=
      return= (Alpha_context.Fees.origination_burn ctxt) in
    let=? '(ctxt, size, paid_storage_size_diff, fees) :=
      Alpha_context.Fees.record_paid_storage_space ctxt contract in
    let result_value :=
      Apply_results.Origination_result
        {|
          Apply_results.successful_manager_operation_result.Origination_result.lazy_storage_diff
            := lazy_storage_diff;
          Apply_results.successful_manager_operation_result.Origination_result.balance_updates
            :=
            Alpha_context.Receipt.cleanup_balance_updates
              [
                ((Alpha_context.Receipt.Contract payer),
                  (Alpha_context.Receipt.Debited fees),
                  Alpha_context.Receipt.Block_application);
                ((Alpha_context.Receipt.Contract payer),
                  (Alpha_context.Receipt.Debited origination_burn),
                  Alpha_context.Receipt.Block_application);
                ((Alpha_context.Receipt.Contract source),
                  (Alpha_context.Receipt.Debited credit),
                  Alpha_context.Receipt.Block_application);
                ((Alpha_context.Receipt.Contract contract),
                  (Alpha_context.Receipt.Credited credit),
                  Alpha_context.Receipt.Block_application)
              ];
          Apply_results.successful_manager_operation_result.Origination_result.originated_contracts
            := [ contract ];
          Apply_results.successful_manager_operation_result.Origination_result.consumed_gas
            := Alpha_context.Gas.consumed before_operation ctxt;
          Apply_results.successful_manager_operation_result.Origination_result.storage_size
            := size;
          Apply_results.successful_manager_operation_result.Origination_result.paid_storage_size_diff
            := paid_storage_size_diff |} in
    return=? (ctxt, result_value, nil)
  | Alpha_context.Delegation delegate =>
    let=? ctxt := Alpha_context.Delegate.set ctxt source delegate in
    return=?
      (ctxt,
        (Apply_results.Delegation_result
          {|
            Apply_results.successful_manager_operation_result.Delegation_result.consumed_gas
              := Alpha_context.Gas.consumed before_operation ctxt |}), nil)
  end.

Inductive success_or_failure : Set :=
| Success : Alpha_context.context -> success_or_failure
| Failure : success_or_failure.

Definition apply_internal_manager_operations
  (ctxt : Alpha_context.context) (mode : Script_ir_translator.unparsing_mode)
  (payer : Alpha_context.Contract.t) (chain_id : Chain_id.t)
  (ops : list Alpha_context.packed_internal_operation)
  : M=
    (success_or_failure * list Apply_results.packed_internal_operation_result) :=
  let fix apply
    (ctxt : Alpha_context.context)
    (applied : list Apply_results.packed_internal_operation_result)
    (worklist : list Alpha_context.packed_internal_operation) {struct ctxt}
    : M=
      (success_or_failure * list Apply_results.packed_internal_operation_result) :=
    match worklist with
    | [] => Lwt._return ((Success ctxt), (List.rev applied))
    |
      cons
        (Alpha_context.Internal_operation
          ({|
            Alpha_context.internal_operation.source := source;
              Alpha_context.internal_operation.operation := operation;
              Alpha_context.internal_operation.nonce := nonce_value
              |} as op)) rest =>
      let= function_parameter :=
        if Alpha_context.internal_nonce_already_recorded ctxt nonce_value then
          Error_monad.fail
            (Build_extensible "Internal_operation_replay"
              Alpha_context.packed_internal_operation
              (Alpha_context.Internal_operation op))
        else
          let ctxt := Alpha_context.record_internal_nonce ctxt nonce_value in
          apply_manager_operation_content ctxt mode payer source chain_id true
            operation in
      match function_parameter with
      | Pervasives.Error errors =>
        let result_value :=
          Apply_results.Internal_operation_result op
            (Apply_results.Failed
              (Alpha_context.manager_kind
                op.(Alpha_context.internal_operation.operation)) errors) in
        let skipped :=
          List.rev_map
            (fun (function_parameter : Alpha_context.packed_internal_operation)
              =>
              let 'Alpha_context.Internal_operation op := function_parameter in
              Apply_results.Internal_operation_result op
                (Apply_results.Skipped
                  (Alpha_context.manager_kind
                    op.(Alpha_context.internal_operation.operation)))) rest in
        Lwt._return
          (Failure,
            (List.rev (Pervasives.op_at skipped (cons result_value applied))))
      | Pervasives.Ok (ctxt, result_value, emitted) =>
        apply ctxt
          (cons
            (Apply_results.Internal_operation_result op
              (Apply_results.Applied result_value)) applied)
          (Pervasives.op_at rest emitted)
      end
    end in
  apply ctxt nil ops.

Definition precheck_manager_contents
  (ctxt : Alpha_context.context) (op : Alpha_context.contents)
  : M=? Alpha_context.context :=
  match op with
  |
    Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.source := source;
        Alpha_context.contents.Manager_operation.fee := fee;
        Alpha_context.contents.Manager_operation.counter := counter;
        Alpha_context.contents.Manager_operation.operation := operation;
        Alpha_context.contents.Manager_operation.gas_limit := gas_limit;
        Alpha_context.contents.Manager_operation.storage_limit := storage_limit
        |} =>
    let=? '_ := return= (Alpha_context.Gas.check_limit ctxt gas_limit) in
    let ctxt := Alpha_context.Gas.set_limit ctxt gas_limit in
    let=? '_ :=
      return= (Alpha_context.Fees.check_storage_limit ctxt storage_limit) in
    let=? '_ :=
      Alpha_context.Contract.must_be_allocated ctxt
        (Alpha_context.Contract.implicit_contract source) in
    let=? '_ :=
      Alpha_context.Contract.check_counter_increment ctxt source counter in
    let=? ctxt :=
      match operation with
      | Alpha_context.Reveal pk =>
        Alpha_context.Contract.reveal_manager_key ctxt source pk
      |
        Alpha_context.Transaction {|
          Alpha_context.manager_operation.Transaction.parameters := parameters
            |} =>
        Lwt._return
          (Error_monad.record_trace
            (Build_extensible "Gas_quota_exceeded_init_deserialize" unit tt)
            (let? '_ :=
              Alpha_context.Gas.check_enough ctxt
                (Alpha_context.Script.minimal_deserialize_cost parameters) in
            let? '(_arg, ctxt) :=
              Alpha_context.Script.force_decode_in_context ctxt parameters in
            return? ctxt))
      |
        Alpha_context.Origination {|
          Alpha_context.manager_operation.Origination.script := script |} =>
        Lwt._return
          (Error_monad.record_trace
            (Build_extensible "Gas_quota_exceeded_init_deserialize" unit tt)
            (let? '_ :=
              Alpha_context.Gas.check_enough ctxt
                (Alpha_context.Gas.op_plusat
                  (Alpha_context.Script.minimal_deserialize_cost
                    script.(Alpha_context.Script.t.code))
                  (Alpha_context.Script.minimal_deserialize_cost
                    script.(Alpha_context.Script.t.storage))) in
            let? '(_code, ctxt) :=
              Alpha_context.Script.force_decode_in_context ctxt
                script.(Alpha_context.Script.t.code) in
            let? '(_storage, ctxt) :=
              Alpha_context.Script.force_decode_in_context ctxt
                script.(Alpha_context.Script.t.storage) in
            return? ctxt))
      | _ => Error_monad._return ctxt
      end in
    let=? ctxt := Alpha_context.Contract.increment_counter ctxt source in
    let=? ctxt :=
      Alpha_context.Contract.spend ctxt
        (Alpha_context.Contract.implicit_contract source) fee in
    Lwt._return (Alpha_context.add_fees ctxt fee)
  | _ => unreachable_gadt_branch
  end.

Definition apply_manager_contents
  (ctxt : Alpha_context.context) (mode : Script_ir_translator.unparsing_mode)
  (chain_id : Chain_id.t) (op : Alpha_context.contents)
  : M=
    (success_or_failure * Apply_results.manager_operation_result *
      list Apply_results.packed_internal_operation_result) :=
  match op with
  |
    Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.source := source;
        Alpha_context.contents.Manager_operation.operation := operation;
        Alpha_context.contents.Manager_operation.gas_limit := gas_limit;
        Alpha_context.contents.Manager_operation.storage_limit := storage_limit
        |} =>
    let ctxt := Alpha_context.Gas.set_limit ctxt gas_limit in
    let ctxt := Alpha_context.Fees.start_counting_storage_fees ctxt in
    let source := Alpha_context.Contract.implicit_contract source in
    let= function_parameter :=
      apply_manager_operation_content ctxt mode source source chain_id false
        operation in
    match function_parameter with
    | Pervasives.Ok (ctxt, operation_results, internal_operations) =>
      let= function_parameter :=
        apply_internal_manager_operations ctxt mode source chain_id
          internal_operations in
      match function_parameter with
      | (Success ctxt, internal_operations_results) =>
        let= function_parameter :=
          Alpha_context.Fees.burn_storage_fees ctxt storage_limit source in
        match function_parameter with
        | Pervasives.Ok ctxt =>
          return=
            ((Success ctxt), (Apply_results.Applied operation_results),
              internal_operations_results)
        | Pervasives.Error errors =>
          return=
            (Failure,
              (Apply_results.Backtracked operation_results (Some errors)),
              internal_operations_results)
        end
      | (Failure, internal_operations_results) =>
        Lwt._return
          (Failure, (Apply_results.Applied operation_results),
            internal_operations_results)
      end
    | Pervasives.Error errors =>
      Lwt._return
        (Failure,
          (Apply_results.Failed (Alpha_context.manager_kind operation) errors),
          nil)
    end
  | _ => unreachable_gadt_branch
  end.

Definition skipped_operation_result
  (operation : Alpha_context.manager_operation)
  : Apply_results.manager_operation_result :=
  match operation with
  | Alpha_context.Reveal _ =>
    Apply_results.Applied
      (Apply_results.Reveal_result
        {|
          Apply_results.successful_manager_operation_result.Reveal_result.consumed_gas
            := Alpha_context.Gas.Arith.zero |})
  | _ => Apply_results.Skipped (Alpha_context.manager_kind operation)
  end.

Fixpoint mark_skipped
  (baker : Signature.public_key_hash) (level : Alpha_context.Level.t)
  (function_parameter : Alpha_context.contents_list)
  : Apply_results.contents_result_list :=
  match function_parameter with
  |
    Alpha_context.Single
      (Alpha_context.Manager_operation {|
        Alpha_context.contents.Manager_operation.source := source;
          Alpha_context.contents.Manager_operation.fee := fee;
          Alpha_context.contents.Manager_operation.operation := operation
          |}) =>
    let source := Alpha_context.Contract.implicit_contract source in
    Apply_results.Single_result
      (Apply_results.Manager_operation_result
        {|
          Apply_results.contents_result.Manager_operation_result.balance_updates
            :=
            Alpha_context.Receipt.cleanup_balance_updates
              [
                ((Alpha_context.Receipt.Contract source),
                  (Alpha_context.Receipt.Debited fee),
                  Alpha_context.Receipt.Block_application);
                ((Alpha_context.Receipt.Fees baker
                  level.(Alpha_context.Level.t.cycle)),
                  (Alpha_context.Receipt.Credited fee),
                  Alpha_context.Receipt.Block_application)
              ];
          Apply_results.contents_result.Manager_operation_result.operation_result
            := skipped_operation_result operation;
          Apply_results.contents_result.Manager_operation_result.internal_operation_results
            := nil |})
  |
    Alpha_context.Cons
      (Alpha_context.Manager_operation {|
        Alpha_context.contents.Manager_operation.source := source;
          Alpha_context.contents.Manager_operation.fee := fee;
          Alpha_context.contents.Manager_operation.operation := operation
          |}) rest =>
    let source := Alpha_context.Contract.implicit_contract source in
    Apply_results.Cons_result
      (Apply_results.Manager_operation_result
        {|
          Apply_results.contents_result.Manager_operation_result.balance_updates
            :=
            Alpha_context.Receipt.cleanup_balance_updates
              [
                ((Alpha_context.Receipt.Contract source),
                  (Alpha_context.Receipt.Debited fee),
                  Alpha_context.Receipt.Block_application);
                ((Alpha_context.Receipt.Fees baker
                  level.(Alpha_context.Level.t.cycle)),
                  (Alpha_context.Receipt.Credited fee),
                  Alpha_context.Receipt.Block_application)
              ];
          Apply_results.contents_result.Manager_operation_result.operation_result
            := skipped_operation_result operation;
          Apply_results.contents_result.Manager_operation_result.internal_operation_results
            := nil |}) (mark_skipped baker level rest)
  | _ => unreachable_gadt_branch
  end.

Fixpoint precheck_manager_contents_list
  (ctxt : Alpha_context.t) (contents_list : Alpha_context.contents_list)
  : M=? Alpha_context.context :=
  match contents_list with
  | Alpha_context.Single ((Alpha_context.Manager_operation _) as op) =>
    precheck_manager_contents ctxt op
  | Alpha_context.Cons ((Alpha_context.Manager_operation _) as op) rest =>
    let=? ctxt := precheck_manager_contents ctxt op in
    precheck_manager_contents_list ctxt rest
  | _ => unreachable_gadt_branch
  end.

Definition check_manager_signature
  (ctxt : Alpha_context.context) (chain_id : Chain_id.t)
  (op : Alpha_context.contents_list) (raw_operation : Alpha_context.operation)
  : M=? unit :=
  let check_same_manager {A : Set}
    (function_parameter : Signature.public_key_hash * option A)
    : option (Signature.public_key_hash * option A) ->
    M? (Signature.public_key_hash * option A) :=
    let '(source, source_key) := function_parameter in
    fun (manager : option (Signature.public_key_hash * option A)) =>
      match manager with
      | None => return? (source, source_key)
      | Some (manager, manager_key) =>
        if
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.equal) source
            manager
        then
          return? (source, (Option.first_some manager_key source_key))
        else
          Error_monad.error_value
            (Build_extensible "Inconsistent_sources" unit tt)
      end in
  let fix find_source
    (contents_list : Alpha_context.contents_list)
    (manager : option (Signature.public_key_hash * option Signature.public_key))
    : M? (Signature.public_key_hash * option Signature.public_key) :=
    let source (function_parameter : Alpha_context.contents)
      : Signature.public_key_hash * option Signature.public_key :=
      match function_parameter with
      |
        Alpha_context.Manager_operation {|
          Alpha_context.contents.Manager_operation.source := source;
            Alpha_context.contents.Manager_operation.operation :=
              Alpha_context.Reveal key_value
            |} => (source, (Some key_value))
      |
        Alpha_context.Manager_operation {|
          Alpha_context.contents.Manager_operation.source := source |} =>
        (source, None)
      | _ => unreachable_gadt_branch
      end in
    match contents_list with
    | Alpha_context.Single op => check_same_manager (source op) manager
    | Alpha_context.Cons op rest =>
      let? manager := check_same_manager (source op) manager in
      find_source rest (Some manager)
    end in
  let=? '(source, source_key) := return= (find_source op None) in
  let=? public_key :=
    match source_key with
    | Some key_value => Error_monad._return key_value
    | None => Alpha_context.Contract.get_manager_key ctxt source
    end in
  Lwt._return
    (Alpha_context.Operation.check_signature public_key chain_id raw_operation).

Fixpoint apply_manager_contents_list_rec
  (ctxt : Alpha_context.t) (mode : Script_ir_translator.unparsing_mode)
  (baker : Alpha_context.public_key_hash) (chain_id : Chain_id.t)
  (contents_list : Alpha_context.contents_list)
  : M= (success_or_failure * Apply_results.contents_result_list) :=
  let level := Alpha_context.Level.current ctxt in
  match contents_list with
  |
    Alpha_context.Single
      ((Alpha_context.Manager_operation {|
        Alpha_context.contents.Manager_operation.source := source;
          Alpha_context.contents.Manager_operation.fee := fee
          |}) as op) =>
    let source := Alpha_context.Contract.implicit_contract source in
    let= '(ctxt_result, operation_result, internal_operation_results) :=
      apply_manager_contents ctxt mode chain_id op in
    let result_value :=
      Apply_results.Manager_operation_result
        {|
          Apply_results.contents_result.Manager_operation_result.balance_updates
            :=
            Alpha_context.Receipt.cleanup_balance_updates
              [
                ((Alpha_context.Receipt.Contract source),
                  (Alpha_context.Receipt.Debited fee),
                  Alpha_context.Receipt.Block_application);
                ((Alpha_context.Receipt.Fees baker
                  level.(Alpha_context.Level.t.cycle)),
                  (Alpha_context.Receipt.Credited fee),
                  Alpha_context.Receipt.Block_application)
              ];
          Apply_results.contents_result.Manager_operation_result.operation_result
            := operation_result;
          Apply_results.contents_result.Manager_operation_result.internal_operation_results
            := internal_operation_results |} in
    return= (ctxt_result, (Apply_results.Single_result result_value))
  |
    Alpha_context.Cons
      ((Alpha_context.Manager_operation {|
        Alpha_context.contents.Manager_operation.source := source;
          Alpha_context.contents.Manager_operation.fee := fee
          |}) as op) rest =>
    let source := Alpha_context.Contract.implicit_contract source in
    let= function_parameter := apply_manager_contents ctxt mode chain_id op in
    match function_parameter with
    | (Failure, operation_result, internal_operation_results) =>
      let result_value :=
        Apply_results.Manager_operation_result
          {|
            Apply_results.contents_result.Manager_operation_result.balance_updates
              :=
              Alpha_context.Receipt.cleanup_balance_updates
                [
                  ((Alpha_context.Receipt.Contract source),
                    (Alpha_context.Receipt.Debited fee),
                    Alpha_context.Receipt.Block_application);
                  ((Alpha_context.Receipt.Fees baker
                    level.(Alpha_context.Level.t.cycle)),
                    (Alpha_context.Receipt.Credited fee),
                    Alpha_context.Receipt.Block_application)
                ];
            Apply_results.contents_result.Manager_operation_result.operation_result
              := operation_result;
            Apply_results.contents_result.Manager_operation_result.internal_operation_results
              := internal_operation_results |} in
      Lwt._return
        (Failure,
          (Apply_results.Cons_result result_value
            (mark_skipped baker level rest)))
    | (Success ctxt, operation_result, internal_operation_results) =>
      let result_value :=
        Apply_results.Manager_operation_result
          {|
            Apply_results.contents_result.Manager_operation_result.balance_updates
              :=
              Alpha_context.Receipt.cleanup_balance_updates
                [
                  ((Alpha_context.Receipt.Contract source),
                    (Alpha_context.Receipt.Debited fee),
                    Alpha_context.Receipt.Block_application);
                  ((Alpha_context.Receipt.Fees baker
                    level.(Alpha_context.Level.t.cycle)),
                    (Alpha_context.Receipt.Credited fee),
                    Alpha_context.Receipt.Block_application)
                ];
            Apply_results.contents_result.Manager_operation_result.operation_result
              := operation_result;
            Apply_results.contents_result.Manager_operation_result.internal_operation_results
              := internal_operation_results |} in
      let= '(ctxt_result, results) :=
        apply_manager_contents_list_rec ctxt mode baker chain_id rest in
      return= (ctxt_result, (Apply_results.Cons_result result_value results))
    end
  | _ => unreachable_gadt_branch
  end.

Definition mark_manager_operation_result
  (function_parameter : Apply_results.manager_operation_result)
  : Apply_results.manager_operation_result :=
  match function_parameter with
  |
    (Apply_results.Failed _ _ | Apply_results.Skipped _ |
    Apply_results.Backtracked _ _) as result_value => result_value
  | (Apply_results.Applied (Apply_results.Reveal_result _)) as result_value =>
    result_value
  | Apply_results.Applied result_value =>
    Apply_results.Backtracked result_value None
  end.

Definition mark_internal_operation_results
  (function_parameter : Apply_results.packed_internal_operation_result)
  : Apply_results.packed_internal_operation_result :=
  let 'Apply_results.Internal_operation_result kind_value result_value :=
    function_parameter in
  Apply_results.Internal_operation_result kind_value
    (mark_manager_operation_result result_value).

Fixpoint mark_backtracked
  (function_parameter : Apply_results.contents_result_list)
  : Apply_results.contents_result_list :=
  match function_parameter with
  | Apply_results.Single_result (Apply_results.Manager_operation_result op) =>
    Apply_results.Single_result
      (Apply_results.Manager_operation_result
        {|
          Apply_results.contents_result.Manager_operation_result.balance_updates
            :=
            op.(Apply_results.contents_result.Manager_operation_result.balance_updates);
          Apply_results.contents_result.Manager_operation_result.operation_result
            :=
            mark_manager_operation_result
              op.(Apply_results.contents_result.Manager_operation_result.operation_result);
          Apply_results.contents_result.Manager_operation_result.internal_operation_results
            :=
            List.map mark_internal_operation_results
              op.(Apply_results.contents_result.Manager_operation_result.internal_operation_results)
          |})
  | Apply_results.Cons_result (Apply_results.Manager_operation_result op) rest
    =>
    Apply_results.Cons_result
      (Apply_results.Manager_operation_result
        {|
          Apply_results.contents_result.Manager_operation_result.balance_updates
            :=
            op.(Apply_results.contents_result.Manager_operation_result.balance_updates);
          Apply_results.contents_result.Manager_operation_result.operation_result
            :=
            mark_manager_operation_result
              op.(Apply_results.contents_result.Manager_operation_result.operation_result);
          Apply_results.contents_result.Manager_operation_result.internal_operation_results
            :=
            List.map mark_internal_operation_results
              op.(Apply_results.contents_result.Manager_operation_result.internal_operation_results)
          |}) (mark_backtracked rest)
  | _ => unreachable_gadt_branch
  end.

Definition apply_manager_contents_list
  (ctxt : Alpha_context.t) (mode : Script_ir_translator.unparsing_mode)
  (baker : Alpha_context.public_key_hash) (chain_id : Chain_id.t)
  (contents_list : Alpha_context.contents_list)
  : M= (Alpha_context.t * Apply_results.contents_result_list) :=
  let= '(ctxt_result, results) :=
    apply_manager_contents_list_rec ctxt mode baker chain_id contents_list in
  match ctxt_result with
  | Failure => Lwt._return (ctxt, (mark_backtracked results))
  | Success ctxt =>
    let= ctxt := Alpha_context.Lazy_storage.cleanup_temporaries ctxt in
    return= (ctxt, results)
  end.

Definition apply_contents_list
  (ctxt : Alpha_context.context) (chain_id : Chain_id.t)
  (mode : Script_ir_translator.unparsing_mode) (pred_block : Block_hash.t)
  (baker : Signature.public_key_hash) (operation : Alpha_context.operation)
  (contents_list : Alpha_context.contents_list)
  : M=? (Alpha_context.context * Apply_results.contents_result_list) :=
  match contents_list with
  |
    Alpha_context.Single
      (Alpha_context.Endorsement {|
        Alpha_context.contents.Endorsement.level := level |}) =>
    let block :=
      operation.(Alpha_context.operation.shell).(Operation.shell_header.branch)
      in
    let=? '_ :=
      return=
        (Error_monad.error_unless (Block_hash.equal block pred_block)
          (Build_extensible "Wrong_endorsement_predecessor"
            (Block_hash.t * Block_hash.t) (pred_block, block))) in
    let current_level :=
      (Alpha_context.Level.current ctxt).(Alpha_context.Level.t.level) in
    let=? '_ :=
      return=
        (Error_monad.error_unless
          (Alpha_context.Raw_level.op_eq (Alpha_context.Raw_level.succ level)
            current_level)
          (Build_extensible "Invalid_endorsement_level" unit tt)) in
    let=? '(delegate, slots, used) :=
      Baking.check_endorsement_rights ctxt chain_id operation in
    if used then
      Error_monad.fail
        (Build_extensible "Duplicate_endorsement" Alpha_context.public_key_hash
          delegate)
    else
      let ctxt := Alpha_context.record_endorsement ctxt delegate in
      let gap := List.length slots in
      let=? deposit :=
        return=
          (Alpha_context.Tez.op_starquestion
            (Alpha_context.Constants.endorsement_security_deposit ctxt)
            (Int64.of_int gap)) in
      let=? ctxt := Alpha_context.Delegate.freeze_deposit ctxt delegate deposit
        in
      let=? block_priority := Alpha_context.Global.get_block_priority ctxt in
      let=? reward := return= (Baking.endorsing_reward ctxt block_priority gap)
        in
      let=? ctxt := Alpha_context.Delegate.freeze_rewards ctxt delegate reward
        in
      let level := Alpha_context.Level.from_raw ctxt None level in
      return=?
        (ctxt,
          (Apply_results.Single_result
            (Apply_results.Endorsement_result
              {|
                Apply_results.contents_result.Endorsement_result.balance_updates
                  :=
                  Alpha_context.Receipt.cleanup_balance_updates
                    [
                      ((Alpha_context.Receipt.Contract
                        (Alpha_context.Contract.implicit_contract
                          delegate)),
                        (Alpha_context.Receipt.Debited deposit),
                        Alpha_context.Receipt.Block_application);
                      ((Alpha_context.Receipt.Deposits delegate
                        level.(Alpha_context.Level.t.cycle)),
                        (Alpha_context.Receipt.Credited
                          deposit),
                        Alpha_context.Receipt.Block_application);
                      ((Alpha_context.Receipt.Rewards delegate
                        level.(Alpha_context.Level.t.cycle)),
                        (Alpha_context.Receipt.Credited reward),
                        Alpha_context.Receipt.Block_application)
                    ];
                Apply_results.contents_result.Endorsement_result.delegate :=
                  delegate;
                Apply_results.contents_result.Endorsement_result.slots := slots
                |})))
  |
    Alpha_context.Single
      (Alpha_context.Seed_nonce_revelation {|
        Alpha_context.contents.Seed_nonce_revelation.level := level;
          Alpha_context.contents.Seed_nonce_revelation.nonce := nonce_value
          |}) =>
    let level := Alpha_context.Level.from_raw ctxt None level in
    let=? ctxt := Alpha_context.Nonce.reveal ctxt level nonce_value in
    let seed_nonce_revelation_tip :=
      Alpha_context.Constants.seed_nonce_revelation_tip ctxt in
    Lwt._return
      (let? ctxt := Alpha_context.add_rewards ctxt seed_nonce_revelation_tip in
      return?
        (ctxt,
          (Apply_results.Single_result
            (Apply_results.Seed_nonce_revelation_result
              [
                ((Alpha_context.Receipt.Rewards baker
                  level.(Alpha_context.Level.t.cycle)),
                  (Alpha_context.Receipt.Credited
                    seed_nonce_revelation_tip),
                  Alpha_context.Receipt.Block_application)
              ]))))
  |
    Alpha_context.Single
      (Alpha_context.Double_endorsement_evidence {|
        Alpha_context.contents.Double_endorsement_evidence.op1 := op1;
          Alpha_context.contents.Double_endorsement_evidence.op2 := op2
          |}) =>
    match
      ((op1.(Alpha_context.operation.protocol_data).(Alpha_context.protocol_data.contents),
        op2.(Alpha_context.operation.protocol_data).(Alpha_context.protocol_data.contents)),
        match
          (op1.(Alpha_context.operation.protocol_data).(Alpha_context.protocol_data.contents),
            op2.(Alpha_context.operation.protocol_data).(Alpha_context.protocol_data.contents))
          with
        |
          (Alpha_context.Single (Alpha_context.Endorsement e1),
            Alpha_context.Single (Alpha_context.Endorsement e2)) =>
          (Alpha_context.Raw_level.op_eq
            e1.(Alpha_context.contents.Endorsement.level)
            e2.(Alpha_context.contents.Endorsement.level)) &&
          (Pervasives.not
            (Block_hash.equal
              op1.(Alpha_context.operation.shell).(Operation.shell_header.branch)
              op2.(Alpha_context.operation.shell).(Operation.shell_header.branch)))
        | _ => false
        end) with
    |
      ((Alpha_context.Single (Alpha_context.Endorsement e1),
        Alpha_context.Single (Alpha_context.Endorsement e2)), true) =>
      let level :=
        Alpha_context.Level.from_raw ctxt None
          e1.(Alpha_context.contents.Endorsement.level) in
      let oldest_level := Alpha_context.Level.last_allowed_fork_level ctxt in
      let=? '_ :=
        Error_monad.fail_unless
          (Alpha_context.Level.op_lt level (Alpha_context.Level.current ctxt))
          (Build_extensible "Too_early_double_endorsement_evidence"
            Too_early_double_endorsement_evidence
            {|
              Too_early_double_endorsement_evidence.level :=
                level.(Alpha_context.Level.t.level);
              Too_early_double_endorsement_evidence.current :=
                (Alpha_context.Level.current ctxt).(Alpha_context.Level.t.level)
              |}) in
      let=? '_ :=
        Error_monad.fail_unless
          (Alpha_context.Raw_level.op_lteq oldest_level
            level.(Alpha_context.Level.t.level))
          (Build_extensible "Outdated_double_endorsement_evidence"
            Outdated_double_endorsement_evidence
            {|
              Outdated_double_endorsement_evidence.level :=
                level.(Alpha_context.Level.t.level);
              Outdated_double_endorsement_evidence.last := oldest_level |}) in
      let=? '(delegate1, _, _) :=
        Baking.check_endorsement_rights ctxt chain_id op1 in
      let=? '(delegate2, _, _) :=
        Baking.check_endorsement_rights ctxt chain_id op2 in
      let=? '_ :=
        Error_monad.fail_unless
          (Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.equal)
            delegate1 delegate2)
          (Build_extensible "Inconsistent_double_endorsement_evidence"
            Inconsistent_double_endorsement_evidence
            {| Inconsistent_double_endorsement_evidence.delegate1 := delegate1;
              Inconsistent_double_endorsement_evidence.delegate2 := delegate2 |})
        in
      let=? valid :=
        Alpha_context.Delegate.has_frozen_balance ctxt delegate1
          level.(Alpha_context.Level.t.cycle) in
      let=? '_ :=
        Error_monad.fail_unless valid
          (Build_extensible "Unrequired_double_endorsement_evidence" unit tt) in
      let=? '(ctxt, balance) :=
        Alpha_context.Delegate.punish ctxt delegate1
          level.(Alpha_context.Level.t.cycle) in
      let=? burned :=
        Lwt._return
          (Alpha_context.Tez.op_plusquestion
            balance.(Alpha_context.Delegate.frozen_balance.deposit)
            balance.(Alpha_context.Delegate.frozen_balance.fees)) in
      let reward :=
        match Alpha_context.Tez.op_divquestion burned 2 with
        | Pervasives.Ok v => v
        | Pervasives.Error _ => Alpha_context.Tez.zero
        end in
      let=? ctxt := return= (Alpha_context.add_rewards ctxt reward) in
      let current_cycle :=
        (Alpha_context.Level.current ctxt).(Alpha_context.Level.t.cycle) in
      Error_monad._return
        (ctxt,
          (Apply_results.Single_result
            (Apply_results.Double_endorsement_evidence_result
              (Alpha_context.Receipt.cleanup_balance_updates
                [
                  ((Alpha_context.Receipt.Deposits delegate1
                    level.(Alpha_context.Level.t.cycle)),
                    (Alpha_context.Receipt.Debited
                      balance.(Alpha_context.Delegate.frozen_balance.deposit)),
                    Alpha_context.Receipt.Block_application);
                  ((Alpha_context.Receipt.Fees delegate1
                    level.(Alpha_context.Level.t.cycle)),
                    (Alpha_context.Receipt.Debited
                      balance.(Alpha_context.Delegate.frozen_balance.fees)),
                    Alpha_context.Receipt.Block_application);
                  ((Alpha_context.Receipt.Rewards delegate1
                    level.(Alpha_context.Level.t.cycle)),
                    (Alpha_context.Receipt.Debited
                      balance.(Alpha_context.Delegate.frozen_balance.rewards)),
                    Alpha_context.Receipt.Block_application);
                  ((Alpha_context.Receipt.Rewards baker current_cycle),
                    (Alpha_context.Receipt.Credited reward),
                    Alpha_context.Receipt.Block_application)
                ]))))
    | ((_, _), _) =>
      Error_monad.fail
        (Build_extensible "Invalid_double_endorsement_evidence" unit tt)
    end
  |
    Alpha_context.Single
      (Alpha_context.Double_baking_evidence {|
        Alpha_context.contents.Double_baking_evidence.bh1 := bh1;
          Alpha_context.contents.Double_baking_evidence.bh2 := bh2
          |}) =>
    let hash1 := Alpha_context.Block_header.hash_value bh1 in
    let hash2 := Alpha_context.Block_header.hash_value bh2 in
    let=? '_ :=
      Error_monad.fail_unless
        ((bh1.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.level)
        =i32
        bh2.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.level))
        && (Pervasives.not (Block_hash.equal hash1 hash2)))
        (Build_extensible "Invalid_double_baking_evidence"
          Invalid_double_baking_evidence
          {| Invalid_double_baking_evidence.hash1 := hash1;
            Invalid_double_baking_evidence.level1 :=
              bh1.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.level);
            Invalid_double_baking_evidence.hash2 := hash2;
            Invalid_double_baking_evidence.level2 :=
              bh2.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.level)
            |}) in
    let=? raw_level :=
      Lwt._return
        (Alpha_context.Raw_level.of_int32
          bh1.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.level))
      in
    let oldest_level := Alpha_context.Level.last_allowed_fork_level ctxt in
    let=? '_ :=
      Error_monad.fail_unless
        (Alpha_context.Raw_level.op_lt raw_level
          (Alpha_context.Level.current ctxt).(Alpha_context.Level.t.level))
        (Build_extensible "Too_early_double_baking_evidence"
          Too_early_double_baking_evidence
          {| Too_early_double_baking_evidence.level := raw_level;
            Too_early_double_baking_evidence.current :=
              (Alpha_context.Level.current ctxt).(Alpha_context.Level.t.level)
            |}) in
    let=? '_ :=
      Error_monad.fail_unless
        (Alpha_context.Raw_level.op_lteq oldest_level raw_level)
        (Build_extensible "Outdated_double_baking_evidence"
          Outdated_double_baking_evidence
          {| Outdated_double_baking_evidence.level := raw_level;
            Outdated_double_baking_evidence.last := oldest_level |}) in
    let level := Alpha_context.Level.from_raw ctxt None raw_level in
    let=? delegate1 :=
      Alpha_context.Roll.baking_rights_owner ctxt level
        bh1.(Alpha_context.Block_header.t.protocol_data).(Alpha_context.Block_header.protocol_data.contents).(Alpha_context.Block_header.contents.priority)
      in
    let=? '_ := Baking.check_signature bh1 chain_id delegate1 in
    let=? delegate2 :=
      Alpha_context.Roll.baking_rights_owner ctxt level
        bh2.(Alpha_context.Block_header.t.protocol_data).(Alpha_context.Block_header.protocol_data.contents).(Alpha_context.Block_header.contents.priority)
      in
    let=? '_ := Baking.check_signature bh2 chain_id delegate2 in
    let=? '_ :=
      Error_monad.fail_unless
        (Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.equal) delegate1 delegate2)
        (Build_extensible "Inconsistent_double_baking_evidence"
          Inconsistent_double_baking_evidence
          {|
            Inconsistent_double_baking_evidence.delegate1 :=
              Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.hash_value) delegate1;
            Inconsistent_double_baking_evidence.delegate2 :=
              Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.hash_value) delegate2
            |}) in
    let delegate :=
      Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.hash_value) delegate1 in
    let=? valid :=
      Alpha_context.Delegate.has_frozen_balance ctxt delegate
        level.(Alpha_context.Level.t.cycle) in
    let=? '_ :=
      Error_monad.fail_unless valid
        (Build_extensible "Unrequired_double_baking_evidence" unit tt) in
    let=? '(ctxt, balance) :=
      Alpha_context.Delegate.punish ctxt delegate
        level.(Alpha_context.Level.t.cycle) in
    let=? burned :=
      return=
        (Alpha_context.Tez.op_plusquestion
          balance.(Alpha_context.Delegate.frozen_balance.deposit)
          balance.(Alpha_context.Delegate.frozen_balance.fees)) in
    let reward :=
      match Alpha_context.Tez.op_divquestion burned 2 with
      | Pervasives.Ok v => v
      | Pervasives.Error _ => Alpha_context.Tez.zero
      end in
    Lwt._return
      (let? ctxt := Alpha_context.add_rewards ctxt reward in
      let current_cycle :=
        (Alpha_context.Level.current ctxt).(Alpha_context.Level.t.cycle) in
      return?
        (ctxt,
          (Apply_results.Single_result
            (Apply_results.Double_baking_evidence_result
              (Alpha_context.Receipt.cleanup_balance_updates
                [
                  ((Alpha_context.Receipt.Deposits delegate
                    level.(Alpha_context.Level.t.cycle)),
                    (Alpha_context.Receipt.Debited
                      balance.(Alpha_context.Delegate.frozen_balance.deposit)),
                    Alpha_context.Receipt.Block_application);
                  ((Alpha_context.Receipt.Fees delegate
                    level.(Alpha_context.Level.t.cycle)),
                    (Alpha_context.Receipt.Debited
                      balance.(Alpha_context.Delegate.frozen_balance.fees)),
                    Alpha_context.Receipt.Block_application);
                  ((Alpha_context.Receipt.Rewards delegate
                    level.(Alpha_context.Level.t.cycle)),
                    (Alpha_context.Receipt.Debited
                      balance.(Alpha_context.Delegate.frozen_balance.rewards)),
                    Alpha_context.Receipt.Block_application);
                  ((Alpha_context.Receipt.Rewards baker current_cycle),
                    (Alpha_context.Receipt.Credited reward),
                    Alpha_context.Receipt.Block_application)
                ])))))
  |
    Alpha_context.Single
      (Alpha_context.Activate_account {|
        Alpha_context.contents.Activate_account.id := pkh;
          Alpha_context.contents.Activate_account.activation_code :=
            activation_code
          |}) =>
    let blinded_pkh :=
      Blinded_public_key_hash.of_ed25519_pkh activation_code pkh in
    let=? function_parameter :=
      Alpha_context.Commitment.get_opt ctxt blinded_pkh in
    match function_parameter with
    | None =>
      Error_monad.fail
        (Build_extensible "Invalid_activation" Invalid_activation
          {| Invalid_activation.pkh := pkh |})
    | Some amount =>
      let=? ctxt := Alpha_context.Commitment.delete ctxt blinded_pkh in
      let contract :=
        Alpha_context.Contract.implicit_contract (Signature.Ed25519Hash pkh) in
      let=? ctxt := Alpha_context.Contract.credit ctxt contract amount in
      return=?
        (ctxt,
          (Apply_results.Single_result
            (Apply_results.Activate_account_result
              [
                ((Alpha_context.Receipt.Contract contract),
                  (Alpha_context.Receipt.Credited amount),
                  Alpha_context.Receipt.Block_application)
              ])))
    end
  |
    Alpha_context.Single
      (Alpha_context.Proposals {|
        Alpha_context.contents.Proposals.source := source;
          Alpha_context.contents.Proposals.period := period;
          Alpha_context.contents.Proposals.proposals := proposals
          |}) =>
    let=? delegate := Alpha_context.Roll.delegate_pubkey ctxt source in
    let=? '_ :=
      return=
        (Alpha_context.Operation.check_signature delegate chain_id operation) in
    let=? '{|
      Alpha_context.Voting_period.voting_period.index := current_period |} :=
      Alpha_context.Voting_period.get_current ctxt in
    let=? '_ :=
      return=
        (Error_monad.error_unless (current_period =i32 period)
          (Build_extensible "Wrong_voting_period" (int32 * int32)
            (current_period, period))) in
    let=? ctxt := Amendment.record_proposals ctxt source proposals in
    return=?
      (ctxt, (Apply_results.Single_result Apply_results.Proposals_result))
  |
    Alpha_context.Single
      (Alpha_context.Ballot {|
        Alpha_context.contents.Ballot.source := source;
          Alpha_context.contents.Ballot.period := period;
          Alpha_context.contents.Ballot.proposal := proposal;
          Alpha_context.contents.Ballot.ballot := ballot
          |}) =>
    let=? delegate := Alpha_context.Roll.delegate_pubkey ctxt source in
    let=? '_ :=
      return=
        (Alpha_context.Operation.check_signature delegate chain_id operation) in
    let=? '{|
      Alpha_context.Voting_period.voting_period.index := current_period |} :=
      Alpha_context.Voting_period.get_current ctxt in
    let=? '_ :=
      return=
        (Error_monad.error_unless (current_period =i32 period)
          (Build_extensible "Wrong_voting_period" (int32 * int32)
            (current_period, period))) in
    let=? ctxt := Amendment.record_ballot ctxt source proposal ballot in
    return=? (ctxt, (Apply_results.Single_result Apply_results.Ballot_result))
  | (Alpha_context.Single (Alpha_context.Manager_operation _)) as op =>
    let=? ctxt := precheck_manager_contents_list ctxt op in
    let=? '_ := check_manager_signature ctxt chain_id op operation in
    Error_monad.op_gtpipeeq
      (apply_manager_contents_list ctxt mode baker chain_id op) Error_monad.ok
  | (Alpha_context.Cons (Alpha_context.Manager_operation _) _) as op =>
    let=? ctxt := precheck_manager_contents_list ctxt op in
    let=? '_ := check_manager_signature ctxt chain_id op operation in
    Error_monad.op_gtpipeeq
      (apply_manager_contents_list ctxt mode baker chain_id op) Error_monad.ok
  | _ => unreachable_gadt_branch
  end.

Definition apply_operation
  (ctxt : Alpha_context.context) (chain_id : Chain_id.t)
  (mode : Script_ir_translator.unparsing_mode) (pred_block : Block_hash.t)
  (baker : Signature.public_key_hash) (hash_value : Operation_hash.t)
  (operation : Alpha_context.operation)
  : M=? (Alpha_context.context * Apply_results.operation_metadata) :=
  let ctxt := Alpha_context.Contract.init_origination_nonce ctxt hash_value in
  let=? '(ctxt, result_value) :=
    apply_contents_list ctxt chain_id mode pred_block baker operation
      operation.(Alpha_context.operation.protocol_data).(Alpha_context.protocol_data.contents)
    in
  let ctxt := Alpha_context.Gas.set_unlimited ctxt in
  let ctxt := Alpha_context.Contract.unset_origination_nonce ctxt in
  return=?
    (ctxt, {| Apply_results.operation_metadata.contents := result_value |}).

Definition may_snapshot_roll (ctxt : Alpha_context.context)
  : M=? Alpha_context.context :=
  let level := Alpha_context.Level.current ctxt in
  let blocks_per_roll_snapshot :=
    Alpha_context.Constants.blocks_per_roll_snapshot ctxt in
  if
    Compare.Int32.(Compare.S.equal)
      (Int32.rem level.(Alpha_context.Level.t.cycle_position)
        blocks_per_roll_snapshot) (Int32.pred blocks_per_roll_snapshot)
  then
    Alpha_context.Roll.snapshot_rolls ctxt
  else
    Error_monad._return ctxt.

Definition may_start_new_cycle (ctxt : Alpha_context.context)
  : M=?
    (Alpha_context.context * Alpha_context.Receipt.balance_updates *
      list Signature.public_key_hash) :=
  match Baking.dawn_of_a_new_cycle ctxt with
  | None => Error_monad._return (ctxt, nil, nil)
  | Some last_cycle =>
    let=? '(ctxt, unrevealed) := Alpha_context.Seed.cycle_end ctxt last_cycle in
    let=? ctxt := Alpha_context.Roll.cycle_end ctxt last_cycle in
    let=? '(ctxt, update_balances, deactivated) :=
      Alpha_context.Delegate.cycle_end ctxt last_cycle unrevealed in
    let=? ctxt := Alpha_context.Bootstrap.cycle_end ctxt last_cycle in
    return=? (ctxt, update_balances, deactivated)
  end.

Definition endorsement_rights_of_pred_level (ctxt : Alpha_context.context)
  : M=?
    (Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
      (Alpha_context.public_key * list int * bool)) :=
  match Alpha_context.Level.pred ctxt (Alpha_context.Level.current ctxt) with
  | None =>
    (* ❌ Assert instruction is not handled. *)
    assert
      (M=?
        (Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
          (Alpha_context.public_key * list int * bool))) false
  | Some pred_level => Baking.endorsement_rights ctxt pred_level
  end.

Definition begin_full_construction
  (ctxt : Alpha_context.context) (pred_timestamp : Time.t)
  (protocol_data : Alpha_context.Block_header.contents)
  : M=?
    (Alpha_context.context * Alpha_context.Block_header.contents *
      Alpha_context.public_key * Alpha_context.Period.t) :=
  let=? ctxt :=
    Alpha_context.Global.set_block_priority ctxt
      protocol_data.(Alpha_context.Block_header.contents.priority) in
  let=? '(delegate_pk, block_delay) :=
    Baking.check_baking_rights ctxt protocol_data pred_timestamp in
  let ctxt := Alpha_context.Fitness.increase ctxt in
  let=? rights := endorsement_rights_of_pred_level ctxt in
  let ctxt := Alpha_context.init_endorsements ctxt rights in
  return=? (ctxt, protocol_data, delegate_pk, block_delay).

Definition begin_partial_construction (ctxt : Alpha_context.context)
  : M=? Alpha_context.context :=
  let ctxt := Alpha_context.Fitness.increase ctxt in
  let=? rights := endorsement_rights_of_pred_level ctxt in
  return=? (Alpha_context.init_endorsements ctxt rights).

Definition begin_application
  (ctxt : Alpha_context.context) (chain_id : Chain_id.t)
  (block_header : Alpha_context.Block_header.t) (pred_timestamp : Time.t)
  : M=?
    (Alpha_context.context * Alpha_context.public_key * Alpha_context.Period.t) :=
  let=? ctxt :=
    Alpha_context.Global.set_block_priority ctxt
      block_header.(Alpha_context.Block_header.t.protocol_data).(Alpha_context.Block_header.protocol_data.contents).(Alpha_context.Block_header.contents.priority)
    in
  let current_level := Alpha_context.Level.current ctxt in
  let=? '_ := return= (Baking.check_proof_of_work_stamp ctxt block_header) in
  let=? '_ := return= (Baking.check_fitness_gap ctxt block_header) in
  let=? '(delegate_pk, block_delay) :=
    Baking.check_baking_rights ctxt
      block_header.(Alpha_context.Block_header.t.protocol_data).(Alpha_context.Block_header.protocol_data.contents)
      pred_timestamp in
  let=? '_ := Baking.check_signature block_header chain_id delegate_pk in
  let has_commitment :=
    Option.is_some
      block_header.(Alpha_context.Block_header.t.protocol_data).(Alpha_context.Block_header.protocol_data.contents).(Alpha_context.Block_header.contents.seed_nonce_hash)
    in
  let=? '_ :=
    return=
      (Error_monad.error_unless
        (Compare.Bool.(Compare.S.op_eq) has_commitment
          current_level.(Alpha_context.Level.t.expected_commitment))
        (Build_extensible "Invalid_commitment" Invalid_commitment
          {|
            Invalid_commitment.expected :=
              current_level.(Alpha_context.Level.t.expected_commitment) |})) in
  let ctxt := Alpha_context.Fitness.increase ctxt in
  let=? rights := endorsement_rights_of_pred_level ctxt in
  let ctxt := Alpha_context.init_endorsements ctxt rights in
  return=? (ctxt, delegate_pk, block_delay).

Definition check_minimum_endorsements
  (ctxt : Alpha_context.context)
  (protocol_data : Alpha_context.Block_header.contents)
  (block_delay : Alpha_context.Period.t) (included_endorsements : int)
  : M? unit :=
  let minimum := Baking.minimum_allowed_endorsements ctxt block_delay in
  let timestamp := Alpha_context.Timestamp.current ctxt in
  Error_monad.error_unless (included_endorsements >=i minimum)
    (Build_extensible "Not_enough_endorsements_for_priority"
      Not_enough_endorsements_for_priority
      {| Not_enough_endorsements_for_priority.required := minimum;
        Not_enough_endorsements_for_priority.priority :=
          protocol_data.(Alpha_context.Block_header.contents.priority);
        Not_enough_endorsements_for_priority.endorsements :=
          included_endorsements;
        Not_enough_endorsements_for_priority.timestamp := timestamp |}).

Definition finalize_application
  (ctxt : Alpha_context.context)
  (protocol_data : Alpha_context.Block_header.contents)
  (delegate : Signature.public_key_hash) (block_delay : Alpha_context.Period.t)
  (migration_balance_updates :
    list
      (Alpha_context.Receipt.balance * Alpha_context.Receipt.balance_update *
        Alpha_context.Receipt.update_origin))
  : M=? (Alpha_context.context * Apply_results.block_metadata) :=
  let included_endorsements := Alpha_context.included_endorsements ctxt in
  let=? '_ :=
    return=
      (check_minimum_endorsements ctxt protocol_data block_delay
        included_endorsements) in
  let deposit := Alpha_context.Constants.block_security_deposit ctxt in
  let=? ctxt := return= (Alpha_context.add_deposit ctxt delegate deposit) in
  let=? reward :=
    return=
      (Baking.baking_reward ctxt
        protocol_data.(Alpha_context.Block_header.contents.priority)
        included_endorsements) in
  let=? ctxt := return= (Alpha_context.add_rewards ctxt reward) in
  let=? ctxt :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.fold)
      (fun (delegate : Signature.public_key_hash) =>
        fun (deposit : Alpha_context.Tez.t) =>
          fun (ctxt : M=? Alpha_context.context) =>
            let=? ctxt := ctxt in
            Alpha_context.Delegate.freeze_deposit ctxt delegate deposit)
      (Alpha_context.get_deposits ctxt) (Error_monad._return ctxt) in
  let fees := Alpha_context.get_fees ctxt in
  let=? ctxt := Alpha_context.Delegate.freeze_fees ctxt delegate fees in
  let rewards := Alpha_context.get_rewards ctxt in
  let=? ctxt := Alpha_context.Delegate.freeze_rewards ctxt delegate rewards in
  let=? ctxt :=
    match protocol_data.(Alpha_context.Block_header.contents.seed_nonce_hash)
      with
    | None => Error_monad._return ctxt
    | Some nonce_hash =>
      Alpha_context.Nonce.record_hash ctxt
        {| Storage.Cycle.unrevealed_nonce.nonce_hash := nonce_hash;
          Storage.Cycle.unrevealed_nonce.delegate := delegate;
          Storage.Cycle.unrevealed_nonce.rewards := rewards;
          Storage.Cycle.unrevealed_nonce.fees := fees |}
    end in
  let=? ctxt := may_snapshot_roll ctxt in
  let=? '(ctxt, balance_updates, deactivated) := may_start_new_cycle ctxt in
  let=? ctxt := Amendment.may_start_new_voting_period ctxt in
  let cycle := (Alpha_context.Level.current ctxt).(Alpha_context.Level.t.cycle)
    in
  let balance_updates :=
    Alpha_context.Receipt.cleanup_balance_updates
      (Pervasives.op_at migration_balance_updates
        (Pervasives.op_at
          [
            ((Alpha_context.Receipt.Contract
              (Alpha_context.Contract.implicit_contract delegate)),
              (Alpha_context.Receipt.Debited deposit),
              Alpha_context.Receipt.Block_application);
            ((Alpha_context.Receipt.Deposits delegate cycle),
              (Alpha_context.Receipt.Credited deposit),
              Alpha_context.Receipt.Block_application);
            ((Alpha_context.Receipt.Rewards delegate cycle),
              (Alpha_context.Receipt.Credited reward),
              Alpha_context.Receipt.Block_application)
          ] balance_updates)) in
  let consumed_gas :=
    Alpha_context.Gas.Arith.sub
      (Alpha_context.Gas.Arith.fp_value
        (Alpha_context.Constants.hard_gas_limit_per_block ctxt))
      (Alpha_context.Gas.block_level ctxt) in
  let=? '{|
    Alpha_context.Voting_period.info.voting_period := {|
      Alpha_context.Voting_period.voting_period.kind := kind_value |}
      |} := Alpha_context.Voting_period.get_current_info ctxt in
  let=?
    '{|
      Alpha_context.Voting_period.info.voting_period := voting_period;
        Alpha_context.Voting_period.info.position := position
        |} as voting_period_info :=
    Alpha_context.Voting_period.get_rpc_fixed_current_info ctxt in
  let level_info := Alpha_context.Level.current ctxt in
  let receipt :=
    {| Apply_results.block_metadata.baker := delegate;
      Apply_results.block_metadata.level :=
        Alpha_context.Level.to_deprecated_type level_info
          voting_period.(Alpha_context.Voting_period.voting_period.index)
          position; Apply_results.block_metadata.level_info := level_info;
      Apply_results.block_metadata.voting_period_kind := kind_value;
      Apply_results.block_metadata.voting_period_info := voting_period_info;
      Apply_results.block_metadata.nonce_hash :=
        protocol_data.(Alpha_context.Block_header.contents.seed_nonce_hash);
      Apply_results.block_metadata.consumed_gas := consumed_gas;
      Apply_results.block_metadata.deactivated := deactivated;
      Apply_results.block_metadata.balance_updates := balance_updates |} in
  return=? (ctxt, receipt).
