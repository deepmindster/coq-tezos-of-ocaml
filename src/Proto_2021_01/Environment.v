Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Module Type Pervasives_signature.
  Parameter raise : forall {a : Set}, extensible_type -> a.
  
  Parameter raise_notrace : forall {a : Set}, extensible_type -> a.
  
  Parameter invalid_arg : forall {a : Set}, string -> a.
  
  Parameter failwith : forall {a : Set}, string -> a.
  
  Parameter not : bool -> bool.
  
  Parameter op_andand : bool -> bool -> bool.
  
  Parameter op_pipepipe : bool -> bool -> bool.
  
  Parameter __LOC__ : string.
  
  Parameter __FILE__ : string.
  
  Parameter __LINE__ : int.
  
  Parameter __MODULE__ : string.
  
  Parameter __POS__ : string * int * int * int.
  
  Parameter __LOC_OF__ : forall {a : Set}, a -> string * a.
  
  Parameter __LINE_OF__ : forall {a : Set}, a -> int * a.
  
  Parameter __POS_OF__ : forall {a : Set}, a -> (string * int * int * int) * a.
  
  Parameter op_pipegt : forall {a b : Set}, a -> (a -> b) -> b.
  
  Parameter op_atat : forall {a b : Set}, (a -> b) -> a -> b.
  
  Parameter op_tildeminus : int -> int.
  
  Parameter op_tildeplus : int -> int.
  
  Parameter succ : int -> int.
  
  Parameter pred : int -> int.
  
  Parameter op_plus : int -> int -> int.
  
  Parameter op_minus : int -> int -> int.
  
  Parameter op_star : int -> int -> int.
  
  Parameter op_div : int -> int -> int.
  
  Parameter _mod : int -> int -> int.
  
  Parameter abs : int -> int.
  
  Parameter max_int : int.
  
  Parameter min_int : int.
  
  Parameter land : int -> int -> int.
  
  Parameter lor : int -> int -> int.
  
  Parameter lxor : int -> int -> int.
  
  Parameter lnot : int -> int.
  
  Parameter lsl : int -> int -> int.
  
  Parameter lsr : int -> int -> int.
  
  Parameter asr : int -> int -> int.
  
  Parameter op_caret : string -> string -> string.
  
  Parameter int_of_char : ascii -> int.
  
  Parameter char_of_int : int -> ascii.
  
  Parameter ignore : forall {a : Set}, a -> unit.
  
  Parameter string_of_bool : bool -> string.
  
  Parameter bool_of_string_opt : string -> option bool.
  
  Parameter string_of_int : int -> string.
  
  Parameter int_of_string_opt : string -> option int.
  
  Parameter fst : forall {a b : Set}, a * b -> a.
  
  Parameter snd : forall {a b : Set}, a * b -> b.
  
  Parameter op_at : forall {a : Set}, list a -> list a -> list a.
  
  Module ref.
    Record record {a : Set} : Set := Build {
      contents : a }.
    Arguments record : clear implicits.
    Definition with_contents {t_a} contents (r : record t_a) :=
      Build t_a contents.
  End ref.
  Definition ref := ref.record.
  
  Parameter ref_value : forall {a : Set}, a -> ref a.
  
  Parameter op_exclamation : forall {a : Set}, ref a -> a.
  
  Parameter op_coloneq : forall {a : Set}, ref a -> a -> unit.
  
  Parameter incr : ref int -> unit.
  
  Parameter decr : ref int -> unit.
  
  Inductive result (a b : Set) : Set :=
  | Ok : a -> result a b
  | Error : b -> result a b.
  
  Arguments Ok {_ _}.
  Arguments Error {_ _}.
  
  Definition format6 (a b c d e f : Set) : Set :=
    CamlinternalFormatBasics.format6 a b c d e f.
  
  Definition format4 (a b c d : Set) : Set := format6 a b c c c d.
  
  Definition format (a b c : Set) : Set := format4 a b c c.
  
  Parameter string_of_format : forall {a b c d e f : Set},
    format6 a b c d e f -> string.
  
  Parameter format_of_string : forall {a b c d e f : Set},
    format6 a b c d e f -> format6 a b c d e f.
  
  Parameter op_caretcaret : forall {a b c d e f g h : Set},
    format6 a b c d e f -> format6 f b c e g h -> format6 a b c d g h.
End Pervasives_signature.
Require Export Proto_2021_01.Environment.Pervasives.
Module Pervasives_check : Pervasives_signature := Pervasives.

Module Type List_signature.
  Definition t (a : Set) : Set := list a.
  
  Parameter length : forall {a : Set}, list a -> int.
  
  Parameter compare_lengths : forall {a b : Set}, list a -> list b -> int.
  
  Parameter compare_length_with : forall {a : Set}, list a -> int -> int.
  
  Parameter cons_value : forall {a : Set}, a -> list a -> list a.
  
  Parameter hd : forall {a : Set}, list a -> a.
  
  Parameter tl : forall {a : Set}, list a -> list a.
  
  Parameter nth_opt : forall {a : Set}, list a -> int -> option a.
  
  Parameter rev : forall {a : Set}, list a -> list a.
  
  Parameter init_value : forall {a : Set}, int -> (int -> a) -> list a.
  
  Parameter append : forall {a : Set}, list a -> list a -> list a.
  
  Parameter rev_append : forall {a : Set}, list a -> list a -> list a.
  
  Parameter concat : forall {a : Set}, list (list a) -> list a.
  
  Parameter flatten : forall {a : Set}, list (list a) -> list a.
  
  Parameter iter : forall {a : Set}, (a -> unit) -> list a -> unit.
  
  Parameter iteri : forall {a : Set}, (int -> a -> unit) -> list a -> unit.
  
  Parameter map : forall {a b : Set}, (a -> b) -> list a -> list b.
  
  Parameter mapi : forall {a b : Set}, (int -> a -> b) -> list a -> list b.
  
  Parameter rev_map : forall {a b : Set}, (a -> b) -> list a -> list b.
  
  Parameter filter_map : forall {a b : Set},
    (a -> option b) -> list a -> list b.
  
  Parameter fold_left : forall {a b : Set}, (a -> b -> a) -> a -> list b -> a.
  
  Parameter fold_right : forall {a b : Set}, (a -> b -> b) -> list a -> b -> b.
  
  Parameter iter2 : forall {a b : Set},
    (a -> b -> unit) -> list a -> list b -> unit.
  
  Parameter map2 : forall {a b c : Set},
    (a -> b -> c) -> list a -> list b -> list c.
  
  Parameter rev_map2 : forall {a b c : Set},
    (a -> b -> c) -> list a -> list b -> list c.
  
  Parameter fold_left2 : forall {a b c : Set},
    (a -> b -> c -> a) -> a -> list b -> list c -> a.
  
  Parameter fold_right2 : forall {a b c : Set},
    (a -> b -> c -> c) -> list a -> list b -> c -> c.
  
  Parameter for_all : forall {a : Set}, (a -> bool) -> list a -> bool.
  
  Parameter _exists : forall {a : Set}, (a -> bool) -> list a -> bool.
  
  Parameter for_all2 : forall {a b : Set},
    (a -> b -> bool) -> list a -> list b -> bool.
  
  Parameter _exists2 : forall {a b : Set},
    (a -> b -> bool) -> list a -> list b -> bool.
  
  Parameter find_opt : forall {a : Set}, (a -> bool) -> list a -> option a.
  
  Parameter filter : forall {a : Set}, (a -> bool) -> list a -> list a.
  
  Parameter find_all : forall {a : Set}, (a -> bool) -> list a -> list a.
  
  Parameter partition : forall {a : Set},
    (a -> bool) -> list a -> list a * list a.
  
  Parameter split : forall {a b : Set}, list (a * b) -> list a * list b.
  
  Parameter combine : forall {a b : Set}, list a -> list b -> list (a * b).
  
  Parameter sort : forall {a : Set}, (a -> a -> int) -> list a -> list a.
  
  Parameter stable_sort : forall {a : Set}, (a -> a -> int) -> list a -> list a.
  
  Parameter fast_sort : forall {a : Set}, (a -> a -> int) -> list a -> list a.
  
  Parameter sort_uniq : forall {a : Set}, (a -> a -> int) -> list a -> list a.
  
  Parameter merge : forall {a : Set},
    (a -> a -> int) -> list a -> list a -> list a.
End List_signature.
Require Export Proto_2021_01.Environment.List.
Module List_check : List_signature := List.

Module Type String_signature.
  Parameter length : string -> int.
  
  Parameter get : string -> int -> ascii.
  
  Parameter make : int -> ascii -> string.
  
  Parameter init_value : int -> (int -> ascii) -> string.
  
  Parameter sub : string -> int -> int -> string.
  
  Parameter blit : string -> int -> bytes -> int -> int -> unit.
  
  Parameter concat : string -> list string -> string.
  
  Parameter iter : (ascii -> unit) -> string -> unit.
  
  Parameter iteri : (int -> ascii -> unit) -> string -> unit.
  
  Parameter map : (ascii -> ascii) -> string -> string.
  
  Parameter mapi : (int -> ascii -> ascii) -> string -> string.
  
  Parameter trim : string -> string.
  
  Parameter escaped : string -> string.
  
  Parameter index_opt : string -> ascii -> option int.
  
  Parameter rindex_opt : string -> ascii -> option int.
  
  Parameter index_from_opt : string -> int -> ascii -> option int.
  
  Parameter rindex_from_opt : string -> int -> ascii -> option int.
  
  Parameter contains : string -> ascii -> bool.
  
  Parameter contains_from : string -> int -> ascii -> bool.
  
  Parameter rcontains_from : string -> int -> ascii -> bool.
  
  Parameter uppercase_ascii : string -> string.
  
  Parameter lowercase_ascii : string -> string.
  
  Parameter capitalize_ascii : string -> string.
  
  Parameter uncapitalize_ascii : string -> string.
  
  Definition t : Set := string.
  
  Parameter compare : t -> t -> int.
  
  Parameter equal : t -> t -> bool.
  
  Parameter split_on_char : ascii -> string -> list string.
End String_signature.
Require Export Proto_2021_01.Environment.String.
Module String_check : String_signature := String.

Module Type Char_signature.
  Parameter code : ascii -> int.
  
  Parameter chr : int -> ascii.
  
  Parameter escaped : ascii -> string.
  
  Parameter lowercase_ascii : ascii -> ascii.
  
  Parameter uppercase_ascii : ascii -> ascii.
  
  Definition t : Set := ascii.
  
  Parameter compare : t -> t -> int.
  
  Parameter equal : t -> t -> bool.
End Char_signature.
Require Export Proto_2021_01.Environment.Char.
Module Char_check : Char_signature := Char.

Module Type Bytes_signature.
  Parameter length : bytes -> int.
  
  Parameter get : bytes -> int -> ascii.
  
  Parameter set : bytes -> int -> ascii -> unit.
  
  Parameter make : int -> ascii -> bytes.
  
  Parameter init_value : int -> (int -> ascii) -> bytes.
  
  Parameter empty : bytes.
  
  Parameter copy : bytes -> bytes.
  
  Parameter of_string : string -> bytes.
  
  Parameter to_string : bytes -> string.
  
  Parameter sub : bytes -> int -> int -> bytes.
  
  Parameter sub_string : bytes -> int -> int -> string.
  
  Parameter extend : bytes -> int -> int -> bytes.
  
  Parameter fill : bytes -> int -> int -> ascii -> unit.
  
  Parameter blit : bytes -> int -> bytes -> int -> int -> unit.
  
  Parameter blit_string : string -> int -> bytes -> int -> int -> unit.
  
  Parameter concat : bytes -> list bytes -> bytes.
  
  Parameter cat : bytes -> bytes -> bytes.
  
  Parameter iter : (ascii -> unit) -> bytes -> unit.
  
  Parameter iteri : (int -> ascii -> unit) -> bytes -> unit.
  
  Parameter map : (ascii -> ascii) -> bytes -> bytes.
  
  Parameter mapi : (int -> ascii -> ascii) -> bytes -> bytes.
  
  Parameter trim : bytes -> bytes.
  
  Parameter escaped : bytes -> bytes.
  
  Parameter index_opt : bytes -> ascii -> option int.
  
  Parameter rindex_opt : bytes -> ascii -> option int.
  
  Parameter index_from_opt : bytes -> int -> ascii -> option int.
  
  Parameter rindex_from_opt : bytes -> int -> ascii -> option int.
  
  Parameter contains : bytes -> ascii -> bool.
  
  Parameter contains_from : bytes -> int -> ascii -> bool.
  
  Parameter rcontains_from : bytes -> int -> ascii -> bool.
  
  Parameter uppercase_ascii : bytes -> bytes.
  
  Parameter lowercase_ascii : bytes -> bytes.
  
  Parameter capitalize_ascii : bytes -> bytes.
  
  Parameter uncapitalize_ascii : bytes -> bytes.
  
  Definition t : Set := bytes.
  
  Parameter compare : t -> t -> int.
  
  Parameter equal : t -> t -> bool.
End Bytes_signature.
Require Export Proto_2021_01.Environment.Bytes.
Module Bytes_check : Bytes_signature := Bytes.

Module Type Int32_signature.
  Parameter zero : int32.
  
  Parameter one : int32.
  
  Parameter minus_one : int32.
  
  Parameter neg : int32 -> int32.
  
  Parameter add : int32 -> int32 -> int32.
  
  Parameter sub : int32 -> int32 -> int32.
  
  Parameter mul : int32 -> int32 -> int32.
  
  Parameter div : int32 -> int32 -> int32.
  
  Parameter rem : int32 -> int32 -> int32.
  
  Parameter succ : int32 -> int32.
  
  Parameter pred : int32 -> int32.
  
  Parameter abs : int32 -> int32.
  
  Parameter max_int : int32.
  
  Parameter min_int : int32.
  
  Parameter logand : int32 -> int32 -> int32.
  
  Parameter logor : int32 -> int32 -> int32.
  
  Parameter logxor : int32 -> int32 -> int32.
  
  Parameter lognot : int32 -> int32.
  
  Parameter shift_left : int32 -> int -> int32.
  
  Parameter shift_right : int32 -> int -> int32.
  
  Parameter shift_right_logical : int32 -> int -> int32.
  
  Parameter of_int : int -> int32.
  
  Parameter to_int : int32 -> int.
  
  Parameter of_string_opt : string -> option int32.
  
  Parameter to_string : int32 -> string.
  
  Definition t : Set := int32.
  
  Parameter compare : t -> t -> int.
  
  Parameter equal : t -> t -> bool.
End Int32_signature.
Require Export Proto_2021_01.Environment.Int32.
Module Int32_check : Int32_signature := Int32.

Module Type Int64_signature.
  Parameter zero : int64.
  
  Parameter one : int64.
  
  Parameter minus_one : int64.
  
  Parameter neg : int64 -> int64.
  
  Parameter add : int64 -> int64 -> int64.
  
  Parameter sub : int64 -> int64 -> int64.
  
  Parameter mul : int64 -> int64 -> int64.
  
  Parameter div : int64 -> int64 -> int64.
  
  Parameter rem : int64 -> int64 -> int64.
  
  Parameter succ : int64 -> int64.
  
  Parameter pred : int64 -> int64.
  
  Parameter abs : int64 -> int64.
  
  Parameter max_int : int64.
  
  Parameter min_int : int64.
  
  Parameter logand : int64 -> int64 -> int64.
  
  Parameter logor : int64 -> int64 -> int64.
  
  Parameter logxor : int64 -> int64 -> int64.
  
  Parameter lognot : int64 -> int64.
  
  Parameter shift_left : int64 -> int -> int64.
  
  Parameter shift_right : int64 -> int -> int64.
  
  Parameter shift_right_logical : int64 -> int -> int64.
  
  Parameter of_int : int -> int64.
  
  Parameter to_int : int64 -> int.
  
  Parameter of_int32 : int32 -> int64.
  
  Parameter to_int32 : int64 -> int32.
  
  Parameter of_string_opt : string -> option int64.
  
  Parameter to_string : int64 -> string.
  
  Definition t : Set := int64.
  
  Parameter compare : t -> t -> int.
  
  Parameter equal : t -> t -> bool.
End Int64_signature.
Require Export Proto_2021_01.Environment.Int64.
Module Int64_check : Int64_signature := Int64.

Module Type Format_signature.
  Parameter formatter : Set.
  
  Parameter pp_open_box : formatter -> int -> unit.
  
  Parameter pp_close_box : formatter -> unit -> unit.
  
  Parameter pp_open_hbox : formatter -> unit -> unit.
  
  Parameter pp_open_vbox : formatter -> int -> unit.
  
  Parameter pp_open_hvbox : formatter -> int -> unit.
  
  Parameter pp_open_hovbox : formatter -> int -> unit.
  
  Parameter pp_print_string : formatter -> string -> unit.
  
  Parameter pp_print_as : formatter -> int -> string -> unit.
  
  Parameter pp_print_int : formatter -> int -> unit.
  
  Parameter pp_print_char : formatter -> ascii -> unit.
  
  Parameter pp_print_bool : formatter -> bool -> unit.
  
  Parameter pp_print_space : formatter -> unit -> unit.
  
  Parameter pp_print_cut : formatter -> unit -> unit.
  
  Parameter pp_print_break : formatter -> int -> int -> unit.
  
  Parameter pp_print_custom_break :
    formatter -> string * int * string -> string * int * string -> unit.
  
  Parameter pp_force_newline : formatter -> unit -> unit.
  
  Parameter pp_print_if_newline : formatter -> unit -> unit.
  
  Parameter pp_print_flush : formatter -> unit -> unit.
  
  Parameter pp_print_newline : formatter -> unit -> unit.
  
  Parameter pp_set_margin : formatter -> int -> unit.
  
  Parameter pp_get_margin : formatter -> unit -> int.
  
  Parameter pp_set_max_indent : formatter -> int -> unit.
  
  Parameter pp_get_max_indent : formatter -> unit -> int.
  
  Parameter pp_set_max_boxes : formatter -> int -> unit.
  
  Parameter pp_get_max_boxes : formatter -> unit -> int.
  
  Parameter pp_over_max_boxes : formatter -> unit -> bool.
  
  Parameter pp_open_tbox : formatter -> unit -> unit.
  
  Parameter pp_close_tbox : formatter -> unit -> unit.
  
  Parameter pp_set_tab : formatter -> unit -> unit.
  
  Parameter pp_print_tab : formatter -> unit -> unit.
  
  Parameter pp_print_tbreak : formatter -> int -> int -> unit.
  
  Parameter pp_set_ellipsis_text : formatter -> string -> unit.
  
  Parameter pp_get_ellipsis_text : formatter -> unit -> string.
  
  Parameter pp_print_list : forall {a : Set},
    option (formatter -> unit -> unit) -> (formatter -> a -> unit) ->
    formatter -> list a -> unit.
  
  Parameter pp_print_text : formatter -> string -> unit.
  
  Parameter pp_print_option : forall {a : Set},
    option (formatter -> unit -> unit) -> (formatter -> a -> unit) ->
    formatter -> option a -> unit.
  
  Parameter pp_print_result : forall {a e : Set},
    (formatter -> a -> unit) -> (formatter -> e -> unit) -> formatter ->
    Pervasives.result a e -> unit.
  
  Parameter fprintf : forall {a : Set},
    formatter -> Pervasives.format a formatter unit -> a.
  
  Parameter sprintf : forall {a : Set}, Pervasives.format a unit string -> a.
  
  Parameter asprintf : forall {a : Set},
    Pervasives.format4 a formatter unit string -> a.
  
  Parameter dprintf : forall {a : Set},
    Pervasives.format4 a formatter unit (formatter -> unit) -> a.
  
  Parameter ifprintf : forall {a : Set},
    formatter -> Pervasives.format a formatter unit -> a.
  
  Parameter kfprintf : forall {a b : Set},
    (formatter -> a) -> formatter -> Pervasives.format4 b formatter unit a -> b.
  
  Parameter kdprintf : forall {a b : Set},
    ((formatter -> unit) -> a) -> Pervasives.format4 b formatter unit a -> b.
  
  Parameter ikfprintf : forall {a b : Set},
    (formatter -> a) -> formatter -> Pervasives.format4 b formatter unit a -> b.
  
  Parameter ksprintf : forall {a b : Set},
    (string -> a) -> Pervasives.format4 b unit string a -> b.
  
  Parameter kasprintf : forall {a b : Set},
    (string -> a) -> Pervasives.format4 b formatter unit a -> b.
End Format_signature.
Require Export Proto_2021_01.Environment.Format.
Module Format_check : Format_signature := Format.

Module Type Hex_signature.
  Inductive t : Set :=
  | Hex : string -> t.
  
  Parameter of_char : ascii -> ascii * ascii.
  
  Parameter to_char : ascii -> ascii -> ascii.
  
  Parameter of_string : option (list ascii) -> string -> t.
  
  Parameter to_string : t -> string.
  
  Parameter of_bytes : option (list ascii) -> bytes -> t.
  
  Parameter to_bytes : t -> bytes.
  
  Parameter hexdump_s : option bool -> option bool -> t -> string.
  
  Parameter pp : Format.formatter -> t -> unit.
  
  Parameter show : t -> string.
End Hex_signature.
Require Export Proto_2021_01.Environment.Hex.
Module Hex_check : Hex_signature := Hex.

Module Type Z_signature.
  Parameter t : Set.
  
  Parameter zero : t.
  
  Parameter one : t.
  
  Parameter minus_one : t.
  
  Parameter of_int : int -> t.
  
  Parameter of_int32 : int32 -> t.
  
  Parameter of_int64 : int64 -> t.
  
  Parameter of_string : string -> t.
  
  Parameter of_substring : string -> int -> int -> t.
  
  Parameter of_string_base : int -> string -> t.
  
  Parameter of_substring_base : int -> string -> int -> int -> t.
  
  Parameter succ : t -> t.
  
  Parameter pred : t -> t.
  
  Parameter abs : t -> t.
  
  Parameter neg : t -> t.
  
  Parameter add : t -> t -> t.
  
  Parameter sub : t -> t -> t.
  
  Parameter mul : t -> t -> t.
  
  Parameter div : t -> t -> t.
  
  Parameter rem : t -> t -> t.
  
  Parameter div_rem : t -> t -> t * t.
  
  Parameter cdiv : t -> t -> t.
  
  Parameter fdiv : t -> t -> t.
  
  Parameter ediv_rem : t -> t -> t * t.
  
  Parameter ediv : t -> t -> t.
  
  Parameter erem : t -> t -> t.
  
  Parameter divexact : t -> t -> t.
  
  Parameter divisible : t -> t -> bool.
  
  Parameter congruent : t -> t -> t -> bool.
  
  Parameter logand : t -> t -> t.
  
  Parameter logor : t -> t -> t.
  
  Parameter logxor : t -> t -> t.
  
  Parameter lognot : t -> t.
  
  Parameter shift_left : t -> int -> t.
  
  Parameter shift_right : t -> int -> t.
  
  Parameter shift_right_trunc : t -> int -> t.
  
  Parameter numbits : t -> int.
  
  Parameter trailing_zeros : t -> int.
  
  Parameter testbit : t -> int -> bool.
  
  Parameter popcount : t -> int.
  
  Parameter hamdist : t -> t -> int.
  
  Parameter to_int : t -> int.
  
  Parameter to_int32 : t -> int32.
  
  Parameter to_int64 : t -> int64.
  
  Parameter to_string : t -> string.
  
  Parameter format : string -> t -> string.
  
  Parameter fits_int : t -> bool.
  
  Parameter fits_int32 : t -> bool.
  
  Parameter fits_int64 : t -> bool.
  
  Parameter pp_print : Format.formatter -> t -> unit.
  
  Parameter compare : t -> t -> int.
  
  Parameter equal : t -> t -> bool.
  
  Parameter leq : t -> t -> bool.
  
  Parameter geq : t -> t -> bool.
  
  Parameter lt : t -> t -> bool.
  
  Parameter gt : t -> t -> bool.
  
  Parameter sign : t -> int.
  
  Parameter min : t -> t -> t.
  
  Parameter max : t -> t -> t.
  
  Parameter is_even : t -> bool.
  
  Parameter is_odd : t -> bool.
  
  Parameter pow : t -> int -> t.
  
  Parameter sqrt : t -> t.
  
  Parameter sqrt_rem : t -> t * t.
  
  Parameter root : t -> int -> t.
  
  Parameter rootrem : t -> int -> t * t.
  
  Parameter perfect_power : t -> bool.
  
  Parameter perfect_square : t -> bool.
  
  Parameter log2 : t -> int.
  
  Parameter log2up : t -> int.
  
  Parameter size : t -> int.
  
  Parameter extract : t -> int -> int -> t.
  
  Parameter signed_extract : t -> int -> int -> t.
  
  Parameter to_bits : t -> string.
  
  Parameter of_bits : string -> t.
End Z_signature.
Require Export Proto_2021_01.Environment.Z.
Module Z_check : Z_signature := Z.

Module Type Lwt_signature.
  Parameter t : forall (a : Set), Set.
  
  Parameter _return : forall {a : Set}, a -> t a.
  
  Parameter bind : forall {a b : Set}, t a -> (a -> t b) -> t b.
  
  Parameter map : forall {a b : Set}, (a -> b) -> t a -> t b.
  
  Parameter return_unit : t unit.
  
  Parameter return_none : forall {A : Set}, t (option A).
  
  Parameter return_nil : forall {A : Set}, t (list A).
  
  Parameter return_true : t bool.
  
  Parameter return_false : t bool.
End Lwt_signature.
Require Export Proto_2021_01.Environment.Lwt.
Module Lwt_check : Lwt_signature := Lwt.

Module Type Lwt_list_signature.
  Parameter iter_s : forall {a : Set},
    (a -> Lwt.t unit) -> list a -> Lwt.t unit.
  
  Parameter iteri_s : forall {a : Set},
    (int -> a -> Lwt.t unit) -> list a -> Lwt.t unit.
  
  Parameter map_s : forall {a b : Set},
    (a -> Lwt.t b) -> list a -> Lwt.t (list b).
  
  Parameter mapi_s : forall {a b : Set},
    (int -> a -> Lwt.t b) -> list a -> Lwt.t (list b).
  
  Parameter rev_map_s : forall {a b : Set},
    (a -> Lwt.t b) -> list a -> Lwt.t (list b).
  
  Parameter fold_left_s : forall {a b : Set},
    (a -> b -> Lwt.t a) -> a -> list b -> Lwt.t a.
  
  Parameter fold_right_s : forall {a b : Set},
    (a -> b -> Lwt.t b) -> list a -> b -> Lwt.t b.
  
  Parameter for_all_s : forall {a : Set},
    (a -> Lwt.t bool) -> list a -> Lwt.t bool.
  
  Parameter exists_s : forall {a : Set},
    (a -> Lwt.t bool) -> list a -> Lwt.t bool.
  
  Parameter find_s : forall {a : Set}, (a -> Lwt.t bool) -> list a -> Lwt.t a.
  
  Parameter filter_s : forall {a : Set},
    (a -> Lwt.t bool) -> list a -> Lwt.t (list a).
  
  Parameter filter_map_s : forall {a b : Set},
    (a -> Lwt.t (option b)) -> list a -> Lwt.t (list b).
  
  Parameter partition_s : forall {a : Set},
    (a -> Lwt.t bool) -> list a -> Lwt.t (list a * list a).
End Lwt_list_signature.
Require Export Proto_2021_01.Environment.Lwt_list.
Module Lwt_list_check : Lwt_list_signature := Lwt_list.

Module Type Data_encoding_signature.
  Inductive json : Set :=
  | Bool : bool -> json
  | Null : json
  | O : list (string * json) -> json
  | Float : float -> json
  | String : string -> json
  | A : list json -> json.
  
  Parameter json_schema : Set.
  
  Parameter t : forall (a : Set), Set.
  
  Definition encoding (a : Set) : Set := t a.
  
  Parameter classify : forall {a : Set},
    encoding a -> (* `Variable *) unit + (* `Fixed *) int + (* `Dynamic *) unit.
  
  Parameter splitted : forall {a : Set}, encoding a -> encoding a -> encoding a.
  
  Parameter null : encoding unit.
  
  Parameter empty : encoding unit.
  
  Parameter unit_value : encoding unit.
  
  Parameter constant : string -> encoding unit.
  
  Parameter int8 : encoding int.
  
  Parameter uint8 : encoding int.
  
  Parameter int16 : encoding int.
  
  Parameter uint16 : encoding int.
  
  Parameter int31 : encoding int.
  
  Parameter int32_value : encoding int32.
  
  Parameter int64_value : encoding int64.
  
  Parameter n : encoding Z.t.
  
  Parameter z : encoding Z.t.
  
  Parameter bool_value : encoding bool.
  
  Parameter string_value : encoding string.
  
  Parameter bytes_value : encoding bytes.
  
  Parameter option_value : forall {a : Set}, encoding a -> encoding (option a).
  
  Parameter string_enum : forall {a : Set}, list (string * a) -> encoding a.
  
  Module Fixed.
    Parameter string_value : int -> encoding string.
    
    Parameter bytes_value : int -> encoding bytes.
    
    Parameter add_padding : forall {a : Set}, encoding a -> int -> encoding a.
  End Fixed.
  
  Module _Variable.
    Parameter string_value : encoding string.
    
    Parameter bytes_value : encoding bytes.
    
    Parameter array : forall {a : Set},
      option int -> encoding a -> encoding (array a).
    
    Parameter list_value : forall {a : Set},
      option int -> encoding a -> encoding (list a).
  End _Variable.
  
  Module Bounded.
    Parameter string_value : int -> encoding string.
    
    Parameter bytes_value : int -> encoding bytes.
  End Bounded.
  
  Parameter dynamic_size : forall {a : Set},
    option ((* `Uint16 *) unit + (* `Uint8 *) unit + (* `Uint30 *) unit) ->
    encoding a -> encoding a.
  
  Parameter json_value : encoding json.
  
  Parameter json_schema_value : encoding json_schema.
  
  Parameter field : forall (a : Set), Set.
  
  Parameter req : forall {t : Set},
    option string -> option string -> string -> encoding t -> field t.
  
  Parameter opt : forall {t : Set},
    option string -> option string -> string -> encoding t -> field (option t).
  
  Parameter varopt : forall {t : Set},
    option string -> option string -> string -> encoding t -> field (option t).
  
  Parameter dft : forall {t : Set},
    option string -> option string -> string -> encoding t -> t -> field t.
  
  Parameter obj1 : forall {f1 : Set}, field f1 -> encoding f1.
  
  Parameter obj2 : forall {f1 f2 : Set},
    field f1 -> field f2 -> encoding (f1 * f2).
  
  Parameter obj3 : forall {f1 f2 f3 : Set},
    field f1 -> field f2 -> field f3 -> encoding (f1 * f2 * f3).
  
  Parameter obj4 : forall {f1 f2 f3 f4 : Set},
    field f1 -> field f2 -> field f3 -> field f4 -> encoding (f1 * f2 * f3 * f4).
  
  Parameter obj5 : forall {f1 f2 f3 f4 f5 : Set},
    field f1 -> field f2 -> field f3 -> field f4 -> field f5 ->
    encoding (f1 * f2 * f3 * f4 * f5).
  
  Parameter obj6 : forall {f1 f2 f3 f4 f5 f6 : Set},
    field f1 -> field f2 -> field f3 -> field f4 -> field f5 -> field f6 ->
    encoding (f1 * f2 * f3 * f4 * f5 * f6).
  
  Parameter obj7 : forall {f1 f2 f3 f4 f5 f6 f7 : Set},
    field f1 -> field f2 -> field f3 -> field f4 -> field f5 -> field f6 ->
    field f7 -> encoding (f1 * f2 * f3 * f4 * f5 * f6 * f7).
  
  Parameter obj8 : forall {f1 f2 f3 f4 f5 f6 f7 f8 : Set},
    field f1 -> field f2 -> field f3 -> field f4 -> field f5 -> field f6 ->
    field f7 -> field f8 -> encoding (f1 * f2 * f3 * f4 * f5 * f6 * f7 * f8).
  
  Parameter obj9 : forall {f1 f2 f3 f4 f5 f6 f7 f8 f9 : Set},
    field f1 -> field f2 -> field f3 -> field f4 -> field f5 -> field f6 ->
    field f7 -> field f8 -> field f9 ->
    encoding (f1 * f2 * f3 * f4 * f5 * f6 * f7 * f8 * f9).
  
  Parameter obj10 : forall {f1 f10 f2 f3 f4 f5 f6 f7 f8 f9 : Set},
    field f1 -> field f2 -> field f3 -> field f4 -> field f5 -> field f6 ->
    field f7 -> field f8 -> field f9 -> field f10 ->
    encoding (f1 * f2 * f3 * f4 * f5 * f6 * f7 * f8 * f9 * f10).
  
  Parameter tup1 : forall {f1 : Set}, encoding f1 -> encoding f1.
  
  Parameter tup2 : forall {f1 f2 : Set},
    encoding f1 -> encoding f2 -> encoding (f1 * f2).
  
  Parameter tup3 : forall {f1 f2 f3 : Set},
    encoding f1 -> encoding f2 -> encoding f3 -> encoding (f1 * f2 * f3).
  
  Parameter tup4 : forall {f1 f2 f3 f4 : Set},
    encoding f1 -> encoding f2 -> encoding f3 -> encoding f4 ->
    encoding (f1 * f2 * f3 * f4).
  
  Parameter tup5 : forall {f1 f2 f3 f4 f5 : Set},
    encoding f1 -> encoding f2 -> encoding f3 -> encoding f4 -> encoding f5 ->
    encoding (f1 * f2 * f3 * f4 * f5).
  
  Parameter tup6 : forall {f1 f2 f3 f4 f5 f6 : Set},
    encoding f1 -> encoding f2 -> encoding f3 -> encoding f4 -> encoding f5 ->
    encoding f6 -> encoding (f1 * f2 * f3 * f4 * f5 * f6).
  
  Parameter tup7 : forall {f1 f2 f3 f4 f5 f6 f7 : Set},
    encoding f1 -> encoding f2 -> encoding f3 -> encoding f4 -> encoding f5 ->
    encoding f6 -> encoding f7 -> encoding (f1 * f2 * f3 * f4 * f5 * f6 * f7).
  
  Parameter tup8 : forall {f1 f2 f3 f4 f5 f6 f7 f8 : Set},
    encoding f1 -> encoding f2 -> encoding f3 -> encoding f4 -> encoding f5 ->
    encoding f6 -> encoding f7 -> encoding f8 ->
    encoding (f1 * f2 * f3 * f4 * f5 * f6 * f7 * f8).
  
  Parameter tup9 : forall {f1 f2 f3 f4 f5 f6 f7 f8 f9 : Set},
    encoding f1 -> encoding f2 -> encoding f3 -> encoding f4 -> encoding f5 ->
    encoding f6 -> encoding f7 -> encoding f8 -> encoding f9 ->
    encoding (f1 * f2 * f3 * f4 * f5 * f6 * f7 * f8 * f9).
  
  Parameter tup10 : forall {f1 f10 f2 f3 f4 f5 f6 f7 f8 f9 : Set},
    encoding f1 -> encoding f2 -> encoding f3 -> encoding f4 -> encoding f5 ->
    encoding f6 -> encoding f7 -> encoding f8 -> encoding f9 -> encoding f10 ->
    encoding (f1 * f2 * f3 * f4 * f5 * f6 * f7 * f8 * f9 * f10).
  
  Parameter merge_objs : forall {o1 o2 : Set},
    encoding o1 -> encoding o2 -> encoding (o1 * o2).
  
  Parameter merge_tups : forall {a1 a2 : Set},
    encoding a1 -> encoding a2 -> encoding (a1 * a2).
  
  Parameter array : forall {a : Set},
    option int -> encoding a -> encoding (array a).
  
  Parameter list_value : forall {a : Set},
    option int -> encoding a -> encoding (list a).
  
  Parameter assoc : forall {a : Set},
    encoding a -> encoding (list (string * a)).
  
  Inductive case_tag : Set :=
  | Tag : int -> case_tag
  | Json_only : case_tag.
  
  Parameter case : forall (t : Set), Set.
  
  Parameter case_value : forall {a t : Set},
    string -> option string -> case_tag -> encoding a -> (t -> option a) ->
    (a -> t) -> case t.
  
  Inductive tag_size : Set :=
  | Uint16 : tag_size
  | Uint8 : tag_size.
  
  Parameter union : forall {t : Set},
    option tag_size -> list (case t) -> encoding t.
  
  Parameter def : forall {t : Set},
    string -> option string -> option string -> encoding t -> encoding t.
  
  Parameter conv : forall {a b : Set},
    (a -> b) -> (b -> a) -> option json_schema -> encoding b -> encoding a.
  
  Parameter mu : forall {a : Set},
    string -> option string -> option string -> (encoding a -> encoding a) ->
    encoding a.
  
  Parameter lazy_t : forall (a : Set), Set.
  
  Parameter lazy_encoding : forall {a : Set}, encoding a -> encoding (lazy_t a).
  
  Parameter force_decode : forall {a : Set}, lazy_t a -> option a.
  
  Parameter force_bytes : forall {a : Set}, lazy_t a -> bytes.
  
  Parameter make_lazy : forall {a : Set}, encoding a -> a -> lazy_t a.
  
  Parameter apply_lazy : forall {a b : Set},
    (a -> b) -> (bytes -> b) -> (b -> b -> b) -> lazy_t a -> b.
  
  Module Json.
    Parameter schema : forall {a : Set},
      option string -> encoding a -> json_schema.
    
    Parameter construct : forall {t : Set}, encoding t -> t -> json.
    
    Parameter destruct : forall {t : Set}, encoding t -> json -> t.
    
    Reserved Notation "'path".
    
    Inductive path_item : Set :=
    | Index : int -> path_item
    | Field : string -> path_item
    | Next : path_item
    | Star : path_item
    
    where "'path" := (list path_item).
    
    Definition path := 'path.
    
    Parameter print_error :
      option (Format.formatter -> extensible_type -> unit) ->
      Format.formatter -> extensible_type -> unit.
    
    Parameter cannot_destruct : forall {a b : Set},
      Pervasives.format4 a Format.formatter unit b -> a.
    
    Parameter wrap_error : forall {a b : Set}, (a -> b) -> a -> b.
    
    Parameter pp : Format.formatter -> json -> unit.
  End Json.
  
  Module Binary.
    Parameter length : forall {a : Set}, encoding a -> a -> int.
    
    Parameter fixed_length : forall {a : Set}, encoding a -> option int.
    
    Parameter read : forall {a : Set},
      encoding a -> bytes -> int -> int -> option (int * a).
    
    Parameter write : forall {a : Set},
      encoding a -> a -> bytes -> int -> int -> option int.
    
    Parameter to_bytes : forall {a : Set}, encoding a -> a -> option bytes.
    
    Parameter to_bytes_exn : forall {a : Set}, encoding a -> a -> bytes.
    
    Parameter of_bytes : forall {a : Set}, encoding a -> bytes -> option a.
    
    Parameter write_error : Set.
  End Binary.
  
  Parameter check_size : forall {a : Set}, int -> encoding a -> encoding a.
End Data_encoding_signature.
Require Export Proto_2021_01.Environment.Data_encoding.
Module Data_encoding_check : Data_encoding_signature := Data_encoding.

Module Type Raw_hashes_signature.
  Parameter blake2b : bytes -> bytes.
  
  Parameter sha256 : bytes -> bytes.
  
  Parameter sha512 : bytes -> bytes.
  
  Parameter keccak256 : bytes -> bytes.
  
  Parameter sha3_256 : bytes -> bytes.
  
  Parameter sha3_512 : bytes -> bytes.
End Raw_hashes_signature.
Require Export Proto_2021_01.Environment.Raw_hashes.
Module Raw_hashes_check : Raw_hashes_signature := Raw_hashes.

Module Type Compare_signature.
  Module COMPARABLE.
    Record signature {t : Set} : Set := {
      t := t;
      compare : t -> t -> int;
    }.
  End COMPARABLE.
  Definition COMPARABLE := @COMPARABLE.signature.
  Arguments COMPARABLE {_}.
  
  Module S.
    Record signature {t : Set} : Set := {
      t := t;
      op_eq : t -> t -> bool;
      op_ltgt : t -> t -> bool;
      op_lt : t -> t -> bool;
      op_lteq : t -> t -> bool;
      op_gteq : t -> t -> bool;
      op_gt : t -> t -> bool;
      compare : t -> t -> int;
      equal : t -> t -> bool;
      max : t -> t -> t;
      min : t -> t -> t;
    }.
  End S.
  Definition S := @S.signature.
  Arguments S {_}.
  
  Parameter Make :
    forall {P_t : Set},
    forall (P : COMPARABLE (t := P_t)), S (t := P.(COMPARABLE.t)).
  
  Parameter Char : S (t := ascii).
  
  Parameter Bool : S (t := bool).
  
  Parameter Int : S (t := int).
  
  Parameter Int32 : S (t := int32).
  
  Parameter Uint32 : S (t := int32).
  
  Parameter Int64 : S (t := int64).
  
  Parameter Uint64 : S (t := int64).
  
  Parameter String : S (t := string).
  
  Parameter Bytes : S (t := bytes).
  
  Parameter Z : S (t := Z.t).
  
  Parameter List :
    forall {P_t : Set},
    forall (P : COMPARABLE (t := P_t)), S (t := list P.(COMPARABLE.t)).
  
  Parameter Option :
    forall {P_t : Set},
    forall (P : COMPARABLE (t := P_t)), S (t := option P.(COMPARABLE.t)).
End Compare_signature.
Require Export Proto_2021_01.Environment.Compare.
Module Compare_check : Compare_signature := Compare.

Module Type Error_monad_signature.
  Inductive error_category : Set :=
  | Permanent : error_category
  | Temporary : error_category
  | Branch : error_category.
  
  Definition _error := extensible_type.
  
  Parameter error_encoding : Data_encoding.t _error.
  
  Parameter pp : Format.formatter -> _error -> unit.
  
  Parameter register_error_kind : forall {err : Set},
    error_category -> string -> string -> string ->
    option (Format.formatter -> err -> unit) -> Data_encoding.t err ->
    (_error -> option err) -> (err -> _error) -> unit.
  
  Parameter classify_error : _error -> error_category.
  
  Parameter json_of_error : _error -> Data_encoding.json.
  
  Parameter error_of_json : Data_encoding.json -> _error.
  
  Module error_info.
    Record record : Set := Build {
      category : error_category;
      id : string;
      title : string;
      description : string;
      schema : Data_encoding.json_schema }.
    Definition with_category category (r : record) :=
      Build category r.(id) r.(title) r.(description) r.(schema).
    Definition with_id id (r : record) :=
      Build r.(category) id r.(title) r.(description) r.(schema).
    Definition with_title title (r : record) :=
      Build r.(category) r.(id) title r.(description) r.(schema).
    Definition with_description description (r : record) :=
      Build r.(category) r.(id) r.(title) description r.(schema).
    Definition with_schema schema (r : record) :=
      Build r.(category) r.(id) r.(title) r.(description) schema.
  End error_info.
  Definition error_info := error_info.record.
  
  Parameter pp_info : Format.formatter -> error_info -> unit.
  
  Parameter get_registered_errors : unit -> list error_info.
  
  Parameter trace : forall (err : Set), Set.
  
  Definition tzresult (a : Set) : Set := Pervasives.result a (trace _error).
  
  Parameter make_trace_encoding : forall {_error : Set},
    Data_encoding.t _error -> Data_encoding.t (trace _error).
  
  Parameter trace_encoding : Data_encoding.t (trace _error).
  
  Parameter pp_trace : Format.formatter -> trace _error -> unit.
  
  Parameter result_encoding : forall {a : Set},
    Data_encoding.t a -> Data_encoding.t (tzresult a).
  
  Parameter ok : forall {a trace : Set}, a -> Pervasives.result a trace.
  
  Parameter ok_unit : forall {trace : Set}, Pervasives.result unit trace.
  
  Parameter ok_none : forall {a trace : Set},
    Pervasives.result (option a) trace.
  
  Parameter ok_some : forall {a trace : Set},
    a -> Pervasives.result (option a) trace.
  
  Parameter ok_nil : forall {a trace : Set}, Pervasives.result (list a) trace.
  
  Parameter ok_true : forall {trace : Set}, Pervasives.result bool trace.
  
  Parameter ok_false : forall {trace : Set}, Pervasives.result bool trace.
  
  Parameter _return : forall {a trace : Set},
    a -> Lwt.t (Pervasives.result a trace).
  
  Parameter return_unit : forall {trace : Set},
    Lwt.t (Pervasives.result unit trace).
  
  Parameter return_none : forall {a trace : Set},
    Lwt.t (Pervasives.result (option a) trace).
  
  Parameter return_some : forall {a trace : Set},
    a -> Lwt.t (Pervasives.result (option a) trace).
  
  Parameter return_nil : forall {a trace : Set},
    Lwt.t (Pervasives.result (list a) trace).
  
  Parameter return_true : forall {trace : Set},
    Lwt.t (Pervasives.result bool trace).
  
  Parameter return_false : forall {trace : Set},
    Lwt.t (Pervasives.result bool trace).
  
  Parameter error_value : forall {a err : Set},
    err -> Pervasives.result a (trace err).
  
  Parameter fail : forall {a err : Set},
    err -> Lwt.t (Pervasives.result a (trace err)).
  
  Parameter op_gtgteq : forall {a b : Set},
    Lwt.t a -> (a -> Lwt.t b) -> Lwt.t b.
  
  Parameter op_gtpipeeq : forall {a b : Set}, Lwt.t a -> (a -> b) -> Lwt.t b.
  
  Parameter op_gtgtquestion : forall {a b trace : Set},
    Pervasives.result a trace -> (a -> Pervasives.result b trace) ->
    Pervasives.result b trace.
  
  Parameter op_gtpipequestion : forall {a b trace : Set},
    Pervasives.result a trace -> (a -> b) -> Pervasives.result b trace.
  
  Parameter op_gtgteqquestion : forall {a b trace : Set},
    Lwt.t (Pervasives.result a trace) ->
    (a -> Lwt.t (Pervasives.result b trace)) ->
    Lwt.t (Pervasives.result b trace).
  
  Parameter op_gtpipeeqquestion : forall {a b trace : Set},
    Lwt.t (Pervasives.result a trace) -> (a -> b) ->
    Lwt.t (Pervasives.result b trace).
  
  Parameter op_gtgtquestioneq : forall {a b trace : Set},
    Pervasives.result a trace -> (a -> Lwt.t (Pervasives.result b trace)) ->
    Lwt.t (Pervasives.result b trace).
  
  Parameter op_gtpipequestioneq : forall {a b trace : Set},
    Pervasives.result a trace -> (a -> Lwt.t b) ->
    Lwt.t (Pervasives.result b trace).
  
  Parameter record_trace : forall {a err : Set},
    err -> Pervasives.result a (trace err) -> Pervasives.result a (trace err).
  
  Parameter trace_value : forall {b err : Set},
    err -> Lwt.t (Pervasives.result b (trace err)) ->
    Lwt.t (Pervasives.result b (trace err)).
  
  Parameter record_trace_eval : forall {a err : Set},
    (unit -> Pervasives.result err (trace err)) ->
    Pervasives.result a (trace err) -> Pervasives.result a (trace err).
  
  Parameter trace_eval : forall {b err : Set},
    (unit -> Lwt.t (Pervasives.result err (trace err))) ->
    Lwt.t (Pervasives.result b (trace err)) ->
    Lwt.t (Pervasives.result b (trace err)).
  
  Parameter error_unless : forall {err : Set},
    bool -> err -> Pervasives.result unit (trace err).
  
  Parameter error_when : forall {err : Set},
    bool -> err -> Pervasives.result unit (trace err).
  
  Parameter fail_unless : forall {err : Set},
    bool -> err -> Lwt.t (Pervasives.result unit (trace err)).
  
  Parameter fail_when : forall {err : Set},
    bool -> err -> Lwt.t (Pervasives.result unit (trace err)).
  
  Parameter unless : forall {trace : Set},
    bool -> (unit -> Lwt.t (Pervasives.result unit trace)) ->
    Lwt.t (Pervasives.result unit trace).
  
  Parameter when_ : forall {trace : Set},
    bool -> (unit -> Lwt.t (Pervasives.result unit trace)) ->
    Lwt.t (Pervasives.result unit trace).
  
  Parameter dont_wait : forall {trace : Set},
    (extensible_type -> unit) -> (trace -> unit) ->
    (unit -> Lwt.t (Pervasives.result unit trace)) -> unit.
  
  Parameter iter : forall {a trace : Set},
    (a -> Pervasives.result unit trace) -> list a ->
    Pervasives.result unit trace.
  
  Parameter iter_s : forall {a trace : Set},
    (a -> Lwt.t (Pervasives.result unit trace)) -> list a ->
    Lwt.t (Pervasives.result unit trace).
  
  Parameter map : forall {a b trace : Set},
    (a -> Pervasives.result b trace) -> list a ->
    Pervasives.result (list b) trace.
  
  Parameter mapi : forall {a b trace : Set},
    (int -> a -> Pervasives.result b trace) -> list a ->
    Pervasives.result (list b) trace.
  
  Parameter map_s : forall {a b trace : Set},
    (a -> Lwt.t (Pervasives.result b trace)) -> list a ->
    Lwt.t (Pervasives.result (list b) trace).
  
  Parameter rev_map_s : forall {a b trace : Set},
    (a -> Lwt.t (Pervasives.result b trace)) -> list a ->
    Lwt.t (Pervasives.result (list b) trace).
  
  Parameter mapi_s : forall {a b trace : Set},
    (int -> a -> Lwt.t (Pervasives.result b trace)) -> list a ->
    Lwt.t (Pervasives.result (list b) trace).
  
  Parameter map2 : forall {a b c trace : Set},
    (a -> b -> Pervasives.result c trace) -> list a -> list b ->
    Pervasives.result (list c) trace.
  
  Parameter mapi2 : forall {a b c trace : Set},
    (int -> a -> b -> Pervasives.result c trace) -> list a -> list b ->
    Pervasives.result (list c) trace.
  
  Parameter map2_s : forall {a b c trace : Set},
    (a -> b -> Lwt.t (Pervasives.result c trace)) -> list a -> list b ->
    Lwt.t (Pervasives.result (list c) trace).
  
  Parameter mapi2_s : forall {a b c trace : Set},
    (int -> a -> b -> Lwt.t (Pervasives.result c trace)) -> list a -> list b ->
    Lwt.t (Pervasives.result (list c) trace).
  
  Parameter filter_map_s : forall {a b trace : Set},
    (a -> Lwt.t (Pervasives.result (option b) trace)) -> list a ->
    Lwt.t (Pervasives.result (list b) trace).
  
  Parameter filter : forall {a trace : Set},
    (a -> Pervasives.result bool trace) -> list a ->
    Pervasives.result (list a) trace.
  
  Parameter filter_s : forall {a trace : Set},
    (a -> Lwt.t (Pervasives.result bool trace)) -> list a ->
    Lwt.t (Pervasives.result (list a) trace).
  
  Parameter fold_left_s : forall {a b trace : Set},
    (a -> b -> Lwt.t (Pervasives.result a trace)) -> a -> list b ->
    Lwt.t (Pervasives.result a trace).
  
  Parameter fold_right_s : forall {a b trace : Set},
    (a -> b -> Lwt.t (Pervasives.result b trace)) -> list a -> b ->
    Lwt.t (Pervasives.result b trace).
  
  Parameter join_e : forall {err : Set},
    list (Pervasives.result unit (trace err)) ->
    Pervasives.result unit (trace err).
  
  Parameter all_e : forall {a err : Set},
    list (Pervasives.result a (trace err)) ->
    Pervasives.result (list a) (trace err).
  
  Parameter both_e : forall {a b err : Set},
    Pervasives.result a (trace err) -> Pervasives.result b (trace err) ->
    Pervasives.result (a * b) (trace err).
  
  Parameter shell_error : Set.
  
  Parameter shell_trace : forall (shell_error : Set), Set.
  
  Definition shell_tzresult (a : Set) : Set :=
    Pervasives.result a (shell_trace shell_error).
End Error_monad_signature.
Require Export Proto_2021_01.Environment.Error_monad.
Module Error_monad_check : Error_monad_signature := Error_monad.

Module Type Logging_signature.
  Parameter debug : forall {a : Set},
    Pervasives.format4 a Format.formatter unit unit -> a.
  
  Parameter log_info : forall {a : Set},
    Pervasives.format4 a Format.formatter unit unit -> a.
  
  Parameter log_notice : forall {a : Set},
    Pervasives.format4 a Format.formatter unit unit -> a.
  
  Parameter warn : forall {a : Set},
    Pervasives.format4 a Format.formatter unit unit -> a.
  
  Parameter log_error : forall {a : Set},
    Pervasives.format4 a Format.formatter unit unit -> a.
  
  Parameter fatal_error : forall {a : Set},
    Pervasives.format4 a Format.formatter unit unit -> a.
  
  Parameter lwt_debug : forall {a : Set},
    Pervasives.format4 a Format.formatter unit (Lwt.t unit) -> a.
  
  Parameter lwt_log_info : forall {a : Set},
    Pervasives.format4 a Format.formatter unit (Lwt.t unit) -> a.
  
  Parameter lwt_log_notice : forall {a : Set},
    Pervasives.format4 a Format.formatter unit (Lwt.t unit) -> a.
  
  Parameter lwt_warn : forall {a : Set},
    Pervasives.format4 a Format.formatter unit (Lwt.t unit) -> a.
  
  Parameter lwt_log_error : forall {a : Set},
    Pervasives.format4 a Format.formatter unit (Lwt.t unit) -> a.
End Logging_signature.
Require Export Proto_2021_01.Environment.Logging.
Module Logging_check : Logging_signature := Logging.

Module Type Time_signature.
  Parameter t : Set.
  
  Parameter Included_S : Compare.S (t := t).
  
  Definition op_eq : t -> t -> bool := Included_S.(Compare.S.op_eq).
  
  Definition op_ltgt : t -> t -> bool := Included_S.(Compare.S.op_ltgt).
  
  Definition op_lt : t -> t -> bool := Included_S.(Compare.S.op_lt).
  
  Definition op_lteq : t -> t -> bool := Included_S.(Compare.S.op_lteq).
  
  Definition op_gteq : t -> t -> bool := Included_S.(Compare.S.op_gteq).
  
  Definition op_gt : t -> t -> bool := Included_S.(Compare.S.op_gt).
  
  Definition compare : t -> t -> int := Included_S.(Compare.S.compare).
  
  Definition equal : t -> t -> bool := Included_S.(Compare.S.equal).
  
  Definition max : t -> t -> t := Included_S.(Compare.S.max).
  
  Definition min : t -> t -> t := Included_S.(Compare.S.min).
  
  Parameter add : t -> int64 -> t.
  
  Parameter diff_value : t -> t -> int64.
  
  Parameter of_seconds : int64 -> t.
  
  Parameter to_seconds : t -> int64.
  
  Parameter of_notation : string -> option t.
  
  Parameter of_notation_exn : string -> t.
  
  Parameter to_notation : t -> string.
  
  Parameter encoding : Data_encoding.t t.
  
  Parameter rfc_encoding : Data_encoding.t t.
  
  Parameter pp_hum : Format.formatter -> t -> unit.
End Time_signature.
Require Export Proto_2021_01.Environment.Time.
Module Time_check : Time_signature := Time.

Module Type Option_signature.
  Definition t (a : Set) : Set := option a.
  
  Parameter none : forall {a : Set}, option a.
  
  Parameter some : forall {a : Set}, a -> option a.
  
  Parameter value : forall {a : Set}, option a -> a -> a.
  
  Parameter bind : forall {a b : Set}, option a -> (a -> option b) -> option b.
  
  Parameter join : forall {a : Set}, option (option a) -> option a.
  
  Parameter map : forall {a b : Set}, (a -> b) -> option a -> option b.
  
  Parameter fold : forall {a b : Set}, a -> (b -> a) -> option b -> a.
  
  Parameter iter : forall {a : Set}, (a -> unit) -> option a -> unit.
  
  Parameter is_none : forall {a : Set}, option a -> bool.
  
  Parameter is_some : forall {a : Set}, option a -> bool.
  
  Parameter equal : forall {a : Set},
    (a -> a -> bool) -> option a -> option a -> bool.
  
  Parameter compare : forall {a : Set},
    (a -> a -> int) -> option a -> option a -> int.
  
  Parameter to_result : forall {a e : Set},
    e -> option a -> Pervasives.result a e.
  
  Parameter to_list : forall {a : Set}, option a -> list a.
  
  Parameter op_gtgteq : forall {a b : Set},
    option a -> (a -> option b) -> option b.
  
  Parameter op_gtgtpipe : forall {a b : Set}, option a -> (a -> b) -> option b.
  
  Parameter first_some : forall {a : Set}, option a -> option a -> option a.
  
  Parameter pp : forall {a : Set},
    option string -> (Format.formatter -> a -> unit) -> Format.formatter ->
    option a -> unit.
End Option_signature.
Require Export Proto_2021_01.Environment.Option.
Module Option_check : Option_signature := Option.

Module Type TzEndian_signature.
  Parameter get_int32 : bytes -> int -> int32.
  
  Parameter set_int32 : bytes -> int -> int32 -> unit.
  
  Parameter set_int8 : bytes -> int -> int -> unit.
  
  Parameter get_int8 : bytes -> int -> int.
  
  Parameter set_int16 : bytes -> int -> int -> unit.
  
  Parameter get_int16 : bytes -> int -> int.
  
  Parameter set_int64 : bytes -> int -> int64 -> unit.
  
  Parameter get_int64 : bytes -> int -> int64.
  
  Parameter get_uint8 : bytes -> int -> int.
  
  Parameter get_uint16 : bytes -> int -> int.
End TzEndian_signature.
Require Export Proto_2021_01.Environment.TzEndian.
Module TzEndian_check : TzEndian_signature := TzEndian.

Module Type RPC_arg_signature.
  Parameter t : forall (a : Set), Set.
  
  Definition arg (a : Set) : Set := t a.
  
  Parameter make : forall {a : Set},
    option string -> string -> (string -> Pervasives.result a string) ->
    (a -> string) -> unit -> arg a.
  
  Module descr.
    Record record : Set := Build {
      name : string;
      descr : option string }.
    Definition with_name name (r : record) :=
      Build name r.(descr).
    Definition with_descr descr (r : record) :=
      Build r.(name) descr.
  End descr.
  Definition descr := descr.record.
  
  Parameter descr_value : forall {a : Set}, arg a -> descr.
  
  Parameter bool_value : arg bool.
  
  Parameter int_value : arg int.
  
  Parameter int32_value : arg int32.
  
  Parameter int64_value : arg int64.
  
  Parameter string_value : arg string.
  
  Parameter like : forall {a : Set}, arg a -> option string -> string -> arg a.
  
  Inductive eq (a : Set) : Set :=
  | Eq : eq a.
  
  Arguments Eq {_}.
  
  Parameter eq_value : forall {a b : Set}, arg a -> arg b -> option (eq a).
End RPC_arg_signature.
Require Export Proto_2021_01.Environment.RPC_arg.
Module RPC_arg_check : RPC_arg_signature := RPC_arg.

Module Type RPC_path_signature.
  Parameter t : forall (prefix params : Set), Set.
  
  Definition path (prefix params : Set) : Set := t prefix params.
  
  Definition context (prefix : Set) : Set := path prefix prefix.
  
  Parameter root : context unit.
  
  Parameter open_root : forall {a : Set}, context a.
  
  Parameter add_suffix : forall {params prefix : Set},
    path prefix params -> string -> path prefix params.
  
  Parameter op_div : forall {params prefix : Set},
    path prefix params -> string -> path prefix params.
  
  Parameter add_arg : forall {a params prefix : Set},
    path prefix params -> RPC_arg.t a -> path prefix (params * a).
  
  Parameter op_divcolon : forall {a params prefix : Set},
    path prefix params -> RPC_arg.t a -> path prefix (params * a).
  
  Parameter add_final_args : forall {a params prefix : Set},
    path prefix params -> RPC_arg.t a -> path prefix (params * list a).
  
  Parameter op_divcolonstar : forall {a params prefix : Set},
    path prefix params -> RPC_arg.t a -> path prefix (params * list a).
End RPC_path_signature.
Require Export Proto_2021_01.Environment.RPC_path.
Module RPC_path_check : RPC_path_signature := RPC_path.

Module Type RPC_query_signature.
  Parameter t : forall (a : Set), Set.
  
  Definition query (a : Set) : Set := t a.
  
  Parameter empty : query unit.
  
  Parameter field : forall (a b : Set), Set.
  
  Parameter field_value : forall {a b : Set},
    option string -> string -> RPC_arg.t a -> a -> (b -> a) -> field b a.
  
  Parameter opt_field : forall {a b : Set},
    option string -> string -> RPC_arg.t a -> (b -> option a) ->
    field b (option a).
  
  Parameter flag : forall {b : Set},
    option string -> string -> (b -> bool) -> field b bool.
  
  Parameter multi_field : forall {a b : Set},
    option string -> string -> RPC_arg.t a -> (b -> list a) -> field b (list a).
  
  Parameter open_query : forall (a b c : Set), Set.
  
  Parameter query_value : forall {a b : Set}, b -> open_query a b b.
  
  Parameter op_pipeplus : forall {a b c d : Set},
    open_query a b (c -> d) -> field a c -> open_query a b d.
  
  Parameter seal : forall {a b : Set}, open_query a b a -> t a.
  
  Definition untyped : Set := list (string * string).
  
  Parameter parse : forall {a : Set}, query a -> untyped -> a.
End RPC_query_signature.
Require Export Proto_2021_01.Environment.RPC_query.
Module RPC_query_check : RPC_query_signature := RPC_query.

Module Type RPC_service_signature.
  Inductive meth : Set :=
  | PUT : meth
  | GET : meth
  | DELETE : meth
  | POST : meth
  | PATCH : meth.
  
  Parameter t : forall (prefix params query input output : Set), Set.
  
  Definition service (prefix params query input output : Set) : Set :=
    t prefix params query input output.
  
  Parameter get_service : forall {output params prefix query : Set},
    option string -> RPC_query.t query -> Data_encoding.t output ->
    RPC_path.t prefix params -> service prefix params query unit output.
  
  Parameter post_service : forall {input output params prefix query : Set},
    option string -> RPC_query.t query -> Data_encoding.t input ->
    Data_encoding.t output -> RPC_path.t prefix params ->
    service prefix params query input output.
  
  Parameter delete_service : forall {output params prefix query : Set},
    option string -> RPC_query.t query -> Data_encoding.t output ->
    RPC_path.t prefix params -> service prefix params query unit output.
  
  Parameter patch_service : forall {input output params prefix query : Set},
    option string -> RPC_query.t query -> Data_encoding.t input ->
    Data_encoding.t output -> RPC_path.t prefix params ->
    service prefix params query input output.
  
  Parameter put_service : forall {input output params prefix query : Set},
    option string -> RPC_query.t query -> Data_encoding.t input ->
    Data_encoding.t output -> RPC_path.t prefix params ->
    service prefix params query input output.
End RPC_service_signature.
Require Export Proto_2021_01.Environment.RPC_service.
Module RPC_service_check : RPC_service_signature := RPC_service.

Module Type RPC_answer_signature.
  Module stream.
    Record record {next shutdown : Set} : Set := Build {
      next : next;
      shutdown : shutdown }.
    Arguments record : clear implicits.
    Definition with_next {t_next t_shutdown} next
      (r : record t_next t_shutdown) :=
      Build t_next t_shutdown next r.(shutdown).
    Definition with_shutdown {t_next t_shutdown} shutdown
      (r : record t_next t_shutdown) :=
      Build t_next t_shutdown r.(next) shutdown.
  End stream.
  Definition stream_skeleton := stream.record.
  
  Reserved Notation "'stream".
  
  Inductive t (o : Set) : Set :=
  | OkStream : 'stream o -> t o
  | Unauthorized : option (list Error_monad._error) -> t o
  | Error : option (list Error_monad._error) -> t o
  | Ok : o -> t o
  | Not_found : option (list Error_monad._error) -> t o
  | Forbidden : option (list Error_monad._error) -> t o
  | Created : option string -> t o
  | Conflict : option (list Error_monad._error) -> t o
  | No_content : t o
  
  where "'stream" := (fun (t_a : Set) =>
    stream_skeleton (unit -> Lwt.t (option t_a)) (unit -> unit)).
  
  Definition stream := 'stream.
  
  Arguments OkStream {_}.
  Arguments Unauthorized {_}.
  Arguments Error {_}.
  Arguments Ok {_}.
  Arguments Not_found {_}.
  Arguments Forbidden {_}.
  Arguments Created {_}.
  Arguments Conflict {_}.
  Arguments No_content {_}.
  
  Parameter _return : forall {o : Set}, o -> Lwt.t (t o).
  
  Parameter return_stream : forall {o : Set}, stream o -> Lwt.t (t o).
  
  Parameter not_found : forall {o : Set}, Lwt.t (t o).
  
  Parameter fail : forall {a : Set}, list Error_monad._error -> Lwt.t (t a).
End RPC_answer_signature.
Require Export Proto_2021_01.Environment.RPC_answer.
Module RPC_answer_check : RPC_answer_signature := RPC_answer.

Module Type RPC_directory_signature.
  Parameter t : forall (prefix : Set), Set.
  
  Definition directory (prefix : Set) : Set := t prefix.
  
  Parameter empty : forall {prefix : Set}, directory prefix.
  
  Parameter map : forall {a b : Set},
    (a -> Lwt.t b) -> directory b -> directory a.
  
  Parameter prefix : forall {p pr : Set},
    RPC_path.path pr p -> directory p -> directory pr.
  
  Parameter merge : forall {a : Set}, directory a -> directory a -> directory a.
  
  Inductive step : Set :=
  | Static : string -> step
  | Dynamic : RPC_arg.descr -> step
  | DynamicTail : RPC_arg.descr -> step.
  
  Inductive conflict : Set :=
  | CService : RPC_service.meth -> conflict
  | CDir : conflict
  | CBuilder : conflict
  | CTail : conflict
  | CTypes : RPC_arg.descr -> RPC_arg.descr -> conflict
  | CType : RPC_arg.descr -> list string -> conflict.
  
  Parameter register : forall {input output params prefix query : Set},
    directory prefix -> RPC_service.t prefix params query input output ->
    (params -> query -> input -> Lwt.t (Error_monad.tzresult output)) ->
    directory prefix.
  
  Parameter opt_register : forall {input output params prefix query : Set},
    directory prefix -> RPC_service.t prefix params query input output ->
    (params -> query -> input -> Lwt.t (Error_monad.tzresult (option output)))
    -> directory prefix.
  
  Parameter gen_register : forall {input output params prefix query : Set},
    directory prefix -> RPC_service.t prefix params query input output ->
    (params -> query -> input ->
    Lwt.t
      ((* `OkStream *) RPC_answer.stream output +
        (* `Unauthorized *) option (list Error_monad._error) +
        (* `Error *) option (list Error_monad._error) + (* `Ok *) output +
        (* `Not_found *) option (list Error_monad._error) +
        (* `Forbidden *) option (list Error_monad._error) +
        (* `Created *) option string +
        (* `Conflict *) option (list Error_monad._error) +
        (* `No_content *) unit)) -> directory prefix.
  
  Parameter lwt_register : forall {input output params prefix query : Set},
    directory prefix -> RPC_service.t prefix params query input output ->
    (params -> query -> input -> Lwt.t output) -> directory prefix.
  
  Parameter register0 : forall {i o q : Set},
    directory unit -> RPC_service.t unit unit q i o ->
    (q -> i -> Lwt.t (Error_monad.tzresult o)) -> directory unit.
  
  Parameter register1 : forall {a i o prefix q : Set},
    directory prefix -> RPC_service.t prefix (unit * a) q i o ->
    (a -> q -> i -> Lwt.t (Error_monad.tzresult o)) -> directory prefix.
  
  Parameter register2 : forall {a b i o prefix q : Set},
    directory prefix -> RPC_service.t prefix ((unit * a) * b) q i o ->
    (a -> b -> q -> i -> Lwt.t (Error_monad.tzresult o)) -> directory prefix.
  
  Parameter register3 : forall {a b c i o prefix q : Set},
    directory prefix -> RPC_service.t prefix (((unit * a) * b) * c) q i o ->
    (a -> b -> c -> q -> i -> Lwt.t (Error_monad.tzresult o)) ->
    directory prefix.
  
  Parameter register4 : forall {a b c d i o prefix q : Set},
    directory prefix ->
    RPC_service.t prefix ((((unit * a) * b) * c) * d) q i o ->
    (a -> b -> c -> d -> q -> i -> Lwt.t (Error_monad.tzresult o)) ->
    directory prefix.
  
  Parameter register5 : forall {a b c d e i o prefix q : Set},
    directory prefix ->
    RPC_service.t prefix (((((unit * a) * b) * c) * d) * e) q i o ->
    (a -> b -> c -> d -> e -> q -> i -> Lwt.t (Error_monad.tzresult o)) ->
    directory prefix.
  
  Parameter opt_register0 : forall {i o q : Set},
    directory unit -> RPC_service.t unit unit q i o ->
    (q -> i -> Lwt.t (Error_monad.tzresult (option o))) -> directory unit.
  
  Parameter opt_register1 : forall {a i o prefix q : Set},
    directory prefix -> RPC_service.t prefix (unit * a) q i o ->
    (a -> q -> i -> Lwt.t (Error_monad.tzresult (option o))) -> directory prefix.
  
  Parameter opt_register2 : forall {a b i o prefix q : Set},
    directory prefix -> RPC_service.t prefix ((unit * a) * b) q i o ->
    (a -> b -> q -> i -> Lwt.t (Error_monad.tzresult (option o))) ->
    directory prefix.
  
  Parameter opt_register3 : forall {a b c i o prefix q : Set},
    directory prefix -> RPC_service.t prefix (((unit * a) * b) * c) q i o ->
    (a -> b -> c -> q -> i -> Lwt.t (Error_monad.tzresult (option o))) ->
    directory prefix.
  
  Parameter opt_register4 : forall {a b c d i o prefix q : Set},
    directory prefix ->
    RPC_service.t prefix ((((unit * a) * b) * c) * d) q i o ->
    (a -> b -> c -> d -> q -> i -> Lwt.t (Error_monad.tzresult (option o))) ->
    directory prefix.
  
  Parameter opt_register5 : forall {a b c d e i o prefix q : Set},
    directory prefix ->
    RPC_service.t prefix (((((unit * a) * b) * c) * d) * e) q i o ->
    (a -> b -> c -> d -> e -> q -> i -> Lwt.t (Error_monad.tzresult (option o)))
    -> directory prefix.
  
  Parameter gen_register0 : forall {i o q : Set},
    directory unit -> RPC_service.t unit unit q i o ->
    (q -> i ->
    Lwt.t
      ((* `OkStream *) RPC_answer.stream o +
        (* `Unauthorized *) option (list Error_monad._error) +
        (* `Error *) option (list Error_monad._error) + (* `Ok *) o +
        (* `Not_found *) option (list Error_monad._error) +
        (* `Forbidden *) option (list Error_monad._error) +
        (* `Created *) option string +
        (* `Conflict *) option (list Error_monad._error) +
        (* `No_content *) unit)) -> directory unit.
  
  Parameter gen_register1 : forall {a i o prefix q : Set},
    directory prefix -> RPC_service.t prefix (unit * a) q i o ->
    (a -> q -> i ->
    Lwt.t
      ((* `OkStream *) RPC_answer.stream o +
        (* `Unauthorized *) option (list Error_monad._error) +
        (* `Error *) option (list Error_monad._error) + (* `Ok *) o +
        (* `Not_found *) option (list Error_monad._error) +
        (* `Forbidden *) option (list Error_monad._error) +
        (* `Created *) option string +
        (* `Conflict *) option (list Error_monad._error) +
        (* `No_content *) unit)) -> directory prefix.
  
  Parameter gen_register2 : forall {a b i o prefix q : Set},
    directory prefix -> RPC_service.t prefix ((unit * a) * b) q i o ->
    (a -> b -> q -> i ->
    Lwt.t
      ((* `OkStream *) RPC_answer.stream o +
        (* `Unauthorized *) option (list Error_monad._error) +
        (* `Error *) option (list Error_monad._error) + (* `Ok *) o +
        (* `Not_found *) option (list Error_monad._error) +
        (* `Forbidden *) option (list Error_monad._error) +
        (* `Created *) option string +
        (* `Conflict *) option (list Error_monad._error) +
        (* `No_content *) unit)) -> directory prefix.
  
  Parameter gen_register3 : forall {a b c i o prefix q : Set},
    directory prefix -> RPC_service.t prefix (((unit * a) * b) * c) q i o ->
    (a -> b -> c -> q -> i ->
    Lwt.t
      ((* `OkStream *) RPC_answer.stream o +
        (* `Unauthorized *) option (list Error_monad._error) +
        (* `Error *) option (list Error_monad._error) + (* `Ok *) o +
        (* `Not_found *) option (list Error_monad._error) +
        (* `Forbidden *) option (list Error_monad._error) +
        (* `Created *) option string +
        (* `Conflict *) option (list Error_monad._error) +
        (* `No_content *) unit)) -> directory prefix.
  
  Parameter gen_register4 : forall {a b c d i o prefix q : Set},
    directory prefix ->
    RPC_service.t prefix ((((unit * a) * b) * c) * d) q i o ->
    (a -> b -> c -> d -> q -> i ->
    Lwt.t
      ((* `OkStream *) RPC_answer.stream o +
        (* `Unauthorized *) option (list Error_monad._error) +
        (* `Error *) option (list Error_monad._error) + (* `Ok *) o +
        (* `Not_found *) option (list Error_monad._error) +
        (* `Forbidden *) option (list Error_monad._error) +
        (* `Created *) option string +
        (* `Conflict *) option (list Error_monad._error) +
        (* `No_content *) unit)) -> directory prefix.
  
  Parameter gen_register5 : forall {a b c d e i o prefix q : Set},
    directory prefix ->
    RPC_service.t prefix (((((unit * a) * b) * c) * d) * e) q i o ->
    (a -> b -> c -> d -> e -> q -> i ->
    Lwt.t
      ((* `OkStream *) RPC_answer.stream o +
        (* `Unauthorized *) option (list Error_monad._error) +
        (* `Error *) option (list Error_monad._error) + (* `Ok *) o +
        (* `Not_found *) option (list Error_monad._error) +
        (* `Forbidden *) option (list Error_monad._error) +
        (* `Created *) option string +
        (* `Conflict *) option (list Error_monad._error) +
        (* `No_content *) unit)) -> directory prefix.
  
  Parameter lwt_register0 : forall {i o q : Set},
    directory unit -> RPC_service.t unit unit q i o -> (q -> i -> Lwt.t o) ->
    directory unit.
  
  Parameter lwt_register1 : forall {a i o prefix q : Set},
    directory prefix -> RPC_service.t prefix (unit * a) q i o ->
    (a -> q -> i -> Lwt.t o) -> directory prefix.
  
  Parameter lwt_register2 : forall {a b i o prefix q : Set},
    directory prefix -> RPC_service.t prefix ((unit * a) * b) q i o ->
    (a -> b -> q -> i -> Lwt.t o) -> directory prefix.
  
  Parameter lwt_register3 : forall {a b c i o prefix q : Set},
    directory prefix -> RPC_service.t prefix (((unit * a) * b) * c) q i o ->
    (a -> b -> c -> q -> i -> Lwt.t o) -> directory prefix.
  
  Parameter lwt_register4 : forall {a b c d i o prefix q : Set},
    directory prefix ->
    RPC_service.t prefix ((((unit * a) * b) * c) * d) q i o ->
    (a -> b -> c -> d -> q -> i -> Lwt.t o) -> directory prefix.
  
  Parameter lwt_register5 : forall {a b c d e i o prefix q : Set},
    directory prefix ->
    RPC_service.t prefix (((((unit * a) * b) * c) * d) * e) q i o ->
    (a -> b -> c -> d -> e -> q -> i -> Lwt.t o) -> directory prefix.
  
  Parameter register_dynamic_directory : forall {a prefix : Set},
    option string -> directory prefix -> RPC_path.t prefix a ->
    (a -> Lwt.t (directory a)) -> directory prefix.
End RPC_directory_signature.
Require Export Proto_2021_01.Environment.RPC_directory.
Module RPC_directory_check : RPC_directory_signature := RPC_directory.

Module Type Base58_signature.
  Parameter encoding : forall (a : Set), Set.
  
  Parameter simple_decode : forall {a : Set}, encoding a -> string -> option a.
  
  Parameter simple_encode : forall {a : Set}, encoding a -> a -> string.
  
  Definition data := extensible_type.
  
  Parameter register_encoding : forall {a : Set},
    string -> int -> (a -> string) -> (string -> option a) -> (a -> data) ->
    encoding a.
  
  Parameter check_encoded_prefix : forall {a : Set},
    encoding a -> string -> int -> unit.
  
  Parameter decode : string -> option data.
End Base58_signature.
Require Export Proto_2021_01.Environment.Base58.
Module Base58_check : Base58_signature := Base58.

Module Type S_signature.
  Module T.
    Record signature {t : Set} : Set := {
      t := t;
      op_eq : t -> t -> bool;
      op_ltgt : t -> t -> bool;
      op_lt : t -> t -> bool;
      op_lteq : t -> t -> bool;
      op_gteq : t -> t -> bool;
      op_gt : t -> t -> bool;
      compare : t -> t -> int;
      equal : t -> t -> bool;
      max : t -> t -> t;
      min : t -> t -> t;
      pp : Format.formatter -> t -> unit;
      encoding : Data_encoding.t t;
      to_bytes : t -> bytes;
      of_bytes : bytes -> option t;
    }.
  End T.
  Definition T := @T.signature.
  Arguments T {_}.
  
  Module HASHABLE.
    Record signature {t hash : Set} : Set := {
      t := t;
      op_eq : t -> t -> bool;
      op_ltgt : t -> t -> bool;
      op_lt : t -> t -> bool;
      op_lteq : t -> t -> bool;
      op_gteq : t -> t -> bool;
      op_gt : t -> t -> bool;
      compare : t -> t -> int;
      equal : t -> t -> bool;
      max : t -> t -> t;
      min : t -> t -> t;
      pp : Format.formatter -> t -> unit;
      encoding : Data_encoding.t t;
      to_bytes : t -> bytes;
      of_bytes : bytes -> option t;
      hash := hash;
      hash_value : t -> hash;
      hash_raw : bytes -> hash;
    }.
  End HASHABLE.
  Definition HASHABLE := @HASHABLE.signature.
  Arguments HASHABLE {_ _}.
  
  Module MINIMAL_HASH.
    Record signature {t : Set} : Set := {
      t := t;
      name : string;
      title : string;
      pp : Format.formatter -> t -> unit;
      pp_short : Format.formatter -> t -> unit;
      op_eq : t -> t -> bool;
      op_ltgt : t -> t -> bool;
      op_lt : t -> t -> bool;
      op_lteq : t -> t -> bool;
      op_gteq : t -> t -> bool;
      op_gt : t -> t -> bool;
      compare : t -> t -> int;
      equal : t -> t -> bool;
      max : t -> t -> t;
      min : t -> t -> t;
      hash_bytes : option bytes -> list bytes -> t;
      hash_string : option string -> list string -> t;
      zero : t;
    }.
  End MINIMAL_HASH.
  Definition MINIMAL_HASH := @MINIMAL_HASH.signature.
  Arguments MINIMAL_HASH {_}.
  
  Module RAW_DATA.
    Record signature {t : Set} : Set := {
      t := t;
      size : int;
      to_bytes : t -> bytes;
      of_bytes_opt : bytes -> option t;
      of_bytes_exn : bytes -> t;
    }.
  End RAW_DATA.
  Definition RAW_DATA := @RAW_DATA.signature.
  Arguments RAW_DATA {_}.
  
  Module B58_DATA.
    Record signature {t : Set} : Set := {
      t := t;
      to_b58check : t -> string;
      to_short_b58check : t -> string;
      of_b58check_exn : string -> t;
      of_b58check_opt : string -> option t;
      b58check_encoding : Base58.encoding t;
    }.
  End B58_DATA.
  Definition B58_DATA := @B58_DATA.signature.
  Arguments B58_DATA {_}.
  
  Module ENCODER.
    Record signature {t : Set} : Set := {
      t := t;
      encoding : Data_encoding.t t;
      rpc_arg : RPC_arg.t t;
    }.
  End ENCODER.
  Definition ENCODER := @ENCODER.signature.
  Arguments ENCODER {_}.
  
  Module SET.
    Record signature {elt t : Set} : Set := {
      elt := elt;
      t := t;
      empty : t;
      is_empty : t -> bool;
      mem : elt -> t -> bool;
      add : elt -> t -> t;
      singleton : elt -> t;
      remove : elt -> t -> t;
      union : t -> t -> t;
      inter : t -> t -> t;
      diff_value : t -> t -> t;
      compare : t -> t -> int;
      equal : t -> t -> bool;
      subset : t -> t -> bool;
      iter : (elt -> unit) -> t -> unit;
      map : (elt -> elt) -> t -> t;
      fold : forall {a : Set}, (elt -> a -> a) -> t -> a -> a;
      for_all : (elt -> bool) -> t -> bool;
      _exists : (elt -> bool) -> t -> bool;
      filter : (elt -> bool) -> t -> t;
      partition : (elt -> bool) -> t -> t * t;
      cardinal : t -> int;
      elements : t -> list elt;
      min_elt_opt : t -> option elt;
      max_elt_opt : t -> option elt;
      choose_opt : t -> option elt;
      split : elt -> t -> t * bool * t;
      find_opt : elt -> t -> option elt;
      find_first_opt : (elt -> bool) -> t -> option elt;
      find_last_opt : (elt -> bool) -> t -> option elt;
      of_list : list elt -> t;
    }.
  End SET.
  Definition SET := @SET.signature.
  Arguments SET {_ _}.
  
  Module MAP.
    Record signature {key : Set} {t : Set -> Set} : Set := {
      key := key;
      t := t;
      empty : forall {a : Set}, t a;
      is_empty : forall {a : Set}, t a -> bool;
      mem : forall {a : Set}, key -> t a -> bool;
      add : forall {a : Set}, key -> a -> t a -> t a;
      update : forall {a : Set}, key -> (option a -> option a) -> t a -> t a;
      singleton : forall {a : Set}, key -> a -> t a;
      remove : forall {a : Set}, key -> t a -> t a;
      merge :
        forall {a b c : Set},
        (key -> option a -> option b -> option c) -> t a -> t b -> t c;
      union :
        forall {a : Set}, (key -> a -> a -> option a) -> t a -> t a -> t a;
      compare : forall {a : Set}, (a -> a -> int) -> t a -> t a -> int;
      equal : forall {a : Set}, (a -> a -> bool) -> t a -> t a -> bool;
      iter : forall {a : Set}, (key -> a -> unit) -> t a -> unit;
      fold : forall {a b : Set}, (key -> a -> b -> b) -> t a -> b -> b;
      for_all : forall {a : Set}, (key -> a -> bool) -> t a -> bool;
      _exists : forall {a : Set}, (key -> a -> bool) -> t a -> bool;
      filter : forall {a : Set}, (key -> a -> bool) -> t a -> t a;
      partition : forall {a : Set}, (key -> a -> bool) -> t a -> t a * t a;
      cardinal : forall {a : Set}, t a -> int;
      bindings : forall {a : Set}, t a -> list (key * a);
      min_binding_opt : forall {a : Set}, t a -> option (key * a);
      max_binding_opt : forall {a : Set}, t a -> option (key * a);
      choose_opt : forall {a : Set}, t a -> option (key * a);
      split : forall {a : Set}, key -> t a -> t a * option a * t a;
      find_opt : forall {a : Set}, key -> t a -> option a;
      find_first_opt :
        forall {a : Set}, (key -> bool) -> t a -> option (key * a);
      find_last_opt :
        forall {a : Set}, (key -> bool) -> t a -> option (key * a);
      map : forall {a b : Set}, (a -> b) -> t a -> t b;
      mapi : forall {a b : Set}, (key -> a -> b) -> t a -> t b;
    }.
  End MAP.
  Definition MAP := @MAP.signature.
  Arguments MAP {_ _}.
  
  Module INDEXES_SET.
    Record signature {elt t : Set} : Set := {
      elt := elt;
      t := t;
      empty : t;
      is_empty : t -> bool;
      mem : elt -> t -> bool;
      add : elt -> t -> t;
      singleton : elt -> t;
      remove : elt -> t -> t;
      union : t -> t -> t;
      inter : t -> t -> t;
      diff_value : t -> t -> t;
      compare : t -> t -> int;
      equal : t -> t -> bool;
      subset : t -> t -> bool;
      iter : (elt -> unit) -> t -> unit;
      map : (elt -> elt) -> t -> t;
      fold : forall {a : Set}, (elt -> a -> a) -> t -> a -> a;
      for_all : (elt -> bool) -> t -> bool;
      _exists : (elt -> bool) -> t -> bool;
      filter : (elt -> bool) -> t -> t;
      partition : (elt -> bool) -> t -> t * t;
      cardinal : t -> int;
      elements : t -> list elt;
      min_elt_opt : t -> option elt;
      max_elt_opt : t -> option elt;
      choose_opt : t -> option elt;
      split : elt -> t -> t * bool * t;
      find_opt : elt -> t -> option elt;
      find_first_opt : (elt -> bool) -> t -> option elt;
      find_last_opt : (elt -> bool) -> t -> option elt;
      of_list : list elt -> t;
      encoding : Data_encoding.t t;
    }.
  End INDEXES_SET.
  Definition INDEXES_SET := @INDEXES_SET.signature.
  Arguments INDEXES_SET {_ _}.
  
  Module INDEXES_MAP.
    Record signature {key : Set} {t : Set -> Set} : Set := {
      key := key;
      t := t;
      empty : forall {a : Set}, t a;
      is_empty : forall {a : Set}, t a -> bool;
      mem : forall {a : Set}, key -> t a -> bool;
      add : forall {a : Set}, key -> a -> t a -> t a;
      update : forall {a : Set}, key -> (option a -> option a) -> t a -> t a;
      singleton : forall {a : Set}, key -> a -> t a;
      remove : forall {a : Set}, key -> t a -> t a;
      merge :
        forall {a b c : Set},
        (key -> option a -> option b -> option c) -> t a -> t b -> t c;
      union :
        forall {a : Set}, (key -> a -> a -> option a) -> t a -> t a -> t a;
      compare : forall {a : Set}, (a -> a -> int) -> t a -> t a -> int;
      equal : forall {a : Set}, (a -> a -> bool) -> t a -> t a -> bool;
      iter : forall {a : Set}, (key -> a -> unit) -> t a -> unit;
      fold : forall {a b : Set}, (key -> a -> b -> b) -> t a -> b -> b;
      for_all : forall {a : Set}, (key -> a -> bool) -> t a -> bool;
      _exists : forall {a : Set}, (key -> a -> bool) -> t a -> bool;
      filter : forall {a : Set}, (key -> a -> bool) -> t a -> t a;
      partition : forall {a : Set}, (key -> a -> bool) -> t a -> t a * t a;
      cardinal : forall {a : Set}, t a -> int;
      bindings : forall {a : Set}, t a -> list (key * a);
      min_binding_opt : forall {a : Set}, t a -> option (key * a);
      max_binding_opt : forall {a : Set}, t a -> option (key * a);
      choose_opt : forall {a : Set}, t a -> option (key * a);
      split : forall {a : Set}, key -> t a -> t a * option a * t a;
      find_opt : forall {a : Set}, key -> t a -> option a;
      find_first_opt :
        forall {a : Set}, (key -> bool) -> t a -> option (key * a);
      find_last_opt :
        forall {a : Set}, (key -> bool) -> t a -> option (key * a);
      map : forall {a b : Set}, (a -> b) -> t a -> t b;
      mapi : forall {a b : Set}, (key -> a -> b) -> t a -> t b;
      encoding : forall {a : Set}, Data_encoding.t a -> Data_encoding.t (t a);
    }.
  End INDEXES_MAP.
  Definition INDEXES_MAP := @INDEXES_MAP.signature.
  Arguments INDEXES_MAP {_ _}.
  
  Module INDEXES.
    Record signature {t Set_t : Set} {Map_t : Set -> Set} : Set := {
      t := t;
      to_path : t -> list string -> list string;
      of_path : list string -> option t;
      of_path_exn : list string -> t;
      prefix_path : string -> list string;
      path_length : int;
      _Set : INDEXES_SET (elt := t) (t := Set_t);
      Map : INDEXES_MAP (key := t) (t := Map_t);
    }.
  End INDEXES.
  Definition INDEXES := @INDEXES.signature.
  Arguments INDEXES {_ _ _}.
  
  Module HASH.
    Record signature {t Set_t : Set} {Map_t : Set -> Set} : Set := {
      t := t;
      name : string;
      title : string;
      pp : Format.formatter -> t -> unit;
      pp_short : Format.formatter -> t -> unit;
      op_eq : t -> t -> bool;
      op_ltgt : t -> t -> bool;
      op_lt : t -> t -> bool;
      op_lteq : t -> t -> bool;
      op_gteq : t -> t -> bool;
      op_gt : t -> t -> bool;
      compare : t -> t -> int;
      equal : t -> t -> bool;
      max : t -> t -> t;
      min : t -> t -> t;
      hash_bytes : option bytes -> list bytes -> t;
      hash_string : option string -> list string -> t;
      zero : t;
      size : int;
      to_bytes : t -> bytes;
      of_bytes_opt : bytes -> option t;
      of_bytes_exn : bytes -> t;
      to_b58check : t -> string;
      to_short_b58check : t -> string;
      of_b58check_exn : string -> t;
      of_b58check_opt : string -> option t;
      (* extensible_type_definition `Base58.data` *)
      b58check_encoding : Base58.encoding t;
      encoding : Data_encoding.t t;
      rpc_arg : RPC_arg.t t;
      to_path : t -> list string -> list string;
      of_path : list string -> option t;
      of_path_exn : list string -> t;
      prefix_path : string -> list string;
      path_length : int;
      _Set : INDEXES_SET (elt := t) (t := Set_t);
      Map : INDEXES_MAP (key := t) (t := Map_t);
    }.
  End HASH.
  Definition HASH := @HASH.signature.
  Arguments HASH {_ _ _}.
  
  Module MERKLE_TREE.
    Record signature {elt t Set_t : Set} {Map_t : Set -> Set} {path : Set} : Set
      := {
      elt := elt;
      t := t;
      name : string;
      title : string;
      pp : Format.formatter -> t -> unit;
      pp_short : Format.formatter -> t -> unit;
      op_eq : t -> t -> bool;
      op_ltgt : t -> t -> bool;
      op_lt : t -> t -> bool;
      op_lteq : t -> t -> bool;
      op_gteq : t -> t -> bool;
      op_gt : t -> t -> bool;
      compare : t -> t -> int;
      equal : t -> t -> bool;
      max : t -> t -> t;
      min : t -> t -> t;
      hash_bytes : option bytes -> list bytes -> t;
      hash_string : option string -> list string -> t;
      zero : t;
      size : int;
      to_bytes : t -> bytes;
      of_bytes_opt : bytes -> option t;
      of_bytes_exn : bytes -> t;
      to_b58check : t -> string;
      to_short_b58check : t -> string;
      of_b58check_exn : string -> t;
      of_b58check_opt : string -> option t;
      (* extensible_type_definition `Base58.data` *)
      b58check_encoding : Base58.encoding t;
      encoding : Data_encoding.t t;
      rpc_arg : RPC_arg.t t;
      to_path : t -> list string -> list string;
      of_path : list string -> option t;
      of_path_exn : list string -> t;
      prefix_path : string -> list string;
      path_length : int;
      _Set : INDEXES_SET (elt := t) (t := Set_t);
      Map : INDEXES_MAP (key := t) (t := Map_t);
      compute : list elt -> t;
      empty : t;
      path := path;
      compute_path : list elt -> int -> path;
      check_path : path -> elt -> t * int;
      path_encoding : Data_encoding.t path;
    }.
  End MERKLE_TREE.
  Definition MERKLE_TREE := @MERKLE_TREE.signature.
  Arguments MERKLE_TREE {_ _ _ _ _}.
  
  Module SIGNATURE_PUBLIC_KEY_HASH.
    Record signature {t Set_t : Set} {Map_t : Set -> Set} : Set := {
      t := t;
      pp : Format.formatter -> t -> unit;
      pp_short : Format.formatter -> t -> unit;
      op_eq : t -> t -> bool;
      op_ltgt : t -> t -> bool;
      op_lt : t -> t -> bool;
      op_lteq : t -> t -> bool;
      op_gteq : t -> t -> bool;
      op_gt : t -> t -> bool;
      compare : t -> t -> int;
      equal : t -> t -> bool;
      max : t -> t -> t;
      min : t -> t -> t;
      size : int;
      to_bytes : t -> bytes;
      of_bytes_opt : bytes -> option t;
      of_bytes_exn : bytes -> t;
      to_b58check : t -> string;
      to_short_b58check : t -> string;
      of_b58check_exn : string -> t;
      of_b58check_opt : string -> option t;
      (* extensible_type_definition `Base58.data` *)
      b58check_encoding : Base58.encoding t;
      encoding : Data_encoding.t t;
      rpc_arg : RPC_arg.t t;
      to_path : t -> list string -> list string;
      of_path : list string -> option t;
      of_path_exn : list string -> t;
      prefix_path : string -> list string;
      path_length : int;
      _Set : INDEXES_SET (elt := t) (t := Set_t);
      Map : INDEXES_MAP (key := t) (t := Map_t);
      zero : t;
    }.
  End SIGNATURE_PUBLIC_KEY_HASH.
  Definition SIGNATURE_PUBLIC_KEY_HASH := @SIGNATURE_PUBLIC_KEY_HASH.signature.
  Arguments SIGNATURE_PUBLIC_KEY_HASH {_ _ _}.
  
  Module SIGNATURE_PUBLIC_KEY.
    Record signature {t public_key_hash_t : Set} : Set := {
      t := t;
      pp : Format.formatter -> t -> unit;
      op_eq : t -> t -> bool;
      op_ltgt : t -> t -> bool;
      op_lt : t -> t -> bool;
      op_lteq : t -> t -> bool;
      op_gteq : t -> t -> bool;
      op_gt : t -> t -> bool;
      compare : t -> t -> int;
      equal : t -> t -> bool;
      max : t -> t -> t;
      min : t -> t -> t;
      to_b58check : t -> string;
      to_short_b58check : t -> string;
      of_b58check_exn : string -> t;
      of_b58check_opt : string -> option t;
      (* extensible_type_definition `Base58.data` *)
      b58check_encoding : Base58.encoding t;
      encoding : Data_encoding.t t;
      rpc_arg : RPC_arg.t t;
      public_key_hash_t := public_key_hash_t;
      hash_value : t -> public_key_hash_t;
      size : t -> int;
    }.
  End SIGNATURE_PUBLIC_KEY.
  Definition SIGNATURE_PUBLIC_KEY := @SIGNATURE_PUBLIC_KEY.signature.
  Arguments SIGNATURE_PUBLIC_KEY {_ _}.
  
  Module SIGNATURE.
    Record signature {Public_key_hash_t Public_key_hash_Set_t : Set}
      {Public_key_hash_Map_t : Set -> Set} {Public_key_t t watermark : Set}
      : Set := {
      Public_key_hash :
        SIGNATURE_PUBLIC_KEY_HASH (t := Public_key_hash_t)
          (Set_t := Public_key_hash_Set_t) (Map_t := Public_key_hash_Map_t);
      Public_key :
        SIGNATURE_PUBLIC_KEY (t := Public_key_t)
          (public_key_hash_t := Public_key_hash.(SIGNATURE_PUBLIC_KEY_HASH.t));
      t := t;
      pp : Format.formatter -> t -> unit;
      size : int;
      to_bytes : t -> bytes;
      of_bytes_opt : bytes -> option t;
      of_bytes_exn : bytes -> t;
      op_eq : t -> t -> bool;
      op_ltgt : t -> t -> bool;
      op_lt : t -> t -> bool;
      op_lteq : t -> t -> bool;
      op_gteq : t -> t -> bool;
      op_gt : t -> t -> bool;
      compare : t -> t -> int;
      equal : t -> t -> bool;
      max : t -> t -> t;
      min : t -> t -> t;
      to_b58check : t -> string;
      to_short_b58check : t -> string;
      of_b58check_exn : string -> t;
      of_b58check_opt : string -> option t;
      (* extensible_type_definition `Base58.data` *)
      b58check_encoding : Base58.encoding t;
      encoding : Data_encoding.t t;
      rpc_arg : RPC_arg.t t;
      zero : t;
      watermark := watermark;
      (** Check a signature *)
      check :
        option watermark -> Public_key.(SIGNATURE_PUBLIC_KEY.t) -> t -> bytes ->
        bool;
    }.
  End SIGNATURE.
  Definition SIGNATURE := @SIGNATURE.signature.
  Arguments SIGNATURE {_ _ _ _ _ _}.
  
  Module FIELD.
    Record signature {t : Set} : Set := {
      t := t;
      (** The order of the finite field *)
      order : Z.t;
      (** minimal number of bytes required to encode a value of the field. *)
      size_in_bytes : int;
      (** [check_bytes bs] returns [true] if [bs] is a correct byte
      representation of a field element *)
      check_bytes : Bytes.t -> bool;
      (** The neutral element for the addition *)
      zero : t;
      (** The neutral element for the multiplication *)
      one : t;
      (** [add a b] returns [a + b mod order] *)
      add : t -> t -> t;
      (** [mul a b] returns [a * b mod order] *)
      mul : t -> t -> t;
      (** [eq a b] returns [true] if [a = b mod order], else [false] *)
      eq_value : t -> t -> bool;
      (** [negate x] returns [-x mod order]. Equivalently, [negate x] returns the
      unique [y] such that [x + y mod order = 0] *)
      negate : t -> t;
      (** [inverse_opt x] returns [x^-1] if [x] is not [0] as an option, else [None] *)
      inverse_opt : t -> option t;
      (** [pow x n] returns [x^n] *)
      pow : t -> Z.t -> t;
      (** From a predefined bytes representation, construct a value t. It is not
      required that to_bytes [(Option.get (of_bytes_opt t)) = t]. By default,
      little endian encoding is used and the given element is modulo the prime
      order *)
      of_bytes_opt : Bytes.t -> option t;
      (** Convert the value t to a bytes representation which can be used for
      hashing for instance. It is not required that [to_bytes (Option.get
      (of_bytes_opt t)) = t]. By default, little endian encoding is used, and
      length of the resulting bytes may vary depending on the order. *)
      to_bytes : t -> Bytes.t;
    }.
  End FIELD.
  Definition FIELD := @FIELD.signature.
  Arguments FIELD {_}.
  
  Module PRIME_FIELD.
    Record signature {t : Set} : Set := {
      t := t;
      order : Z.t;
      size_in_bytes : int;
      check_bytes : Bytes.t -> bool;
      zero : t;
      one : t;
      add : t -> t -> t;
      mul : t -> t -> t;
      eq_value : t -> t -> bool;
      negate : t -> t;
      inverse_opt : t -> option t;
      pow : t -> Z.t -> t;
      of_bytes_opt : Bytes.t -> option t;
      to_bytes : t -> Bytes.t;
      (** [of_z x] builds an element t from the Zarith element [x]. [mod order] is
      applied if [x >= order] or [x < 0]. *)
      of_z : Z.t -> t;
      (** [to_z x] builds a Zarith element, using the decimal representation.
      Arithmetic on the result can be done using the modular functions on
      integers *)
      to_z : t -> Z.t;
    }.
  End PRIME_FIELD.
  Definition PRIME_FIELD := @PRIME_FIELD.signature.
  Arguments PRIME_FIELD {_}.
  
  Module CURVE.
    Record signature {t Scalar_t : Set} : Set := {
      (** The type of the element in the elliptic curve *)
      t := t;
      (** The size of a point representation, in bytes *)
      size_in_bytes : int;
      Scalar : FIELD (t := Scalar_t);
      (** Check if a point, represented as a byte array, is on the curve * *)
      check_bytes : Bytes.t -> bool;
      (** Attempt to construct a point from a byte array *)
      of_bytes_opt : Bytes.t -> option t;
      (** Return a representation in bytes *)
      to_bytes : t -> Bytes.t;
      (** Zero of the elliptic curve *)
      zero : t;
      (** A fixed generator of the elliptic curve *)
      one : t;
      (** Return the addition of two element *)
      add : t -> t -> t;
      (** Double the element *)
      double : t -> t;
      (** Return the opposite of the element *)
      negate : t -> t;
      (** Return [true] if the two elements are algebraically the same *)
      eq_value : t -> t -> bool;
      (** Multiply an element by a scalar *)
      mul : t -> Scalar.(FIELD.t) -> t;
    }.
  End CURVE.
  Definition CURVE := @CURVE.signature.
  Arguments CURVE {_ _}.
  
  Module PAIRING.
    Record signature {Gt_t G1_t G1_Scalar_t G2_t G2_Scalar_t : Set} : Set := {
      Gt : FIELD (t := Gt_t);
      G1 : CURVE (t := G1_t) (Scalar_t := G1_Scalar_t);
      G2 : CURVE (t := G2_t) (Scalar_t := G2_Scalar_t);
      miller_loop : list (G1.(CURVE.t) * G2.(CURVE.t)) -> Gt.(FIELD.t);
      final_exponentiation_opt : Gt.(FIELD.t) -> option Gt.(FIELD.t);
      pairing : G1.(CURVE.t) -> G2.(CURVE.t) -> Gt.(FIELD.t);
    }.
  End PAIRING.
  Definition PAIRING := @PAIRING.signature.
  Arguments PAIRING {_ _ _ _ _}.
  
  Module PVSS_ELEMENT.
    Record signature {t : Set} : Set := {
      t := t;
      to_b58check : t -> string;
      to_short_b58check : t -> string;
      of_b58check_exn : string -> t;
      of_b58check_opt : string -> option t;
      (* extensible_type_definition `Base58.data` *)
      b58check_encoding : Base58.encoding t;
      encoding : Data_encoding.t t;
      rpc_arg : RPC_arg.t t;
    }.
  End PVSS_ELEMENT.
  Definition PVSS_ELEMENT := @PVSS_ELEMENT.signature.
  Arguments PVSS_ELEMENT {_}.
  
  Module PVSS_PUBLIC_KEY.
    Record signature {t : Set} : Set := {
      t := t;
      pp : Format.formatter -> t -> unit;
      op_eq : t -> t -> bool;
      op_ltgt : t -> t -> bool;
      op_lt : t -> t -> bool;
      op_lteq : t -> t -> bool;
      op_gteq : t -> t -> bool;
      op_gt : t -> t -> bool;
      compare : t -> t -> int;
      equal : t -> t -> bool;
      max : t -> t -> t;
      min : t -> t -> t;
      size : int;
      to_bytes : t -> bytes;
      of_bytes_opt : bytes -> option t;
      of_bytes_exn : bytes -> t;
      to_b58check : t -> string;
      to_short_b58check : t -> string;
      of_b58check_exn : string -> t;
      of_b58check_opt : string -> option t;
      (* extensible_type_definition `Base58.data` *)
      b58check_encoding : Base58.encoding t;
      encoding : Data_encoding.t t;
      rpc_arg : RPC_arg.t t;
    }.
  End PVSS_PUBLIC_KEY.
  Definition PVSS_PUBLIC_KEY := @PVSS_PUBLIC_KEY.signature.
  Arguments PVSS_PUBLIC_KEY {_}.
  
  Module PVSS_SECRET_KEY.
    Record signature {public_key t : Set} : Set := {
      public_key := public_key;
      t := t;
      encoding : Data_encoding.t t;
      rpc_arg : RPC_arg.t t;
      to_public_key : t -> public_key;
    }.
  End PVSS_SECRET_KEY.
  Definition PVSS_SECRET_KEY := @PVSS_SECRET_KEY.signature.
  Arguments PVSS_SECRET_KEY {_ _}.
  
  Module PVSS.
    Record signature
      {proof Clear_share_t Commitment_t Encrypted_share_t Public_key_t
        Secret_key_t : Set} : Set := {
      proof := proof;
      Clear_share : PVSS_ELEMENT (t := Clear_share_t);
      Commitment : PVSS_ELEMENT (t := Commitment_t);
      Encrypted_share : PVSS_ELEMENT (t := Encrypted_share_t);
      Public_key : PVSS_PUBLIC_KEY (t := Public_key_t);
      Secret_key :
        PVSS_SECRET_KEY (public_key := Public_key.(PVSS_PUBLIC_KEY.t))
          (t := Secret_key_t);
      proof_encoding : Data_encoding.t proof;
      check_dealer_proof :
        list Encrypted_share.(PVSS_ELEMENT.t) ->
        list Commitment.(PVSS_ELEMENT.t) -> proof ->
        list Public_key.(PVSS_PUBLIC_KEY.t) -> bool;
      check_revealed_share :
        Encrypted_share.(PVSS_ELEMENT.t) -> Clear_share.(PVSS_ELEMENT.t) ->
        Public_key.(PVSS_PUBLIC_KEY.t) -> proof -> bool;
      reconstruct :
        list Clear_share.(PVSS_ELEMENT.t) -> list int ->
        Public_key.(PVSS_PUBLIC_KEY.t);
    }.
  End PVSS.
  Definition PVSS := @PVSS.signature.
  Arguments PVSS {_ _ _ _ _ _}.
End S_signature.
Require Export Proto_2021_01.Environment.S.
Module S_check : S_signature := S.

Module Type _Set_signature.
  Parameter Make_t :
    forall {Ord_t : Set} (Ord : Compare.COMPARABLE (t := Ord_t)), Set.
  
  Parameter Make :
    forall {Ord_t : Set},
    forall (Ord : Compare.COMPARABLE (t := Ord_t)),
    S.SET (elt := Ord.(Compare.COMPARABLE.t)) (t := Make_t Ord).
End _Set_signature.
Require Export Proto_2021_01.Environment._Set.
Module _Set_check : _Set_signature := _Set.

Module Type Map_signature.
  Parameter Make_t :
    forall {Ord_t : Set} (Ord : Compare.COMPARABLE (t := Ord_t)), Set -> Set.
  
  Parameter Make :
    forall {Ord_t : Set},
    forall (Ord : Compare.COMPARABLE (t := Ord_t)),
    S.MAP (key := Ord.(Compare.COMPARABLE.t)) (t := Make_t Ord).
End Map_signature.
Require Export Proto_2021_01.Environment.Map.
Module Map_check : Map_signature := Map.

Module Type Blake2B_signature.
  Module Name.
    Record signature : Set := {
      name : string;
      title : string;
      size : option int;
    }.
  End Name.
  Definition Name := Name.signature.
  
  Module PrefixedName.
    Record signature : Set := {
      name : string;
      title : string;
      size : option int;
      b58check_prefix : string;
    }.
  End PrefixedName.
  Definition PrefixedName := PrefixedName.signature.
  
  Parameter Make_minimal_t : forall (Name : Name), Set.
  
  Parameter Make_minimal :
    forall (Name : Name), S.MINIMAL_HASH (t := Make_minimal_t Name).
  
  Module Register.
    Record signature : Set := {
      register_encoding :
        forall {a : Set},
        string -> int -> (a -> string) -> (string -> option a) ->
        (a -> Base58.data) -> Base58.encoding a;
    }.
  End Register.
  Definition Register := Register.signature.
  
  Parameter Make_t : forall (Register : Register) (Name : PrefixedName), Set.
  
  Parameter Make_Set_t :
    forall (Register : Register) (Name : PrefixedName), Set.
  
  Parameter Make_Map_t :
    forall (Register : Register) (Name : PrefixedName), Set -> Set.
  
  Parameter Make :
    forall (Register : Register),
    forall (Name : PrefixedName),
    S.HASH (t := Make_t Register Name) (Set_t := Make_Set_t Register Name)
      (Map_t := Make_Map_t Register Name).
End Blake2B_signature.
Require Export Proto_2021_01.Environment.Blake2B.
Module Blake2B_check : Blake2B_signature := Blake2B.

Module Type Bls12_381_signature.
  Parameter Fr_t : Set.
  
  Parameter Fr : S.PRIME_FIELD (t := Fr_t).
  
  Parameter Fq12_t : Set.
  
  Parameter Fq12 : S.FIELD (t := Fq12_t).
  
  Parameter Included_PAIRING_G1_t : Set.
  
  Parameter Included_PAIRING_G2_t : Set.
  
  Parameter Included_PAIRING :
    S.PAIRING (Gt_t := Fq12.(S.FIELD.t)) (G1_t := Included_PAIRING_G1_t)
      (G1_Scalar_t := Fr.(S.PRIME_FIELD.t)) (G2_t := Included_PAIRING_G2_t)
      (G2_Scalar_t := Fr.(S.PRIME_FIELD.t)).
  
  Definition Gt := Included_PAIRING.(S.PAIRING.Gt).
  
  Definition G1 := Included_PAIRING.(S.PAIRING.G1).
  
  Definition G2 := Included_PAIRING.(S.PAIRING.G2).
  
  Definition miller_loop :
    list (G1.(S.CURVE.t) * G2.(S.CURVE.t)) -> Fq12.(S.FIELD.t) :=
    Included_PAIRING.(S.PAIRING.miller_loop).
  
  Definition final_exponentiation_opt :
    Fq12.(S.FIELD.t) -> option Fq12.(S.FIELD.t) :=
    Included_PAIRING.(S.PAIRING.final_exponentiation_opt).
  
  Definition pairing : G1.(S.CURVE.t) -> G2.(S.CURVE.t) -> Fq12.(S.FIELD.t) :=
    Included_PAIRING.(S.PAIRING.pairing).
End Bls12_381_signature.
Require Export Proto_2021_01.Environment.Bls12_381.
Module Bls12_381_check : Bls12_381_signature := Bls12_381.

Module Type Ed25519_signature.
  Parameter Included_SIGNATURE_Public_key_hash_t : Set.
  
  Parameter Included_SIGNATURE_Public_key_hash_Set_t : Set.
  
  Parameter Included_SIGNATURE_Public_key_hash_Map_t : Set -> Set.
  
  Parameter Included_SIGNATURE_Public_key_t : Set.
  
  Parameter Included_SIGNATURE_t : Set.
  
  Parameter Included_SIGNATURE :
    S.SIGNATURE (Public_key_hash_t := Included_SIGNATURE_Public_key_hash_t)
      (Public_key_hash_Set_t := Included_SIGNATURE_Public_key_hash_Set_t)
      (Public_key_hash_Map_t := Included_SIGNATURE_Public_key_hash_Map_t)
      (Public_key_t := Included_SIGNATURE_Public_key_t)
      (t := Included_SIGNATURE_t) (watermark := bytes).
  
  Definition Public_key_hash :=
    Included_SIGNATURE.(S.SIGNATURE.Public_key_hash).
  
  Definition Public_key := Included_SIGNATURE.(S.SIGNATURE.Public_key).
  
  Definition t := Included_SIGNATURE.(S.SIGNATURE.t).
  
  Definition pp : Format.formatter -> t -> unit :=
    Included_SIGNATURE.(S.SIGNATURE.pp).
  
  Definition size : int := Included_SIGNATURE.(S.SIGNATURE.size).
  
  Definition to_bytes : t -> bytes := Included_SIGNATURE.(S.SIGNATURE.to_bytes).
  
  Definition of_bytes_opt : bytes -> option t :=
    Included_SIGNATURE.(S.SIGNATURE.of_bytes_opt).
  
  Definition of_bytes_exn : bytes -> t :=
    Included_SIGNATURE.(S.SIGNATURE.of_bytes_exn).
  
  Definition op_eq : t -> t -> bool := Included_SIGNATURE.(S.SIGNATURE.op_eq).
  
  Definition op_ltgt : t -> t -> bool :=
    Included_SIGNATURE.(S.SIGNATURE.op_ltgt).
  
  Definition op_lt : t -> t -> bool := Included_SIGNATURE.(S.SIGNATURE.op_lt).
  
  Definition op_lteq : t -> t -> bool :=
    Included_SIGNATURE.(S.SIGNATURE.op_lteq).
  
  Definition op_gteq : t -> t -> bool :=
    Included_SIGNATURE.(S.SIGNATURE.op_gteq).
  
  Definition op_gt : t -> t -> bool := Included_SIGNATURE.(S.SIGNATURE.op_gt).
  
  Definition compare : t -> t -> int :=
    Included_SIGNATURE.(S.SIGNATURE.compare).
  
  Definition equal : t -> t -> bool := Included_SIGNATURE.(S.SIGNATURE.equal).
  
  Definition max : t -> t -> t := Included_SIGNATURE.(S.SIGNATURE.max).
  
  Definition min : t -> t -> t := Included_SIGNATURE.(S.SIGNATURE.min).
  
  Definition to_b58check : t -> string :=
    Included_SIGNATURE.(S.SIGNATURE.to_b58check).
  
  Definition to_short_b58check : t -> string :=
    Included_SIGNATURE.(S.SIGNATURE.to_short_b58check).
  
  Definition of_b58check_exn : string -> t :=
    Included_SIGNATURE.(S.SIGNATURE.of_b58check_exn).
  
  Definition of_b58check_opt : string -> option t :=
    Included_SIGNATURE.(S.SIGNATURE.of_b58check_opt).
  
  Definition b58check_encoding : Base58.encoding t :=
    Included_SIGNATURE.(S.SIGNATURE.b58check_encoding).
  
  Definition encoding : Data_encoding.t t :=
    Included_SIGNATURE.(S.SIGNATURE.encoding).
  
  Definition rpc_arg : RPC_arg.t t := Included_SIGNATURE.(S.SIGNATURE.rpc_arg).
  
  Definition zero : t := Included_SIGNATURE.(S.SIGNATURE.zero).
  
  Definition check :
    option bytes -> Public_key.(S.SIGNATURE_PUBLIC_KEY.t) -> t -> bytes -> bool
    := Included_SIGNATURE.(S.SIGNATURE.check).
End Ed25519_signature.
Require Export Proto_2021_01.Environment.Ed25519.
Module Ed25519_check : Ed25519_signature := Ed25519.

Module Type Secp256k1_signature.
  Parameter Included_SIGNATURE_Public_key_hash_t : Set.
  
  Parameter Included_SIGNATURE_Public_key_hash_Set_t : Set.
  
  Parameter Included_SIGNATURE_Public_key_hash_Map_t : Set -> Set.
  
  Parameter Included_SIGNATURE_Public_key_t : Set.
  
  Parameter Included_SIGNATURE_t : Set.
  
  Parameter Included_SIGNATURE :
    S.SIGNATURE (Public_key_hash_t := Included_SIGNATURE_Public_key_hash_t)
      (Public_key_hash_Set_t := Included_SIGNATURE_Public_key_hash_Set_t)
      (Public_key_hash_Map_t := Included_SIGNATURE_Public_key_hash_Map_t)
      (Public_key_t := Included_SIGNATURE_Public_key_t)
      (t := Included_SIGNATURE_t) (watermark := bytes).
  
  Definition Public_key_hash :=
    Included_SIGNATURE.(S.SIGNATURE.Public_key_hash).
  
  Definition Public_key := Included_SIGNATURE.(S.SIGNATURE.Public_key).
  
  Definition t := Included_SIGNATURE.(S.SIGNATURE.t).
  
  Definition pp : Format.formatter -> t -> unit :=
    Included_SIGNATURE.(S.SIGNATURE.pp).
  
  Definition size : int := Included_SIGNATURE.(S.SIGNATURE.size).
  
  Definition to_bytes : t -> bytes := Included_SIGNATURE.(S.SIGNATURE.to_bytes).
  
  Definition of_bytes_opt : bytes -> option t :=
    Included_SIGNATURE.(S.SIGNATURE.of_bytes_opt).
  
  Definition of_bytes_exn : bytes -> t :=
    Included_SIGNATURE.(S.SIGNATURE.of_bytes_exn).
  
  Definition op_eq : t -> t -> bool := Included_SIGNATURE.(S.SIGNATURE.op_eq).
  
  Definition op_ltgt : t -> t -> bool :=
    Included_SIGNATURE.(S.SIGNATURE.op_ltgt).
  
  Definition op_lt : t -> t -> bool := Included_SIGNATURE.(S.SIGNATURE.op_lt).
  
  Definition op_lteq : t -> t -> bool :=
    Included_SIGNATURE.(S.SIGNATURE.op_lteq).
  
  Definition op_gteq : t -> t -> bool :=
    Included_SIGNATURE.(S.SIGNATURE.op_gteq).
  
  Definition op_gt : t -> t -> bool := Included_SIGNATURE.(S.SIGNATURE.op_gt).
  
  Definition compare : t -> t -> int :=
    Included_SIGNATURE.(S.SIGNATURE.compare).
  
  Definition equal : t -> t -> bool := Included_SIGNATURE.(S.SIGNATURE.equal).
  
  Definition max : t -> t -> t := Included_SIGNATURE.(S.SIGNATURE.max).
  
  Definition min : t -> t -> t := Included_SIGNATURE.(S.SIGNATURE.min).
  
  Definition to_b58check : t -> string :=
    Included_SIGNATURE.(S.SIGNATURE.to_b58check).
  
  Definition to_short_b58check : t -> string :=
    Included_SIGNATURE.(S.SIGNATURE.to_short_b58check).
  
  Definition of_b58check_exn : string -> t :=
    Included_SIGNATURE.(S.SIGNATURE.of_b58check_exn).
  
  Definition of_b58check_opt : string -> option t :=
    Included_SIGNATURE.(S.SIGNATURE.of_b58check_opt).
  
  Definition b58check_encoding : Base58.encoding t :=
    Included_SIGNATURE.(S.SIGNATURE.b58check_encoding).
  
  Definition encoding : Data_encoding.t t :=
    Included_SIGNATURE.(S.SIGNATURE.encoding).
  
  Definition rpc_arg : RPC_arg.t t := Included_SIGNATURE.(S.SIGNATURE.rpc_arg).
  
  Definition zero : t := Included_SIGNATURE.(S.SIGNATURE.zero).
  
  Definition check :
    option bytes -> Public_key.(S.SIGNATURE_PUBLIC_KEY.t) -> t -> bytes -> bool
    := Included_SIGNATURE.(S.SIGNATURE.check).
End Secp256k1_signature.
Require Export Proto_2021_01.Environment.Secp256k1.
Module Secp256k1_check : Secp256k1_signature := Secp256k1.

Module Type P256_signature.
  Parameter Included_SIGNATURE_Public_key_hash_t : Set.
  
  Parameter Included_SIGNATURE_Public_key_hash_Set_t : Set.
  
  Parameter Included_SIGNATURE_Public_key_hash_Map_t : Set -> Set.
  
  Parameter Included_SIGNATURE_Public_key_t : Set.
  
  Parameter Included_SIGNATURE_t : Set.
  
  Parameter Included_SIGNATURE :
    S.SIGNATURE (Public_key_hash_t := Included_SIGNATURE_Public_key_hash_t)
      (Public_key_hash_Set_t := Included_SIGNATURE_Public_key_hash_Set_t)
      (Public_key_hash_Map_t := Included_SIGNATURE_Public_key_hash_Map_t)
      (Public_key_t := Included_SIGNATURE_Public_key_t)
      (t := Included_SIGNATURE_t) (watermark := bytes).
  
  Definition Public_key_hash :=
    Included_SIGNATURE.(S.SIGNATURE.Public_key_hash).
  
  Definition Public_key := Included_SIGNATURE.(S.SIGNATURE.Public_key).
  
  Definition t := Included_SIGNATURE.(S.SIGNATURE.t).
  
  Definition pp : Format.formatter -> t -> unit :=
    Included_SIGNATURE.(S.SIGNATURE.pp).
  
  Definition size : int := Included_SIGNATURE.(S.SIGNATURE.size).
  
  Definition to_bytes : t -> bytes := Included_SIGNATURE.(S.SIGNATURE.to_bytes).
  
  Definition of_bytes_opt : bytes -> option t :=
    Included_SIGNATURE.(S.SIGNATURE.of_bytes_opt).
  
  Definition of_bytes_exn : bytes -> t :=
    Included_SIGNATURE.(S.SIGNATURE.of_bytes_exn).
  
  Definition op_eq : t -> t -> bool := Included_SIGNATURE.(S.SIGNATURE.op_eq).
  
  Definition op_ltgt : t -> t -> bool :=
    Included_SIGNATURE.(S.SIGNATURE.op_ltgt).
  
  Definition op_lt : t -> t -> bool := Included_SIGNATURE.(S.SIGNATURE.op_lt).
  
  Definition op_lteq : t -> t -> bool :=
    Included_SIGNATURE.(S.SIGNATURE.op_lteq).
  
  Definition op_gteq : t -> t -> bool :=
    Included_SIGNATURE.(S.SIGNATURE.op_gteq).
  
  Definition op_gt : t -> t -> bool := Included_SIGNATURE.(S.SIGNATURE.op_gt).
  
  Definition compare : t -> t -> int :=
    Included_SIGNATURE.(S.SIGNATURE.compare).
  
  Definition equal : t -> t -> bool := Included_SIGNATURE.(S.SIGNATURE.equal).
  
  Definition max : t -> t -> t := Included_SIGNATURE.(S.SIGNATURE.max).
  
  Definition min : t -> t -> t := Included_SIGNATURE.(S.SIGNATURE.min).
  
  Definition to_b58check : t -> string :=
    Included_SIGNATURE.(S.SIGNATURE.to_b58check).
  
  Definition to_short_b58check : t -> string :=
    Included_SIGNATURE.(S.SIGNATURE.to_short_b58check).
  
  Definition of_b58check_exn : string -> t :=
    Included_SIGNATURE.(S.SIGNATURE.of_b58check_exn).
  
  Definition of_b58check_opt : string -> option t :=
    Included_SIGNATURE.(S.SIGNATURE.of_b58check_opt).
  
  Definition b58check_encoding : Base58.encoding t :=
    Included_SIGNATURE.(S.SIGNATURE.b58check_encoding).
  
  Definition encoding : Data_encoding.t t :=
    Included_SIGNATURE.(S.SIGNATURE.encoding).
  
  Definition rpc_arg : RPC_arg.t t := Included_SIGNATURE.(S.SIGNATURE.rpc_arg).
  
  Definition zero : t := Included_SIGNATURE.(S.SIGNATURE.zero).
  
  Definition check :
    option bytes -> Public_key.(S.SIGNATURE_PUBLIC_KEY.t) -> t -> bytes -> bool
    := Included_SIGNATURE.(S.SIGNATURE.check).
End P256_signature.
Require Export Proto_2021_01.Environment.P256.
Module P256_check : P256_signature := P256.

Module Type Chain_id_signature.
  Parameter Included_HASH_t : Set.
  
  Parameter Included_HASH_Set_t : Set.
  
  Parameter Included_HASH_Map_t : Set -> Set.
  
  Parameter Included_HASH :
    S.HASH (t := Included_HASH_t) (Set_t := Included_HASH_Set_t)
      (Map_t := Included_HASH_Map_t).
  
  Definition t := Included_HASH.(S.HASH.t).
  
  Definition name : string := Included_HASH.(S.HASH.name).
  
  Definition title : string := Included_HASH.(S.HASH.title).
  
  Definition pp : Format.formatter -> t -> unit := Included_HASH.(S.HASH.pp).
  
  Definition pp_short : Format.formatter -> t -> unit :=
    Included_HASH.(S.HASH.pp_short).
  
  Definition op_eq : t -> t -> bool := Included_HASH.(S.HASH.op_eq).
  
  Definition op_ltgt : t -> t -> bool := Included_HASH.(S.HASH.op_ltgt).
  
  Definition op_lt : t -> t -> bool := Included_HASH.(S.HASH.op_lt).
  
  Definition op_lteq : t -> t -> bool := Included_HASH.(S.HASH.op_lteq).
  
  Definition op_gteq : t -> t -> bool := Included_HASH.(S.HASH.op_gteq).
  
  Definition op_gt : t -> t -> bool := Included_HASH.(S.HASH.op_gt).
  
  Definition compare : t -> t -> int := Included_HASH.(S.HASH.compare).
  
  Definition equal : t -> t -> bool := Included_HASH.(S.HASH.equal).
  
  Definition max : t -> t -> t := Included_HASH.(S.HASH.max).
  
  Definition min : t -> t -> t := Included_HASH.(S.HASH.min).
  
  Definition hash_bytes : option bytes -> list bytes -> t :=
    Included_HASH.(S.HASH.hash_bytes).
  
  Definition hash_string : option string -> list string -> t :=
    Included_HASH.(S.HASH.hash_string).
  
  Definition zero : t := Included_HASH.(S.HASH.zero).
  
  Definition size : int := Included_HASH.(S.HASH.size).
  
  Definition to_bytes : t -> bytes := Included_HASH.(S.HASH.to_bytes).
  
  Definition of_bytes_opt : bytes -> option t :=
    Included_HASH.(S.HASH.of_bytes_opt).
  
  Definition of_bytes_exn : bytes -> t := Included_HASH.(S.HASH.of_bytes_exn).
  
  Definition to_b58check : t -> string := Included_HASH.(S.HASH.to_b58check).
  
  Definition to_short_b58check : t -> string :=
    Included_HASH.(S.HASH.to_short_b58check).
  
  Definition of_b58check_exn : string -> t :=
    Included_HASH.(S.HASH.of_b58check_exn).
  
  Definition of_b58check_opt : string -> option t :=
    Included_HASH.(S.HASH.of_b58check_opt).
  
  Definition b58check_encoding : Base58.encoding t :=
    Included_HASH.(S.HASH.b58check_encoding).
  
  Definition encoding : Data_encoding.t t := Included_HASH.(S.HASH.encoding).
  
  Definition rpc_arg : RPC_arg.t t := Included_HASH.(S.HASH.rpc_arg).
  
  Definition to_path : t -> list string -> list string :=
    Included_HASH.(S.HASH.to_path).
  
  Definition of_path : list string -> option t :=
    Included_HASH.(S.HASH.of_path).
  
  Definition of_path_exn : list string -> t :=
    Included_HASH.(S.HASH.of_path_exn).
  
  Definition prefix_path : string -> list string :=
    Included_HASH.(S.HASH.prefix_path).
  
  Definition path_length : int := Included_HASH.(S.HASH.path_length).
  
  Definition _Set := Included_HASH.(S.HASH._Set).
  
  Definition Map := Included_HASH.(S.HASH.Map).
End Chain_id_signature.
Require Export Proto_2021_01.Environment.Chain_id.
Module Chain_id_check : Chain_id_signature := Chain_id.

Module Type Signature_signature.
  Inductive public_key_hash : Set :=
  | Ed25519Hash :
    Ed25519.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.t) -> public_key_hash
  | Secp256k1Hash :
    Secp256k1.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.t) -> public_key_hash
  | P256Hash :
    P256.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.t) -> public_key_hash.
  
  Inductive public_key : Set :=
  | Ed25519 : Ed25519.Public_key.(S.SIGNATURE_PUBLIC_KEY.t) -> public_key
  | Secp256k1 : Secp256k1.Public_key.(S.SIGNATURE_PUBLIC_KEY.t) -> public_key
  | P256 : P256.Public_key.(S.SIGNATURE_PUBLIC_KEY.t) -> public_key.
  
  Inductive watermark : Set :=
  | Block_header : Chain_id.t -> watermark
  | Endorsement : Chain_id.t -> watermark
  | Generic_operation : watermark
  | Custom : bytes -> watermark.
  
  Parameter Included_SIGNATURE_Public_key_hash_Set_t : Set.
  
  Parameter Included_SIGNATURE_Public_key_hash_Map_t : Set -> Set.
  
  Parameter Included_SIGNATURE_t : Set.
  
  Parameter Included_SIGNATURE :
    S.SIGNATURE (Public_key_hash_t := public_key_hash)
      (Public_key_hash_Set_t := Included_SIGNATURE_Public_key_hash_Set_t)
      (Public_key_hash_Map_t := Included_SIGNATURE_Public_key_hash_Map_t)
      (Public_key_t := public_key) (t := Included_SIGNATURE_t)
      (watermark := watermark).
  
  Definition Public_key_hash :=
    Included_SIGNATURE.(S.SIGNATURE.Public_key_hash).
  
  Definition Public_key := Included_SIGNATURE.(S.SIGNATURE.Public_key).
  
  Definition t := Included_SIGNATURE.(S.SIGNATURE.t).
  
  Definition pp : Format.formatter -> t -> unit :=
    Included_SIGNATURE.(S.SIGNATURE.pp).
  
  Definition size : int := Included_SIGNATURE.(S.SIGNATURE.size).
  
  Definition to_bytes : t -> bytes := Included_SIGNATURE.(S.SIGNATURE.to_bytes).
  
  Definition of_bytes_opt : bytes -> option t :=
    Included_SIGNATURE.(S.SIGNATURE.of_bytes_opt).
  
  Definition of_bytes_exn : bytes -> t :=
    Included_SIGNATURE.(S.SIGNATURE.of_bytes_exn).
  
  Definition op_eq : t -> t -> bool := Included_SIGNATURE.(S.SIGNATURE.op_eq).
  
  Definition op_ltgt : t -> t -> bool :=
    Included_SIGNATURE.(S.SIGNATURE.op_ltgt).
  
  Definition op_lt : t -> t -> bool := Included_SIGNATURE.(S.SIGNATURE.op_lt).
  
  Definition op_lteq : t -> t -> bool :=
    Included_SIGNATURE.(S.SIGNATURE.op_lteq).
  
  Definition op_gteq : t -> t -> bool :=
    Included_SIGNATURE.(S.SIGNATURE.op_gteq).
  
  Definition op_gt : t -> t -> bool := Included_SIGNATURE.(S.SIGNATURE.op_gt).
  
  Definition compare : t -> t -> int :=
    Included_SIGNATURE.(S.SIGNATURE.compare).
  
  Definition equal : t -> t -> bool := Included_SIGNATURE.(S.SIGNATURE.equal).
  
  Definition max : t -> t -> t := Included_SIGNATURE.(S.SIGNATURE.max).
  
  Definition min : t -> t -> t := Included_SIGNATURE.(S.SIGNATURE.min).
  
  Definition to_b58check : t -> string :=
    Included_SIGNATURE.(S.SIGNATURE.to_b58check).
  
  Definition to_short_b58check : t -> string :=
    Included_SIGNATURE.(S.SIGNATURE.to_short_b58check).
  
  Definition of_b58check_exn : string -> t :=
    Included_SIGNATURE.(S.SIGNATURE.of_b58check_exn).
  
  Definition of_b58check_opt : string -> option t :=
    Included_SIGNATURE.(S.SIGNATURE.of_b58check_opt).
  
  Definition b58check_encoding : Base58.encoding t :=
    Included_SIGNATURE.(S.SIGNATURE.b58check_encoding).
  
  Definition encoding : Data_encoding.t t :=
    Included_SIGNATURE.(S.SIGNATURE.encoding).
  
  Definition rpc_arg : RPC_arg.t t := Included_SIGNATURE.(S.SIGNATURE.rpc_arg).
  
  Definition zero : t := Included_SIGNATURE.(S.SIGNATURE.zero).
  
  Definition check : option watermark -> public_key -> t -> bytes -> bool :=
    Included_SIGNATURE.(S.SIGNATURE.check).
End Signature_signature.
Require Export Proto_2021_01.Environment.Signature.
Module Signature_check : Signature_signature := Signature.

Module Type Block_hash_signature.
  Parameter Included_HASH_t : Set.
  
  Parameter Included_HASH_Set_t : Set.
  
  Parameter Included_HASH_Map_t : Set -> Set.
  
  Parameter Included_HASH :
    S.HASH (t := Included_HASH_t) (Set_t := Included_HASH_Set_t)
      (Map_t := Included_HASH_Map_t).
  
  Definition t := Included_HASH.(S.HASH.t).
  
  Definition name : string := Included_HASH.(S.HASH.name).
  
  Definition title : string := Included_HASH.(S.HASH.title).
  
  Definition pp : Format.formatter -> t -> unit := Included_HASH.(S.HASH.pp).
  
  Definition pp_short : Format.formatter -> t -> unit :=
    Included_HASH.(S.HASH.pp_short).
  
  Definition op_eq : t -> t -> bool := Included_HASH.(S.HASH.op_eq).
  
  Definition op_ltgt : t -> t -> bool := Included_HASH.(S.HASH.op_ltgt).
  
  Definition op_lt : t -> t -> bool := Included_HASH.(S.HASH.op_lt).
  
  Definition op_lteq : t -> t -> bool := Included_HASH.(S.HASH.op_lteq).
  
  Definition op_gteq : t -> t -> bool := Included_HASH.(S.HASH.op_gteq).
  
  Definition op_gt : t -> t -> bool := Included_HASH.(S.HASH.op_gt).
  
  Definition compare : t -> t -> int := Included_HASH.(S.HASH.compare).
  
  Definition equal : t -> t -> bool := Included_HASH.(S.HASH.equal).
  
  Definition max : t -> t -> t := Included_HASH.(S.HASH.max).
  
  Definition min : t -> t -> t := Included_HASH.(S.HASH.min).
  
  Definition hash_bytes : option bytes -> list bytes -> t :=
    Included_HASH.(S.HASH.hash_bytes).
  
  Definition hash_string : option string -> list string -> t :=
    Included_HASH.(S.HASH.hash_string).
  
  Definition zero : t := Included_HASH.(S.HASH.zero).
  
  Definition size : int := Included_HASH.(S.HASH.size).
  
  Definition to_bytes : t -> bytes := Included_HASH.(S.HASH.to_bytes).
  
  Definition of_bytes_opt : bytes -> option t :=
    Included_HASH.(S.HASH.of_bytes_opt).
  
  Definition of_bytes_exn : bytes -> t := Included_HASH.(S.HASH.of_bytes_exn).
  
  Definition to_b58check : t -> string := Included_HASH.(S.HASH.to_b58check).
  
  Definition to_short_b58check : t -> string :=
    Included_HASH.(S.HASH.to_short_b58check).
  
  Definition of_b58check_exn : string -> t :=
    Included_HASH.(S.HASH.of_b58check_exn).
  
  Definition of_b58check_opt : string -> option t :=
    Included_HASH.(S.HASH.of_b58check_opt).
  
  Definition b58check_encoding : Base58.encoding t :=
    Included_HASH.(S.HASH.b58check_encoding).
  
  Definition encoding : Data_encoding.t t := Included_HASH.(S.HASH.encoding).
  
  Definition rpc_arg : RPC_arg.t t := Included_HASH.(S.HASH.rpc_arg).
  
  Definition to_path : t -> list string -> list string :=
    Included_HASH.(S.HASH.to_path).
  
  Definition of_path : list string -> option t :=
    Included_HASH.(S.HASH.of_path).
  
  Definition of_path_exn : list string -> t :=
    Included_HASH.(S.HASH.of_path_exn).
  
  Definition prefix_path : string -> list string :=
    Included_HASH.(S.HASH.prefix_path).
  
  Definition path_length : int := Included_HASH.(S.HASH.path_length).
  
  Definition _Set := Included_HASH.(S.HASH._Set).
  
  Definition Map := Included_HASH.(S.HASH.Map).
End Block_hash_signature.
Require Export Proto_2021_01.Environment.Block_hash.
Module Block_hash_check : Block_hash_signature := Block_hash.

Module Type Operation_hash_signature.
  Parameter Included_HASH_t : Set.
  
  Parameter Included_HASH_Set_t : Set.
  
  Parameter Included_HASH_Map_t : Set -> Set.
  
  Parameter Included_HASH :
    S.HASH (t := Included_HASH_t) (Set_t := Included_HASH_Set_t)
      (Map_t := Included_HASH_Map_t).
  
  Definition t := Included_HASH.(S.HASH.t).
  
  Definition name : string := Included_HASH.(S.HASH.name).
  
  Definition title : string := Included_HASH.(S.HASH.title).
  
  Definition pp : Format.formatter -> t -> unit := Included_HASH.(S.HASH.pp).
  
  Definition pp_short : Format.formatter -> t -> unit :=
    Included_HASH.(S.HASH.pp_short).
  
  Definition op_eq : t -> t -> bool := Included_HASH.(S.HASH.op_eq).
  
  Definition op_ltgt : t -> t -> bool := Included_HASH.(S.HASH.op_ltgt).
  
  Definition op_lt : t -> t -> bool := Included_HASH.(S.HASH.op_lt).
  
  Definition op_lteq : t -> t -> bool := Included_HASH.(S.HASH.op_lteq).
  
  Definition op_gteq : t -> t -> bool := Included_HASH.(S.HASH.op_gteq).
  
  Definition op_gt : t -> t -> bool := Included_HASH.(S.HASH.op_gt).
  
  Definition compare : t -> t -> int := Included_HASH.(S.HASH.compare).
  
  Definition equal : t -> t -> bool := Included_HASH.(S.HASH.equal).
  
  Definition max : t -> t -> t := Included_HASH.(S.HASH.max).
  
  Definition min : t -> t -> t := Included_HASH.(S.HASH.min).
  
  Definition hash_bytes : option bytes -> list bytes -> t :=
    Included_HASH.(S.HASH.hash_bytes).
  
  Definition hash_string : option string -> list string -> t :=
    Included_HASH.(S.HASH.hash_string).
  
  Definition zero : t := Included_HASH.(S.HASH.zero).
  
  Definition size : int := Included_HASH.(S.HASH.size).
  
  Definition to_bytes : t -> bytes := Included_HASH.(S.HASH.to_bytes).
  
  Definition of_bytes_opt : bytes -> option t :=
    Included_HASH.(S.HASH.of_bytes_opt).
  
  Definition of_bytes_exn : bytes -> t := Included_HASH.(S.HASH.of_bytes_exn).
  
  Definition to_b58check : t -> string := Included_HASH.(S.HASH.to_b58check).
  
  Definition to_short_b58check : t -> string :=
    Included_HASH.(S.HASH.to_short_b58check).
  
  Definition of_b58check_exn : string -> t :=
    Included_HASH.(S.HASH.of_b58check_exn).
  
  Definition of_b58check_opt : string -> option t :=
    Included_HASH.(S.HASH.of_b58check_opt).
  
  Definition b58check_encoding : Base58.encoding t :=
    Included_HASH.(S.HASH.b58check_encoding).
  
  Definition encoding : Data_encoding.t t := Included_HASH.(S.HASH.encoding).
  
  Definition rpc_arg : RPC_arg.t t := Included_HASH.(S.HASH.rpc_arg).
  
  Definition to_path : t -> list string -> list string :=
    Included_HASH.(S.HASH.to_path).
  
  Definition of_path : list string -> option t :=
    Included_HASH.(S.HASH.of_path).
  
  Definition of_path_exn : list string -> t :=
    Included_HASH.(S.HASH.of_path_exn).
  
  Definition prefix_path : string -> list string :=
    Included_HASH.(S.HASH.prefix_path).
  
  Definition path_length : int := Included_HASH.(S.HASH.path_length).
  
  Definition _Set := Included_HASH.(S.HASH._Set).
  
  Definition Map := Included_HASH.(S.HASH.Map).
End Operation_hash_signature.
Require Export Proto_2021_01.Environment.Operation_hash.
Module Operation_hash_check : Operation_hash_signature := Operation_hash.

Module Type Operation_list_hash_signature.
  Parameter Included_MERKLE_TREE_t : Set.
  
  Parameter Included_MERKLE_TREE_Set_t : Set.
  
  Parameter Included_MERKLE_TREE_Map_t : Set -> Set.
  
  Parameter Included_MERKLE_TREE_path : Set.
  
  Parameter Included_MERKLE_TREE :
    S.MERKLE_TREE (elt := Operation_hash.t) (t := Included_MERKLE_TREE_t)
      (Set_t := Included_MERKLE_TREE_Set_t)
      (Map_t := Included_MERKLE_TREE_Map_t) (path := Included_MERKLE_TREE_path).
  
  Definition elt := Included_MERKLE_TREE.(S.MERKLE_TREE.elt).
  
  Definition t := Included_MERKLE_TREE.(S.MERKLE_TREE.t).
  
  Definition name : string := Included_MERKLE_TREE.(S.MERKLE_TREE.name).
  
  Definition title : string := Included_MERKLE_TREE.(S.MERKLE_TREE.title).
  
  Definition pp : Format.formatter -> t -> unit :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.pp).
  
  Definition pp_short : Format.formatter -> t -> unit :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.pp_short).
  
  Definition op_eq : t -> t -> bool :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.op_eq).
  
  Definition op_ltgt : t -> t -> bool :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.op_ltgt).
  
  Definition op_lt : t -> t -> bool :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.op_lt).
  
  Definition op_lteq : t -> t -> bool :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.op_lteq).
  
  Definition op_gteq : t -> t -> bool :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.op_gteq).
  
  Definition op_gt : t -> t -> bool :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.op_gt).
  
  Definition compare : t -> t -> int :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.compare).
  
  Definition equal : t -> t -> bool :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.equal).
  
  Definition max : t -> t -> t := Included_MERKLE_TREE.(S.MERKLE_TREE.max).
  
  Definition min : t -> t -> t := Included_MERKLE_TREE.(S.MERKLE_TREE.min).
  
  Definition hash_bytes : option bytes -> list bytes -> t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.hash_bytes).
  
  Definition hash_string : option string -> list string -> t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.hash_string).
  
  Definition zero : t := Included_MERKLE_TREE.(S.MERKLE_TREE.zero).
  
  Definition size : int := Included_MERKLE_TREE.(S.MERKLE_TREE.size).
  
  Definition to_bytes : t -> bytes :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.to_bytes).
  
  Definition of_bytes_opt : bytes -> option t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.of_bytes_opt).
  
  Definition of_bytes_exn : bytes -> t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.of_bytes_exn).
  
  Definition to_b58check : t -> string :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.to_b58check).
  
  Definition to_short_b58check : t -> string :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.to_short_b58check).
  
  Definition of_b58check_exn : string -> t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.of_b58check_exn).
  
  Definition of_b58check_opt : string -> option t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.of_b58check_opt).
  
  Definition b58check_encoding : Base58.encoding t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.b58check_encoding).
  
  Definition encoding : Data_encoding.t t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.encoding).
  
  Definition rpc_arg : RPC_arg.t t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.rpc_arg).
  
  Definition to_path : t -> list string -> list string :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.to_path).
  
  Definition of_path : list string -> option t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.of_path).
  
  Definition of_path_exn : list string -> t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.of_path_exn).
  
  Definition prefix_path : string -> list string :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.prefix_path).
  
  Definition path_length : int :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.path_length).
  
  Definition _Set := Included_MERKLE_TREE.(S.MERKLE_TREE._Set).
  
  Definition Map := Included_MERKLE_TREE.(S.MERKLE_TREE.Map).
  
  Definition compute : list elt -> t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.compute).
  
  Definition empty : t := Included_MERKLE_TREE.(S.MERKLE_TREE.empty).
  
  Definition path := Included_MERKLE_TREE.(S.MERKLE_TREE.path).
  
  Definition compute_path : list elt -> int -> path :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.compute_path).
  
  Definition check_path : path -> elt -> t * int :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.check_path).
  
  Definition path_encoding : Data_encoding.t path :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.path_encoding).
End Operation_list_hash_signature.
Require Export Proto_2021_01.Environment.Operation_list_hash.
Module Operation_list_hash_check : Operation_list_hash_signature := Operation_list_hash.

Module Type Operation_list_list_hash_signature.
  Parameter Included_MERKLE_TREE_t : Set.
  
  Parameter Included_MERKLE_TREE_Set_t : Set.
  
  Parameter Included_MERKLE_TREE_Map_t : Set -> Set.
  
  Parameter Included_MERKLE_TREE_path : Set.
  
  Parameter Included_MERKLE_TREE :
    S.MERKLE_TREE (elt := Operation_list_hash.t) (t := Included_MERKLE_TREE_t)
      (Set_t := Included_MERKLE_TREE_Set_t)
      (Map_t := Included_MERKLE_TREE_Map_t) (path := Included_MERKLE_TREE_path).
  
  Definition elt := Included_MERKLE_TREE.(S.MERKLE_TREE.elt).
  
  Definition t := Included_MERKLE_TREE.(S.MERKLE_TREE.t).
  
  Definition name : string := Included_MERKLE_TREE.(S.MERKLE_TREE.name).
  
  Definition title : string := Included_MERKLE_TREE.(S.MERKLE_TREE.title).
  
  Definition pp : Format.formatter -> t -> unit :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.pp).
  
  Definition pp_short : Format.formatter -> t -> unit :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.pp_short).
  
  Definition op_eq : t -> t -> bool :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.op_eq).
  
  Definition op_ltgt : t -> t -> bool :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.op_ltgt).
  
  Definition op_lt : t -> t -> bool :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.op_lt).
  
  Definition op_lteq : t -> t -> bool :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.op_lteq).
  
  Definition op_gteq : t -> t -> bool :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.op_gteq).
  
  Definition op_gt : t -> t -> bool :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.op_gt).
  
  Definition compare : t -> t -> int :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.compare).
  
  Definition equal : t -> t -> bool :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.equal).
  
  Definition max : t -> t -> t := Included_MERKLE_TREE.(S.MERKLE_TREE.max).
  
  Definition min : t -> t -> t := Included_MERKLE_TREE.(S.MERKLE_TREE.min).
  
  Definition hash_bytes : option bytes -> list bytes -> t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.hash_bytes).
  
  Definition hash_string : option string -> list string -> t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.hash_string).
  
  Definition zero : t := Included_MERKLE_TREE.(S.MERKLE_TREE.zero).
  
  Definition size : int := Included_MERKLE_TREE.(S.MERKLE_TREE.size).
  
  Definition to_bytes : t -> bytes :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.to_bytes).
  
  Definition of_bytes_opt : bytes -> option t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.of_bytes_opt).
  
  Definition of_bytes_exn : bytes -> t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.of_bytes_exn).
  
  Definition to_b58check : t -> string :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.to_b58check).
  
  Definition to_short_b58check : t -> string :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.to_short_b58check).
  
  Definition of_b58check_exn : string -> t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.of_b58check_exn).
  
  Definition of_b58check_opt : string -> option t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.of_b58check_opt).
  
  Definition b58check_encoding : Base58.encoding t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.b58check_encoding).
  
  Definition encoding : Data_encoding.t t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.encoding).
  
  Definition rpc_arg : RPC_arg.t t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.rpc_arg).
  
  Definition to_path : t -> list string -> list string :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.to_path).
  
  Definition of_path : list string -> option t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.of_path).
  
  Definition of_path_exn : list string -> t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.of_path_exn).
  
  Definition prefix_path : string -> list string :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.prefix_path).
  
  Definition path_length : int :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.path_length).
  
  Definition _Set := Included_MERKLE_TREE.(S.MERKLE_TREE._Set).
  
  Definition Map := Included_MERKLE_TREE.(S.MERKLE_TREE.Map).
  
  Definition compute : list elt -> t :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.compute).
  
  Definition empty : t := Included_MERKLE_TREE.(S.MERKLE_TREE.empty).
  
  Definition path := Included_MERKLE_TREE.(S.MERKLE_TREE.path).
  
  Definition compute_path : list elt -> int -> path :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.compute_path).
  
  Definition check_path : path -> elt -> t * int :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.check_path).
  
  Definition path_encoding : Data_encoding.t path :=
    Included_MERKLE_TREE.(S.MERKLE_TREE.path_encoding).
End Operation_list_list_hash_signature.
Require Export Proto_2021_01.Environment.Operation_list_list_hash.
Module Operation_list_list_hash_check : Operation_list_list_hash_signature := Operation_list_list_hash.

Module Type Protocol_hash_signature.
  Parameter Included_HASH_t : Set.
  
  Parameter Included_HASH_Set_t : Set.
  
  Parameter Included_HASH_Map_t : Set -> Set.
  
  Parameter Included_HASH :
    S.HASH (t := Included_HASH_t) (Set_t := Included_HASH_Set_t)
      (Map_t := Included_HASH_Map_t).
  
  Definition t := Included_HASH.(S.HASH.t).
  
  Definition name : string := Included_HASH.(S.HASH.name).
  
  Definition title : string := Included_HASH.(S.HASH.title).
  
  Definition pp : Format.formatter -> t -> unit := Included_HASH.(S.HASH.pp).
  
  Definition pp_short : Format.formatter -> t -> unit :=
    Included_HASH.(S.HASH.pp_short).
  
  Definition op_eq : t -> t -> bool := Included_HASH.(S.HASH.op_eq).
  
  Definition op_ltgt : t -> t -> bool := Included_HASH.(S.HASH.op_ltgt).
  
  Definition op_lt : t -> t -> bool := Included_HASH.(S.HASH.op_lt).
  
  Definition op_lteq : t -> t -> bool := Included_HASH.(S.HASH.op_lteq).
  
  Definition op_gteq : t -> t -> bool := Included_HASH.(S.HASH.op_gteq).
  
  Definition op_gt : t -> t -> bool := Included_HASH.(S.HASH.op_gt).
  
  Definition compare : t -> t -> int := Included_HASH.(S.HASH.compare).
  
  Definition equal : t -> t -> bool := Included_HASH.(S.HASH.equal).
  
  Definition max : t -> t -> t := Included_HASH.(S.HASH.max).
  
  Definition min : t -> t -> t := Included_HASH.(S.HASH.min).
  
  Definition hash_bytes : option bytes -> list bytes -> t :=
    Included_HASH.(S.HASH.hash_bytes).
  
  Definition hash_string : option string -> list string -> t :=
    Included_HASH.(S.HASH.hash_string).
  
  Definition zero : t := Included_HASH.(S.HASH.zero).
  
  Definition size : int := Included_HASH.(S.HASH.size).
  
  Definition to_bytes : t -> bytes := Included_HASH.(S.HASH.to_bytes).
  
  Definition of_bytes_opt : bytes -> option t :=
    Included_HASH.(S.HASH.of_bytes_opt).
  
  Definition of_bytes_exn : bytes -> t := Included_HASH.(S.HASH.of_bytes_exn).
  
  Definition to_b58check : t -> string := Included_HASH.(S.HASH.to_b58check).
  
  Definition to_short_b58check : t -> string :=
    Included_HASH.(S.HASH.to_short_b58check).
  
  Definition of_b58check_exn : string -> t :=
    Included_HASH.(S.HASH.of_b58check_exn).
  
  Definition of_b58check_opt : string -> option t :=
    Included_HASH.(S.HASH.of_b58check_opt).
  
  Definition b58check_encoding : Base58.encoding t :=
    Included_HASH.(S.HASH.b58check_encoding).
  
  Definition encoding : Data_encoding.t t := Included_HASH.(S.HASH.encoding).
  
  Definition rpc_arg : RPC_arg.t t := Included_HASH.(S.HASH.rpc_arg).
  
  Definition to_path : t -> list string -> list string :=
    Included_HASH.(S.HASH.to_path).
  
  Definition of_path : list string -> option t :=
    Included_HASH.(S.HASH.of_path).
  
  Definition of_path_exn : list string -> t :=
    Included_HASH.(S.HASH.of_path_exn).
  
  Definition prefix_path : string -> list string :=
    Included_HASH.(S.HASH.prefix_path).
  
  Definition path_length : int := Included_HASH.(S.HASH.path_length).
  
  Definition _Set := Included_HASH.(S.HASH._Set).
  
  Definition Map := Included_HASH.(S.HASH.Map).
End Protocol_hash_signature.
Require Export Proto_2021_01.Environment.Protocol_hash.
Module Protocol_hash_check : Protocol_hash_signature := Protocol_hash.

Module Type Context_hash_signature.
  Parameter Included_HASH_t : Set.
  
  Parameter Included_HASH_Set_t : Set.
  
  Parameter Included_HASH_Map_t : Set -> Set.
  
  Parameter Included_HASH :
    S.HASH (t := Included_HASH_t) (Set_t := Included_HASH_Set_t)
      (Map_t := Included_HASH_Map_t).
  
  Definition t := Included_HASH.(S.HASH.t).
  
  Definition name : string := Included_HASH.(S.HASH.name).
  
  Definition title : string := Included_HASH.(S.HASH.title).
  
  Definition pp : Format.formatter -> t -> unit := Included_HASH.(S.HASH.pp).
  
  Definition pp_short : Format.formatter -> t -> unit :=
    Included_HASH.(S.HASH.pp_short).
  
  Definition op_eq : t -> t -> bool := Included_HASH.(S.HASH.op_eq).
  
  Definition op_ltgt : t -> t -> bool := Included_HASH.(S.HASH.op_ltgt).
  
  Definition op_lt : t -> t -> bool := Included_HASH.(S.HASH.op_lt).
  
  Definition op_lteq : t -> t -> bool := Included_HASH.(S.HASH.op_lteq).
  
  Definition op_gteq : t -> t -> bool := Included_HASH.(S.HASH.op_gteq).
  
  Definition op_gt : t -> t -> bool := Included_HASH.(S.HASH.op_gt).
  
  Definition compare : t -> t -> int := Included_HASH.(S.HASH.compare).
  
  Definition equal : t -> t -> bool := Included_HASH.(S.HASH.equal).
  
  Definition max : t -> t -> t := Included_HASH.(S.HASH.max).
  
  Definition min : t -> t -> t := Included_HASH.(S.HASH.min).
  
  Definition hash_bytes : option bytes -> list bytes -> t :=
    Included_HASH.(S.HASH.hash_bytes).
  
  Definition hash_string : option string -> list string -> t :=
    Included_HASH.(S.HASH.hash_string).
  
  Definition zero : t := Included_HASH.(S.HASH.zero).
  
  Definition size : int := Included_HASH.(S.HASH.size).
  
  Definition to_bytes : t -> bytes := Included_HASH.(S.HASH.to_bytes).
  
  Definition of_bytes_opt : bytes -> option t :=
    Included_HASH.(S.HASH.of_bytes_opt).
  
  Definition of_bytes_exn : bytes -> t := Included_HASH.(S.HASH.of_bytes_exn).
  
  Definition to_b58check : t -> string := Included_HASH.(S.HASH.to_b58check).
  
  Definition to_short_b58check : t -> string :=
    Included_HASH.(S.HASH.to_short_b58check).
  
  Definition of_b58check_exn : string -> t :=
    Included_HASH.(S.HASH.of_b58check_exn).
  
  Definition of_b58check_opt : string -> option t :=
    Included_HASH.(S.HASH.of_b58check_opt).
  
  Definition b58check_encoding : Base58.encoding t :=
    Included_HASH.(S.HASH.b58check_encoding).
  
  Definition encoding : Data_encoding.t t := Included_HASH.(S.HASH.encoding).
  
  Definition rpc_arg : RPC_arg.t t := Included_HASH.(S.HASH.rpc_arg).
  
  Definition to_path : t -> list string -> list string :=
    Included_HASH.(S.HASH.to_path).
  
  Definition of_path : list string -> option t :=
    Included_HASH.(S.HASH.of_path).
  
  Definition of_path_exn : list string -> t :=
    Included_HASH.(S.HASH.of_path_exn).
  
  Definition prefix_path : string -> list string :=
    Included_HASH.(S.HASH.prefix_path).
  
  Definition path_length : int := Included_HASH.(S.HASH.path_length).
  
  Definition _Set := Included_HASH.(S.HASH._Set).
  
  Definition Map := Included_HASH.(S.HASH.Map).
End Context_hash_signature.
Require Export Proto_2021_01.Environment.Context_hash.
Module Context_hash_check : Context_hash_signature := Context_hash.

Module Type Pvss_secp256k1_signature.
  Parameter Included_PVSS_proof : Set.
  
  Parameter Included_PVSS_Clear_share_t : Set.
  
  Parameter Included_PVSS_Commitment_t : Set.
  
  Parameter Included_PVSS_Encrypted_share_t : Set.
  
  Parameter Included_PVSS_Public_key_t : Set.
  
  Parameter Included_PVSS_Secret_key_t : Set.
  
  Parameter Included_PVSS :
    S.PVSS (proof := Included_PVSS_proof)
      (Clear_share_t := Included_PVSS_Clear_share_t)
      (Commitment_t := Included_PVSS_Commitment_t)
      (Encrypted_share_t := Included_PVSS_Encrypted_share_t)
      (Public_key_t := Included_PVSS_Public_key_t)
      (Secret_key_t := Included_PVSS_Secret_key_t).
  
  Definition proof := Included_PVSS.(S.PVSS.proof).
  
  Definition Clear_share := Included_PVSS.(S.PVSS.Clear_share).
  
  Definition Commitment := Included_PVSS.(S.PVSS.Commitment).
  
  Definition Encrypted_share := Included_PVSS.(S.PVSS.Encrypted_share).
  
  Definition Public_key := Included_PVSS.(S.PVSS.Public_key).
  
  Definition Secret_key := Included_PVSS.(S.PVSS.Secret_key).
  
  Definition proof_encoding : Data_encoding.t proof :=
    Included_PVSS.(S.PVSS.proof_encoding).
  
  Definition check_dealer_proof :
    list Encrypted_share.(S.PVSS_ELEMENT.t) ->
    list Commitment.(S.PVSS_ELEMENT.t) -> proof ->
    list Public_key.(S.PVSS_PUBLIC_KEY.t) -> bool :=
    Included_PVSS.(S.PVSS.check_dealer_proof).
  
  Definition check_revealed_share :
    Encrypted_share.(S.PVSS_ELEMENT.t) -> Clear_share.(S.PVSS_ELEMENT.t) ->
    Public_key.(S.PVSS_PUBLIC_KEY.t) -> proof -> bool :=
    Included_PVSS.(S.PVSS.check_revealed_share).
  
  Definition reconstruct :
    list Clear_share.(S.PVSS_ELEMENT.t) -> list int ->
    Public_key.(S.PVSS_PUBLIC_KEY.t) := Included_PVSS.(S.PVSS.reconstruct).
End Pvss_secp256k1_signature.
Require Export Proto_2021_01.Environment.Pvss_secp256k1.
Module Pvss_secp256k1_check : Pvss_secp256k1_signature := Pvss_secp256k1.

Module Type Sapling_signature.
  Module Ciphertext.
    Parameter t : Set.
    
    Parameter encoding : Data_encoding.t t.
    
    Parameter get_memo_size : t -> int.
  End Ciphertext.
  
  Module Commitment.
    Parameter t : Set.
    
    Parameter encoding : Data_encoding.t t.
    
    Parameter valid_position : int64 -> bool.
  End Commitment.
  
  Module CV.
    Parameter t : Set.
    
    Parameter encoding : Data_encoding.t t.
  End CV.
  
  Module Hash.
    Parameter t : Set.
    
    Parameter compare : t -> t -> int.
    
    Parameter encoding : Data_encoding.t t.
    
    Parameter to_bytes : t -> Bytes.t.
    
    Parameter of_bytes_exn : Bytes.t -> t.
    
    Parameter uncommitted : int -> t.
    
    Parameter merkle_hash : int -> t -> t -> t.
    
    Parameter of_commitment : Commitment.t -> t.
    
    Parameter to_commitment : t -> Commitment.t.
  End Hash.
  
  Module Nullifier.
    Parameter t : Set.
    
    Parameter encoding : Data_encoding.t t.
    
    Parameter compare : t -> t -> int.
  End Nullifier.
  
  Module UTXO.
    Parameter rk : Set.
    
    Parameter spend_proof : Set.
    
    Parameter spend_sig : Set.
    
    Parameter output_proof : Set.
    
    Module input.
      Record record : Set := Build {
        cv : CV.t;
        nf : Nullifier.t;
        rk : rk;
        proof_i : spend_proof;
        signature : spend_sig }.
      Definition with_cv cv (r : record) :=
        Build cv r.(nf) r.(rk) r.(proof_i) r.(signature).
      Definition with_nf nf (r : record) :=
        Build r.(cv) nf r.(rk) r.(proof_i) r.(signature).
      Definition with_rk rk (r : record) :=
        Build r.(cv) r.(nf) rk r.(proof_i) r.(signature).
      Definition with_proof_i proof_i (r : record) :=
        Build r.(cv) r.(nf) r.(rk) proof_i r.(signature).
      Definition with_signature signature (r : record) :=
        Build r.(cv) r.(nf) r.(rk) r.(proof_i) signature.
    End input.
    Definition input := input.record.
    
    Parameter input_encoding : Data_encoding.t input.
    
    Module output.
      Record record : Set := Build {
        cm : Commitment.t;
        proof_o : output_proof;
        ciphertext : Ciphertext.t }.
      Definition with_cm cm (r : record) :=
        Build cm r.(proof_o) r.(ciphertext).
      Definition with_proof_o proof_o (r : record) :=
        Build r.(cm) proof_o r.(ciphertext).
      Definition with_ciphertext ciphertext (r : record) :=
        Build r.(cm) r.(proof_o) ciphertext.
    End output.
    Definition output := output.record.
    
    Parameter output_encoding : Data_encoding.t output.
    
    Parameter binding_sig : Set.
    
    Module transaction.
      Record record : Set := Build {
        inputs : list input;
        outputs : list output;
        binding_sig : binding_sig;
        balance : Int64.t;
        root : Hash.t }.
      Definition with_inputs inputs (r : record) :=
        Build inputs r.(outputs) r.(binding_sig) r.(balance) r.(root).
      Definition with_outputs outputs (r : record) :=
        Build r.(inputs) outputs r.(binding_sig) r.(balance) r.(root).
      Definition with_binding_sig binding_sig (r : record) :=
        Build r.(inputs) r.(outputs) binding_sig r.(balance) r.(root).
      Definition with_balance balance (r : record) :=
        Build r.(inputs) r.(outputs) r.(binding_sig) balance r.(root).
      Definition with_root root (r : record) :=
        Build r.(inputs) r.(outputs) r.(binding_sig) r.(balance) root.
    End transaction.
    Definition transaction := transaction.record.
    
    Parameter transaction_encoding : Data_encoding.t transaction.
    
    Parameter binding_sig_encoding : Data_encoding.t binding_sig.
  End UTXO.
  
  Module Verification.
    Parameter t : Set.
    
    Parameter with_verification_ctx : forall {a : Set}, (t -> a) -> a.
    
    Parameter check_spend : t -> UTXO.input -> Hash.t -> string -> bool.
    
    Parameter check_output : t -> UTXO.output -> bool.
    
    Parameter final_check : t -> UTXO.transaction -> string -> bool.
  End Verification.
End Sapling_signature.
Require Export Proto_2021_01.Environment.Sapling.
Module Sapling_check : Sapling_signature := Sapling.

Module Type Micheline_signature.
  Definition annot : Set := list string.
  
  Inductive node (l p : Set) : Set :=
  | Int : l -> Z.t -> node l p
  | String : l -> string -> node l p
  | Bytes : l -> bytes -> node l p
  | Prim : l -> p -> list (node l p) -> annot -> node l p
  | Seq : l -> list (node l p) -> node l p.
  
  Arguments Int {_ _}.
  Arguments String {_ _}.
  Arguments Bytes {_ _}.
  Arguments Prim {_ _}.
  Arguments Seq {_ _}.
  
  Parameter canonical : forall (p : Set), Set.
  
  Definition canonical_location : Set := int.
  
  Parameter root : forall {p : Set}, canonical p -> node canonical_location p.
  
  Parameter canonical_location_encoding :
    Data_encoding.encoding canonical_location.
  
  Parameter canonical_encoding : forall {l : Set},
    string -> Data_encoding.encoding l -> Data_encoding.encoding (canonical l).
  
  Parameter canonical_encoding_v1 : forall {l : Set},
    string -> Data_encoding.encoding l -> Data_encoding.encoding (canonical l).
  
  Parameter location : forall {l p : Set}, node l p -> l.
  
  Parameter annotations : forall {l p : Set}, node l p -> list string.
  
  Parameter strip_locations : forall {A p : Set}, node A p -> canonical p.
  
  Parameter extract_locations : forall {l p : Set},
    node l p -> canonical p * list (canonical_location * l).
  
  Parameter inject_locations : forall {l p : Set},
    (canonical_location -> l) -> canonical p -> node l p.
End Micheline_signature.
Require Export Proto_2021_01.Environment.Micheline.
Module Micheline_check : Micheline_signature := Micheline.

Module Type Block_header_signature.
  Module shell_header.
    Record record : Set := Build {
      level : Int32.t;
      proto_level : int;
      predecessor : Block_hash.t;
      timestamp : Time.t;
      validation_passes : int;
      operations_hash : Operation_list_list_hash.t;
      fitness : list Bytes.t;
      context : Context_hash.t }.
    Definition with_level level (r : record) :=
      Build level r.(proto_level) r.(predecessor) r.(timestamp)
        r.(validation_passes) r.(operations_hash) r.(fitness) r.(context).
    Definition with_proto_level proto_level (r : record) :=
      Build r.(level) proto_level r.(predecessor) r.(timestamp)
        r.(validation_passes) r.(operations_hash) r.(fitness) r.(context).
    Definition with_predecessor predecessor (r : record) :=
      Build r.(level) r.(proto_level) predecessor r.(timestamp)
        r.(validation_passes) r.(operations_hash) r.(fitness) r.(context).
    Definition with_timestamp timestamp (r : record) :=
      Build r.(level) r.(proto_level) r.(predecessor) timestamp
        r.(validation_passes) r.(operations_hash) r.(fitness) r.(context).
    Definition with_validation_passes validation_passes (r : record) :=
      Build r.(level) r.(proto_level) r.(predecessor) r.(timestamp)
        validation_passes r.(operations_hash) r.(fitness) r.(context).
    Definition with_operations_hash operations_hash (r : record) :=
      Build r.(level) r.(proto_level) r.(predecessor) r.(timestamp)
        r.(validation_passes) operations_hash r.(fitness) r.(context).
    Definition with_fitness fitness (r : record) :=
      Build r.(level) r.(proto_level) r.(predecessor) r.(timestamp)
        r.(validation_passes) r.(operations_hash) fitness r.(context).
    Definition with_context context (r : record) :=
      Build r.(level) r.(proto_level) r.(predecessor) r.(timestamp)
        r.(validation_passes) r.(operations_hash) r.(fitness) context.
  End shell_header.
  Definition shell_header := shell_header.record.
  
  Parameter shell_header_encoding : Data_encoding.t shell_header.
  
  Module t.
    Record record : Set := Build {
      shell : shell_header;
      protocol_data : bytes }.
    Definition with_shell shell (r : record) :=
      Build shell r.(protocol_data).
    Definition with_protocol_data protocol_data (r : record) :=
      Build r.(shell) protocol_data.
  End t.
  Definition t := t.record.
  
  Parameter Included_HASHABLE : S.HASHABLE (t := t) (hash := Block_hash.t).
  
  Definition op_eq : t -> t -> bool := Included_HASHABLE.(S.HASHABLE.op_eq).
  
  Definition op_ltgt : t -> t -> bool := Included_HASHABLE.(S.HASHABLE.op_ltgt).
  
  Definition op_lt : t -> t -> bool := Included_HASHABLE.(S.HASHABLE.op_lt).
  
  Definition op_lteq : t -> t -> bool := Included_HASHABLE.(S.HASHABLE.op_lteq).
  
  Definition op_gteq : t -> t -> bool := Included_HASHABLE.(S.HASHABLE.op_gteq).
  
  Definition op_gt : t -> t -> bool := Included_HASHABLE.(S.HASHABLE.op_gt).
  
  Definition compare : t -> t -> int := Included_HASHABLE.(S.HASHABLE.compare).
  
  Definition equal : t -> t -> bool := Included_HASHABLE.(S.HASHABLE.equal).
  
  Definition max : t -> t -> t := Included_HASHABLE.(S.HASHABLE.max).
  
  Definition min : t -> t -> t := Included_HASHABLE.(S.HASHABLE.min).
  
  Definition pp : Format.formatter -> t -> unit :=
    Included_HASHABLE.(S.HASHABLE.pp).
  
  Definition encoding : Data_encoding.t t :=
    Included_HASHABLE.(S.HASHABLE.encoding).
  
  Definition to_bytes : t -> bytes := Included_HASHABLE.(S.HASHABLE.to_bytes).
  
  Definition of_bytes : bytes -> option t :=
    Included_HASHABLE.(S.HASHABLE.of_bytes).
  
  Definition hash_value : t -> Block_hash.t :=
    Included_HASHABLE.(S.HASHABLE.hash_value).
  
  Definition hash_raw : bytes -> Block_hash.t :=
    Included_HASHABLE.(S.HASHABLE.hash_raw).
End Block_header_signature.
Require Export Proto_2021_01.Environment.Block_header.
Module Block_header_check : Block_header_signature := Block_header.

Module Type Fitness_signature.
  Parameter Included_T : S.T (t := list bytes).
  
  Definition t := Included_T.(S.T.t).
  
  Definition op_eq : t -> t -> bool := Included_T.(S.T.op_eq).
  
  Definition op_ltgt : t -> t -> bool := Included_T.(S.T.op_ltgt).
  
  Definition op_lt : t -> t -> bool := Included_T.(S.T.op_lt).
  
  Definition op_lteq : t -> t -> bool := Included_T.(S.T.op_lteq).
  
  Definition op_gteq : t -> t -> bool := Included_T.(S.T.op_gteq).
  
  Definition op_gt : t -> t -> bool := Included_T.(S.T.op_gt).
  
  Definition compare : t -> t -> int := Included_T.(S.T.compare).
  
  Definition equal : t -> t -> bool := Included_T.(S.T.equal).
  
  Definition max : t -> t -> t := Included_T.(S.T.max).
  
  Definition min : t -> t -> t := Included_T.(S.T.min).
  
  Definition pp : Format.formatter -> t -> unit := Included_T.(S.T.pp).
  
  Definition encoding : Data_encoding.t t := Included_T.(S.T.encoding).
  
  Definition to_bytes : t -> bytes := Included_T.(S.T.to_bytes).
  
  Definition of_bytes : bytes -> option t := Included_T.(S.T.of_bytes).
End Fitness_signature.
Require Export Proto_2021_01.Environment.Fitness.
Module Fitness_check : Fitness_signature := Fitness.

Module Type Operation_signature.
  Module shell_header.
    Record record : Set := Build {
      branch : Block_hash.t }.
    Definition with_branch branch (r : record) :=
      Build branch.
  End shell_header.
  Definition shell_header := shell_header.record.
  
  Parameter shell_header_encoding : Data_encoding.t shell_header.
  
  Module t.
    Record record : Set := Build {
      shell : shell_header;
      proto : bytes }.
    Definition with_shell shell (r : record) :=
      Build shell r.(proto).
    Definition with_proto proto (r : record) :=
      Build r.(shell) proto.
  End t.
  Definition t := t.record.
  
  Parameter Included_HASHABLE : S.HASHABLE (t := t) (hash := Operation_hash.t).
  
  Definition op_eq : t -> t -> bool := Included_HASHABLE.(S.HASHABLE.op_eq).
  
  Definition op_ltgt : t -> t -> bool := Included_HASHABLE.(S.HASHABLE.op_ltgt).
  
  Definition op_lt : t -> t -> bool := Included_HASHABLE.(S.HASHABLE.op_lt).
  
  Definition op_lteq : t -> t -> bool := Included_HASHABLE.(S.HASHABLE.op_lteq).
  
  Definition op_gteq : t -> t -> bool := Included_HASHABLE.(S.HASHABLE.op_gteq).
  
  Definition op_gt : t -> t -> bool := Included_HASHABLE.(S.HASHABLE.op_gt).
  
  Definition compare : t -> t -> int := Included_HASHABLE.(S.HASHABLE.compare).
  
  Definition equal : t -> t -> bool := Included_HASHABLE.(S.HASHABLE.equal).
  
  Definition max : t -> t -> t := Included_HASHABLE.(S.HASHABLE.max).
  
  Definition min : t -> t -> t := Included_HASHABLE.(S.HASHABLE.min).
  
  Definition pp : Format.formatter -> t -> unit :=
    Included_HASHABLE.(S.HASHABLE.pp).
  
  Definition encoding : Data_encoding.t t :=
    Included_HASHABLE.(S.HASHABLE.encoding).
  
  Definition to_bytes : t -> bytes := Included_HASHABLE.(S.HASHABLE.to_bytes).
  
  Definition of_bytes : bytes -> option t :=
    Included_HASHABLE.(S.HASHABLE.of_bytes).
  
  Definition hash_value : t -> Operation_hash.t :=
    Included_HASHABLE.(S.HASHABLE.hash_value).
  
  Definition hash_raw : bytes -> Operation_hash.t :=
    Included_HASHABLE.(S.HASHABLE.hash_raw).
End Operation_signature.
Require Export Proto_2021_01.Environment.Operation.
Module Operation_check : Operation_signature := Operation.

Module Type Protocol_signature.
  Module component.
    Record record {name interface implementation : Set} : Set := Build {
      name : name;
      interface : interface;
      implementation : implementation }.
    Arguments record : clear implicits.
    Definition with_name {t_name t_interface t_implementation} name
      (r : record t_name t_interface t_implementation) :=
      Build t_name t_interface t_implementation name r.(interface)
        r.(implementation).
    Definition with_interface {t_name t_interface t_implementation} interface
      (r : record t_name t_interface t_implementation) :=
      Build t_name t_interface t_implementation r.(name) interface
        r.(implementation).
    Definition with_implementation {t_name t_interface t_implementation}
      implementation (r : record t_name t_interface t_implementation) :=
      Build t_name t_interface t_implementation r.(name) r.(interface)
        implementation.
  End component.
  Definition component_skeleton := component.record.
  
  Module t.
    Record record {expected_env components : Set} : Set := Build {
      expected_env : expected_env;
      components : components }.
    Arguments record : clear implicits.
    Definition with_expected_env {t_expected_env t_components} expected_env
      (r : record t_expected_env t_components) :=
      Build t_expected_env t_components expected_env r.(components).
    Definition with_components {t_expected_env t_components} components
      (r : record t_expected_env t_components) :=
      Build t_expected_env t_components r.(expected_env) components.
  End t.
  Definition t_skeleton := t.record.
  
  Reserved Notation "'t".
  Reserved Notation "'component".
  
  Inductive env_version : Set :=
  | V0 : env_version
  | V1 : env_version
  | V2 : env_version
  
  where "'component" := (component_skeleton string (option string) string)
  and "'t" := (t_skeleton env_version (list 'component)).
  
  Definition t := 't.
  Definition component := 'component.
  
  Parameter component_encoding : Data_encoding.t component.
  
  Parameter env_version_encoding : Data_encoding.t env_version.
  
  Parameter Included_HASHABLE : S.HASHABLE (t := t) (hash := Protocol_hash.t).
  
  Definition op_eq : t -> t -> bool := Included_HASHABLE.(S.HASHABLE.op_eq).
  
  Definition op_ltgt : t -> t -> bool := Included_HASHABLE.(S.HASHABLE.op_ltgt).
  
  Definition op_lt : t -> t -> bool := Included_HASHABLE.(S.HASHABLE.op_lt).
  
  Definition op_lteq : t -> t -> bool := Included_HASHABLE.(S.HASHABLE.op_lteq).
  
  Definition op_gteq : t -> t -> bool := Included_HASHABLE.(S.HASHABLE.op_gteq).
  
  Definition op_gt : t -> t -> bool := Included_HASHABLE.(S.HASHABLE.op_gt).
  
  Definition compare : t -> t -> int := Included_HASHABLE.(S.HASHABLE.compare).
  
  Definition equal : t -> t -> bool := Included_HASHABLE.(S.HASHABLE.equal).
  
  Definition max : t -> t -> t := Included_HASHABLE.(S.HASHABLE.max).
  
  Definition min : t -> t -> t := Included_HASHABLE.(S.HASHABLE.min).
  
  Definition pp : Format.formatter -> t -> unit :=
    Included_HASHABLE.(S.HASHABLE.pp).
  
  Definition encoding : Data_encoding.t t :=
    Included_HASHABLE.(S.HASHABLE.encoding).
  
  Definition to_bytes : t -> bytes := Included_HASHABLE.(S.HASHABLE.to_bytes).
  
  Definition of_bytes : bytes -> option t :=
    Included_HASHABLE.(S.HASHABLE.of_bytes).
  
  Definition hash_value : t -> Protocol_hash.t :=
    Included_HASHABLE.(S.HASHABLE.hash_value).
  
  Definition hash_raw : bytes -> Protocol_hash.t :=
    Included_HASHABLE.(S.HASHABLE.hash_raw).
End Protocol_signature.
Require Export Proto_2021_01.Environment.Protocol.
Module Protocol_check : Protocol_signature := Protocol.

Module Type Context_signature.
  Parameter t : Set.
  
  Definition key : Set := list string.
  
  Definition value : Set := bytes.
  
  Parameter mem : t -> key -> Lwt.t bool.
  
  Parameter dir_mem : t -> key -> Lwt.t bool.
  
  Parameter get : t -> key -> Lwt.t (option value).
  
  Parameter set : t -> key -> value -> Lwt.t t.
  
  Parameter copy : t -> key -> key -> Lwt.t (option t).
  
  Parameter remove_rec : t -> key -> Lwt.t t.
  
  Inductive key_or_dir : Set :=
  | Dir : key -> key_or_dir
  | Key : key -> key_or_dir.
  
  Parameter fold : forall {a : Set},
    t -> key -> a -> (key_or_dir -> a -> Lwt.t a) -> Lwt.t a.
  
  Parameter keys : t -> key -> Lwt.t (list key).
  
  Parameter fold_keys : forall {a : Set},
    t -> key -> a -> (key -> a -> Lwt.t a) -> Lwt.t a.
  
  Parameter register_resolver : forall {a : Set},
    Base58.encoding a -> (t -> string -> Lwt.t (list a)) -> unit.
  
  Parameter complete : t -> string -> Lwt.t (list string).
End Context_signature.
Require Export Proto_2021_01.Environment.Context.
Module Context_check : Context_signature := Context.

Module Type Updater_signature.
  Module validation_result.
    Record record : Set := Build {
      context : Context.t;
      fitness : Fitness.t;
      message : option string;
      max_operations_ttl : int;
      last_allowed_fork_level : Int32.t }.
    Definition with_context context (r : record) :=
      Build context r.(fitness) r.(message) r.(max_operations_ttl)
        r.(last_allowed_fork_level).
    Definition with_fitness fitness (r : record) :=
      Build r.(context) fitness r.(message) r.(max_operations_ttl)
        r.(last_allowed_fork_level).
    Definition with_message message (r : record) :=
      Build r.(context) r.(fitness) message r.(max_operations_ttl)
        r.(last_allowed_fork_level).
    Definition with_max_operations_ttl max_operations_ttl (r : record) :=
      Build r.(context) r.(fitness) r.(message) max_operations_ttl
        r.(last_allowed_fork_level).
    Definition with_last_allowed_fork_level last_allowed_fork_level
      (r : record) :=
      Build r.(context) r.(fitness) r.(message) r.(max_operations_ttl)
        last_allowed_fork_level.
  End validation_result.
  Definition validation_result := validation_result.record.
  
  Module quota.
    Record record : Set := Build {
      max_size : int;
      max_op : option int }.
    Definition with_max_size max_size (r : record) :=
      Build max_size r.(max_op).
    Definition with_max_op max_op (r : record) :=
      Build r.(max_size) max_op.
  End quota.
  Definition quota := quota.record.
  
  Module rpc_context.
    Record record : Set := Build {
      block_hash : Block_hash.t;
      block_header : Block_header.shell_header;
      context : Context.t }.
    Definition with_block_hash block_hash (r : record) :=
      Build block_hash r.(block_header) r.(context).
    Definition with_block_header block_header (r : record) :=
      Build r.(block_hash) block_header r.(context).
    Definition with_context context (r : record) :=
      Build r.(block_hash) r.(block_header) context.
  End rpc_context.
  Definition rpc_context := rpc_context.record.
  
  Module PROTOCOL.
    Record signature
      {block_header_data block_header block_header_metadata operation_data
        operation_receipt operation validation_state : Set} : Set := {
      (** The maximum size of a block header in bytes. *)
      max_block_length : int;
      (** The maximum size of an operation in bytes. *)
      max_operation_data_length : int;
      (** The number of validation passes (length of the list) and the
      operation's quota for each pass. *)
      validation_passes : list quota;
      (** The version specific type of blocks. *)
      block_header_data := block_header_data;
      (** Encoding for version specific part of block headers. *)
      block_header_data_encoding : Data_encoding.t block_header_data;
      (** A fully parsed block header. *)
      block_header := block_header;
      (** Version-specific side information computed by the protocol
      during the validation of a block. Should not include information
      about the evaluation of operations which is handled separately by
      {!operation_metadata}. To be used as an execution trace by tools
      (client, indexer). Not necessary for validation. *)
      block_header_metadata := block_header_metadata;
      (** Encoding for version-specific block metadata. *)
      block_header_metadata_encoding : Data_encoding.t block_header_metadata;
      (** The version specific type of operations. *)
      operation_data := operation_data;
      (** Version-specific side information computed by the protocol
      during the validation of each operation, to be used conjointly
      with {!block_header_metadata}. *)
      operation_receipt := operation_receipt;
      (** A fully parsed operation. *)
      operation := operation;
      (** Encoding for version-specific operation data. *)
      operation_data_encoding : Data_encoding.t operation_data;
      (** Encoding for version-specific operation receipts. *)
      operation_receipt_encoding : Data_encoding.t operation_receipt;
      (** Encoding that mixes an operation data and its receipt. *)
      operation_data_and_receipt_encoding :
        Data_encoding.t (operation_data * operation_receipt);
      (** The Validation passes in which an operation can appear.
      For instance [[0]] if it only belongs to the first pass.
      An answer of [[]] means that the operation is ill-formed
      and cannot be included at all. *)
      acceptable_passes : operation -> list int;
      (** Basic ordering of operations. [compare_operations op1 op2] means
      that [op1] should appear before [op2] in a block. *)
      compare_operations : operation -> operation -> int;
      (** A functional state that is transmitted through the steps of a
      block validation sequence. It must retain the current state of
      the store (that can be extracted from the outside using
      {!current_context}, and whose final value is produced by
      {!finalize_block}). It can also contain the information that
      must be remembered during the validation, which must be
      immutable (as validator or baker implementations are allowed to
      pause, replay or backtrack during the validation process). *)
      validation_state := validation_state;
      (** Access the context at a given validation step. *)
      current_context :
        validation_state -> Lwt.t (Error_monad.tzresult Context.t);
      (** Checks that a block is well formed in a given context. This
      function should run quickly, as its main use is to reject bad
      blocks from the chain as early as possible. The input context
      is the one resulting of an ancestor block of same protocol
      version. This ancestor of the current head is guaranteed to be
      more recent than `last_allowed_fork_level`.

      The resulting `validation_state` will be used for multi-pass
      validation. *)
      begin_partial_application :
        Chain_id.t -> Context.t -> Time.t -> Fitness.t -> block_header ->
        Lwt.t (Error_monad.tzresult validation_state);
      (** The first step in a block validation sequence. Initializes a
      validation context for validating a block. Takes as argument the
      {!Block_header.t} to initialize the context for this block. The
      function {!precheck_block} may not have been called before
      [begin_application], so all the check performed by the former
      must be repeated in the latter. *)
      begin_application :
        Chain_id.t -> Context.t -> Time.t -> Fitness.t -> block_header ->
        Lwt.t (Error_monad.tzresult validation_state);
      (** Initializes a validation context for constructing a new block
      (as opposed to validating an existing block). When the
      [protocol_data] argument is specified, it should contains a
      'prototype' of a the protocol specific part of a block header,
      and the function should produce the exact same effect on the
      context than would produce the validation of a block containing
      an "equivalent" (but complete) header. For instance, if the
      block header usually includes a signature, the header provided
      to {!begin_construction} should includes a faked signature. *)
      begin_construction :
        Chain_id.t -> Context.t -> Time.t -> Int32.t -> Fitness.t ->
        Block_hash.t -> Time.t -> option block_header_data -> unit ->
        Lwt.t (Error_monad.tzresult validation_state);
      (** Called after {!begin_application} (or {!begin_construction}) and
      before {!finalize_block}, with each operation in the block. *)
      apply_operation :
        validation_state -> operation ->
        Lwt.t (Error_monad.tzresult (validation_state * operation_receipt));
      (** The last step in a block validation sequence. It produces the
      context that will be used as input for the validation of its
      successor block candidates. *)
      finalize_block :
        validation_state ->
        Lwt.t (Error_monad.tzresult (validation_result * block_header_metadata));
      (** The list of remote procedures exported by this implementation *)
      rpc_services : RPC_directory.t rpc_context;
      (** Initialize the context (or upgrade the context after a protocol
      amendment). This function receives the context resulting of the
      application of a block that triggered the amendment. It also
      receives the header of the block that triggered the amendment. *)
      init_value :
        Context.t -> Block_header.shell_header ->
        Lwt.t (Error_monad.tzresult validation_result);
    }.
  End PROTOCOL.
  Definition PROTOCOL := @PROTOCOL.signature.
  Arguments PROTOCOL {_ _ _ _ _ _ _}.
  
  Parameter activate : Context.t -> Protocol_hash.t -> Lwt.t Context.t.
  
  Parameter fork_test_chain :
    Context.t -> Protocol_hash.t -> Time.t -> Lwt.t Context.t.
End Updater_signature.
Require Export Proto_2021_01.Environment.Updater.
Module Updater_check : Updater_signature := Updater.

Module Type RPC_context_signature.
  Definition t : Set := Updater.rpc_context.
  
  Module simple.
    Record record {pr : Set} : Set := Build {
      call_proto_service0 :
        forall {q i o : Set},
        RPC_service.t t t q i o -> pr -> q -> i ->
        Lwt.t (Error_monad.shell_tzresult o);
      call_proto_service1 :
        forall {a q i o : Set},
        RPC_service.t t (t * a) q i o -> pr -> a -> q -> i ->
        Lwt.t (Error_monad.shell_tzresult o);
      call_proto_service2 :
        forall {a b q i o : Set},
        RPC_service.t t ((t * a) * b) q i o -> pr -> a -> b -> q -> i ->
        Lwt.t (Error_monad.shell_tzresult o);
      call_proto_service3 :
        forall {a b c q i o : Set},
        RPC_service.t t (((t * a) * b) * c) q i o -> pr -> a -> b -> c -> q ->
        i -> Lwt.t (Error_monad.shell_tzresult o) }.
    Arguments record : clear implicits.
  End simple.
  Definition simple := simple.record.
  
  Parameter make_call0 : forall {i o pr q : Set},
    RPC_service.t t t q i o -> simple pr -> pr -> q -> i ->
    Lwt.t (Error_monad.shell_tzresult o).
  
  Parameter make_call1 : forall {a i o pr q : Set},
    RPC_service.t t (t * a) q i o -> simple pr -> pr -> a -> q -> i ->
    Lwt.t (Error_monad.shell_tzresult o).
  
  Parameter make_call2 : forall {a b i o pr q : Set},
    RPC_service.t t ((t * a) * b) q i o -> simple pr -> pr -> a -> b -> q ->
    i -> Lwt.t (Error_monad.shell_tzresult o).
  
  Parameter make_call3 : forall {a b c i o pr q : Set},
    RPC_service.t t (((t * a) * b) * c) q i o -> simple pr -> pr -> a -> b ->
    c -> q -> i -> Lwt.t (Error_monad.shell_tzresult o).
  
  Parameter make_opt_call0 : forall {i o pr q : Set},
    RPC_service.t t t q i o -> simple pr -> pr -> q -> i ->
    Lwt.t (Error_monad.shell_tzresult (option o)).
  
  Parameter make_opt_call1 : forall {a i o pr q : Set},
    RPC_service.t t (t * a) q i o -> simple pr -> pr -> a -> q -> i ->
    Lwt.t (Error_monad.shell_tzresult (option o)).
  
  Parameter make_opt_call2 : forall {a b i o pr q : Set},
    RPC_service.t t ((t * a) * b) q i o -> simple pr -> pr -> a -> b -> q ->
    i -> Lwt.t (Error_monad.shell_tzresult (option o)).
  
  Parameter make_opt_call3 : forall {a b c i o pr q : Set},
    RPC_service.t t (((t * a) * b) * c) q i o -> simple pr -> pr -> a -> b ->
    c -> q -> i -> Lwt.t (Error_monad.shell_tzresult (option o)).
End RPC_context_signature.
Require Export Proto_2021_01.Environment.RPC_context.
Module RPC_context_check : RPC_context_signature := RPC_context.

Module Notations.
  Export Compare.Notations.
  Export Error_monad.Notations.
  Export Int32.Notations.
  Export Int64.Notations.
  Export Lwt.Notations.
  Export Option.Notations.
  Export Pervasives.Notations.
  Export Z.Notations.
End Notations.
Export Notations.
