Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Blinded_public_key_hash.
Require TezosOfOCaml.Proto_2021_01.Contract_repr.
Require TezosOfOCaml.Proto_2021_01.Cycle_repr.
Require TezosOfOCaml.Proto_2021_01.Gas_limit_repr.
Require TezosOfOCaml.Proto_2021_01.Lazy_storage_kind.
Require TezosOfOCaml.Proto_2021_01.Level_repr.
Require TezosOfOCaml.Proto_2021_01.Manager_repr.
Require TezosOfOCaml.Proto_2021_01.Misc.
Require TezosOfOCaml.Proto_2021_01.Nonce_hash.
Require TezosOfOCaml.Proto_2021_01.Raw_context.
Require TezosOfOCaml.Proto_2021_01.Raw_level_repr.
Require TezosOfOCaml.Proto_2021_01.Receipt_repr.
Require TezosOfOCaml.Proto_2021_01.Roll_repr.
Require TezosOfOCaml.Proto_2021_01.Sapling_repr.
Require TezosOfOCaml.Proto_2021_01.Script_expr_hash.
Require TezosOfOCaml.Proto_2021_01.Script_repr.
Require TezosOfOCaml.Proto_2021_01.Seed_repr.
Require TezosOfOCaml.Proto_2021_01.Storage_description.
Require TezosOfOCaml.Proto_2021_01.Storage_functors.
Require TezosOfOCaml.Proto_2021_01.Storage_sigs.
Require TezosOfOCaml.Proto_2021_01.Tez_repr.
Require TezosOfOCaml.Proto_2021_01.Vote_repr.
Require TezosOfOCaml.Proto_2021_01.Voting_period_repr.

Module Encoding.
  Module UInt16.
    Definition t : Set := int.
    
    Definition encoding : Data_encoding.encoding int := Data_encoding.uint16.
    
    Definition module :=
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}.
  End UInt16.
  Definition UInt16 := UInt16.module.
  
  Module Int32.
    Definition t : Set := Int32.t.
    
    Definition encoding : Data_encoding.encoding int32 :=
      Data_encoding.int32_value.
    
    Definition module :=
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}.
  End Int32.
  Definition Int32 := Int32.module.
  
  Module Int64.
    Definition t : Set := Int64.t.
    
    Definition encoding : Data_encoding.encoding int64 :=
      Data_encoding.int64_value.
    
    Definition module :=
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}.
  End Int64.
  Definition Int64 := Int64.module.
  
  Module Z.
    Definition t : Set := Z.t.
    
    Definition encoding : Data_encoding.encoding Z.t := Data_encoding.z.
    
    Definition module :=
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}.
  End Z.
  Definition Z := Z.module.
End Encoding.

Module Int31_index.
  Definition t : Set := int.
  
  Definition path_length : int := 1.
  
  Definition to_path (c : int) (l_value : list string) : list string :=
    cons (Pervasives.string_of_int c) l_value.
  
  Definition of_path (function_parameter : list string) : option int :=
    match function_parameter with
    | ([] | cons _ (cons _ _)) => None
    | cons c [] => Pervasives.int_of_string_opt c
    end.
  
  Definition ipath (a : Set) : Set := a * t.
  
  Definition args : Storage_description.args :=
    Storage_description.One
      {| Storage_description.args.One.rpc_arg := RPC_arg.int_value;
        Storage_description.args.One.encoding := Data_encoding.int31;
        Storage_description.args.One.compare := Compare.Int.(Compare.S.compare)
        |}.
  
  Definition module : Storage_functors.INDEX.signature (ipath := ipath) := {|
      Storage_functors.INDEX.path_length := path_length;
      Storage_functors.INDEX.to_path := to_path;
      Storage_functors.INDEX.of_path := of_path;
      Storage_functors.INDEX.args := args
    |}.
End Int31_index.
Definition Int31_index : Storage_functors.INDEX (t := int) (ipath := _) :=
  Int31_index.module.

Module Make_index.
  Class FArgs {H_t : Set} := {
    H : Storage_description.INDEX (t := H_t);
  }.
  Arguments Build_FArgs {_}.
  
  (** Inclusion of the module [H] *)
  Definition t `{FArgs} := H.(Storage_description.INDEX.t).
  
  Definition path_length `{FArgs} := H.(Storage_description.INDEX.path_length).
  
  Definition to_path `{FArgs} := H.(Storage_description.INDEX.to_path).
  
  Definition of_path `{FArgs} := H.(Storage_description.INDEX.of_path).
  
  Definition rpc_arg `{FArgs} := H.(Storage_description.INDEX.rpc_arg).
  
  Definition encoding `{FArgs} := H.(Storage_description.INDEX.encoding).
  
  Definition compare `{FArgs} := H.(Storage_description.INDEX.compare).
  
  Definition ipath `{FArgs} (a : Set) : Set := a * t.
  
  Definition args `{FArgs} : Storage_description.args :=
    Storage_description.One
      {| Storage_description.args.One.rpc_arg := rpc_arg;
        Storage_description.args.One.encoding := encoding;
        Storage_description.args.One.compare := compare |}.
  
  Definition functor `{FArgs} : Storage_functors.INDEX.signature (ipath := ipath) := {|
      Storage_functors.INDEX.path_length := path_length;
      Storage_functors.INDEX.to_path := to_path;
      Storage_functors.INDEX.of_path := of_path;
      Storage_functors.INDEX.args := args
    |}.
End Make_index.
Definition Make_index {H_t : Set} (H : Storage_description.INDEX (t := H_t))
  : Storage_functors.INDEX (t := H.(Storage_description.INDEX.t))
    (ipath := fun (a : Set) => a * H.(Storage_description.INDEX.t)) :=
  let '_ := Make_index.Build_FArgs H in
  Make_index.functor.

Module GET_SET_INIT.
  Record signature {value : Set} : Set := {
    value := value;
    get : Raw_context.t -> M=? value;
    set : Raw_context.t -> value -> M=? Raw_context.t;
    init_value : Raw_context.t -> value -> M=? Raw_context.t;
  }.
End GET_SET_INIT.
Definition GET_SET_INIT := @GET_SET_INIT.signature.
Arguments GET_SET_INIT {_}.

Module Block_priority.
  Definition Storage :
    Storage_sigs.Single_data_storage (t := Raw_context.t) (value := int) :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      {|
        Raw_context.T.mem := Raw_context.mem;
        Raw_context.T.dir_mem := Raw_context.dir_mem;
        Raw_context.T.get := Raw_context.get;
        Raw_context.T.get_option := Raw_context.get_option;
        Raw_context.T.init_value := Raw_context.init_value;
        Raw_context.T.set := Raw_context.set;
        Raw_context.T.init_set := Raw_context.init_set;
        Raw_context.T.set_option := Raw_context.set_option;
        Raw_context.T.delete := Raw_context.delete;
        Raw_context.T.remove := Raw_context.remove;
        Raw_context.T.remove_rec := Raw_context.remove_rec;
        Raw_context.T.copy := Raw_context.copy;
        Raw_context.T.fold _ := Raw_context.fold;
        Raw_context.T.keys := Raw_context.keys;
        Raw_context.T.fold_keys _ := Raw_context.fold_keys;
        Raw_context.T.project := Raw_context.project;
        Raw_context.T.absolute_key := Raw_context.absolute_key;
        Raw_context.T.consume_gas := Raw_context.consume_gas;
        Raw_context.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context.T.description := Raw_context.description
      |}
      (let name := [ "block_priority" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.UInt16.
  
  (** Inclusion of the module [Storage] *)
  Definition context := Storage.(Storage_sigs.Single_data_storage.context).
  
  Definition value := Storage.(Storage_sigs.Single_data_storage.value).
  
  Definition mem := Storage.(Storage_sigs.Single_data_storage.mem).
  
  Definition get := Storage.(Storage_sigs.Single_data_storage.get).
  
  Definition get_option :=
    Storage.(Storage_sigs.Single_data_storage.get_option).
  
  Definition init_value :=
    Storage.(Storage_sigs.Single_data_storage.init_value).
  
  Definition set := Storage.(Storage_sigs.Single_data_storage.set).
  
  Definition init_set := Storage.(Storage_sigs.Single_data_storage.init_set).
  
  Definition set_option :=
    Storage.(Storage_sigs.Single_data_storage.set_option).
  
  Definition delete := Storage.(Storage_sigs.Single_data_storage.delete).
  
  Definition remove := Storage.(Storage_sigs.Single_data_storage.remove).
  
  Definition module :=
    {|
      GET_SET_INIT.get := get;
      GET_SET_INIT.set := set;
      GET_SET_INIT.init_value := init_value
    |}.
End Block_priority.
Definition Block_priority : GET_SET_INIT (value := int) :=
  Block_priority.module.

Module Contract.
  Definition Raw_context : Raw_context.T (t := Raw_context.t) :=
    Storage_functors.Make_subcontext Storage_functors.Registered
      {|
        Raw_context.T.mem := Raw_context.mem;
        Raw_context.T.dir_mem := Raw_context.dir_mem;
        Raw_context.T.get := Raw_context.get;
        Raw_context.T.get_option := Raw_context.get_option;
        Raw_context.T.init_value := Raw_context.init_value;
        Raw_context.T.set := Raw_context.set;
        Raw_context.T.init_set := Raw_context.init_set;
        Raw_context.T.set_option := Raw_context.set_option;
        Raw_context.T.delete := Raw_context.delete;
        Raw_context.T.remove := Raw_context.remove;
        Raw_context.T.remove_rec := Raw_context.remove_rec;
        Raw_context.T.copy := Raw_context.copy;
        Raw_context.T.fold _ := Raw_context.fold;
        Raw_context.T.keys := Raw_context.keys;
        Raw_context.T.fold_keys _ := Raw_context.fold_keys;
        Raw_context.T.project := Raw_context.project;
        Raw_context.T.absolute_key := Raw_context.absolute_key;
        Raw_context.T.consume_gas := Raw_context.consume_gas;
        Raw_context.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context.T.description := Raw_context.description
      |}
      (let name := [ "contracts" ] in
      {|
        Storage_sigs.NAME.name := name
      |}).
  
  Module Global_counter.
    Definition Storage :
      Storage_sigs.Single_data_storage (t := Raw_context.t) (value := Z.t) :=
      Storage_functors.Make_single_data_storage Storage_functors.Registered
        Raw_context
        (let name := [ "global_counter" ] in
        {|
          Storage_sigs.NAME.name := name
        |}) Encoding.Z.
    
    (** Inclusion of the module [Storage] *)
    Definition context := Storage.(Storage_sigs.Single_data_storage.context).
    
    Definition value := Storage.(Storage_sigs.Single_data_storage.value).
    
    Definition mem := Storage.(Storage_sigs.Single_data_storage.mem).
    
    Definition get := Storage.(Storage_sigs.Single_data_storage.get).
    
    Definition get_option :=
      Storage.(Storage_sigs.Single_data_storage.get_option).
    
    Definition init_value :=
      Storage.(Storage_sigs.Single_data_storage.init_value).
    
    Definition set := Storage.(Storage_sigs.Single_data_storage.set).
    
    Definition init_set := Storage.(Storage_sigs.Single_data_storage.init_set).
    
    Definition set_option :=
      Storage.(Storage_sigs.Single_data_storage.set_option).
    
    Definition delete := Storage.(Storage_sigs.Single_data_storage.delete).
    
    Definition remove := Storage.(Storage_sigs.Single_data_storage.remove).
    
    Definition module :=
      {|
        GET_SET_INIT.get := get;
        GET_SET_INIT.set := set;
        GET_SET_INIT.init_value := init_value
      |}.
  End Global_counter.
  Definition Global_counter : GET_SET_INIT (value := Z.t) :=
    Global_counter.module.
  
  Definition Indexed_context :
    Storage_sigs.Indexed_raw_context (t := Raw_context.t)
      (key := Contract_repr.t) (ipath := fun (a : Set) => a * Contract_repr.t)
    :=
    Storage_functors.Make_indexed_subcontext
      (Storage_functors.Make_subcontext Storage_functors.Registered Raw_context
        (let name := [ "index" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) (Make_index Contract_repr.Index).
  
  Definition fold {A : Set}
    : Raw_context.t -> A -> (Contract_repr.t -> A -> M= A) -> M= A :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.fold_keys).
  
  Definition list_value : Raw_context.t -> M= (list Contract_repr.t) :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.keys).
  
  Definition Balance :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t)
      (key := Contract_repr.t) (value := Tez_repr.t) :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "balance" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Tez_repr.encoding
      |}.
  
  Definition Frozen_balance_index :
    Storage_sigs.Indexed_raw_context
      (t :=
        Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context.T.t))
      (key := Cycle_repr.t) (ipath := fun (a : Set) => a * Cycle_repr.t) :=
    Storage_functors.Make_indexed_subcontext
      (Storage_functors.Make_subcontext Storage_functors.Registered
        Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
        (let name := [ "frozen_balance" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) (Make_index Cycle_repr.Index).
  
  Definition Frozen_deposits :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t * Contract_repr.t)
      (key := Cycle_repr.t) (value := Tez_repr.t) :=
    Frozen_balance_index.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "deposits" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Tez_repr.encoding
      |}.
  
  Definition Frozen_fees :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t * Contract_repr.t)
      (key := Cycle_repr.t) (value := Tez_repr.t) :=
    Frozen_balance_index.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "fees" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Tez_repr.encoding
      |}.
  
  Definition Frozen_rewards :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t * Contract_repr.t)
      (key := Cycle_repr.t) (value := Tez_repr.t) :=
    Frozen_balance_index.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "rewards" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Tez_repr.encoding
      |}.
  
  Definition Manager :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t)
      (key := Contract_repr.t) (value := Manager_repr.t) :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "manager" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Manager_repr.encoding
      |}.
  
  Definition Delegate :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t)
      (key := Contract_repr.t) (value := Signature.public_key_hash) :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "delegate" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding :=
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)
      |}.
  
  Definition Inactive_delegate :
    Storage_sigs.Data_set_storage (t := Raw_context.t) (elt := Contract_repr.t)
    :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_set)
      Storage_functors.Registered
      (let name := [ "inactive_delegate" ] in
      {|
        Storage_sigs.NAME.name := name
      |}).
  
  Definition Delegate_desactivation :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t)
      (key := Contract_repr.t) (value := Cycle_repr.t) :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "delegate_desactivation" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Cycle_repr.encoding
      |}.
  
  Definition Delegated :
    Storage_sigs.Data_set_storage (t := Raw_context.t * Contract_repr.t)
      (elt := Contract_repr.t) :=
    Storage_functors.Make_data_set_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered
        Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
        (let name := [ "delegated" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) (Make_index Contract_repr.Index).
  
  Definition Counter :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t)
      (key := Contract_repr.t) (value := Z.t) :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "counter" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.Z.
  
  Module Make_carbonated_map_expr.
    Class FArgs := {
      N : Storage_sigs.NAME;
    }.
    
    Definition I `{FArgs} :=
      Indexed_context.(Storage_sigs.Indexed_raw_context.Make_carbonated_map) N
        (let t : Set := Script_repr.lazy_expr in
        let encoding := Script_repr.lazy_expr_encoding in
        {|
          Storage_sigs.VALUE.encoding := encoding
        |}).
    
    Definition context `{FArgs} : Set := Raw_context.t.
    
    Definition key `{FArgs} : Set := Contract_repr.t.
    
    Definition value `{FArgs} : Set := Script_repr.lazy_expr.
    
    Definition mem `{FArgs}
      : Raw_context.t -> Contract_repr.t -> M=? (Raw_context.t * bool) :=
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.mem).
    
    Definition delete `{FArgs}
      : Raw_context.t -> Contract_repr.t -> M=? (Raw_context.t * int) :=
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.delete).
    
    Definition remove `{FArgs}
      : Raw_context.t -> Contract_repr.t -> M=? (Raw_context.t * int * bool) :=
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove).
    
    Definition consume_deserialize_gas `{FArgs}
      (ctxt : Raw_context.t) (value : Script_repr.lazy_expr)
      : M? Raw_context.t :=
      let? '_ :=
        Raw_context.(Raw_context.T.check_enough_gas) ctxt
          (Script_repr.minimal_deserialize_cost value) in
      let? '(_value, value_cost) := Script_repr.force_decode value in
      Raw_context.(Raw_context.T.consume_gas) ctxt value_cost.
    
    Definition consume_serialize_gas `{FArgs}
      (ctxt : Raw_context.t) (value : Script_repr.lazy_expr)
      : M? Raw_context.t :=
      let? '(_value, value_cost) := Script_repr.force_bytes value in
      Raw_context.(Raw_context.T.consume_gas) ctxt value_cost.
    
    Definition get `{FArgs} (ctxt : Raw_context.t) (contract : Contract_repr.t)
      : M=? (Raw_context.t * Script_repr.lazy_expr) :=
      let=? '(ctxt, value) :=
        I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.get) ctxt
          contract in
      Lwt._return
        (let? ctxt := consume_deserialize_gas ctxt value in
        return? (ctxt, value)).
    
    Definition get_option `{FArgs}
      (ctxt : Raw_context.t) (contract : Contract_repr.t)
      : M=? (Raw_context.t * option Script_repr.lazy_expr) :=
      let=? '(ctxt, value_opt) :=
        I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.get_option)
          ctxt contract in
      Lwt._return
        match value_opt with
        | None => return? (ctxt, None)
        | Some value =>
          let? ctxt := consume_deserialize_gas ctxt value in
          return? (ctxt, value_opt)
        end.
    
    Definition set `{FArgs}
      (ctxt : Raw_context.t) (contract : Contract_repr.t)
      (value : Script_repr.lazy_expr) : M=? (Raw_context.t * int) :=
      let=? ctxt := return= (consume_serialize_gas ctxt value) in
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.set) ctxt
        contract value.
    
    Definition set_option `{FArgs}
      (ctxt : Raw_context.t) (contract : Contract_repr.t)
      (value_opt : option Script_repr.lazy_expr)
      : M=? (Raw_context.t * int * bool) :=
      match value_opt with
      | None =>
        I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.set_option)
          ctxt contract None
      | Some value =>
        let=? ctxt := return= (consume_serialize_gas ctxt value) in
        I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.set_option)
          ctxt contract value_opt
      end.
    
    Definition init_value `{FArgs}
      (ctxt : Raw_context.t) (contract : Contract_repr.t)
      (value : Script_repr.lazy_expr) : M=? (Raw_context.t * int) :=
      let=? ctxt := return= (consume_serialize_gas ctxt value) in
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_value)
        ctxt contract value.
    
    Definition init_set `{FArgs}
      (ctxt : Raw_context.t) (contract : Contract_repr.t)
      (value : Script_repr.lazy_expr) : M=? (Raw_context.t * int * bool) :=
      let=? ctxt := return= (consume_serialize_gas ctxt value) in
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_set)
        ctxt contract value.
    
    Definition functor `{FArgs} :=
      {|
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.mem := mem;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.get := get;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.get_option :=
          get_option;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.set := set;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_value :=
          init_value;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_set :=
          init_set;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.set_option :=
          set_option;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.delete :=
          delete;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove :=
          remove
      |}.
  End Make_carbonated_map_expr.
  Definition Make_carbonated_map_expr (N : Storage_sigs.NAME)
    : Storage_sigs.Non_iterable_indexed_carbonated_data_storage
      (t := Raw_context.t) (key := Contract_repr.t)
      (value := Script_repr.lazy_expr) :=
    let '_ := Make_carbonated_map_expr.Build_FArgs N in
    Make_carbonated_map_expr.functor.
  
  Definition Code :
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage
      (t := Raw_context.t) (key := Contract_repr.t)
      (value := Script_repr.lazy_expr) :=
    Make_carbonated_map_expr
      (let name := [ "code" ] in
      {|
        Storage_sigs.NAME.name := name
      |}).
  
  Definition Storage :
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage
      (t := Raw_context.t) (key := Contract_repr.t)
      (value := Script_repr.lazy_expr) :=
    Make_carbonated_map_expr
      (let name := [ "storage" ] in
      {|
        Storage_sigs.NAME.name := name
      |}).
  
  Definition Paid_storage_space :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t)
      (key := Contract_repr.t) (value := Z.t) :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "paid_bytes" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.Z.
  
  Definition Used_storage_space :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t)
      (key := Contract_repr.t) (value := Z.t) :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "used_bytes" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.Z.
  
  Definition Roll_list :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t)
      (key := Contract_repr.t) (value := Roll_repr.t) :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "roll_list" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Roll_repr.encoding
      |}.
  
  Definition Change :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t)
      (key := Contract_repr.t) (value := Tez_repr.t) :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "change" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Tez_repr.encoding
      |}.
End Contract.

Module NEXT.
  Record signature {id : Set} : Set := {
    id := id;
    init_value : Raw_context.t -> M=? Raw_context.t;
    incr : Raw_context.t -> M=? (Raw_context.t * id);
  }.
End NEXT.
Definition NEXT := @NEXT.signature.
Arguments NEXT {_}.

Module Big_map.
  Definition id : Set :=
    Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t).
  
  Definition Raw_context : Raw_context.T (t := Raw_context.t) :=
    Storage_functors.Make_subcontext Storage_functors.Registered
      {|
        Raw_context.T.mem := Raw_context.mem;
        Raw_context.T.dir_mem := Raw_context.dir_mem;
        Raw_context.T.get := Raw_context.get;
        Raw_context.T.get_option := Raw_context.get_option;
        Raw_context.T.init_value := Raw_context.init_value;
        Raw_context.T.set := Raw_context.set;
        Raw_context.T.init_set := Raw_context.init_set;
        Raw_context.T.set_option := Raw_context.set_option;
        Raw_context.T.delete := Raw_context.delete;
        Raw_context.T.remove := Raw_context.remove;
        Raw_context.T.remove_rec := Raw_context.remove_rec;
        Raw_context.T.copy := Raw_context.copy;
        Raw_context.T.fold _ := Raw_context.fold;
        Raw_context.T.keys := Raw_context.keys;
        Raw_context.T.fold_keys _ := Raw_context.fold_keys;
        Raw_context.T.project := Raw_context.project;
        Raw_context.T.absolute_key := Raw_context.absolute_key;
        Raw_context.T.consume_gas := Raw_context.consume_gas;
        Raw_context.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context.T.description := Raw_context.description
      |}
      (let name := [ "big_maps" ] in
      {|
        Storage_sigs.NAME.name := name
      |}).
  
  Module Next.
    Definition Storage :=
      Storage_functors.Make_single_data_storage Storage_functors.Registered
        Raw_context
        (let name := [ "next" ] in
        {|
          Storage_sigs.NAME.name := name
        |})
        {|
          Storage_sigs.VALUE.encoding :=
            Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.encoding)
        |}.
    
    Definition incr (ctxt : Raw_context.t)
      : M=?
        (Raw_context.t *
          Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t)) :=
      let=? i := Storage.(Storage_sigs.Single_data_storage.get) ctxt in
      let=? ctxt :=
        Storage.(Storage_sigs.Single_data_storage.set) ctxt
          (Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.next) i)
        in
      return=? (ctxt, i).
    
    Definition init_value (ctxt : Raw_context.t) : M=? Raw_context.t :=
      Storage.(Storage_sigs.Single_data_storage.init_value) ctxt
        Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.init_value).
    
    Definition module :=
      {|
        NEXT.init_value := init_value;
        NEXT.incr := incr
      |}.
  End Next.
  Definition Next : NEXT (id := id) := Next.module.
  
  Module Index.
    Definition Id := Lazy_storage_kind.Big_map.Id.
    
    Definition t : Set :=
      Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t).
    
    Definition path_length : int :=
      6 +i Id.(Lazy_storage_kind.IdWithTemp.path_length).
    
    Definition to_path
      (c : Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t))
      (l_value : list string) : list string :=
      let raw_key :=
        Data_encoding.Binary.to_bytes_exn
          Id.(Lazy_storage_kind.IdWithTemp.encoding) c in
      let 'Hex.Hex index_key := Hex.of_bytes None (Raw_hashes.blake2b raw_key)
        in
      cons (String.sub index_key 0 2)
        (cons (String.sub index_key 2 2)
          (cons (String.sub index_key 4 2)
            (cons (String.sub index_key 6 2)
              (cons (String.sub index_key 8 2)
                (cons (String.sub index_key 10 2)
                  (Id.(Lazy_storage_kind.IdWithTemp.to_path) c l_value)))))).
    
    Definition of_path (function_parameter : list string)
      : option Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t) :=
      match function_parameter with
      |
        ([] | cons _ [] | cons _ (cons _ []) | cons _ (cons _ (cons _ [])) |
        cons _ (cons _ (cons _ (cons _ []))) |
        cons _ (cons _ (cons _ (cons _ (cons _ [])))) |
        cons _ (cons _ (cons _ (cons _ (cons _ (cons _ []))))) |
        cons _ (cons _ (cons _ (cons _ (cons _ (cons _ (cons _ (cons _ _))))))))
        => None
      |
        cons index1
          (cons index2
            (cons index3 (cons index4 (cons index5 (cons index6 tail))))) =>
        Option.map
          (fun (c :
            Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t)) =>
            let raw_key :=
              Data_encoding.Binary.to_bytes_exn
                Id.(Lazy_storage_kind.IdWithTemp.encoding) c in
            let 'Hex.Hex index_key :=
              Hex.of_bytes None (Raw_hashes.blake2b raw_key) in
            let '_ :=
              (* ❌ Assert instruction is not handled. *)
              assert unit
                (Compare.String.(Compare.S.op_eq) (String.sub index_key 0 2)
                  index1) in
            let '_ :=
              (* ❌ Assert instruction is not handled. *)
              assert unit
                (Compare.String.(Compare.S.op_eq) (String.sub index_key 2 2)
                  index2) in
            let '_ :=
              (* ❌ Assert instruction is not handled. *)
              assert unit
                (Compare.String.(Compare.S.op_eq) (String.sub index_key 4 2)
                  index3) in
            let '_ :=
              (* ❌ Assert instruction is not handled. *)
              assert unit
                (Compare.String.(Compare.S.op_eq) (String.sub index_key 6 2)
                  index4) in
            let '_ :=
              (* ❌ Assert instruction is not handled. *)
              assert unit
                (Compare.String.(Compare.S.op_eq) (String.sub index_key 8 2)
                  index5) in
            let '_ :=
              (* ❌ Assert instruction is not handled. *)
              assert unit
                (Compare.String.(Compare.S.op_eq) (String.sub index_key 10 2)
                  index6) in
            c) (Id.(Lazy_storage_kind.IdWithTemp.of_path) tail)
      end.
    
    Definition rpc_arg
      : RPC_arg.arg
        Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t) :=
      Id.(Lazy_storage_kind.IdWithTemp.rpc_arg).
    
    Definition encoding
      : Data_encoding.t
        Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t) :=
      Id.(Lazy_storage_kind.IdWithTemp.encoding).
    
    Definition compare
      : Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t) ->
      Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t) -> int :=
      Id.(Lazy_storage_kind.IdWithTemp.compare).
    
    Definition module :=
      {|
        Storage_description.INDEX.path_length := path_length;
        Storage_description.INDEX.to_path := to_path;
        Storage_description.INDEX.of_path := of_path;
        Storage_description.INDEX.rpc_arg := rpc_arg;
        Storage_description.INDEX.encoding := encoding;
        Storage_description.INDEX.compare := compare
      |}.
  End Index.
  Definition Index
    : Storage_description.INDEX
      (t := Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t)) :=
    Index.module.
  
  Definition Indexed_context :
    Storage_sigs.Indexed_raw_context (t := Raw_context.t)
      (key := Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t))
      (ipath :=
        fun (a : Set) =>
          a * Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t)) :=
    Storage_functors.Make_indexed_subcontext
      (Storage_functors.Make_subcontext Storage_functors.Registered Raw_context
        (let name := [ "index" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) (Make_index Index).
  
  Definition rpc_arg
    : RPC_arg.t Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t) :=
    Index.(Storage_description.INDEX.rpc_arg).
  
  Definition fold {A : Set}
    : Raw_context.t -> A ->
    (Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t) -> A -> M= A)
    -> M= A := Indexed_context.(Storage_sigs.Indexed_raw_context.fold_keys).
  
  Definition list_value
    : Raw_context.t ->
    M= (list Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t)) :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.keys).
  
  Definition remove_rec
    (ctxt : Raw_context.t)
    (n : Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t))
    : M= Raw_context.t :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.remove_rec) ctxt n.
  
  Definition copy
    (ctxt : Raw_context.t)
    (from : Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t))
    (to_ : Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t))
    : M=? Raw_context.t :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.copy) ctxt from to_.
  
  Definition key : Set :=
    Raw_context.t *
      Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t).
  
  Definition Total_bytes :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t) (key := id)
      (value := Z.t) :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "total_bytes" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.Z.
  
  Definition Key_type :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t) (key := id)
      (value := Script_repr.expr) :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "key_type" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      (let t : Set := Script_repr.expr in
      let encoding := Script_repr.expr_encoding in
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}).
  
  Definition Value_type :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t) (key := id)
      (value := Script_repr.expr) :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "value_type" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      (let t : Set := Script_repr.expr in
      let encoding := Script_repr.expr_encoding in
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}).
  
  Module Contents.
    Definition I :
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage
        (t :=
          Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context.T.t))
        (key := Script_expr_hash.t) (value := Script_repr.expr) :=
      Storage_functors.Make_indexed_carbonated_data_storage
        (Storage_functors.Make_subcontext Storage_functors.Registered
          Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
          (let name := [ "contents" ] in
          {|
            Storage_sigs.NAME.name := name
          |}))
        (Make_index
          {|
            Storage_description.INDEX.path_length :=
              Script_expr_hash.path_length;
            Storage_description.INDEX.to_path := Script_expr_hash.to_path;
            Storage_description.INDEX.of_path := Script_expr_hash.of_path;
            Storage_description.INDEX.rpc_arg := Script_expr_hash.rpc_arg;
            Storage_description.INDEX.encoding := Script_expr_hash.encoding;
            Storage_description.INDEX.compare := Script_expr_hash.compare
          |})
        (let t : Set := Script_repr.expr in
        let encoding := Script_repr.expr_encoding in
        {|
          Storage_sigs.VALUE.encoding := encoding
        |}).
    
    Definition context : Set :=
      Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context.T.t).
    
    Definition key : Set := Script_expr_hash.t.
    
    Definition value : Set := Script_repr.expr.
    
    Definition mem
      : Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context.T.t)
      -> Script_expr_hash.t -> M=? (Raw_context.t * bool) :=
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.mem).
    
    Definition delete
      : Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context.T.t)
      -> Script_expr_hash.t -> M=? (Raw_context.t * int) :=
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.delete).
    
    Definition remove
      : Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context.T.t)
      -> Script_expr_hash.t -> M=? (Raw_context.t * int * bool) :=
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove).
    
    Definition set
      : Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context.T.t)
      -> Script_expr_hash.t -> Script_repr.expr -> M=? (Raw_context.t * int) :=
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.set).
    
    Definition set_option
      : Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context.T.t)
      -> Script_expr_hash.t -> option Script_repr.expr ->
      M=? (Raw_context.t * int * bool) :=
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.set_option).
    
    Definition init_value
      : Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context.T.t)
      -> Script_expr_hash.t -> Script_repr.expr -> M=? (Raw_context.t * int) :=
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_value).
    
    Definition init_set
      : Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context.T.t)
      -> Script_expr_hash.t -> Script_repr.expr ->
      M=? (Raw_context.t * int * bool) :=
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_set).
    
    Definition consume_deserialize_gas
      (ctxt : Raw_context.t) (value : Script_repr.expr) : M? Raw_context.t :=
      Raw_context.(Raw_context.T.consume_gas) ctxt
        (Script_repr.deserialized_cost value).
    
    Definition get
      (ctxt :
        Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context.T.t))
      (contract : Script_expr_hash.t)
      : M=? (Raw_context.t * Script_repr.expr) :=
      let=? '(ctxt, value) :=
        I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.get) ctxt
          contract in
      Lwt._return
        (let? ctxt := consume_deserialize_gas ctxt value in
        return? (ctxt, value)).
    
    Definition get_option
      (ctxt :
        Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context.T.t))
      (contract : Script_expr_hash.t)
      : M=? (Raw_context.t * option Script_repr.expr) :=
      let=? '(ctxt, value_opt) :=
        I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.get_option)
          ctxt contract in
      Lwt._return
        match value_opt with
        | None => return? (ctxt, None)
        | Some value =>
          let? ctxt := consume_deserialize_gas ctxt value in
          return? (ctxt, value_opt)
        end.
    
    Definition module :=
      {|
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.mem := mem;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.get := get;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.get_option :=
          get_option;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.set := set;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_value :=
          init_value;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_set :=
          init_set;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.set_option :=
          set_option;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.delete :=
          delete;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove :=
          remove
      |}.
  End Contents.
  Definition Contents
    : Storage_sigs.Non_iterable_indexed_carbonated_data_storage (t := key)
      (key := Script_expr_hash.t) (value := Script_repr.expr) :=
    Contents.module.
End Big_map.

Module Sapling.
  Definition id : Set :=
    Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.IdWithTemp.t).
  
  Definition Raw_context : Raw_context.T (t := Raw_context.t) :=
    Storage_functors.Make_subcontext Storage_functors.Registered
      {|
        Raw_context.T.mem := Raw_context.mem;
        Raw_context.T.dir_mem := Raw_context.dir_mem;
        Raw_context.T.get := Raw_context.get;
        Raw_context.T.get_option := Raw_context.get_option;
        Raw_context.T.init_value := Raw_context.init_value;
        Raw_context.T.set := Raw_context.set;
        Raw_context.T.init_set := Raw_context.init_set;
        Raw_context.T.set_option := Raw_context.set_option;
        Raw_context.T.delete := Raw_context.delete;
        Raw_context.T.remove := Raw_context.remove;
        Raw_context.T.remove_rec := Raw_context.remove_rec;
        Raw_context.T.copy := Raw_context.copy;
        Raw_context.T.fold _ := Raw_context.fold;
        Raw_context.T.keys := Raw_context.keys;
        Raw_context.T.fold_keys _ := Raw_context.fold_keys;
        Raw_context.T.project := Raw_context.project;
        Raw_context.T.absolute_key := Raw_context.absolute_key;
        Raw_context.T.consume_gas := Raw_context.consume_gas;
        Raw_context.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context.T.description := Raw_context.description
      |}
      (let name := [ "sapling" ] in
      {|
        Storage_sigs.NAME.name := name
      |}).
  
  Module Next.
    Definition Storage :=
      Storage_functors.Make_single_data_storage Storage_functors.Registered
        Raw_context
        (let name := [ "next" ] in
        {|
          Storage_sigs.NAME.name := name
        |})
        {|
          Storage_sigs.VALUE.encoding :=
            Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.IdWithTemp.encoding)
        |}.
    
    Definition incr (ctxt : Raw_context.t)
      : M=?
        (Raw_context.t *
          Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.IdWithTemp.t)) :=
      let=? i := Storage.(Storage_sigs.Single_data_storage.get) ctxt in
      let=? ctxt :=
        Storage.(Storage_sigs.Single_data_storage.set) ctxt
          (Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.IdWithTemp.next)
            i) in
      return=? (ctxt, i).
    
    Definition init_value (ctxt : Raw_context.t) : M=? Raw_context.t :=
      Storage.(Storage_sigs.Single_data_storage.init_value) ctxt
        Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.IdWithTemp.init_value).
    
    Definition module :=
      {|
        NEXT.init_value := init_value;
        NEXT.incr := incr
      |}.
  End Next.
  Definition Next : NEXT (id := id) := Next.module.
  
  Definition Index := Lazy_storage_kind.Sapling_state.Id.
  
  Definition rpc_arg
    : RPC_arg.arg
      Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.IdWithTemp.t) :=
    Index.(Lazy_storage_kind.IdWithTemp.rpc_arg).
  
  Definition Indexed_context :
    Storage_sigs.Indexed_raw_context (t := Raw_context.t)
      (key :=
        Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.IdWithTemp.t))
      (ipath :=
        fun (a : Set) =>
          a *
            Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.IdWithTemp.t))
    :=
    Storage_functors.Make_indexed_subcontext
      (Storage_functors.Make_subcontext Storage_functors.Registered Raw_context
        (let name := [ "index" ] in
        {|
          Storage_sigs.NAME.name := name
        |}))
      (Make_index
        {|
          Storage_description.INDEX.path_length :=
            Index.(Lazy_storage_kind.IdWithTemp.path_length);
          Storage_description.INDEX.to_path :=
            Index.(Lazy_storage_kind.IdWithTemp.to_path);
          Storage_description.INDEX.of_path :=
            Index.(Lazy_storage_kind.IdWithTemp.of_path);
          Storage_description.INDEX.rpc_arg :=
            Index.(Lazy_storage_kind.IdWithTemp.rpc_arg);
          Storage_description.INDEX.encoding :=
            Index.(Lazy_storage_kind.IdWithTemp.encoding);
          Storage_description.INDEX.compare :=
            Index.(Lazy_storage_kind.IdWithTemp.compare)
        |}).
  
  Definition remove_rec
    (ctxt : Raw_context.t)
    (n : Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.IdWithTemp.t))
    : M= Raw_context.t :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.remove_rec) ctxt n.
  
  Definition copy
    (ctxt : Raw_context.t)
    (from : Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.IdWithTemp.t))
    (to_ : Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.IdWithTemp.t))
    : M=? Raw_context.t :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.copy) ctxt from to_.
  
  Definition Total_bytes :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t) (key := id)
      (value := Z.t) :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "total_bytes" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.Z.
  
  Definition Commitments_size :
    Storage_sigs.Single_data_storage (t := Raw_context.t * id) (value := int64)
    :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
      (let name := [ "commitments_size" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.Int64.
  
  Definition Memo_size :
    Storage_sigs.Single_data_storage (t := Raw_context.t * id) (value := int) :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
      (let name := [ "memo_size" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Sapling_repr.Memo_size.encoding
      |}.
  
  Definition Commitments :
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage
      (t := Raw_context.t * id) (key := int64) (value := Sapling.Hash.t) :=
    Storage_functors.Make_indexed_carbonated_data_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered
        Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
        (let name := [ "commitments" ] in
        {|
          Storage_sigs.NAME.name := name
        |}))
      (Make_index
        (let t : Set := int64 in
        let rpc_arg :=
          let construct := Int64.to_string in
          let destruct (hash_value : string) : Pervasives.result int64 string :=
            match Int64.of_string_opt hash_value with
            | None => Pervasives.Error "Cannot parse node position"
            | Some id => Pervasives.Ok id
            end in
          RPC_arg.make
            (Some "The position of a node in a sapling commitment tree")
            "sapling_node_position" destruct construct tt in
        let encoding :=
          Data_encoding.def "sapling_node_position"
            (Some "Sapling node position")
            (Some "The position of a node in a sapling commitment tree")
            Data_encoding.int64_value in
        let compare := Compare.Int64.(Compare.S.compare) in
        let path_length := 1 in
        let to_path (c : int64) (l_value : list string) : list string :=
          cons (Int64.to_string c) l_value in
        let of_path (function_parameter : list string) : option int64 :=
          match function_parameter with
          | cons c [] => Int64.of_string_opt c
          | _ => None
          end in
        {|
          Storage_description.INDEX.path_length := path_length;
          Storage_description.INDEX.to_path := to_path;
          Storage_description.INDEX.of_path := of_path;
          Storage_description.INDEX.rpc_arg := rpc_arg;
          Storage_description.INDEX.encoding := encoding;
          Storage_description.INDEX.compare := compare
        |}))
      {|
        Storage_sigs.VALUE.encoding := Sapling.Hash.encoding
      |}.
  
  Definition commitments_init
    (ctx : Raw_context.t)
    (id : Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.IdWithTemp.t))
    : M= Raw_context.t :=
    let= '(ctx, _id) :=
      Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context.T.remove_rec)
        (ctx, id) [ "commitments" ] in
    return= ctx.
  
  Definition Ciphertexts :
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage
      (t := Raw_context.t * id) (key := int64) (value := Sapling.Ciphertext.t)
    :=
    Storage_functors.Make_indexed_carbonated_data_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered
        Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
        (let name := [ "ciphertexts" ] in
        {|
          Storage_sigs.NAME.name := name
        |}))
      (Make_index
        (let t : Set := int64 in
        let rpc_arg :=
          let construct := Int64.to_string in
          let destruct (hash_value : string) : Pervasives.result int64 string :=
            match Int64.of_string_opt hash_value with
            | None => Pervasives.Error "Cannot parse ciphertext position"
            | Some id => Pervasives.Ok id
            end in
          RPC_arg.make (Some "The position of a sapling ciphertext")
            "sapling_ciphertext_position" destruct construct tt in
        let encoding :=
          Data_encoding.def "sapling_ciphertext_position"
            (Some "Sapling ciphertext position")
            (Some "The position of a sapling ciphertext")
            Data_encoding.int64_value in
        let compare := Compare.Int64.(Compare.S.compare) in
        let path_length := 1 in
        let to_path (c : int64) (l_value : list string) : list string :=
          cons (Int64.to_string c) l_value in
        let of_path (function_parameter : list string) : option int64 :=
          match function_parameter with
          | cons c [] => Int64.of_string_opt c
          | _ => None
          end in
        {|
          Storage_description.INDEX.path_length := path_length;
          Storage_description.INDEX.to_path := to_path;
          Storage_description.INDEX.of_path := of_path;
          Storage_description.INDEX.rpc_arg := rpc_arg;
          Storage_description.INDEX.encoding := encoding;
          Storage_description.INDEX.compare := compare
        |}))
      {|
        Storage_sigs.VALUE.encoding := Sapling.Ciphertext.encoding
      |}.
  
  Definition ciphertexts_init
    (ctx : Raw_context.t)
    (id : Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.IdWithTemp.t))
    : M= Raw_context.t :=
    let= '(ctx, _id) :=
      Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context.T.remove_rec)
        (ctx, id) [ "commitments" ] in
    return= ctx.
  
  Definition Nullifiers_size :
    Storage_sigs.Single_data_storage (t := Raw_context.t * id) (value := int64)
    :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
      (let name := [ "nullifiers_size" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.Int64.
  
  Module Nullifiers_ordered.
    Definition Storage :
      Storage_sigs.Indexed_data_storage (t := Raw_context.t * id) (key := int64)
        (value := Sapling.Nullifier.t) :=
      Storage_functors.Make_indexed_data_storage
        (Storage_functors.Make_subcontext Storage_functors.Registered
          Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
          (let name := [ "nullifiers_ordered" ] in
          {|
            Storage_sigs.NAME.name := name
          |}))
        (Make_index
          (let t : Set := int64 in
          let rpc_arg :=
            let construct := Int64.to_string in
            let destruct (hash_value : string)
              : Pervasives.result int64 string :=
              match Int64.of_string_opt hash_value with
              | None => Pervasives.Error "Cannot parse nullifier position"
              | Some id => Pervasives.Ok id
              end in
            RPC_arg.make (Some "A sapling nullifier position")
              "sapling_nullifier_position" destruct construct tt in
          let encoding :=
            Data_encoding.def "sapling_nullifier_position"
              (Some "Sapling nullifier position")
              (Some "Sapling nullifier position") Data_encoding.int64_value in
          let compare := Compare.Int64.(Compare.S.compare) in
          let path_length := 1 in
          let to_path (c : int64) (l_value : list string) : list string :=
            cons (Int64.to_string c) l_value in
          let of_path (function_parameter : list string) : option int64 :=
            match function_parameter with
            | cons c [] => Int64.of_string_opt c
            | _ => None
            end in
          {|
            Storage_description.INDEX.path_length := path_length;
            Storage_description.INDEX.to_path := to_path;
            Storage_description.INDEX.of_path := of_path;
            Storage_description.INDEX.rpc_arg := rpc_arg;
            Storage_description.INDEX.encoding := encoding;
            Storage_description.INDEX.compare := compare
          |}))
        {|
          Storage_sigs.VALUE.encoding := Sapling.Nullifier.encoding
        |}.
    
    (** Inclusion of the module [Storage] *)
    Definition context := Storage.(Storage_sigs.Indexed_data_storage.context).
    
    Definition key := Storage.(Storage_sigs.Indexed_data_storage.key).
    
    Definition value := Storage.(Storage_sigs.Indexed_data_storage.value).
    
    Definition mem := Storage.(Storage_sigs.Indexed_data_storage.mem).
    
    Definition get := Storage.(Storage_sigs.Indexed_data_storage.get).
    
    Definition get_option :=
      Storage.(Storage_sigs.Indexed_data_storage.get_option).
    
    Definition set := Storage.(Storage_sigs.Indexed_data_storage.set).
    
    Definition init_value :=
      Storage.(Storage_sigs.Indexed_data_storage.init_value).
    
    Definition init_set := Storage.(Storage_sigs.Indexed_data_storage.init_set).
    
    Definition set_option :=
      Storage.(Storage_sigs.Indexed_data_storage.set_option).
    
    Definition delete := Storage.(Storage_sigs.Indexed_data_storage.delete).
    
    Definition remove := Storage.(Storage_sigs.Indexed_data_storage.remove).
    
    Definition clear := Storage.(Storage_sigs.Indexed_data_storage.clear).
    
    Definition keys := Storage.(Storage_sigs.Indexed_data_storage.keys).
    
    Definition bindings := Storage.(Storage_sigs.Indexed_data_storage.bindings).
    
    Definition fold {a : Set} :=
      Storage.(Storage_sigs.Indexed_data_storage.fold) (a := a).
    
    Definition fold_keys {a : Set} :=
      Storage.(Storage_sigs.Indexed_data_storage.fold_keys) (a := a).
    
    Definition module :=
      {|
        Storage_sigs.Non_iterable_indexed_data_storage.mem := mem;
        Storage_sigs.Non_iterable_indexed_data_storage.get := get;
        Storage_sigs.Non_iterable_indexed_data_storage.get_option := get_option;
        Storage_sigs.Non_iterable_indexed_data_storage.set := set;
        Storage_sigs.Non_iterable_indexed_data_storage.init_value := init_value;
        Storage_sigs.Non_iterable_indexed_data_storage.init_set := init_set;
        Storage_sigs.Non_iterable_indexed_data_storage.set_option := set_option;
        Storage_sigs.Non_iterable_indexed_data_storage.delete := delete;
        Storage_sigs.Non_iterable_indexed_data_storage.remove := remove
      |}.
  End Nullifiers_ordered.
  Definition Nullifiers_ordered
    : Storage_sigs.Non_iterable_indexed_data_storage (t := Raw_context.t * id)
      (key := int64) (value := Sapling.Nullifier.t) :=
    Nullifiers_ordered.module.
  
  Definition Nullifiers_hashed :
    Storage_sigs.Carbonated_data_set_storage (t := Raw_context.t * id)
      (elt := Sapling.Nullifier.t) :=
    Storage_functors.Make_carbonated_data_set_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered
        Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
        (let name := [ "nullifiers_hashed" ] in
        {|
          Storage_sigs.NAME.name := name
        |}))
      (Make_index
        (let t : Set := Sapling.Nullifier.t in
        let encoding := Sapling.Nullifier.encoding in
        let of_string (hexstring : string)
          : Pervasives.result Sapling.Nullifier.t string :=
          let b_value := Hex.to_bytes (Hex.Hex hexstring) in
          match Data_encoding.Binary.of_bytes encoding b_value with
          | None => Pervasives.Error "Cannot parse sapling nullifier"
          | Some nf => Pervasives.Ok nf
          end in
        let to_string (nf : Sapling.Nullifier.t) : string :=
          let b_value := Data_encoding.Binary.to_bytes_exn encoding nf in
          let 'Hex.Hex hexstring := Hex.of_bytes None b_value in
          hexstring in
        let rpc_arg :=
          RPC_arg.make (Some "A sapling nullifier") "sapling_nullifier"
            of_string to_string tt in
        let compare := Sapling.Nullifier.compare in
        let path_length := 1 in
        let to_path (c : Sapling.Nullifier.t) (l_value : list string)
          : list string :=
          cons (to_string c) l_value in
        let of_path (function_parameter : list string)
          : option Sapling.Nullifier.t :=
          match function_parameter with
          | cons c [] =>
            match of_string c with
            | Pervasives.Error _ => None
            | Pervasives.Ok nf => Some nf
            end
          | _ => None
          end in
        {|
          Storage_description.INDEX.path_length := path_length;
          Storage_description.INDEX.to_path := to_path;
          Storage_description.INDEX.of_path := of_path;
          Storage_description.INDEX.rpc_arg := rpc_arg;
          Storage_description.INDEX.encoding := encoding;
          Storage_description.INDEX.compare := compare
        |})).
  
  Definition nullifiers_init (ctx : Raw_context.t) (id : id)
    : M= Raw_context.t :=
    let= ctx :=
      Nullifiers_size.(Storage_sigs.Single_data_storage.init_set) (ctx, id)
        Int64.zero in
    let= '(ctx, id) :=
      Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context.T.remove_rec)
        (ctx, id) [ "nullifiers_ordered" ] in
    let= '(ctx, _id) :=
      Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context.T.remove_rec)
        (ctx, id) [ "nullifiers_hashed" ] in
    return= ctx.
  
  Module Roots.
    Definition Storage :
      Storage_sigs.Indexed_data_storage (t := Raw_context.t * id) (key := int32)
        (value := Sapling.Hash.t) :=
      Storage_functors.Make_indexed_data_storage
        (Storage_functors.Make_subcontext Storage_functors.Registered
          Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
          (let name := [ "roots" ] in
          {|
            Storage_sigs.NAME.name := name
          |}))
        (Make_index
          (let t : Set := int32 in
          let rpc_arg :=
            let construct := Int32.to_string in
            let destruct (hash_value : string)
              : Pervasives.result int32 string :=
              match Int32.of_string_opt hash_value with
              | None => Pervasives.Error "Cannot parse nullifier position"
              | Some id => Pervasives.Ok id
              end in
            RPC_arg.make (Some "A sapling root") "sapling_root" destruct
              construct tt in
          let encoding :=
            Data_encoding.def "sapling_root" (Some "Sapling root")
              (Some "Sapling root") Data_encoding.int32_value in
          let compare := Compare.Int32.(Compare.S.compare) in
          let path_length := 1 in
          let to_path (c : int32) (l_value : list string) : list string :=
            cons (Int32.to_string c) l_value in
          let of_path (function_parameter : list string) : option int32 :=
            match function_parameter with
            | cons c [] => Int32.of_string_opt c
            | _ => None
            end in
          {|
            Storage_description.INDEX.path_length := path_length;
            Storage_description.INDEX.to_path := to_path;
            Storage_description.INDEX.of_path := of_path;
            Storage_description.INDEX.rpc_arg := rpc_arg;
            Storage_description.INDEX.encoding := encoding;
            Storage_description.INDEX.compare := compare
          |}))
        {|
          Storage_sigs.VALUE.encoding := Sapling.Hash.encoding
        |}.
    
    (** Inclusion of the module [Storage] *)
    Definition context := Storage.(Storage_sigs.Indexed_data_storage.context).
    
    Definition key := Storage.(Storage_sigs.Indexed_data_storage.key).
    
    Definition value := Storage.(Storage_sigs.Indexed_data_storage.value).
    
    Definition mem := Storage.(Storage_sigs.Indexed_data_storage.mem).
    
    Definition get := Storage.(Storage_sigs.Indexed_data_storage.get).
    
    Definition get_option :=
      Storage.(Storage_sigs.Indexed_data_storage.get_option).
    
    Definition set := Storage.(Storage_sigs.Indexed_data_storage.set).
    
    Definition init_value :=
      Storage.(Storage_sigs.Indexed_data_storage.init_value).
    
    Definition init_set := Storage.(Storage_sigs.Indexed_data_storage.init_set).
    
    Definition set_option :=
      Storage.(Storage_sigs.Indexed_data_storage.set_option).
    
    Definition delete := Storage.(Storage_sigs.Indexed_data_storage.delete).
    
    Definition remove := Storage.(Storage_sigs.Indexed_data_storage.remove).
    
    Definition clear := Storage.(Storage_sigs.Indexed_data_storage.clear).
    
    Definition keys := Storage.(Storage_sigs.Indexed_data_storage.keys).
    
    Definition bindings := Storage.(Storage_sigs.Indexed_data_storage.bindings).
    
    Definition fold {a : Set} :=
      Storage.(Storage_sigs.Indexed_data_storage.fold) (a := a).
    
    Definition fold_keys {a : Set} :=
      Storage.(Storage_sigs.Indexed_data_storage.fold_keys) (a := a).
    
    Definition module :=
      {|
        Storage_sigs.Non_iterable_indexed_data_storage.mem := mem;
        Storage_sigs.Non_iterable_indexed_data_storage.get := get;
        Storage_sigs.Non_iterable_indexed_data_storage.get_option := get_option;
        Storage_sigs.Non_iterable_indexed_data_storage.set := set;
        Storage_sigs.Non_iterable_indexed_data_storage.init_value := init_value;
        Storage_sigs.Non_iterable_indexed_data_storage.init_set := init_set;
        Storage_sigs.Non_iterable_indexed_data_storage.set_option := set_option;
        Storage_sigs.Non_iterable_indexed_data_storage.delete := delete;
        Storage_sigs.Non_iterable_indexed_data_storage.remove := remove
      |}.
  End Roots.
  Definition Roots
    : Storage_sigs.Non_iterable_indexed_data_storage (t := Raw_context.t * id)
      (key := int32) (value := Sapling.Hash.t) := Roots.module.
  
  Definition Roots_pos :
    Storage_sigs.Single_data_storage (t := Raw_context.t * id) (value := int32)
    :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
      (let name := [ "roots_pos" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.Int32.
  
  Definition Roots_level :
    Storage_sigs.Single_data_storage (t := Raw_context.t * id)
      (value := Raw_level_repr.t) :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
      (let name := [ "roots_level" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Raw_level_repr.encoding
      |}.
End Sapling.

Definition Delegates :
  Storage_sigs.Data_set_storage (t := Raw_context.t)
    (elt := Signature.public_key_hash) :=
  Storage_functors.Make_data_set_storage
    (Storage_functors.Make_subcontext Storage_functors.Registered
      {|
        Raw_context.T.mem := Raw_context.mem;
        Raw_context.T.dir_mem := Raw_context.dir_mem;
        Raw_context.T.get := Raw_context.get;
        Raw_context.T.get_option := Raw_context.get_option;
        Raw_context.T.init_value := Raw_context.init_value;
        Raw_context.T.set := Raw_context.set;
        Raw_context.T.init_set := Raw_context.init_set;
        Raw_context.T.set_option := Raw_context.set_option;
        Raw_context.T.delete := Raw_context.delete;
        Raw_context.T.remove := Raw_context.remove;
        Raw_context.T.remove_rec := Raw_context.remove_rec;
        Raw_context.T.copy := Raw_context.copy;
        Raw_context.T.fold _ := Raw_context.fold;
        Raw_context.T.keys := Raw_context.keys;
        Raw_context.T.fold_keys _ := Raw_context.fold_keys;
        Raw_context.T.project := Raw_context.project;
        Raw_context.T.absolute_key := Raw_context.absolute_key;
        Raw_context.T.consume_gas := Raw_context.consume_gas;
        Raw_context.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context.T.description := Raw_context.description
      |}
      (let name := [ "delegates" ] in
      {|
        Storage_sigs.NAME.name := name
      |}))
    (Make_index
      {|
        Storage_description.INDEX.path_length :=
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.path_length);
        Storage_description.INDEX.to_path :=
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.to_path);
        Storage_description.INDEX.of_path :=
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.of_path);
        Storage_description.INDEX.rpc_arg :=
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.rpc_arg);
        Storage_description.INDEX.encoding :=
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding);
        Storage_description.INDEX.compare :=
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.compare)
      |}).

Definition Active_delegates_with_rolls :
  Storage_sigs.Data_set_storage (t := Raw_context.t)
    (elt := Signature.public_key_hash) :=
  Storage_functors.Make_data_set_storage
    (Storage_functors.Make_subcontext Storage_functors.Registered
      {|
        Raw_context.T.mem := Raw_context.mem;
        Raw_context.T.dir_mem := Raw_context.dir_mem;
        Raw_context.T.get := Raw_context.get;
        Raw_context.T.get_option := Raw_context.get_option;
        Raw_context.T.init_value := Raw_context.init_value;
        Raw_context.T.set := Raw_context.set;
        Raw_context.T.init_set := Raw_context.init_set;
        Raw_context.T.set_option := Raw_context.set_option;
        Raw_context.T.delete := Raw_context.delete;
        Raw_context.T.remove := Raw_context.remove;
        Raw_context.T.remove_rec := Raw_context.remove_rec;
        Raw_context.T.copy := Raw_context.copy;
        Raw_context.T.fold _ := Raw_context.fold;
        Raw_context.T.keys := Raw_context.keys;
        Raw_context.T.fold_keys _ := Raw_context.fold_keys;
        Raw_context.T.project := Raw_context.project;
        Raw_context.T.absolute_key := Raw_context.absolute_key;
        Raw_context.T.consume_gas := Raw_context.consume_gas;
        Raw_context.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context.T.description := Raw_context.description
      |}
      (let name := [ "active_delegates_with_rolls" ] in
      {|
        Storage_sigs.NAME.name := name
      |}))
    (Make_index
      {|
        Storage_description.INDEX.path_length :=
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.path_length);
        Storage_description.INDEX.to_path :=
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.to_path);
        Storage_description.INDEX.of_path :=
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.of_path);
        Storage_description.INDEX.rpc_arg :=
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.rpc_arg);
        Storage_description.INDEX.encoding :=
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding);
        Storage_description.INDEX.compare :=
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.compare)
      |}).

Definition Delegates_with_frozen_balance_index :=
  Storage_functors.Make_indexed_subcontext
    (Storage_functors.Make_subcontext Storage_functors.Registered
      {|
        Raw_context.T.mem := Raw_context.mem;
        Raw_context.T.dir_mem := Raw_context.dir_mem;
        Raw_context.T.get := Raw_context.get;
        Raw_context.T.get_option := Raw_context.get_option;
        Raw_context.T.init_value := Raw_context.init_value;
        Raw_context.T.set := Raw_context.set;
        Raw_context.T.init_set := Raw_context.init_set;
        Raw_context.T.set_option := Raw_context.set_option;
        Raw_context.T.delete := Raw_context.delete;
        Raw_context.T.remove := Raw_context.remove;
        Raw_context.T.remove_rec := Raw_context.remove_rec;
        Raw_context.T.copy := Raw_context.copy;
        Raw_context.T.fold _ := Raw_context.fold;
        Raw_context.T.keys := Raw_context.keys;
        Raw_context.T.fold_keys _ := Raw_context.fold_keys;
        Raw_context.T.project := Raw_context.project;
        Raw_context.T.absolute_key := Raw_context.absolute_key;
        Raw_context.T.consume_gas := Raw_context.consume_gas;
        Raw_context.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context.T.description := Raw_context.description
      |}
      (let name := [ "delegates_with_frozen_balance" ] in
      {|
        Storage_sigs.NAME.name := name
      |})) (Make_index Cycle_repr.Index).

Definition Delegates_with_frozen_balance :
  Storage_sigs.Data_set_storage (t := Raw_context.t * Cycle_repr.t)
    (elt := Signature.public_key_hash) :=
  Storage_functors.Make_data_set_storage
    Delegates_with_frozen_balance_index.(Storage_sigs.Indexed_raw_context.Raw_context)
    (Make_index
      {|
        Storage_description.INDEX.path_length :=
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.path_length);
        Storage_description.INDEX.to_path :=
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.to_path);
        Storage_description.INDEX.of_path :=
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.of_path);
        Storage_description.INDEX.rpc_arg :=
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.rpc_arg);
        Storage_description.INDEX.encoding :=
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding);
        Storage_description.INDEX.compare :=
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.compare)
      |}).

Module Cycle.
  Definition Indexed_context :
    Storage_sigs.Indexed_raw_context (t := Raw_context.t) (key := Cycle_repr.t)
      (ipath := fun (a : Set) => a * Cycle_repr.t) :=
    Storage_functors.Make_indexed_subcontext
      (Storage_functors.Make_subcontext Storage_functors.Registered
        {|
          Raw_context.T.mem := Raw_context.mem;
          Raw_context.T.dir_mem := Raw_context.dir_mem;
          Raw_context.T.get := Raw_context.get;
          Raw_context.T.get_option := Raw_context.get_option;
          Raw_context.T.init_value := Raw_context.init_value;
          Raw_context.T.set := Raw_context.set;
          Raw_context.T.init_set := Raw_context.init_set;
          Raw_context.T.set_option := Raw_context.set_option;
          Raw_context.T.delete := Raw_context.delete;
          Raw_context.T.remove := Raw_context.remove;
          Raw_context.T.remove_rec := Raw_context.remove_rec;
          Raw_context.T.copy := Raw_context.copy;
          Raw_context.T.fold _ := Raw_context.fold;
          Raw_context.T.keys := Raw_context.keys;
          Raw_context.T.fold_keys _ := Raw_context.fold_keys;
          Raw_context.T.project := Raw_context.project;
          Raw_context.T.absolute_key := Raw_context.absolute_key;
          Raw_context.T.consume_gas := Raw_context.consume_gas;
          Raw_context.T.check_enough_gas := Raw_context.check_enough_gas;
          Raw_context.T.description := Raw_context.description
        |}
        (let name := [ "cycle" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) (Make_index Cycle_repr.Index).
  
  Definition Last_roll :=
    Storage_functors.Make_indexed_data_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered
        Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
        (let name := [ "last_roll" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) Int31_index
      {|
        Storage_sigs.VALUE.encoding := Roll_repr.encoding
      |}.
  
  Definition Roll_snapshot :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "roll_snapshot" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.UInt16.
  
  Module unrevealed_nonce.
    Record record : Set := Build {
      nonce_hash : Nonce_hash.t;
      delegate : Signature.public_key_hash;
      rewards : Tez_repr.t;
      fees : Tez_repr.t }.
    Definition with_nonce_hash nonce_hash (r : record) :=
      Build nonce_hash r.(delegate) r.(rewards) r.(fees).
    Definition with_delegate delegate (r : record) :=
      Build r.(nonce_hash) delegate r.(rewards) r.(fees).
    Definition with_rewards rewards (r : record) :=
      Build r.(nonce_hash) r.(delegate) rewards r.(fees).
    Definition with_fees fees (r : record) :=
      Build r.(nonce_hash) r.(delegate) r.(rewards) fees.
  End unrevealed_nonce.
  Definition unrevealed_nonce := unrevealed_nonce.record.
  
  Inductive nonce_status : Set :=
  | Unrevealed : unrevealed_nonce -> nonce_status
  | Revealed : Seed_repr.nonce -> nonce_status.
  
  Definition nonce_status_encoding : Data_encoding.encoding nonce_status :=
    Data_encoding.union None
      [
        Data_encoding.case_value "Unrevealed" None (Data_encoding.Tag 0)
          (Data_encoding.tup4 Nonce_hash.encoding
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)
            Tez_repr.encoding Tez_repr.encoding)
          (fun (function_parameter : nonce_status) =>
            match function_parameter with
            |
              Unrevealed {|
                unrevealed_nonce.nonce_hash := nonce_hash;
                  unrevealed_nonce.delegate := delegate;
                  unrevealed_nonce.rewards := rewards;
                  unrevealed_nonce.fees := fees
                  |} =>
              Some (nonce_hash, delegate, rewards, fees)
            | _ => None
            end)
          (fun (function_parameter :
            Nonce_hash.t * Signature.public_key_hash * Tez_repr.t *
              Tez_repr.t) =>
            let '(nonce_hash, delegate, rewards, fees) :=
              function_parameter in
            Unrevealed
              {| unrevealed_nonce.nonce_hash := nonce_hash;
                unrevealed_nonce.delegate := delegate;
                unrevealed_nonce.rewards := rewards;
                unrevealed_nonce.fees := fees |});
        Data_encoding.case_value "Revealed" None (Data_encoding.Tag 1)
          Seed_repr.nonce_encoding
          (fun (function_parameter : nonce_status) =>
            match function_parameter with
            | Revealed nonce_value => Some nonce_value
            | _ => None
            end)
          (fun (nonce_value : Seed_repr.nonce) => Revealed nonce_value)
      ].
  
  Definition Nonce :=
    Storage_functors.Make_indexed_data_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered
        Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
        (let name := [ "nonces" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) (Make_index Raw_level_repr.Index)
      (let t : Set := nonce_status in
      let encoding := nonce_status_encoding in
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}).
  
  Definition Seed :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t) (key := Cycle_repr.t)
      (value := Seed_repr.seed) :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "random_seed" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      (let t : Set := Seed_repr.seed in
      let encoding := Seed_repr.seed_encoding in
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}).
End Cycle.

Module Roll.
  Definition Raw_context : Raw_context.T (t := Raw_context.t) :=
    Storage_functors.Make_subcontext Storage_functors.Registered
      {|
        Raw_context.T.mem := Raw_context.mem;
        Raw_context.T.dir_mem := Raw_context.dir_mem;
        Raw_context.T.get := Raw_context.get;
        Raw_context.T.get_option := Raw_context.get_option;
        Raw_context.T.init_value := Raw_context.init_value;
        Raw_context.T.set := Raw_context.set;
        Raw_context.T.init_set := Raw_context.init_set;
        Raw_context.T.set_option := Raw_context.set_option;
        Raw_context.T.delete := Raw_context.delete;
        Raw_context.T.remove := Raw_context.remove;
        Raw_context.T.remove_rec := Raw_context.remove_rec;
        Raw_context.T.copy := Raw_context.copy;
        Raw_context.T.fold _ := Raw_context.fold;
        Raw_context.T.keys := Raw_context.keys;
        Raw_context.T.fold_keys _ := Raw_context.fold_keys;
        Raw_context.T.project := Raw_context.project;
        Raw_context.T.absolute_key := Raw_context.absolute_key;
        Raw_context.T.consume_gas := Raw_context.consume_gas;
        Raw_context.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context.T.description := Raw_context.description
      |}
      (let name := [ "rolls" ] in
      {|
        Storage_sigs.NAME.name := name
      |}).
  
  Definition Indexed_context :
    Storage_sigs.Indexed_raw_context (t := Raw_context.t) (key := int32)
      (ipath := fun (a : Set) => a * int32) :=
    Storage_functors.Make_indexed_subcontext
      (Storage_functors.Make_subcontext Storage_functors.Registered Raw_context
        (let name := [ "index" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) (Make_index Roll_repr.Index).
  
  Definition Next :
    Storage_sigs.Single_data_storage (t := Raw_context.t) (value := Roll_repr.t)
    :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Raw_context
      (let name := [ "next" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Roll_repr.encoding
      |}.
  
  Definition Limbo :
    Storage_sigs.Single_data_storage (t := Raw_context.t) (value := Roll_repr.t)
    :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Raw_context
      (let name := [ "limbo" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Roll_repr.encoding
      |}.
  
  Definition Delegate_roll_list :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t)
      (key := Signature.public_key_hash) (value := Roll_repr.t) :=
    Storage_functors.Wrap_indexed_data_storage Contract.Roll_list
      (let t : Set := Signature.public_key_hash in
      let wrap := Contract_repr.implicit_contract in
      let unwrap := Contract_repr.is_implicit in
      {|
        Storage_functors.WRAPPER.wrap := wrap;
        Storage_functors.WRAPPER.unwrap := unwrap
      |}).
  
  Definition Successor :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t) (key := Roll_repr.t)
      (value := Roll_repr.t) :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "successor" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Roll_repr.encoding
      |}.
  
  Definition Delegate_change :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t)
      (key := Signature.public_key_hash) (value := Tez_repr.t) :=
    Storage_functors.Wrap_indexed_data_storage Contract.Change
      (let t : Set := Signature.public_key_hash in
      let wrap := Contract_repr.implicit_contract in
      let unwrap := Contract_repr.is_implicit in
      {|
        Storage_functors.WRAPPER.wrap := wrap;
        Storage_functors.WRAPPER.unwrap := unwrap
      |}).
  
  Module Snapshoted_owner_index.
    Definition t : Set := Cycle_repr.t * int.
    
    Definition path_length : int :=
      Cycle_repr.Index.(Storage_description.INDEX.path_length) +i 1.
    
    Definition to_path (function_parameter : Cycle_repr.t * int)
      : list string -> list string :=
      let '(c, n) := function_parameter in
      fun (s : list string) =>
        Cycle_repr.Index.(Storage_description.INDEX.to_path) c
          (cons (Pervasives.string_of_int n) s).
    
    Definition of_path (l_value : list string) : option (Cycle_repr.t * int) :=
      match
        Misc.take Cycle_repr.Index.(Storage_description.INDEX.path_length)
          l_value with
      | (None | Some (_, ([] | cons _ (cons _ _)))) => None
      | Some (l1, cons l2 []) =>
        match
          ((Cycle_repr.Index.(Storage_description.INDEX.of_path) l1),
            (Pervasives.int_of_string_opt l2)) with
        | ((None, _) | (_, None)) => None
        | (Some c, Some i) => Some (c, i)
        end
      end.
    
    Definition ipath (a : Set) : Set := (a * Cycle_repr.t) * int.
    
    Definition left_args : Storage_description.args :=
      Storage_description.One
        {| Storage_description.args.One.rpc_arg := Cycle_repr.rpc_arg;
          Storage_description.args.One.encoding := Cycle_repr.encoding;
          Storage_description.args.One.compare := Cycle_repr.compare |}.
    
    Definition right_args : Storage_description.args :=
      Storage_description.One
        {| Storage_description.args.One.rpc_arg := RPC_arg.int_value;
          Storage_description.args.One.encoding := Data_encoding.int31;
          Storage_description.args.One.compare :=
            Compare.Int.(Compare.S.compare) |}.
    
    Definition args : Storage_description.args :=
      Storage_description.Pair left_args right_args.
    
    Definition module : Storage_functors.INDEX.signature (ipath := ipath) := {|
        Storage_functors.INDEX.path_length := path_length;
        Storage_functors.INDEX.to_path := to_path;
        Storage_functors.INDEX.of_path := of_path;
        Storage_functors.INDEX.args := args
      |}.
  End Snapshoted_owner_index.
  Definition Snapshoted_owner_index
    : Storage_functors.INDEX (t := Cycle_repr.t * int) (ipath := _) :=
    Snapshoted_owner_index.module.
  
  Definition Owner :
    Storage_sigs.Indexed_data_snapshotable_storage
      (snapshot := Cycle_repr.t * int) (key := Roll_repr.t) (t := Raw_context.t)
      (value := Signature.public_key) :=
    Storage_functors.Make_indexed_data_snapshotable_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered Raw_context
        (let name := [ "owner" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) Snapshoted_owner_index (Make_index Roll_repr.Index)
      {|
        Storage_sigs.VALUE.encoding :=
          Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.encoding)
      |}.
  
  Definition Snapshot_for_cycle :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t) (key := Cycle_repr.t)
      (value := int) := Cycle.Roll_snapshot.
  
  Definition Last_for_snapshot :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t * Cycle_repr.t)
      (key := int) (value := Roll_repr.t) := Cycle.Last_roll.
  
  Definition clear : Raw_context.t -> M= Raw_context.t :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.clear).
End Roll.

Module Vote.
  Definition Raw_context : Raw_context.T (t := Raw_context.t) :=
    Storage_functors.Make_subcontext Storage_functors.Registered
      {|
        Raw_context.T.mem := Raw_context.mem;
        Raw_context.T.dir_mem := Raw_context.dir_mem;
        Raw_context.T.get := Raw_context.get;
        Raw_context.T.get_option := Raw_context.get_option;
        Raw_context.T.init_value := Raw_context.init_value;
        Raw_context.T.set := Raw_context.set;
        Raw_context.T.init_set := Raw_context.init_set;
        Raw_context.T.set_option := Raw_context.set_option;
        Raw_context.T.delete := Raw_context.delete;
        Raw_context.T.remove := Raw_context.remove;
        Raw_context.T.remove_rec := Raw_context.remove_rec;
        Raw_context.T.copy := Raw_context.copy;
        Raw_context.T.fold _ := Raw_context.fold;
        Raw_context.T.keys := Raw_context.keys;
        Raw_context.T.fold_keys _ := Raw_context.fold_keys;
        Raw_context.T.project := Raw_context.project;
        Raw_context.T.absolute_key := Raw_context.absolute_key;
        Raw_context.T.consume_gas := Raw_context.consume_gas;
        Raw_context.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context.T.description := Raw_context.description
      |}
      (let name := [ "votes" ] in
      {|
        Storage_sigs.NAME.name := name
      |}).
  
  Definition Pred_period_kind :
    Storage_sigs.Single_data_storage (t := Raw_context.t)
      (value := Voting_period_repr.kind) :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Raw_context
      (let name := [ "pred_period_kind" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      (let t : Set := Voting_period_repr.kind in
      let encoding := Voting_period_repr.kind_encoding in
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}).
  
  Definition Current_period :
    Storage_sigs.Single_data_storage (t := Raw_context.t)
      (value := Voting_period_repr.t) :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Raw_context
      (let name := [ "current_period" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      (let t : Set := Voting_period_repr.t in
      let encoding := Voting_period_repr.encoding in
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}).
  
  Definition Participation_ema :
    Storage_sigs.Single_data_storage (t := Raw_context.t) (value := int32) :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Raw_context
      (let name := [ "participation_ema" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.Int32.
  
  Definition Current_proposal :
    Storage_sigs.Single_data_storage (t := Raw_context.t)
      (value := Protocol_hash.t) :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Raw_context
      (let name := [ "current_proposal" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Protocol_hash.encoding
      |}.
  
  Definition Listings_size :
    Storage_sigs.Single_data_storage (t := Raw_context.t) (value := int32) :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Raw_context
      (let name := [ "listings_size" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.Int32.
  
  Definition Listings :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t)
      (key := Signature.public_key_hash) (value := int32) :=
    Storage_functors.Make_indexed_data_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered Raw_context
        (let name := [ "listings" ] in
        {|
          Storage_sigs.NAME.name := name
        |}))
      (Make_index
        {|
          Storage_description.INDEX.path_length :=
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.path_length);
          Storage_description.INDEX.to_path :=
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.to_path);
          Storage_description.INDEX.of_path :=
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.of_path);
          Storage_description.INDEX.rpc_arg :=
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.rpc_arg);
          Storage_description.INDEX.encoding :=
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding);
          Storage_description.INDEX.compare :=
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.compare)
        |}) Encoding.Int32.
  
  Definition Proposals :
    Storage_sigs.Data_set_storage (t := Raw_context.t)
      (elt := Protocol_hash.t * Signature.public_key_hash) :=
    Storage_functors.Make_data_set_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered Raw_context
        (let name := [ "proposals" ] in
        {|
          Storage_sigs.NAME.name := name
        |}))
      (Storage_functors.Pair
        (Make_index
          {|
            Storage_description.INDEX.path_length := Protocol_hash.path_length;
            Storage_description.INDEX.to_path := Protocol_hash.to_path;
            Storage_description.INDEX.of_path := Protocol_hash.of_path;
            Storage_description.INDEX.rpc_arg := Protocol_hash.rpc_arg;
            Storage_description.INDEX.encoding := Protocol_hash.encoding;
            Storage_description.INDEX.compare := Protocol_hash.compare
          |})
        (Make_index
          {|
            Storage_description.INDEX.path_length :=
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.path_length);
            Storage_description.INDEX.to_path :=
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.to_path);
            Storage_description.INDEX.of_path :=
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.of_path);
            Storage_description.INDEX.rpc_arg :=
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.rpc_arg);
            Storage_description.INDEX.encoding :=
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding);
            Storage_description.INDEX.compare :=
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.compare)
          |})).
  
  Definition Proposals_count :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t)
      (key := Signature.public_key_hash) (value := int) :=
    Storage_functors.Make_indexed_data_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered Raw_context
        (let name := [ "proposals_count" ] in
        {|
          Storage_sigs.NAME.name := name
        |}))
      (Make_index
        {|
          Storage_description.INDEX.path_length :=
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.path_length);
          Storage_description.INDEX.to_path :=
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.to_path);
          Storage_description.INDEX.of_path :=
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.of_path);
          Storage_description.INDEX.rpc_arg :=
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.rpc_arg);
          Storage_description.INDEX.encoding :=
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding);
          Storage_description.INDEX.compare :=
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.compare)
        |}) Encoding.UInt16.
  
  Definition Ballots :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t)
      (key := Signature.public_key_hash) (value := Vote_repr.ballot) :=
    Storage_functors.Make_indexed_data_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered Raw_context
        (let name := [ "ballots" ] in
        {|
          Storage_sigs.NAME.name := name
        |}))
      (Make_index
        {|
          Storage_description.INDEX.path_length :=
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.path_length);
          Storage_description.INDEX.to_path :=
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.to_path);
          Storage_description.INDEX.of_path :=
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.of_path);
          Storage_description.INDEX.rpc_arg :=
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.rpc_arg);
          Storage_description.INDEX.encoding :=
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding);
          Storage_description.INDEX.compare :=
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.compare)
        |})
      (let t : Set := Vote_repr.ballot in
      let encoding := Vote_repr.ballot_encoding in
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}).
End Vote.

Module FOR_CYCLE.
  Record signature : Set := {
    init_value :
      Raw_context.t -> Cycle_repr.t -> Seed_repr.seed -> M=? Raw_context.t;
    get : Raw_context.t -> Cycle_repr.t -> M=? Seed_repr.seed;
    delete : Raw_context.t -> Cycle_repr.t -> M=? Raw_context.t;
  }.
End FOR_CYCLE.
Definition FOR_CYCLE := FOR_CYCLE.signature.

Module Seed.
  Definition unrevealed_nonce : Set := Cycle.unrevealed_nonce.
  
  Definition nonce_status : Set := Cycle.nonce_status.
  
  Module Nonce.
    Definition context : Set := Raw_context.t.
    
    Definition mem (ctxt : Raw_context.t) (l_value : Level_repr.t) : M= bool :=
      Cycle.Nonce.(Storage_sigs.Indexed_data_storage.mem)
        (ctxt, l_value.(Level_repr.t.cycle)) l_value.(Level_repr.t.level).
    
    Definition get (ctxt : Raw_context.t) (l_value : Level_repr.t)
      : M=? Cycle.nonce_status :=
      Cycle.Nonce.(Storage_sigs.Indexed_data_storage.get)
        (ctxt, l_value.(Level_repr.t.cycle)) l_value.(Level_repr.t.level).
    
    Definition get_option (ctxt : Raw_context.t) (l_value : Level_repr.t)
      : M=? (option Cycle.nonce_status) :=
      Cycle.Nonce.(Storage_sigs.Indexed_data_storage.get_option)
        (ctxt, l_value.(Level_repr.t.cycle)) l_value.(Level_repr.t.level).
    
    Definition set
      (ctxt : Raw_context.t) (l_value : Level_repr.t) (v : Cycle.nonce_status)
      : M=? Raw_context.t :=
      Cycle.Nonce.(Storage_sigs.Indexed_data_storage.set)
        (ctxt, l_value.(Level_repr.t.cycle)) l_value.(Level_repr.t.level) v.
    
    Definition init_value
      (ctxt : Raw_context.t) (l_value : Level_repr.t) (v : Cycle.nonce_status)
      : M=? Raw_context.t :=
      Cycle.Nonce.(Storage_sigs.Indexed_data_storage.init_value)
        (ctxt, l_value.(Level_repr.t.cycle)) l_value.(Level_repr.t.level) v.
    
    Definition init_set
      (ctxt : Raw_context.t) (l_value : Level_repr.t) (v : Cycle.nonce_status)
      : M= Raw_context.t :=
      Cycle.Nonce.(Storage_sigs.Indexed_data_storage.init_set)
        (ctxt, l_value.(Level_repr.t.cycle)) l_value.(Level_repr.t.level) v.
    
    Definition set_option
      (ctxt : Raw_context.t) (l_value : Level_repr.t)
      (v : option Cycle.nonce_status) : M= Raw_context.t :=
      Cycle.Nonce.(Storage_sigs.Indexed_data_storage.set_option)
        (ctxt, l_value.(Level_repr.t.cycle)) l_value.(Level_repr.t.level) v.
    
    Definition delete (ctxt : Raw_context.t) (l_value : Level_repr.t)
      : M=? Raw_context.t :=
      Cycle.Nonce.(Storage_sigs.Indexed_data_storage.delete)
        (ctxt, l_value.(Level_repr.t.cycle)) l_value.(Level_repr.t.level).
    
    Definition remove (ctxt : Raw_context.t) (l_value : Level_repr.t)
      : M= Raw_context.t :=
      Cycle.Nonce.(Storage_sigs.Indexed_data_storage.remove)
        (ctxt, l_value.(Level_repr.t.cycle)) l_value.(Level_repr.t.level).
    
    Definition module :=
      {|
        Storage_sigs.Non_iterable_indexed_data_storage.mem := mem;
        Storage_sigs.Non_iterable_indexed_data_storage.get := get;
        Storage_sigs.Non_iterable_indexed_data_storage.get_option := get_option;
        Storage_sigs.Non_iterable_indexed_data_storage.set := set;
        Storage_sigs.Non_iterable_indexed_data_storage.init_value := init_value;
        Storage_sigs.Non_iterable_indexed_data_storage.init_set := init_set;
        Storage_sigs.Non_iterable_indexed_data_storage.set_option := set_option;
        Storage_sigs.Non_iterable_indexed_data_storage.delete := delete;
        Storage_sigs.Non_iterable_indexed_data_storage.remove := remove
      |}.
  End Nonce.
  Definition Nonce
    : Storage_sigs.Non_iterable_indexed_data_storage (t := Raw_context.t)
      (key := Level_repr.t) (value := nonce_status) := Nonce.module.
  
  Definition For_cycle : FOR_CYCLE :=
    {|
      FOR_CYCLE.init_value :=
        Cycle.Seed.(Storage_sigs.Indexed_data_storage.init_value);
      FOR_CYCLE.get := Cycle.Seed.(Storage_sigs.Indexed_data_storage.get);
      FOR_CYCLE.delete := Cycle.Seed.(Storage_sigs.Indexed_data_storage.delete)
    |}.
End Seed.

Definition Commitments :
  Storage_sigs.Indexed_data_storage (t := Raw_context.t)
    (key := Blinded_public_key_hash.t) (value := Tez_repr.t) :=
  Storage_functors.Make_indexed_data_storage
    (Storage_functors.Make_subcontext Storage_functors.Registered
      {|
        Raw_context.T.mem := Raw_context.mem;
        Raw_context.T.dir_mem := Raw_context.dir_mem;
        Raw_context.T.get := Raw_context.get;
        Raw_context.T.get_option := Raw_context.get_option;
        Raw_context.T.init_value := Raw_context.init_value;
        Raw_context.T.set := Raw_context.set;
        Raw_context.T.init_set := Raw_context.init_set;
        Raw_context.T.set_option := Raw_context.set_option;
        Raw_context.T.delete := Raw_context.delete;
        Raw_context.T.remove := Raw_context.remove;
        Raw_context.T.remove_rec := Raw_context.remove_rec;
        Raw_context.T.copy := Raw_context.copy;
        Raw_context.T.fold _ := Raw_context.fold;
        Raw_context.T.keys := Raw_context.keys;
        Raw_context.T.fold_keys _ := Raw_context.fold_keys;
        Raw_context.T.project := Raw_context.project;
        Raw_context.T.absolute_key := Raw_context.absolute_key;
        Raw_context.T.consume_gas := Raw_context.consume_gas;
        Raw_context.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context.T.description := Raw_context.description
      |}
      (let name := [ "commitments" ] in
      {|
        Storage_sigs.NAME.name := name
      |})) (Make_index Blinded_public_key_hash.Index)
    {|
      Storage_sigs.VALUE.encoding := Tez_repr.encoding
    |}.

Module Ramp_up.
  Definition Rewards :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t) (key := Cycle_repr.t)
      (value := list Tez_repr.t * list Tez_repr.t) :=
    Storage_functors.Make_indexed_data_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered
        {|
          Raw_context.T.mem := Raw_context.mem;
          Raw_context.T.dir_mem := Raw_context.dir_mem;
          Raw_context.T.get := Raw_context.get;
          Raw_context.T.get_option := Raw_context.get_option;
          Raw_context.T.init_value := Raw_context.init_value;
          Raw_context.T.set := Raw_context.set;
          Raw_context.T.init_set := Raw_context.init_set;
          Raw_context.T.set_option := Raw_context.set_option;
          Raw_context.T.delete := Raw_context.delete;
          Raw_context.T.remove := Raw_context.remove;
          Raw_context.T.remove_rec := Raw_context.remove_rec;
          Raw_context.T.copy := Raw_context.copy;
          Raw_context.T.fold _ := Raw_context.fold;
          Raw_context.T.keys := Raw_context.keys;
          Raw_context.T.fold_keys _ := Raw_context.fold_keys;
          Raw_context.T.project := Raw_context.project;
          Raw_context.T.absolute_key := Raw_context.absolute_key;
          Raw_context.T.consume_gas := Raw_context.consume_gas;
          Raw_context.T.check_enough_gas := Raw_context.check_enough_gas;
          Raw_context.T.description := Raw_context.description
        |}
        (let name := [ "ramp_up"; "rewards" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) (Make_index Cycle_repr.Index)
      (let t : Set := list Tez_repr.t * list Tez_repr.t in
      let encoding :=
        Data_encoding.obj2
          (Data_encoding.req None None "baking_reward_per_endorsement"
            (Data_encoding.list_value None Tez_repr.encoding))
          (Data_encoding.req None None "endorsement_reward"
            (Data_encoding.list_value None Tez_repr.encoding)) in
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}).
  
  Definition Security_deposits :
    Storage_sigs.Indexed_data_storage (t := Raw_context.t) (key := Cycle_repr.t)
      (value := Tez_repr.t * Tez_repr.t) :=
    Storage_functors.Make_indexed_data_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered
        {|
          Raw_context.T.mem := Raw_context.mem;
          Raw_context.T.dir_mem := Raw_context.dir_mem;
          Raw_context.T.get := Raw_context.get;
          Raw_context.T.get_option := Raw_context.get_option;
          Raw_context.T.init_value := Raw_context.init_value;
          Raw_context.T.set := Raw_context.set;
          Raw_context.T.init_set := Raw_context.init_set;
          Raw_context.T.set_option := Raw_context.set_option;
          Raw_context.T.delete := Raw_context.delete;
          Raw_context.T.remove := Raw_context.remove;
          Raw_context.T.remove_rec := Raw_context.remove_rec;
          Raw_context.T.copy := Raw_context.copy;
          Raw_context.T.fold _ := Raw_context.fold;
          Raw_context.T.keys := Raw_context.keys;
          Raw_context.T.fold_keys _ := Raw_context.fold_keys;
          Raw_context.T.project := Raw_context.project;
          Raw_context.T.absolute_key := Raw_context.absolute_key;
          Raw_context.T.consume_gas := Raw_context.consume_gas;
          Raw_context.T.check_enough_gas := Raw_context.check_enough_gas;
          Raw_context.T.description := Raw_context.description
        |}
        (let name := [ "ramp_up"; "deposits" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) (Make_index Cycle_repr.Index)
      (let t : Set := Tez_repr.t * Tez_repr.t in
      let encoding := Data_encoding.tup2 Tez_repr.encoding Tez_repr.encoding in
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}).
End Ramp_up.

Definition Pending_migration_balance_updates :=
  Storage_functors.Make_single_data_storage Storage_functors.Registered
    {|
      Raw_context.T.mem := Raw_context.mem;
      Raw_context.T.dir_mem := Raw_context.dir_mem;
      Raw_context.T.get := Raw_context.get;
      Raw_context.T.get_option := Raw_context.get_option;
      Raw_context.T.init_value := Raw_context.init_value;
      Raw_context.T.set := Raw_context.set;
      Raw_context.T.init_set := Raw_context.init_set;
      Raw_context.T.set_option := Raw_context.set_option;
      Raw_context.T.delete := Raw_context.delete;
      Raw_context.T.remove := Raw_context.remove;
      Raw_context.T.remove_rec := Raw_context.remove_rec;
      Raw_context.T.copy := Raw_context.copy;
      Raw_context.T.fold _ := Raw_context.fold;
      Raw_context.T.keys := Raw_context.keys;
      Raw_context.T.fold_keys _ := Raw_context.fold_keys;
      Raw_context.T.project := Raw_context.project;
      Raw_context.T.absolute_key := Raw_context.absolute_key;
      Raw_context.T.consume_gas := Raw_context.consume_gas;
      Raw_context.T.check_enough_gas := Raw_context.check_enough_gas;
      Raw_context.T.description := Raw_context.description
    |}
    (let name := [ "pending_migration_balance_updates" ] in
    {|
      Storage_sigs.NAME.name := name
    |})
    (let t : Set := Receipt_repr.balance_updates in
    let encoding := Receipt_repr.balance_updates_encoding in
    {|
      Storage_sigs.VALUE.encoding := encoding
    |}).
