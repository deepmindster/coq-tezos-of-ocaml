Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Seed_repr.
Require TezosOfOCaml.Proto_2021_01.Storage_description.

(** Inclusion of the module [Compare.Int32] *)
Definition t := Compare.Int32.(Compare.S.t).

Definition op_eq := Compare.Int32.(Compare.S.op_eq).

Definition op_ltgt := Compare.Int32.(Compare.S.op_ltgt).

Definition op_lt := Compare.Int32.(Compare.S.op_lt).

Definition op_lteq := Compare.Int32.(Compare.S.op_lteq).

Definition op_gteq := Compare.Int32.(Compare.S.op_gteq).

Definition op_gt := Compare.Int32.(Compare.S.op_gt).

Definition compare := Compare.Int32.(Compare.S.compare).

Definition equal := Compare.Int32.(Compare.S.equal).

Definition max := Compare.Int32.(Compare.S.max).

Definition min := Compare.Int32.(Compare.S.min).

Definition roll : Set := t.

Definition encoding : Data_encoding.encoding int32 := Data_encoding.int32_value.

Definition first : int32 := 0.

Definition succ (i : int32) : int32 := Int32.succ i.

Definition random (sequence_value : Seed_repr.sequence) (bound : int32)
  : int32 * Seed_repr.sequence := Seed_repr.take_int32 sequence_value bound.

Definition rpc_arg : RPC_arg.arg int32 :=
  RPC_arg.like RPC_arg.int32_value None "roll".

Definition to_int32 {A : Set} (v : A) : A := v.

Module Index.
  Definition t : Set := roll.
  
  Definition path_length : int := 3.
  
  Definition to_path (roll : int32) (l_value : list string) : list string :=
    cons (Int32.to_string (Int32.logand roll (Int32.of_int 255)))
      (cons
        (Int32.to_string
          (Int32.logand (Int32.shift_right_logical roll 8) (Int32.of_int 255)))
        (cons (Int32.to_string roll) l_value)).
  
  Definition of_path (function_parameter : list string) : option int32 :=
    match function_parameter with
    | cons _ (cons _ (cons s _)) => Int32.of_string_opt s
    | _ => None
    end.
  
  Definition rpc_arg : RPC_arg.arg int32 := rpc_arg.
  
  Definition encoding : Data_encoding.encoding int32 := encoding.
  
  Definition compare : t -> t -> int := compare.
  
  Definition module :=
    {|
      Storage_description.INDEX.path_length := path_length;
      Storage_description.INDEX.to_path := to_path;
      Storage_description.INDEX.of_path := of_path;
      Storage_description.INDEX.rpc_arg := rpc_arg;
      Storage_description.INDEX.encoding := encoding;
      Storage_description.INDEX.compare := compare
    |}.
End Index.
Definition Index := Index.module.
