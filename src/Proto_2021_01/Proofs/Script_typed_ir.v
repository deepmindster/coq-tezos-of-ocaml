Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Alpha_context.
Require TezosOfOCaml.Proto_2021_01.Script_typed_ir.
Require TezosOfOCaml.Proto_2021_01.Script_ir_translator.

Require TezosOfOCaml.Proto_2021_01.Proofs.Sapling_repr.

Module Comparable_ty.
  Fixpoint to_Set (comparable_ty : Script_typed_ir.comparable_ty)
    : Set :=
    match comparable_ty with
    | Script_typed_ir.Unit_key => unit
    | Script_typed_ir.Never_key => Empty_set
    | Script_typed_ir.Int_key => int
    | Script_typed_ir.Nat_key => int
    | Script_typed_ir.Signature_key => Alpha_context.signature
    | Script_typed_ir.String_key => string
    | Script_typed_ir.Bytes_key => Bytes.t
    | Script_typed_ir.Mutez_key => Alpha_context.Tez.t
    | Script_typed_ir.Bool_key => bool
    | Script_typed_ir.Key_hash_key => public_key_hash
    | Script_typed_ir.Key_key => public_key
    | Script_typed_ir.Timestamp_key => Alpha_context.Script_timestamp.t
    | Script_typed_ir.Chain_id_key => Chain_id.t
    | Script_typed_ir.Address_key => Script_typed_ir.address
    | Script_typed_ir.Pair_key ty1 ty2 => to_Set ty1 * to_Set ty2
    | Script_typed_ir.Union_key ty1 ty2 =>
      Script_typed_ir.union (to_Set ty1) (to_Set ty2)
    | Script_typed_ir.Option_key ty => option (to_Set ty)
    end.
End Comparable_ty.

Module Ty.
  Fixpoint to_Set (ty : Script_typed_ir.ty) : Set :=
    match ty with
    | Script_typed_ir.Unit_t => unit
    | Script_typed_ir.Int_t => int
    | Script_typed_ir.Nat_t => int
    | Script_typed_ir.Signature_t => Alpha_context.signature
    | Script_typed_ir.String_t => string
    | Script_typed_ir.Bytes_t => Bytes.t
    | Script_typed_ir.Mutez_t => Alpha_context.Tez.t
    | Script_typed_ir.Key_hash_t => public_key_hash
    | Script_typed_ir.Key_t => public_key
    | Script_typed_ir.Timestamp_t => Alpha_context.Script_timestamp.t
    | Script_typed_ir.Address_t => Script_typed_ir.address
    | Script_typed_ir.Bool_t => bool
    | Script_typed_ir.Pair_t ty1 ty2 => to_Set ty1 * to_Set ty2
    | Script_typed_ir.Union_t ty1 ty2 =>
      Script_typed_ir.union (to_Set ty1) (to_Set ty2)
    | Script_typed_ir.Lambda_t _ _ => Script_typed_ir.lambda
    | Script_typed_ir.Option_t ty => option (to_Set ty)
    | Script_typed_ir.List_t ty => list (to_Set ty)
    | Script_typed_ir.Set_t comparable_ty =>
      Script_typed_ir.set (Comparable_ty.to_Set comparable_ty)
    | Script_typed_ir.Map_t comparable_ty ty =>
      Script_typed_ir.map (Comparable_ty.to_Set comparable_ty) (to_Set ty)
    | Script_typed_ir.Big_map_t comparable_ty ty =>
      Script_typed_ir.big_map (Comparable_ty.to_Set comparable_ty) (to_Set ty)
    | Script_typed_ir.Contract_t _ => Script_typed_ir.typed_contract
    | Script_typed_ir.Sapling_transaction_t _ =>
      Alpha_context.Sapling.transaction
    | Script_typed_ir.Sapling_state_t _ => Alpha_context.Sapling.state
    | Script_typed_ir.Operation_t => Script_typed_ir.operation
    | Script_typed_ir.Chain_id_t => Chain_id.t
    | Script_typed_ir.Never_t => Empty_set
    | Script_typed_ir.Bls12_381_g1_t => Bls12_381.G1.(S.CURVE.t)
    | Script_typed_ir.Bls12_381_g2_t => Bls12_381.G2.(S.CURVE.t)
    | Script_typed_ir.Bls12_381_fr_t => Bls12_381.Fr.(S.PRIME_FIELD.t)
    | Script_typed_ir.Ticket_t comparable_ty =>
      Script_typed_ir.ticket (Comparable_ty.to_Set comparable_ty)
    end.

  Fixpoint is_valid 
    (legacy : bool)
    (allow_lazy_storage allow_operation allow_contract allow_ticket : bool)
    (ty : Script_typed_ir.ty)
    : bool :=
    match ty with
    | Script_typed_ir.Option_t ty
    | Script_typed_ir.List_t ty
    | Script_typed_ir.Map_t _ ty =>
      is_valid
        legacy
        allow_lazy_storage allow_operation allow_contract allow_ticket
        ty
    | Script_typed_ir.Pair_t ty1 ty2
    | Script_typed_ir.Union_t ty1 ty2 =>
      is_valid
        legacy
        allow_lazy_storage allow_operation allow_contract allow_ticket
        ty1 &&
      is_valid
        legacy
        allow_lazy_storage allow_operation allow_contract allow_ticket
        ty2
    | Script_typed_ir.Sapling_transaction_t size =>
      Sapling_repr.Memo_size.is_valid size
    | Script_typed_ir.Sapling_state_t size =>
      allow_lazy_storage &&
      Sapling_repr.Memo_size.is_valid size
    | Script_typed_ir.Contract_t ty =>
      allow_contract &&
      is_valid
        legacy
        true false true true
        ty
    | Script_typed_ir.Lambda_t ty1 ty2 =>
      is_valid
        legacy
        true true true true
        ty1 &&
      is_valid
        legacy
        true true true true
        ty2
    | Script_typed_ir.Big_map_t _ ty =>
      allow_lazy_storage &&
      is_valid
        legacy
        false false legacy true
        ty
    | Script_typed_ir.Operation_t => allow_operation
    | Script_typed_ir.Ticket_t _ => allow_ticket
    | _ => true
    end.
End Ty.

Module Comparable_data.
  Fixpoint __forall
    (ty : Script_typed_ir.comparable_ty)
    (x : Comparable_ty.to_Set ty)
    (predicate : forall ty, Comparable_ty.to_Set ty -> bool)
    : bool :=
    predicate _ x &&
    match ty, x with
    | Script_typed_ir.Unit_key, _ => true
    | Script_typed_ir.Never_key, _ => true
    | Script_typed_ir.Int_key, _ => true
    | Script_typed_ir.Nat_key, _ => true
    | Script_typed_ir.Signature_key, _ => true
    | Script_typed_ir.String_key, _ => true
    | Script_typed_ir.Bytes_key, _ => true
    | Script_typed_ir.Mutez_key, _ => true
    | Script_typed_ir.Bool_key, _ => true
    | Script_typed_ir.Key_hash_key, _ => true
    | Script_typed_ir.Key_key, _ => true
    | Script_typed_ir.Timestamp_key, _ => true
    | Script_typed_ir.Chain_id_key, _ => true
    | Script_typed_ir.Address_key, _ => true
    | Script_typed_ir.Pair_key ty1 ty2, (x1, x2) =>
      __forall ty1 x1 predicate && __forall ty2 x2 predicate
    | Script_typed_ir.Union_key ty1 _, Script_typed_ir.L x1 =>
      __forall ty1 x1 predicate
    | Script_typed_ir.Union_key _ ty2, Script_typed_ir.R x2 =>
      __forall ty2 x2 predicate
    | Script_typed_ir.Option_key _, None => true
    | Script_typed_ir.Option_key ty, Some x => __forall ty x predicate
    end.

  Definition is_valid ty (x : Comparable_ty.to_Set ty) : bool :=
    __forall ty x (fun ty x =>
      match ty, x with
      | Script_typed_ir.Nat_key, n => negb (n <Z Z.zero)
      | Script_typed_ir.String_key, s =>
        Script_ir_translator.check_printable_ascii s
      | Script_typed_ir.Mutez_key, mutez => negb (mutez <Z Z.zero)
      | Script_typed_ir.Address_key, (_, entrypoint) =>
        match entrypoint with
        | "" => false
        | _ => true
        end
      | _, _ => true
      end
    ).

  Lemma is_valid_pair {ty1 ty2 x1 x2}
    : is_valid (Script_typed_ir.Pair_key ty1 ty2) (x1, x2) = true ->
    is_valid ty1 x1 = true /\ is_valid ty2 x2 = true.
    unfold is_valid; simpl; intro H.
    now rewrite Bool.andb_true_iff in H.
  Qed.
End Comparable_data.
