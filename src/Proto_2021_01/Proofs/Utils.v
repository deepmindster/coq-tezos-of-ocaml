Require TezosOfOCaml.Proto_2021_01.Environment.
Import Environment.

Inductive terminates {A : Set} : M? A -> Prop :=
| Terminates : forall (v : A), terminates (Pervasives.Ok v).

Ltac impossible_branch :=
  now (intro H; inversion H).

Ltac sub_expression_terminates :=
  match goal with
  | |- Utils.terminates ?e => now destruct e
  end.

Module When_success.
  Inductive t {A : Set} (P : A -> Prop) : M? A -> Prop :=
  | Error : forall v, t P (Pervasives.Error v)
  | Success : forall v, P v -> t P (Pervasives.Ok v).
End When_success.

Definition when_success {A : Set} (e : M? A) (P : A -> Prop) : Prop :=
  match e with
  | Pervasives.Ok v => P v
  | _ => True
  end.

Lemma when_success_bind {A1 A2 : Set} (P1 : A1 -> Prop) (P2 : A2 -> Prop)
  (e1 : M? A1) (e2 : A1 -> M? A2)
  : when_success e1 P1 -> (forall v1, P1 v1 -> when_success (e2 v1) P2) ->
    when_success (let? v1 := e1 in e2 v1) P2.
  intros H1 H2.
  destruct e1; simpl in *; trivial.
  now apply H2.
Qed.

(** Document the proof state by checking that a certain expression is there,
    either in the goal or the hypothesis. *)
Ltac step e :=
  match goal with
  | |- context [e] => idtac
  | _ : context [e] |- _ => idtac
  | _ => fail "expression not found in the current goal"
  end.
