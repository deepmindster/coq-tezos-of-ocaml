Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Script_ir_translator.

Require Import TezosOfOCaml.Proto_2021_01.Proofs.Utils.
Require TezosOfOCaml.Proto_2021_01.Proofs.Contract_repr.
Require TezosOfOCaml.Proto_2021_01.Proofs.Script_typed_ir.

Fixpoint comparable_type_size_eq_type_size ty :
  Script_ir_translator.type_size (Script_ir_translator.ty_of_comparable_ty ty) =
  Script_ir_translator.comparable_type_size ty.
  destruct ty; simpl; try reflexivity;
    now repeat rewrite comparable_type_size_eq_type_size.
Qed.

Lemma kind_equal_is_sounds kind1 kind2 :
  Script_ir_translator.kind_equal kind1 kind2 = true <->
  kind1 = kind2.
  split; intro H.
  - now destruct kind1, kind2.
  - rewrite H.
    now destruct kind2.
Qed.

Fixpoint compare_comparable_refl kind
  (x : Script_typed_ir.Comparable_ty.to_Set kind) {struct kind}
  : Script_ir_translator.compare_comparable kind x x = 0.
  destruct kind; simpl; simpl in x;
    try rewrite cast_eval; unfold Script_ir_translator.wrap_compare.
  - reflexivity.
  - destruct x.
  - unfold Alpha_context.Script_int.compare, Z.compare.
    now rewrite Z.sub_diag.
  - unfold Alpha_context.Script_int.compare, Z.compare.
    now rewrite Z.sub_diag.
  - now rewrite Signature.compare_refl.
  - unfold String.compare; simpl.
    now rewrite String.eqb_refl.
  - unfold String.compare; simpl.
    now rewrite String.eqb_refl.
  - unfold Alpha_context.Tez.compare; simpl.
    now rewrite Z.sub_diag.
  - now rewrite Z.sub_diag.
  - now rewrite Signature.Public_key_hash_compare_refl.
  - now rewrite Signature.Public_key_compare_refl.
  - now rewrite Z.sub_diag.
  - unfold Chain_id.compare.
    now rewrite Chain_id.compare_refl.
  - destruct x as [x ex]; simpl.
    rewrite Contract_repr.compare_refl.
    unfold String.compare; simpl.
    now rewrite String.eqb_refl.
  - destruct x as [x y].
    rewrite_cast_exists_eval_eq [
      Script_typed_ir.Comparable_ty.to_Set kind1,
      Script_typed_ir.Comparable_ty.to_Set kind2
    ].
    now do 2 rewrite compare_comparable_refl.
  - rewrite_cast_exists_eval_eq [
      Script_typed_ir.Comparable_ty.to_Set kind1,
      Script_typed_ir.Comparable_ty.to_Set kind2
    ].
    destruct x as [lx | rx]; apply compare_comparable_refl.
  - rewrite_cast_exists_eval_eq (Script_typed_ir.Comparable_ty.to_Set kind).
    destruct x as [x|].
    + apply compare_comparable_refl.
    + reflexivity.
Qed.
