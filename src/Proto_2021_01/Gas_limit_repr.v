Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Misc.
Require TezosOfOCaml.Proto_2021_01.Saturation_repr.

Definition decimals : int := 3.

Parameter fp_tag : Set.

Parameter integral_tag : Set.

Definition scaling_factor : int := 1000.

Module Arith.
  Definition t : Set := Saturation_repr.t.
  
  Definition fp : Set := t.
  
  Definition integral : Set := t.
  
  Definition scaling_factor : Saturation_repr.t :=
    match
      Option.bind (Saturation_repr.of_int_opt scaling_factor)
        Saturation_repr.mul_safe_value with
    | None =>
      (* ❌ Assert instruction is not handled. *)
      assert Saturation_repr.t false
    | Some x => x
    end.
  
  Definition sub
    : Saturation_repr.t -> Saturation_repr.t -> Saturation_repr.t :=
    Saturation_repr.sub.
  
  Definition add
    : Saturation_repr.t -> Saturation_repr.t -> Saturation_repr.t :=
    Saturation_repr.add.
  
  Definition zero : Saturation_repr.t :=
    Saturation_repr.may_saturate_value Saturation_repr.zero.
  
  Definition min
    : Saturation_repr.t -> Saturation_repr.t -> Saturation_repr.t :=
    Saturation_repr.min.
  
  Definition max
    : Saturation_repr.t -> Saturation_repr.t -> Saturation_repr.t :=
    Saturation_repr.max.
  
  Definition compare : Saturation_repr.t -> Saturation_repr.t -> int :=
    Saturation_repr.compare.
  
  Definition op_lt : Saturation_repr.t -> Saturation_repr.t -> bool :=
    Saturation_repr.op_lt.
  
  Definition op_ltgt : Saturation_repr.t -> Saturation_repr.t -> bool :=
    Saturation_repr.op_ltgt.
  
  Definition op_gt : Saturation_repr.t -> Saturation_repr.t -> bool :=
    Saturation_repr.op_gt.
  
  Definition op_lteq : Saturation_repr.t -> Saturation_repr.t -> bool :=
    Saturation_repr.op_lteq.
  
  Definition op_gteq : Saturation_repr.t -> Saturation_repr.t -> bool :=
    Saturation_repr.op_gteq.
  
  Definition op_eq : Saturation_repr.t -> Saturation_repr.t -> bool :=
    Saturation_repr.op_eq.
  
  Definition equal : Saturation_repr.t -> Saturation_repr.t -> bool :=
    Saturation_repr.equal.
  
  Definition of_int_opt : int -> option Saturation_repr.t :=
    Saturation_repr.of_int_opt.
  
  Definition fatally_saturated_int {A : Set} (i : int) : A :=
    Pervasives.failwith
      (Pervasives.op_caret (Pervasives.string_of_int i)
        " should not be saturated.").
  
  Definition fatally_saturated_z {A : Set} (z : Z.t) : A :=
    Pervasives.failwith
      (Pervasives.op_caret (Z.to_string z) " should not be saturated.").
  
  Definition integral_of_int_exn (i : int) : Saturation_repr.t :=
    match Saturation_repr.of_int_opt i with
    | None => fatally_saturated_int i
    | Some i' =>
      let r_value := Saturation_repr.scale_fast scaling_factor i' in
      if Saturation_repr.op_eq r_value Saturation_repr.saturated then
        fatally_saturated_int i
      else
        r_value
    end.
  
  Definition integral_exn (z : Z.t) : Saturation_repr.t :=
    match
      Misc.result_of_exception
        (fun (function_parameter : unit) =>
          let '_ := function_parameter in
          Z.to_int z) with
    | Pervasives.Ok i => integral_of_int_exn i
    | Pervasives.Error exn_value =>
      match exn_value with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Overflow" then
          fatally_saturated_z z
        else Pervasives.raise exn_value
      end
    end.
  
  Definition integral_to_z (i : integral) : Z.t :=
    Saturation_repr.to_z (Saturation_repr.ediv i scaling_factor).
  
  Definition ceil (x : Saturation_repr.t) : Saturation_repr.t :=
    let r_value := Saturation_repr.erem x scaling_factor in
    if op_eq r_value zero then
      x
    else
      add x (sub scaling_factor r_value).
  
  Definition floor (x : Saturation_repr.t) : Saturation_repr.t :=
    sub x (Saturation_repr.erem x scaling_factor).
  
  Definition fp_value {A : Set} (x : A) : A := x.
  
  Definition pp (fmtr : Format.formatter) (fp_value : Saturation_repr.t)
    : unit :=
    let q :=
      Saturation_repr.to_int (Saturation_repr.ediv fp_value scaling_factor) in
    let r_value :=
      Saturation_repr.to_int (Saturation_repr.erem fp_value scaling_factor) in
    if r_value =i 0 then
      Format.fprintf fmtr
        (CamlinternalFormatBasics.Format
          (CamlinternalFormatBasics.Int CamlinternalFormatBasics.Int_d
            CamlinternalFormatBasics.No_padding
            CamlinternalFormatBasics.No_precision
            CamlinternalFormatBasics.End_of_format) "%d") q
    else
      Format.fprintf fmtr
        (CamlinternalFormatBasics.Format
          (CamlinternalFormatBasics.Int CamlinternalFormatBasics.Int_d
            CamlinternalFormatBasics.No_padding
            CamlinternalFormatBasics.No_precision
            (CamlinternalFormatBasics.Char_literal "." % char
              (CamlinternalFormatBasics.Int CamlinternalFormatBasics.Int_d
                (CamlinternalFormatBasics.Arg_padding
                  CamlinternalFormatBasics.Zeros)
                CamlinternalFormatBasics.No_precision
                CamlinternalFormatBasics.End_of_format))) "%d.%0*d") q decimals
        r_value.
  
  Definition pp_integral : Format.formatter -> Saturation_repr.t -> unit := pp.
  
  Definition n_fp_encoding : Data_encoding.t fp := Saturation_repr.n_encoding.
  
  Definition z_fp_encoding : Data_encoding.t fp := Saturation_repr.z_encoding.
  
  Definition n_integral_encoding : Data_encoding.t integral :=
    Data_encoding.conv integral_to_z integral_exn None Data_encoding.n.
  
  Definition z_integral_encoding : Data_encoding.t integral :=
    Data_encoding.conv integral_to_z integral_exn None Data_encoding.z.
  
  Definition unsafe_fp (x : Z.t) : Saturation_repr.t :=
    match of_int_opt (Z.to_int x) with
    | Some int_value => int_value
    | None => fatally_saturated_z x
    end.
  
  Definition safe_fp (x : Z.t) : Saturation_repr.t :=
    match of_int_opt (Z.to_int x) with
    | Some int_value => int_value
    | None => Saturation_repr.saturated
    end.
  
  Definition sub_opt
    : Saturation_repr.t -> Saturation_repr.t -> option Saturation_repr.t :=
    Saturation_repr.sub_opt.
End Arith.

(** Records for the constructor parameters *)
Module ConstructorRecords_t.
  Module t.
    Module Limited.
      Record record {remaining : Set} : Set := Build {
        remaining : remaining }.
      Arguments record : clear implicits.
      Definition with_remaining {t_remaining} remaining
        (r : record t_remaining) :=
        Build t_remaining remaining.
    End Limited.
    Definition Limited_skeleton := Limited.record.
  End t.
End ConstructorRecords_t.
Import ConstructorRecords_t.

Reserved Notation "'t.Limited".

Inductive t : Set :=
| Unaccounted : t
| Limited : 't.Limited -> t

where "'t.Limited" := (t.Limited_skeleton Arith.fp).

Module t.
  Include ConstructorRecords_t.t.
  Definition Limited := 't.Limited.
End t.

Definition cost : Set := Z.t.

Definition encoding : Data_encoding.encoding t :=
  Data_encoding.union None
    [
      Data_encoding.case_value "Limited" None (Data_encoding.Tag 0)
        Arith.z_fp_encoding
        (fun (function_parameter : t) =>
          match function_parameter with
          | Limited {| t.Limited.remaining := remaining |} => Some remaining
          | _ => None
          end)
        (fun (remaining : Arith.fp) =>
          Limited {| t.Limited.remaining := remaining |});
      Data_encoding.case_value "Unaccounted" None (Data_encoding.Tag 1)
        (Data_encoding.constant "unaccounted")
        (fun (function_parameter : t) =>
          match function_parameter with
          | Unaccounted => Some tt
          | _ => None
          end)
        (fun (function_parameter : unit) =>
          let '_ := function_parameter in
          Unaccounted)
    ].

Definition pp (ppf : Format.formatter) (function_parameter : t) : unit :=
  match function_parameter with
  | Unaccounted =>
    Format.fprintf ppf
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.String_literal "unaccounted"
          CamlinternalFormatBasics.End_of_format) "unaccounted")
  | Limited {| t.Limited.remaining := remaining |} =>
    Format.fprintf ppf
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.Alpha
          (CamlinternalFormatBasics.String_literal " units remaining"
            CamlinternalFormatBasics.End_of_format)) "%a units remaining")
      Arith.pp remaining
  end.

Definition cost_encoding : Data_encoding.encoding Z.t := Data_encoding.z.

Definition pp_cost (fmt : Format.formatter) (z : Z.t) : unit :=
  Z.pp_print fmt z.

Definition allocation_weight : Z.t := Z.of_int (scaling_factor *i 2).

Definition step_weight : Z.t := Z.of_int scaling_factor.

Definition read_base_weight : Z.t := Z.of_int (scaling_factor *i 100).

Definition write_base_weight : Z.t := Z.of_int (scaling_factor *i 160).

Definition byte_read_weight : Z.t := Z.of_int (scaling_factor *i 10).

Definition byte_written_weight : Z.t := Z.of_int (scaling_factor *i 15).

Definition cost_to_milligas (cost : cost) : Arith.fp := Arith.safe_fp cost.

Definition raw_consume (gas_counter : Saturation_repr.t) (cost : cost)
  : option Saturation_repr.t :=
  let gas := cost_to_milligas cost in
  Arith.sub_opt gas_counter gas.

Definition alloc_cost (n : Z.t) : Z.t := allocation_weight *Z (Z.succ n).

Definition alloc_bytes_cost (n : int) : Z.t :=
  alloc_cost (Z.of_int ((n +i 7) /i 8)).

Definition atomic_step_cost {A : Set} (n : A) : A := n.

Definition step_cost (n : Z.t) : Z.t := step_weight *Z n.

Definition free : Z.t := Z.zero.

Definition read_bytes_cost (n : Z.t) : Z.t :=
  read_base_weight +Z (byte_read_weight *Z n).

Definition write_bytes_cost (n : Z.t) : Z.t :=
  write_base_weight +Z (byte_written_weight *Z n).

Definition op_plusat (x : Z.t) (y : Z.t) : Z.t := x +Z y.

Definition op_starat (x : Z.t) (y : Z.t) : Z.t := x *Z y.

Definition alloc_mbytes_cost (n : int) : Z.t :=
  op_plusat (alloc_cost (Z.of_int 12)) (alloc_bytes_cost n).
