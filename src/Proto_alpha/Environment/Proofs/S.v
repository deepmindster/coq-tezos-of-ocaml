Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Compare.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.RPC_arg.

Module MINIMAL_HASH.
  Definition to_Compare_S {t} (H : S.MINIMAL_HASH (t := t))
    : Compare.S (t := t) := {|
      Compare.S.op_eq := H.(S.MINIMAL_HASH.op_eq);
      Compare.S.op_ltgt := H.(S.MINIMAL_HASH.op_ltgt);
      Compare.S.op_lt := H.(S.MINIMAL_HASH.op_lt);
      Compare.S.op_lteq := H.(S.MINIMAL_HASH.op_lteq);
      Compare.S.op_gteq := H.(S.MINIMAL_HASH.op_gteq);
      Compare.S.op_gt := H.(S.MINIMAL_HASH.op_gt);
      Compare.S.compare := H.(S.MINIMAL_HASH.compare);
      Compare.S.equal := H.(S.MINIMAL_HASH.equal);
      Compare.S.max := H.(S.MINIMAL_HASH.max);
      Compare.S.min := H.(S.MINIMAL_HASH.min);
    |}.

    Module Valid.
      Record t {t} {H : S.MINIMAL_HASH (t := t)} : Prop := {
        Compare_S : Compare.S.Valid.t (to_Compare_S H);
      }.
      Arguments t {_}.
    End Valid.
End MINIMAL_HASH.

Module RAW_DATA.
  Module Valid.
    Record t {t} {R : S.RAW_DATA (t := t)} : Prop := {
      of_bytes_opt_to_bytes v :
        R.(S.RAW_DATA.of_bytes_opt) (R.(S.RAW_DATA.to_bytes) v) =
        Some v;
      to_bytes_of_bytes_opt bytes :
        match R.(S.RAW_DATA.of_bytes_opt) bytes with
        | Some v => R.(S.RAW_DATA.to_bytes) v = bytes
        | None => True
        end;
    }.
    Arguments t {_}.
  End Valid.
End RAW_DATA.

Module B58_DATA.
  Module Valid.
    Record t {t} (B : S.B58_DATA (t := t)) : Prop := {
      of_to_b58_check value :
        B.(S.B58_DATA.of_b58check_opt) (B.(S.B58_DATA.to_b58check) value) =
        Some value;
      to_of_b58_check string :
        match B.(S.B58_DATA.of_b58check_opt) string with
        | Some value => B.(S.B58_DATA.to_b58check) value = string
        | None => True
        end;
    }.
    Arguments t {_}.
  End Valid.
End B58_DATA.

Module ENCODER.
  Module Valid.
    Record t {t : Set} {domain : t -> Prop} {E : S.ENCODER (t := t)} : Prop := {
      encoding : Data_encoding.Valid.t (fun _ => True) E.(S.ENCODER.encoding);
      rpc_arg : RPC_arg.Valid.t domain E.(S.ENCODER.rpc_arg);
    }.
    Arguments t {_}.
  End Valid.
End ENCODER.

Module HASH.
  Definition to_MINIMAL_HASH {t Set_t Map_t}
    (H : S.HASH (t := t) (Set_t := Set_t) (Map_t := Map_t))
    : S.MINIMAL_HASH (t := t) := {|
      S.MINIMAL_HASH.name := H.(S.HASH.name);
      S.MINIMAL_HASH.title := H.(S.HASH.title);
      S.MINIMAL_HASH.pp := H.(S.HASH.pp);
      S.MINIMAL_HASH.pp_short := H.(S.HASH.pp_short);
      S.MINIMAL_HASH.op_eq := H.(S.HASH.op_eq);
      S.MINIMAL_HASH.op_ltgt := H.(S.HASH.op_ltgt);
      S.MINIMAL_HASH.op_lt := H.(S.HASH.op_lt);
      S.MINIMAL_HASH.op_lteq := H.(S.HASH.op_lteq);
      S.MINIMAL_HASH.op_gteq := H.(S.HASH.op_gteq);
      S.MINIMAL_HASH.op_gt := H.(S.HASH.op_gt);
      S.MINIMAL_HASH.compare := H.(S.HASH.compare);
      S.MINIMAL_HASH.equal := H.(S.HASH.equal);
      S.MINIMAL_HASH.max := H.(S.HASH.max);
      S.MINIMAL_HASH.min := H.(S.HASH.min);
      S.MINIMAL_HASH.hash_bytes := H.(S.HASH.hash_bytes);
      S.MINIMAL_HASH.hash_string := H.(S.HASH.hash_string);
      S.MINIMAL_HASH.zero := H.(S.HASH.zero);
    |}.

  Definition to_RAW_DATA {t Set_t Map_t}
    (H : S.HASH (t := t) (Set_t := Set_t) (Map_t := Map_t))
    : S.RAW_DATA (t := t) := {|
      S.RAW_DATA.size_value := H.(S.HASH.size_value);
      S.RAW_DATA.to_bytes := H.(S.HASH.to_bytes);
      S.RAW_DATA.of_bytes_opt := H.(S.HASH.of_bytes_opt);
      S.RAW_DATA.of_bytes_exn := H.(S.HASH.of_bytes_exn);
    |}.

  Definition to_B58_DATA {t Set_t Map_t}
    (H : S.HASH (t := t) (Set_t := Set_t) (Map_t := Map_t))
    : S.B58_DATA (t := t) := {|
      S.B58_DATA.to_b58check := H.(HASH.to_b58check);
      S.B58_DATA.to_short_b58check := H.(HASH.to_short_b58check);
      S.B58_DATA.of_b58check_exn := H.(HASH.of_b58check_exn);
      S.B58_DATA.of_b58check_opt := H.(HASH.of_b58check_opt);
      S.B58_DATA.b58check_encoding := H.(HASH.b58check_encoding);
    |}.

  Definition to_ENCODER {t Set_t Map_t}
    (H : S.HASH (t := t) (Set_t := Set_t) (Map_t := Map_t))
    : S.ENCODER (t := t) := {|
      S.ENCODER.encoding := H.(S.HASH.encoding);
      S.ENCODER.rpc_arg := H.(S.HASH.rpc_arg);
    |}.

  Definition to_INDEXES {t Set_t Map_t}
    (H : S.HASH (t := t) (Set_t := Set_t) (Map_t := Map_t))
    : S.INDEXES (t := t) := {|
      S.INDEXES._Set := H.(S.HASH._Set);
      S.INDEXES.Map := H.(S.HASH.Map);
    |}.

  Module Valid.
    Record t {t Set_t Map_t} {domain}
      {H : S.HASH (t := t) (Set_t := Set_t) (Map_t := Map_t)} : Prop := {
      MINIMAL_HASH : MINIMAL_HASH.Valid.t (to_MINIMAL_HASH H);
      RAW_DATA : RAW_DATA.Valid.t (to_RAW_DATA H);
      B58_DATA : B58_DATA.Valid.t (to_B58_DATA H);
      ENCODER : ENCODER.Valid.t domain (to_ENCODER H);
    }.
    Arguments t {_ _ _}.
  End Valid.
End HASH.

Module MERKLE_TREE.
  Definition to_HASH {elt t Set_t Map_t path}
    (M : S.MERKLE_TREE
      (elt := elt) (t := t) (Set_t := Set_t) (Map_t := Map_t) (path := path))
    : S.HASH (t := t) (Set_t := Set_t) (Map_t := Map_t) := {|
      S.HASH.name := M.(S.MERKLE_TREE.name);
      S.HASH.title := M.(S.MERKLE_TREE.title);
      S.HASH.pp := M.(S.MERKLE_TREE.pp);
      S.HASH.pp_short := M.(S.MERKLE_TREE.pp_short);
      S.HASH.op_eq := M.(S.MERKLE_TREE.op_eq);
      S.HASH.op_ltgt := M.(S.MERKLE_TREE.op_ltgt);
      S.HASH.op_lt := M.(S.MERKLE_TREE.op_lt);
      S.HASH.op_lteq := M.(S.MERKLE_TREE.op_lteq);
      S.HASH.op_gteq := M.(S.MERKLE_TREE.op_gteq);
      S.HASH.op_gt := M.(S.MERKLE_TREE.op_gt);
      S.HASH.compare := M.(S.MERKLE_TREE.compare);
      S.HASH.equal := M.(S.MERKLE_TREE.equal);
      S.HASH.max := M.(S.MERKLE_TREE.max);
      S.HASH.min := M.(S.MERKLE_TREE.min);
      S.HASH.hash_bytes := M.(S.MERKLE_TREE.hash_bytes);
      S.HASH.hash_string := M.(S.MERKLE_TREE.hash_string);
      S.HASH.zero := M.(S.MERKLE_TREE.zero);
      S.HASH.size_value := M.(S.MERKLE_TREE.size_value);
      S.HASH.to_bytes := M.(S.MERKLE_TREE.to_bytes);
      S.HASH.of_bytes_opt := M.(S.MERKLE_TREE.of_bytes_opt);
      S.HASH.of_bytes_exn := M.(S.MERKLE_TREE.of_bytes_exn);
      S.HASH.to_b58check := M.(S.MERKLE_TREE.to_b58check);
      S.HASH.to_short_b58check := M.(S.MERKLE_TREE.to_short_b58check);
      S.HASH.of_b58check_exn := M.(S.MERKLE_TREE.of_b58check_exn);
      S.HASH.of_b58check_opt := M.(S.MERKLE_TREE.of_b58check_opt);
      S.HASH.b58check_encoding := M.(S.MERKLE_TREE.b58check_encoding);
      S.HASH.encoding := M.(S.MERKLE_TREE.encoding);
      S.HASH.rpc_arg := M.(S.MERKLE_TREE.rpc_arg);
      S.HASH._Set := M.(S.MERKLE_TREE._Set);
      S.HASH.Map := M.(S.MERKLE_TREE.Map);
    |}.

  Module Valid.
    Record t {elt t Set_t Map_t path} {domain}
      {M : S.MERKLE_TREE
        (elt := elt) (t := t) (Set_t := Set_t) (Map_t := Map_t) (path := path)}
      : Prop := {
      HASH : HASH.Valid.t domain (to_HASH M);
      path_encoding :
        Data_encoding.Valid.t (fun _ => True) M.(S.MERKLE_TREE.path_encoding);
    }.
    Arguments t {_ _ _ _ _}.
  End Valid.
End MERKLE_TREE.
