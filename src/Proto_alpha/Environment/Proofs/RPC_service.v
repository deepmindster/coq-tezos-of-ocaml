Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Module Valid.
  Record t {prefix params query input output : Set}
    {service : RPC_service.t prefix params query input output} : Prop := {
    input_encoding :
      Data_encoding.Valid.t (fun _ => True) service.(RPC_service.t.input_encoding);
    output_encoding :
      Data_encoding.Valid.t (fun _ => True) service.(RPC_service.t.output_encoding);
  }.
  Arguments t {_ _ _ _ _}.
End Valid.

Ltac rpc_auto :=
  match goal with
    [|- RPC_service.Valid.t ?x] => try unfold x
  end;
  constructor; Data_encoding.Valid.data_encoding_auto.
