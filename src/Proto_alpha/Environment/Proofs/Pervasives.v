Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import Lia.
Require Import TezosOfOCaml.Proto_alpha.Environment.

Require Import TezosOfOCaml.Proto_alpha.Proofs.Utils.


(** normalize preserves values between min and max *)
Module Int.
  Module Valid.
    Definition t x := Pervasives.min_int <= x <= Pervasives.max_int.
    #[global] Hint Unfold t : tezos_z.

    Lemma int_plus_0_r_eq : forall {i : int},
        t i -> i +i 0 = i.
      Utils.tezos_z_auto.
    Qed.
  End Valid.
End Int.

Lemma normalize_identity : forall {x : int},
  Int.Valid.t x -> Pervasives.normalize x = x.
  Utils.tezos_z_auto.
Qed.

(** normalize never exceeds its boundaries *)
Lemma normalize_never_exceeds : forall {x : int},
  Int.Valid.t (Pervasives.normalize x).
  Utils.tezos_z_auto.
Qed.
