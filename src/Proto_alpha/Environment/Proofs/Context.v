Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Map.

Module Tree.
  Lemma mem_find (tree : Context.Tree.t) (path : Context.key)
    : Context.Tree.mem tree path =
      let value := Tree.find tree path in
      match value with
      | Some _ => true
      | None => false
      end.
    unfold Tree.mem, Tree.find.
    destruct (Tree.Aux.get _ _) as [tree'|]; simpl; trivial.
    now destruct tree'.
  Qed.
End Tree.

Lemma mem_find (view : Context.View.t) (path : Context.key)
  : Context.mem view path =
    let value := Context.find view path in
    match value with
    | Some _ => true
    | None => false
    end.
  unfold Context.mem, Context.find.
  now rewrite Tree.mem_find.
Qed.

Module Key.
  Parameter compare : key -> key -> int.
End Key.
