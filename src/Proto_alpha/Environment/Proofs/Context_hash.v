Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.S.

Axiom Included_HASH_is_valid
  : S.HASH.Valid.t (fun _ => True) Context_hash.Included_HASH.

Module Version.
  Axiom Included_S_is_valid : Compare.S.Valid.t Context_hash.Version.Included_S.

  Axiom encoding_is_valid : Data_encoding.Valid.t (fun _ => True) Context_hash.Version.encoding.
End Version.
