Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Axiom of_string_opt_to_string : forall i,
  Int32.of_string_opt (Int32.to_string i) = Some i.

Axiom to_string_of_string_opt : forall s,
  match Int32.of_string_opt s with
  | Some i => Int32.to_string i = s
  | None => True
  end.
