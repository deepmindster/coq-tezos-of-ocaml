Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Require Import Lia.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.Pervasives.
Require Import TezosOfOCaml.Proto_alpha.Proofs.Utils.

Lemma length_destruct {a : Set} (x : a) (l : list a) :
  List.length (x :: l) = List.length l +i 1.
  Utils.tezos_z_autounfold.
  induction l; simpl; try Utils.tezos_z_auto.
Qed.

Lemma length_is_valid {a : Set} (l : list a) :
    Pervasives.Int.Valid.t (List.length l).
  Utils.tezos_z_autounfold.
  induction l; simpl; try lia.
  Utils.tezos_z_auto.
Qed.

Fixpoint mem_eq_dec {a : Set} {eq_dec : a -> a -> bool}
  (H_eq_dec : forall x y, eq_dec x y = true -> x = y) (v : a) (l : list a)
  : List.mem eq_dec v l = true ->
    List.In v l.
  destruct l; simpl; [congruence|].
  destruct (eq_dec _ _) eqn:H; simpl; [left | right].
  { symmetry.
    now apply H_eq_dec.
  }
  { now apply (mem_eq_dec _ eq_dec). }
Qed.

Definition in_list_string (s : string) (list : list string) : bool :=
  List.mem String.eqb s list.

Fixpoint all_distinct_strings (l : list string) : bool :=
  match l with
  | [] => true
  | h :: tl =>
    if in_list_string h tl then
      false
    else
      all_distinct_strings tl
  end.

Fixpoint Forall_True {a : Set} {P : a -> Prop} {l : list a}
  : (forall x, P x) -> List.Forall P l.
  intro; destruct l; constructor; trivial; now apply Forall_True.
Qed.

Lemma nil_has_no_last : forall (a : Set), last_opt (nil : List.t a) = None.
  reflexivity.
Qed.

Lemma tz_rev_append_rev {a : Set} : forall (l l' : list a),
  CoqOfOCaml.List.rev_append l l' = (Lists.List.rev l ++ l')%list.
  intro l; induction l; simpl; auto; intros.
  rewrite <- app_assoc; firstorder.
Qed.

Lemma lst_lst : forall (a : Set) (l : list a) (x : a),
  last_opt (l ++ [x])%list = Some x.
  intros.
  induction l; auto.
  simpl; rewrite IHl.
  destruct l; reflexivity.
Qed.

Lemma head_is_last_of_rev : forall (a : Set) (l : list a) (x : a),
  last_opt (rev l) = hd l.
  unfold rev; unfold CoqOfOCaml.List.rev.
  intros.
  induction l; auto.
  simpl; rewrite tz_rev_append_rev; apply lst_lst.
Qed.

Lemma head_of_init : forall {a trace : Set} (f : int -> a) (x : trace),
  init_value x 1 f = Pervasives.Ok [f(0)].
  intros.
  unfold init_value.
  reflexivity.
Qed.
