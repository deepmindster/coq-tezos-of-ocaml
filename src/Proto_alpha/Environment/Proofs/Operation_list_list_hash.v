Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.S.

Axiom Included_MERKLE_TREE_is_valid
  : S.MERKLE_TREE.Valid.t (fun _ => True) Operation_list_list_hash.Included_MERKLE_TREE.
