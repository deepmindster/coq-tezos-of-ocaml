Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Compare.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Axiom Public_key_hash_compare_is_valid
  : Compare.Valid.t (Compare.wrap_compare
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.compare)).

Axiom Public_key_hash_encoding_is_valid
  : Data_encoding.Valid.t (fun  _ => True)
    Public_key_hash.(SIGNATURE_PUBLIC_KEY_HASH.encoding).
#[global] Hint Resolve Public_key_hash_encoding_is_valid : Data_encoding_db.

Axiom Public_key_compare_is_valid
  : Compare.Valid.t (Compare.wrap_compare
    Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.compare)).

Axiom Public_key_encoding_is_valid
  : Data_encoding.Valid.t (fun _ => True)
    Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.encoding).

#[global] Hint Resolve 
  Public_key_hash_encoding_is_valid
  Public_key_encoding_is_valid : Data_encoding_db.

(** Forget about the kind of signature and only keep the binary representation.
    Equivalent to converting a signature back and forth with bytes. *)
Parameter canonize : Signature.t -> Signature.t.

(** The [Signature.compare] function is only a pre-order because we compare the
    binary representation of signatures. This representation does not contain
    the kind of the signature. *)
Axiom compare_is_pre_valid
  : Compare.Pre_valid.t canonize (Compare.wrap_compare Signature.compare).

Axiom of_b58check_opt_to_b58check
  : forall signature,
    Signature.of_b58check_opt (Signature.to_b58check signature) =
    Some signature.

Axiom encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Signature.encoding.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.
