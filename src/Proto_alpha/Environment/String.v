Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Definition length : string -> int := String.length.

Definition get : string -> int -> ascii := String.get.

Definition make : int -> ascii -> string := String.make.

Parameter init_value : int -> (int -> ascii) -> string.

Parameter sub : string -> int -> int -> string.

Parameter blit : string -> int -> bytes -> int -> int -> unit.

Definition concat : string -> list string -> string := String.concat.

Parameter iter : (ascii -> unit) -> string -> unit.

Parameter iteri : (int -> ascii -> unit) -> string -> unit.

Fixpoint map (f : ascii -> ascii) (s : string) : string :=
  match s with
  | EmptyString => s
  | String c s => String (f c) (map f s)
  end.

Parameter mapi : (int -> ascii -> ascii) -> string -> string.

Parameter trim : string -> string.

Parameter escaped : string -> string.

Parameter index_opt : string -> ascii -> option int.

Parameter rindex_opt : string -> ascii -> option int.

Parameter index_from_opt : string -> int -> ascii -> option int.

Parameter rindex_from_opt : string -> int -> ascii -> option int.

Parameter contains : string -> ascii -> bool.

Parameter contains_from : string -> int -> ascii -> bool.

Parameter rcontains_from : string -> int -> ascii -> bool.

Parameter uppercase_ascii : string -> string.

Parameter lowercase_ascii : string -> string.

Parameter capitalize_ascii : string -> string.

Parameter uncapitalize_ascii : string -> string.

Definition t : Set := string.

Definition compare : t -> t -> int :=
  fun s1 s2 =>
  match compare s1 s2 with
  | Lt => -1
  | Eq => 0
  | Gt => 1
  end.

Definition equal : t -> t -> bool := String.eqb.

Parameter split_on_char : ascii -> string -> list string.
