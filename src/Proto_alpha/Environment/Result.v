Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_alpha.Environment.Pervasives.
Require Proto_alpha.Environment.Seq.

Definition t (a e : Set) : Set := Pervasives.result a e.

Definition ok {a e : Set} (v : a) : Pervasives.result a e :=
  Pervasives.Ok v.

Definition bind {a b e : Set}
  (e1 : Pervasives.result a e) (e2 : a -> Pervasives.result b e)
  : Pervasives.result b e :=
  match e1 with
  | Pervasives.Ok v1 => e2 v1
  | Pervasives.Error e1 => Pervasives.Error e1
  end.

Parameter ok_s : forall {a e : Set}, a -> Pervasives.result a e.

Parameter error_value : forall {a e : Set}, e -> Pervasives.result a e.

Parameter error_s : forall {a e : Set}, e -> Pervasives.result a e.

Definition _return {a e : Set} (v : a) : Pervasives.result a e :=
  Pervasives.Ok v.

Definition return_unit {trace : Set} : Pervasives.result unit trace :=
  ok tt.

Definition return_none {a trace : Set} : Pervasives.result (option a) trace :=
  ok None.

Definition return_some {a trace : Set} (x : a) :
  Pervasives.result (option a) trace :=
  ok (Some x).

Definition return_nil {a trace : Set} : Pervasives.result (list a) trace :=
  ok [].

Definition return_true {trace : Set} : Pervasives.result bool trace :=
  ok true.

Definition return_false {trace : Set} : Pervasives.result bool trace :=
  ok false.

Parameter value_value : forall {a e : Set}, Pervasives.result a e -> a -> a.

Parameter value_f : forall {a e : Set},
  Pervasives.result a e -> (unit -> a) -> a.

Parameter bind_s : forall {a b e : Set},
  Pervasives.result a e -> (a -> Pervasives.result b e) ->
  Pervasives.result b e.

Parameter bind_error : forall {a e f : Set},
  Pervasives.result a e -> (e -> Pervasives.result a f) ->
  Pervasives.result a f.

Parameter bind_error_s : forall {a e f : Set},
  Pervasives.result a e -> (e -> Pervasives.result a f) ->
  Pervasives.result a f.

Parameter join : forall {a e : Set},
  Pervasives.result (Pervasives.result a e) e -> Pervasives.result a e.

Parameter map : forall {a b e : Set},
  (a -> b) -> Pervasives.result a e -> Pervasives.result b e.

Parameter map_e : forall {a b e : Set},
  (a -> Pervasives.result b e) -> Pervasives.result a e ->
  Pervasives.result b e.

Parameter map_s : forall {a b e : Set},
  (a -> b) -> Pervasives.result a e -> Pervasives.result b e.

Parameter map_es : forall {a b e : Set},
  (a -> Pervasives.result b e) -> Pervasives.result a e ->
  Pervasives.result b e.

Definition map_error {a e f : Set} (func : e -> f) (res : Pervasives.result a e) : Pervasives.result a f :=
  match res with
    Pervasives.Ok a => Pervasives.Ok a
  | Pervasives.Error e => Pervasives.Error (func e)
  end.

Parameter map_error_e : forall {a e f : Set},
  (e -> Pervasives.result a f) -> Pervasives.result a e ->
  Pervasives.result a f.

Parameter map_error_s : forall {a e f : Set},
  (e -> f) -> Pervasives.result a e -> Pervasives.result a f.

Parameter map_error_es : forall {a e f : Set},
  (e -> Pervasives.result a f) -> Pervasives.result a e ->
  Pervasives.result a f.

Parameter fold : forall {a c e : Set},
  (a -> c) -> (e -> c) -> Pervasives.result a e -> c.

Parameter iter : forall {a e : Set},
  (a -> unit) -> Pervasives.result a e -> unit.

Parameter iter_s : forall {a e : Set},
  (a -> unit) -> Pervasives.result a e -> unit.

Parameter iter_error : forall {a e : Set},
  (e -> unit) -> Pervasives.result a e -> unit.

Parameter iter_error_s : forall {a e : Set},
  (e -> unit) -> Pervasives.result a e -> unit.

Parameter is_ok : forall {a e : Set}, Pervasives.result a e -> bool.

Parameter is_error : forall {a e : Set}, Pervasives.result a e -> bool.

Parameter equal : forall {a e : Set},
  (a -> a -> bool) -> (e -> e -> bool) -> Pervasives.result a e ->
  Pervasives.result a e -> bool.

Parameter compare : forall {a e : Set},
  (a -> a -> int) -> (e -> e -> int) -> Pervasives.result a e ->
  Pervasives.result a e -> int.

Parameter to_option : forall {a e : Set}, Pervasives.result a e -> option a.

Parameter of_option : forall {a e : Set},
  e -> option a -> Pervasives.result a e.

Parameter to_list : forall {a e : Set}, Pervasives.result a e -> list a.

Parameter to_seq : forall {a e : Set},
  Pervasives.result a e -> Seq.t a.

Parameter catch : forall {a : Set},
  option (extensible_type -> bool) -> (unit -> a) ->
  Pervasives.result a extensible_type.

Parameter catch_f : forall {_error a : Set},
  option (extensible_type -> bool) -> (unit -> a) ->
  (extensible_type -> _error) -> Pervasives.result a _error.

Parameter catch_s : forall {a : Set},
  option (extensible_type -> bool) -> (unit -> a) ->
  Pervasives.result a extensible_type.
