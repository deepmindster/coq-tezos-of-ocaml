Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Definition t : Set := Z.

Definition zero : t := 0.

Definition one : t := 1.

Definition minus_one : t := -1.

Definition neg : t -> t := fun z => -z.

Definition add: t -> t -> t := Z.add.

Definition sub: t -> t -> t := Z.sub.

Definition mul: t -> t -> t := Z.mul.

Definition div: t -> t -> t := Z.div.

Definition rem: t -> t -> t := Z.rem.

Definition succ: t -> t := fun z => Z.add z 1.

Definition pred: t -> t := fun z => Z.sub z 1.

Definition abs: t -> t := Z.abs.

Parameter max_int : t.

Parameter min_int : t.

Parameter logand : int32 -> int32 -> int32.

Parameter logor : int32 -> int32 -> int32.

Parameter logxor : int32 -> int32 -> int32.

Parameter lognot : int32 -> int32.

Parameter shift_left : int32 -> int -> int32.

Parameter shift_right : int32 -> int -> int32.

Parameter shift_right_logical : int32 -> int -> int32.

Definition of_int: int -> t := fun z => z.

Definition to_int: t -> int := fun z => z.

Parameter of_string_opt : string -> option t.

Parameter to_string : t -> string.

Definition compare : t -> t -> int := Z.sub.

Definition equal : t -> t -> bool := Z.eqb.

Module Notations.
  Infix "+i32" := add (at level 50, left associativity).
  Infix "-i32" := sub (at level 50, left associativity).
  Infix "*i32" := mul (at level 40, left associativity).
  Infix "/i32" := div (at level 40, left associativity).
End Notations.

Global Hint Unfold add sub mul succ pred compare equal : tezos_z.
