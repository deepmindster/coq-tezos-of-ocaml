Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Script_comparable.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.

Definition key_ty {a b : Set} (Box : Script_typed_ir.map a b)
  : Script_typed_ir.comparable_ty :=
  let 'existS _ _ Box := Box in
  Box.(Script_typed_ir.Boxed_map.key_ty).

Definition empty {a b : Set} (ty : Script_typed_ir.comparable_ty)
  : Script_typed_ir.map a b :=
  let OPS :=
    Map.Make
      (let t : Set := a in
      let compare := Script_comparable.compare_comparable ty in
      {|
        Compare.COMPARABLE.compare := compare
      |}) in
  existS (A := Set) _ _
    (let key : Set := a in
    let value : Set := b in
    let key_ty := ty in
    let OPS :=
      let key : Set := a in
      let value : Set := b in
      let t : Set := OPS.(Map.S.t) value in
      let empty := OPS.(Map.S.empty) in
      let add := OPS.(Map.S.add) in
      let remove := OPS.(Map.S.remove) in
      let find := OPS.(Map.S.find) in
      let fold {a : Set} := OPS.(Map.S.fold) (b := a) in
      {|
        Script_typed_ir.Boxed_map_OPS.empty := empty;
        Script_typed_ir.Boxed_map_OPS.add := add;
        Script_typed_ir.Boxed_map_OPS.remove := remove;
        Script_typed_ir.Boxed_map_OPS.find := find;
        Script_typed_ir.Boxed_map_OPS.fold _ := fold
      |} in
    let boxed := OPS.(Script_typed_ir.Boxed_map_OPS.empty) in
    let size_value := 0 in
    {|
      Script_typed_ir.Boxed_map.key_ty := key_ty;
      Script_typed_ir.Boxed_map.OPS := OPS;
      Script_typed_ir.Boxed_map.boxed := boxed;
      Script_typed_ir.Boxed_map.size_value := size_value
    |}).

Definition get {key value : Set}
  (k_value : key) (Box : Script_typed_ir.map key value) : option value :=
  let 'existS _ _ Box := Box in
  Box.(Script_typed_ir.Boxed_map.OPS).(Script_typed_ir.Boxed_map_OPS.find)
    k_value Box.(Script_typed_ir.Boxed_map.boxed).

Definition update {a b : Set}
  (k_value : a) (v_value : option b) (Box : Script_typed_ir.map a b)
  : Script_typed_ir.map a b :=
  let 'existS _ _ Box := Box in
  let '(boxed, size_value) :=
    let contains :=
      match
        Box.(Script_typed_ir.Boxed_map.OPS).(Script_typed_ir.Boxed_map_OPS.find)
          k_value Box.(Script_typed_ir.Boxed_map.boxed) with
      | None => false
      | _ => true
      end in
    match v_value with
    | Some v_value =>
      ((Box.(Script_typed_ir.Boxed_map.OPS).(Script_typed_ir.Boxed_map_OPS.add)
        k_value v_value Box.(Script_typed_ir.Boxed_map.boxed)),
        (Box.(Script_typed_ir.Boxed_map.size_value) +i
        (if contains then
          0
        else
          1)))
    | None =>
      ((Box.(Script_typed_ir.Boxed_map.OPS).(Script_typed_ir.Boxed_map_OPS.remove)
        k_value Box.(Script_typed_ir.Boxed_map.boxed)),
        (Box.(Script_typed_ir.Boxed_map.size_value) -i
        (if contains then
          1
        else
          0)))
    end in
  existS (A := Set) _ _
    (let key : Set := a in
    let value : Set := b in
    let key_ty := Box.(Script_typed_ir.Boxed_map.key_ty) in
    let OPS := Box.(Script_typed_ir.Boxed_map.OPS) in
    {|
      Script_typed_ir.Boxed_map.key_ty := key_ty;
      Script_typed_ir.Boxed_map.OPS := OPS;
      Script_typed_ir.Boxed_map.boxed := boxed;
      Script_typed_ir.Boxed_map.size_value := size_value
    |}).

Definition mem {key value : Set}
  (k_value : key) (Box : Script_typed_ir.map key value) : bool :=
  let 'existS _ _ Box := Box in
  match
    Box.(Script_typed_ir.Boxed_map.OPS).(Script_typed_ir.Boxed_map_OPS.find)
      k_value Box.(Script_typed_ir.Boxed_map.boxed) with
  | None => false
  | _ => true
  end.

Definition fold {key value acc : Set}
  (f_value : key -> value -> acc -> acc) (Box : Script_typed_ir.map key value)
  : acc -> acc :=
  let 'existS _ _ Box := Box in
  Box.(Script_typed_ir.Boxed_map.OPS).(Script_typed_ir.Boxed_map_OPS.fold)
    f_value Box.(Script_typed_ir.Boxed_map.boxed).

Definition size_value {key value : Set} (Box : Script_typed_ir.map key value)
  : Alpha_context.Script_int.num :=
  let 'existS _ _ Box := Box in
  Alpha_context.Script_int.abs
    (Alpha_context.Script_int.of_int Box.(Script_typed_ir.Boxed_map.size_value)).
