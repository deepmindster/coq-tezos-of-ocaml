Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Script_comparable.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.

Definition empty {a : Set} (ty : Script_typed_ir.comparable_ty)
  : Script_typed_ir.set a :=
  let OPS :=
    let functor_result :=
      _Set.Make
        (let t : Set := a in
        let compare := Script_comparable.compare_comparable ty in
        {|
          Compare.COMPARABLE.compare := compare
        |}) in
    {|
      Script_typed_ir.Boxed_set_OPS.empty := functor_result.(_Set.S.empty);
      Script_typed_ir.Boxed_set_OPS.add := functor_result.(_Set.S.add);
      Script_typed_ir.Boxed_set_OPS.mem := functor_result.(_Set.S.mem);
      Script_typed_ir.Boxed_set_OPS.remove := functor_result.(_Set.S.remove);
      Script_typed_ir.Boxed_set_OPS.fold _ := functor_result.(_Set.S.fold)
    |} in
  existS (A := Set) _ _
    (let elt : Set := a in
    let elt_ty := ty in
    let OPS := OPS in
    let boxed := OPS.(Script_typed_ir.Boxed_set_OPS.empty) in
    let size_value := 0 in
    {|
      Script_typed_ir.Boxed_set.elt_ty := elt_ty;
      Script_typed_ir.Boxed_set.OPS := OPS;
      Script_typed_ir.Boxed_set.boxed := boxed;
      Script_typed_ir.Boxed_set.size_value := size_value
    |}).

Definition update {a : Set}
  (v_value : a) (b_value : bool) (Box : Script_typed_ir.set a)
  : Script_typed_ir.set a :=
  let 'existS _ _ Box := Box in
  existS (A := Set) _ _
    (let elt : Set := a in
    let elt_ty := Box.(Script_typed_ir.Boxed_set.elt_ty) in
    let OPS := Box.(Script_typed_ir.Boxed_set.OPS) in
    let boxed :=
      if b_value then
        Box.(Script_typed_ir.Boxed_set.OPS).(Script_typed_ir.Boxed_set_OPS.add)
          v_value Box.(Script_typed_ir.Boxed_set.boxed)
      else
        Box.(Script_typed_ir.Boxed_set.OPS).(Script_typed_ir.Boxed_set_OPS.remove)
          v_value Box.(Script_typed_ir.Boxed_set.boxed) in
    let size_value :=
      let mem :=
        Box.(Script_typed_ir.Boxed_set.OPS).(Script_typed_ir.Boxed_set_OPS.mem)
          v_value Box.(Script_typed_ir.Boxed_set.boxed) in
      if mem then
        if b_value then
          Box.(Script_typed_ir.Boxed_set.size_value)
        else
          Box.(Script_typed_ir.Boxed_set.size_value) -i 1
      else
        if b_value then
          Box.(Script_typed_ir.Boxed_set.size_value) +i 1
        else
          Box.(Script_typed_ir.Boxed_set.size_value) in
    {|
      Script_typed_ir.Boxed_set.elt_ty := elt_ty;
      Script_typed_ir.Boxed_set.OPS := OPS;
      Script_typed_ir.Boxed_set.boxed := boxed;
      Script_typed_ir.Boxed_set.size_value := size_value
    |}).

Definition mem {elt : Set} (v_value : elt) (Box : Script_typed_ir.set elt)
  : bool :=
  let 'existS _ _ Box := Box in
  Box.(Script_typed_ir.Boxed_set.OPS).(Script_typed_ir.Boxed_set_OPS.mem)
    v_value Box.(Script_typed_ir.Boxed_set.boxed).

Definition fold {elt acc : Set}
  (f_value : elt -> acc -> acc) (Box : Script_typed_ir.set elt) : acc -> acc :=
  let 'existS _ _ Box := Box in
  Box.(Script_typed_ir.Boxed_set.OPS).(Script_typed_ir.Boxed_set_OPS.fold)
    f_value Box.(Script_typed_ir.Boxed_set.boxed).

Definition size_value {elt : Set} (Box : Script_typed_ir.set elt)
  : Alpha_context.Script_int.num :=
  let 'existS _ _ Box := Box in
  Alpha_context.Script_int.abs
    (Alpha_context.Script_int.of_int Box.(Script_typed_ir.Boxed_set.size_value)).
