Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Inductive n : Set :=
| Natural_tag : n.

Inductive z : Set :=
| Integer_tag : z.

Definition num : Set := Z.t.

Definition compare (x_value : Z.t) (y_value : Z.t) : int :=
  Z.compare x_value y_value.

Definition zero : Z.t := Z.zero.

Definition zero_n : Z.t := Z.zero.

Definition one_n : Z.t := Z.one.

Definition to_string (x_value : Z.t) : string := Z.to_string x_value.

Definition of_string (s_value : string) : option Z.t :=
  Option.catch None
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Z.of_string s_value).

Definition of_int32 (n_value : int32) : Z.t :=
  Z.of_int64 (Int64.of_int32 n_value).

Definition to_int64 (x_value : Z.t) : option int64 :=
  Option.catch None
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Z.to_int64 x_value).

Definition of_int64 (n_value : int64) : Z.t := Z.of_int64 n_value.

Definition to_int (x_value : Z.t) : option int :=
  Option.catch None
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Z.to_int x_value).

Definition of_int (n_value : int) : Z.t := Z.of_int n_value.

Definition of_zint {A : Set} (x_value : A) : A := x_value.

Definition to_zint {A : Set} (x_value : A) : A := x_value.

Definition add (x_value : Z.t) (y_value : Z.t) : Z.t := x_value +Z y_value.

Definition sub (x_value : Z.t) (y_value : Z.t) : Z.t := x_value -Z y_value.

Definition mul (x_value : Z.t) (y_value : Z.t) : Z.t := x_value *Z y_value.

Definition ediv (x_value : Z.t) (y_value : Z.t) : option (Z.t * Z.t) :=
  Option.catch None
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Z.ediv_rem x_value y_value).

Definition add_n : Z.t -> Z.t -> Z.t := add.

Definition succ_n : Z.t -> Z.t := Z.succ.

Definition mul_n : Z.t -> Z.t -> Z.t := mul.

Definition ediv_n : Z.t -> Z.t -> option (Z.t * Z.t) := ediv.

Definition abs (x_value : Z.t) : Z.t := Z.abs x_value.

Definition is_nat (x_value : Z.t) : option Z.t :=
  if x_value <Z Z.zero then
    None
  else
    Some x_value.

Definition neg (x_value : Z.t) : Z.t := Z.neg x_value.

Definition int_value {A : Set} (x_value : A) : A := x_value.

Definition shift_left (x_value : Z.t) (y_value : Z.t) : option Z.t :=
  if (Z.compare y_value (Z.of_int 256)) >i 0 then
    None
  else
    let y_value := Z.to_int y_value in
    Some (Z.shift_left x_value y_value).

Definition shift_right (x_value : Z.t) (y_value : Z.t) : option Z.t :=
  if (Z.compare y_value (Z.of_int 256)) >i 0 then
    None
  else
    let y_value := Z.to_int y_value in
    Some (Z.shift_right x_value y_value).

Definition shift_left_n : Z.t -> Z.t -> option Z.t := shift_left.

Definition shift_right_n : Z.t -> Z.t -> option Z.t := shift_right.

Definition logor (x_value : Z.t) (y_value : Z.t) : Z.t :=
  Z.logor x_value y_value.

Definition logxor (x_value : Z.t) (y_value : Z.t) : Z.t :=
  Z.logxor x_value y_value.

Definition logand (x_value : Z.t) (y_value : Z.t) : Z.t :=
  Z.logand x_value y_value.

Definition lognot (x_value : Z.t) : Z.t := Z.lognot x_value.

Definition z_encoding : Data_encoding.encoding num := Data_encoding.z_value.

Definition n_encoding : Data_encoding.encoding num := Data_encoding.n_value.
