Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Period_repr.
Require TezosOfOCaml.Proto_alpha.Round_repr.
Require TezosOfOCaml.Proto_alpha.Tez_repr.

Definition fitness_version_number : string := "\002".

Definition proof_of_work_nonce_size : int := 8.

Definition nonce_length : int := 32.

Definition max_anon_ops_per_block : int := 132.

Definition max_proposals_per_delegate : int := 20.

Definition max_operation_data_length : int := 32 *i 1024.

Definition max_micheline_node_count : int := 50000.

Definition max_micheline_bytes_limit : int := 50000.

Definition max_allowed_global_constant_depth : int := 10000.

Definition stake_distribution_size : int := (500 *i 15) *i 4.

Definition sampler_state_size : int := 80 *i 4.

Definition cache_layout : list int :=
  [ 100000000; 8 *i stake_distribution_size; 8 *i sampler_state_size ].

Definition michelson_maximum_type_size : int := 2001.

Definition fixed : Set := unit.

Module ratio.
  Record record : Set := Build {
    numerator : int;
    denominator : int }.
  Definition with_numerator numerator (r : record) :=
    Build numerator r.(denominator).
  Definition with_denominator denominator (r : record) :=
    Build r.(numerator) denominator.
End ratio.
Definition ratio := ratio.record.

Definition ratio_encoding : Data_encoding.encoding ratio :=
  Data_encoding.conv_with_guard
    (fun (r_value : ratio) =>
      (r_value.(ratio.numerator), r_value.(ratio.denominator)))
    (fun (function_parameter : int * int) =>
      let '(numerator, denominator) := function_parameter in
      if denominator >i 0 then
        return?
          {| ratio.numerator := numerator; ratio.denominator := denominator |}
      else
        Pervasives.Error "The denominator must be greater than 0.") None
    (Data_encoding.obj2
      (Data_encoding.req None None "numerator" Data_encoding.uint16)
      (Data_encoding.req None None "denominator" Data_encoding.uint16)).

Definition pp_ratio (fmt : Format.formatter) (function_parameter : ratio)
  : unit :=
  let '{| ratio.numerator := numerator; ratio.denominator := denominator |} :=
    function_parameter in
  Format.fprintf fmt
    (CamlinternalFormatBasics.Format
      (CamlinternalFormatBasics.Int CamlinternalFormatBasics.Int_d
        CamlinternalFormatBasics.No_padding
        CamlinternalFormatBasics.No_precision
        (CamlinternalFormatBasics.Char_literal "/" % char
          (CamlinternalFormatBasics.Int CamlinternalFormatBasics.Int_d
            CamlinternalFormatBasics.No_padding
            CamlinternalFormatBasics.No_precision
            CamlinternalFormatBasics.End_of_format))) "%d/%d") numerator
    denominator.

Definition fixed_encoding : Data_encoding.encoding unit :=
  let uint62 :=
    let max_int_int64 := Int64.of_int Pervasives.max_int in
    Data_encoding.conv_with_guard
      (fun (int_value : int) => Int64.of_int int_value)
      (fun (int64_value : int64) =>
        if int64_value <i64 0 then
          Pervasives.Error "Negative integer"
        else
          if int64_value >i64 max_int_int64 then
            Pervasives.Error "Integer does not fit in 62 bits"
          else
            return? (Int64.to_int int64_value)) None Data_encoding.int64_value
    in
  Data_encoding.conv
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      (proof_of_work_nonce_size, nonce_length, max_anon_ops_per_block,
        max_operation_data_length, max_proposals_per_delegate,
        max_micheline_node_count, max_micheline_bytes_limit,
        max_allowed_global_constant_depth, cache_layout,
        michelson_maximum_type_size))
    (fun (function_parameter :
      int * int * int * int * int * int * int * int * list int * int) =>
      let
        '(_proof_of_work_nonce_size, _nonce_length, _max_anon_ops_per_block,
          _max_operation_data_length, _max_proposals_per_delegate,
          _max_micheline_node_count, _max_micheline_bytes_limit,
          _max_allowed_global_constant_depth, _cache_layout,
          _michelson_maximum_type_size) := function_parameter in
      tt) None
    (Data_encoding.obj10
      (Data_encoding.req None None "proof_of_work_nonce_size"
        Data_encoding.uint8)
      (Data_encoding.req None None "nonce_length" Data_encoding.uint8)
      (Data_encoding.req None None "max_anon_ops_per_block" Data_encoding.uint8)
      (Data_encoding.req None None "max_operation_data_length"
        Data_encoding.int31)
      (Data_encoding.req None None "max_proposals_per_delegate"
        Data_encoding.uint8)
      (Data_encoding.req None None "max_micheline_node_count"
        Data_encoding.int31)
      (Data_encoding.req None None "max_micheline_bytes_limit"
        Data_encoding.int31)
      (Data_encoding.req None None "max_allowed_global_constants_depth"
        Data_encoding.int31)
      (Data_encoding.req None None "cache_layout"
        (Data_encoding.list_value None uint62))
      (Data_encoding.req None None "michelson_maximum_type_size"
        Data_encoding.uint16)).

Definition fixed_value : unit := tt.

Inductive delegate_selection : Set :=
| Random : delegate_selection
| Round_robin_over : list (list Signature.public_key) -> delegate_selection.

Definition delegate_selection_encoding
  : Data_encoding.encoding delegate_selection :=
  Data_encoding.union None
    [
      Data_encoding.case_value "Random_delegate_selection" None
        (Data_encoding.Tag 0) (Data_encoding.constant "random")
        (fun (function_parameter : delegate_selection) =>
          match function_parameter with
          | Random => Some tt
          | _ => None
          end)
        (fun (function_parameter : unit) =>
          let '_ := function_parameter in
          Random);
      Data_encoding.case_value "Round_robin_over_delegates" None
        (Data_encoding.Tag 1)
        (Data_encoding.list_value None
          (Data_encoding.list_value None
            Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.encoding)))
        (fun (function_parameter : delegate_selection) =>
          match function_parameter with
          | Round_robin_over l_value => Some l_value
          | _ => None
          end)
        (fun (l_value : list (list Signature.public_key)) =>
          Round_robin_over l_value)
    ].

Module parametric.
  Record record : Set := Build {
    preserved_cycles : int;
    blocks_per_cycle : int32;
    blocks_per_commitment : int32;
    blocks_per_stake_snapshot : int32;
    blocks_per_voting_period : int32;
    hard_gas_limit_per_operation : Gas_limit_repr.Arith.integral;
    hard_gas_limit_per_block : Gas_limit_repr.Arith.integral;
    proof_of_work_threshold : int64;
    tokens_per_roll : Tez_repr.t;
    seed_nonce_revelation_tip : Tez_repr.t;
    origination_size : int;
    baking_reward_fixed_portion : Tez_repr.t;
    baking_reward_bonus_per_slot : Tez_repr.t;
    endorsing_reward_per_slot : Tez_repr.t;
    cost_per_byte : Tez_repr.t;
    hard_storage_limit_per_operation : Z.t;
    quorum_min : int32;
    quorum_max : int32;
    min_proposal_quorum : int32;
    liquidity_baking_subsidy : Tez_repr.t;
    liquidity_baking_sunset_level : int32;
    liquidity_baking_escape_ema_threshold : int32;
    max_operations_time_to_live : int;
    round_durations : Round_repr.Durations.t;
    minimal_participation_ratio : ratio;
    consensus_committee_size : int;
    consensus_threshold : int;
    max_slashing_period : int;
    frozen_deposits_percentage : int;
    double_baking_punishment : Tez_repr.t;
    ratio_of_frozen_deposits_slashed_per_double_endorsement : ratio;
    delegate_selection : delegate_selection }.
  Definition with_preserved_cycles preserved_cycles (r : record) :=
    Build preserved_cycles r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_blocks_per_cycle blocks_per_cycle (r : record) :=
    Build r.(preserved_cycles) blocks_per_cycle r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_blocks_per_commitment blocks_per_commitment (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) blocks_per_commitment
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_blocks_per_stake_snapshot blocks_per_stake_snapshot
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      blocks_per_stake_snapshot r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_blocks_per_voting_period blocks_per_voting_period
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) blocks_per_voting_period
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_hard_gas_limit_per_operation hard_gas_limit_per_operation
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      hard_gas_limit_per_operation r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_hard_gas_limit_per_block hard_gas_limit_per_block
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) hard_gas_limit_per_block
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_proof_of_work_threshold proof_of_work_threshold
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      proof_of_work_threshold r.(tokens_per_roll) r.(seed_nonce_revelation_tip)
      r.(origination_size) r.(baking_reward_fixed_portion)
      r.(baking_reward_bonus_per_slot) r.(endorsing_reward_per_slot)
      r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
      r.(quorum_max) r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_tokens_per_roll tokens_per_roll (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) tokens_per_roll r.(seed_nonce_revelation_tip)
      r.(origination_size) r.(baking_reward_fixed_portion)
      r.(baking_reward_bonus_per_slot) r.(endorsing_reward_per_slot)
      r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
      r.(quorum_max) r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_seed_nonce_revelation_tip seed_nonce_revelation_tip
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll) seed_nonce_revelation_tip
      r.(origination_size) r.(baking_reward_fixed_portion)
      r.(baking_reward_bonus_per_slot) r.(endorsing_reward_per_slot)
      r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
      r.(quorum_max) r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_origination_size origination_size (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) origination_size
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_baking_reward_fixed_portion baking_reward_fixed_portion
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      baking_reward_fixed_portion r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_baking_reward_bonus_per_slot baking_reward_bonus_per_slot
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) baking_reward_bonus_per_slot
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_endorsing_reward_per_slot endorsing_reward_per_slot
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      endorsing_reward_per_slot r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_cost_per_byte cost_per_byte (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) cost_per_byte
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_hard_storage_limit_per_operation
    hard_storage_limit_per_operation (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      hard_storage_limit_per_operation r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_quorum_min quorum_min (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) quorum_min r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_quorum_max quorum_max (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) quorum_max
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_min_proposal_quorum min_proposal_quorum (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      min_proposal_quorum r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_liquidity_baking_subsidy liquidity_baking_subsidy
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) liquidity_baking_subsidy
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_liquidity_baking_sunset_level liquidity_baking_sunset_level
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      liquidity_baking_sunset_level r.(liquidity_baking_escape_ema_threshold)
      r.(max_operations_time_to_live) r.(round_durations)
      r.(minimal_participation_ratio) r.(consensus_committee_size)
      r.(consensus_threshold) r.(max_slashing_period)
      r.(frozen_deposits_percentage) r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_liquidity_baking_escape_ema_threshold
    liquidity_baking_escape_ema_threshold (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level) liquidity_baking_escape_ema_threshold
      r.(max_operations_time_to_live) r.(round_durations)
      r.(minimal_participation_ratio) r.(consensus_committee_size)
      r.(consensus_threshold) r.(max_slashing_period)
      r.(frozen_deposits_percentage) r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_max_operations_time_to_live max_operations_time_to_live
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) max_operations_time_to_live
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_round_durations round_durations (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      round_durations r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_minimal_participation_ratio minimal_participation_ratio
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) minimal_participation_ratio
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_consensus_committee_size consensus_committee_size
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      consensus_committee_size r.(consensus_threshold) r.(max_slashing_period)
      r.(frozen_deposits_percentage) r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_consensus_threshold consensus_threshold (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) consensus_threshold r.(max_slashing_period)
      r.(frozen_deposits_percentage) r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_max_slashing_period max_slashing_period (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold) max_slashing_period
      r.(frozen_deposits_percentage) r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_frozen_deposits_percentage frozen_deposits_percentage
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) frozen_deposits_percentage
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_double_baking_punishment double_baking_punishment
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      double_baking_punishment
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      r.(delegate_selection).
  Definition with_ratio_of_frozen_deposits_slashed_per_double_endorsement
    ratio_of_frozen_deposits_slashed_per_double_endorsement (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      ratio_of_frozen_deposits_slashed_per_double_endorsement
      r.(delegate_selection).
  Definition with_delegate_selection delegate_selection (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_stake_snapshot) r.(blocks_per_voting_period)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(baking_reward_fixed_portion) r.(baking_reward_bonus_per_slot)
      r.(endorsing_reward_per_slot) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold) r.(max_operations_time_to_live)
      r.(round_durations) r.(minimal_participation_ratio)
      r.(consensus_committee_size) r.(consensus_threshold)
      r.(max_slashing_period) r.(frozen_deposits_percentage)
      r.(double_baking_punishment)
      r.(ratio_of_frozen_deposits_slashed_per_double_endorsement)
      delegate_selection.
End parametric.
Definition parametric := parametric.record.

Definition parametric_encoding : Data_encoding.encoding parametric :=
  Data_encoding.conv
    (fun (c_value : parametric) =>
      ((c_value.(parametric.preserved_cycles),
        c_value.(parametric.blocks_per_cycle),
        c_value.(parametric.blocks_per_commitment),
        c_value.(parametric.blocks_per_stake_snapshot),
        c_value.(parametric.blocks_per_voting_period),
        c_value.(parametric.hard_gas_limit_per_operation),
        c_value.(parametric.hard_gas_limit_per_block),
        c_value.(parametric.proof_of_work_threshold),
        c_value.(parametric.tokens_per_roll)),
        ((c_value.(parametric.seed_nonce_revelation_tip),
          c_value.(parametric.origination_size),
          c_value.(parametric.baking_reward_fixed_portion),
          c_value.(parametric.baking_reward_bonus_per_slot),
          c_value.(parametric.endorsing_reward_per_slot),
          c_value.(parametric.cost_per_byte),
          c_value.(parametric.hard_storage_limit_per_operation),
          c_value.(parametric.quorum_min)),
          ((c_value.(parametric.quorum_max),
            c_value.(parametric.min_proposal_quorum),
            c_value.(parametric.liquidity_baking_subsidy),
            c_value.(parametric.liquidity_baking_sunset_level),
            c_value.(parametric.liquidity_baking_escape_ema_threshold),
            c_value.(parametric.max_operations_time_to_live),
            c_value.(parametric.round_durations),
            c_value.(parametric.consensus_committee_size),
            c_value.(parametric.consensus_threshold)),
            (c_value.(parametric.minimal_participation_ratio),
              c_value.(parametric.max_slashing_period),
              c_value.(parametric.frozen_deposits_percentage),
              c_value.(parametric.double_baking_punishment),
              c_value.(parametric.ratio_of_frozen_deposits_slashed_per_double_endorsement),
              c_value.(parametric.delegate_selection))))))
    (fun (function_parameter :
      (int * int32 * int32 * int32 * int32 * Gas_limit_repr.Arith.integral *
        Gas_limit_repr.Arith.integral * int64 * Tez_repr.t) *
        ((Tez_repr.t * int * Tez_repr.t * Tez_repr.t * Tez_repr.t * Tez_repr.t *
          Z.t * int32) *
          ((int32 * int32 * Tez_repr.t * int32 * int32 * int *
            Round_repr.Durations.t * int * int) *
            (ratio * int * int * Tez_repr.t * ratio * delegate_selection)))) =>
      let
        '((preserved_cycles, blocks_per_cycle, blocks_per_commitment,
          blocks_per_stake_snapshot, blocks_per_voting_period,
          hard_gas_limit_per_operation, hard_gas_limit_per_block,
          proof_of_work_threshold, tokens_per_roll),
          ((seed_nonce_revelation_tip, origination_size,
            baking_reward_fixed_portion, baking_reward_bonus_per_slot,
            endorsing_reward_per_slot, cost_per_byte,
            hard_storage_limit_per_operation, quorum_min),
            ((quorum_max, min_proposal_quorum, liquidity_baking_subsidy,
              liquidity_baking_sunset_level,
              liquidity_baking_escape_ema_threshold,
              max_operations_time_to_live, round_durations,
              consensus_committee_size, consensus_threshold),
              (minimal_participation_ratio, max_slashing_period,
                frozen_deposits_percentage, double_baking_punishment,
                ratio_of_frozen_deposits_slashed_per_double_endorsement,
                delegate_selection)))) := function_parameter in
      {| parametric.preserved_cycles := preserved_cycles;
        parametric.blocks_per_cycle := blocks_per_cycle;
        parametric.blocks_per_commitment := blocks_per_commitment;
        parametric.blocks_per_stake_snapshot := blocks_per_stake_snapshot;
        parametric.blocks_per_voting_period := blocks_per_voting_period;
        parametric.hard_gas_limit_per_operation := hard_gas_limit_per_operation;
        parametric.hard_gas_limit_per_block := hard_gas_limit_per_block;
        parametric.proof_of_work_threshold := proof_of_work_threshold;
        parametric.tokens_per_roll := tokens_per_roll;
        parametric.seed_nonce_revelation_tip := seed_nonce_revelation_tip;
        parametric.origination_size := origination_size;
        parametric.baking_reward_fixed_portion := baking_reward_fixed_portion;
        parametric.baking_reward_bonus_per_slot := baking_reward_bonus_per_slot;
        parametric.endorsing_reward_per_slot := endorsing_reward_per_slot;
        parametric.cost_per_byte := cost_per_byte;
        parametric.hard_storage_limit_per_operation :=
          hard_storage_limit_per_operation; parametric.quorum_min := quorum_min;
        parametric.quorum_max := quorum_max;
        parametric.min_proposal_quorum := min_proposal_quorum;
        parametric.liquidity_baking_subsidy := liquidity_baking_subsidy;
        parametric.liquidity_baking_sunset_level :=
          liquidity_baking_sunset_level;
        parametric.liquidity_baking_escape_ema_threshold :=
          liquidity_baking_escape_ema_threshold;
        parametric.max_operations_time_to_live := max_operations_time_to_live;
        parametric.round_durations := round_durations;
        parametric.minimal_participation_ratio := minimal_participation_ratio;
        parametric.consensus_committee_size := consensus_committee_size;
        parametric.consensus_threshold := consensus_threshold;
        parametric.max_slashing_period := max_slashing_period;
        parametric.frozen_deposits_percentage := frozen_deposits_percentage;
        parametric.double_baking_punishment := double_baking_punishment;
        parametric.ratio_of_frozen_deposits_slashed_per_double_endorsement :=
          ratio_of_frozen_deposits_slashed_per_double_endorsement;
        parametric.delegate_selection := delegate_selection |}) None
    (Data_encoding.merge_objs
      (Data_encoding.obj9
        (Data_encoding.req None None "preserved_cycles" Data_encoding.uint8)
        (Data_encoding.req None None "blocks_per_cycle"
          Data_encoding.int32_value)
        (Data_encoding.req None None "blocks_per_commitment"
          Data_encoding.int32_value)
        (Data_encoding.req None None "blocks_per_stake_snapshot"
          Data_encoding.int32_value)
        (Data_encoding.req None None "blocks_per_voting_period"
          Data_encoding.int32_value)
        (Data_encoding.req None None "hard_gas_limit_per_operation"
          Gas_limit_repr.Arith.z_integral_encoding)
        (Data_encoding.req None None "hard_gas_limit_per_block"
          Gas_limit_repr.Arith.z_integral_encoding)
        (Data_encoding.req None None "proof_of_work_threshold"
          Data_encoding.int64_value)
        (Data_encoding.req None None "tokens_per_roll" Tez_repr.encoding))
      (Data_encoding.merge_objs
        (Data_encoding.obj8
          (Data_encoding.req None None "seed_nonce_revelation_tip"
            Tez_repr.encoding)
          (Data_encoding.req None None "origination_size" Data_encoding.int31)
          (Data_encoding.req None None "baking_reward_fixed_portion"
            Tez_repr.encoding)
          (Data_encoding.req None None "baking_reward_bonus_per_slot"
            Tez_repr.encoding)
          (Data_encoding.req None None "endorsing_reward_per_slot"
            Tez_repr.encoding)
          (Data_encoding.req None None "cost_per_byte" Tez_repr.encoding)
          (Data_encoding.req None None "hard_storage_limit_per_operation"
            Data_encoding.z_value)
          (Data_encoding.req None None "quorum_min" Data_encoding.int32_value))
        (Data_encoding.merge_objs
          (Data_encoding.obj9
            (Data_encoding.req None None "quorum_max" Data_encoding.int32_value)
            (Data_encoding.req None None "min_proposal_quorum"
              Data_encoding.int32_value)
            (Data_encoding.req None None "liquidity_baking_subsidy"
              Tez_repr.encoding)
            (Data_encoding.req None None "liquidity_baking_sunset_level"
              Data_encoding.int32_value)
            (Data_encoding.req None None "liquidity_baking_escape_ema_threshold"
              Data_encoding.int32_value)
            (Data_encoding.req None None "max_operations_time_to_live"
              Data_encoding.int16)
            (Data_encoding.req None None "round_durations"
              Round_repr.Durations.encoding)
            (Data_encoding.req None None "consensus_committee_size"
              Data_encoding.int31)
            (Data_encoding.req None None "consensus_threshold"
              Data_encoding.int31))
          (Data_encoding.obj6
            (Data_encoding.req None None "minimal_participation_ratio"
              ratio_encoding)
            (Data_encoding.req None None "max_slashing_period"
              Data_encoding.int31)
            (Data_encoding.req None None "frozen_deposits_percentage"
              Data_encoding.int31)
            (Data_encoding.req None None "double_baking_punishment"
              Tez_repr.encoding)
            (Data_encoding.req None None
              "ratio_of_frozen_deposits_slashed_per_double_endorsement"
              ratio_encoding)
            (Data_encoding.dft None None "delegate_selection"
              delegate_selection_encoding Random))))).

Module t.
  Record record : Set := Build {
    fixed : fixed;
    parametric : parametric }.
  Definition with_fixed fixed (r : record) :=
    Build fixed r.(parametric).
  Definition with_parametric parametric (r : record) :=
    Build r.(fixed) parametric.
End t.
Definition t := t.record.

Definition all_of_parametric (parametric_value : parametric) : t :=
  {| t.fixed := fixed_value; t.parametric := parametric_value |}.

Definition encoding : Data_encoding.encoding t :=
  Data_encoding.conv
    (fun (function_parameter : t) =>
      let '{| t.fixed := fixed_value; t.parametric := parametric_value |} :=
        function_parameter in
      (fixed_value, parametric_value))
    (fun (function_parameter : fixed * parametric) =>
      let '(fixed_value, parametric_value) := function_parameter in
      {| t.fixed := fixed_value; t.parametric := parametric_value |}) None
    (Data_encoding.merge_objs fixed_encoding parametric_encoding).

(** Init function; without side-effects in Coq *)
Definition init_module_repr : unit :=
  Error_monad.register_error_kind Error_monad.Permanent
    "constants.invalid_protocol_constants" "Invalid protocol constants"
    "The provided protocol constants are not coherent."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (reason : string) =>
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal
                "Invalid protocol constants: "
                (CamlinternalFormatBasics.String
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.End_of_format))
              "Invalid protocol constants: %s") reason))
    (Data_encoding.obj1
      (Data_encoding.req None None "reason" Data_encoding.string_value))
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Invalid_protocol_constants" then
          let 'reason := cast string payload in
          Some reason
        else None
      end)
    (fun (reason : string) =>
      Build_extensible "Invalid_protocol_constants" string reason).

Definition check_constants (constants : parametric) : M? unit :=
  let? '_ :=
    Error_monad.error_unless
      (constants.(parametric.consensus_committee_size) >i 3)
      (Build_extensible "Invalid_protocol_constants" string
        "The consensus committee size must be strictly greater than 3.") in
  let? '_ :=
    Error_monad.error_unless
      ((constants.(parametric.consensus_threshold) >=i 0) &&
      (constants.(parametric.consensus_threshold) <=i
      constants.(parametric.consensus_committee_size)))
      (Build_extensible "Invalid_protocol_constants" string
        "The consensus threshold must be greater than or equal to 0 and less than or equal to the consensus commitee size.")
    in
  let? '_ :=
    Error_monad.error_unless
      (let '{|
        ratio.numerator := numerator; ratio.denominator := denominator |} :=
        constants.(parametric.minimal_participation_ratio) in
      (numerator >=i 0) && (denominator >i 0))
      (Build_extensible "Invalid_protocol_constants" string
        "The minimal participation ratio must be a non-negative valid ratio.")
    in
  let? '_ :=
    Error_monad.error_unless
      (constants.(parametric.minimal_participation_ratio).(ratio.numerator) <=i
      constants.(parametric.minimal_participation_ratio).(ratio.denominator))
      (Build_extensible "Invalid_protocol_constants" string
        "The minimal participation ratio must be less than or equal to 100%.")
    in
  let? '_ :=
    Error_monad.error_unless (constants.(parametric.max_slashing_period) >i 0)
      (Build_extensible "Invalid_protocol_constants" string
        "The unfreeze delay must be strictly greater than 0.") in
  let? '_ :=
    Error_monad.error_unless
      ((constants.(parametric.frozen_deposits_percentage) >i 0) &&
      (constants.(parametric.frozen_deposits_percentage) <=i 100))
      (Build_extensible "Invalid_protocol_constants" string
        "The frozen percentage ratio must be strictly greater than 0 and less or equal than 100.")
    in
  let? '_ :=
    Error_monad.error_unless
      (Tez_repr.op_gteq constants.(parametric.double_baking_punishment)
        Tez_repr.zero)
      (Build_extensible "Invalid_protocol_constants" string
        "The double baking punishment must be non-negative.") in
  let? '_ :=
    Error_monad.error_unless
      (let '{|
        ratio.numerator := numerator; ratio.denominator := denominator |} :=
        constants.(parametric.ratio_of_frozen_deposits_slashed_per_double_endorsement)
        in
      (numerator >=i 0) && (denominator >i 0))
      (Build_extensible "Invalid_protocol_constants" string
        "The ratio of frozen deposits ratio slashed per double endorsement must be a non-negative valid ratio.")
    in
  Result.return_unit.

Module Generated.
  Module t.
    Record record : Set := Build {
      consensus_threshold : int;
      baking_reward_fixed_portion : Tez_repr.t;
      baking_reward_bonus_per_slot : Tez_repr.t;
      endorsing_reward_per_slot : Tez_repr.t }.
    Definition with_consensus_threshold consensus_threshold (r : record) :=
      Build consensus_threshold r.(baking_reward_fixed_portion)
        r.(baking_reward_bonus_per_slot) r.(endorsing_reward_per_slot).
    Definition with_baking_reward_fixed_portion baking_reward_fixed_portion
      (r : record) :=
      Build r.(consensus_threshold) baking_reward_fixed_portion
        r.(baking_reward_bonus_per_slot) r.(endorsing_reward_per_slot).
    Definition with_baking_reward_bonus_per_slot baking_reward_bonus_per_slot
      (r : record) :=
      Build r.(consensus_threshold) r.(baking_reward_fixed_portion)
        baking_reward_bonus_per_slot r.(endorsing_reward_per_slot).
    Definition with_endorsing_reward_per_slot endorsing_reward_per_slot
      (r : record) :=
      Build r.(consensus_threshold) r.(baking_reward_fixed_portion)
        r.(baking_reward_bonus_per_slot) endorsing_reward_per_slot.
  End t.
  Definition t := t.record.
  
  Definition generate
    (consensus_committee_size : int) (blocks_per_minute : ratio) : t :=
    let consensus_threshold := ((consensus_committee_size *i 2) /i 3) +i 1 in
    let rewards_per_minute := Tez_repr.mul_exn Tez_repr.one 80 in
    let rewards_per_block :=
      Tez_repr.div_exn
        (Tez_repr.mul_exn rewards_per_minute
          blocks_per_minute.(ratio.denominator))
        blocks_per_minute.(ratio.numerator) in
    let rewards_half := Tez_repr.div_exn rewards_per_block 2 in
    let rewards_quarter := Tez_repr.div_exn rewards_per_block 4 in
    {| t.consensus_threshold := consensus_threshold;
      t.baking_reward_fixed_portion := rewards_quarter;
      t.baking_reward_bonus_per_slot :=
        Tez_repr.div_exn rewards_quarter
          (consensus_committee_size -i consensus_threshold);
      t.endorsing_reward_per_slot :=
        Tez_repr.div_exn rewards_half consensus_committee_size |}.
End Generated.

Module Proto_previous.
  Module parametric.
    Record record : Set := Build {
      preserved_cycles : int;
      blocks_per_cycle : int32;
      blocks_per_commitment : int32;
      blocks_per_roll_snapshot : int32;
      blocks_per_voting_period : int32;
      time_between_blocks : list Period_repr.t;
      minimal_block_delay : Period_repr.t;
      endorsers_per_block : int;
      hard_gas_limit_per_operation : Gas_limit_repr.Arith.integral;
      hard_gas_limit_per_block : Gas_limit_repr.Arith.integral;
      proof_of_work_threshold : int64;
      tokens_per_roll : Tez_repr.t;
      seed_nonce_revelation_tip : Tez_repr.t;
      origination_size : int;
      block_security_deposit : Tez_repr.t;
      endorsement_security_deposit : Tez_repr.t;
      baking_reward_per_endorsement : list Tez_repr.t;
      endorsement_reward : list Tez_repr.t;
      cost_per_byte : Tez_repr.t;
      hard_storage_limit_per_operation : Z.t;
      quorum_min : int32;
      quorum_max : int32;
      min_proposal_quorum : int32;
      initial_endorsers : int;
      delay_per_missing_endorsement : Period_repr.t;
      liquidity_baking_subsidy : Tez_repr.t;
      liquidity_baking_sunset_level : int32;
      liquidity_baking_escape_ema_threshold : int32 }.
    Definition with_preserved_cycles preserved_cycles (r : record) :=
      Build preserved_cycles r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_blocks_per_cycle blocks_per_cycle (r : record) :=
      Build r.(preserved_cycles) blocks_per_cycle r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_blocks_per_commitment blocks_per_commitment (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) blocks_per_commitment
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_blocks_per_roll_snapshot blocks_per_roll_snapshot
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        blocks_per_roll_snapshot r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_blocks_per_voting_period blocks_per_voting_period
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) blocks_per_voting_period
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_time_between_blocks time_between_blocks (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        time_between_blocks r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_minimal_block_delay minimal_block_delay (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) minimal_block_delay r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_endorsers_per_block endorsers_per_block (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) endorsers_per_block
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_hard_gas_limit_per_operation hard_gas_limit_per_operation
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        hard_gas_limit_per_operation r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_hard_gas_limit_per_block hard_gas_limit_per_block
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) hard_gas_limit_per_block
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_proof_of_work_threshold proof_of_work_threshold
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        proof_of_work_threshold r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_tokens_per_roll tokens_per_roll (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) tokens_per_roll
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_seed_nonce_revelation_tip seed_nonce_revelation_tip
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        seed_nonce_revelation_tip r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_origination_size origination_size (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) origination_size
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_block_security_deposit block_security_deposit
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        block_security_deposit r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_endorsement_security_deposit endorsement_security_deposit
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) endorsement_security_deposit
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_baking_reward_per_endorsement baking_reward_per_endorsement
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        baking_reward_per_endorsement r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_endorsement_reward endorsement_reward (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) endorsement_reward r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_cost_per_byte cost_per_byte (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward) cost_per_byte
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_hard_storage_limit_per_operation
      hard_storage_limit_per_operation (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) hard_storage_limit_per_operation r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_quorum_min quorum_min (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) quorum_min
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_quorum_max quorum_max (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        quorum_max r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_min_proposal_quorum min_proposal_quorum (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) min_proposal_quorum r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_initial_endorsers initial_endorsers (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) initial_endorsers
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_delay_per_missing_endorsement delay_per_missing_endorsement
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        delay_per_missing_endorsement r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_liquidity_baking_subsidy liquidity_baking_subsidy
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) liquidity_baking_subsidy
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_liquidity_baking_sunset_level liquidity_baking_sunset_level
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        liquidity_baking_sunset_level r.(liquidity_baking_escape_ema_threshold).
    Definition with_liquidity_baking_escape_ema_threshold
      liquidity_baking_escape_ema_threshold (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(seed_nonce_revelation_tip) r.(origination_size)
        r.(block_security_deposit) r.(endorsement_security_deposit)
        r.(baking_reward_per_endorsement) r.(endorsement_reward)
        r.(cost_per_byte) r.(hard_storage_limit_per_operation) r.(quorum_min)
        r.(quorum_max) r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level) liquidity_baking_escape_ema_threshold.
  End parametric.
  Definition parametric := parametric.record.
  
  Definition parametric_encoding : Data_encoding.encoding parametric :=
    Data_encoding.conv
      (fun (c_value : parametric) =>
        ((c_value.(parametric.preserved_cycles),
          c_value.(parametric.blocks_per_cycle),
          c_value.(parametric.blocks_per_commitment),
          c_value.(parametric.blocks_per_roll_snapshot),
          c_value.(parametric.blocks_per_voting_period),
          c_value.(parametric.time_between_blocks),
          c_value.(parametric.endorsers_per_block),
          c_value.(parametric.hard_gas_limit_per_operation),
          c_value.(parametric.hard_gas_limit_per_block),
          c_value.(parametric.proof_of_work_threshold)),
          ((c_value.(parametric.tokens_per_roll),
            c_value.(parametric.seed_nonce_revelation_tip),
            c_value.(parametric.origination_size),
            c_value.(parametric.block_security_deposit),
            c_value.(parametric.endorsement_security_deposit),
            c_value.(parametric.baking_reward_per_endorsement),
            c_value.(parametric.endorsement_reward),
            c_value.(parametric.cost_per_byte),
            c_value.(parametric.hard_storage_limit_per_operation)),
            (c_value.(parametric.quorum_min), c_value.(parametric.quorum_max),
              c_value.(parametric.min_proposal_quorum),
              c_value.(parametric.initial_endorsers),
              c_value.(parametric.delay_per_missing_endorsement),
              c_value.(parametric.minimal_block_delay),
              c_value.(parametric.liquidity_baking_subsidy),
              c_value.(parametric.liquidity_baking_sunset_level),
              c_value.(parametric.liquidity_baking_escape_ema_threshold)))))
      (fun (function_parameter :
        (int * int32 * int32 * int32 * int32 * list Period_repr.t * int *
          Gas_limit_repr.Arith.integral * Gas_limit_repr.Arith.integral * int64)
          *
          ((Tez_repr.t * Tez_repr.t * int * Tez_repr.t * Tez_repr.t *
            list Tez_repr.t * list Tez_repr.t * Tez_repr.t * Z.t) *
            (int32 * int32 * int32 * int * Period_repr.t * Period_repr.t *
              Tez_repr.t * int32 * int32))) =>
        let
          '((preserved_cycles, blocks_per_cycle, blocks_per_commitment,
            blocks_per_roll_snapshot, blocks_per_voting_period,
            time_between_blocks, endorsers_per_block,
            hard_gas_limit_per_operation, hard_gas_limit_per_block,
            proof_of_work_threshold),
            ((tokens_per_roll, seed_nonce_revelation_tip, origination_size,
              block_security_deposit, endorsement_security_deposit,
              baking_reward_per_endorsement, endorsement_reward, cost_per_byte,
              hard_storage_limit_per_operation),
              (quorum_min, quorum_max, min_proposal_quorum, initial_endorsers,
                delay_per_missing_endorsement, minimal_block_delay,
                liquidity_baking_subsidy, liquidity_baking_sunset_level,
                liquidity_baking_escape_ema_threshold))) := function_parameter
          in
        {| parametric.preserved_cycles := preserved_cycles;
          parametric.blocks_per_cycle := blocks_per_cycle;
          parametric.blocks_per_commitment := blocks_per_commitment;
          parametric.blocks_per_roll_snapshot := blocks_per_roll_snapshot;
          parametric.blocks_per_voting_period := blocks_per_voting_period;
          parametric.time_between_blocks := time_between_blocks;
          parametric.minimal_block_delay := minimal_block_delay;
          parametric.endorsers_per_block := endorsers_per_block;
          parametric.hard_gas_limit_per_operation :=
            hard_gas_limit_per_operation;
          parametric.hard_gas_limit_per_block := hard_gas_limit_per_block;
          parametric.proof_of_work_threshold := proof_of_work_threshold;
          parametric.tokens_per_roll := tokens_per_roll;
          parametric.seed_nonce_revelation_tip := seed_nonce_revelation_tip;
          parametric.origination_size := origination_size;
          parametric.block_security_deposit := block_security_deposit;
          parametric.endorsement_security_deposit :=
            endorsement_security_deposit;
          parametric.baking_reward_per_endorsement :=
            baking_reward_per_endorsement;
          parametric.endorsement_reward := endorsement_reward;
          parametric.cost_per_byte := cost_per_byte;
          parametric.hard_storage_limit_per_operation :=
            hard_storage_limit_per_operation;
          parametric.quorum_min := quorum_min;
          parametric.quorum_max := quorum_max;
          parametric.min_proposal_quorum := min_proposal_quorum;
          parametric.initial_endorsers := initial_endorsers;
          parametric.delay_per_missing_endorsement :=
            delay_per_missing_endorsement;
          parametric.liquidity_baking_subsidy := liquidity_baking_subsidy;
          parametric.liquidity_baking_sunset_level :=
            liquidity_baking_sunset_level;
          parametric.liquidity_baking_escape_ema_threshold :=
            liquidity_baking_escape_ema_threshold |}) None
      (Data_encoding.merge_objs
        (Data_encoding.obj10
          (Data_encoding.req None None "preserved_cycles" Data_encoding.uint8)
          (Data_encoding.req None None "blocks_per_cycle"
            Data_encoding.int32_value)
          (Data_encoding.req None None "blocks_per_commitment"
            Data_encoding.int32_value)
          (Data_encoding.req None None "blocks_per_roll_snapshot"
            Data_encoding.int32_value)
          (Data_encoding.req None None "blocks_per_voting_period"
            Data_encoding.int32_value)
          (Data_encoding.req None None "time_between_blocks"
            (Data_encoding.list_value None Period_repr.encoding))
          (Data_encoding.req None None "endorsers_per_block"
            Data_encoding.uint16)
          (Data_encoding.req None None "hard_gas_limit_per_operation"
            Gas_limit_repr.Arith.z_integral_encoding)
          (Data_encoding.req None None "hard_gas_limit_per_block"
            Gas_limit_repr.Arith.z_integral_encoding)
          (Data_encoding.req None None "proof_of_work_threshold"
            Data_encoding.int64_value))
        (Data_encoding.merge_objs
          (Data_encoding.obj9
            (Data_encoding.req None None "tokens_per_roll" Tez_repr.encoding)
            (Data_encoding.req None None "seed_nonce_revelation_tip"
              Tez_repr.encoding)
            (Data_encoding.req None None "origination_size" Data_encoding.int31)
            (Data_encoding.req None None "block_security_deposit"
              Tez_repr.encoding)
            (Data_encoding.req None None "endorsement_security_deposit"
              Tez_repr.encoding)
            (Data_encoding.req None None "baking_reward_per_endorsement"
              (Data_encoding.list_value None Tez_repr.encoding))
            (Data_encoding.req None None "endorsement_reward"
              (Data_encoding.list_value None Tez_repr.encoding))
            (Data_encoding.req None None "cost_per_byte" Tez_repr.encoding)
            (Data_encoding.req None None "hard_storage_limit_per_operation"
              Data_encoding.z_value))
          (Data_encoding.obj9
            (Data_encoding.req None None "quorum_min" Data_encoding.int32_value)
            (Data_encoding.req None None "quorum_max" Data_encoding.int32_value)
            (Data_encoding.req None None "min_proposal_quorum"
              Data_encoding.int32_value)
            (Data_encoding.req None None "initial_endorsers"
              Data_encoding.uint16)
            (Data_encoding.req None None "delay_per_missing_endorsement"
              Period_repr.encoding)
            (Data_encoding.req None None "minimal_block_delay"
              Period_repr.encoding)
            (Data_encoding.req None None "liquidity_baking_subsidy"
              Tez_repr.encoding)
            (Data_encoding.req None None "liquidity_baking_sunset_level"
              Data_encoding.int32_value)
            (Data_encoding.req None None "liquidity_baking_escape_ema_threshold"
              Data_encoding.int32_value)))).
End Proto_previous.
