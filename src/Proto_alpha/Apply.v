Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Amendment.
Require TezosOfOCaml.Proto_alpha.Apply_results.
Require TezosOfOCaml.Proto_alpha.Baking.
Require TezosOfOCaml.Proto_alpha.Blinded_public_key_hash.
Require TezosOfOCaml.Proto_alpha.Block_payload_hash.
Require TezosOfOCaml.Proto_alpha.Constants_repr.
Require TezosOfOCaml.Proto_alpha.Michelson_v1_gas.
Require TezosOfOCaml.Proto_alpha.Michelson_v1_primitives.
Require TezosOfOCaml.Proto_alpha.Nonce_hash.
Require TezosOfOCaml.Proto_alpha.Saturation_repr.
Require TezosOfOCaml.Proto_alpha.Script_cache.
Require TezosOfOCaml.Proto_alpha.Script_expr_hash.
Require TezosOfOCaml.Proto_alpha.Script_interpreter.
Require TezosOfOCaml.Proto_alpha.Script_ir_translator_mli.
  Module Script_ir_translator := Script_ir_translator_mli.
Require TezosOfOCaml.Proto_alpha.Script_tc_errors.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.

Module Not_enough_endorsements.
  Record record : Set := Build {
    required : int;
    endorsements : int }.
  Definition with_required required (r : record) :=
    Build required r.(endorsements).
  Definition with_endorsements endorsements (r : record) :=
    Build r.(required) endorsements.
End Not_enough_endorsements.
Definition Not_enough_endorsements := Not_enough_endorsements.record.

Module Invalid_double_baking_evidence.
  Record record : Set := Build {
    hash1 : Block_hash.t;
    level1 : Alpha_context.Raw_level.t;
    round1 : Alpha_context.Round.t;
    hash2 : Block_hash.t;
    level2 : Alpha_context.Raw_level.t;
    round2 : Alpha_context.Round.t }.
  Definition with_hash1 hash1 (r : record) :=
    Build hash1 r.(level1) r.(round1) r.(hash2) r.(level2) r.(round2).
  Definition with_level1 level1 (r : record) :=
    Build r.(hash1) level1 r.(round1) r.(hash2) r.(level2) r.(round2).
  Definition with_round1 round1 (r : record) :=
    Build r.(hash1) r.(level1) round1 r.(hash2) r.(level2) r.(round2).
  Definition with_hash2 hash2 (r : record) :=
    Build r.(hash1) r.(level1) r.(round1) hash2 r.(level2) r.(round2).
  Definition with_level2 level2 (r : record) :=
    Build r.(hash1) r.(level1) r.(round1) r.(hash2) level2 r.(round2).
  Definition with_round2 round2 (r : record) :=
    Build r.(hash1) r.(level1) r.(round1) r.(hash2) r.(level2) round2.
End Invalid_double_baking_evidence.
Definition Invalid_double_baking_evidence :=
  Invalid_double_baking_evidence.record.

Module Wrong_level_for_consensus_operation.
  Record record : Set := Build {
    expected : Alpha_context.Raw_level.t;
    provided : Alpha_context.Raw_level.t }.
  Definition with_expected expected (r : record) :=
    Build expected r.(provided).
  Definition with_provided provided (r : record) :=
    Build r.(expected) provided.
End Wrong_level_for_consensus_operation.
Definition Wrong_level_for_consensus_operation :=
  Wrong_level_for_consensus_operation.record.

Module Wrong_round_for_consensus_operation.
  Record record : Set := Build {
    expected : Alpha_context.Round.t;
    provided : Alpha_context.Round.t }.
  Definition with_expected expected (r : record) :=
    Build expected r.(provided).
  Definition with_provided provided (r : record) :=
    Build r.(expected) provided.
End Wrong_round_for_consensus_operation.
Definition Wrong_round_for_consensus_operation :=
  Wrong_round_for_consensus_operation.record.

Module Preendorsement_round_too_high.
  Record record : Set := Build {
    block_round : Alpha_context.Round.t;
    provided : Alpha_context.Round.t }.
  Definition with_block_round block_round (r : record) :=
    Build block_round r.(provided).
  Definition with_provided provided (r : record) :=
    Build r.(block_round) provided.
End Preendorsement_round_too_high.
Definition Preendorsement_round_too_high :=
  Preendorsement_round_too_high.record.

Module Wrong_payload_hash_for_consensus_operation.
  Record record : Set := Build {
    expected : Block_payload_hash.t;
    provided : Block_payload_hash.t }.
  Definition with_expected expected (r : record) :=
    Build expected r.(provided).
  Definition with_provided provided (r : record) :=
    Build r.(expected) provided.
End Wrong_payload_hash_for_consensus_operation.
Definition Wrong_payload_hash_for_consensus_operation :=
  Wrong_payload_hash_for_consensus_operation.record.

Module Consensus_operation_for_future_level.
  Record record : Set := Build {
    expected : Alpha_context.Raw_level.t;
    provided : Alpha_context.Raw_level.t }.
  Definition with_expected expected (r : record) :=
    Build expected r.(provided).
  Definition with_provided provided (r : record) :=
    Build r.(expected) provided.
End Consensus_operation_for_future_level.
Definition Consensus_operation_for_future_level :=
  Consensus_operation_for_future_level.record.

Module Consensus_operation_for_future_round.
  Record record : Set := Build {
    expected : Alpha_context.Round.t;
    provided : Alpha_context.Round.t }.
  Definition with_expected expected (r : record) :=
    Build expected r.(provided).
  Definition with_provided provided (r : record) :=
    Build r.(expected) provided.
End Consensus_operation_for_future_round.
Definition Consensus_operation_for_future_round :=
  Consensus_operation_for_future_round.record.

Module Consensus_operation_for_old_level.
  Record record : Set := Build {
    expected : Alpha_context.Raw_level.t;
    provided : Alpha_context.Raw_level.t }.
  Definition with_expected expected (r : record) :=
    Build expected r.(provided).
  Definition with_provided provided (r : record) :=
    Build r.(expected) provided.
End Consensus_operation_for_old_level.
Definition Consensus_operation_for_old_level :=
  Consensus_operation_for_old_level.record.

Module Consensus_operation_for_old_round.
  Record record : Set := Build {
    expected : Alpha_context.Round.t;
    provided : Alpha_context.Round.t }.
  Definition with_expected expected (r : record) :=
    Build expected r.(provided).
  Definition with_provided provided (r : record) :=
    Build r.(expected) provided.
End Consensus_operation_for_old_round.
Definition Consensus_operation_for_old_round :=
  Consensus_operation_for_old_round.record.

Module Consensus_operation_on_competing_proposal.
  Record record : Set := Build {
    expected : Block_payload_hash.t;
    provided : Block_payload_hash.t }.
  Definition with_expected expected (r : record) :=
    Build expected r.(provided).
  Definition with_provided provided (r : record) :=
    Build r.(expected) provided.
End Consensus_operation_on_competing_proposal.
Definition Consensus_operation_on_competing_proposal :=
  Consensus_operation_on_competing_proposal.record.

Module Set_deposits_limit_too_high.
  Record record : Set := Build {
    limit : Alpha_context.Tez.t;
    max_limit : Alpha_context.Tez.t }.
  Definition with_limit limit (r : record) :=
    Build limit r.(max_limit).
  Definition with_max_limit max_limit (r : record) :=
    Build r.(limit) max_limit.
End Set_deposits_limit_too_high.
Definition Set_deposits_limit_too_high := Set_deposits_limit_too_high.record.

(** Init function; without side-effects in Coq *)
Definition init_module1 : unit :=
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "operations.wrong_slot" "wrong slot" "wrong slot"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "wrong slot"
                  CamlinternalFormatBasics.End_of_format) "wrong slot")))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Wrong_slot_used_for_consensus_operation" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Wrong_slot_used_for_consensus_operation" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "operation.not_enough_endorsements" "Not enough endorsements"
      "The block being validated does not include the required minimum number of endorsements."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : int * int) =>
            let '(required, endorsements) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Wrong number of endorsements ("
                  (CamlinternalFormatBasics.Int CamlinternalFormatBasics.Int_i
                    CamlinternalFormatBasics.No_padding
                    CamlinternalFormatBasics.No_precision
                    (CamlinternalFormatBasics.String_literal "), at least "
                      (CamlinternalFormatBasics.Int
                        CamlinternalFormatBasics.Int_i
                        CamlinternalFormatBasics.No_padding
                        CamlinternalFormatBasics.No_precision
                        (CamlinternalFormatBasics.String_literal " are expected"
                          CamlinternalFormatBasics.End_of_format)))))
                "Wrong number of endorsements (%i), at least %i are expected")
              endorsements required))
      (Data_encoding.obj2
        (Data_encoding.req None None "required" Data_encoding.int31)
        (Data_encoding.req None None "endorsements" Data_encoding.int31))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Not_enough_endorsements" then
            let '{|
              Not_enough_endorsements.required := required;
                Not_enough_endorsements.endorsements := endorsements
                |} := cast Not_enough_endorsements payload in
            Some (required, endorsements)
          else None
        end)
      (fun (function_parameter : int * int) =>
        let '(required, endorsements) := function_parameter in
        Build_extensible "Not_enough_endorsements" Not_enough_endorsements
          {| Not_enough_endorsements.required := required;
            Not_enough_endorsements.endorsements := endorsements |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "operation.wrong_consensus_operation_branch"
      "Wrong consensus operation branch"
      "Trying to include an endorsement or preendorsement which points to the wrong block.\n       It should be the predecessor for preendorsements and the grandfather for endorsements."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : Block_hash.t * Block_hash.t) =>
            let '(e_value, p_value) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "Wrong branch "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal ", expected "
                      (CamlinternalFormatBasics.Alpha
                        CamlinternalFormatBasics.End_of_format))))
                "Wrong branch %a, expected %a") Block_hash.pp p_value
              Block_hash.pp e_value))
      (Data_encoding.obj2
        (Data_encoding.req None None "expected" Block_hash.encoding)
        (Data_encoding.req None None "provided" Block_hash.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Wrong_consensus_operation_branch" then
            let '(e_value, p_value) :=
              cast (Block_hash.t * Block_hash.t) payload in
            Some (e_value, p_value)
          else None
        end)
      (fun (function_parameter : Block_hash.t * Block_hash.t) =>
        let '(e_value, p_value) := function_parameter in
        Build_extensible "Wrong_consensus_operation_branch"
          (Block_hash.t * Block_hash.t) (e_value, p_value)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "block.invalid_double_baking_evidence" "Invalid double baking evidence"
      "A double-baking evidence is inconsistent  (two distinct level)"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            Block_hash.t * Alpha_context.Raw_level.raw_level *
              Alpha_context.Round.t * Block_hash.t *
              Alpha_context.Raw_level.raw_level * Alpha_context.Round.t) =>
            let '(hash1, level1, round1, hash2, level2, round2) :=
              function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Invalid double-baking evidence (hash: "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal " and "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal
                          ", levels/rounds: ("
                          (CamlinternalFormatBasics.Int32
                            CamlinternalFormatBasics.Int_d
                            CamlinternalFormatBasics.No_padding
                            CamlinternalFormatBasics.No_precision
                            (CamlinternalFormatBasics.Char_literal "," % char
                              (CamlinternalFormatBasics.Int32
                                CamlinternalFormatBasics.Int_d
                                CamlinternalFormatBasics.No_padding
                                CamlinternalFormatBasics.No_precision
                                (CamlinternalFormatBasics.String_literal
                                  ") and ("
                                  (CamlinternalFormatBasics.Int32
                                    CamlinternalFormatBasics.Int_d
                                    CamlinternalFormatBasics.No_padding
                                    CamlinternalFormatBasics.No_precision
                                    (CamlinternalFormatBasics.Char_literal
                                      "," % char
                                      (CamlinternalFormatBasics.Int32
                                        CamlinternalFormatBasics.Int_d
                                        CamlinternalFormatBasics.No_padding
                                        CamlinternalFormatBasics.No_precision
                                        (CamlinternalFormatBasics.String_literal
                                          "))"
                                          CamlinternalFormatBasics.End_of_format)))))))))))))
                "Invalid double-baking evidence (hash: %a and %a, levels/rounds: (%ld,%ld) and (%ld,%ld))")
              Block_hash.pp hash1 Block_hash.pp hash2
              (Alpha_context.Raw_level.to_int32 level1)
              (Alpha_context.Round.to_int32 round1)
              (Alpha_context.Raw_level.to_int32 level2)
              (Alpha_context.Round.to_int32 round2)))
      (Data_encoding.obj6
        (Data_encoding.req None None "hash1" Block_hash.encoding)
        (Data_encoding.req None None "level1" Alpha_context.Raw_level.encoding)
        (Data_encoding.req None None "round1" Alpha_context.Round.encoding)
        (Data_encoding.req None None "hash2" Block_hash.encoding)
        (Data_encoding.req None None "level2" Alpha_context.Raw_level.encoding)
        (Data_encoding.req None None "round2" Alpha_context.Round.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_double_baking_evidence" then
            let '{|
              Invalid_double_baking_evidence.hash1 := hash1;
                Invalid_double_baking_evidence.level1 := level1;
                Invalid_double_baking_evidence.round1 := round1;
                Invalid_double_baking_evidence.hash2 := hash2;
                Invalid_double_baking_evidence.level2 := level2;
                Invalid_double_baking_evidence.round2 := round2
                |} := cast Invalid_double_baking_evidence payload in
            Some (hash1, level1, round1, hash2, level2, round2)
          else None
        end)
      (fun (function_parameter :
        Block_hash.t * Alpha_context.Raw_level.raw_level * Alpha_context.Round.t
          * Block_hash.t * Alpha_context.Raw_level.raw_level *
          Alpha_context.Round.t) =>
        let '(hash1, level1, round1, hash2, level2, round2) :=
          function_parameter in
        Build_extensible "Invalid_double_baking_evidence"
          Invalid_double_baking_evidence
          {| Invalid_double_baking_evidence.hash1 := hash1;
            Invalid_double_baking_evidence.level1 := level1;
            Invalid_double_baking_evidence.round1 := round1;
            Invalid_double_baking_evidence.hash2 := hash2;
            Invalid_double_baking_evidence.level2 := level2;
            Invalid_double_baking_evidence.round2 := round2 |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "wrong_level_for_consensus_operation"
      "wrong level for consensus operation"
      "Wrong level for consensus operation."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            Alpha_context.Raw_level.t * Alpha_context.Raw_level.t) =>
            let '(expected, provided) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Wrong level for consensus operation (expected: "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal ", provided: "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal ")."
                          CamlinternalFormatBasics.End_of_format)))))
                "Wrong level for consensus operation (expected: %a, provided: %a).")
              Alpha_context.Raw_level.pp expected Alpha_context.Raw_level.pp
              provided))
      (Data_encoding.obj2
        (Data_encoding.req None None "expected" Alpha_context.Raw_level.encoding)
        (Data_encoding.req None None "provided" Alpha_context.Raw_level.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Wrong_level_for_consensus_operation" then
            let '{|
              Wrong_level_for_consensus_operation.expected := expected;
                Wrong_level_for_consensus_operation.provided := provided
                |} := cast Wrong_level_for_consensus_operation payload in
            Some (expected, provided)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Raw_level.t * Alpha_context.Raw_level.t) =>
        let '(expected, provided) := function_parameter in
        Build_extensible "Wrong_level_for_consensus_operation"
          Wrong_level_for_consensus_operation
          {| Wrong_level_for_consensus_operation.expected := expected;
            Wrong_level_for_consensus_operation.provided := provided |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "wrong_round_for_consensus_operation"
      "wrong round for consensus operation"
      "Wrong round for consensus operation."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            Alpha_context.Round.t * Alpha_context.Round.t) =>
            let '(expected, provided) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Wrong round for consensus operation (expected: "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal ", provided: "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal ")."
                          CamlinternalFormatBasics.End_of_format)))))
                "Wrong round for consensus operation (expected: %a, provided: %a).")
              Alpha_context.Round.pp expected Alpha_context.Round.pp provided))
      (Data_encoding.obj2
        (Data_encoding.req None None "expected" Alpha_context.Round.encoding)
        (Data_encoding.req None None "provided" Alpha_context.Round.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Wrong_round_for_consensus_operation" then
            let '{|
              Wrong_round_for_consensus_operation.expected := expected;
                Wrong_round_for_consensus_operation.provided := provided
                |} := cast Wrong_round_for_consensus_operation payload in
            Some (expected, provided)
          else None
        end)
      (fun (function_parameter : Alpha_context.Round.t * Alpha_context.Round.t)
        =>
        let '(expected, provided) := function_parameter in
        Build_extensible "Wrong_round_for_consensus_operation"
          Wrong_round_for_consensus_operation
          {| Wrong_round_for_consensus_operation.expected := expected;
            Wrong_round_for_consensus_operation.provided := provided |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "preendorsement_round_too_high" "preendorsement round too high"
      "Preendorsement round too high."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            Alpha_context.Round.t * Alpha_context.Round.t) =>
            let '(block_round, provided) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Preendorsement round too high (block_round: "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal ", provided: "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal ")."
                          CamlinternalFormatBasics.End_of_format)))))
                "Preendorsement round too high (block_round: %a, provided: %a).")
              Alpha_context.Round.pp block_round Alpha_context.Round.pp provided))
      (Data_encoding.obj2
        (Data_encoding.req None None "block_round" Alpha_context.Round.encoding)
        (Data_encoding.req None None "provided" Alpha_context.Round.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Preendorsement_round_too_high" then
            let '{|
              Preendorsement_round_too_high.block_round := block_round;
                Preendorsement_round_too_high.provided := provided
                |} := cast Preendorsement_round_too_high payload in
            Some (block_round, provided)
          else None
        end)
      (fun (function_parameter : Alpha_context.Round.t * Alpha_context.Round.t)
        =>
        let '(block_round, provided) := function_parameter in
        Build_extensible "Preendorsement_round_too_high"
          Preendorsement_round_too_high
          {| Preendorsement_round_too_high.block_round := block_round;
            Preendorsement_round_too_high.provided := provided |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "wrong_payload_hash_for_consensus_operation"
      "wrong payload hash for consensus operation"
      "Wrong payload hash for consensus operation."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : Block_payload_hash.t * Block_payload_hash.t)
            =>
            let '(expected, provided) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Wrong payload hash for consensus operation (expected: "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal ", provided: "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal ")."
                          CamlinternalFormatBasics.End_of_format)))))
                "Wrong payload hash for consensus operation (expected: %a, provided: %a).")
              Block_payload_hash.pp_short expected Block_payload_hash.pp_short
              provided))
      (Data_encoding.obj2
        (Data_encoding.req None None "expected" Block_payload_hash.encoding)
        (Data_encoding.req None None "provided" Block_payload_hash.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Wrong_payload_hash_for_consensus_operation" then
            let '{|
              Wrong_payload_hash_for_consensus_operation.expected := expected;
                Wrong_payload_hash_for_consensus_operation.provided := provided
                |} := cast Wrong_payload_hash_for_consensus_operation payload in
            Some (expected, provided)
          else None
        end)
      (fun (function_parameter : Block_payload_hash.t * Block_payload_hash.t) =>
        let '(expected, provided) := function_parameter in
        Build_extensible "Wrong_payload_hash_for_consensus_operation"
          Wrong_payload_hash_for_consensus_operation
          {| Wrong_payload_hash_for_consensus_operation.expected := expected;
            Wrong_payload_hash_for_consensus_operation.provided := provided |})
    in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "unexpected_endorsement_in_block" "unexpected endorsement in block"
      "Unexpected endorsement in block."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Unexpected endorsement in block."
                  CamlinternalFormatBasics.End_of_format)
                "Unexpected endorsement in block."))) Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Unexpected_endorsement_in_block" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Unexpected_endorsement_in_block" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "unexpected_preendorsement_in_block" "unexpected preendorsement in block"
      "Unexpected preendorsement in block."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Unexpected preendorsement in block."
                  CamlinternalFormatBasics.End_of_format)
                "Unexpected preendorsement in block."))) Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Unexpected_preendorsement_in_block" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Unexpected_preendorsement_in_block" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "consensus_operation_for_future_level"
      "Consensus operation for future level"
      "Consensus operation for future level."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            Alpha_context.Raw_level.t * Alpha_context.Raw_level.t) =>
            let '(expected, provided) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Consensus operation for future level\n                            (expected: "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal ", provided: "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal ")."
                          CamlinternalFormatBasics.End_of_format)))))
                "Consensus operation for future level\n                            (expected: %a, provided: %a).")
              Alpha_context.Raw_level.pp expected Alpha_context.Raw_level.pp
              provided))
      (Data_encoding.obj2
        (Data_encoding.req None None "expected" Alpha_context.Raw_level.encoding)
        (Data_encoding.req None None "provided" Alpha_context.Raw_level.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Consensus_operation_for_future_level" then
            let '{|
              Consensus_operation_for_future_level.expected := expected;
                Consensus_operation_for_future_level.provided := provided
                |} := cast Consensus_operation_for_future_level payload in
            Some (expected, provided)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Raw_level.t * Alpha_context.Raw_level.t) =>
        let '(expected, provided) := function_parameter in
        Build_extensible "Consensus_operation_for_future_level"
          Consensus_operation_for_future_level
          {| Consensus_operation_for_future_level.expected := expected;
            Consensus_operation_for_future_level.provided := provided |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "consensus_operation_for_future_round"
      "Consensus operation for future round"
      "Consensus operation for future round."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            Alpha_context.Round.t * Alpha_context.Round.t) =>
            let '(expected, provided) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Consensus operation for future round (expected: "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal ", provided: "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal ")."
                          CamlinternalFormatBasics.End_of_format)))))
                "Consensus operation for future round (expected: %a, provided: %a).")
              Alpha_context.Round.pp expected Alpha_context.Round.pp provided))
      (Data_encoding.obj2
        (Data_encoding.req None None "expected_max" Alpha_context.Round.encoding)
        (Data_encoding.req None None "provided" Alpha_context.Round.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Consensus_operation_for_future_round" then
            let '{|
              Consensus_operation_for_future_round.expected := expected;
                Consensus_operation_for_future_round.provided := provided
                |} := cast Consensus_operation_for_future_round payload in
            Some (expected, provided)
          else None
        end)
      (fun (function_parameter : Alpha_context.Round.t * Alpha_context.Round.t)
        =>
        let '(expected, provided) := function_parameter in
        Build_extensible "Consensus_operation_for_future_round"
          Consensus_operation_for_future_round
          {| Consensus_operation_for_future_round.expected := expected;
            Consensus_operation_for_future_round.provided := provided |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Outdated
      "consensus_operation_for_old_level" "Consensus operation for old level"
      "Consensus operation for old level."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            Alpha_context.Raw_level.t * Alpha_context.Raw_level.t) =>
            let '(expected, provided) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Consensus operation for old level (expected: "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal ", provided: "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal ")."
                          CamlinternalFormatBasics.End_of_format)))))
                "Consensus operation for old level (expected: %a, provided: %a).")
              Alpha_context.Raw_level.pp expected Alpha_context.Raw_level.pp
              provided))
      (Data_encoding.obj2
        (Data_encoding.req None None "expected" Alpha_context.Raw_level.encoding)
        (Data_encoding.req None None "provided" Alpha_context.Raw_level.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Consensus_operation_for_old_level" then
            let '{|
              Consensus_operation_for_old_level.expected := expected;
                Consensus_operation_for_old_level.provided := provided
                |} := cast Consensus_operation_for_old_level payload in
            Some (expected, provided)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Raw_level.t * Alpha_context.Raw_level.t) =>
        let '(expected, provided) := function_parameter in
        Build_extensible "Consensus_operation_for_old_level"
          Consensus_operation_for_old_level
          {| Consensus_operation_for_old_level.expected := expected;
            Consensus_operation_for_old_level.provided := provided |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Branch
      "consensus_operation_for_old_round" "Consensus operation for old round"
      "Consensus operation for old round."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            Alpha_context.Round.t * Alpha_context.Round.t) =>
            let '(expected, provided) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Consensus operation for old round (expected_min: "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal ", provided: "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal ")."
                          CamlinternalFormatBasics.End_of_format)))))
                "Consensus operation for old round (expected_min: %a, provided: %a).")
              Alpha_context.Round.pp expected Alpha_context.Round.pp provided))
      (Data_encoding.obj2
        (Data_encoding.req None None "expected_min" Alpha_context.Round.encoding)
        (Data_encoding.req None None "provided" Alpha_context.Round.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Consensus_operation_for_old_round" then
            let '{|
              Consensus_operation_for_old_round.expected := expected;
                Consensus_operation_for_old_round.provided := provided
                |} := cast Consensus_operation_for_old_round payload in
            Some (expected, provided)
          else None
        end)
      (fun (function_parameter : Alpha_context.Round.t * Alpha_context.Round.t)
        =>
        let '(expected, provided) := function_parameter in
        Build_extensible "Consensus_operation_for_old_round"
          Consensus_operation_for_old_round
          {| Consensus_operation_for_old_round.expected := expected;
            Consensus_operation_for_old_round.provided := provided |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Branch
      "consensus_operation_on_competing_proposal"
      "Consensus operation on competing proposal"
      "Consensus operation on competing proposal."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : Block_payload_hash.t * Block_payload_hash.t)
            =>
            let '(expected, provided) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Consensus operation on competing proposal (expected: "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal ", provided: "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal ")."
                          CamlinternalFormatBasics.End_of_format)))))
                "Consensus operation on competing proposal (expected: %a, provided: %a).")
              Block_payload_hash.pp_short expected Block_payload_hash.pp_short
              provided))
      (Data_encoding.obj2
        (Data_encoding.req None None "expected" Block_payload_hash.encoding)
        (Data_encoding.req None None "provided" Block_payload_hash.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Consensus_operation_on_competing_proposal" then
            let '{|
              Consensus_operation_on_competing_proposal.expected := expected;
                Consensus_operation_on_competing_proposal.provided := provided
                |} := cast Consensus_operation_on_competing_proposal payload in
            Some (expected, provided)
          else None
        end)
      (fun (function_parameter : Block_payload_hash.t * Block_payload_hash.t) =>
        let '(expected, provided) := function_parameter in
        Build_extensible "Consensus_operation_on_competing_proposal"
          Consensus_operation_on_competing_proposal
          {| Consensus_operation_on_competing_proposal.expected := expected;
            Consensus_operation_on_competing_proposal.provided := provided |})
    in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "operation.set_deposits_limit_on_originated_contract"
      "Set deposits limit on an originated contract"
      "Cannot set deposits limit on an originated contract."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Cannot set deposits limit on an originated contract."
                  CamlinternalFormatBasics.End_of_format)
                "Cannot set deposits limit on an originated contract.")))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Set_deposits_limit_on_originated_contract" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Set_deposits_limit_on_originated_contract" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "operation.set_deposits_limit_on_unregistered_delegate"
      "Set deposits limit on an unregistered delegate"
      "Cannot set deposits limit on an unregistered delegate."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (c_value : Signature.public_key_hash) =>
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Cannot set a deposits limit on the unregistered delegate "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.Char_literal "." % char
                      CamlinternalFormatBasics.End_of_format)))
                "Cannot set a deposits limit on the unregistered delegate %a.")
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp) c_value))
      (Data_encoding.obj1
        (Data_encoding.req None None "delegate"
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Set_deposits_limit_on_unregistered_delegate" then
            let 'c_value := cast Signature.public_key_hash payload in
            Some c_value
          else None
        end)
      (fun (c_value : Signature.public_key_hash) =>
        Build_extensible "Set_deposits_limit_on_unregistered_delegate"
          Signature.public_key_hash c_value) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "operation.set_deposits_limit_too_high"
      "Set deposits limit to a too high value"
      "Cannot set deposits limit such that the active stake overflows."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : Alpha_context.Tez.t * Alpha_context.Tez.t)
            =>
            let '(limit, max_limit) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Cannot set deposits limit to "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal
                      " as it is higher the allowed maximum "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.Char_literal "." % char
                          CamlinternalFormatBasics.End_of_format)))))
                "Cannot set deposits limit to %a as it is higher the allowed maximum %a.")
              Alpha_context.Tez.pp limit Alpha_context.Tez.pp max_limit))
      (Data_encoding.obj2
        (Data_encoding.req None None "limit" Alpha_context.Tez.encoding)
        (Data_encoding.req None None "max_limit" Alpha_context.Tez.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Set_deposits_limit_too_high" then
            let '{|
              Set_deposits_limit_too_high.limit := limit;
                Set_deposits_limit_too_high.max_limit := max_limit
                |} := cast Set_deposits_limit_too_high payload in
            Some (limit, max_limit)
          else None
        end)
      (fun (function_parameter : Alpha_context.Tez.t * Alpha_context.Tez.t) =>
        let '(limit, max_limit) := function_parameter in
        Build_extensible "Set_deposits_limit_too_high"
          Set_deposits_limit_too_high
          {| Set_deposits_limit_too_high.limit := limit;
            Set_deposits_limit_too_high.max_limit := max_limit |}) in
  Error_monad.register_error_kind Error_monad.Branch
    "contract.empty_transaction" "Empty transaction"
    "Forbidden to credit 0\234\156\169 to a contract without code."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (contract : Alpha_context.Contract.t) =>
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal
                "Transactions of 0\234\156\169 towards a contract without code are forbidden ("
                (CamlinternalFormatBasics.Alpha
                  (CamlinternalFormatBasics.String_literal ")."
                    CamlinternalFormatBasics.End_of_format)))
              "Transactions of 0\234\156\169 towards a contract without code are forbidden (%a).")
            Alpha_context.Contract.pp contract))
    (Data_encoding.obj1
      (Data_encoding.req None None "contract" Alpha_context.Contract.encoding))
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Empty_transaction" then
          let 'c_value := cast Alpha_context.Contract.t payload in
          Some c_value
        else None
      end)
    (fun (c_value : Alpha_context.Contract.t) =>
      Build_extensible "Empty_transaction" Alpha_context.Contract.t c_value).

Inductive denunciation_kind : Set :=
| Preendorsement : denunciation_kind
| Endorsement : denunciation_kind
| Block : denunciation_kind.

Definition denunciation_kind_encoding
  : Data_encoding.encoding denunciation_kind :=
  Data_encoding.string_enum
    [
      ("preendorsement", Preendorsement);
      ("endorsement", Endorsement);
      ("block", Block)
    ].

Definition pp_denunciation_kind
  (fmt : Format.formatter) (function_parameter : denunciation_kind) : unit :=
  match function_parameter with
  | Preendorsement =>
    Format.fprintf fmt
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.String_literal "preendorsement"
          CamlinternalFormatBasics.End_of_format) "preendorsement")
  | Endorsement =>
    Format.fprintf fmt
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.String_literal "endorsement"
          CamlinternalFormatBasics.End_of_format) "endorsement")
  | Block =>
    Format.fprintf fmt
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.String_literal "baking"
          CamlinternalFormatBasics.End_of_format) "baking")
  end.

Module Inconsistent_denunciation.
  Record record : Set := Build {
    kind : denunciation_kind;
    delegate1 : Signature.public_key_hash;
    delegate2 : Signature.public_key_hash }.
  Definition with_kind kind (r : record) :=
    Build kind r.(delegate1) r.(delegate2).
  Definition with_delegate1 delegate1 (r : record) :=
    Build r.(kind) delegate1 r.(delegate2).
  Definition with_delegate2 delegate2 (r : record) :=
    Build r.(kind) r.(delegate1) delegate2.
End Inconsistent_denunciation.
Definition Inconsistent_denunciation := Inconsistent_denunciation.record.

Module Too_early_denunciation.
  Record record : Set := Build {
    kind : denunciation_kind;
    level : Alpha_context.Raw_level.t;
    current : Alpha_context.Raw_level.t }.
  Definition with_kind kind (r : record) :=
    Build kind r.(level) r.(current).
  Definition with_level level (r : record) :=
    Build r.(kind) level r.(current).
  Definition with_current current (r : record) :=
    Build r.(kind) r.(level) current.
End Too_early_denunciation.
Definition Too_early_denunciation := Too_early_denunciation.record.

Module Outdated_denunciation.
  Record record : Set := Build {
    kind : denunciation_kind;
    level : Alpha_context.Raw_level.t;
    last_cycle : Alpha_context.Cycle.t }.
  Definition with_kind kind (r : record) :=
    Build kind r.(level) r.(last_cycle).
  Definition with_level level (r : record) :=
    Build r.(kind) level r.(last_cycle).
  Definition with_last_cycle last_cycle (r : record) :=
    Build r.(kind) r.(level) last_cycle.
End Outdated_denunciation.
Definition Outdated_denunciation := Outdated_denunciation.record.

Module Invalid_activation.
  Record record : Set := Build {
    pkh : Ed25519.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.t) }.
  Definition with_pkh pkh (r : record) :=
    Build pkh.
End Invalid_activation.
Definition Invalid_activation := Invalid_activation.record.

(** Init function; without side-effects in Coq *)
Definition init_module2 : unit :=
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "operation.wrong_voting_period" "Wrong voting period"
      "Trying to include a proposal or ballot meant for another voting period"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : int32 * int32) =>
            let '(e_value, p_value) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "Wrong voting period "
                  (CamlinternalFormatBasics.Int32 CamlinternalFormatBasics.Int_d
                    CamlinternalFormatBasics.No_padding
                    CamlinternalFormatBasics.No_precision
                    (CamlinternalFormatBasics.String_literal ", current is "
                      (CamlinternalFormatBasics.Int32
                        CamlinternalFormatBasics.Int_d
                        CamlinternalFormatBasics.No_padding
                        CamlinternalFormatBasics.No_precision
                        CamlinternalFormatBasics.End_of_format))))
                "Wrong voting period %ld, current is %ld") p_value e_value))
      (Data_encoding.obj2
        (Data_encoding.req None None "current_index" Data_encoding.int32_value)
        (Data_encoding.req None None "provided_index" Data_encoding.int32_value))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Wrong_voting_period" then
            let '(e_value, p_value) := cast (int32 * int32) payload in
            Some (e_value, p_value)
          else None
        end)
      (fun (function_parameter : int32 * int32) =>
        let '(e_value, p_value) := function_parameter in
        Build_extensible "Wrong_voting_period" (int32 * int32)
          (e_value, p_value)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "internal_operation_replay" "Internal operation replay"
      "An internal operation was emitted twice by a script"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : Alpha_context.packed_internal_operation) =>
            let
              'Alpha_context.Internal_operation {|
                Alpha_context.internal_operation.nonce := nonce_value |} :=
              function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "Internal operation "
                  (CamlinternalFormatBasics.Int CamlinternalFormatBasics.Int_d
                    CamlinternalFormatBasics.No_padding
                    CamlinternalFormatBasics.No_precision
                    (CamlinternalFormatBasics.String_literal
                      " was emitted twice by a script"
                      CamlinternalFormatBasics.End_of_format)))
                "Internal operation %d was emitted twice by a script")
              nonce_value)) Alpha_context.Operation.internal_operation_encoding
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Internal_operation_replay" then
            let 'op := cast Alpha_context.packed_internal_operation payload in
            Some op
          else None
        end)
      (fun (op : Alpha_context.packed_internal_operation) =>
        Build_extensible "Internal_operation_replay"
          Alpha_context.packed_internal_operation op) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "block.invalid_denunciation" "Invalid denunciation"
      "A denunciation is malformed"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (kind_value : denunciation_kind) =>
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "Malformed double-"
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal " evidence"
                      CamlinternalFormatBasics.End_of_format)))
                "Malformed double-%a evidence") pp_denunciation_kind kind_value))
      (Data_encoding.obj1
        (Data_encoding.req None None "kind" denunciation_kind_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_denunciation" then
            let 'kind_value := cast denunciation_kind payload in
            Some kind_value
          else None
        end)
      (fun (kind_value : denunciation_kind) =>
        Build_extensible "Invalid_denunciation" denunciation_kind kind_value) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "block.inconsistent_denunciation" "Inconsistent denunciation"
      "A denunciation operation is inconsistent (two distinct delegates)"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            denunciation_kind * Signature.public_key_hash *
              Signature.public_key_hash) =>
            let '(kind_value, delegate1, delegate2) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "Inconsistent double-"
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal
                      " evidence (distinct delegate: "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal " and "
                          (CamlinternalFormatBasics.Alpha
                            (CamlinternalFormatBasics.Char_literal ")" % char
                              CamlinternalFormatBasics.End_of_format)))))))
                "Inconsistent double-%a evidence (distinct delegate: %a and %a)")
              pp_denunciation_kind kind_value
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp_short)
              delegate1
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp_short)
              delegate2))
      (Data_encoding.obj3
        (Data_encoding.req None None "kind" denunciation_kind_encoding)
        (Data_encoding.req None None "delegate1"
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
        (Data_encoding.req None None "delegate2"
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Inconsistent_denunciation" then
            let '{|
              Inconsistent_denunciation.kind := kind_value;
                Inconsistent_denunciation.delegate1 := delegate1;
                Inconsistent_denunciation.delegate2 := delegate2
                |} := cast Inconsistent_denunciation payload in
            Some (kind_value, delegate1, delegate2)
          else None
        end)
      (fun (function_parameter :
        denunciation_kind * Signature.public_key_hash *
          Signature.public_key_hash) =>
        let '(kind_value, delegate1, delegate2) := function_parameter in
        Build_extensible "Inconsistent_denunciation" Inconsistent_denunciation
          {| Inconsistent_denunciation.kind := kind_value;
            Inconsistent_denunciation.delegate1 := delegate1;
            Inconsistent_denunciation.delegate2 := delegate2 |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Branch
      "block.unrequired_denunciation" "Unrequired denunciation"
      "A denunciation is unrequired"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "A valid denunciation cannot be applied: the associated delegate has already been denounced for this level."
                  CamlinternalFormatBasics.End_of_format)
                "A valid denunciation cannot be applied: the associated delegate has already been denounced for this level.")))
      Data_encoding.unit_value
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Unrequired_denunciation" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Unrequired_denunciation" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "block.too_early_denunciation" "Too early denunciation"
      "A denunciation is too far in the future"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            denunciation_kind * Alpha_context.Raw_level.t *
              Alpha_context.Raw_level.t) =>
            let '(kind_value, level, current) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "A double-"
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal
                      " denunciation is too far in the future (current level: "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal
                          ", given level: "
                          (CamlinternalFormatBasics.Alpha
                            (CamlinternalFormatBasics.Char_literal ")" % char
                              CamlinternalFormatBasics.End_of_format)))))))
                "A double-%a denunciation is too far in the future (current level: %a, given level: %a)")
              pp_denunciation_kind kind_value Alpha_context.Raw_level.pp current
              Alpha_context.Raw_level.pp level))
      (Data_encoding.obj3
        (Data_encoding.req None None "kind" denunciation_kind_encoding)
        (Data_encoding.req None None "level" Alpha_context.Raw_level.encoding)
        (Data_encoding.req None None "current" Alpha_context.Raw_level.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Too_early_denunciation" then
            let '{|
              Too_early_denunciation.kind := kind_value;
                Too_early_denunciation.level := level;
                Too_early_denunciation.current := current
                |} := cast Too_early_denunciation payload in
            Some (kind_value, level, current)
          else None
        end)
      (fun (function_parameter :
        denunciation_kind * Alpha_context.Raw_level.t *
          Alpha_context.Raw_level.t) =>
        let '(kind_value, level, current) := function_parameter in
        Build_extensible "Too_early_denunciation" Too_early_denunciation
          {| Too_early_denunciation.kind := kind_value;
            Too_early_denunciation.level := level;
            Too_early_denunciation.current := current |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "block.outdated_denunciation" "Outdated denunciation"
      "A denunciation is outdated."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            denunciation_kind * Alpha_context.Raw_level.t *
              Alpha_context.Cycle.t) =>
            let '(kind_value, level, last_cycle) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "A double-"
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal
                      " is outdated (last acceptable cycle: "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal
                          ", given level: "
                          (CamlinternalFormatBasics.Alpha
                            (CamlinternalFormatBasics.Char_literal ")" % char
                              CamlinternalFormatBasics.End_of_format)))))))
                "A double-%a is outdated (last acceptable cycle: %a, given level: %a)")
              pp_denunciation_kind kind_value Alpha_context.Cycle.pp last_cycle
              Alpha_context.Raw_level.pp level))
      (Data_encoding.obj3
        (Data_encoding.req None None "kind" denunciation_kind_encoding)
        (Data_encoding.req None None "level" Alpha_context.Raw_level.encoding)
        (Data_encoding.req None None "last" Alpha_context.Cycle.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Outdated_denunciation" then
            let '{|
              Outdated_denunciation.kind := kind_value;
                Outdated_denunciation.level := level;
                Outdated_denunciation.last_cycle := last_cycle
                |} := cast Outdated_denunciation payload in
            Some (kind_value, level, last_cycle)
          else None
        end)
      (fun (function_parameter :
        denunciation_kind * Alpha_context.Raw_level.t * Alpha_context.Cycle.t)
        =>
        let '(kind_value, level, last_cycle) := function_parameter in
        Build_extensible "Outdated_denunciation" Outdated_denunciation
          {| Outdated_denunciation.kind := kind_value;
            Outdated_denunciation.level := level;
            Outdated_denunciation.last_cycle := last_cycle |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "operation.invalid_activation" "Invalid activation"
      "The given key and secret do not correspond to any existing preallocated contract"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (pkh : Ed25519.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.t)) =>
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Invalid activation. The public key "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal
                      " does not match any commitment."
                      CamlinternalFormatBasics.End_of_format)))
                "Invalid activation. The public key %a does not match any commitment.")
              Ed25519.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp) pkh))
      (Data_encoding.obj1
        (Data_encoding.req None None "pkh"
          Ed25519.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_activation" then
            let '{| Invalid_activation.pkh := pkh |} :=
              cast Invalid_activation payload in
            Some pkh
          else None
        end)
      (fun (pkh : Ed25519.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.t)) =>
        Build_extensible "Invalid_activation" Invalid_activation
          {| Invalid_activation.pkh := pkh |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "block.multiple_revelation"
      "Multiple revelations were included in a manager operation"
      "A manager operation should not contain more than one revelation"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Multiple revelations were included in a manager operation"
                  CamlinternalFormatBasics.End_of_format)
                "Multiple revelations were included in a manager operation")))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Multiple_revelation" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Multiple_revelation" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "gas_exhausted.init_deserialize"
      "Not enough gas for initial deserialization of script expressions"
      "Gas limit was not high enough to deserialize the transaction parameters or origination script code or initial storage, making the operation impossible to parse within the provided gas bounds."
      None Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Gas_quota_exceeded_init_deserialize" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Gas_quota_exceeded_init_deserialize" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "operation.inconsistent_sources" "Inconsistent sources in operation pack"
      "The operation pack includes operations from different sources."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.pp_print_string ppf
              "The operation pack includes operations from different sources."))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Inconsistent_sources" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Inconsistent_sources" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "operation.failing_noop"
      "Failing_noop operation are not executed by the protocol"
      "The failing_noop operation is an operation that is not and never will be executed by the protocol."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "The failing_noop operation cannot be executed by the protocol"
                  CamlinternalFormatBasics.End_of_format)
                "The failing_noop operation cannot be executed by the protocol")))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Failing_noop_error" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Failing_noop_error" unit tt) in
  Error_monad.register_error_kind Error_monad.Permanent
    "delegate.zero_frozen_deposits" "Zero frozen deposits"
    "The delegate has zero frozen deposits."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (delegate : Signature.public_key_hash) =>
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal "Delegate "
                (CamlinternalFormatBasics.Alpha
                  (CamlinternalFormatBasics.String_literal
                    " has zero frozen deposits; it is not allowed to bake/preendorse/endorse."
                    CamlinternalFormatBasics.End_of_format)))
              "Delegate %a has zero frozen deposits; it is not allowed to bake/preendorse/endorse.")
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp) delegate))
    (Data_encoding.obj1
      (Data_encoding.req None None "delegate"
        Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Zero_frozen_deposits" then
          let 'delegate := cast Signature.public_key_hash payload in
          Some delegate
        else None
      end)
    (fun (delegate : Signature.public_key_hash) =>
      Build_extensible "Zero_frozen_deposits" Signature.public_key_hash delegate).

Definition cache_layout : list int := Constants_repr.cache_layout.

Definition apply_manager_operation_content
  (ctxt : Alpha_context.t) (mode : Script_ir_translator.unparsing_mode)
  (payer : Alpha_context.Contract.t) (source : Alpha_context.Contract.t)
  (chain_id : Chain_id.t) (internal : bool)
  (gas_consumed_in_precheck : option Alpha_context.Gas.cost)
  (operation : Alpha_context.manager_operation)
  : M?
    (Alpha_context.context * Apply_results.successful_manager_operation_result *
      list Alpha_context.packed_internal_operation) :=
  let before_operation := ctxt in
  let? '_ := Alpha_context.Contract.must_exist ctxt source in
  let? ctxt :=
    Alpha_context.Gas.consume ctxt Michelson_v1_gas.Cost_of.manager_operation in
  let? ctxt :=
    match gas_consumed_in_precheck with
    | None => Pervasives.Ok ctxt
    | Some gas => Alpha_context.Gas.consume ctxt gas
    end in
  let consume_deserialization_gas := Alpha_context.Script.When_needed in
  match operation with
  | Alpha_context.Reveal _ =>
    return?
      (ctxt,
        (Apply_results.Reveal_result
          {|
            Apply_results.successful_manager_operation_result.Reveal_result.consumed_gas
              := Alpha_context.Gas.consumed before_operation ctxt |}), nil)
  |
    Alpha_context.Transaction {|
      Alpha_context.manager_operation.Transaction.amount := amount;
        Alpha_context.manager_operation.Transaction.parameters := parameters;
        Alpha_context.manager_operation.Transaction.entrypoint := entrypoint;
        Alpha_context.manager_operation.Transaction.destination := destination
        |} =>
    let? '(parameter, ctxt) :=
      Alpha_context.Script.force_decode_in_context consume_deserialization_gas
        ctxt parameters in
    let? allocated_destination_contract :=
      match Alpha_context.Contract.is_implicit destination with
      | None =>
        let? '_ :=
          if Alpha_context.Tez.op_eq amount Alpha_context.Tez.zero then
            Alpha_context.Contract.must_exist ctxt destination
          else
            Error_monad.return_unit in
        Error_monad.return_false
      | Some _ =>
        let? '_ :=
          Error_monad.error_when
            (Alpha_context.Tez.op_eq amount Alpha_context.Tez.zero)
            (Build_extensible "Empty_transaction"
              Alpha_context.Contract.contract destination) in
        Error_monad.op_gtpipeeqquestion
          (Alpha_context.Contract.allocated ctxt destination) Pervasives.not
      end in
    let? '(ctxt, balance_updates) :=
      Alpha_context.Token.transfer None ctxt
        (Alpha_context.Token.SourceContainer
          (Alpha_context.Token.Contract source))
        (Alpha_context.Token.SinkContainer
          (Alpha_context.Token.Contract destination)) amount in
    let? '(ctxt, cache_key, script) := Script_cache.find ctxt destination in
    match script with
    | None =>
      let? ctxt :=
        let? '_ :=
          match entrypoint with
          | "default" => Result.return_unit
          | entrypoint =>
            Error_monad.error_value
              (Build_extensible "No_such_entrypoint" string entrypoint)
          end in
        match Micheline.root parameter with
        | Micheline.Prim _ Michelson_v1_primitives.D_Unit [] _ => return? ctxt
        | _ =>
          Error_monad.error_value
            (Build_extensible "Bad_contract_parameter"
              Alpha_context.Contract.contract destination)
        end in
      let result_value :=
        Apply_results.Transaction_result
          {|
            Apply_results.successful_manager_operation_result.Transaction_result.storage
              := None;
            Apply_results.successful_manager_operation_result.Transaction_result.lazy_storage_diff
              := None;
            Apply_results.successful_manager_operation_result.Transaction_result.balance_updates
              := balance_updates;
            Apply_results.successful_manager_operation_result.Transaction_result.originated_contracts
              := nil;
            Apply_results.successful_manager_operation_result.Transaction_result.consumed_gas
              := Alpha_context.Gas.consumed before_operation ctxt;
            Apply_results.successful_manager_operation_result.Transaction_result.storage_size
              := Z.zero;
            Apply_results.successful_manager_operation_result.Transaction_result.paid_storage_size_diff
              := Z.zero;
            Apply_results.successful_manager_operation_result.Transaction_result.allocated_destination_contract
              := allocated_destination_contract |} in
      return? (ctxt, result_value, nil)
    | Some (script, script_ir) =>
      let now := Alpha_context.Script_timestamp.now ctxt in
      let level :=
        Alpha_context.Script_int.abs
          (Alpha_context.Script_int.of_int32
            (Alpha_context.Raw_level.to_int32
              (Alpha_context.Level.current ctxt).(Alpha_context.Level.t.level)))
        in
      let step_constants :=
        {| Script_typed_ir.step_constants.source := source;
          Script_typed_ir.step_constants.payer := payer;
          Script_typed_ir.step_constants.self := destination;
          Script_typed_ir.step_constants.amount := amount;
          Script_typed_ir.step_constants.chain_id := chain_id;
          Script_typed_ir.step_constants.now := now;
          Script_typed_ir.step_constants.level := level |} in
      let?
        '({|
          Script_interpreter.execution_result.ctxt := ctxt;
            Script_interpreter.execution_result.storage := storage_value;
            Script_interpreter.execution_result.lazy_storage_diff :=
              lazy_storage_diff;
            Script_interpreter.execution_result.operations := operations
            |}, (updated_cached_script, updated_size)) :=
        Script_interpreter.execute None ctxt (Some script_ir) mode
          step_constants script entrypoint parameter internal in
      let? ctxt :=
        Alpha_context.Contract.update_script_storage ctxt destination
          storage_value lazy_storage_diff in
      let? '(ctxt, new_size, paid_storage_size_diff) :=
        Alpha_context.Fees.record_paid_storage_space ctxt destination in
      let? originated_contracts :=
        Alpha_context.Contract.originated_from_current_nonce before_operation
          ctxt in
      let? ctxt :=
        Script_cache.update ctxt cache_key
          ((Alpha_context.Script.t.with_storage
            (Alpha_context.Script.lazy_expr_value storage_value) script),
            updated_cached_script) updated_size in
      let result_value :=
        Apply_results.Transaction_result
          {|
            Apply_results.successful_manager_operation_result.Transaction_result.storage
              := Some storage_value;
            Apply_results.successful_manager_operation_result.Transaction_result.lazy_storage_diff
              := lazy_storage_diff;
            Apply_results.successful_manager_operation_result.Transaction_result.balance_updates
              := balance_updates;
            Apply_results.successful_manager_operation_result.Transaction_result.originated_contracts
              := originated_contracts;
            Apply_results.successful_manager_operation_result.Transaction_result.consumed_gas
              := Alpha_context.Gas.consumed before_operation ctxt;
            Apply_results.successful_manager_operation_result.Transaction_result.storage_size
              := new_size;
            Apply_results.successful_manager_operation_result.Transaction_result.paid_storage_size_diff
              := paid_storage_size_diff;
            Apply_results.successful_manager_operation_result.Transaction_result.allocated_destination_contract
              := allocated_destination_contract |} in
      return? (ctxt, result_value, operations)
    end
  |
    Alpha_context.Origination {|
      Alpha_context.manager_operation.Origination.delegate := delegate;
        Alpha_context.manager_operation.Origination.script := script;
        Alpha_context.manager_operation.Origination.credit := credit;
        Alpha_context.manager_operation.Origination.preorigination :=
          preorigination
        |} =>
    let? '(_unparsed_storage, ctxt) :=
      Alpha_context.Script.force_decode_in_context consume_deserialization_gas
        ctxt script.(Alpha_context.Script.t.storage) in
    let? '(unparsed_code, ctxt) :=
      Alpha_context.Script.force_decode_in_context consume_deserialization_gas
        ctxt script.(Alpha_context.Script.t.code) in
    let? '(Script_ir_translator.Ex_script parsed_script, ctxt) :=
      Script_ir_translator.parse_script None ctxt false internal script in
    let views_result :=
      Script_ir_translator.typecheck_views None ctxt false
        parsed_script.(Script_typed_ir.script.storage_type)
        parsed_script.(Script_typed_ir.script.views) in
    let? ctxt :=
      Error_monad.trace_value
        (Build_extensible "Ill_typed_contract"
          (Alpha_context.Script.expr * Script_tc_errors.type_map)
          (unparsed_code, nil)) views_result in
    let? '(to_duplicate, ctxt) :=
      Script_ir_translator.collect_lazy_storage ctxt
        parsed_script.(Script_typed_ir.script.storage_type)
        parsed_script.(Script_typed_ir.script.storage) in
    let to_update := Script_ir_translator.no_lazy_storage_id in
    let? '(storage_value, lazy_storage_diff, ctxt) :=
      Script_ir_translator.extract_lazy_storage_diff ctxt
        Script_ir_translator.Optimized false to_duplicate to_update
        parsed_script.(Script_typed_ir.script.storage_type)
        parsed_script.(Script_typed_ir.script.storage) in
    let? '(storage_value, ctxt) :=
      Script_ir_translator.unparse_data ctxt Script_ir_translator.Optimized
        parsed_script.(Script_typed_ir.script.storage_type) storage_value in
    let storage_value :=
      Alpha_context.Script.lazy_expr_value
        (Micheline.strip_locations storage_value) in
    let script := Alpha_context.Script.t.with_storage storage_value script in
    let? '(ctxt, contract) :=
      match preorigination with
      | Some contract =>
        let '_ :=
          (* ❌ Assert instruction is not handled. *)
          assert unit internal in
        return? (ctxt, contract)
      | None => Alpha_context.Contract.fresh_contract_from_current_nonce ctxt
      end in
    let? ctxt :=
      Alpha_context.Contract.raw_originate ctxt false contract
        (script, lazy_storage_diff) in
    let? ctxt :=
      match delegate with
      | None => return? ctxt
      | Some delegate =>
        Alpha_context.Delegate.init_value ctxt contract delegate
      end in
    let? '(ctxt, balance_updates) :=
      Alpha_context.Token.transfer None ctxt
        (Alpha_context.Token.SourceContainer
          (Alpha_context.Token.Contract source))
        (Alpha_context.Token.SinkContainer
          (Alpha_context.Token.Contract contract)) credit in
    let? '(ctxt, size_value, paid_storage_size_diff) :=
      Alpha_context.Fees.record_paid_storage_space ctxt contract in
    let result_value :=
      Apply_results.Origination_result
        {|
          Apply_results.successful_manager_operation_result.Origination_result.lazy_storage_diff
            := lazy_storage_diff;
          Apply_results.successful_manager_operation_result.Origination_result.balance_updates
            := balance_updates;
          Apply_results.successful_manager_operation_result.Origination_result.originated_contracts
            := [ contract ];
          Apply_results.successful_manager_operation_result.Origination_result.consumed_gas
            := Alpha_context.Gas.consumed before_operation ctxt;
          Apply_results.successful_manager_operation_result.Origination_result.storage_size
            := size_value;
          Apply_results.successful_manager_operation_result.Origination_result.paid_storage_size_diff
            := paid_storage_size_diff |} in
    return? (ctxt, result_value, nil)
  | Alpha_context.Delegation delegate =>
    let? ctxt := Alpha_context.Delegate.set ctxt source delegate in
    return?
      (ctxt,
        (Apply_results.Delegation_result
          {|
            Apply_results.successful_manager_operation_result.Delegation_result.consumed_gas
              := Alpha_context.Gas.consumed before_operation ctxt |}), nil)
  |
    Alpha_context.Register_global_constant {|
      Alpha_context.manager_operation.Register_global_constant.value := value_value
        |} =>
    let? '(expr, ctxt) :=
      Alpha_context.Script.force_decode_in_context consume_deserialization_gas
        ctxt value_value in
    let? '(ctxt, address, size_value) :=
      Alpha_context.Global_constants_storage.register ctxt expr in
    let '(ctxt, paid_size) :=
      Alpha_context.Fees.record_global_constant_storage_space ctxt size_value in
    let result_value :=
      Apply_results.Register_global_constant_result
        {|
          Apply_results.successful_manager_operation_result.Register_global_constant_result.balance_updates
            := nil;
          Apply_results.successful_manager_operation_result.Register_global_constant_result.consumed_gas
            := Alpha_context.Gas.consumed before_operation ctxt;
          Apply_results.successful_manager_operation_result.Register_global_constant_result.size_of_constant
            := paid_size;
          Apply_results.successful_manager_operation_result.Register_global_constant_result.global_address
            := address |} in
    return? (ctxt, result_value, nil)
  | Alpha_context.Set_deposits_limit limit =>
    let? '_ :=
      match limit with
      | None => Error_monad.return_unit
      | Some limit =>
        let frozen_deposits_percentage :=
          Alpha_context.Constants.frozen_deposits_percentage ctxt in
        let max_limit :=
          Alpha_context.Tez.of_mutez_exn
            ((Int64.of_int frozen_deposits_percentage) *i64
            (Int64.max_int /i64 100)) in
        Error_monad.fail_when (Alpha_context.Tez.op_gt limit max_limit)
          (Build_extensible "Set_deposits_limit_too_high"
            Set_deposits_limit_too_high
            {| Set_deposits_limit_too_high.limit := limit;
              Set_deposits_limit_too_high.max_limit := max_limit |})
      end in
    (fun (function_parameter : option Alpha_context.public_key_hash) =>
      match function_parameter with
      | None =>
        Error_monad.fail
          (Build_extensible "Set_deposits_limit_on_originated_contract" unit tt)
      | Some delegate =>
        let? is_registered := Alpha_context.Delegate.registered ctxt delegate in
        let? '_ :=
          Error_monad.fail_unless is_registered
            (Build_extensible "Set_deposits_limit_on_unregistered_delegate"
              Alpha_context.public_key_hash delegate) in
        let ctxt :=
          Alpha_context.Delegate.set_frozen_deposits_limit ctxt delegate limit
          in
        return?
          (ctxt,
            (Apply_results.Set_deposits_limit_result
              {|
                Apply_results.successful_manager_operation_result.Set_deposits_limit_result.consumed_gas
                  := Alpha_context.Gas.consumed before_operation ctxt |}), nil)
      end) (Alpha_context.Contract.is_implicit source)
  end.

Inductive success_or_failure : Set :=
| Success : Alpha_context.context -> success_or_failure
| Failure : success_or_failure.

Definition apply_internal_manager_operations
  (ctxt : Alpha_context.context) (mode : Script_ir_translator.unparsing_mode)
  (payer : Alpha_context.Contract.t) (chain_id : Chain_id.t)
  (ops : list Alpha_context.packed_internal_operation)
  : success_or_failure * list Apply_results.packed_internal_operation_result :=
  let fix apply
    (ctxt : Alpha_context.context)
    (applied : list Apply_results.packed_internal_operation_result)
    (worklist : list Alpha_context.packed_internal_operation) {struct ctxt}
    : success_or_failure * list Apply_results.packed_internal_operation_result :=
    match worklist with
    | [] => ((Success ctxt), (List.rev applied))
    |
      cons
        (Alpha_context.Internal_operation
          ({|
            Alpha_context.internal_operation.source := source;
              Alpha_context.internal_operation.operation := operation;
              Alpha_context.internal_operation.nonce := nonce_value
              |} as op)) rest =>
      let function_parameter :=
        if Alpha_context.internal_nonce_already_recorded ctxt nonce_value then
          Error_monad.fail
            (Build_extensible "Internal_operation_replay"
              Alpha_context.packed_internal_operation
              (Alpha_context.Internal_operation op))
        else
          let ctxt := Alpha_context.record_internal_nonce ctxt nonce_value in
          apply_manager_operation_content ctxt mode payer source chain_id true
            None operation in
      match function_parameter with
      | Pervasives.Error errors =>
        let result_value :=
          Apply_results.Internal_operation_result op
            (Apply_results.Failed
              (Alpha_context.manager_kind
                op.(Alpha_context.internal_operation.operation)) errors) in
        let skipped :=
          List.rev_map
            (fun (function_parameter : Alpha_context.packed_internal_operation)
              =>
              let 'Alpha_context.Internal_operation op := function_parameter in
              Apply_results.Internal_operation_result op
                (Apply_results.Skipped
                  (Alpha_context.manager_kind
                    op.(Alpha_context.internal_operation.operation)))) rest in
        (Failure,
          (List.rev (Pervasives.op_at skipped (cons result_value applied))))
      | Pervasives.Ok (ctxt, result_value, emitted) =>
        apply ctxt
          (cons
            (Apply_results.Internal_operation_result op
              (Apply_results.Applied result_value)) applied)
          (Pervasives.op_at emitted rest)
      end
    end in
  apply ctxt nil ops.

Definition precheck_manager_contents
  (ctxt : Alpha_context.context) (op : Alpha_context.contents)
  (only_batch : bool)
  : M? (Alpha_context.context * Apply_results.precheck_result) :=
  match op with
  |
    Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.source := source;
        Alpha_context.contents.Manager_operation.fee := fee;
        Alpha_context.contents.Manager_operation.counter := counter;
        Alpha_context.contents.Manager_operation.operation := operation;
        Alpha_context.contents.Manager_operation.gas_limit := gas_limit;
        Alpha_context.contents.Manager_operation.storage_limit := storage_limit
        |} =>
    let? ctxt :=
      (if only_batch then
        Error_monad.record_trace (Build_extensible "Gas_limit_too_high" unit tt)
      else
        fun (errs : M? Alpha_context.context) => errs)
        (Alpha_context.Gas.consume_limit_in_block ctxt gas_limit) in
    let ctxt := Alpha_context.Gas.set_limit ctxt gas_limit in
    let ctxt_before := ctxt in
    let? '_ := Alpha_context.Fees.check_storage_limit ctxt storage_limit in
    let source_contract := Alpha_context.Contract.implicit_contract source in
    let? '_ := Alpha_context.Contract.must_be_allocated ctxt source_contract in
    let? '_ :=
      Alpha_context.Contract.check_counter_increment ctxt source counter in
    let consume_deserialization_gas := Alpha_context.Script.Always in
    let? ctxt :=
      match operation with
      | Alpha_context.Reveal pk =>
        Alpha_context.Contract.reveal_manager_key ctxt source pk
      |
        Alpha_context.Transaction {|
          Alpha_context.manager_operation.Transaction.parameters := parameters
            |} =>
        Error_monad.record_trace
          (Build_extensible "Gas_quota_exceeded_init_deserialize" unit tt)
          (let? '(_arg, ctxt) :=
            Alpha_context.Script.force_decode_in_context
              consume_deserialization_gas ctxt parameters in
          return? ctxt)
      |
        Alpha_context.Origination {|
          Alpha_context.manager_operation.Origination.script := script |} =>
        Error_monad.record_trace
          (Build_extensible "Gas_quota_exceeded_init_deserialize" unit tt)
          (let? '(_code, ctxt) :=
            Alpha_context.Script.force_decode_in_context
              consume_deserialization_gas ctxt
              script.(Alpha_context.Script.t.code) in
          let? '(_storage, ctxt) :=
            Alpha_context.Script.force_decode_in_context
              consume_deserialization_gas ctxt
              script.(Alpha_context.Script.t.storage) in
          return? ctxt)
      |
        Alpha_context.Register_global_constant {|
          Alpha_context.manager_operation.Register_global_constant.value := value_value
            |} =>
        Error_monad.record_trace
          (Build_extensible "Gas_quota_exceeded_init_deserialize" unit tt)
          (let? '(_value, ctxt) :=
            Alpha_context.Script.force_decode_in_context
              consume_deserialization_gas ctxt value_value in
          return? ctxt)
      | _ => return? ctxt
      end in
    let? ctxt := Alpha_context.Contract.increment_counter ctxt source in
    let? '(ctxt, balance_updates) :=
      Alpha_context.Token.transfer None ctxt
        (Alpha_context.Token.SourceContainer
          (Alpha_context.Token.Contract source_contract))
        (Alpha_context.Token.SinkContainer Alpha_context.Token.Block_fees) fee
      in
    let consumed_gas := Alpha_context.Gas.consumed ctxt_before ctxt in
    return?
      (ctxt,
        {| Apply_results.precheck_result.consumed_gas := consumed_gas;
          Apply_results.precheck_result.balance_updates := balance_updates |})
  | _ => unreachable_gadt_branch
  end.

(** [burn_storage_fees ctxt smopr storage_limit payer] burns the storage fees
    associated to the transaction or origination result [smopr].
    Returns an updated context, an updated storage limit with the space consumed
    by the operation subtracted, and [smopr] with the relevant balance updates
    included. *)
Definition burn_storage_fees
  (ctxt : Alpha_context.context)
  (smopr : Apply_results.successful_manager_operation_result)
  (storage_limit : Z.t) (payer : Alpha_context.Contract.t)
  : M?
    (Alpha_context.context * Z.t *
      Apply_results.successful_manager_operation_result) :=
  match smopr with
  | Apply_results.Transaction_result payload =>
    let consumed :=
      payload.(Apply_results.successful_manager_operation_result.Transaction_result.paid_storage_size_diff)
      in
    let payer :=
      Alpha_context.Token.SourceContainer (Alpha_context.Token.Contract payer)
      in
    let? '(ctxt, storage_limit, storage_bus) :=
      Alpha_context.Fees.burn_storage_fees None ctxt storage_limit payer
        consumed in
    let? '(ctxt, storage_limit, origination_bus) :=
      if
        payload.(Apply_results.successful_manager_operation_result.Transaction_result.allocated_destination_contract)
      then
        Alpha_context.Fees.burn_origination_fees None ctxt storage_limit payer
      else
        return? (ctxt, storage_limit, nil) in
    let balance_updates :=
      Pervasives.op_at storage_bus
        (Pervasives.op_at
          payload.(Apply_results.successful_manager_operation_result.Transaction_result.balance_updates)
          origination_bus) in
    return?
      (ctxt, storage_limit,
        (Apply_results.Transaction_result
          {|
            Apply_results.successful_manager_operation_result.Transaction_result.storage
              :=
              payload.(Apply_results.successful_manager_operation_result.Transaction_result.storage);
            Apply_results.successful_manager_operation_result.Transaction_result.lazy_storage_diff
              :=
              payload.(Apply_results.successful_manager_operation_result.Transaction_result.lazy_storage_diff);
            Apply_results.successful_manager_operation_result.Transaction_result.balance_updates
              := balance_updates;
            Apply_results.successful_manager_operation_result.Transaction_result.originated_contracts
              :=
              payload.(Apply_results.successful_manager_operation_result.Transaction_result.originated_contracts);
            Apply_results.successful_manager_operation_result.Transaction_result.consumed_gas
              :=
              payload.(Apply_results.successful_manager_operation_result.Transaction_result.consumed_gas);
            Apply_results.successful_manager_operation_result.Transaction_result.storage_size
              :=
              payload.(Apply_results.successful_manager_operation_result.Transaction_result.storage_size);
            Apply_results.successful_manager_operation_result.Transaction_result.paid_storage_size_diff
              :=
              payload.(Apply_results.successful_manager_operation_result.Transaction_result.paid_storage_size_diff);
            Apply_results.successful_manager_operation_result.Transaction_result.allocated_destination_contract
              :=
              payload.(Apply_results.successful_manager_operation_result.Transaction_result.allocated_destination_contract)
            |}))
  | Apply_results.Origination_result payload =>
    let consumed :=
      payload.(Apply_results.successful_manager_operation_result.Origination_result.paid_storage_size_diff)
      in
    let payer :=
      Alpha_context.Token.SourceContainer (Alpha_context.Token.Contract payer)
      in
    let? '(ctxt, storage_limit, storage_bus) :=
      Alpha_context.Fees.burn_storage_fees None ctxt storage_limit payer
        consumed in
    let? '(ctxt, storage_limit, origination_bus) :=
      Alpha_context.Fees.burn_origination_fees None ctxt storage_limit payer in
    let balance_updates :=
      Pervasives.op_at storage_bus
        (Pervasives.op_at origination_bus
          payload.(Apply_results.successful_manager_operation_result.Origination_result.balance_updates))
      in
    return?
      (ctxt, storage_limit,
        (Apply_results.Origination_result
          {|
            Apply_results.successful_manager_operation_result.Origination_result.lazy_storage_diff
              :=
              payload.(Apply_results.successful_manager_operation_result.Origination_result.lazy_storage_diff);
            Apply_results.successful_manager_operation_result.Origination_result.balance_updates
              := balance_updates;
            Apply_results.successful_manager_operation_result.Origination_result.originated_contracts
              :=
              payload.(Apply_results.successful_manager_operation_result.Origination_result.originated_contracts);
            Apply_results.successful_manager_operation_result.Origination_result.consumed_gas
              :=
              payload.(Apply_results.successful_manager_operation_result.Origination_result.consumed_gas);
            Apply_results.successful_manager_operation_result.Origination_result.storage_size
              :=
              payload.(Apply_results.successful_manager_operation_result.Origination_result.storage_size);
            Apply_results.successful_manager_operation_result.Origination_result.paid_storage_size_diff
              :=
              payload.(Apply_results.successful_manager_operation_result.Origination_result.paid_storage_size_diff)
            |}))
  | (Apply_results.Reveal_result _ | Apply_results.Delegation_result _) =>
    return? (ctxt, storage_limit, smopr)
  | Apply_results.Register_global_constant_result payload =>
    let consumed :=
      payload.(Apply_results.successful_manager_operation_result.Register_global_constant_result.size_of_constant)
      in
    let payer :=
      Alpha_context.Token.SourceContainer (Alpha_context.Token.Contract payer)
      in
    let? '(ctxt, storage_limit, storage_bus) :=
      Alpha_context.Fees.burn_storage_fees None ctxt storage_limit payer
        consumed in
    let balance_updates :=
      Pervasives.op_at storage_bus
        payload.(Apply_results.successful_manager_operation_result.Register_global_constant_result.balance_updates)
      in
    return?
      (ctxt, storage_limit,
        (Apply_results.Register_global_constant_result
          {|
            Apply_results.successful_manager_operation_result.Register_global_constant_result.balance_updates
              := balance_updates;
            Apply_results.successful_manager_operation_result.Register_global_constant_result.consumed_gas
              :=
              payload.(Apply_results.successful_manager_operation_result.Register_global_constant_result.consumed_gas);
            Apply_results.successful_manager_operation_result.Register_global_constant_result.size_of_constant
              :=
              payload.(Apply_results.successful_manager_operation_result.Register_global_constant_result.size_of_constant);
            Apply_results.successful_manager_operation_result.Register_global_constant_result.global_address
              :=
              payload.(Apply_results.successful_manager_operation_result.Register_global_constant_result.global_address)
            |}))
  | Apply_results.Set_deposits_limit_result _ =>
    return? (ctxt, storage_limit, smopr)
  end.

Definition apply_manager_contents
  (ctxt : Alpha_context.context) (mode : Script_ir_translator.unparsing_mode)
  (chain_id : Chain_id.t)
  (gas_consumed_in_precheck : option Alpha_context.Gas.cost)
  (op : Alpha_context.contents)
  : success_or_failure * Apply_results.manager_operation_result *
    list Apply_results.packed_internal_operation_result :=
  match op with
  |
    Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.source := source;
        Alpha_context.contents.Manager_operation.operation := operation;
        Alpha_context.contents.Manager_operation.gas_limit := gas_limit;
        Alpha_context.contents.Manager_operation.storage_limit := storage_limit
        |} =>
    let ctxt := Alpha_context.Gas.set_limit ctxt gas_limit in
    let source := Alpha_context.Contract.implicit_contract source in
    let function_parameter :=
      apply_manager_operation_content ctxt mode source source chain_id false
        gas_consumed_in_precheck operation in
    match function_parameter with
    | Pervasives.Ok (ctxt, operation_results, internal_operations) =>
      let function_parameter :=
        apply_internal_manager_operations ctxt mode source chain_id
          internal_operations in
      match function_parameter with
      | (Success ctxt, internal_operations_results) =>
        let function_parameter :=
          burn_storage_fees ctxt operation_results storage_limit source in
        match function_parameter with
        | Pervasives.Ok (ctxt, storage_limit, operation_results) =>
          let function_parameter :=
            List.fold_left_es
              (fun (function_parameter :
                Alpha_context.context * Z.t *
                  list Apply_results.packed_internal_operation_result) =>
                let '(ctxt, storage_limit, res) := function_parameter in
                fun (iopr : Apply_results.packed_internal_operation_result) =>
                  let 'Apply_results.Internal_operation_result op mopr := iopr
                    in
                  match mopr with
                  | Apply_results.Applied smopr =>
                    let? '(ctxt, storage_limit, smopr) :=
                      burn_storage_fees ctxt smopr storage_limit source in
                    let iopr :=
                      Apply_results.Internal_operation_result op
                        (Apply_results.Applied smopr) in
                    return? (ctxt, storage_limit, (cons iopr res))
                  | _ => return? (ctxt, storage_limit, (cons iopr res))
                  end) (ctxt, storage_limit, nil) internal_operations_results in
          match function_parameter with
          | Pervasives.Ok (ctxt, _, internal_operations_results) =>
            ((Success ctxt), (Apply_results.Applied operation_results),
              (List.rev internal_operations_results))
          | Pervasives.Error errors =>
            (Failure,
              (Apply_results.Backtracked operation_results (Some errors)),
              internal_operations_results)
          end
        | Pervasives.Error errors =>
          (Failure, (Apply_results.Backtracked operation_results (Some errors)),
            internal_operations_results)
        end
      | (Failure, internal_operations_results) =>
        (Failure, (Apply_results.Applied operation_results),
          internal_operations_results)
      end
    | Pervasives.Error errors =>
      (Failure,
        (Apply_results.Failed (Alpha_context.manager_kind operation) errors),
        nil)
    end
  | _ => unreachable_gadt_branch
  end.

Definition skipped_operation_result
  (operation : Alpha_context.manager_operation)
  : Apply_results.manager_operation_result :=
  match operation with
  | Alpha_context.Reveal _ =>
    Apply_results.Applied
      (Apply_results.Reveal_result
        {|
          Apply_results.successful_manager_operation_result.Reveal_result.consumed_gas
            := Alpha_context.Gas.Arith.zero |})
  | _ => Apply_results.Skipped (Alpha_context.manager_kind operation)
  end.

Axiom mark_skipped :
  Signature.public_key_hash -> Alpha_context.Level.t ->
  Apply_results.prechecked_contents_list -> Apply_results.contents_result_list.

(** Returns an updated context, and a list of prechecked contents containing
    balance updates for fees related to each manager operation in
    [contents_list]. *)
Definition precheck_manager_contents_list
  (ctxt : Alpha_context.context) (contents_list : Alpha_context.contents_list)
  (mempool_mode : bool)
  : M? (Alpha_context.context * Apply_results.prechecked_contents_list) :=
  let fix rec_precheck_manager_contents_list
    (ctxt : Alpha_context.t) (contents_list : Alpha_context.contents_list)
    : M? (Alpha_context.context * Apply_results.prechecked_contents_list) :=
    match contents_list with
    | Alpha_context.Single contents =>
      let? '(ctxt, result_value) :=
        precheck_manager_contents ctxt contents mempool_mode in
      return?
        (ctxt,
          (Apply_results.PrecheckedSingle
            {| Apply_results.prechecked_contents.contents := contents;
              Apply_results.prechecked_contents.result := result_value |}))
    | Alpha_context.Cons contents rest =>
      let? '(ctxt, result_value) :=
        precheck_manager_contents ctxt contents mempool_mode in
      let? '(ctxt, results_rest) := rec_precheck_manager_contents_list ctxt rest
        in
      return?
        (ctxt,
          (Apply_results.PrecheckedCons
            {| Apply_results.prechecked_contents.contents := contents;
              Apply_results.prechecked_contents.result := result_value |}
            results_rest))
    end in
  let ctxt :=
    if mempool_mode then
      Alpha_context.Gas.reset_block_gas ctxt
    else
      ctxt in
  rec_precheck_manager_contents_list ctxt contents_list.

Definition check_manager_signature
  (ctxt : Alpha_context.context) (chain_id : Chain_id.t)
  (op : Alpha_context.contents_list) (raw_operation : Alpha_context.operation)
  : M? unit :=
  let check_same_manager {A : Set}
    (function_parameter : Signature.public_key_hash * option A)
    : option (Signature.public_key_hash * option A) ->
    M? (Signature.public_key_hash * option A) :=
    let '(source, source_key) := function_parameter in
    fun (manager : option (Signature.public_key_hash * option A)) =>
      match manager with
      | None => return? (source, source_key)
      | Some (manager, manager_key) =>
        if
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.equal) source
            manager
        then
          return? (source, (Option.either manager_key source_key))
        else
          Error_monad.error_value
            (Build_extensible "Inconsistent_sources" unit tt)
      end in
  let fix find_source
    (contents_list : Alpha_context.contents_list)
    (manager : option (Signature.public_key_hash * option Signature.public_key))
    : M? (Signature.public_key_hash * option Signature.public_key) :=
    let source (function_parameter : Alpha_context.contents)
      : Signature.public_key_hash * option Signature.public_key :=
      match function_parameter with
      |
        Alpha_context.Manager_operation {|
          Alpha_context.contents.Manager_operation.source := source;
            Alpha_context.contents.Manager_operation.operation :=
              Alpha_context.Reveal key_value
            |} => (source, (Some key_value))
      |
        Alpha_context.Manager_operation {|
          Alpha_context.contents.Manager_operation.source := source |} =>
        (source, None)
      | _ => unreachable_gadt_branch
      end in
    match contents_list with
    | Alpha_context.Single op => check_same_manager (source op) manager
    | Alpha_context.Cons op rest =>
      let? manager := check_same_manager (source op) manager in
      find_source rest (Some manager)
    end in
  let? '(source, source_key) := find_source op None in
  let? public_key :=
    match source_key with
    | Some key_value => return? key_value
    | None => Alpha_context.Contract.get_manager_key None ctxt source
    end in
  Alpha_context.Operation.check_signature public_key chain_id raw_operation.

Axiom apply_manager_contents_list_rec :
  Alpha_context.t -> Script_ir_translator.unparsing_mode ->
  Alpha_context.public_key_hash -> Chain_id.t ->
  Apply_results.prechecked_contents_list ->
  success_or_failure * Apply_results.contents_result_list.

Axiom mark_backtracked :
  Apply_results.contents_result_list -> Apply_results.contents_result_list.

(** Records for the constructor parameters *)
Module ConstructorRecords_apply_mode.
  Module apply_mode.
    Module Application.
      Record record {predecessor_block payload_hash locked_round
        predecessor_level predecessor_round round : Set} : Set := Build {
        predecessor_block : predecessor_block;
        payload_hash : payload_hash;
        locked_round : locked_round;
        predecessor_level : predecessor_level;
        predecessor_round : predecessor_round;
        round : round }.
      Arguments record : clear implicits.
      Definition with_predecessor_block
        {t_predecessor_block t_payload_hash t_locked_round t_predecessor_level
          t_predecessor_round t_round} predecessor_block
        (r :
          record t_predecessor_block t_payload_hash t_locked_round
            t_predecessor_level t_predecessor_round t_round) :=
        Build t_predecessor_block t_payload_hash t_locked_round
          t_predecessor_level t_predecessor_round t_round predecessor_block
          r.(payload_hash) r.(locked_round) r.(predecessor_level)
          r.(predecessor_round) r.(round).
      Definition with_payload_hash
        {t_predecessor_block t_payload_hash t_locked_round t_predecessor_level
          t_predecessor_round t_round} payload_hash
        (r :
          record t_predecessor_block t_payload_hash t_locked_round
            t_predecessor_level t_predecessor_round t_round) :=
        Build t_predecessor_block t_payload_hash t_locked_round
          t_predecessor_level t_predecessor_round t_round r.(predecessor_block)
          payload_hash r.(locked_round) r.(predecessor_level)
          r.(predecessor_round) r.(round).
      Definition with_locked_round
        {t_predecessor_block t_payload_hash t_locked_round t_predecessor_level
          t_predecessor_round t_round} locked_round
        (r :
          record t_predecessor_block t_payload_hash t_locked_round
            t_predecessor_level t_predecessor_round t_round) :=
        Build t_predecessor_block t_payload_hash t_locked_round
          t_predecessor_level t_predecessor_round t_round r.(predecessor_block)
          r.(payload_hash) locked_round r.(predecessor_level)
          r.(predecessor_round) r.(round).
      Definition with_predecessor_level
        {t_predecessor_block t_payload_hash t_locked_round t_predecessor_level
          t_predecessor_round t_round} predecessor_level
        (r :
          record t_predecessor_block t_payload_hash t_locked_round
            t_predecessor_level t_predecessor_round t_round) :=
        Build t_predecessor_block t_payload_hash t_locked_round
          t_predecessor_level t_predecessor_round t_round r.(predecessor_block)
          r.(payload_hash) r.(locked_round) predecessor_level
          r.(predecessor_round) r.(round).
      Definition with_predecessor_round
        {t_predecessor_block t_payload_hash t_locked_round t_predecessor_level
          t_predecessor_round t_round} predecessor_round
        (r :
          record t_predecessor_block t_payload_hash t_locked_round
            t_predecessor_level t_predecessor_round t_round) :=
        Build t_predecessor_block t_payload_hash t_locked_round
          t_predecessor_level t_predecessor_round t_round r.(predecessor_block)
          r.(payload_hash) r.(locked_round) r.(predecessor_level)
          predecessor_round r.(round).
      Definition with_round
        {t_predecessor_block t_payload_hash t_locked_round t_predecessor_level
          t_predecessor_round t_round} round
        (r :
          record t_predecessor_block t_payload_hash t_locked_round
            t_predecessor_level t_predecessor_round t_round) :=
        Build t_predecessor_block t_payload_hash t_locked_round
          t_predecessor_level t_predecessor_round t_round r.(predecessor_block)
          r.(payload_hash) r.(locked_round) r.(predecessor_level)
          r.(predecessor_round) round.
    End Application.
    Definition Application_skeleton := Application.record.
    
    Module Full_construction.
      Record record {predecessor_block payload_hash predecessor_level
        predecessor_round round : Set} : Set := Build {
        predecessor_block : predecessor_block;
        payload_hash : payload_hash;
        predecessor_level : predecessor_level;
        predecessor_round : predecessor_round;
        round : round }.
      Arguments record : clear implicits.
      Definition with_predecessor_block
        {t_predecessor_block t_payload_hash t_predecessor_level
          t_predecessor_round t_round} predecessor_block
        (r :
          record t_predecessor_block t_payload_hash t_predecessor_level
            t_predecessor_round t_round) :=
        Build t_predecessor_block t_payload_hash t_predecessor_level
          t_predecessor_round t_round predecessor_block r.(payload_hash)
          r.(predecessor_level) r.(predecessor_round) r.(round).
      Definition with_payload_hash
        {t_predecessor_block t_payload_hash t_predecessor_level
          t_predecessor_round t_round} payload_hash
        (r :
          record t_predecessor_block t_payload_hash t_predecessor_level
            t_predecessor_round t_round) :=
        Build t_predecessor_block t_payload_hash t_predecessor_level
          t_predecessor_round t_round r.(predecessor_block) payload_hash
          r.(predecessor_level) r.(predecessor_round) r.(round).
      Definition with_predecessor_level
        {t_predecessor_block t_payload_hash t_predecessor_level
          t_predecessor_round t_round} predecessor_level
        (r :
          record t_predecessor_block t_payload_hash t_predecessor_level
            t_predecessor_round t_round) :=
        Build t_predecessor_block t_payload_hash t_predecessor_level
          t_predecessor_round t_round r.(predecessor_block) r.(payload_hash)
          predecessor_level r.(predecessor_round) r.(round).
      Definition with_predecessor_round
        {t_predecessor_block t_payload_hash t_predecessor_level
          t_predecessor_round t_round} predecessor_round
        (r :
          record t_predecessor_block t_payload_hash t_predecessor_level
            t_predecessor_round t_round) :=
        Build t_predecessor_block t_payload_hash t_predecessor_level
          t_predecessor_round t_round r.(predecessor_block) r.(payload_hash)
          r.(predecessor_level) predecessor_round r.(round).
      Definition with_round
        {t_predecessor_block t_payload_hash t_predecessor_level
          t_predecessor_round t_round} round
        (r :
          record t_predecessor_block t_payload_hash t_predecessor_level
            t_predecessor_round t_round) :=
        Build t_predecessor_block t_payload_hash t_predecessor_level
          t_predecessor_round t_round r.(predecessor_block) r.(payload_hash)
          r.(predecessor_level) r.(predecessor_round) round.
    End Full_construction.
    Definition Full_construction_skeleton := Full_construction.record.
    
    Module Partial_construction.
      Record record {predecessor_level predecessor_round grand_parent_round :
        Set} : Set := Build {
        predecessor_level : predecessor_level;
        predecessor_round : predecessor_round;
        grand_parent_round : grand_parent_round }.
      Arguments record : clear implicits.
      Definition with_predecessor_level
        {t_predecessor_level t_predecessor_round t_grand_parent_round}
        predecessor_level
        (r :
          record t_predecessor_level t_predecessor_round t_grand_parent_round) :=
        Build t_predecessor_level t_predecessor_round t_grand_parent_round
          predecessor_level r.(predecessor_round) r.(grand_parent_round).
      Definition with_predecessor_round
        {t_predecessor_level t_predecessor_round t_grand_parent_round}
        predecessor_round
        (r :
          record t_predecessor_level t_predecessor_round t_grand_parent_round) :=
        Build t_predecessor_level t_predecessor_round t_grand_parent_round
          r.(predecessor_level) predecessor_round r.(grand_parent_round).
      Definition with_grand_parent_round
        {t_predecessor_level t_predecessor_round t_grand_parent_round}
        grand_parent_round
        (r :
          record t_predecessor_level t_predecessor_round t_grand_parent_round) :=
        Build t_predecessor_level t_predecessor_round t_grand_parent_round
          r.(predecessor_level) r.(predecessor_round) grand_parent_round.
    End Partial_construction.
    Definition Partial_construction_skeleton := Partial_construction.record.
  End apply_mode.
End ConstructorRecords_apply_mode.
Import ConstructorRecords_apply_mode.

Reserved Notation "'apply_mode.Application".
Reserved Notation "'apply_mode.Full_construction".
Reserved Notation "'apply_mode.Partial_construction".

Inductive apply_mode : Set :=
| Application : 'apply_mode.Application -> apply_mode
| Full_construction : 'apply_mode.Full_construction -> apply_mode
| Partial_construction : 'apply_mode.Partial_construction -> apply_mode

where "'apply_mode.Application" :=
  (apply_mode.Application_skeleton Block_hash.t Block_payload_hash.t
    (option Alpha_context.Round.t) Alpha_context.Level.t Alpha_context.Round.t
    Alpha_context.Round.t)
and "'apply_mode.Full_construction" :=
  (apply_mode.Full_construction_skeleton Block_hash.t Block_payload_hash.t
    Alpha_context.Level.t Alpha_context.Round.t Alpha_context.Round.t)
and "'apply_mode.Partial_construction" :=
  (apply_mode.Partial_construction_skeleton Alpha_context.Level.t
    Alpha_context.Round.t Alpha_context.Round.t).

Module apply_mode.
  Include ConstructorRecords_apply_mode.apply_mode.
  Definition Application := 'apply_mode.Application.
  Definition Full_construction := 'apply_mode.Full_construction.
  Definition Partial_construction := 'apply_mode.Partial_construction.
End apply_mode.

Definition get_predecessor_level (function_parameter : apply_mode)
  : Alpha_context.Level.t :=
  match function_parameter with
  |
    (Application {|
      apply_mode.Application.predecessor_level := predecessor_level |} |
    Full_construction {|
      apply_mode.Full_construction.predecessor_level := predecessor_level |}
    |
    Partial_construction {|
      apply_mode.Partial_construction.predecessor_level := predecessor_level
        |}) => predecessor_level
  end.

Definition record_operation
  (ctxt : Alpha_context.context) (operation : Alpha_context.operation)
  : Alpha_context.context :=
  match
    operation.(Alpha_context.operation.protocol_data).(Alpha_context.protocol_data.contents)
    with
  | Alpha_context.Single (Alpha_context.Preendorsement _) => ctxt
  | Alpha_context.Single (Alpha_context.Endorsement _) => ctxt
  |
    (Alpha_context.Single
      (Alpha_context.Failing_noop _ | Alpha_context.Proposals _ |
      Alpha_context.Ballot _ | Alpha_context.Seed_nonce_revelation _ |
      Alpha_context.Double_endorsement_evidence _ |
      Alpha_context.Double_preendorsement_evidence _ |
      Alpha_context.Double_baking_evidence _ | Alpha_context.Activate_account _
      | Alpha_context.Manager_operation _) |
    Alpha_context.Cons (Alpha_context.Manager_operation _) _) =>
    let hash_value := Alpha_context.Operation.hash_value operation in
    Alpha_context.record_non_consensus_operation_hash ctxt hash_value
  | _ => unreachable_gadt_branch
  end.

Module expected_consensus_content.
  Record record : Set := Build {
    payload_hash : Block_payload_hash.t;
    branch : Block_hash.t;
    level : Alpha_context.Level.t;
    round : Alpha_context.Round.t }.
  Definition with_payload_hash payload_hash (r : record) :=
    Build payload_hash r.(branch) r.(level) r.(round).
  Definition with_branch branch (r : record) :=
    Build r.(payload_hash) branch r.(level) r.(round).
  Definition with_level level (r : record) :=
    Build r.(payload_hash) r.(branch) level r.(round).
  Definition with_round round (r : record) :=
    Build r.(payload_hash) r.(branch) r.(level) round.
End expected_consensus_content.
Definition expected_consensus_content := expected_consensus_content.record.

Definition compute_expected_consensus_content
  (current_level : Alpha_context.Level.t)
  (proposal_level : Alpha_context.Level.t) (ctxt : Alpha_context.t)
  (application_mode : apply_mode)
  (operation_kind : Alpha_context.Consensus_operation_type.t)
  (operation_round : Alpha_context.Round.t)
  (operation_level : Alpha_context.Raw_level.t)
  : M? (Alpha_context.t * expected_consensus_content) :=
  match operation_kind with
  | Alpha_context.Consensus_operation_type.Endorsement =>
    match Alpha_context.Consensus.endorsement_branch ctxt with
    | None =>
      match application_mode with
      | (Application _ | Full_construction _) =>
        Error_monad.fail
          (Build_extensible "Unexpected_endorsement_in_block" unit tt)
      | Partial_construction _ =>
        Error_monad.fail
          (Build_extensible "Consensus_operation_for_future_level"
            Consensus_operation_for_future_level
            {|
              Consensus_operation_for_future_level.expected :=
                proposal_level.(Alpha_context.Level.t.level);
              Consensus_operation_for_future_level.provided := operation_level
              |})
      end
    | Some (branch, payload_hash) =>
      match application_mode with
      |
        (Application {|
          apply_mode.Application.predecessor_round := predecessor_round |} |
        Full_construction {|
          apply_mode.Full_construction.predecessor_round := predecessor_round
            |} |
        Partial_construction {|
          apply_mode.Partial_construction.predecessor_round := predecessor_round
            |}) =>
        return?
          (ctxt,
            {| expected_consensus_content.payload_hash := payload_hash;
              expected_consensus_content.branch := branch;
              expected_consensus_content.level := proposal_level;
              expected_consensus_content.round := predecessor_round |})
      end
    end
  | Alpha_context.Consensus_operation_type.Preendorsement =>
    match application_mode with
    | Application {| apply_mode.Application.locked_round := None |} =>
      Error_monad.fail
        (Build_extensible "Unexpected_preendorsement_in_block" unit tt)
    |
      Application {|
        apply_mode.Application.predecessor_block := branch;
          apply_mode.Application.payload_hash := payload_hash;
          apply_mode.Application.locked_round := Some locked_round
          |} =>
      return?
        (ctxt,
          {| expected_consensus_content.payload_hash := payload_hash;
            expected_consensus_content.branch := branch;
            expected_consensus_content.level := current_level;
            expected_consensus_content.round := locked_round |})
    |
      Partial_construction {|
        apply_mode.Partial_construction.predecessor_round := predecessor_round
          |} =>
      match Alpha_context.Consensus.endorsement_branch ctxt with
      | None =>
        Error_monad.fail
          (Build_extensible "Consensus_operation_for_future_level"
            Consensus_operation_for_future_level
            {|
              Consensus_operation_for_future_level.expected :=
                proposal_level.(Alpha_context.Level.t.level);
              Consensus_operation_for_future_level.provided := operation_level
              |})
      | Some (branch, payload_hash) =>
        return?
          (ctxt,
            {| expected_consensus_content.payload_hash := payload_hash;
              expected_consensus_content.branch := branch;
              expected_consensus_content.level := proposal_level;
              expected_consensus_content.round := predecessor_round |})
      end
    |
      Full_construction {|
        apply_mode.Full_construction.predecessor_block := branch;
          apply_mode.Full_construction.payload_hash := payload_hash
          |} =>
      let '(ctxt', round) :=
        match Alpha_context.Consensus.get_preendorsements_quorum_round ctxt with
        | None =>
          ((Alpha_context.Consensus.set_preendorsements_quorum_round ctxt
            operation_round), operation_round)
        | Some round => (ctxt, round)
        end in
      return?
        (ctxt',
          {| expected_consensus_content.payload_hash := payload_hash;
            expected_consensus_content.branch := branch;
            expected_consensus_content.level := current_level;
            expected_consensus_content.round := round |})
    end
  end.

Definition check_level
  (apply_mode : apply_mode) (expected : Alpha_context.Raw_level.t)
  (provided : Alpha_context.Raw_level.t) : M? unit :=
  match apply_mode with
  | (Application _ | Full_construction _) =>
    Error_monad.error_unless (Alpha_context.Raw_level.equal expected provided)
      (Build_extensible "Wrong_level_for_consensus_operation"
        Wrong_level_for_consensus_operation
        {| Wrong_level_for_consensus_operation.expected := expected;
          Wrong_level_for_consensus_operation.provided := provided |})
  | Partial_construction _ =>
    let? '_ :=
      Error_monad.error_when (Alpha_context.Raw_level.op_gt expected provided)
        (Build_extensible "Consensus_operation_for_old_level"
          Consensus_operation_for_old_level
          {| Consensus_operation_for_old_level.expected := expected;
            Consensus_operation_for_old_level.provided := provided |}) in
    Error_monad.error_when (Alpha_context.Raw_level.op_lt expected provided)
      (Build_extensible "Consensus_operation_for_future_level"
        Consensus_operation_for_future_level
        {| Consensus_operation_for_future_level.expected := expected;
          Consensus_operation_for_future_level.provided := provided |})
  end.

Definition check_payload_hash
  (apply_mode : apply_mode) (expected : Block_payload_hash.t)
  (provided : Block_payload_hash.t) : M? unit :=
  match apply_mode with
  | (Application _ | Full_construction _) =>
    Error_monad.error_unless (Block_payload_hash.equal expected provided)
      (Build_extensible "Wrong_payload_hash_for_consensus_operation"
        Wrong_payload_hash_for_consensus_operation
        {| Wrong_payload_hash_for_consensus_operation.expected := expected;
          Wrong_payload_hash_for_consensus_operation.provided := provided |})
  | Partial_construction _ =>
    Error_monad.error_unless (Block_payload_hash.equal expected provided)
      (Build_extensible "Consensus_operation_on_competing_proposal"
        Consensus_operation_on_competing_proposal
        {| Consensus_operation_on_competing_proposal.expected := expected;
          Consensus_operation_on_competing_proposal.provided := provided |})
  end.

Definition check_operation_branch
  (expected : Block_hash.t) (provided : Block_hash.t) : M? unit :=
  Error_monad.error_unless (Block_hash.equal expected provided)
    (Build_extensible "Wrong_consensus_operation_branch"
      (Block_hash.t * Block_hash.t) (expected, provided)).

Definition check_round
  (operation_kind : Alpha_context.Consensus_operation_type.t)
  (apply_mode : apply_mode) (expected : Alpha_context.Round.t)
  (provided : Alpha_context.Round.t) : M? unit :=
  match apply_mode with
  | Partial_construction _ =>
    let? '_ :=
      Error_monad.error_when (Alpha_context.Round.op_gt expected provided)
        (Build_extensible "Consensus_operation_for_old_round"
          Consensus_operation_for_old_round
          {| Consensus_operation_for_old_round.expected := expected;
            Consensus_operation_for_old_round.provided := provided |}) in
    Error_monad.error_when (Alpha_context.Round.op_lt expected provided)
      (Build_extensible "Consensus_operation_for_future_round"
        Consensus_operation_for_future_round
        {| Consensus_operation_for_future_round.expected := expected;
          Consensus_operation_for_future_round.provided := provided |})
  |
    (Full_construction {| apply_mode.Full_construction.round := round |} |
    Application {| apply_mode.Application.round := round |}) =>
    let? '_ :=
      match operation_kind with
      | Alpha_context.Consensus_operation_type.Preendorsement =>
        Error_monad.error_when (Alpha_context.Round.op_lteq round provided)
          (Build_extensible "Preendorsement_round_too_high"
            Preendorsement_round_too_high
            {| Preendorsement_round_too_high.block_round := round;
              Preendorsement_round_too_high.provided := provided |})
      | Alpha_context.Consensus_operation_type.Endorsement => Result.return_unit
      end in
    Error_monad.error_unless (Alpha_context.Round.equal expected provided)
      (Build_extensible "Wrong_round_for_consensus_operation"
        Wrong_round_for_consensus_operation
        {| Wrong_round_for_consensus_operation.expected := expected;
          Wrong_round_for_consensus_operation.provided := provided |})
  end.

Definition check_consensus_content
  (apply_mode : apply_mode) (content : Alpha_context.consensus_content)
  (operation_branch : Block_hash.t)
  (operation_kind : Alpha_context.Consensus_operation_type.t)
  (expected_content : expected_consensus_content) : M? unit :=
  let expected_level :=
    expected_content.(expected_consensus_content.level).(Alpha_context.Level.t.level)
    in
  let provided_level := content.(Alpha_context.consensus_content.level) in
  let expected_round := expected_content.(expected_consensus_content.round) in
  let provided_round := content.(Alpha_context.consensus_content.round) in
  let? '_ := check_level apply_mode expected_level provided_level in
  let? '_ := check_round operation_kind apply_mode expected_round provided_round
    in
  let? '_ :=
    check_operation_branch expected_content.(expected_consensus_content.branch)
      operation_branch in
  check_payload_hash apply_mode
    expected_content.(expected_consensus_content.payload_hash)
    content.(Alpha_context.consensus_content.block_payload_hash).

Axiom validate_consensus_contents :
  Alpha_context.context -> Chain_id.t ->
  Alpha_context.Consensus_operation_type.t -> Alpha_context.operation ->
  apply_mode -> Alpha_context.consensus_content ->
  M? (Alpha_context.context * Alpha_context.public_key_hash * int).

Definition apply_manager_contents_list
  (ctxt : Alpha_context.t) (mode : Script_ir_translator.unparsing_mode)
  (payload_producer : Alpha_context.public_key_hash) (chain_id : Chain_id.t)
  (prechecked_contents_list : Apply_results.prechecked_contents_list)
  : Alpha_context.t * Apply_results.contents_result_list :=
  let '(ctxt_result, results) :=
    apply_manager_contents_list_rec ctxt mode payload_producer chain_id
      prechecked_contents_list in
  match ctxt_result with
  | Failure => (ctxt, (mark_backtracked results))
  | Success ctxt =>
    let ctxt := Alpha_context.Lazy_storage.cleanup_temporaries ctxt in
    (ctxt, results)
  end.

Definition check_denunciation_age
  (ctxt : Alpha_context.context) (kind_value : denunciation_kind)
  (given_level : Alpha_context.Raw_level.t) : M? unit :=
  let max_slashing_period := Alpha_context.Constants.max_slashing_period ctxt in
  let current_cycle :=
    (Alpha_context.Level.current ctxt).(Alpha_context.Level.t.cycle) in
  let given_cycle :=
    (Alpha_context.Level.from_raw ctxt given_level).(Alpha_context.Level.t.cycle)
    in
  let last_slashable_cycle :=
    Alpha_context.Cycle.add given_cycle max_slashing_period in
  let? '_ :=
    Error_monad.fail_when (Alpha_context.Cycle.op_gt given_cycle current_cycle)
      (Build_extensible "Too_early_denunciation" Too_early_denunciation
        {| Too_early_denunciation.kind := kind_value;
          Too_early_denunciation.level := given_level;
          Too_early_denunciation.current :=
            (Alpha_context.Level.current ctxt).(Alpha_context.Level.t.level) |})
    in
  Error_monad.fail_unless
    (Alpha_context.Cycle.op_gt last_slashable_cycle current_cycle)
    (Build_extensible "Outdated_denunciation" Outdated_denunciation
      {| Outdated_denunciation.kind := kind_value;
        Outdated_denunciation.level := given_level;
        Outdated_denunciation.last_cycle := last_slashable_cycle |}).

Inductive mistake : Set :=
| Double_baking : mistake
| Double_endorsing : mistake.

Definition punish_delegate
  (ctxt : Alpha_context.context) (delegate : Alpha_context.public_key_hash)
  (level : Alpha_context.Level.t) (mistake : mistake)
  (mk_result :
    list
      (Alpha_context.Receipt.balance * Alpha_context.Receipt.balance_update *
        Alpha_context.Receipt.update_origin) -> Apply_results.contents_result)
  (payload_producer : Alpha_context.public_key_hash)
  : M? (Alpha_context.context * Apply_results.contents_result_list) :=
  let '(already_slashed, punish) :=
    match mistake with
    | Double_baking =>
      (Alpha_context.Delegate.already_slashed_for_double_baking,
        Alpha_context.Delegate.punish_double_baking)
    | Double_endorsing =>
      (Alpha_context.Delegate.already_slashed_for_double_endorsing,
        Alpha_context.Delegate.punish_double_endorsing)
    end in
  let? slashed := already_slashed ctxt delegate level in
  let? '_ :=
    Error_monad.fail_when slashed
      (Build_extensible "Unrequired_denunciation" unit tt) in
  let? '(ctxt, burned, punish_balance_updates) := punish ctxt delegate level in
  let? '(ctxt, reward_balance_updates) :=
    match Alpha_context.Tez.op_divquestion burned 2 with
    | Pervasives.Ok reward =>
      Alpha_context.Token.transfer None ctxt
        Alpha_context.Token.Double_signing_evidence_rewards
        (Alpha_context.Token.SinkContainer
          (Alpha_context.Token.Contract
            (Alpha_context.Contract.implicit_contract payload_producer))) reward
    | Pervasives.Error _ => return? (ctxt, nil)
    end in
  let balance_updates :=
    Pervasives.op_at reward_balance_updates punish_balance_updates in
  return? (ctxt, (Apply_results.Single_result (mk_result balance_updates))).

Definition punish_double_endorsement_or_preendorsement
  (ctxt : Alpha_context.context) (chain_id : Chain_id.t) (preendorsement : bool)
  (op1 : Alpha_context.Operation.t) (op2 : Alpha_context.Operation.t)
  (payload_producer : Alpha_context.public_key_hash)
  : M? (Alpha_context.t * Apply_results.contents_result_list) :=
  let mk_result (balance_updates : Alpha_context.Receipt.balance_updates)
    : Apply_results.contents_result :=
    match
      op1.(Alpha_context.operation.protocol_data).(Alpha_context.protocol_data.contents)
      with
    | Alpha_context.Single (Alpha_context.Preendorsement _) =>
      Apply_results.Double_preendorsement_evidence_result balance_updates
    | Alpha_context.Single (Alpha_context.Endorsement _) =>
      Apply_results.Double_endorsement_evidence_result balance_updates
    | _ => unreachable_gadt_branch
    end in
  match
    (op1.(Alpha_context.operation.protocol_data).(Alpha_context.protocol_data.contents),
      op2.(Alpha_context.operation.protocol_data).(Alpha_context.protocol_data.contents))
    with
  |
    ((Alpha_context.Single (Alpha_context.Preendorsement e1),
      Alpha_context.Single (Alpha_context.Preendorsement e2)) |
    (Alpha_context.Single (Alpha_context.Endorsement e1),
      Alpha_context.Single (Alpha_context.Endorsement e2))) =>
    let kind_value :=
      if preendorsement then
        Preendorsement
      else
        Endorsement in
    let op1_hash := Alpha_context.Operation.hash_value op1 in
    let op2_hash := Alpha_context.Operation.hash_value op2 in
    let? '_ :=
      Error_monad.fail_unless
        ((Alpha_context.Raw_level.op_eq
          e1.(Alpha_context.consensus_content.level)
          e2.(Alpha_context.consensus_content.level)) &&
        ((Alpha_context.Round.op_eq e1.(Alpha_context.consensus_content.round)
          e2.(Alpha_context.consensus_content.round)) &&
        ((Pervasives.not
          (Block_payload_hash.equal
            e1.(Alpha_context.consensus_content.block_payload_hash)
            e2.(Alpha_context.consensus_content.block_payload_hash))) &&
        (Operation_hash.op_lt op1_hash op2_hash))))
        (Build_extensible "Invalid_denunciation" denunciation_kind kind_value)
      in
    let level :=
      Alpha_context.Level.from_raw ctxt
        e1.(Alpha_context.consensus_content.level) in
    let? '_ :=
      check_denunciation_age ctxt kind_value level.(Alpha_context.Level.t.level)
      in
    let? '(ctxt, (delegate1_pk, delegate1)) :=
      Alpha_context.Stake_distribution.slot_owner ctxt level
        e1.(Alpha_context.consensus_content.slot) in
    let? '(ctxt, (_delegate2_pk, delegate2)) :=
      Alpha_context.Stake_distribution.slot_owner ctxt level
        e2.(Alpha_context.consensus_content.slot) in
    let? '_ :=
      Error_monad.fail_unless
        (Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.equal) delegate1
          delegate2)
        (Build_extensible "Inconsistent_denunciation" Inconsistent_denunciation
          {| Inconsistent_denunciation.kind := kind_value;
            Inconsistent_denunciation.delegate1 := delegate1;
            Inconsistent_denunciation.delegate2 := delegate2 |}) in
    let '(delegate_pk, delegate) := (delegate1_pk, delegate1) in
    let? '_ := Alpha_context.Operation.check_signature delegate_pk chain_id op1
      in
    let? '_ := Alpha_context.Operation.check_signature delegate_pk chain_id op2
      in
    punish_delegate ctxt delegate level Double_endorsing mk_result
      payload_producer
  | _ => unreachable_gadt_branch
  end.

Definition punish_double_baking
  (ctxt : Alpha_context.context) (chain_id : Chain_id.t)
  (bh1 : Alpha_context.Block_header.block_header)
  (bh2 : Alpha_context.Block_header.block_header)
  (payload_producer : Alpha_context.public_key_hash)
  : M? (Alpha_context.context * Apply_results.contents_result_list) :=
  let hash1 := Alpha_context.Block_header.hash_value bh1 in
  let hash2 := Alpha_context.Block_header.hash_value bh2 in
  let? bh1_fitness :=
    Alpha_context.Fitness.from_raw
      bh1.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.fitness)
    in
  let round1 := Alpha_context.Fitness.round bh1_fitness in
  let? bh2_fitness :=
    Alpha_context.Fitness.from_raw
      bh2.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.fitness)
    in
  let round2 := Alpha_context.Fitness.round bh2_fitness in
  let? '_ :=
    let? level1 :=
      Alpha_context.Raw_level.of_int32
        bh1.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.level)
      in
    let? level2 :=
      Alpha_context.Raw_level.of_int32
        bh2.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.level)
      in
    Error_monad.fail_unless
      ((bh1.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.level)
      =i32
      bh2.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.level))
      &&
      ((Alpha_context.Round.op_eq round1 round2) &&
      (Block_hash.op_lt hash1 hash2)))
      (Build_extensible "Invalid_double_baking_evidence"
        Invalid_double_baking_evidence
        {| Invalid_double_baking_evidence.hash1 := hash1;
          Invalid_double_baking_evidence.level1 := level1;
          Invalid_double_baking_evidence.round1 := round1;
          Invalid_double_baking_evidence.hash2 := hash2;
          Invalid_double_baking_evidence.level2 := level2;
          Invalid_double_baking_evidence.round2 := round2 |}) in
  let? raw_level :=
    Alpha_context.Raw_level.of_int32
      bh1.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.level)
    in
  let? '_ := check_denunciation_age ctxt Block raw_level in
  let level := Alpha_context.Level.from_raw ctxt raw_level in
  let committee_size := Alpha_context.Constants.consensus_committee_size ctxt in
  let? slot1 := Alpha_context.Round.to_slot round1 committee_size in
  let? '(ctxt, (delegate1_pk, delegate1)) :=
    Alpha_context.Stake_distribution.slot_owner ctxt level slot1 in
  let? slot2 := Alpha_context.Round.to_slot round2 committee_size in
  let? '(ctxt, (_delegate2_pk, delegate2)) :=
    Alpha_context.Stake_distribution.slot_owner ctxt level slot2 in
  let? '_ :=
    Error_monad.fail_unless
      (Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.op_eq) delegate1
        delegate2)
      (Build_extensible "Inconsistent_denunciation" Inconsistent_denunciation
        {| Inconsistent_denunciation.kind := Block;
          Inconsistent_denunciation.delegate1 := delegate1;
          Inconsistent_denunciation.delegate2 := delegate2 |}) in
  let '(delegate_pk, delegate) := (delegate1_pk, delegate1) in
  let? '_ := Alpha_context.Block_header.check_signature bh1 chain_id delegate_pk
    in
  let? '_ := Alpha_context.Block_header.check_signature bh2 chain_id delegate_pk
    in
  punish_delegate ctxt delegate level Double_baking
    (fun (balance_updates : Alpha_context.Receipt.balance_updates) =>
      Apply_results.Double_baking_evidence_result balance_updates)
    payload_producer.

Definition is_parent_endorsement
  (ctxt : Alpha_context.t) (proposal_level : Alpha_context.Level.t)
  (grand_parent_round : Alpha_context.Round.t)
  (operation : Alpha_context.operation)
  (operation_content : Alpha_context.consensus_content) : bool :=
  match Alpha_context.Consensus.grand_parent_branch ctxt with
  | None => false
  | Some (great_grand_parent_hash, grand_parent_payload_hash) =>
    (Alpha_context.Raw_level.op_eq proposal_level.(Alpha_context.Level.t.level)
      (Alpha_context.Raw_level.succ
        operation_content.(Alpha_context.consensus_content.level))) &&
    ((Alpha_context.Round.op_eq grand_parent_round
      operation_content.(Alpha_context.consensus_content.round)) &&
    ((Block_payload_hash.op_eq grand_parent_payload_hash
      operation_content.(Alpha_context.consensus_content.block_payload_hash)) &&
    (Block_hash.op_eq great_grand_parent_hash
      operation.(Alpha_context.operation.shell).(Operation.shell_header.branch))))
  end.

Definition validate_grand_parent_endorsement
  (ctxt : Alpha_context.context) (chain_id : Chain_id.t)
  (op : Alpha_context.operation)
  : M? (Alpha_context.t * Apply_results.contents_result_list) :=
  match
    op.(Alpha_context.operation.protocol_data).(Alpha_context.protocol_data.contents)
    with
  | Alpha_context.Single (Alpha_context.Endorsement e_value) =>
    let level :=
      Alpha_context.Level.from_raw ctxt
        e_value.(Alpha_context.consensus_content.level) in
    let? '(ctxt, (delegate_pk, pkh)) :=
      Alpha_context.Stake_distribution.slot_owner ctxt level
        e_value.(Alpha_context.consensus_content.slot) in
    let? '_ := Alpha_context.Operation.check_signature delegate_pk chain_id op
      in
    let? ctxt :=
      Alpha_context.Consensus.record_grand_parent_endorsement ctxt pkh in
    return?
      (ctxt,
        (Apply_results.Single_result
          (Apply_results.Endorsement_result
            {|
              Apply_results.contents_result.Endorsement_result.balance_updates
                := nil;
              Apply_results.contents_result.Endorsement_result.delegate := pkh;
              Apply_results.contents_result.Endorsement_result.endorsement_power
                := 0 |})))
  | _ => unreachable_gadt_branch
  end.

Definition apply_contents_list
  (ctxt : Alpha_context.context) (chain_id : Chain_id.t)
  (apply_mode : apply_mode) (mode : Script_ir_translator.unparsing_mode)
  (payload_producer : Alpha_context.public_key_hash)
  (operation : Alpha_context.operation)
  (contents_list : Alpha_context.contents_list)
  : M? (Alpha_context.context * Apply_results.contents_result_list) :=
  let mempool_mode :=
    match apply_mode with
    | Partial_construction _ => true
    | (Full_construction _ | Application _) => false
    end in
  match contents_list with
  | Alpha_context.Single (Alpha_context.Preendorsement consensus_content) =>
    let? '(ctxt, delegate, voting_power) :=
      validate_consensus_contents ctxt chain_id
        Alpha_context.Consensus_operation_type.Preendorsement operation
        apply_mode consensus_content in
    let? ctxt :=
      Alpha_context.Consensus.record_preendorsement ctxt
        consensus_content.(Alpha_context.consensus_content.slot) voting_power
        consensus_content.(Alpha_context.consensus_content.round) in
    return?
      (ctxt,
        (Apply_results.Single_result
          (Apply_results.Preendorsement_result
            {|
              Apply_results.contents_result.Preendorsement_result.balance_updates
                := nil;
              Apply_results.contents_result.Preendorsement_result.delegate :=
                delegate;
              Apply_results.contents_result.Preendorsement_result.preendorsement_power
                := voting_power |})))
  | Alpha_context.Single (Alpha_context.Endorsement consensus_content) =>
    let proposal_level := get_predecessor_level apply_mode in
    match
      (apply_mode,
        match apply_mode with
        |
          Partial_construction {|
            apply_mode.Partial_construction.grand_parent_round := grand_parent_round
              |} =>
          is_parent_endorsement ctxt proposal_level grand_parent_round operation
            consensus_content
        | _ => false
        end) with
    |
      (Partial_construction {|
        apply_mode.Partial_construction.grand_parent_round := grand_parent_round
          |}, true) => validate_grand_parent_endorsement ctxt chain_id operation
    | (_, _) =>
      let? '(ctxt, delegate, voting_power) :=
        validate_consensus_contents ctxt chain_id
          Alpha_context.Consensus_operation_type.Endorsement operation
          apply_mode consensus_content in
      let? ctxt :=
        Alpha_context.Consensus.record_endorsement ctxt
          consensus_content.(Alpha_context.consensus_content.slot) voting_power
        in
      return?
        (ctxt,
          (Apply_results.Single_result
            (Apply_results.Endorsement_result
              {|
                Apply_results.contents_result.Endorsement_result.balance_updates
                  := nil;
                Apply_results.contents_result.Endorsement_result.delegate :=
                  delegate;
                Apply_results.contents_result.Endorsement_result.endorsement_power
                  := voting_power |})))
    end
  |
    Alpha_context.Single
      (Alpha_context.Seed_nonce_revelation {|
        Alpha_context.contents.Seed_nonce_revelation.level := level;
          Alpha_context.contents.Seed_nonce_revelation.nonce := nonce_value
          |}) =>
    let level := Alpha_context.Level.from_raw ctxt level in
    let? ctxt := Alpha_context.Nonce.reveal ctxt level nonce_value in
    let tip := Alpha_context.Constants.seed_nonce_revelation_tip ctxt in
    let contract := Alpha_context.Contract.implicit_contract payload_producer in
    let? '(ctxt, balance_updates) :=
      Alpha_context.Token.transfer None ctxt
        Alpha_context.Token.Revelation_rewards
        (Alpha_context.Token.SinkContainer
          (Alpha_context.Token.Contract contract)) tip in
    return?
      (ctxt,
        (Apply_results.Single_result
          (Apply_results.Seed_nonce_revelation_result balance_updates)))
  |
    Alpha_context.Single
      (Alpha_context.Double_preendorsement_evidence {|
        Alpha_context.contents.Double_preendorsement_evidence.op1 := op1;
          Alpha_context.contents.Double_preendorsement_evidence.op2 := op2
          |}) =>
    punish_double_endorsement_or_preendorsement ctxt chain_id true op1 op2
      payload_producer
  |
    Alpha_context.Single
      (Alpha_context.Double_endorsement_evidence {|
        Alpha_context.contents.Double_endorsement_evidence.op1 := op1;
          Alpha_context.contents.Double_endorsement_evidence.op2 := op2
          |}) =>
    punish_double_endorsement_or_preendorsement ctxt chain_id false op1 op2
      payload_producer
  |
    Alpha_context.Single
      (Alpha_context.Double_baking_evidence {|
        Alpha_context.contents.Double_baking_evidence.bh1 := bh1;
          Alpha_context.contents.Double_baking_evidence.bh2 := bh2
          |}) => punish_double_baking ctxt chain_id bh1 bh2 payload_producer
  |
    Alpha_context.Single
      (Alpha_context.Activate_account {|
        Alpha_context.contents.Activate_account.id := pkh;
          Alpha_context.contents.Activate_account.activation_code :=
            activation_code
          |}) =>
    let blinded_pkh :=
      Blinded_public_key_hash.of_ed25519_pkh activation_code pkh in
    let src := Alpha_context.Token.Collected_commitments blinded_pkh in
    let? src_exists := Alpha_context.Token.allocated ctxt src in
    let? function_parameter :=
      Error_monad.fail_unless src_exists
        (Build_extensible "Invalid_activation" Invalid_activation
          {| Invalid_activation.pkh := pkh |}) in
    let '_ := function_parameter in
    let contract :=
      Alpha_context.Contract.implicit_contract (Signature.Ed25519Hash pkh) in
    let? amount := Alpha_context.Token.balance ctxt src in
    let? '(ctxt, bupds) :=
      Alpha_context.Token.transfer None ctxt
        (Alpha_context.Token.SourceContainer src)
        (Alpha_context.Token.SinkContainer
          (Alpha_context.Token.Contract contract)) amount in
    return?
      (ctxt,
        (Apply_results.Single_result
          (Apply_results.Activate_account_result bupds)))
  |
    Alpha_context.Single
      (Alpha_context.Proposals {|
        Alpha_context.contents.Proposals.source := source;
          Alpha_context.contents.Proposals.period := period;
          Alpha_context.contents.Proposals.proposals := proposals
          |}) =>
    let? delegate := Alpha_context.Delegate.pubkey ctxt source in
    let? '_ :=
      Alpha_context.Operation.check_signature delegate chain_id operation in
    let? '{|
      Alpha_context.Voting_period.voting_period.index := current_period |} :=
      Alpha_context.Voting_period.get_current ctxt in
    let? '_ :=
      Error_monad.error_unless (current_period =i32 period)
        (Build_extensible "Wrong_voting_period" (int32 * int32)
          (current_period, period)) in
    let? ctxt := Amendment.record_proposals ctxt source proposals in
    return? (ctxt, (Apply_results.Single_result Apply_results.Proposals_result))
  |
    Alpha_context.Single
      (Alpha_context.Ballot {|
        Alpha_context.contents.Ballot.source := source;
          Alpha_context.contents.Ballot.period := period;
          Alpha_context.contents.Ballot.proposal := proposal;
          Alpha_context.contents.Ballot.ballot := ballot
          |}) =>
    let? delegate := Alpha_context.Delegate.pubkey ctxt source in
    let? '_ :=
      Alpha_context.Operation.check_signature delegate chain_id operation in
    let? '{|
      Alpha_context.Voting_period.voting_period.index := current_period |} :=
      Alpha_context.Voting_period.get_current ctxt in
    let? '_ :=
      Error_monad.error_unless (current_period =i32 period)
        (Build_extensible "Wrong_voting_period" (int32 * int32)
          (current_period, period)) in
    let? ctxt := Amendment.record_ballot ctxt source proposal ballot in
    return? (ctxt, (Apply_results.Single_result Apply_results.Ballot_result))
  | Alpha_context.Single (Alpha_context.Failing_noop _) =>
    Error_monad.fail (Build_extensible "Failing_noop_error" unit tt)
  | (Alpha_context.Single (Alpha_context.Manager_operation _)) as op =>
    let? '(ctxt, prechecked_contents_list) :=
      precheck_manager_contents_list ctxt op mempool_mode in
    let? '_ := check_manager_signature ctxt chain_id op operation in
    Error_monad.op_gtpipeeq
      (apply_manager_contents_list ctxt mode payload_producer chain_id
        prechecked_contents_list) Error_monad.ok
  | (Alpha_context.Cons (Alpha_context.Manager_operation _) _) as op =>
    let? '(ctxt, prechecked_contents_list) :=
      precheck_manager_contents_list ctxt op mempool_mode in
    let? '_ := check_manager_signature ctxt chain_id op operation in
    Error_monad.op_gtpipeeq
      (apply_manager_contents_list ctxt mode payload_producer chain_id
        prechecked_contents_list) Error_monad.ok
  | _ => unreachable_gadt_branch
  end.

Definition apply_operation
  (ctxt : Alpha_context.context) (chain_id : Chain_id.t)
  (apply_mode : apply_mode) (mode : Script_ir_translator.unparsing_mode)
  (payload_producer : Alpha_context.public_key_hash)
  (hash_value : Operation_hash.t) (operation : Alpha_context.operation)
  : M? (Alpha_context.context * Apply_results.operation_metadata) :=
  let ctxt := Alpha_context.Contract.init_origination_nonce ctxt hash_value in
  let ctxt := record_operation ctxt operation in
  let? '(ctxt, result_value) :=
    apply_contents_list ctxt chain_id apply_mode mode payload_producer operation
      operation.(Alpha_context.operation.protocol_data).(Alpha_context.protocol_data.contents)
    in
  let ctxt := Alpha_context.Gas.set_unlimited ctxt in
  let ctxt := Alpha_context.Contract.unset_origination_nonce ctxt in
  return?
    (ctxt, {| Apply_results.operation_metadata.contents := result_value |}).

Definition may_start_new_cycle (ctxt : Alpha_context.context)
  : M?
    (Alpha_context.context * Alpha_context.Receipt.balance_updates *
      list Signature.public_key_hash) :=
  match Alpha_context.Level.dawn_of_a_new_cycle ctxt with
  | None => return? (ctxt, nil, nil)
  | Some last_cycle =>
    let? '(ctxt, unrevealed) := Alpha_context.Seed.cycle_end ctxt last_cycle in
    let? '(ctxt, balance_updates, deactivated) :=
      Alpha_context.Delegate.cycle_end ctxt last_cycle unrevealed in
    let? ctxt := Alpha_context.Bootstrap.cycle_end ctxt last_cycle in
    return? (ctxt, balance_updates, deactivated)
  end.

Definition init_allowed_consensus_operations
  (ctxt : Alpha_context.context) (endorsement_level : Alpha_context.Level.t)
  (preendorsement_level : Alpha_context.Level.t) : M? Alpha_context.t :=
  let? ctxt := Alpha_context.Delegate.prepare_stake_distribution ctxt in
  let? '(ctxt, allowed_endorsements, allowed_preendorsements) :=
    if Alpha_context.Level.op_eq endorsement_level preendorsement_level then
      let? '(ctxt, slots) :=
        Baking.endorsing_rights_by_first_slot ctxt endorsement_level in
      let consensus_operations := slots in
      return? (ctxt, consensus_operations, consensus_operations)
    else
      let? '(ctxt, endorsements_slots) :=
        Baking.endorsing_rights_by_first_slot ctxt endorsement_level in
      let endorsements := endorsements_slots in
      let? '(ctxt, preendorsements_slots) :=
        Baking.endorsing_rights_by_first_slot ctxt preendorsement_level in
      let preendorsements := preendorsements_slots in
      return? (ctxt, endorsements, preendorsements) in
  return?
    (Alpha_context.Consensus.initialize_consensus_operation ctxt
      allowed_endorsements allowed_preendorsements).

Definition apply_liquidity_baking_subsidy
  (ctxt : Alpha_context.context) (escape_vote : bool)
  : M?
    (Alpha_context.context *
      list Apply_results.packed_successful_manager_operation_result *
      Alpha_context.Liquidity_baking.escape_ema) :=
  Alpha_context.Liquidity_baking.on_subsidy_allowed ctxt escape_vote
    (fun (ctxt : Alpha_context.context) =>
      fun (liquidity_baking_cpmm_contract : Alpha_context.Contract.t) =>
        let ctxt :=
          Alpha_context.Gas.set_limit ctxt
            (Alpha_context.Gas.Arith.integral_exn
              ((Alpha_context.Gas.Arith.integral_to_z
                (Alpha_context.Constants.hard_gas_limit_per_block ctxt)) /Z
              (Z.of_int 20))) in
        let backtracking_ctxt := ctxt in
        let function_parameter :=
          let liquidity_baking_subsidy :=
            Alpha_context.Constants.liquidity_baking_subsidy ctxt in
          let? '(ctxt, balance_updates) :=
            Alpha_context.Token.transfer (Some Alpha_context.Receipt.Subsidy)
              ctxt Alpha_context.Token.Liquidity_baking_subsidies
              (Alpha_context.Token.SinkContainer
                (Alpha_context.Token.Contract liquidity_baking_cpmm_contract))
              liquidity_baking_subsidy in
          let? '(ctxt, cache_key, script) :=
            Script_cache.find ctxt liquidity_baking_cpmm_contract in
          match script with
          | None =>
            Error_monad.fail
              (Build_extensible "No_such_entrypoint" string "default")
          | Some (script, script_ir) =>
            let now := Alpha_context.Script_timestamp.now ctxt in
            let level :=
              Alpha_context.Script_int.abs
                (Alpha_context.Script_int.of_int32
                  (Alpha_context.Raw_level.to_int32
                    (Alpha_context.Level.current ctxt).(Alpha_context.Level.t.level)))
              in
            let step_constants :=
              {|
                Script_typed_ir.step_constants.source :=
                  liquidity_baking_cpmm_contract;
                Script_typed_ir.step_constants.payer :=
                  liquidity_baking_cpmm_contract;
                Script_typed_ir.step_constants.self :=
                  liquidity_baking_cpmm_contract;
                Script_typed_ir.step_constants.amount :=
                  liquidity_baking_subsidy;
                Script_typed_ir.step_constants.chain_id := Chain_id.zero;
                Script_typed_ir.step_constants.now := now;
                Script_typed_ir.step_constants.level := level |} in
            let parameter :=
              Micheline.strip_locations
                (Micheline.Prim 0 Michelson_v1_primitives.D_Unit nil nil) in
            let?
              '({|
                Script_interpreter.execution_result.ctxt := ctxt;
                  Script_interpreter.execution_result.storage := storage_value;
                  Script_interpreter.execution_result.lazy_storage_diff :=
                    lazy_storage_diff;
                  Script_interpreter.execution_result.operations := operations
                  |}, (updated_cached_script, updated_size)) :=
              Script_interpreter.execute None ctxt (Some script_ir)
                Script_ir_translator.Optimized step_constants script "default"
                parameter false in
            match operations with
            | cons _ _ => return? (backtracking_ctxt, nil)
            | [] =>
              let? ctxt :=
                Alpha_context.Contract.update_script_storage ctxt
                  liquidity_baking_cpmm_contract storage_value lazy_storage_diff
                in
              let? '(ctxt, new_size, paid_storage_size_diff) :=
                Alpha_context.Fees.record_paid_storage_space ctxt
                  liquidity_baking_cpmm_contract in
              let consumed_gas :=
                Alpha_context.Gas.consumed backtracking_ctxt ctxt in
              let? ctxt :=
                Script_cache.update ctxt cache_key
                  ((Alpha_context.Script.t.with_storage
                    (Alpha_context.Script.lazy_expr_value storage_value) script),
                    updated_cached_script) updated_size in
              let result_value :=
                Apply_results.Transaction_result
                  {|
                    Apply_results.successful_manager_operation_result.Transaction_result.storage
                      := Some storage_value;
                    Apply_results.successful_manager_operation_result.Transaction_result.lazy_storage_diff
                      := lazy_storage_diff;
                    Apply_results.successful_manager_operation_result.Transaction_result.balance_updates
                      := balance_updates;
                    Apply_results.successful_manager_operation_result.Transaction_result.originated_contracts
                      := nil;
                    Apply_results.successful_manager_operation_result.Transaction_result.consumed_gas
                      := consumed_gas;
                    Apply_results.successful_manager_operation_result.Transaction_result.storage_size
                      := new_size;
                    Apply_results.successful_manager_operation_result.Transaction_result.paid_storage_size_diff
                      := paid_storage_size_diff;
                    Apply_results.successful_manager_operation_result.Transaction_result.allocated_destination_contract
                      := false |} in
              let ctxt := Alpha_context.Gas.set_unlimited ctxt in
              return?
                (ctxt, [ Apply_results.Successful_manager_result result_value ])
            end
          end in
        match function_parameter with
        | Pervasives.Ok (ctxt, results) => Pervasives.Ok (ctxt, results)
        | Pervasives.Error _ =>
          let ctxt := Alpha_context.Gas.set_unlimited backtracking_ctxt in
          Pervasives.Ok (ctxt, nil)
        end).

Module full_construction.
  Record record {a : Set} : Set := Build {
    ctxt : Alpha_context.t;
    protocol_data : a;
    payload_producer : Signature.public_key_hash;
    block_producer : Signature.public_key_hash;
    round : Alpha_context.Round.t;
    implicit_operations_results :
      list Apply_results.packed_successful_manager_operation_result;
    liquidity_baking_escape_ema : Alpha_context.Liquidity_baking.escape_ema }.
  Arguments record : clear implicits.
  Definition with_ctxt {t_a} ctxt (r : record t_a) :=
    Build t_a ctxt r.(protocol_data) r.(payload_producer) r.(block_producer)
      r.(round) r.(implicit_operations_results) r.(liquidity_baking_escape_ema).
  Definition with_protocol_data {t_a} protocol_data (r : record t_a) :=
    Build t_a r.(ctxt) protocol_data r.(payload_producer) r.(block_producer)
      r.(round) r.(implicit_operations_results) r.(liquidity_baking_escape_ema).
  Definition with_payload_producer {t_a} payload_producer (r : record t_a) :=
    Build t_a r.(ctxt) r.(protocol_data) payload_producer r.(block_producer)
      r.(round) r.(implicit_operations_results) r.(liquidity_baking_escape_ema).
  Definition with_block_producer {t_a} block_producer (r : record t_a) :=
    Build t_a r.(ctxt) r.(protocol_data) r.(payload_producer) block_producer
      r.(round) r.(implicit_operations_results) r.(liquidity_baking_escape_ema).
  Definition with_round {t_a} round (r : record t_a) :=
    Build t_a r.(ctxt) r.(protocol_data) r.(payload_producer) r.(block_producer)
      round r.(implicit_operations_results) r.(liquidity_baking_escape_ema).
  Definition with_implicit_operations_results {t_a} implicit_operations_results
    (r : record t_a) :=
    Build t_a r.(ctxt) r.(protocol_data) r.(payload_producer) r.(block_producer)
      r.(round) implicit_operations_results r.(liquidity_baking_escape_ema).
  Definition with_liquidity_baking_escape_ema {t_a} liquidity_baking_escape_ema
    (r : record t_a) :=
    Build t_a r.(ctxt) r.(protocol_data) r.(payload_producer) r.(block_producer)
      r.(round) r.(implicit_operations_results) liquidity_baking_escape_ema.
End full_construction.
Definition full_construction := full_construction.record.

Definition begin_full_construction
  (ctxt : Alpha_context.context) (predecessor_timestamp : Time.t)
  (predecessor_level : Alpha_context.Level.t)
  (predecessor_round : Alpha_context.Round.t) (round : Alpha_context.Round.t)
  (protocol_data : Alpha_context.Block_header.contents)
  : M? (full_construction Alpha_context.Block_header.contents) :=
  let round_durations := Alpha_context.Constants.round_durations ctxt in
  let timestamp := Alpha_context.Timestamp.current ctxt in
  let? '_ :=
    Alpha_context.Block_header.check_timestamp round_durations timestamp round
      predecessor_timestamp predecessor_round in
  let current_level := Alpha_context.Level.current ctxt in
  let? '(ctxt, _slot, (_block_producer_pk, block_producer)) :=
    Alpha_context.Stake_distribution.baking_rights_owner ctxt current_level
      round in
  let? frozen_deposits :=
    Alpha_context.Delegate.frozen_deposits ctxt block_producer in
  let? '_ :=
    Error_monad.fail_unless
      (Alpha_context.Tez.op_gt frozen_deposits Alpha_context.Tez.zero)
      (Build_extensible "Zero_frozen_deposits" Alpha_context.public_key_hash
        block_producer) in
  let? '(ctxt, _slot, (_payload_producer_pk, payload_producer)) :=
    Alpha_context.Stake_distribution.baking_rights_owner ctxt current_level
      protocol_data.(Alpha_context.Block_header.contents.payload_round) in
  let? ctxt :=
    init_allowed_consensus_operations ctxt predecessor_level current_level in
  let escape_vote :=
    protocol_data.(Alpha_context.Block_header.contents.liquidity_baking_escape_vote)
    in
  let?
    '(ctxt, liquidity_baking_operations_results, liquidity_baking_escape_ema) :=
    apply_liquidity_baking_subsidy ctxt escape_vote in
  return?
    {| full_construction.ctxt := ctxt;
      full_construction.protocol_data := protocol_data;
      full_construction.payload_producer := payload_producer;
      full_construction.block_producer := block_producer;
      full_construction.round := round;
      full_construction.implicit_operations_results :=
        liquidity_baking_operations_results;
      full_construction.liquidity_baking_escape_ema :=
        liquidity_baking_escape_ema |}.

Definition begin_partial_construction
  (ctxt : Alpha_context.context) (predecessor_level : Alpha_context.Level.t)
  (escape_vote : bool)
  : M?
    (Alpha_context.context *
      list Apply_results.packed_successful_manager_operation_result *
      Alpha_context.Liquidity_baking.escape_ema) :=
  let? ctxt :=
    init_allowed_consensus_operations ctxt predecessor_level predecessor_level
    in
  apply_liquidity_baking_subsidy ctxt escape_vote.

Definition begin_application
  (ctxt : Alpha_context.context) (chain_id : Chain_id.t)
  (block_header : Alpha_context.Block_header.t)
  (fitness : Alpha_context.Fitness.t) (predecessor_timestamp : Time.t)
  (predecessor_level : Alpha_context.Level.t)
  (predecessor_round : Alpha_context.Round.t)
  : M?
    (Alpha_context.context * Alpha_context.public_key *
      Alpha_context.public_key_hash *
      list Apply_results.packed_successful_manager_operation_result *
      Alpha_context.Liquidity_baking.escape_ema) :=
  let round := Alpha_context.Fitness.round fitness in
  let current_level := Alpha_context.Level.current ctxt in
  let? '(ctxt, _slot, (block_producer_pk, block_producer)) :=
    Alpha_context.Stake_distribution.baking_rights_owner ctxt current_level
      round in
  let timestamp :=
    block_header.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.timestamp)
    in
  let? '_ :=
    Alpha_context.Block_header.begin_validate_block_header block_header chain_id
      predecessor_timestamp predecessor_round fitness timestamp
      block_producer_pk (Alpha_context.Constants.round_durations ctxt)
      (Alpha_context.Constants.proof_of_work_threshold ctxt)
      current_level.(Alpha_context.Level.t.expected_commitment) in
  let? frozen_deposits :=
    Alpha_context.Delegate.frozen_deposits ctxt block_producer in
  let? '_ :=
    Error_monad.fail_unless
      (Alpha_context.Tez.op_gt frozen_deposits Alpha_context.Tez.zero)
      (Build_extensible "Zero_frozen_deposits" Alpha_context.public_key_hash
        block_producer) in
  let? '(ctxt, _slot, (payload_producer_pk, _payload_producer)) :=
    Alpha_context.Stake_distribution.baking_rights_owner ctxt current_level
      block_header.(Alpha_context.Block_header.t.protocol_data).(Alpha_context.Block_header.protocol_data.contents).(Alpha_context.Block_header.contents.payload_round)
    in
  let? ctxt :=
    init_allowed_consensus_operations ctxt predecessor_level current_level in
  let escape_vote :=
    block_header.(Alpha_context.Block_header.t.protocol_data).(Alpha_context.Block_header.protocol_data.contents).(Alpha_context.Block_header.contents.liquidity_baking_escape_vote)
    in
  let?
    '(ctxt, liquidity_baking_operations_results, liquidity_baking_escape_ema) :=
    apply_liquidity_baking_subsidy ctxt escape_vote in
  return?
    (ctxt, payload_producer_pk, block_producer,
      liquidity_baking_operations_results, liquidity_baking_escape_ema).

(** Records for the constructor parameters *)
Module ConstructorRecords_finalize_application_mode.
  Module finalize_application_mode.
    Module Finalize_full_construction.
      Record record {level predecessor_round : Set} : Set := Build {
        level : level;
        predecessor_round : predecessor_round }.
      Arguments record : clear implicits.
      Definition with_level {t_level t_predecessor_round} level
        (r : record t_level t_predecessor_round) :=
        Build t_level t_predecessor_round level r.(predecessor_round).
      Definition with_predecessor_round {t_level t_predecessor_round}
        predecessor_round (r : record t_level t_predecessor_round) :=
        Build t_level t_predecessor_round r.(level) predecessor_round.
    End Finalize_full_construction.
    Definition Finalize_full_construction_skeleton :=
      Finalize_full_construction.record.
  End finalize_application_mode.
End ConstructorRecords_finalize_application_mode.
Import ConstructorRecords_finalize_application_mode.

Reserved Notation "'finalize_application_mode.Finalize_full_construction".

Inductive finalize_application_mode : Set :=
| Finalize_full_construction :
  'finalize_application_mode.Finalize_full_construction ->
  finalize_application_mode
| Finalize_application : Alpha_context.Fitness.t -> finalize_application_mode

where "'finalize_application_mode.Finalize_full_construction" :=
  (finalize_application_mode.Finalize_full_construction_skeleton
    Alpha_context.Raw_level.t Alpha_context.Round.t).

Module finalize_application_mode.
  Include ConstructorRecords_finalize_application_mode.finalize_application_mode.
  Definition Finalize_full_construction :=
    'finalize_application_mode.Finalize_full_construction.
End finalize_application_mode.

Definition compute_payload_hash
  (ctxt : Alpha_context.t) (predecessor : Block_hash.t)
  (payload_round : Alpha_context.Round.t) : Block_payload_hash.t :=
  let non_consensus_operations := Alpha_context.non_consensus_operations ctxt in
  let operations_hash := Operation_list_hash.compute non_consensus_operations in
  Alpha_context.Block_payload.hash_value predecessor payload_round
    operations_hash.

Definition are_endorsements_required
  (ctxt : Alpha_context.context) (level : Alpha_context.Raw_level.raw_level)
  : M? bool :=
  let? first_Tenderbake_level :=
    Alpha_context.First_level_of_tenderbake.get ctxt in
  let tenderbake_level_position :=
    Alpha_context.Raw_level.diff_value level first_Tenderbake_level in
  return? (tenderbake_level_position >i32 1).

Definition check_minimum_endorsements (endorsing_power : int) (minimum : int)
  : M? unit :=
  Error_monad.fail_when (endorsing_power <i minimum)
    (Build_extensible "Not_enough_endorsements" Not_enough_endorsements
      {| Not_enough_endorsements.required := minimum;
        Not_enough_endorsements.endorsements := endorsing_power |}).

Definition finalize_application_check_validity
  (ctxt : Alpha_context.t) (mode : finalize_application_mode)
  (protocol_data : Alpha_context.Block_header.contents)
  (round : Alpha_context.Round.t) (predecessor : Block_hash.t)
  (endorsing_power : int) (consensus_threshold : int)
  (required_endorsements : bool)
  : M? (Alpha_context.Fitness.t * Block_payload_hash.t) :=
  let? '_ :=
    if required_endorsements then
      check_minimum_endorsements endorsing_power consensus_threshold
    else
      Error_monad.return_unit in
  let block_payload_hash :=
    compute_payload_hash ctxt predecessor
      protocol_data.(Alpha_context.Block_header.contents.payload_round) in
  let locked_round_evidence :=
    Option.map
      (fun (function_parameter : Alpha_context.Round.t * int) =>
        let '(preendorsement_round, preendorsement_count) := function_parameter
          in
        {|
          Alpha_context.Block_header.locked_round_evidence.preendorsement_round
            := preendorsement_round;
          Alpha_context.Block_header.locked_round_evidence.preendorsement_count
            := preendorsement_count |})
      (Alpha_context.Consensus.locked_round_evidence ctxt) in
  let? fitness :=
    match mode with
    | Finalize_application fitness => return? fitness
    |
      Finalize_full_construction {|
        finalize_application_mode.Finalize_full_construction.level := level;
          finalize_application_mode.Finalize_full_construction.predecessor_round
            := predecessor_round
          |} =>
      let locked_round :=
        match locked_round_evidence with
        | None => None
        |
          Some {|
            Alpha_context.Block_header.locked_round_evidence.preendorsement_round :=
              preendorsement_round
              |} => Some preendorsement_round
        end in
      Alpha_context.Fitness.create level locked_round predecessor_round round
    end in
  let checkable_payload_hash :=
    match mode with
    | Finalize_application _ =>
      Alpha_context.Block_header.Expected_payload_hash block_payload_hash
    | Finalize_full_construction _ =>
      match locked_round_evidence with
      | Some _ =>
        Alpha_context.Block_header.Expected_payload_hash block_payload_hash
      | None => Alpha_context.Block_header.No_check
      end
    end in
  let? '_ :=
    Alpha_context.Block_header.finalize_validate_block_header protocol_data
      round fitness checkable_payload_hash locked_round_evidence
      consensus_threshold in
  return? (fitness, block_payload_hash).

Definition record_endorsing_participation (ctxt : Alpha_context.context)
  : M? Alpha_context.context :=
  let validators := Alpha_context.Consensus.allowed_endorsements ctxt in
  Alpha_context.Slot.Map.(Map.S.fold_es)
    (fun (initial_slot : Alpha_context.Slot.t) =>
      fun (function_parameter :
        Signature.public_key * Signature.public_key_hash * int) =>
        let '(_delegate_pk, delegate, power) := function_parameter in
        fun (ctxt : Alpha_context.context) =>
          let participation :=
            if
              Alpha_context.Slot._Set.(_Set.S.mem) initial_slot
                (Alpha_context.Consensus.endorsements_seen ctxt)
            then
              Alpha_context.Delegate.Participated
            else
              Alpha_context.Delegate.Didn't_participate in
          Alpha_context.Delegate.record_endorsing_participation ctxt delegate
            participation power) validators ctxt.

Definition finalize_application
  (ctxt : Alpha_context.context) (mode : finalize_application_mode)
  (protocol_data : Alpha_context.Block_header.contents)
  (payload_producer : Signature.public_key_hash)
  (block_producer : Alpha_context.public_key_hash)
  (liquidity_baking_escape_ema : Alpha_context.Liquidity_baking.escape_ema)
  (implicit_operations_results :
    list Apply_results.packed_successful_manager_operation_result)
  (round : Alpha_context.Round.t) (predecessor : Block_hash.t)
  (migration_balance_updates :
    list
      (Alpha_context.Receipt.balance * Alpha_context.Receipt.balance_update *
        Alpha_context.Receipt.update_origin))
  : M?
    (Alpha_context.context * Alpha_context.Fitness.t *
      Apply_results.block_metadata) :=
  let level := Alpha_context.Level.current ctxt in
  let block_endorsing_power :=
    Alpha_context.Consensus.current_endorsement_power ctxt in
  let consensus_threshold := Alpha_context.Constants.consensus_threshold ctxt in
  let? required_endorsements :=
    are_endorsements_required ctxt level.(Alpha_context.Level.t.level) in
  let? '(fitness, block_payload_hash) :=
    finalize_application_check_validity ctxt mode protocol_data round
      predecessor block_endorsing_power consensus_threshold
      required_endorsements in
  let? ctxt :=
    match Alpha_context.Consensus.endorsement_branch ctxt with
    | Some predecessor_branch =>
      Error_monad.op_gtgteq
        (Alpha_context.Consensus.store_grand_parent_branch ctxt
          predecessor_branch) Error_monad._return
    | None => return? ctxt
    end in
  let ctxt :=
    Alpha_context.Consensus.store_endorsement_branch ctxt
      (predecessor, block_payload_hash) in
  let? ctxt := Alpha_context.Round.update ctxt round in
  let? ctxt :=
    match protocol_data.(Alpha_context.Block_header.contents.seed_nonce_hash)
      with
    | None => return? ctxt
    | Some nonce_hash =>
      Alpha_context.Nonce.record_hash ctxt
        {| Storage.Cycle.unrevealed_nonce.nonce_hash := nonce_hash;
          Storage.Cycle.unrevealed_nonce.delegate := block_producer |}
    end in
  let? ctxt := record_endorsing_participation ctxt in
  let baking_reward := Alpha_context.Constants.baking_reward_fixed_portion ctxt
    in
  let? reward_bonus :=
    if required_endorsements then
      Error_monad.op_gtgtquestion
        (Baking.bonus_baking_reward ctxt block_endorsing_power)
        Result.return_some
    else
      Result.return_none in
  let? '(ctxt, baking_receipts) :=
    Alpha_context.Delegate.record_baking_activity_and_pay_rewards_and_fees ctxt
      payload_producer block_producer baking_reward reward_bonus in
  let? ctxt :=
    if Alpha_context.Level.may_snapshot_rolls ctxt then
      Alpha_context.Stake_distribution.snapshot_value ctxt
    else
      return? ctxt in
  let? '(ctxt, cycle_end_balance_updates, deactivated) :=
    may_start_new_cycle ctxt in
  let? ctxt := Amendment.may_start_new_voting_period ctxt in
  let balance_updates :=
    Pervasives.op_at migration_balance_updates
      (Pervasives.op_at baking_receipts cycle_end_balance_updates) in
  let consumed_gas :=
    Alpha_context.Gas.Arith.sub
      (Alpha_context.Gas.Arith.fp_value
        (Alpha_context.Constants.hard_gas_limit_per_block ctxt))
      (Alpha_context.Gas.block_level ctxt) in
  let? voting_period_info :=
    Alpha_context.Voting_period.get_rpc_current_info ctxt in
  let receipt :=
    {| Apply_results.block_metadata.proposer := payload_producer;
      Apply_results.block_metadata.baker := block_producer;
      Apply_results.block_metadata.level_info := level;
      Apply_results.block_metadata.voting_period_info := voting_period_info;
      Apply_results.block_metadata.nonce_hash :=
        protocol_data.(Alpha_context.Block_header.contents.seed_nonce_hash);
      Apply_results.block_metadata.consumed_gas := consumed_gas;
      Apply_results.block_metadata.deactivated := deactivated;
      Apply_results.block_metadata.balance_updates := balance_updates;
      Apply_results.block_metadata.liquidity_baking_escape_ema :=
        liquidity_baking_escape_ema;
      Apply_results.block_metadata.implicit_operations_results :=
        implicit_operations_results |} in
  return? (ctxt, fitness, receipt).

Definition value_of_key
  (ctxt : Alpha_context.context) (k_value : Context.cache_key)
  : M? Context.cache_value :=
  Alpha_context.Cache.Admin.value_of_key ctxt k_value.
