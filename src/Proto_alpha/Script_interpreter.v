Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Cache_memory_helpers.
Require TezosOfOCaml.Proto_alpha.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Gas_monad.
Require TezosOfOCaml.Proto_alpha.Local_gas_counter.
Require TezosOfOCaml.Proto_alpha.Saturation_repr.
Require TezosOfOCaml.Proto_alpha.Script_comparable.
Require TezosOfOCaml.Proto_alpha.Script_interpreter_defs.
Require TezosOfOCaml.Proto_alpha.Script_ir_annot.
Require TezosOfOCaml.Proto_alpha.Script_ir_translator_mli.
  Module Script_ir_translator := Script_ir_translator_mli.
Require TezosOfOCaml.Proto_alpha.Script_list.
Require TezosOfOCaml.Proto_alpha.Script_map.
Require TezosOfOCaml.Proto_alpha.Script_set.
Require TezosOfOCaml.Proto_alpha.Script_tc_errors.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.

Module S := Saturation_repr.

Definition step_constants : Set := Script_typed_ir.step_constants.

(** Init function; without side-effects in Coq *)
Definition init_module : unit :=
  let trace_encoding :=
    (let arg := Data_encoding.list_value in
    fun (eta :
      Data_encoding.encoding
        (Alpha_context.Script.location * Alpha_context.Gas.t *
          list (Alpha_context.Script.expr * option string))) => arg None eta)
      (Data_encoding.obj3
        (Data_encoding.req None None "location"
          Alpha_context.Script.location_encoding)
        (Data_encoding.req None None "gas" Alpha_context.Gas.encoding)
        (Data_encoding.req None None "stack"
          (Data_encoding.list_value None
            (Data_encoding.obj2
              (Data_encoding.req None None "item"
                Alpha_context.Script.expr_encoding)
              (Data_encoding.opt None None "annot" Data_encoding.string_value)))))
    in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "michelson_v1.script_rejected" "Script failed"
      "A FAILWITH instruction was reached" None
      (Data_encoding.obj3
        (Data_encoding.req None None "location"
          Alpha_context.Script.location_encoding)
        (Data_encoding.req None None "with" Alpha_context.Script.expr_encoding)
        (Data_encoding.opt None None "trace" trace_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Reject" then
            let '(loc, v_value, trace_value) :=
              cast
                (Alpha_context.Script.location * Alpha_context.Script.expr *
                  option Script_typed_ir.execution_trace) payload in
            Some (loc, v_value, trace_value)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location * Alpha_context.Script.expr *
          option Script_typed_ir.execution_trace) =>
        let '(loc, v_value, trace_value) := function_parameter in
        Build_extensible "Reject"
          (Alpha_context.Script.location * Alpha_context.Script.expr *
            option Script_typed_ir.execution_trace) (loc, v_value, trace_value))
    in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "michelson_v1.script_overflow" "Script failed (overflow error)"
      "A FAIL instruction was reached due to the detection of an overflow" None
      (Data_encoding.obj2
        (Data_encoding.req None None "location"
          Alpha_context.Script.location_encoding)
        (Data_encoding.opt None None "trace" trace_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Overflow" then
            let '(loc, trace_value) :=
              cast
                (Alpha_context.Script.location *
                  option Script_typed_ir.execution_trace) payload in
            Some (loc, trace_value)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location * option Script_typed_ir.execution_trace)
        =>
        let '(loc, trace_value) := function_parameter in
        Build_extensible "Overflow"
          (Alpha_context.Script.location *
            option Script_typed_ir.execution_trace) (loc, trace_value)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "michelson_v1.runtime_error" "Script runtime error"
      "Toplevel error for all runtime script errors" None
      (Data_encoding.obj2
        (Data_encoding.req None None "contract_handle"
          Alpha_context.Contract.encoding)
        (Data_encoding.req None None "contract_code"
          Alpha_context.Script.expr_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Runtime_contract_error" then
            let '(contract, expr) :=
              cast (Alpha_context.Contract.t * Alpha_context.Script.expr)
                payload in
            Some (contract, expr)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Contract.t * Alpha_context.Script.expr) =>
        let '(contract, expr) := function_parameter in
        Build_extensible "Runtime_contract_error"
          (Alpha_context.Contract.t * Alpha_context.Script.expr)
          (contract, expr)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.bad_contract_parameter"
      "Contract supplied an invalid parameter"
      "Either no parameter was supplied to a contract with a non-unit parameter type, a non-unit parameter was passed to an account, or a parameter was supplied of the wrong type"
      None
      (Data_encoding.obj1
        (Data_encoding.req None None "contract" Alpha_context.Contract.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Bad_contract_parameter" then
            let 'c_value := cast Alpha_context.Contract.t payload in
            Some c_value
          else None
        end)
      (fun (c_value : Alpha_context.Contract.t) =>
        Build_extensible "Bad_contract_parameter" Alpha_context.Contract.t
          c_value) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "michelson_v1.cannot_serialize_failure"
      "Not enough gas to serialize argument of FAILWITH"
      "Argument of FAILWITH was too big to be serialized with the provided gas"
      None Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Cannot_serialize_failure" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Cannot_serialize_failure" unit tt) in
  Error_monad.register_error_kind Error_monad.Temporary
    "michelson_v1.cannot_serialize_storage"
    "Not enough gas to serialize execution storage"
    "The returned storage was too big to be serialized with the provided gas"
    None Data_encoding.empty
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Cannot_serialize_storage" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Cannot_serialize_storage" unit tt).

Fixpoint kmap_exit {m n o a b g h : Set}
  (mk : Script_typed_ir.continuation -> Script_typed_ir.continuation)
  (g_value : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (function_parameter :
    Script_typed_ir.kinstr * list (m * n) * Script_typed_ir.map m o * m)
  {struct gas}
  : Script_typed_ir.continuation -> o -> a * b ->
  M?
    (g * h * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter) :=
  let '(body, xs, ys, yk) := function_parameter in
  fun (ks : Script_typed_ir.continuation) =>
    fun (accu_value : o) =>
      fun (stack_value : a * b) =>
        let ys := Script_map.update yk (Some accu_value) ys in
        let ks := mk (Script_typed_ir.KMap_enter_body body xs ys ks) in
        let '(accu_value, stack_value) := stack_value in
        next g_value gas ks accu_value stack_value

with kmap_enter {j k a b c d e : Set}
  (mk : Script_typed_ir.continuation -> Script_typed_ir.continuation)
  (g_value : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (function_parameter :
    Script_typed_ir.kinstr * list (j * k) * Script_typed_ir.map j a)
  {struct gas}
  : Script_typed_ir.continuation -> b -> c ->
  M?
    (d * e * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter) :=
  let '(body, xs, ys) := function_parameter in
  fun (ks : Script_typed_ir.continuation) =>
    fun (accu_value : b) =>
      fun (stack_value : c) =>
        match xs with
        | [] => next g_value gas ks ys (accu_value, stack_value)
        | cons (xk, xv) xs =>
          let ks := mk (Script_typed_ir.KMap_exit_body body xs ys xk ks) in
          let res := (xk, xv) in
          let stack_value := (accu_value, stack_value) in
          step g_value gas body ks res stack_value
        end

with klist_exit {i j a b c d : Set}
  (mk : Script_typed_ir.continuation -> Script_typed_ir.continuation)
  (g_value : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (function_parameter :
    Script_typed_ir.kinstr * list i * list j *
      Local_gas_counter.local_gas_counter) {struct gas}
  : Script_typed_ir.continuation -> j -> a * b ->
  M?
    (c * d * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter) :=
  let '(body, xs, ys, len) := function_parameter in
  fun (ks : Script_typed_ir.continuation) =>
    fun (accu_value : j) =>
      fun (stack_value : a * b) =>
        let ks :=
          mk
            (Script_typed_ir.KList_enter_body body xs (cons accu_value ys) len
              ks) in
        let '(accu_value, stack_value) := stack_value in
        next g_value gas ks accu_value stack_value

with klist_enter {j b a c d e : Set}
  (mk : Script_typed_ir.continuation -> Script_typed_ir.continuation)
  (g_value : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (function_parameter :
    Script_typed_ir.kinstr * list j * list b *
      Local_gas_counter.local_gas_counter) {struct gas}
  : Script_typed_ir.continuation -> a -> c ->
  M?
    (d * e * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter) :=
  let '(body, xs, ys, len) := function_parameter in
  fun (ks' : Script_typed_ir.continuation) =>
    fun (accu_value : a) =>
      fun (stack_value : c) =>
        match xs with
        | [] =>
          let ys :=
            {| Script_typed_ir.boxed_list.elements := List.rev ys;
              Script_typed_ir.boxed_list.length := len |} in
          next g_value gas ks' ys (accu_value, stack_value)
        | cons x_value xs =>
          let ks := mk (Script_typed_ir.KList_exit_body body xs ys len ks') in
          step g_value gas body ks x_value (accu_value, stack_value)
        end

with kloop_in_left {a b g e f : Set}
  (g_value : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (ks0 : Script_typed_ir.continuation) (ki : Script_typed_ir.kinstr)
  (ks' : Script_typed_ir.continuation) (accu_value : Script_typed_ir.union a b)
  (stack_value : g) {struct gas}
  : M?
    (e * f * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter) :=
  match accu_value with
  | Script_typed_ir.L v_value => step g_value gas ki ks0 v_value stack_value
  | Script_typed_ir.R v_value => next g_value gas ks' v_value stack_value
  end

with kloop_in {a s r f : Set}
  (g_value : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (ks0 : Script_typed_ir.continuation) (ki : Script_typed_ir.kinstr)
  (ks' : Script_typed_ir.continuation) (accu_value : bool) (stack_value : a * s)
  {struct gas}
  : M?
    (r * f * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter) :=
  let '(accu', stack') := stack_value in
  if accu_value then
    step g_value gas ki ks0 accu' stack'
  else
    next g_value gas ks' accu' stack'

with kiter {b a s r f : Set}
  (mk : Script_typed_ir.continuation -> Script_typed_ir.continuation)
  (g_value : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (function_parameter : Script_typed_ir.kinstr * list b) {struct gas}
  : Script_typed_ir.continuation -> a -> s ->
  M?
    (r * f * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter) :=
  let '(body, xs) := function_parameter in
  fun (ks : Script_typed_ir.continuation) =>
    fun (accu_value : a) =>
      fun (stack_value : s) =>
        match xs with
        | [] => next g_value gas ks accu_value stack_value
        | cons x_value xs =>
          let ks := mk (Script_typed_ir.KIter body xs ks) in
          step g_value gas body ks x_value (accu_value, stack_value)
        end

with next {a s r f : Set}
  (function_parameter :
    Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  {struct function_parameter}
  : Local_gas_counter.local_gas_counter -> Script_typed_ir.continuation -> a ->
  s ->
  M?
    (r * f * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter) :=
  let '(ctxt, _) as g_value := function_parameter in
  fun (gas : Local_gas_counter.local_gas_counter) =>
    fun (ks0 : Script_typed_ir.continuation) =>
      fun (accu_value : a) =>
        fun (stack_value : s) =>
          match Script_interpreter_defs.consume_control gas ks0 with
          | None =>
            Error_monad.fail
              (Build_extensible "Operation_quota_exceeded" unit tt)
          | Some gas =>
            match (ks0, accu_value, stack_value) with
            | (Script_typed_ir.KLog ks logger, _, _) =>
              let '[logger, ks] :=
                cast [Script_typed_ir.logger ** Script_typed_ir.continuation]
                  [logger, ks] in
              klog logger g_value gas ks0 ks accu_value stack_value
            
            | (Script_typed_ir.KNil, accu_value, stack_value) =>
              let '[stack_value, accu_value] :=
                cast [f ** r] [stack_value, accu_value] in
              Pervasives.Ok (accu_value, stack_value, ctxt, gas)
            
            | (Script_typed_ir.KCons k_value ks, _, _) =>
              let 'existT _ [__KCons_'b, __KCons_'t] [ks, k_value] :=
                cast_exists (Es := [Set ** Set])
                  (fun '[__KCons_'b, __KCons_'t] =>
                    [Script_typed_ir.continuation ** Script_typed_ir.kinstr])
                  [ks, k_value] in
              step g_value gas k_value ks accu_value stack_value
            
            | (Script_typed_ir.KLoop_in ki ks', accu_value, stack_value) =>
              let 'existT _ [__0, __1] [stack_value, accu_value, ks', ki] :=
                cast_exists (Es := [Set ** Set])
                  (fun '[__0, __1] =>
                    [__0 * __1 ** bool ** Script_typed_ir.continuation **
                      Script_typed_ir.kinstr])
                  [stack_value, accu_value, ks', ki] in
              kloop_in g_value gas ks0 ki ks' accu_value stack_value
            
            | (Script_typed_ir.KReturn stack' ks, _, _) =>
              let 'existT _ __KReturn_'s [ks, stack'] :=
                cast_exists (Es := Set)
                  (fun __KReturn_'s =>
                    [Script_typed_ir.continuation ** __KReturn_'s]) [ks, stack']
                in
              next g_value gas ks accu_value stack'
            
            | (Script_typed_ir.KMap_head f_value ks, _, _) =>
              let 'existT _ __KMap_head_'b [ks, f_value] :=
                cast_exists (Es := Set)
                  (fun __KMap_head_'b =>
                    [Script_typed_ir.continuation ** a -> __KMap_head_'b])
                  [ks, f_value] in
              next g_value gas ks (f_value accu_value) stack_value
            
            | (Script_typed_ir.KLoop_in_left ki ks', accu_value, _) =>
              let 'existT _ [__2, __3] [accu_value, ks', ki] :=
                cast_exists (Es := [Set ** Set])
                  (fun '[__2, __3] =>
                    [Script_typed_ir.union __2 __3 **
                      Script_typed_ir.continuation ** Script_typed_ir.kinstr])
                  [accu_value, ks', ki] in
              kloop_in_left g_value gas ks0 ki ks' accu_value stack_value
            
            | (Script_typed_ir.KUndip x_value ks, _, _) =>
              let 'existT _ __KUndip_'b [ks, x_value] :=
                cast_exists (Es := Set)
                  (fun __KUndip_'b =>
                    [Script_typed_ir.continuation ** __KUndip_'b]) [ks, x_value]
                in
              next g_value gas ks x_value (accu_value, stack_value)
            
            | (Script_typed_ir.KIter body xs ks, _, _) =>
              let 'existT _ __KIter_'a [ks, xs, body] :=
                cast_exists (Es := Set)
                  (fun __KIter_'a =>
                    [Script_typed_ir.continuation ** list __KIter_'a **
                      Script_typed_ir.kinstr]) [ks, xs, body] in
              let extra := (body, xs) in
              kiter Script_interpreter_defs.id g_value gas extra ks accu_value
                stack_value
            
            | (Script_typed_ir.KList_enter_body body xs ys len ks, _, _) =>
              let 'existT _ [__KList_enter_body_'b, __KList_enter_body_'a]
                [ks, len, ys, xs, body] :=
                cast_exists (Es := [Set ** Set])
                  (fun '[__KList_enter_body_'b, __KList_enter_body_'a] =>
                    [Script_typed_ir.continuation ** int **
                      list __KList_enter_body_'b ** list __KList_enter_body_'a
                      ** Script_typed_ir.kinstr]) [ks, len, ys, xs, body] in
              let extra := (body, xs, ys, len) in
              klist_enter Script_interpreter_defs.id g_value gas extra ks
                accu_value stack_value
            
            |
              (Script_typed_ir.KList_exit_body body xs ys len ks, _, stack_value)
              =>
              let 'existT _ [__4, __5, __KList_exit_body_'a]
                [stack_value, ks, len, ys, xs, body] :=
                cast_exists (Es := [Set ** Set ** Set])
                  (fun '[__4, __5, __KList_exit_body_'a] =>
                    [__4 * __5 ** Script_typed_ir.continuation ** int ** list a
                      ** list __KList_exit_body_'a ** Script_typed_ir.kinstr])
                  [stack_value, ks, len, ys, xs, body] in
              let extra := (body, xs, ys, len) in
              klist_exit Script_interpreter_defs.id g_value gas extra ks
                accu_value stack_value
            
            | (Script_typed_ir.KMap_enter_body body xs ys ks, _, _) =>
              let 'existT _
                [__KMap_enter_body_'a, __KMap_enter_body_'c,
                  __KMap_enter_body_'b] [ks, ys, xs, body] :=
                cast_exists (Es := [Set ** Set ** Set])
                  (fun
                    '[__KMap_enter_body_'a, __KMap_enter_body_'c,
                      __KMap_enter_body_'b] =>
                    [Script_typed_ir.continuation **
                      Script_typed_ir.map __KMap_enter_body_'a
                        __KMap_enter_body_'c **
                      list (__KMap_enter_body_'a * __KMap_enter_body_'b) **
                      Script_typed_ir.kinstr]) [ks, ys, xs, body] in
              let extra := (body, xs, ys) in
              kmap_enter Script_interpreter_defs.id g_value gas extra ks
                accu_value stack_value
            
            | (Script_typed_ir.KMap_exit_body body xs ys yk ks, _, stack_value)
              =>
              let 'existT _ [__6, __7, __KMap_exit_body_'a, __KMap_exit_body_'b]
                [stack_value, ks, yk, ys, xs, body] :=
                cast_exists (Es := [Set ** Set ** Set ** Set])
                  (fun '[__6, __7, __KMap_exit_body_'a, __KMap_exit_body_'b] =>
                    [__6 * __7 ** Script_typed_ir.continuation **
                      __KMap_exit_body_'a **
                      Script_typed_ir.map __KMap_exit_body_'a a **
                      list (__KMap_exit_body_'a * __KMap_exit_body_'b) **
                      Script_typed_ir.kinstr])
                  [stack_value, ks, yk, ys, xs, body] in
              let extra := (body, xs, ys, yk) in
              kmap_exit Script_interpreter_defs.id g_value gas extra ks
                accu_value stack_value
            
            | (Script_typed_ir.KView_exit orig_step_constants ks, _, _) =>
              let '[ks, orig_step_constants] :=
                cast
                  [Script_typed_ir.continuation **
                    Script_typed_ir.step_constants] [ks, orig_step_constants] in
              let g_value := ((Pervasives.fst g_value), orig_step_constants) in
              next g_value gas ks accu_value stack_value
            end
          end

with ilist_map {e a b c d f : Set}
  (log_if_needed : Script_typed_ir.continuation -> Script_typed_ir.continuation)
  (g_value : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (function_parameter : Script_typed_ir.kinstr * Script_typed_ir.kinstr)
  {struct gas}
  : Script_typed_ir.continuation -> Script_typed_ir.boxed_list e -> a * b ->
  M?
    (c * d * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter) :=
  let '(body, k_value) := function_parameter in
  fun (ks : Script_typed_ir.continuation) =>
    fun (accu_value : Script_typed_ir.boxed_list e) =>
      fun (stack_value : a * b) =>
        let xs := accu_value.(Script_typed_ir.boxed_list.elements) in
        let ys := (nil : list f) in
        let len := accu_value.(Script_typed_ir.boxed_list.length) in
        let ks :=
          log_if_needed
            (Script_typed_ir.KList_enter_body body xs ys len
              (Script_typed_ir.KCons k_value ks)) in
        let '(accu_value, stack_value) := stack_value in
        next g_value gas ks accu_value stack_value

with ilist_iter {e a b c d : Set}
  (log_if_needed : Script_typed_ir.continuation -> Script_typed_ir.continuation)
  (g_value : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (function_parameter : Script_typed_ir.kinstr * Script_typed_ir.kinstr)
  {struct gas}
  : Script_typed_ir.continuation -> Script_typed_ir.boxed_list e -> a * b ->
  M?
    (c * d * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter) :=
  let '(body, k_value) := function_parameter in
  fun (ks : Script_typed_ir.continuation) =>
    fun (accu_value : Script_typed_ir.boxed_list e) =>
      fun (stack_value : a * b) =>
        let xs := accu_value.(Script_typed_ir.boxed_list.elements) in
        let ks :=
          log_if_needed
            (Script_typed_ir.KIter body xs (Script_typed_ir.KCons k_value ks))
          in
        let '(accu_value, stack_value) := stack_value in
        next g_value gas ks accu_value stack_value

with iset_iter {e a b c d : Set}
  (log_if_needed : Script_typed_ir.continuation -> Script_typed_ir.continuation)
  (g_value : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (function_parameter : Script_typed_ir.kinstr * Script_typed_ir.kinstr)
  {struct gas}
  : Script_typed_ir.continuation -> Script_typed_ir.set e -> a * b ->
  M?
    (c * d * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter) :=
  let '(body, k_value) := function_parameter in
  fun (ks : Script_typed_ir.continuation) =>
    fun (accu_value : Script_typed_ir.set e) =>
      fun (stack_value : a * b) =>
        let set := accu_value in
        let l_value :=
          List.rev
            (Script_set.fold
              (fun (e_value : e) =>
                fun (acc_value : list e) => cons e_value acc_value) set nil) in
        let ks :=
          log_if_needed
            (Script_typed_ir.KIter body l_value
              (Script_typed_ir.KCons k_value ks)) in
        let '(accu_value, stack_value) := stack_value in
        next g_value gas ks accu_value stack_value

with imap_map {e f a b c d g : Set}
  (log_if_needed : Script_typed_ir.continuation -> Script_typed_ir.continuation)
  (g_value : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (function_parameter : Script_typed_ir.kinstr * Script_typed_ir.kinstr)
  {struct gas}
  : Script_typed_ir.continuation -> Script_typed_ir.map e f -> a * b ->
  M?
    (c * d * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter) :=
  let '(body, k_value) := function_parameter in
  fun (ks : Script_typed_ir.continuation) =>
    fun (accu_value : Script_typed_ir.map e f) =>
      fun (stack_value : a * b) =>
        let map := accu_value in
        let xs :=
          List.rev
            (Script_map.fold
              (fun (k_value : e) =>
                fun (v_value : f) =>
                  fun (a_value : list (e * f)) =>
                    cons (k_value, v_value) a_value) map nil) in
        let ys :=
          (Script_map.empty :
            Script_typed_ir.comparable_ty -> Script_typed_ir.map e g)
            (Script_map.key_ty map) in
        let ks :=
          log_if_needed
            (Script_typed_ir.KMap_enter_body body xs ys
              (Script_typed_ir.KCons k_value ks)) in
        let '(accu_value, stack_value) := stack_value in
        next g_value gas ks accu_value stack_value

with imap_iter {e f a b c d : Set}
  (log_if_needed : Script_typed_ir.continuation -> Script_typed_ir.continuation)
  (g_value : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (function_parameter : Script_typed_ir.kinstr * Script_typed_ir.kinstr)
  {struct gas}
  : Script_typed_ir.continuation -> Script_typed_ir.map e f -> a * b ->
  M?
    (c * d * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter) :=
  let '(body, k_value) := function_parameter in
  fun (ks : Script_typed_ir.continuation) =>
    fun (accu_value : Script_typed_ir.map e f) =>
      fun (stack_value : a * b) =>
        let map := accu_value in
        let l_value :=
          List.rev
            (Script_map.fold
              (fun (k_value : e) =>
                fun (v_value : f) =>
                  fun (a_value : list (e * f)) =>
                    cons (k_value, v_value) a_value) map nil) in
        let ks :=
          log_if_needed
            (Script_typed_ir.KIter body l_value
              (Script_typed_ir.KCons k_value ks)) in
        let '(accu_value, stack_value) := stack_value in
        next g_value gas ks accu_value stack_value

with imul_teznat {b e f : Set}
  (logger : option Script_typed_ir.logger)
  (g_value : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (function_parameter : Script_typed_ir.kinfo * Script_typed_ir.kinstr)
  {struct gas}
  : Script_typed_ir.continuation -> Alpha_context.Tez.t ->
  Alpha_context.Script_int.num * b ->
  M?
    (e * f * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter) :=
  let '(kinfo_value, k_value) := function_parameter in
  fun (ks : Script_typed_ir.continuation) =>
    fun (accu_value : Alpha_context.Tez.t) =>
      fun (stack_value : Alpha_context.Script_int.num * b) =>
        let x_value := accu_value in
        let '(y_value, stack_value) := stack_value in
        match Alpha_context.Script_int.to_int64 y_value with
        | None =>
          let? log := Script_interpreter_defs.get_log logger in
          Error_monad.fail
            (Build_extensible "Overflow"
              (Alpha_context.Script.location *
                option Script_typed_ir.execution_trace)
              (kinfo_value.(Script_typed_ir.kinfo.iloc), log))
        | Some y_value =>
          let? res := Alpha_context.Tez.op_starquestion x_value y_value in
          step g_value gas k_value ks res stack_value
        end

with imul_nattez {b e f : Set}
  (logger : option Script_typed_ir.logger)
  (g_value : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (function_parameter : Script_typed_ir.kinfo * Script_typed_ir.kinstr)
  {struct gas}
  : Script_typed_ir.continuation -> Alpha_context.Script_int.num ->
  Alpha_context.Tez.t * b ->
  M?
    (e * f * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter) :=
  let '(kinfo_value, k_value) := function_parameter in
  fun (ks : Script_typed_ir.continuation) =>
    fun (accu_value : Alpha_context.Script_int.num) =>
      fun (stack_value : Alpha_context.Tez.t * b) =>
        let y_value := accu_value in
        let '(x_value, stack_value) := stack_value in
        match Alpha_context.Script_int.to_int64 y_value with
        | None =>
          let? log := Script_interpreter_defs.get_log logger in
          Error_monad.fail
            (Build_extensible "Overflow"
              (Alpha_context.Script.location *
                option Script_typed_ir.execution_trace)
              (kinfo_value.(Script_typed_ir.kinfo.iloc), log))
        | Some y_value =>
          let? res := Alpha_context.Tez.op_starquestion x_value y_value in
          step g_value gas k_value ks res stack_value
        end

with ilsl_nat {b e f : Set}
  (logger : option Script_typed_ir.logger)
  (g_value : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (function_parameter : Script_typed_ir.kinfo * Script_typed_ir.kinstr)
  {struct gas}
  : Script_typed_ir.continuation -> Alpha_context.Script_int.num ->
  Alpha_context.Script_int.num * b ->
  M?
    (e * f * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter) :=
  let '(kinfo_value, k_value) := function_parameter in
  fun (ks : Script_typed_ir.continuation) =>
    fun (accu_value : Alpha_context.Script_int.num) =>
      fun (stack_value : Alpha_context.Script_int.num * b) =>
        let x_value := accu_value in
        let '(y_value, stack_value) := stack_value in
        match Alpha_context.Script_int.shift_left_n x_value y_value with
        | None =>
          let? log := Script_interpreter_defs.get_log logger in
          Error_monad.fail
            (Build_extensible "Overflow"
              (Alpha_context.Script.location *
                option Script_typed_ir.execution_trace)
              (kinfo_value.(Script_typed_ir.kinfo.iloc), log))
        | Some x_value => step g_value gas k_value ks x_value stack_value
        end

with ilsr_nat {b e f : Set}
  (logger : option Script_typed_ir.logger)
  (g_value : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (function_parameter : Script_typed_ir.kinfo * Script_typed_ir.kinstr)
  {struct gas}
  : Script_typed_ir.continuation -> Alpha_context.Script_int.num ->
  Alpha_context.Script_int.num * b ->
  M?
    (e * f * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter) :=
  let '(kinfo_value, k_value) := function_parameter in
  fun (ks : Script_typed_ir.continuation) =>
    fun (accu_value : Alpha_context.Script_int.num) =>
      fun (stack_value : Alpha_context.Script_int.num * b) =>
        let x_value := accu_value in
        let '(y_value, stack_value) := stack_value in
        match Alpha_context.Script_int.shift_right_n x_value y_value with
        | None =>
          let? log := Script_interpreter_defs.get_log logger in
          Error_monad.fail
            (Build_extensible "Overflow"
              (Alpha_context.Script.location *
                option Script_typed_ir.execution_trace)
              (kinfo_value.(Script_typed_ir.kinfo.iloc), log))
        | Some r_value => step g_value gas k_value ks r_value stack_value
        end

with ifailwith {a b : Set}
  (logger : option Script_typed_ir.logger)
  (function_parameter :
    Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  {struct function_parameter}
  : Local_gas_counter.local_gas_counter -> Alpha_context.Script.location ->
  Script_typed_ir.ty -> a -> M? b :=
  let '(ctxt, _) := function_parameter in
  fun (gas : Local_gas_counter.local_gas_counter) =>
    fun (kloc : Alpha_context.Script.location) =>
      fun (tv : Script_typed_ir.ty) =>
        fun (accu_value : a) =>
          let v_value := accu_value in
          let ctxt := Local_gas_counter.update_context gas ctxt in
          let? '(v_value, _ctxt) :=
            Error_monad.trace_value
              (Build_extensible "Cannot_serialize_failure" unit tt)
              (Script_ir_translator.unparse_data ctxt
                Script_ir_translator.Optimized tv v_value) in
          let v_value := Micheline.strip_locations v_value in
          let? log := Script_interpreter_defs.get_log logger in
          Error_monad.fail
            (Build_extensible "Reject"
              (Alpha_context.Script.location *
                Micheline.canonical Alpha_context.Script.prim *
                option Script_typed_ir.execution_trace) (kloc, v_value, log))

with iexec {g b e f : Set}
  (logger : option Script_typed_ir.logger)
  (g_value : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter) (k_value : Script_typed_ir.kinstr)
  (ks : Script_typed_ir.continuation) (accu_value : g)
  (stack_value : Script_typed_ir.lambda * b) {struct gas}
  : M?
    (e * f * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter) :=
  let arg := accu_value in
  let '(code, stack_value) := stack_value in
  let 'Script_typed_ir.Lam code _ := code in
  let code :=
    match logger with
    | None => code.(Script_typed_ir.kdescr.kinstr)
    | Some logger =>
      Script_interpreter_defs.log_kinstr logger
        code.(Script_typed_ir.kdescr.kinstr)
    end in
  let ks :=
    Script_typed_ir.KReturn stack_value (Script_typed_ir.KCons k_value ks) in
  step g_value gas code ks arg
    (Script_typed_ir.EmptyCell, Script_typed_ir.EmptyCell)

with step {a s r f : Set}
  (function_parameter :
    Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  {struct function_parameter}
  : Local_gas_counter.local_gas_counter -> Script_typed_ir.kinstr ->
  Script_typed_ir.continuation -> a -> s ->
  M?
    (r * f * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter) :=
  let '(ctxt, sc) as g_value := function_parameter in
  fun (gas : Local_gas_counter.local_gas_counter) =>
    fun (i_value : Script_typed_ir.kinstr) =>
      fun (ks : Script_typed_ir.continuation) =>
        fun (accu_value : a) =>
          fun (stack_value : s) =>
            match
              Script_interpreter_defs.consume_instr gas i_value accu_value
                stack_value with
            | None =>
              Error_monad.fail
                (Build_extensible "Operation_quota_exceeded" unit tt)
            | Some gas =>
              match (i_value, accu_value, stack_value) with
              | (Script_typed_ir.ILog _ event logger k_value, _, _) =>
                let '[k_value, logger, event] :=
                  cast
                    [Script_typed_ir.kinstr ** Script_typed_ir.logger **
                      Script_typed_ir.logging_event] [k_value, logger, event] in
                log (logger, event) g_value gas k_value ks accu_value
                  stack_value
              
              | (Script_typed_ir.IHalt _, _, _) =>
                next g_value gas ks accu_value stack_value
              
              | (Script_typed_ir.IDrop _ k_value, _, stack_value) =>
                let 'existT _ [__22, __23] [stack_value, k_value] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__22, __23] =>
                      [__22 * __23 ** Script_typed_ir.kinstr])
                    [stack_value, k_value] in
                let '(accu_value, stack_value) := stack_value in
                step g_value gas k_value ks accu_value stack_value
              
              | (Script_typed_ir.IDup _ k_value, _, _) =>
                let k_value := cast Script_typed_ir.kinstr k_value in
                step g_value gas k_value ks accu_value (accu_value, stack_value)
              
              | (Script_typed_ir.ISwap _ k_value, _, stack_value) =>
                let 'existT _ [__24, __25] [stack_value, k_value] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__24, __25] =>
                      [__24 * __25 ** Script_typed_ir.kinstr])
                    [stack_value, k_value] in
                let '(top, stack_value) := stack_value in
                step g_value gas k_value ks top (accu_value, stack_value)
              
              | (Script_typed_ir.IConst _ v_value k_value, _, _) =>
                let 'existT _ __IConst_'ty [k_value, v_value] :=
                  cast_exists (Es := Set)
                    (fun __IConst_'ty =>
                      [Script_typed_ir.kinstr ** __IConst_'ty])
                    [k_value, v_value] in
                step g_value gas k_value ks v_value (accu_value, stack_value)
              
              | (Script_typed_ir.ICons_some _ k_value, _, _) =>
                let k_value := cast Script_typed_ir.kinstr k_value in
                step g_value gas k_value ks (Some accu_value) stack_value
              
              | (Script_typed_ir.ICons_none _ k_value, _, _) =>
                let 'existT _ __ICons_none_'b k_value :=
                  cast_exists (Es := Set)
                    (fun __ICons_none_'b => Script_typed_ir.kinstr) k_value in
                step g_value gas k_value ks (None : option __ICons_none_'b)
                  (accu_value, stack_value)
              
              |
                (Script_typed_ir.IIf_none {|
                  Script_typed_ir.kinstr.IIf_none.branch_if_none := branch_if_none;
                    Script_typed_ir.kinstr.IIf_none.branch_if_some :=
                      branch_if_some;
                    Script_typed_ir.kinstr.IIf_none.k := k_value
                    |}, accu_value, stack_value) =>
                let 'existT _ [__27, __28, __26, __IIf_none_'c, __IIf_none_'t]
                  [stack_value, accu_value, k_value, branch_if_some,
                    branch_if_none] :=
                  cast_exists (Es := [Set ** Set ** Set ** Set ** Set])
                    (fun '[__27, __28, __26, __IIf_none_'c, __IIf_none_'t] =>
                      [__27 * __28 ** option __26 ** Script_typed_ir.kinstr **
                        Script_typed_ir.kinstr ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value, branch_if_some,
                      branch_if_none] in
                match accu_value with
                | None =>
                  let '(accu_value, stack_value) := stack_value in
                  step g_value gas branch_if_none
                    (Script_typed_ir.KCons k_value ks) accu_value stack_value
                | Some v_value =>
                  step g_value gas branch_if_some
                    (Script_typed_ir.KCons k_value ks) v_value stack_value
                end
              
              |
                (Script_typed_ir.IOpt_map {|
                  Script_typed_ir.kinstr.IOpt_map.kinfo := _;
                    Script_typed_ir.kinstr.IOpt_map.body := body;
                    Script_typed_ir.kinstr.IOpt_map.k := k_value
                    |}, accu_value, _) =>
                let 'existT _ [__29, __IOpt_map_'b] [accu_value, k_value, body]
                  :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__29, __IOpt_map_'b] =>
                      [option __29 ** Script_typed_ir.kinstr **
                        Script_typed_ir.kinstr]) [accu_value, k_value, body] in
                match accu_value with
                | None =>
                  step g_value gas k_value ks (None : option __IOpt_map_'b)
                    stack_value
                | Some v_value =>
                  let ks' :=
                    Script_typed_ir.KMap_head
                      (Option.some : __IOpt_map_'b -> option __IOpt_map_'b)
                      (Script_typed_ir.KCons k_value ks) in
                  step g_value gas body ks' v_value stack_value
                end
              
              | (Script_typed_ir.ICons_pair _ k_value, _, stack_value) =>
                let 'existT _ [__30, __31] [stack_value, k_value] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__30, __31] =>
                      [__30 * __31 ** Script_typed_ir.kinstr])
                    [stack_value, k_value] in
                let '(b_value, stack_value) := stack_value in
                step g_value gas k_value ks (accu_value, b_value) stack_value
              
              | (Script_typed_ir.IUnpair _ k_value, accu_value, _) =>
                let 'existT _ [__32, __33] [accu_value, k_value] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__32, __33] =>
                      [__32 * __33 ** Script_typed_ir.kinstr])
                    [accu_value, k_value] in
                let '(a_value, b_value) := accu_value in
                step g_value gas k_value ks a_value (b_value, stack_value)
              
              | (Script_typed_ir.ICar _ k_value, accu_value, _) =>
                let 'existT _ [__34, __35] [accu_value, k_value] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__34, __35] =>
                      [__34 * __35 ** Script_typed_ir.kinstr])
                    [accu_value, k_value] in
                let '(a_value, _) := accu_value in
                step g_value gas k_value ks a_value stack_value
              
              | (Script_typed_ir.ICdr _ k_value, accu_value, _) =>
                let 'existT _ [__36, __37] [accu_value, k_value] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__36, __37] =>
                      [__36 * __37 ** Script_typed_ir.kinstr])
                    [accu_value, k_value] in
                let '(_, b_value) := accu_value in
                step g_value gas k_value ks b_value stack_value
              
              | (Script_typed_ir.ICons_left _ k_value, _, _) =>
                let 'existT _ __ICons_left_'b k_value :=
                  cast_exists (Es := Set)
                    (fun __ICons_left_'b => Script_typed_ir.kinstr) k_value in
                step g_value gas k_value ks
                  ((Script_typed_ir.L accu_value) :
                    Script_typed_ir.union a __ICons_left_'b) stack_value
              
              | (Script_typed_ir.ICons_right _ k_value, _, _) =>
                let 'existT _ __ICons_right_'a k_value :=
                  cast_exists (Es := Set)
                    (fun __ICons_right_'a => Script_typed_ir.kinstr) k_value in
                step g_value gas k_value ks
                  ((Script_typed_ir.R accu_value) :
                    Script_typed_ir.union __ICons_right_'a a) stack_value
              
              |
                (Script_typed_ir.IIf_left {|
                  Script_typed_ir.kinstr.IIf_left.branch_if_left := branch_if_left;
                    Script_typed_ir.kinstr.IIf_left.branch_if_right :=
                      branch_if_right;
                    Script_typed_ir.kinstr.IIf_left.k := k_value
                    |}, accu_value, _) =>
                let 'existT _ [__38, __39, __IIf_left_'c, __IIf_left_'t]
                  [accu_value, k_value, branch_if_right, branch_if_left] :=
                  cast_exists (Es := [Set ** Set ** Set ** Set])
                    (fun '[__38, __39, __IIf_left_'c, __IIf_left_'t] =>
                      [Script_typed_ir.union __38 __39 ** Script_typed_ir.kinstr
                        ** Script_typed_ir.kinstr ** Script_typed_ir.kinstr])
                    [accu_value, k_value, branch_if_right, branch_if_left] in
                match accu_value with
                | Script_typed_ir.L v_value =>
                  step g_value gas branch_if_left
                    (Script_typed_ir.KCons k_value ks) v_value stack_value
                | Script_typed_ir.R v_value =>
                  step g_value gas branch_if_right
                    (Script_typed_ir.KCons k_value ks) v_value stack_value
                end
              
              | (Script_typed_ir.ICons_list _ k_value, _, stack_value) =>
                let 'existT _ __40 [stack_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __40 =>
                      [Script_typed_ir.boxed_list a * __40 **
                        Script_typed_ir.kinstr]) [stack_value, k_value] in
                let '(tl, stack_value) := stack_value in
                let accu_value := Script_list.cons_value accu_value tl in
                step g_value gas k_value ks accu_value stack_value
              
              | (Script_typed_ir.INil _ k_value, _, _) =>
                let 'existT _ __INil_'b k_value :=
                  cast_exists (Es := Set)
                    (fun __INil_'b => Script_typed_ir.kinstr) k_value in
                let stack_value := (accu_value, stack_value) in
                let accu_value {E : Set} : Script_typed_ir.boxed_list E :=
                  Script_list.empty in
                step g_value gas k_value ks (accu_value (E := __INil_'b))
                  stack_value
              
              |
                (Script_typed_ir.IIf_cons {|
                  Script_typed_ir.kinstr.IIf_cons.branch_if_cons := branch_if_cons;
                    Script_typed_ir.kinstr.IIf_cons.branch_if_nil :=
                      branch_if_nil;
                    Script_typed_ir.kinstr.IIf_cons.k := k_value
                    |}, accu_value, stack_value) =>
                let 'existT _ [__42, __43, __41, __IIf_cons_'c, __IIf_cons_'t]
                  [stack_value, accu_value, k_value, branch_if_nil,
                    branch_if_cons] :=
                  cast_exists (Es := [Set ** Set ** Set ** Set ** Set])
                    (fun '[__42, __43, __41, __IIf_cons_'c, __IIf_cons_'t] =>
                      [__42 * __43 ** Script_typed_ir.boxed_list __41 **
                        Script_typed_ir.kinstr ** Script_typed_ir.kinstr **
                        Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value, branch_if_nil,
                      branch_if_cons] in
                match accu_value.(Script_typed_ir.boxed_list.elements) with
                | [] =>
                  let '(accu_value, stack_value) := stack_value in
                  step g_value gas branch_if_nil
                    (Script_typed_ir.KCons k_value ks) accu_value stack_value
                | cons hd tl =>
                  let tl :=
                    {| Script_typed_ir.boxed_list.elements := tl;
                      Script_typed_ir.boxed_list.length :=
                        accu_value.(Script_typed_ir.boxed_list.length) -i 1 |}
                    in
                  step g_value gas branch_if_cons
                    (Script_typed_ir.KCons k_value ks) hd (tl, stack_value)
                end
              
              |
                (Script_typed_ir.IList_map _ body k_value, accu_value,
                  stack_value) =>
                let 'existT _ [__45, __46, __44, __IList_map_'b]
                  [stack_value, accu_value, k_value, body] :=
                  cast_exists (Es := [Set ** Set ** Set ** Set])
                    (fun '[__45, __46, __44, __IList_map_'b] =>
                      [__45 * __46 ** Script_typed_ir.boxed_list __44 **
                        Script_typed_ir.kinstr ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value, body] in
                (ilist_map (f := __IList_map_'b)) Script_interpreter_defs.id
                  g_value gas (body, k_value) ks accu_value stack_value
              
              | (Script_typed_ir.IList_size _ k_value, accu_value, _) =>
                let 'existT _ __47 [accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __47 =>
                      [Script_typed_ir.boxed_list __47 **
                        Script_typed_ir.kinstr]) [accu_value, k_value] in
                let list_value := accu_value in
                let len :=
                  Alpha_context.Script_int.abs
                    (Alpha_context.Script_int.of_int
                      list_value.(Script_typed_ir.boxed_list.length)) in
                step g_value gas k_value ks len stack_value
              
              |
                (Script_typed_ir.IList_iter _ body k_value, accu_value,
                  stack_value) =>
                let 'existT _ [__49, __50, __48]
                  [stack_value, accu_value, k_value, body] :=
                  cast_exists (Es := [Set ** Set ** Set])
                    (fun '[__49, __50, __48] =>
                      [__49 * __50 ** Script_typed_ir.boxed_list __48 **
                        Script_typed_ir.kinstr ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value, body] in
                ilist_iter Script_interpreter_defs.id g_value gas
                  (body, k_value) ks accu_value stack_value
              
              | (Script_typed_ir.IEmpty_set _ ty k_value, _, _) =>
                let 'existT _ __IEmpty_set_'b [k_value, ty] :=
                  cast_exists (Es := Set)
                    (fun __IEmpty_set_'b =>
                      [Script_typed_ir.kinstr ** Script_typed_ir.comparable_ty])
                    [k_value, ty] in
                let res :=
                  (Script_set.empty :
                    Script_typed_ir.comparable_ty ->
                    Script_typed_ir.set __IEmpty_set_'b) ty in
                let stack_value := (accu_value, stack_value) in
                step g_value gas k_value ks res stack_value
              
              |
                (Script_typed_ir.ISet_iter _ body k_value, accu_value,
                  stack_value) =>
                let 'existT _ [__52, __53, __51]
                  [stack_value, accu_value, k_value, body] :=
                  cast_exists (Es := [Set ** Set ** Set])
                    (fun '[__52, __53, __51] =>
                      [__52 * __53 ** Script_typed_ir.set __51 **
                        Script_typed_ir.kinstr ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value, body] in
                iset_iter Script_interpreter_defs.id g_value gas (body, k_value)
                  ks accu_value stack_value
              
              | (Script_typed_ir.ISet_mem _ k_value, _, stack_value) =>
                let 'existT _ __54 [stack_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __54 =>
                      [Script_typed_ir.set a * __54 ** Script_typed_ir.kinstr])
                    [stack_value, k_value] in
                let '(set, stack_value) := stack_value in
                let res := Script_set.mem accu_value set in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.ISet_update _ k_value, _, stack_value) =>
                let 'existT _ __55 [stack_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __55 =>
                      [bool * (Script_typed_ir.set a * __55) **
                        Script_typed_ir.kinstr]) [stack_value, k_value] in
                let '(presence, (set, stack_value)) := stack_value in
                let res := Script_set.update accu_value presence set in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.ISet_size _ k_value, accu_value, _) =>
                let 'existT _ __56 [accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __56 =>
                      [Script_typed_ir.set __56 ** Script_typed_ir.kinstr])
                    [accu_value, k_value] in
                let res := Script_set.size_value accu_value in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.IEmpty_map _ ty k_value, _, _) =>
                let 'existT _ [__IEmpty_map_'b, __IEmpty_map_'c] [k_value, ty]
                  :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__IEmpty_map_'b, __IEmpty_map_'c] =>
                      [Script_typed_ir.kinstr ** Script_typed_ir.comparable_ty])
                    [k_value, ty] in
                let stack_value := (accu_value, stack_value) in
                let res :=
                  (Script_map.empty :
                    Script_typed_ir.comparable_ty ->
                    Script_typed_ir.map __IEmpty_map_'b __IEmpty_map_'c) ty in
                step g_value gas k_value ks res stack_value
              
              |
                (Script_typed_ir.IMap_map _ body k_value, accu_value,
                  stack_value) =>
                let 'existT _ [__59, __60, __57, __58, __IMap_map_'c]
                  [stack_value, accu_value, k_value, body] :=
                  cast_exists (Es := [Set ** Set ** Set ** Set ** Set])
                    (fun '[__59, __60, __57, __58, __IMap_map_'c] =>
                      [__59 * __60 ** Script_typed_ir.map __57 __58 **
                        Script_typed_ir.kinstr ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value, body] in
                (imap_map (g := __IMap_map_'c)) Script_interpreter_defs.id
                  g_value gas (body, k_value) ks accu_value stack_value
              
              |
                (Script_typed_ir.IMap_iter _ body k_value, accu_value,
                  stack_value) =>
                let 'existT _ [__63, __64, __61, __62]
                  [stack_value, accu_value, k_value, body] :=
                  cast_exists (Es := [Set ** Set ** Set ** Set])
                    (fun '[__63, __64, __61, __62] =>
                      [__63 * __64 ** Script_typed_ir.map __61 __62 **
                        Script_typed_ir.kinstr ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value, body] in
                imap_iter Script_interpreter_defs.id g_value gas (body, k_value)
                  ks accu_value stack_value
              
              | (Script_typed_ir.IMap_mem _ k_value, _, stack_value) =>
                let 'existT _ [__65, __66] [stack_value, k_value] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__65, __66] =>
                      [Script_typed_ir.map a __65 * __66 **
                        Script_typed_ir.kinstr]) [stack_value, k_value] in
                let '(map, stack_value) := stack_value in
                let res := Script_map.mem accu_value map in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.IMap_get _ k_value, _, stack_value) =>
                let 'existT _ [__67, __68] [stack_value, k_value] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__67, __68] =>
                      [Script_typed_ir.map a __67 * __68 **
                        Script_typed_ir.kinstr]) [stack_value, k_value] in
                let '(map, stack_value) := stack_value in
                let res := Script_map.get accu_value map in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.IMap_update _ k_value, _, stack_value) =>
                let 'existT _ [__69, __70] [stack_value, k_value] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__69, __70] =>
                      [option __69 * (Script_typed_ir.map a __69 * __70) **
                        Script_typed_ir.kinstr]) [stack_value, k_value] in
                let '(v_value, (map, stack_value)) := stack_value in
                let key_value := accu_value in
                let res := Script_map.update key_value v_value map in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.IMap_get_and_update _ k_value, _, stack_value)
                =>
                let 'existT _ [__71, __72] [stack_value, k_value] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__71, __72] =>
                      [option __71 * (Script_typed_ir.map a __71 * __72) **
                        Script_typed_ir.kinstr]) [stack_value, k_value] in
                let key_value := accu_value in
                let '(v_value, (map, rest)) := stack_value in
                let map' := Script_map.update key_value v_value map in
                let v' := Script_map.get key_value map in
                step g_value gas k_value ks v' (map', rest)
              
              | (Script_typed_ir.IMap_size _ k_value, accu_value, _) =>
                let 'existT _ [__73, __74] [accu_value, k_value] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__73, __74] =>
                      [Script_typed_ir.map __73 __74 ** Script_typed_ir.kinstr])
                    [accu_value, k_value] in
                let res := Script_map.size_value accu_value in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.IEmpty_big_map _ tk tv k_value, _, _) =>
                let 'existT _ [__IEmpty_big_map_'b, __IEmpty_big_map_'c]
                  [k_value, tv, tk] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__IEmpty_big_map_'b, __IEmpty_big_map_'c] =>
                      [Script_typed_ir.kinstr ** Script_typed_ir.ty **
                        Script_typed_ir.comparable_ty]) [k_value, tv, tk] in
                let ebm :=
                  (Script_ir_translator.empty_big_map :
                    Script_typed_ir.comparable_ty -> Script_typed_ir.ty ->
                    Script_typed_ir.big_map __IEmpty_big_map_'b
                      __IEmpty_big_map_'c) tk tv in
                step g_value gas k_value ks ebm (accu_value, stack_value)
              
              | (Script_typed_ir.IBig_map_mem _ k_value, _, stack_value) =>
                let 'existT _ [__75, __76] [stack_value, k_value] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__75, __76] =>
                      [Script_typed_ir.big_map a __75 * __76 **
                        Script_typed_ir.kinstr]) [stack_value, k_value] in
                let '(map, stack_value) := stack_value in
                let key_value := accu_value in
                let? '(res, ctxt, gas) :=
                  Local_gas_counter.use_gas_counter_in_ctxt ctxt gas
                    (fun (ctxt : Alpha_context.context) =>
                      Script_ir_translator.big_map_mem ctxt key_value map) in
                step (ctxt, sc) gas k_value ks res stack_value
              
              | (Script_typed_ir.IBig_map_get _ k_value, _, stack_value) =>
                let 'existT _ [__77, __78] [stack_value, k_value] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__77, __78] =>
                      [Script_typed_ir.big_map a __77 * __78 **
                        Script_typed_ir.kinstr]) [stack_value, k_value] in
                let '(map, stack_value) := stack_value in
                let key_value := accu_value in
                let? '(res, ctxt, gas) :=
                  Local_gas_counter.use_gas_counter_in_ctxt ctxt gas
                    (fun (ctxt : Alpha_context.context) =>
                      Script_ir_translator.big_map_get ctxt key_value map) in
                step (ctxt, sc) gas k_value ks res stack_value
              
              | (Script_typed_ir.IBig_map_update _ k_value, _, stack_value) =>
                let 'existT _ [__79, __80] [stack_value, k_value] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__79, __80] =>
                      [option __79 * (Script_typed_ir.big_map a __79 * __80) **
                        Script_typed_ir.kinstr]) [stack_value, k_value] in
                let key_value := accu_value in
                let '(maybe_value, (map, stack_value)) := stack_value in
                let? '(big_map, ctxt, gas) :=
                  Local_gas_counter.use_gas_counter_in_ctxt ctxt gas
                    (fun (ctxt : Alpha_context.context) =>
                      Script_ir_translator.big_map_update ctxt key_value
                        maybe_value map) in
                step (ctxt, sc) gas k_value ks big_map stack_value
              
              |
                (Script_typed_ir.IBig_map_get_and_update _ k_value, _,
                  stack_value) =>
                let 'existT _ [__81, __82] [stack_value, k_value] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__81, __82] =>
                      [option __81 * (Script_typed_ir.big_map a __81 * __82) **
                        Script_typed_ir.kinstr]) [stack_value, k_value] in
                let key_value := accu_value in
                let '(v_value, (map, stack_value)) := stack_value in
                let? '((v', map'), ctxt, gas) :=
                  Local_gas_counter.use_gas_counter_in_ctxt ctxt gas
                    (fun (ctxt : Alpha_context.context) =>
                      Script_ir_translator.big_map_get_and_update ctxt key_value
                        v_value map) in
                step (ctxt, sc) gas k_value ks v' (map', stack_value)
              
              |
                (Script_typed_ir.IAdd_seconds_to_timestamp _ k_value,
                  accu_value, stack_value) =>
                let 'existT _ __83 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __83 =>
                      [Alpha_context.Script_timestamp.t * __83 **
                        Alpha_context.Script_int.num ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let n_value := accu_value in
                let '(t_value, stack_value) := stack_value in
                let result_value :=
                  Alpha_context.Script_timestamp.add_delta t_value n_value in
                step g_value gas k_value ks result_value stack_value
              
              |
                (Script_typed_ir.IAdd_timestamp_to_seconds _ k_value,
                  accu_value, stack_value) =>
                let 'existT _ __84 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __84 =>
                      [Alpha_context.Script_int.num * __84 **
                        Alpha_context.Script_timestamp.t **
                        Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let t_value := accu_value in
                let '(n_value, stack_value) := stack_value in
                let result_value :=
                  Alpha_context.Script_timestamp.add_delta t_value n_value in
                step g_value gas k_value ks result_value stack_value
              
              |
                (Script_typed_ir.ISub_timestamp_seconds _ k_value, accu_value,
                  stack_value) =>
                let 'existT _ __85 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __85 =>
                      [Alpha_context.Script_int.num * __85 **
                        Alpha_context.Script_timestamp.t **
                        Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let t_value := accu_value in
                let '(s_value, stack_value) := stack_value in
                let result_value :=
                  Alpha_context.Script_timestamp.sub_delta t_value s_value in
                step g_value gas k_value ks result_value stack_value
              
              |
                (Script_typed_ir.IDiff_timestamps _ k_value, accu_value,
                  stack_value) =>
                let 'existT _ __86 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __86 =>
                      [Alpha_context.Script_timestamp.t * __86 **
                        Alpha_context.Script_timestamp.t **
                        Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let t1 := accu_value in
                let '(t2, stack_value) := stack_value in
                let result_value :=
                  Alpha_context.Script_timestamp.diff_value t1 t2 in
                step g_value gas k_value ks result_value stack_value
              
              |
                (Script_typed_ir.IConcat_string_pair _ k_value, accu_value,
                  stack_value) =>
                let 'existT _ __87 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __87 =>
                      [Alpha_context.Script_string.t * __87 **
                        Alpha_context.Script_string.t ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let s_value :=
                  Alpha_context.Script_string.concat_pair x_value y_value in
                step g_value gas k_value ks s_value stack_value
              
              | (Script_typed_ir.IConcat_string _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast
                    [Script_typed_ir.boxed_list Alpha_context.Script_string.t **
                      Script_typed_ir.kinstr] [accu_value, k_value] in
                let ss := accu_value in
                let total_length :=
                  List.fold_left
                    (fun (acc_value : S.t) =>
                      fun (s_value : Alpha_context.Script_string.t) =>
                        S.add acc_value
                          (S.safe_int
                            (Alpha_context.Script_string.length s_value)))
                    S.zero ss.(Script_typed_ir.boxed_list.elements) in
                let? gas :=
                  Local_gas_counter.consume gas
                    (Script_interpreter_defs.Interp_costs.concat_string
                      total_length) in
                let s_value :=
                  Alpha_context.Script_string.concat
                    ss.(Script_typed_ir.boxed_list.elements) in
                step g_value gas k_value ks s_value stack_value
              
              |
                (Script_typed_ir.ISlice_string _ k_value, accu_value,
                  stack_value) =>
                let 'existT _ __88 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __88 =>
                      [Alpha_context.Script_int.num *
                        (Alpha_context.Script_string.t * __88) **
                        Alpha_context.Script_int.num ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let offset := accu_value in
                let '(length, (s_value, stack_value)) := stack_value in
                let s_length :=
                  Z.of_int (Alpha_context.Script_string.length s_value) in
                let offset := Alpha_context.Script_int.to_zint offset in
                let length := Alpha_context.Script_int.to_zint length in
                if
                  (offset <Z s_length) && ((offset +Z length) <=Z s_length)
                then
                  let s_value :=
                    Alpha_context.Script_string.sub s_value (Z.to_int offset)
                      (Z.to_int length) in
                  step g_value gas k_value ks (Some s_value) stack_value
                else
                  step g_value gas k_value ks
                    (None : option Alpha_context.Script_string.t) stack_value
              
              | (Script_typed_ir.IString_size _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [Alpha_context.Script_string.t ** Script_typed_ir.kinstr]
                    [accu_value, k_value] in
                let s_value := accu_value in
                let result_value :=
                  Alpha_context.Script_int.abs
                    (Alpha_context.Script_int.of_int
                      (Alpha_context.Script_string.length s_value)) in
                step g_value gas k_value ks result_value stack_value
              
              |
                (Script_typed_ir.IConcat_bytes_pair _ k_value, accu_value,
                  stack_value) =>
                let 'existT _ __89 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __89 =>
                      [bytes * __89 ** bytes ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let s_value := Bytes.cat x_value y_value in
                step g_value gas k_value ks s_value stack_value
              
              | (Script_typed_ir.IConcat_bytes _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast
                    [Script_typed_ir.boxed_list bytes ** Script_typed_ir.kinstr]
                    [accu_value, k_value] in
                let ss := accu_value in
                let total_length :=
                  List.fold_left
                    (fun (acc_value : S.t) =>
                      fun (s_value : bytes) =>
                        S.add acc_value (S.safe_int (Bytes.length s_value)))
                    S.zero ss.(Script_typed_ir.boxed_list.elements) in
                let? gas :=
                  Local_gas_counter.consume gas
                    (Script_interpreter_defs.Interp_costs.concat_string
                      total_length) in
                let s_value :=
                  Bytes.concat Bytes.empty
                    ss.(Script_typed_ir.boxed_list.elements) in
                step g_value gas k_value ks s_value stack_value
              
              |
                (Script_typed_ir.ISlice_bytes _ k_value, accu_value, stack_value)
                =>
                let 'existT _ __90 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __90 =>
                      [Alpha_context.Script_int.num * (bytes * __90) **
                        Alpha_context.Script_int.num ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let offset := accu_value in
                let '(length, (s_value, stack_value)) := stack_value in
                let s_length := Z.of_int (Bytes.length s_value) in
                let offset := Alpha_context.Script_int.to_zint offset in
                let length := Alpha_context.Script_int.to_zint length in
                if
                  (offset <Z s_length) && ((offset +Z length) <=Z s_length)
                then
                  let s_value :=
                    Bytes.sub s_value (Z.to_int offset) (Z.to_int length) in
                  step g_value gas k_value ks (Some s_value) stack_value
                else
                  step g_value gas k_value ks (None : option bytes) stack_value
              
              | (Script_typed_ir.IBytes_size _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [bytes ** Script_typed_ir.kinstr] [accu_value, k_value]
                  in
                let s_value := accu_value in
                let result_value :=
                  Alpha_context.Script_int.abs
                    (Alpha_context.Script_int.of_int (Bytes.length s_value)) in
                step g_value gas k_value ks result_value stack_value
              
              | (Script_typed_ir.IAdd_tez _ k_value, accu_value, stack_value) =>
                let 'existT _ __91 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __91 =>
                      [Alpha_context.Tez.t * __91 ** Alpha_context.Tez.t **
                        Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let? res := Alpha_context.Tez.op_plusquestion x_value y_value in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.ISub_tez _ k_value, accu_value, stack_value) =>
                let 'existT _ __92 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __92 =>
                      [Alpha_context.Tez.t * __92 ** Alpha_context.Tez.t **
                        Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let res := Alpha_context.Tez.sub_opt x_value y_value in
                step g_value gas k_value ks res stack_value
              
              |
                (Script_typed_ir.ISub_tez_legacy _ k_value, accu_value,
                  stack_value) =>
                let 'existT _ __93 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __93 =>
                      [Alpha_context.Tez.t * __93 ** Alpha_context.Tez.t **
                        Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let? res := Alpha_context.Tez.op_minusquestion x_value y_value
                  in
                step g_value gas k_value ks res stack_value
              
              |
                (Script_typed_ir.IMul_teznat kinfo_value k_value, accu_value,
                  stack_value) =>
                let 'existT _ __94
                  [stack_value, accu_value, k_value, kinfo_value] :=
                  cast_exists (Es := Set)
                    (fun __94 =>
                      [Alpha_context.Script_int.num * __94 **
                        Alpha_context.Tez.t ** Script_typed_ir.kinstr **
                        Script_typed_ir.kinfo])
                    [stack_value, accu_value, k_value, kinfo_value] in
                imul_teznat None g_value gas (kinfo_value, k_value) ks
                  accu_value stack_value
              
              |
                (Script_typed_ir.IMul_nattez kinfo_value k_value, accu_value,
                  stack_value) =>
                let 'existT _ __95
                  [stack_value, accu_value, k_value, kinfo_value] :=
                  cast_exists (Es := Set)
                    (fun __95 =>
                      [Alpha_context.Tez.t * __95 **
                        Alpha_context.Script_int.num ** Script_typed_ir.kinstr
                        ** Script_typed_ir.kinfo])
                    [stack_value, accu_value, k_value, kinfo_value] in
                imul_nattez None g_value gas (kinfo_value, k_value) ks
                  accu_value stack_value
              
              | (Script_typed_ir.IOr _ k_value, accu_value, stack_value) =>
                let 'existT _ __96 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __96 => [bool * __96 ** bool ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                step g_value gas k_value ks (x_value || y_value) stack_value
              
              | (Script_typed_ir.IAnd _ k_value, accu_value, stack_value) =>
                let 'existT _ __97 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __97 => [bool * __97 ** bool ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                step g_value gas k_value ks (x_value && y_value) stack_value
              
              | (Script_typed_ir.IXor _ k_value, accu_value, stack_value) =>
                let 'existT _ __98 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __98 => [bool * __98 ** bool ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let res := Compare.Bool.(Compare.S.op_ltgt) x_value y_value in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.INot _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [bool ** Script_typed_ir.kinstr] [accu_value, k_value] in
                let x_value := accu_value in
                step g_value gas k_value ks (Pervasives.not x_value) stack_value
              
              | (Script_typed_ir.IIs_nat _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [Alpha_context.Script_int.num ** Script_typed_ir.kinstr]
                    [accu_value, k_value] in
                let x_value := accu_value in
                let res := Alpha_context.Script_int.is_nat x_value in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.IAbs_int _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [Alpha_context.Script_int.num ** Script_typed_ir.kinstr]
                    [accu_value, k_value] in
                let x_value := accu_value in
                let res := Alpha_context.Script_int.abs x_value in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.IInt_nat _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [Alpha_context.Script_int.num ** Script_typed_ir.kinstr]
                    [accu_value, k_value] in
                let x_value := accu_value in
                let res := Alpha_context.Script_int.int_value x_value in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.INeg _ k_value, accu_value, _) =>
                let 'existT _ __99 [accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __99 =>
                      [Alpha_context.Script_int.num ** Script_typed_ir.kinstr])
                    [accu_value, k_value] in
                let x_value := accu_value in
                let res := Alpha_context.Script_int.neg x_value in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.IAdd_int _ k_value, accu_value, stack_value) =>
                let 'existT _ [__101, __102, __100]
                  [stack_value, accu_value, k_value] :=
                  cast_exists (Es := [Set ** Set ** Set])
                    (fun '[__101, __102, __100] =>
                      [Alpha_context.Script_int.num * __102 **
                        Alpha_context.Script_int.num ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let res := Alpha_context.Script_int.add x_value y_value in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.IAdd_nat _ k_value, accu_value, stack_value) =>
                let 'existT _ __103 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __103 =>
                      [Alpha_context.Script_int.num * __103 **
                        Alpha_context.Script_int.num ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let res := Alpha_context.Script_int.add_n x_value y_value in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.ISub_int _ k_value, accu_value, stack_value) =>
                let 'existT _ [__105, __106, __104]
                  [stack_value, accu_value, k_value] :=
                  cast_exists (Es := [Set ** Set ** Set])
                    (fun '[__105, __106, __104] =>
                      [Alpha_context.Script_int.num * __106 **
                        Alpha_context.Script_int.num ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let res := Alpha_context.Script_int.sub x_value y_value in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.IMul_int _ k_value, accu_value, stack_value) =>
                let 'existT _ [__108, __109, __107]
                  [stack_value, accu_value, k_value] :=
                  cast_exists (Es := [Set ** Set ** Set])
                    (fun '[__108, __109, __107] =>
                      [Alpha_context.Script_int.num * __109 **
                        Alpha_context.Script_int.num ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let res := Alpha_context.Script_int.mul x_value y_value in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.IMul_nat _ k_value, accu_value, stack_value) =>
                let 'existT _ [__110, __111] [stack_value, accu_value, k_value]
                  :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__110, __111] =>
                      [Alpha_context.Script_int.num * __111 **
                        Alpha_context.Script_int.num ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let res := Alpha_context.Script_int.mul_n x_value y_value in
                step g_value gas k_value ks res stack_value
              
              |
                (Script_typed_ir.IEdiv_teznat _ k_value, accu_value, stack_value)
                =>
                let 'existT _ __112 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __112 =>
                      [Alpha_context.Script_int.num * __112 **
                        Alpha_context.Tez.t ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let x_value :=
                  Alpha_context.Script_int.of_int64
                    (Alpha_context.Tez.to_mutez x_value) in
                let result_value :=
                  match Alpha_context.Script_int.ediv x_value y_value with
                  | None => None
                  | Some (q_value, r_value) =>
                    match
                      ((Alpha_context.Script_int.to_int64 q_value),
                        (Alpha_context.Script_int.to_int64 r_value)) with
                    | (Some q_value, Some r_value) =>
                      match
                        ((Alpha_context.Tez.of_mutez q_value),
                          (Alpha_context.Tez.of_mutez r_value)) with
                      | (Some q_value, Some r_value) => Some (q_value, r_value)
                      | _ =>
                        (* ❌ Assert instruction is not handled. *)
                        assert
                          (option
                            (Alpha_context.Tez.tez * Alpha_context.Tez.tez))
                          false
                      end
                    | _ =>
                      (* ❌ Assert instruction is not handled. *)
                      assert
                        (option (Alpha_context.Tez.tez * Alpha_context.Tez.tez))
                        false
                    end
                  end in
                step g_value gas k_value ks result_value stack_value
              
              | (Script_typed_ir.IEdiv_tez _ k_value, accu_value, stack_value)
                =>
                let 'existT _ __113 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __113 =>
                      [Alpha_context.Tez.t * __113 ** Alpha_context.Tez.t **
                        Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let x_value :=
                  Alpha_context.Script_int.abs
                    (Alpha_context.Script_int.of_int64
                      (Alpha_context.Tez.to_mutez x_value)) in
                let y_value :=
                  Alpha_context.Script_int.abs
                    (Alpha_context.Script_int.of_int64
                      (Alpha_context.Tez.to_mutez y_value)) in
                let result_value :=
                  match Alpha_context.Script_int.ediv_n x_value y_value with
                  | None => None
                  | Some (q_value, r_value) =>
                    match Alpha_context.Script_int.to_int64 r_value with
                    | None =>
                      (* ❌ Assert instruction is not handled. *)
                      assert
                        (option
                          (Alpha_context.Script_int.num * Alpha_context.Tez.tez))
                        false
                    | Some r_value =>
                      match Alpha_context.Tez.of_mutez r_value with
                      | None =>
                        (* ❌ Assert instruction is not handled. *)
                        assert
                          (option
                            (Alpha_context.Script_int.num *
                              Alpha_context.Tez.tez)) false
                      | Some r_value => Some (q_value, r_value)
                      end
                    end
                  end in
                step g_value gas k_value ks result_value stack_value
              
              | (Script_typed_ir.IEdiv_int _ k_value, accu_value, stack_value)
                =>
                let 'existT _ [__115, __116, __114]
                  [stack_value, accu_value, k_value] :=
                  cast_exists (Es := [Set ** Set ** Set])
                    (fun '[__115, __116, __114] =>
                      [Alpha_context.Script_int.num * __116 **
                        Alpha_context.Script_int.num ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let res := Alpha_context.Script_int.ediv x_value y_value in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.IEdiv_nat _ k_value, accu_value, stack_value)
                =>
                let 'existT _ [__117, __118] [stack_value, accu_value, k_value]
                  :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__117, __118] =>
                      [Alpha_context.Script_int.num * __118 **
                        Alpha_context.Script_int.num ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let res := Alpha_context.Script_int.ediv_n x_value y_value in
                step g_value gas k_value ks res stack_value
              
              |
                (Script_typed_ir.ILsl_nat kinfo_value k_value, accu_value,
                  stack_value) =>
                let 'existT _ __119
                  [stack_value, accu_value, k_value, kinfo_value] :=
                  cast_exists (Es := Set)
                    (fun __119 =>
                      [Alpha_context.Script_int.num * __119 **
                        Alpha_context.Script_int.num ** Script_typed_ir.kinstr
                        ** Script_typed_ir.kinfo])
                    [stack_value, accu_value, k_value, kinfo_value] in
                ilsl_nat None g_value gas (kinfo_value, k_value) ks accu_value
                  stack_value
              
              |
                (Script_typed_ir.ILsr_nat kinfo_value k_value, accu_value,
                  stack_value) =>
                let 'existT _ __120
                  [stack_value, accu_value, k_value, kinfo_value] :=
                  cast_exists (Es := Set)
                    (fun __120 =>
                      [Alpha_context.Script_int.num * __120 **
                        Alpha_context.Script_int.num ** Script_typed_ir.kinstr
                        ** Script_typed_ir.kinfo])
                    [stack_value, accu_value, k_value, kinfo_value] in
                ilsr_nat None g_value gas (kinfo_value, k_value) ks accu_value
                  stack_value
              
              | (Script_typed_ir.IOr_nat _ k_value, accu_value, stack_value) =>
                let 'existT _ __121 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __121 =>
                      [Alpha_context.Script_int.num * __121 **
                        Alpha_context.Script_int.num ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let res := Alpha_context.Script_int.logor x_value y_value in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.IAnd_nat _ k_value, accu_value, stack_value) =>
                let 'existT _ __122 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __122 =>
                      [Alpha_context.Script_int.num * __122 **
                        Alpha_context.Script_int.num ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let res := Alpha_context.Script_int.logand x_value y_value in
                step g_value gas k_value ks res stack_value
              
              |
                (Script_typed_ir.IAnd_int_nat _ k_value, accu_value, stack_value)
                =>
                let 'existT _ __123 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __123 =>
                      [Alpha_context.Script_int.num * __123 **
                        Alpha_context.Script_int.num ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let res := Alpha_context.Script_int.logand x_value y_value in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.IXor_nat _ k_value, accu_value, stack_value) =>
                let 'existT _ __124 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __124 =>
                      [Alpha_context.Script_int.num * __124 **
                        Alpha_context.Script_int.num ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let res := Alpha_context.Script_int.logxor x_value y_value in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.INot_int _ k_value, accu_value, _) =>
                let 'existT _ __125 [accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __125 =>
                      [Alpha_context.Script_int.num ** Script_typed_ir.kinstr])
                    [accu_value, k_value] in
                let x_value := accu_value in
                let res := Alpha_context.Script_int.lognot x_value in
                step g_value gas k_value ks res stack_value
              
              |
                (Script_typed_ir.IIf {|
                  Script_typed_ir.kinstr.IIf.branch_if_true := branch_if_true;
                    Script_typed_ir.kinstr.IIf.branch_if_false :=
                      branch_if_false;
                    Script_typed_ir.kinstr.IIf.k := k_value
                    |}, accu_value, stack_value) =>
                let 'existT _ [__126, __127, __IIf_'b, __IIf_'u]
                  [stack_value, accu_value, k_value, branch_if_false,
                    branch_if_true] :=
                  cast_exists (Es := [Set ** Set ** Set ** Set])
                    (fun '[__126, __127, __IIf_'b, __IIf_'u] =>
                      [__126 * __127 ** bool ** Script_typed_ir.kinstr **
                        Script_typed_ir.kinstr ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value, branch_if_false,
                      branch_if_true] in
                let '(res, stack_value) := stack_value in
                if accu_value then
                  step g_value gas branch_if_true
                    (Script_typed_ir.KCons k_value ks) res stack_value
                else
                  step g_value gas branch_if_false
                    (Script_typed_ir.KCons k_value ks) res stack_value
              
              | (Script_typed_ir.ILoop _ body k_value, _, _) =>
                let 'existT _ [__128, __129] [k_value, body] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__128, __129] =>
                      [Script_typed_ir.kinstr ** Script_typed_ir.kinstr])
                    [k_value, body] in
                let ks :=
                  Script_typed_ir.KLoop_in body
                    (Script_typed_ir.KCons k_value ks) in
                next g_value gas ks accu_value stack_value
              
              | (Script_typed_ir.ILoop_left _ bl br, _, _) =>
                let 'existT _ [__131, __130] [br, bl] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__131, __130] =>
                      [Script_typed_ir.kinstr ** Script_typed_ir.kinstr])
                    [br, bl] in
                let ks :=
                  Script_typed_ir.KLoop_in_left bl (Script_typed_ir.KCons br ks)
                  in
                next g_value gas ks accu_value stack_value
              
              | (Script_typed_ir.IDip _ b_value k_value, _, stack_value) =>
                let 'existT _ [__132, __133, __IDip_'c, __IDip_'t]
                  [stack_value, k_value, b_value] :=
                  cast_exists (Es := [Set ** Set ** Set ** Set])
                    (fun '[__132, __133, __IDip_'c, __IDip_'t] =>
                      [__132 * __133 ** Script_typed_ir.kinstr **
                        Script_typed_ir.kinstr]) [stack_value, k_value, b_value]
                  in
                let ign := accu_value in
                let ks :=
                  Script_typed_ir.KUndip ign (Script_typed_ir.KCons k_value ks)
                  in
                let '(accu_value, stack_value) := stack_value in
                step g_value gas b_value ks accu_value stack_value
              
              | (Script_typed_ir.IExec _ k_value, _, stack_value) =>
                let 'existT _ [__134, __135] [stack_value, k_value] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__134, __135] =>
                      [Script_typed_ir.lambda * __135 ** Script_typed_ir.kinstr])
                    [stack_value, k_value] in
                iexec None g_value gas k_value ks accu_value stack_value
              
              | (Script_typed_ir.IApply _ capture_ty k_value, _, stack_value) =>
                let 'existT _ [__136, __137, __138]
                  [stack_value, k_value, capture_ty] :=
                  cast_exists (Es := [Set ** Set ** Set])
                    (fun '[__136, __137, __138] =>
                      [Script_typed_ir.lambda * __138 ** Script_typed_ir.kinstr
                        ** Script_typed_ir.ty])
                    [stack_value, k_value, capture_ty] in
                let capture := accu_value in
                let '(lam, stack_value) := stack_value in
                let? '(lam', ctxt, gas) :=
                  Script_interpreter_defs.apply ctxt gas capture_ty capture lam
                  in
                step (ctxt, sc) gas k_value ks lam' stack_value
              
              | (Script_typed_ir.ILambda _ lam k_value, _, _) =>
                let 'existT _ [__ILambda_'b, __ILambda_'c] [k_value, lam] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__ILambda_'b, __ILambda_'c] =>
                      [Script_typed_ir.kinstr ** Script_typed_ir.lambda])
                    [k_value, lam] in
                step g_value gas k_value ks lam (accu_value, stack_value)
              
              | (Script_typed_ir.IFailwith _ kloc tv, _, _) =>
                let '[tv, kloc] :=
                  cast [Script_typed_ir.ty ** Alpha_context.Script.location]
                    [tv, kloc] in
                ifailwith None g_value gas kloc tv accu_value
              
              | (Script_typed_ir.ICompare _ ty k_value, _, stack_value) =>
                let 'existT _ __139 [stack_value, k_value, ty] :=
                  cast_exists (Es := Set)
                    (fun __139 =>
                      [a * __139 ** Script_typed_ir.kinstr **
                        Script_typed_ir.comparable_ty])
                    [stack_value, k_value, ty] in
                let a_value := accu_value in
                let '(b_value, stack_value) := stack_value in
                let r_value :=
                  Alpha_context.Script_int.of_int
                    (Script_comparable.compare_comparable ty a_value b_value) in
                step g_value gas k_value ks r_value stack_value
              
              | (Script_typed_ir.IEq _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [Alpha_context.Script_int.num ** Script_typed_ir.kinstr]
                    [accu_value, k_value] in
                let a_value := accu_value in
                let a_value :=
                  Alpha_context.Script_int.compare a_value
                    Alpha_context.Script_int.zero in
                let a_value := a_value =i 0 in
                step g_value gas k_value ks a_value stack_value
              
              | (Script_typed_ir.INeq _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [Alpha_context.Script_int.num ** Script_typed_ir.kinstr]
                    [accu_value, k_value] in
                let a_value := accu_value in
                let a_value :=
                  Alpha_context.Script_int.compare a_value
                    Alpha_context.Script_int.zero in
                let a_value := a_value <>i 0 in
                step g_value gas k_value ks a_value stack_value
              
              | (Script_typed_ir.ILt _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [Alpha_context.Script_int.num ** Script_typed_ir.kinstr]
                    [accu_value, k_value] in
                let a_value := accu_value in
                let a_value :=
                  Alpha_context.Script_int.compare a_value
                    Alpha_context.Script_int.zero in
                let a_value := a_value <i 0 in
                step g_value gas k_value ks a_value stack_value
              
              | (Script_typed_ir.ILe _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [Alpha_context.Script_int.num ** Script_typed_ir.kinstr]
                    [accu_value, k_value] in
                let a_value := accu_value in
                let a_value :=
                  Alpha_context.Script_int.compare a_value
                    Alpha_context.Script_int.zero in
                let a_value := a_value <=i 0 in
                step g_value gas k_value ks a_value stack_value
              
              | (Script_typed_ir.IGt _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [Alpha_context.Script_int.num ** Script_typed_ir.kinstr]
                    [accu_value, k_value] in
                let a_value := accu_value in
                let a_value :=
                  Alpha_context.Script_int.compare a_value
                    Alpha_context.Script_int.zero in
                let a_value := a_value >i 0 in
                step g_value gas k_value ks a_value stack_value
              
              | (Script_typed_ir.IGe _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [Alpha_context.Script_int.num ** Script_typed_ir.kinstr]
                    [accu_value, k_value] in
                let a_value := accu_value in
                let a_value :=
                  Alpha_context.Script_int.compare a_value
                    Alpha_context.Script_int.zero in
                let a_value := a_value >=i 0 in
                step g_value gas k_value ks a_value stack_value
              
              | (Script_typed_ir.IPack _ ty k_value, _, _) =>
                let '[k_value, ty] :=
                  cast [Script_typed_ir.kinstr ** Script_typed_ir.ty]
                    [k_value, ty] in
                let value_value := accu_value in
                let? '(bytes_value, ctxt, gas) :=
                  Local_gas_counter.use_gas_counter_in_ctxt ctxt gas
                    (fun (ctxt : Alpha_context.context) =>
                      Script_ir_translator.pack_data ctxt ty value_value) in
                step (ctxt, sc) gas k_value ks bytes_value stack_value
              
              | (Script_typed_ir.IUnpack _ ty k_value, accu_value, _) =>
                let 'existT _ __IUnpack_'a [accu_value, k_value, ty] :=
                  cast_exists (Es := Set)
                    (fun __IUnpack_'a =>
                      [bytes ** Script_typed_ir.kinstr ** Script_typed_ir.ty])
                    [accu_value, k_value, ty] in
                let bytes_value := accu_value in
                let? '(opt, ctxt, gas) :=
                  Local_gas_counter.use_gas_counter_in_ctxt ctxt gas
                    (fun (ctxt : Alpha_context.context) =>
                      (Script_interpreter_defs.unpack :
                        Alpha_context.context -> Script_typed_ir.ty -> bytes ->
                        M? (option __IUnpack_'a * Alpha_context.context)) ctxt
                        ty bytes_value) in
                step (ctxt, sc) gas k_value ks opt stack_value
              
              | (Script_typed_ir.IAddress _ k_value, accu_value, _) =>
                let 'existT _ __140 [accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __140 =>
                      [Script_typed_ir.ty * Script_typed_ir.address **
                        Script_typed_ir.kinstr]) [accu_value, k_value] in
                let '(_, address) := accu_value in
                step g_value gas k_value ks address stack_value
              
              |
                (Script_typed_ir.IContract kinfo_value t_value entrypoint
                  k_value, accu_value, _) =>
                let 'existT _ __IContract_'a
                  [accu_value, k_value, entrypoint, t_value, kinfo_value] :=
                  cast_exists (Es := Set)
                    (fun __IContract_'a =>
                      [Script_typed_ir.address ** Script_typed_ir.kinstr **
                        string ** Script_typed_ir.ty ** Script_typed_ir.kinfo])
                    [accu_value, k_value, entrypoint, t_value, kinfo_value] in
                let '(contract, contract_entrypoint) := accu_value in
                let entrypoint :=
                  if String.equal contract_entrypoint "default" then
                    Some entrypoint
                  else
                    if String.equal entrypoint "default" then
                      Some contract_entrypoint
                    else
                      None in
                match entrypoint with
                | Some entrypoint =>
                  let ctxt := Local_gas_counter.update_context gas ctxt in
                  let? '(ctxt, maybe_contract) :=
                    Script_ir_translator.parse_contract_for_script ctxt
                      kinfo_value.(Script_typed_ir.kinfo.iloc) t_value contract
                      entrypoint in
                  let gas := Local_gas_counter.update_local_gas_counter ctxt in
                  let ctxt := Local_gas_counter.outdated ctxt in
                  let accu_value := maybe_contract in
                  step (ctxt, sc) gas k_value ks accu_value stack_value
                | _ =>
                  step (ctxt, sc) gas k_value ks
                    (None : option Script_typed_ir.typed_contract) stack_value
                end
              
              | (Script_typed_ir.ITransfer_tokens _ k_value, _, stack_value) =>
                let 'existT _ __141 [stack_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __141 =>
                      [Alpha_context.Tez.t *
                        ((Script_typed_ir.ty *
                          (Alpha_context.Contract.t * string)) * __141) **
                        Script_typed_ir.kinstr]) [stack_value, k_value] in
                let p_value := accu_value in
                let '(amount, ((tp, (destination, entrypoint)), stack_value)) :=
                  stack_value in
                let? '(accu_value, ctxt, gas) :=
                  Script_interpreter_defs.transfer (ctxt, sc) gas amount tp
                    p_value destination entrypoint in
                step (ctxt, sc) gas k_value ks accu_value stack_value
              
              | (Script_typed_ir.IImplicit_account _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [Alpha_context.public_key_hash ** Script_typed_ir.kinstr]
                    [accu_value, k_value] in
                let key_value := accu_value in
                let contract :=
                  Alpha_context.Contract.implicit_contract key_value in
                let res :=
                  ((Script_typed_ir.unit_t None), (contract, "default")) in
                step g_value gas k_value ks res stack_value
              
              |
                (Script_typed_ir.IView _
                  (Script_typed_ir.View_signature {|
                    Script_typed_ir.view_signature.View_signature.name := name;
                      Script_typed_ir.view_signature.View_signature.input_ty :=
                        input_ty;
                      Script_typed_ir.view_signature.View_signature.output_ty :=
                        output_ty
                      |}) k_value, _, stack_value) =>
                let 'existT _ [__142, __IView_'b]
                  [stack_value, k_value, output_ty, input_ty, name] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__142, __IView_'b] =>
                      [(Alpha_context.Contract.t * string) * __142 **
                        Script_typed_ir.kinstr ** Script_typed_ir.ty **
                        Script_typed_ir.ty ** Alpha_context.Script_string.t])
                    [stack_value, k_value, output_ty, input_ty, name] in
                let input := accu_value in
                let '((c_value, _entrypoint_is_ignored), stack_value) :=
                  stack_value in
                let ctxt := Local_gas_counter.update_context gas ctxt in
                let? '(ctxt, script_opt) :=
                  Alpha_context.Contract.get_script ctxt c_value in
                let return_none (ctxt : Alpha_context.context)
                  : M?
                    (r * f * Local_gas_counter.outdated_context *
                      Local_gas_counter.local_gas_counter) :=
                  step ((Local_gas_counter.outdated ctxt), sc)
                    (Local_gas_counter.update_local_gas_counter ctxt) k_value ks
                    (None : option __IView_'b) stack_value in
                match script_opt with
                | None => return_none ctxt
                | Some script =>
                  let?
                    '(Script_ir_translator.Ex_script {|
                      Script_typed_ir.script.storage := storage_value;
                        Script_typed_ir.script.storage_type := storage_type;
                        Script_typed_ir.script.views := views
                        |}, ctxt) :=
                    Script_ir_translator.parse_script None ctxt true true script
                    in
                  let 'existT _ __Ex_script_'b
                    [ctxt, views, storage_type, storage_value] :=
                    cast_exists (Es := Set)
                      (fun __Ex_script_'b =>
                        [Alpha_context.context **
                          Script_typed_ir.SMap.(Map.S.t) Script_typed_ir.view **
                          Script_typed_ir.ty ** __Ex_script_'b])
                      [ctxt, views, storage_type, storage_value] in
                  let? ctxt :=
                    Alpha_context.Gas.consume ctxt
                      (Script_interpreter_defs.Interp_costs.view_get name views)
                    in
                  match Script_typed_ir.SMap.(Map.S.find) name views with
                  | None => return_none ctxt
                  | Some view =>
                    let view_result :=
                      (Script_ir_translator.parse_view_returning :
                        option Script_ir_translator.type_logger ->
                        Alpha_context.context -> bool -> Script_typed_ir.ty ->
                        Script_typed_ir.view ->
                        M?
                          (Script_ir_translator.ex_view __Ex_script_'b *
                            Alpha_context.context)) None ctxt true storage_type
                        view in
                    let? '(Script_ir_translator.Ex_view f_value, ctxt) :=
                      Error_monad.trace_eval
                        (fun (function_parameter : unit) =>
                          let '_ := function_parameter in
                          Build_extensible "Ill_typed_contract"
                            (Micheline.canonical Alpha_context.Script.prim *
                              Script_tc_errors.type_map)
                            ((Micheline.strip_locations
                              view.(Script_typed_ir.view.view_code)), nil))
                        view_result in
                    match f_value with
                    |
                      Script_typed_ir.Lam {|
                        Script_typed_ir.kdescr.kloc := kloc;
                          Script_typed_ir.kdescr.kbef :=
                            Script_typed_ir.Item_t bef_ty
                              Script_typed_ir.Bot_t
                              _;
                          Script_typed_ir.kdescr.kaft :=
                            Script_typed_ir.Item_t aft_ty
                              Script_typed_ir.Bot_t
                              _;
                          Script_typed_ir.kdescr.kinstr := kinstr
                          |} _script_view =>
                      let? pair_ty :=
                        Script_typed_ir.pair_t kloc (input_ty, None, None)
                          (storage_type, None, None) None in
                      let io_ty :=
                        Gas_monad.op_gtgtdollar
                          (Script_ir_translator.merge_types true
                            Script_ir_translator.Default_merge_type_error kloc
                            aft_ty output_ty)
                          (fun (function_parameter :
                            Script_ir_translator.eq * Script_typed_ir.ty) =>
                            let '(out_eq, _ty) := function_parameter in
                            Gas_monad.op_gtpipedollar
                              (Script_ir_translator.merge_types true
                                Script_ir_translator.Default_merge_type_error
                                kloc bef_ty pair_ty)
                              (fun (function_parameter :
                                Script_ir_translator.eq * Script_typed_ir.ty) =>
                                let '(in_eq, _ty) := function_parameter in
                                (out_eq, in_eq))) in
                      let? '(eq_value, ctxt) := Gas_monad.run ctxt io_ty in
                      match eq_value with
                      | Pervasives.Error _ => return_none ctxt
                      |
                        Pervasives.Ok
                          (Script_ir_translator.Eq, Script_ir_translator.Eq) =>
                        let kkinfo := Script_typed_ir.kinfo_of_kinstr k_value in
                        match kkinfo.(Script_typed_ir.kinfo.kstack_ty) with
                        | Script_typed_ir.Item_t _ s_value a_value =>
                          let kstack_ty :=
                            Script_typed_ir.Item_t output_ty s_value a_value in
                          let kkinfo :=
                            Script_typed_ir.kinfo.with_kstack_ty kstack_ty
                              kkinfo in
                          let ks :=
                            Script_typed_ir.KCons
                              (Script_typed_ir.ICons_some kkinfo k_value) ks in
                          step
                            ((Local_gas_counter.outdated ctxt),
                              (Script_typed_ir.step_constants.with_amount
                                Alpha_context.Tez.zero
                                (Script_typed_ir.step_constants.with_self
                                  c_value
                                  (Script_typed_ir.step_constants.with_source
                                    sc.(Script_typed_ir.step_constants.self) sc))))
                            (Local_gas_counter.update_local_gas_counter ctxt)
                            kinstr
                            (Script_typed_ir.KView_exit sc
                              (Script_typed_ir.KReturn stack_value ks))
                            (input, storage_value)
                            (Script_typed_ir.EmptyCell,
                              Script_typed_ir.EmptyCell)
                        | _ => unreachable_gadt_branch
                        end
                      end
                    | _ => unreachable_gadt_branch
                    end
                  end
                end
              
              |
                (Script_typed_ir.ICreate_contract {|
                  Script_typed_ir.kinstr.ICreate_contract.storage_type := storage_type;
                    Script_typed_ir.kinstr.ICreate_contract.arg_type := arg_type;
                    Script_typed_ir.kinstr.ICreate_contract.lambda :=
                      Script_typed_ir.Lam _ code;
                    Script_typed_ir.kinstr.ICreate_contract.views := views;
                    Script_typed_ir.kinstr.ICreate_contract.root_name :=
                      root_name;
                    Script_typed_ir.kinstr.ICreate_contract.k := k_value
                    |}, accu_value, stack_value) =>
                let 'existT _ [__143, __144, __ICreate_contract_'b]
                  [stack_value, accu_value, k_value, root_name, views, code,
                    arg_type, storage_type] :=
                  cast_exists (Es := [Set ** Set ** Set])
                    (fun '[__143, __144, __ICreate_contract_'b] =>
                      [Alpha_context.Tez.t * (__143 * __144) **
                        option Alpha_context.public_key_hash **
                        Script_typed_ir.kinstr **
                        option Script_ir_annot.field_annot **
                        Script_typed_ir.SMap.(Map.S.t) Script_typed_ir.view **
                        Alpha_context.Script.node ** Script_typed_ir.ty **
                        Script_typed_ir.ty])
                    [stack_value, accu_value, k_value, root_name, views, code,
                      arg_type, storage_type] in
                let delegate := accu_value in
                let '(credit, (init_value, stack_value)) := stack_value in
                let? '(res, contract, ctxt, gas) :=
                  Script_interpreter_defs.create_contract g_value gas
                    storage_type arg_type code views root_name delegate credit
                    init_value in
                let stack_value := ((contract, "default"), stack_value) in
                step (ctxt, sc) gas k_value ks res stack_value
              
              | (Script_typed_ir.ISet_delegate _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast
                    [option Alpha_context.public_key_hash **
                      Script_typed_ir.kinstr] [accu_value, k_value] in
                let delegate := accu_value in
                let operation := Alpha_context.Delegation delegate in
                let ctxt := Local_gas_counter.update_context gas ctxt in
                let? '(ctxt, nonce_value) :=
                  Alpha_context.fresh_internal_nonce ctxt in
                let res :=
                  ((Alpha_context.Internal_operation
                    {|
                      Alpha_context.internal_operation.source :=
                        sc.(Script_typed_ir.step_constants.self);
                      Alpha_context.internal_operation.operation := operation;
                      Alpha_context.internal_operation.nonce := nonce_value |}),
                    (None : option Alpha_context.Lazy_storage.diffs)) in
                let gas := Local_gas_counter.update_local_gas_counter ctxt in
                let ctxt := Local_gas_counter.outdated ctxt in
                step (ctxt, sc) gas k_value ks res stack_value
              
              | (Script_typed_ir.IBalance _ k_value, _, _) =>
                let k_value := cast Script_typed_ir.kinstr k_value in
                let ctxt := Local_gas_counter.update_context gas ctxt in
                let? '(ctxt, balance) :=
                  Alpha_context.Contract.get_balance_carbonated ctxt
                    sc.(Script_typed_ir.step_constants.self) in
                let gas := Local_gas_counter.update_local_gas_counter ctxt in
                let ctxt := Local_gas_counter.outdated ctxt in
                let g_value := (ctxt, sc) in
                step g_value gas k_value ks balance (accu_value, stack_value)
              
              | (Script_typed_ir.ILevel _ k_value, _, _) =>
                let k_value := cast Script_typed_ir.kinstr k_value in
                step g_value gas k_value ks
                  sc.(Script_typed_ir.step_constants.level)
                  (accu_value, stack_value)
              
              | (Script_typed_ir.INow _ k_value, _, _) =>
                let k_value := cast Script_typed_ir.kinstr k_value in
                step g_value gas k_value ks
                  sc.(Script_typed_ir.step_constants.now)
                  (accu_value, stack_value)
              
              |
                (Script_typed_ir.ICheck_signature _ k_value, accu_value,
                  stack_value) =>
                let 'existT _ __145 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __145 =>
                      [Alpha_context.signature * (bytes * __145) **
                        Alpha_context.public_key ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let key_value := accu_value in
                let '(signature, (message, stack_value)) := stack_value in
                let res := Signature.check None key_value signature message in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.IHash_key _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [Alpha_context.public_key ** Script_typed_ir.kinstr]
                    [accu_value, k_value] in
                let key_value := accu_value in
                let res :=
                  Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.hash_value)
                    key_value in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.IBlake2b _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [bytes ** Script_typed_ir.kinstr] [accu_value, k_value]
                  in
                let bytes_value := accu_value in
                let hash_value := Raw_hashes.blake2b bytes_value in
                step g_value gas k_value ks hash_value stack_value
              
              | (Script_typed_ir.ISha256 _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [bytes ** Script_typed_ir.kinstr] [accu_value, k_value]
                  in
                let bytes_value := accu_value in
                let hash_value := Raw_hashes.sha256 bytes_value in
                step g_value gas k_value ks hash_value stack_value
              
              | (Script_typed_ir.ISha512 _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [bytes ** Script_typed_ir.kinstr] [accu_value, k_value]
                  in
                let bytes_value := accu_value in
                let hash_value := Raw_hashes.sha512 bytes_value in
                step g_value gas k_value ks hash_value stack_value
              
              | (Script_typed_ir.ISource _ k_value, _, _) =>
                let k_value := cast Script_typed_ir.kinstr k_value in
                let res :=
                  (sc.(Script_typed_ir.step_constants.payer), "default") in
                step g_value gas k_value ks res (accu_value, stack_value)
              
              | (Script_typed_ir.ISender _ k_value, _, _) =>
                let k_value := cast Script_typed_ir.kinstr k_value in
                let res :=
                  (sc.(Script_typed_ir.step_constants.source), "default") in
                step g_value gas k_value ks res (accu_value, stack_value)
              
              | (Script_typed_ir.ISelf _ ty entrypoint k_value, _, _) =>
                let 'existT _ __ISelf_'b [k_value, entrypoint, ty] :=
                  cast_exists (Es := Set)
                    (fun __ISelf_'b =>
                      [Script_typed_ir.kinstr ** string ** Script_typed_ir.ty])
                    [k_value, entrypoint, ty] in
                let res :=
                  (ty, (sc.(Script_typed_ir.step_constants.self), entrypoint))
                  in
                step g_value gas k_value ks res (accu_value, stack_value)
              
              | (Script_typed_ir.ISelf_address _ k_value, _, _) =>
                let k_value := cast Script_typed_ir.kinstr k_value in
                let res := (sc.(Script_typed_ir.step_constants.self), "default")
                  in
                step g_value gas k_value ks res (accu_value, stack_value)
              
              | (Script_typed_ir.IAmount _ k_value, _, _) =>
                let k_value := cast Script_typed_ir.kinstr k_value in
                let stack_value := (accu_value, stack_value) in
                let accu_value := sc.(Script_typed_ir.step_constants.amount) in
                step g_value gas k_value ks accu_value stack_value
              
              | (Script_typed_ir.IDig _ _n n' k_value, _, _) =>
                let 'existT _
                  [__IDig_'b, __IDig_'d, __IDig_'u, __IDig_'c, __IDig_'t]
                  [k_value, n', _n] :=
                  cast_exists (Es := [Set ** Set ** Set ** Set ** Set])
                    (fun
                      '[__IDig_'b, __IDig_'d, __IDig_'u, __IDig_'c, __IDig_'t]
                      =>
                      [Script_typed_ir.kinstr **
                        Script_typed_ir.stack_prefix_preservation_witness **
                        int]) [k_value, n', _n] in
                let '((accu_value, stack_value), x_value) :=
                  Script_interpreter_defs.interp_stack_prefix_preserving_operation
                    (fun (v_value : __IDig_'b) =>
                      fun (stack_value : __IDig_'c * __IDig_'t) =>
                        (stack_value, v_value)) n' accu_value stack_value in
                let stack_value :=
                  ((accu_value, stack_value) : __IDig_'d * __IDig_'u) in
                let accu_value := x_value in
                step g_value gas k_value ks accu_value stack_value
              
              | (Script_typed_ir.IDug _ _n n' k_value, _, stack_value) =>
                let 'existT _
                  [__146, __147, __IDug_'d, __IDug_'u, __IDug_'c, __IDug_'t]
                  [stack_value, k_value, n', _n] :=
                  cast_exists (Es := [Set ** Set ** Set ** Set ** Set ** Set])
                    (fun
                      '[__146, __147, __IDug_'d, __IDug_'u, __IDug_'c,
                        __IDug_'t] =>
                      [__146 * __147 ** Script_typed_ir.kinstr **
                        Script_typed_ir.stack_prefix_preservation_witness **
                        int]) [stack_value, k_value, n', _n] in
                let v_value := accu_value in
                let '(accu_value, stack_value) := stack_value in
                let '((accu_value, stack_value), _) :=
                  Script_interpreter_defs.interp_stack_prefix_preserving_operation
                    (fun (accu_value : __IDug_'c) =>
                      fun (stack_value : __IDug_'t) =>
                        ((v_value, (accu_value, stack_value)), tt)) n'
                    accu_value stack_value in
                step g_value gas k_value ks (accu_value : __IDug_'d)
                  (stack_value : __IDug_'u)
              
              | (Script_typed_ir.IDipn _ _n n' b_value k_value, _, _) =>
                let 'existT _
                  [__IDipn_'b, __IDipn_'u, __IDipn_'c, __IDipn_'t, __IDipn_'d,
                    __IDipn_'v] [k_value, b_value, n', _n] :=
                  cast_exists (Es := [Set ** Set ** Set ** Set ** Set ** Set])
                    (fun
                      '[__IDipn_'b, __IDipn_'u, __IDipn_'c, __IDipn_'t,
                        __IDipn_'d, __IDipn_'v] =>
                      [Script_typed_ir.kinstr ** Script_typed_ir.kinstr **
                        Script_typed_ir.stack_prefix_preservation_witness **
                        int]) [k_value, b_value, n', _n] in
                let '(accu_value, stack_value, restore_prefix) :=
                  (Script_interpreter_defs.kundip :
                    Script_typed_ir.stack_prefix_preservation_witness -> a ->
                    s -> Script_typed_ir.kinstr ->
                    __IDipn_'c * __IDipn_'t * Script_typed_ir.kinstr) n'
                    accu_value stack_value k_value in
                let ks := Script_typed_ir.KCons restore_prefix ks in
                step g_value gas b_value ks accu_value stack_value
              
              | (Script_typed_ir.IDropn _ _n n' k_value, _, _) =>
                let 'existT _ [__IDropn_'b, __IDropn_'u] [k_value, n', _n] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__IDropn_'b, __IDropn_'u] =>
                      [Script_typed_ir.kinstr **
                        Script_typed_ir.stack_prefix_preservation_witness **
                        int]) [k_value, n', _n] in
                let stack_value :=
                  let fix aux {a b s t : Set}
                    (w_value : Script_typed_ir.stack_prefix_preservation_witness)
                    (accu_value : a) (stack_value : s) {struct w_value}
                    : b * t :=
                    match (w_value, accu_value, stack_value) with
                    | (Script_typed_ir.KRest, accu_value, stack_value) =>
                      let '[stack_value, accu_value] :=
                        cast [t ** b] [stack_value, accu_value] in
                      (accu_value, stack_value)
                    | (Script_typed_ir.KPrefix _ w_value, _, stack_value) =>
                      let 'existT _ [__1333, __1334] [stack_value, w_value] :=
                        cast_exists (Es := [Set ** Set])
                          (fun '[__1333, __1334] =>
                            [__1333 * __1334 **
                              Script_typed_ir.stack_prefix_preservation_witness])
                          [stack_value, w_value] in
                      let '(accu_value, stack_value) := stack_value in
                      aux w_value accu_value stack_value
                    end in
                  (aux :
                    Script_typed_ir.stack_prefix_preservation_witness -> a ->
                    s -> __IDropn_'b * __IDropn_'u) n' accu_value stack_value in
                let '(accu_value, stack_value) := stack_value in
                step g_value gas k_value ks accu_value stack_value
              
              | (Script_typed_ir.ISapling_empty_state _ memo_size k_value, _, _)
                =>
                let '[k_value, memo_size] :=
                  cast
                    [Script_typed_ir.kinstr **
                      Alpha_context.Sapling.Memo_size.t] [k_value, memo_size] in
                let state := Alpha_context.Sapling.empty_state None memo_size tt
                  in
                step g_value gas k_value ks state (accu_value, stack_value)
              
              |
                (Script_typed_ir.ISapling_verify_update _ k_value, accu_value,
                  stack_value) =>
                let 'existT _ __148 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __148 =>
                      [Alpha_context.Sapling.state * __148 **
                        Alpha_context.Sapling.transaction **
                        Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let transaction := accu_value in
                let '(state, stack_value) := stack_value in
                let address :=
                  Alpha_context.Contract.to_b58check
                    sc.(Script_typed_ir.step_constants.self) in
                let chain_id :=
                  Chain_id.to_b58check
                    sc.(Script_typed_ir.step_constants.chain_id) in
                let anti_replay := Pervasives.op_caret address chain_id in
                let ctxt := Local_gas_counter.update_context gas ctxt in
                let? '(ctxt, balance_state_opt) :=
                  Alpha_context.Sapling.verify_update ctxt state transaction
                    anti_replay in
                let gas := Local_gas_counter.update_local_gas_counter ctxt in
                let ctxt := Local_gas_counter.outdated ctxt in
                match balance_state_opt with
                | Some (balance, state) =>
                  let state :=
                    Some ((Alpha_context.Script_int.of_int64 balance), state) in
                  step (ctxt, sc) gas k_value ks state stack_value
                | None =>
                  step (ctxt, sc) gas k_value ks
                    (None :
                      option
                        (Script_typed_ir.pair Alpha_context.Script_int.num
                          Alpha_context.Sapling.state)) stack_value
                end
              
              | (Script_typed_ir.IChainId _ k_value, _, _) =>
                let k_value := cast Script_typed_ir.kinstr k_value in
                let stack_value := (accu_value, stack_value) in
                let accu_value := sc.(Script_typed_ir.step_constants.chain_id)
                  in
                step g_value gas k_value ks accu_value stack_value
              
              | (Script_typed_ir.IVoting_power _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [Alpha_context.public_key_hash ** Script_typed_ir.kinstr]
                    [accu_value, k_value] in
                let key_hash := accu_value in
                let ctxt := Local_gas_counter.update_context gas ctxt in
                let? '(ctxt, rolls) :=
                  Alpha_context.Vote.get_voting_power ctxt key_hash in
                let power :=
                  Alpha_context.Script_int.abs
                    (Alpha_context.Script_int.of_int32 rolls) in
                let gas := Local_gas_counter.update_local_gas_counter ctxt in
                let ctxt := Local_gas_counter.outdated ctxt in
                step (ctxt, sc) gas k_value ks power stack_value
              
              | (Script_typed_ir.ITotal_voting_power _ k_value, _, _) =>
                let k_value := cast Script_typed_ir.kinstr k_value in
                let ctxt := Local_gas_counter.update_context gas ctxt in
                let? '(ctxt, rolls) :=
                  Alpha_context.Vote.get_total_voting_power ctxt in
                let power :=
                  Alpha_context.Script_int.abs
                    (Alpha_context.Script_int.of_int32 rolls) in
                let gas := Local_gas_counter.update_local_gas_counter ctxt in
                let ctxt := Local_gas_counter.outdated ctxt in
                let g_value := (ctxt, sc) in
                step g_value gas k_value ks power (accu_value, stack_value)
              
              | (Script_typed_ir.IKeccak _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [bytes ** Script_typed_ir.kinstr] [accu_value, k_value]
                  in
                let bytes_value := accu_value in
                let hash_value := Raw_hashes.keccak256 bytes_value in
                step g_value gas k_value ks hash_value stack_value
              
              | (Script_typed_ir.ISha3 _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [bytes ** Script_typed_ir.kinstr] [accu_value, k_value]
                  in
                let bytes_value := accu_value in
                let hash_value := Raw_hashes.sha3_256 bytes_value in
                step g_value gas k_value ks hash_value stack_value
              
              |
                (Script_typed_ir.IAdd_bls12_381_g1 _ k_value, accu_value,
                  stack_value) =>
                let 'existT _ __149 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __149 =>
                      [Bls12_381.G1.(S.CURVE.t) * __149 **
                        Bls12_381.G1.(S.CURVE.t) ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let accu_value := Bls12_381.G1.(S.CURVE.add) x_value y_value in
                step g_value gas k_value ks accu_value stack_value
              
              |
                (Script_typed_ir.IAdd_bls12_381_g2 _ k_value, accu_value,
                  stack_value) =>
                let 'existT _ __150 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __150 =>
                      [Bls12_381.G2.(S.CURVE.t) * __150 **
                        Bls12_381.G2.(S.CURVE.t) ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let accu_value := Bls12_381.G2.(S.CURVE.add) x_value y_value in
                step g_value gas k_value ks accu_value stack_value
              
              |
                (Script_typed_ir.IAdd_bls12_381_fr _ k_value, accu_value,
                  stack_value) =>
                let 'existT _ __151 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __151 =>
                      [Bls12_381.Fr.(S.PRIME_FIELD.t) * __151 **
                        Bls12_381.Fr.(S.PRIME_FIELD.t) **
                        Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let accu_value :=
                  Bls12_381.Fr.(S.PRIME_FIELD.add) x_value y_value in
                step g_value gas k_value ks accu_value stack_value
              
              |
                (Script_typed_ir.IMul_bls12_381_g1 _ k_value, accu_value,
                  stack_value) =>
                let 'existT _ __152 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __152 =>
                      [Bls12_381.Fr.(S.PRIME_FIELD.t) * __152 **
                        Bls12_381.G1.(S.CURVE.t) ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let accu_value := Bls12_381.G1.(S.CURVE.mul) x_value y_value in
                step g_value gas k_value ks accu_value stack_value
              
              |
                (Script_typed_ir.IMul_bls12_381_g2 _ k_value, accu_value,
                  stack_value) =>
                let 'existT _ __153 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __153 =>
                      [Bls12_381.Fr.(S.PRIME_FIELD.t) * __153 **
                        Bls12_381.G2.(S.CURVE.t) ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let accu_value := Bls12_381.G2.(S.CURVE.mul) x_value y_value in
                step g_value gas k_value ks accu_value stack_value
              
              |
                (Script_typed_ir.IMul_bls12_381_fr _ k_value, accu_value,
                  stack_value) =>
                let 'existT _ __154 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __154 =>
                      [Bls12_381.Fr.(S.PRIME_FIELD.t) * __154 **
                        Bls12_381.Fr.(S.PRIME_FIELD.t) **
                        Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let accu_value :=
                  Bls12_381.Fr.(S.PRIME_FIELD.mul) x_value y_value in
                step g_value gas k_value ks accu_value stack_value
              
              |
                (Script_typed_ir.IMul_bls12_381_fr_z _ k_value, accu_value,
                  stack_value) =>
                let 'existT _ [__156, __155] [stack_value, accu_value, k_value]
                  :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__156, __155] =>
                      [Bls12_381.Fr.(S.PRIME_FIELD.t) * __156 **
                        Alpha_context.Script_int.num ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let x_value := accu_value in
                let '(y_value, stack_value) := stack_value in
                let x_value :=
                  Bls12_381.Fr.(S.PRIME_FIELD.of_z)
                    (Alpha_context.Script_int.to_zint x_value) in
                let res := Bls12_381.Fr.(S.PRIME_FIELD.mul) x_value y_value in
                step g_value gas k_value ks res stack_value
              
              |
                (Script_typed_ir.IMul_bls12_381_z_fr _ k_value, accu_value,
                  stack_value) =>
                let 'existT _ [__157, __158] [stack_value, accu_value, k_value]
                  :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__157, __158] =>
                      [Alpha_context.Script_int.num * __158 **
                        Bls12_381.Fr.(S.PRIME_FIELD.t) **
                        Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let y_value := accu_value in
                let '(x_value, stack_value) := stack_value in
                let x_value :=
                  Bls12_381.Fr.(S.PRIME_FIELD.of_z)
                    (Alpha_context.Script_int.to_zint x_value) in
                let res := Bls12_381.Fr.(S.PRIME_FIELD.mul) x_value y_value in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.IInt_bls12_381_fr _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast
                    [Bls12_381.Fr.(S.PRIME_FIELD.t) ** Script_typed_ir.kinstr]
                    [accu_value, k_value] in
                let x_value := accu_value in
                let res :=
                  Alpha_context.Script_int.of_zint
                    (Bls12_381.Fr.(S.PRIME_FIELD.to_z) x_value) in
                step g_value gas k_value ks res stack_value
              
              | (Script_typed_ir.INeg_bls12_381_g1 _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [Bls12_381.G1.(S.CURVE.t) ** Script_typed_ir.kinstr]
                    [accu_value, k_value] in
                let x_value := accu_value in
                let accu_value := Bls12_381.G1.(S.CURVE.negate) x_value in
                step g_value gas k_value ks accu_value stack_value
              
              | (Script_typed_ir.INeg_bls12_381_g2 _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast [Bls12_381.G2.(S.CURVE.t) ** Script_typed_ir.kinstr]
                    [accu_value, k_value] in
                let x_value := accu_value in
                let accu_value := Bls12_381.G2.(S.CURVE.negate) x_value in
                step g_value gas k_value ks accu_value stack_value
              
              | (Script_typed_ir.INeg_bls12_381_fr _ k_value, accu_value, _) =>
                let '[accu_value, k_value] :=
                  cast
                    [Bls12_381.Fr.(S.PRIME_FIELD.t) ** Script_typed_ir.kinstr]
                    [accu_value, k_value] in
                let x_value := accu_value in
                let accu_value := Bls12_381.Fr.(S.PRIME_FIELD.negate) x_value in
                step g_value gas k_value ks accu_value stack_value
              
              |
                (Script_typed_ir.IPairing_check_bls12_381 _ k_value, accu_value,
                  _) =>
                let '[accu_value, k_value] :=
                  cast
                    [Script_typed_ir.boxed_list
                      (Script_typed_ir.pair Bls12_381.G1.(S.CURVE.t)
                        Bls12_381.G2.(S.CURVE.t)) ** Script_typed_ir.kinstr]
                    [accu_value, k_value] in
                let pairs := accu_value in
                let check :=
                  Bls12_381.pairing_check
                    pairs.(Script_typed_ir.boxed_list.elements) in
                step g_value gas k_value ks check stack_value
              
              | (Script_typed_ir.IComb _ _ witness k_value, _, _) =>
                let 'existT _ [__IComb_'b, __IComb_'u] [k_value, witness] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__IComb_'b, __IComb_'u] =>
                      [Script_typed_ir.kinstr **
                        Script_typed_ir.comb_gadt_witness]) [k_value, witness]
                  in
                let fix aux {before after : Set}
                  (witness : Script_typed_ir.comb_gadt_witness)
                  (stack_value : before) {struct witness} : after :=
                  match (witness, stack_value) with
                  | (Script_typed_ir.Comb_one, stack_value) =>
                    let stack_value := cast after stack_value in
                    stack_value
                  | (Script_typed_ir.Comb_succ witness', stack_value) =>
                    let 'existT _ [__1342, __1343, __1344, __1345]
                      [stack_value, witness'] :=
                      cast_exists (Es := [Set ** Set ** Set ** Set])
                        (fun '[__1342, __1343, __1344, __1345] =>
                          [__1342 * __1343 ** Script_typed_ir.comb_gadt_witness])
                        [stack_value, witness'] in
                    let '(a_value, tl) := stack_value in
                    let '(b_value, tl') :=
                      (aux :
                        Script_typed_ir.comb_gadt_witness -> __1343 ->
                        __1344 * __1345) witness' tl in
                    cast after ((a_value, b_value), tl')
                  end in
                let stack_value :=
                  (aux :
                    Script_typed_ir.comb_gadt_witness -> a * s ->
                    __IComb_'b * __IComb_'u) witness (accu_value, stack_value)
                  in
                let '(accu_value, stack_value) := stack_value in
                step g_value gas k_value ks accu_value stack_value
              
              | (Script_typed_ir.IUncomb _ _ witness k_value, _, _) =>
                let 'existT _ [__IUncomb_'b, __IUncomb_'u] [k_value, witness] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__IUncomb_'b, __IUncomb_'u] =>
                      [Script_typed_ir.kinstr **
                        Script_typed_ir.uncomb_gadt_witness]) [k_value, witness]
                  in
                let fix aux {before after : Set}
                  (witness : Script_typed_ir.uncomb_gadt_witness)
                  (stack_value : before) {struct witness} : after :=
                  match (witness, stack_value) with
                  | (Script_typed_ir.Uncomb_one, stack_value) =>
                    let stack_value := cast after stack_value in
                    stack_value
                  | (Script_typed_ir.Uncomb_succ witness', stack_value) =>
                    let 'existT _ [__1353, __1354, __1355, __1356]
                      [stack_value, witness'] :=
                      cast_exists (Es := [Set ** Set ** Set ** Set])
                        (fun '[__1353, __1354, __1355, __1356] =>
                          [(__1353 * __1354) * __1355 **
                            Script_typed_ir.uncomb_gadt_witness])
                        [stack_value, witness'] in
                    let '((a_value, b_value), tl) := stack_value in
                    cast after
                      (a_value,
                        ((aux :
                          Script_typed_ir.uncomb_gadt_witness ->
                          __1354 * __1355 -> __1356) witness' (b_value, tl)))
                  end in
                let stack_value :=
                  (aux :
                    Script_typed_ir.uncomb_gadt_witness -> a * s ->
                    __IUncomb_'b * __IUncomb_'u) witness
                    (accu_value, stack_value) in
                let '(accu_value, stack_value) := stack_value in
                step g_value gas k_value ks accu_value stack_value
              
              | (Script_typed_ir.IComb_get _ _ witness k_value, _, _) =>
                let 'existT _ __IComb_get_'v [k_value, witness] :=
                  cast_exists (Es := Set)
                    (fun __IComb_get_'v =>
                      [Script_typed_ir.kinstr **
                        Script_typed_ir.comb_get_gadt_witness])
                    [k_value, witness] in
                let comb := accu_value in
                let fix aux {before after : Set}
                  (witness : Script_typed_ir.comb_get_gadt_witness)
                  (comb : before) {struct witness} : after :=
                  match (witness, comb) with
                  | (Script_typed_ir.Comb_get_zero, v_value) =>
                    let v_value := cast after v_value in
                    v_value
                  | (Script_typed_ir.Comb_get_one, comb) =>
                    let 'existT _ __1362 comb :=
                      cast_exists (Es := Set) (fun __1362 => after * __1362)
                        comb in
                    let '(a_value, _) := comb in
                    a_value
                  | (Script_typed_ir.Comb_get_plus_two witness', comb) =>
                    let 'existT _ [__1363, __1364] [comb, witness'] :=
                      cast_exists (Es := [Set ** Set])
                        (fun '[__1363, __1364] =>
                          [__1363 * __1364 **
                            Script_typed_ir.comb_get_gadt_witness])
                        [comb, witness'] in
                    let '(_, b_value) := comb in
                    aux witness' b_value
                  end in
                let accu_value :=
                  (aux :
                    Script_typed_ir.comb_get_gadt_witness -> a -> __IComb_get_'v)
                    witness comb in
                step g_value gas k_value ks accu_value stack_value
              
              | (Script_typed_ir.IComb_set _ _ witness k_value, _, stack_value)
                =>
                let 'existT _ [__159, __160, __IComb_set_'c]
                  [stack_value, k_value, witness] :=
                  cast_exists (Es := [Set ** Set ** Set])
                    (fun '[__159, __160, __IComb_set_'c] =>
                      [__159 * __160 ** Script_typed_ir.kinstr **
                        Script_typed_ir.comb_set_gadt_witness])
                    [stack_value, k_value, witness] in
                let value_value := accu_value in
                let '(comb, stack_value) := stack_value in
                let fix aux {value before after : Set}
                  (witness : Script_typed_ir.comb_set_gadt_witness)
                  (value_value : value) (item : before) {struct witness}
                  : after :=
                  match (witness, value_value, item) with
                  | (Script_typed_ir.Comb_set_zero, value_value, _) =>
                    let value_value := cast after value_value in
                    value_value
                  | (Script_typed_ir.Comb_set_one, _, item) =>
                    let 'existT _ [__1369, __1370] item :=
                      cast_exists (Es := [Set ** Set])
                        (fun '[__1369, __1370] => __1369 * __1370) item in
                    let '(_hd, tl) := item in
                    cast after (value_value, tl)
                  | (Script_typed_ir.Comb_set_plus_two witness', _, item) =>
                    let 'existT _ [__1371, __1372, __1373] [item, witness'] :=
                      cast_exists (Es := [Set ** Set ** Set])
                        (fun '[__1371, __1372, __1373] =>
                          [__1371 * __1372 **
                            Script_typed_ir.comb_set_gadt_witness])
                        [item, witness'] in
                    let '(hd, tl) := item in
                    cast after
                      (hd,
                        ((aux :
                          Script_typed_ir.comb_set_gadt_witness -> value ->
                          __1372 -> __1373) witness' value_value tl))
                  end in
                let accu_value :=
                  (aux :
                    Script_typed_ir.comb_set_gadt_witness -> a -> __159 ->
                    __IComb_set_'c) witness value_value comb in
                step g_value gas k_value ks accu_value stack_value
              
              | (Script_typed_ir.IDup_n _ _ witness k_value, _, _) =>
                let 'existT _ __IDup_n_'t [k_value, witness] :=
                  cast_exists (Es := Set)
                    (fun __IDup_n_'t =>
                      [Script_typed_ir.kinstr **
                        Script_typed_ir.dup_n_gadt_witness]) [k_value, witness]
                  in
                let fix aux {before after : Set}
                  (witness : Script_typed_ir.dup_n_gadt_witness)
                  (stack_value : before) {struct witness} : after :=
                  match (witness, stack_value) with
                  | (Script_typed_ir.Dup_n_zero, stack_value) =>
                    let 'existT _ __1380 stack_value :=
                      cast_exists (Es := Set) (fun __1380 => after * __1380)
                        stack_value in
                    let '(a_value, _) := stack_value in
                    a_value
                  | (Script_typed_ir.Dup_n_succ witness', stack_value) =>
                    let 'existT _ [__1381, __1382] [stack_value, witness'] :=
                      cast_exists (Es := [Set ** Set])
                        (fun '[__1381, __1382] =>
                          [__1381 * __1382 **
                            Script_typed_ir.dup_n_gadt_witness])
                        [stack_value, witness'] in
                    let '(_, tl) := stack_value in
                    aux witness' tl
                  end in
                let stack_value := (accu_value, stack_value) in
                let accu_value :=
                  (aux :
                    Script_typed_ir.dup_n_gadt_witness -> a * s -> __IDup_n_'t)
                    witness stack_value in
                step g_value gas k_value ks accu_value stack_value
              
              | (Script_typed_ir.ITicket _ k_value, _, stack_value) =>
                let 'existT _ __161 [stack_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __161 =>
                      [Alpha_context.Script_int.num * __161 **
                        Script_typed_ir.kinstr]) [stack_value, k_value] in
                let contents := accu_value in
                let '(amount, stack_value) := stack_value in
                let ticketer := sc.(Script_typed_ir.step_constants.self) in
                let accu_value :=
                  {| Script_typed_ir.ticket.ticketer := ticketer;
                    Script_typed_ir.ticket.contents := contents;
                    Script_typed_ir.ticket.amount := amount |} in
                step g_value gas k_value ks accu_value stack_value
              
              | (Script_typed_ir.IRead_ticket _ k_value, accu_value, _) =>
                let 'existT _ __162 [accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __162 =>
                      [Script_typed_ir.ticket __162 ** Script_typed_ir.kinstr])
                    [accu_value, k_value] in
                let '{|
                  Script_typed_ir.ticket.ticketer := ticketer;
                    Script_typed_ir.ticket.contents := contents;
                    Script_typed_ir.ticket.amount := amount
                    |} := accu_value in
                let stack_value := (accu_value, stack_value) in
                let accu_value := ((ticketer, "default"), (contents, amount)) in
                step g_value gas k_value ks accu_value stack_value
              
              |
                (Script_typed_ir.ISplit_ticket _ k_value, accu_value,
                  stack_value) =>
                let 'existT _ [__164, __163] [stack_value, accu_value, k_value]
                  :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__164, __163] =>
                      [(Alpha_context.Script_int.num *
                        Alpha_context.Script_int.num) * __164 **
                        Script_typed_ir.ticket __163 ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let ticket := accu_value in
                let '((amount_a, amount_b), stack_value) := stack_value in
                let result_value :=
                  if
                    (Alpha_context.Script_int.compare
                      (Alpha_context.Script_int.add_n amount_a
                        amount_b)
                      ticket.(Script_typed_ir.ticket.amount)) =i
                    0
                  then
                    Some
                      ((Script_typed_ir.ticket.with_amount amount_a ticket),
                        (Script_typed_ir.ticket.with_amount amount_b ticket))
                  else
                    None in
                step g_value gas k_value ks result_value stack_value
              
              |
                (Script_typed_ir.IJoin_tickets _ contents_ty k_value,
                  accu_value, _) =>
                let 'existT _ __165 [accu_value, k_value, contents_ty] :=
                  cast_exists (Es := Set)
                    (fun __165 =>
                      [Script_typed_ir.ticket __165 *
                        Script_typed_ir.ticket __165 ** Script_typed_ir.kinstr
                        ** Script_typed_ir.comparable_ty])
                    [accu_value, k_value, contents_ty] in
                let '(ticket_a, ticket_b) := accu_value in
                let result_value :=
                  if
                    ((Alpha_context.Contract.compare
                      ticket_a.(Script_typed_ir.ticket.ticketer)
                      ticket_b.(Script_typed_ir.ticket.ticketer))
                    =i 0) &&
                    ((Script_comparable.compare_comparable contents_ty
                      ticket_a.(Script_typed_ir.ticket.contents)
                      ticket_b.(Script_typed_ir.ticket.contents))
                    =i 0)
                  then
                    Some
                      {|
                        Script_typed_ir.ticket.ticketer :=
                          ticket_a.(Script_typed_ir.ticket.ticketer);
                        Script_typed_ir.ticket.contents :=
                          ticket_a.(Script_typed_ir.ticket.contents);
                        Script_typed_ir.ticket.amount :=
                          Alpha_context.Script_int.add_n
                            ticket_a.(Script_typed_ir.ticket.amount)
                            ticket_b.(Script_typed_ir.ticket.amount) |}
                  else
                    None in
                step g_value gas k_value ks result_value stack_value
              
              | (Script_typed_ir.IOpen_chest _ k_value, accu_value, stack_value)
                =>
                let 'existT _ __166 [stack_value, accu_value, k_value] :=
                  cast_exists (Es := Set)
                    (fun __166 =>
                      [Timelock.chest * (Alpha_context.Script_int.num * __166)
                        ** Timelock.chest_key ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value] in
                let chest_key := accu_value in
                let '(chest, (time_z, stack_value)) := stack_value in
                let accu_value :=
                  match Alpha_context.Script_int.to_int time_z with
                  | None => Script_typed_ir.R false
                  | Some time =>
                    match Timelock.open_chest chest chest_key time with
                    | Timelock.Correct bytes_value =>
                      Script_typed_ir.L bytes_value
                    | Timelock.Bogus_cipher => Script_typed_ir.R false
                    | Timelock.Bogus_opening => Script_typed_ir.R true
                    end
                  end in
                step g_value gas k_value ks accu_value stack_value
              | _ => unreachable_gadt_branch
              end
            end

with log {a s r f : Set}
  (function_parameter : Script_typed_ir.logger * Script_typed_ir.logging_event)
  {struct function_parameter} : Script_interpreter_defs.step_type a s r f :=
  let '(logger, event) := function_parameter in
  fun (function_parameter :
    Local_gas_counter.outdated_context * Script_typed_ir.step_constants) =>
    let '(ctxt, _) as g_value := function_parameter in
    fun (gas : Local_gas_counter.local_gas_counter) =>
      fun (k_value : Script_typed_ir.kinstr) =>
        fun (ks : Script_typed_ir.continuation) =>
          fun (accu_value : a) =>
            fun (stack_value : s) =>
              let '_ :=
                match (k_value, event) with
                | (Script_typed_ir.ILog _ _ _ _, Script_typed_ir.LogEntry) => tt
                | (_, Script_typed_ir.LogEntry) =>
                  Script_interpreter_defs.log_entry logger ctxt gas k_value
                    accu_value stack_value
                | (_, Script_typed_ir.LogExit prev_kinfo) =>
                  Script_interpreter_defs.log_exit logger ctxt gas prev_kinfo
                    k_value accu_value stack_value
                end in
              let k_value :=
                Script_interpreter_defs.log_next_kinstr logger k_value in
              let with_log (k_value : Script_typed_ir.continuation)
                : Script_typed_ir.continuation :=
                match k_value with
                | Script_typed_ir.KLog _ _ => k_value
                | _ => Script_typed_ir.KLog k_value logger
                end in
              match (k_value, accu_value, stack_value) with
              |
                (Script_typed_ir.IList_map _ body k_value, accu_value,
                  stack_value) =>
                let 'existT _ [__1853, __1854, __1852, __IList_map_'b2]
                  [stack_value, accu_value, k_value, body] :=
                  cast_exists (Es := [Set ** Set ** Set ** Set])
                    (fun '[__1853, __1854, __1852, __IList_map_'b2] =>
                      [__1853 * __1854 ** Script_typed_ir.boxed_list __1852 **
                        Script_typed_ir.kinstr ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value, body] in
                (ilist_map (f := __IList_map_'b2)) with_log g_value gas
                  (body, k_value) ks accu_value stack_value
              
              |
                (Script_typed_ir.IList_iter _ body k_value, accu_value,
                  stack_value) =>
                let 'existT _ [__1856, __1857, __1855]
                  [stack_value, accu_value, k_value, body] :=
                  cast_exists (Es := [Set ** Set ** Set])
                    (fun '[__1856, __1857, __1855] =>
                      [__1856 * __1857 ** Script_typed_ir.boxed_list __1855 **
                        Script_typed_ir.kinstr ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value, body] in
                ilist_iter with_log g_value gas (body, k_value) ks accu_value
                  stack_value
              
              |
                (Script_typed_ir.ISet_iter _ body k_value, accu_value,
                  stack_value) =>
                let 'existT _ [__1859, __1860, __1858]
                  [stack_value, accu_value, k_value, body] :=
                  cast_exists (Es := [Set ** Set ** Set])
                    (fun '[__1859, __1860, __1858] =>
                      [__1859 * __1860 ** Script_typed_ir.set __1858 **
                        Script_typed_ir.kinstr ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value, body] in
                iset_iter with_log g_value gas (body, k_value) ks accu_value
                  stack_value
              
              |
                (Script_typed_ir.IMap_map _ body k_value, accu_value,
                  stack_value) =>
                let 'existT _ [__1863, __1864, __1861, __1862, __IMap_map_'c2]
                  [stack_value, accu_value, k_value, body] :=
                  cast_exists (Es := [Set ** Set ** Set ** Set ** Set])
                    (fun '[__1863, __1864, __1861, __1862, __IMap_map_'c2] =>
                      [__1863 * __1864 ** Script_typed_ir.map __1861 __1862 **
                        Script_typed_ir.kinstr ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value, body] in
                (imap_map (g := __IMap_map_'c2)) with_log g_value gas
                  (body, k_value) ks accu_value stack_value
              
              |
                (Script_typed_ir.IMap_iter _ body k_value, accu_value,
                  stack_value) =>
                let 'existT _ [__1867, __1868, __1865, __1866]
                  [stack_value, accu_value, k_value, body] :=
                  cast_exists (Es := [Set ** Set ** Set ** Set])
                    (fun '[__1867, __1868, __1865, __1866] =>
                      [__1867 * __1868 ** Script_typed_ir.map __1865 __1866 **
                        Script_typed_ir.kinstr ** Script_typed_ir.kinstr])
                    [stack_value, accu_value, k_value, body] in
                imap_iter with_log g_value gas (body, k_value) ks accu_value
                  stack_value
              
              | (Script_typed_ir.ILoop _ body k_value, _, _) =>
                let 'existT _ [__1869, __1870] [k_value, body] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__1869, __1870] =>
                      [Script_typed_ir.kinstr ** Script_typed_ir.kinstr])
                    [k_value, body] in
                let ks :=
                  with_log
                    (Script_typed_ir.KLoop_in body
                      (Script_typed_ir.KCons k_value ks)) in
                next g_value gas ks accu_value stack_value
              
              | (Script_typed_ir.ILoop_left _ bl br, _, _) =>
                let 'existT _ [__1872, __1871] [br, bl] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__1872, __1871] =>
                      [Script_typed_ir.kinstr ** Script_typed_ir.kinstr])
                    [br, bl] in
                let ks :=
                  with_log
                    (Script_typed_ir.KLoop_in_left bl
                      (Script_typed_ir.KCons br ks)) in
                next g_value gas ks accu_value stack_value
              
              |
                (Script_typed_ir.IMul_teznat kinfo_value k_value, accu_value,
                  stack_value) =>
                let 'existT _ __1873
                  [stack_value, accu_value, k_value, kinfo_value] :=
                  cast_exists (Es := Set)
                    (fun __1873 =>
                      [Alpha_context.Script_int.num * __1873 **
                        Alpha_context.Tez.t ** Script_typed_ir.kinstr **
                        Script_typed_ir.kinfo])
                    [stack_value, accu_value, k_value, kinfo_value] in
                let extra := (kinfo_value, k_value) in
                imul_teznat (Some logger) g_value gas extra ks accu_value
                  stack_value
              
              |
                (Script_typed_ir.IMul_nattez kinfo_value k_value, accu_value,
                  stack_value) =>
                let 'existT _ __1874
                  [stack_value, accu_value, k_value, kinfo_value] :=
                  cast_exists (Es := Set)
                    (fun __1874 =>
                      [Alpha_context.Tez.t * __1874 **
                        Alpha_context.Script_int.num ** Script_typed_ir.kinstr
                        ** Script_typed_ir.kinfo])
                    [stack_value, accu_value, k_value, kinfo_value] in
                let extra := (kinfo_value, k_value) in
                imul_nattez (Some logger) g_value gas extra ks accu_value
                  stack_value
              
              |
                (Script_typed_ir.ILsl_nat kinfo_value k_value, accu_value,
                  stack_value) =>
                let 'existT _ __1875
                  [stack_value, accu_value, k_value, kinfo_value] :=
                  cast_exists (Es := Set)
                    (fun __1875 =>
                      [Alpha_context.Script_int.num * __1875 **
                        Alpha_context.Script_int.num ** Script_typed_ir.kinstr
                        ** Script_typed_ir.kinfo])
                    [stack_value, accu_value, k_value, kinfo_value] in
                let extra := (kinfo_value, k_value) in
                ilsl_nat (Some logger) g_value gas extra ks accu_value
                  stack_value
              
              |
                (Script_typed_ir.ILsr_nat kinfo_value k_value, accu_value,
                  stack_value) =>
                let 'existT _ __1876
                  [stack_value, accu_value, k_value, kinfo_value] :=
                  cast_exists (Es := Set)
                    (fun __1876 =>
                      [Alpha_context.Script_int.num * __1876 **
                        Alpha_context.Script_int.num ** Script_typed_ir.kinstr
                        ** Script_typed_ir.kinfo])
                    [stack_value, accu_value, k_value, kinfo_value] in
                let extra := (kinfo_value, k_value) in
                ilsr_nat (Some logger) g_value gas extra ks accu_value
                  stack_value
              
              | (Script_typed_ir.IFailwith _ kloc tv, _, _) =>
                let '[tv, kloc] :=
                  cast [Script_typed_ir.ty ** Alpha_context.Script.location]
                    [tv, kloc] in
                ifailwith (Some logger) g_value gas kloc tv accu_value
              
              | (Script_typed_ir.IExec _ k_value, _, stack_value) =>
                let 'existT _ [__1877, __1878] [stack_value, k_value] :=
                  cast_exists (Es := [Set ** Set])
                    (fun '[__1877, __1878] =>
                      [Script_typed_ir.lambda * __1878 **
                        Script_typed_ir.kinstr]) [stack_value, k_value] in
                iexec (Some logger) g_value gas k_value ks accu_value
                  stack_value
              
              | _ =>
                step g_value gas k_value (with_log ks) accu_value stack_value
              end

with klog {a s r f : Set}
  (logger : Script_typed_ir.logger)
  (g_value : Local_gas_counter.outdated_context * step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (ks0 : Script_typed_ir.continuation) (ks : Script_typed_ir.continuation)
  (accu_value : a) (stack_value : s) {struct gas}
  : M?
    (r * f * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter) :=
  let '_ :=
    match ks with
    | Script_typed_ir.KLog _ _ => tt
    | _ => Script_interpreter_defs.log_control logger ks
    end in
  let enable_log (ki : Script_typed_ir.kinstr) : Script_typed_ir.kinstr :=
    Script_interpreter_defs.log_kinstr logger ki in
  let mk (k_value : Script_typed_ir.continuation)
    : Script_typed_ir.continuation :=
    match k_value with
    | Script_typed_ir.KLog _ _ => k_value
    | _ => Script_typed_ir.KLog k_value logger
    end in
  match (ks, accu_value, stack_value) with
  | (Script_typed_ir.KCons ki ks', _, _) =>
    let 'existT _ [__KCons_'b2, __KCons_'t2] [ks', ki] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__KCons_'b2, __KCons_'t2] =>
          [Script_typed_ir.continuation ** Script_typed_ir.kinstr]) [ks', ki] in
    let log := enable_log ki in
    let ks := mk ks' in
    step g_value gas log ks accu_value stack_value
  
  | (Script_typed_ir.KNil, _, _) => next g_value gas ks accu_value stack_value
  
  | (Script_typed_ir.KLoop_in ki ks', accu_value, stack_value) =>
    let 'existT _ [__1929, __1930] [stack_value, accu_value, ks', ki] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__1929, __1930] =>
          [__1929 * __1930 ** bool ** Script_typed_ir.continuation **
            Script_typed_ir.kinstr]) [stack_value, accu_value, ks', ki] in
    let ks' := mk ks' in
    let ki := enable_log ki in
    kloop_in g_value gas ks0 ki ks' accu_value stack_value
  
  | (Script_typed_ir.KReturn stack' ks', _, _) =>
    let 'existT _ __KReturn_'s2 [ks', stack'] :=
      cast_exists (Es := Set)
        (fun __KReturn_'s2 => [Script_typed_ir.continuation ** __KReturn_'s2])
        [ks', stack'] in
    let ks' := mk ks' in
    let ks := Script_typed_ir.KReturn stack' ks' in
    next g_value gas ks accu_value stack_value
  
  | (Script_typed_ir.KMap_head f_value ks, _, _) =>
    let 'existT _ __KMap_head_'b2 [ks, f_value] :=
      cast_exists (Es := Set)
        (fun __KMap_head_'b2 =>
          [Script_typed_ir.continuation ** a -> __KMap_head_'b2]) [ks, f_value]
      in
    next g_value gas ks (f_value accu_value) stack_value
  
  | (Script_typed_ir.KLoop_in_left ki ks', accu_value, _) =>
    let 'existT _ [__1931, __1932] [accu_value, ks', ki] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__1931, __1932] =>
          [Script_typed_ir.union __1931 __1932 ** Script_typed_ir.continuation
            ** Script_typed_ir.kinstr]) [accu_value, ks', ki] in
    let ks' := mk ks' in
    let ki := enable_log ki in
    kloop_in_left g_value gas ks0 ki ks' accu_value stack_value
  
  | (Script_typed_ir.KUndip x_value ks', _, _) =>
    let 'existT _ __KUndip_'b2 [ks', x_value] :=
      cast_exists (Es := Set)
        (fun __KUndip_'b2 => [Script_typed_ir.continuation ** __KUndip_'b2])
        [ks', x_value] in
    let ks' := mk ks' in
    let ks := Script_typed_ir.KUndip x_value ks' in
    next g_value gas ks accu_value stack_value
  
  | (Script_typed_ir.KIter body xs ks', _, _) =>
    let 'existT _ __KIter_'a2 [ks', xs, body] :=
      cast_exists (Es := Set)
        (fun __KIter_'a2 =>
          [Script_typed_ir.continuation ** list __KIter_'a2 **
            Script_typed_ir.kinstr]) [ks', xs, body] in
    let ks' := mk ks' in
    let body := enable_log body in
    kiter mk g_value gas (body, xs) ks' accu_value stack_value
  
  | (Script_typed_ir.KList_enter_body body xs ys len ks', _, _) =>
    let 'existT _ [__KList_enter_body_'b2, __KList_enter_body_'a2]
      [ks', len, ys, xs, body] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__KList_enter_body_'b2, __KList_enter_body_'a2] =>
          [Script_typed_ir.continuation ** int ** list __KList_enter_body_'b2 **
            list __KList_enter_body_'a2 ** Script_typed_ir.kinstr])
        [ks', len, ys, xs, body] in
    let ks' := mk ks' in
    let extra := (body, xs, ys, len) in
    klist_enter mk g_value gas extra ks' accu_value stack_value
  
  | (Script_typed_ir.KList_exit_body body xs ys len ks', _, stack_value) =>
    let 'existT _ [__1933, __1934, __KList_exit_body_'a2]
      [stack_value, ks', len, ys, xs, body] :=
      cast_exists (Es := [Set ** Set ** Set])
        (fun '[__1933, __1934, __KList_exit_body_'a2] =>
          [__1933 * __1934 ** Script_typed_ir.continuation ** int ** list a **
            list __KList_exit_body_'a2 ** Script_typed_ir.kinstr])
        [stack_value, ks', len, ys, xs, body] in
    let ks' := mk ks' in
    let extra := (body, xs, ys, len) in
    klist_exit mk g_value gas extra ks' accu_value stack_value
  
  | (Script_typed_ir.KMap_enter_body body xs ys ks', _, _) =>
    let 'existT _
      [__KMap_enter_body_'a2, __KMap_enter_body_'c2, __KMap_enter_body_'b2]
      [ks', ys, xs, body] :=
      cast_exists (Es := [Set ** Set ** Set])
        (fun
          '[__KMap_enter_body_'a2, __KMap_enter_body_'c2, __KMap_enter_body_'b2]
          =>
          [Script_typed_ir.continuation **
            Script_typed_ir.map __KMap_enter_body_'a2 __KMap_enter_body_'c2 **
            list (__KMap_enter_body_'a2 * __KMap_enter_body_'b2) **
            Script_typed_ir.kinstr]) [ks', ys, xs, body] in
    let ks' := mk ks' in
    kmap_enter mk g_value gas (body, xs, ys) ks' accu_value stack_value
  
  | (Script_typed_ir.KMap_exit_body body xs ys yk ks', _, stack_value) =>
    let 'existT _ [__1935, __1936, __KMap_exit_body_'a2, __KMap_exit_body_'b2]
      [stack_value, ks', yk, ys, xs, body] :=
      cast_exists (Es := [Set ** Set ** Set ** Set])
        (fun '[__1935, __1936, __KMap_exit_body_'a2, __KMap_exit_body_'b2] =>
          [__1935 * __1936 ** Script_typed_ir.continuation **
            __KMap_exit_body_'a2 ** Script_typed_ir.map __KMap_exit_body_'a2 a
            ** list (__KMap_exit_body_'a2 * __KMap_exit_body_'b2) **
            Script_typed_ir.kinstr]) [stack_value, ks', yk, ys, xs, body] in
    let ks' := mk ks' in
    kmap_exit mk g_value gas (body, xs, ys, yk) ks' accu_value stack_value
  
  | (Script_typed_ir.KView_exit orig_step_constants ks', _, _) =>
    let '[ks', orig_step_constants] :=
      cast [Script_typed_ir.continuation ** Script_typed_ir.step_constants]
        [ks', orig_step_constants] in
    let g_value := ((Pervasives.fst g_value), orig_step_constants) in
    next g_value gas ks' accu_value stack_value
  
  | (Script_typed_ir.KLog _ _, _, _) =>
    next g_value gas ks accu_value stack_value
  end.

Definition step_descr {A B C D : Set}
  (log_now : bool) (logger : option Script_typed_ir.logger)
  (function_parameter : Alpha_context.context * Script_typed_ir.step_constants)
  : Script_typed_ir.kdescr -> A -> B -> M? (C * D * Alpha_context.context) :=
  let '(ctxt, sc) := function_parameter in
  fun (descr_value : Script_typed_ir.kdescr) =>
    fun (accu_value : A) =>
      fun (stack_value : B) =>
        let gas := Alpha_context.Gas.remaining_operation_gas ctxt in
        let? '(accu_value, stack_value, ctxt, gas) :=
          match logger with
          | None =>
            step ((Local_gas_counter.outdated ctxt), sc) gas
              descr_value.(Script_typed_ir.kdescr.kinstr) Script_typed_ir.KNil
              accu_value stack_value
          | Some logger =>
            let '_ :=
              if log_now then
                let kinfo_value :=
                  Script_typed_ir.kinfo_of_kinstr
                    descr_value.(Script_typed_ir.kdescr.kinstr) in
                logger.(Script_typed_ir.logger.log_interp) _ _
                  descr_value.(Script_typed_ir.kdescr.kinstr) ctxt
                  kinfo_value.(Script_typed_ir.kinfo.iloc)
                  descr_value.(Script_typed_ir.kdescr.kbef)
                  (accu_value, stack_value)
              else
                tt in
            let log :=
              Script_typed_ir.ILog
                (Script_typed_ir.kinfo_of_kinstr
                  descr_value.(Script_typed_ir.kdescr.kinstr))
                Script_typed_ir.LogEntry logger
                descr_value.(Script_typed_ir.kdescr.kinstr) in
            step ((Local_gas_counter.outdated ctxt), sc) gas log
              Script_typed_ir.KNil accu_value stack_value
          end in
        return?
          (accu_value, stack_value, (Local_gas_counter.update_context gas ctxt)).

Definition interp {A B : Set}
  (logger : option Script_typed_ir.logger)
  (g_value : Alpha_context.context * Script_typed_ir.step_constants)
  (function_parameter : Script_typed_ir.lambda)
  : A -> M? (B * Alpha_context.context) :=
  let 'Script_typed_ir.Lam code _ := function_parameter in
  fun (arg : A) =>
    let? '(ret, (Script_typed_ir.EmptyCell, Script_typed_ir.EmptyCell), ctxt) :=
      step_descr true logger g_value code arg
        (Script_typed_ir.EmptyCell, Script_typed_ir.EmptyCell) in
    return? (ret, ctxt).

Definition kstep {A B C D : Set}
  (logger : option Script_typed_ir.logger) (ctxt : Alpha_context.context)
  (step_constants : Script_typed_ir.step_constants)
  (kinstr : Script_typed_ir.kinstr) (accu_value : A) (stack_value : B)
  : M? (C * D * Alpha_context.context) :=
  let gas := Alpha_context.Gas.remaining_operation_gas ctxt in
  let kinstr :=
    match logger with
    | None => kinstr
    | Some logger =>
      Script_typed_ir.ILog (Script_typed_ir.kinfo_of_kinstr kinstr)
        Script_typed_ir.LogEntry logger kinstr
    end in
  let? '(accu_value, stack_value, ctxt, gas) :=
    step ((Local_gas_counter.outdated ctxt), step_constants) gas kinstr
      Script_typed_ir.KNil accu_value stack_value in
  return? (accu_value, stack_value, (Local_gas_counter.update_context gas ctxt)).

Definition internal_step {A B C D : Set}
  (ctxt : Local_gas_counter.outdated_context)
  (step_constants : Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter) (kinstr : Script_typed_ir.kinstr)
  (accu_value : A) (stack_value : B)
  : M?
    (C * D * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter) :=
  step (ctxt, step_constants) gas kinstr Script_typed_ir.KNil accu_value
    stack_value.

Definition test_step {A B C D : Set}
  (logger : option Script_typed_ir.logger) (ctxt : Alpha_context.context)
  (step_constants : Script_typed_ir.step_constants)
  (descr_value : Script_typed_ir.kdescr) (stack_value : A)
  : B -> M? (C * D * Alpha_context.context) :=
  step_descr false logger (ctxt, step_constants) descr_value stack_value.

Definition raw_execute
  (logger : option Script_typed_ir.logger) (ctxt : Alpha_context.context)
  (mode : Script_ir_translator.unparsing_mode) (step_constants : step_constants)
  (entrypoint : string) (internal : bool)
  (unparsed_script : Alpha_context.Script.t)
  (cached_script : option Script_ir_translator.ex_script)
  (arg : Alpha_context.Script.node)
  : M?
    (Alpha_context.Script.expr * list Alpha_context.packed_internal_operation *
      Alpha_context.context * option Alpha_context.Lazy_storage.diffs *
      Script_ir_translator.ex_script * int) :=
  let?
    '(Script_ir_translator.Ex_script {|
      Script_typed_ir.script.code := code;
        Script_typed_ir.script.arg_type := arg_type;
        Script_typed_ir.script.storage := storage_value;
        Script_typed_ir.script.storage_type := storage_type;
        Script_typed_ir.script.views := views;
        Script_typed_ir.script.root_name := root_name;
        Script_typed_ir.script.code_size := code_size
        |}, ctxt) :=
    match cached_script with
    | None =>
      Script_ir_translator.parse_script None ctxt true true unparsed_script
    | Some ex_script => return? (ex_script, ctxt)
    end in
  let 'existT _ [__Ex_script_'b1, __Ex_script_'a1]
    [ctxt, code_size, root_name, views, storage_type, storage_value, arg_type,
      code] :=
    cast_exists (Es := [Set ** Set])
      (fun '[__Ex_script_'b1, __Ex_script_'a1] =>
        [Alpha_context.context ** Cache_memory_helpers.sint **
          option Script_ir_annot.field_annot **
          Script_typed_ir.SMap.(Map.S.t) Script_typed_ir.view **
          Script_typed_ir.ty ** __Ex_script_'b1 ** Script_typed_ir.ty **
          Script_typed_ir.lambda])
      [ctxt, code_size, root_name, views, storage_type, storage_value, arg_type,
        code] in
  let? '(box, _) :=
    Error_monad.record_trace
      (Build_extensible "Bad_contract_parameter" Alpha_context.Contract.t
        step_constants.(Script_typed_ir.step_constants.self))
      (Script_ir_translator.find_entrypoint arg_type root_name entrypoint) in
  let? '(arg, ctxt) :=
    Error_monad.trace_value
      (Build_extensible "Bad_contract_parameter" Alpha_context.Contract.t
        step_constants.(Script_typed_ir.step_constants.self))
      ((Script_ir_translator.parse_data :
        option Script_ir_translator.type_logger -> Alpha_context.context ->
        bool -> bool -> Script_typed_ir.ty -> Alpha_context.Script.node ->
        M? (__Ex_script_'a1 * Alpha_context.context)) None ctxt false internal
        arg_type (box arg)) in
  let? '(script_code, ctxt) :=
    Alpha_context.Script.force_decode_in_context
      Alpha_context.Script.When_needed ctxt
      unparsed_script.(Alpha_context.Script.t.code) in
  let? '(to_duplicate, ctxt) :=
    Script_ir_translator.collect_lazy_storage ctxt arg_type arg in
  let? '(to_update, ctxt) :=
    Script_ir_translator.collect_lazy_storage ctxt storage_type storage_value in
  let? '((ops, storage_value), ctxt) :=
    Error_monad.trace_value
      (Build_extensible "Runtime_contract_error"
        (Alpha_context.Contract.t * Alpha_context.Script.expr)
        (step_constants.(Script_typed_ir.step_constants.self), script_code))
      (interp logger (ctxt, step_constants) code (arg, storage_value)) in
  let? '(storage_value, lazy_storage_diff, ctxt) :=
    Script_ir_translator.extract_lazy_storage_diff ctxt mode false to_duplicate
      to_update storage_type storage_value in
  let? '(unparsed_storage, ctxt) :=
    Error_monad.trace_value
      (Build_extensible "Cannot_serialize_storage" unit tt)
      (let? '(unparsed_storage, ctxt) :=
        (Script_ir_translator.unparse_data :
          Alpha_context.context -> Script_ir_translator.unparsing_mode ->
          Script_typed_ir.ty -> __Ex_script_'b1 ->
          M? (Alpha_context.Script.node * Alpha_context.context)) ctxt mode
          storage_type storage_value in
      let? ctxt :=
        Alpha_context.Gas.consume ctxt
          (Alpha_context.Script.strip_locations_cost unparsed_storage) in
      return? ((Micheline.strip_locations unparsed_storage), ctxt)) in
  let '(ops, op_diffs) := List.split ops.(Script_typed_ir.boxed_list.elements)
    in
  let lazy_storage_diff :=
    match
      List.flatten
        (List.map (fun x_1 => Option.value_value x_1 nil)
          (Pervasives.op_at op_diffs [ lazy_storage_diff ])) with
    | [] => None
    | diff_value => Some diff_value
    end in
  let script :=
    Script_ir_translator.Ex_script
      {| Script_typed_ir.script.code := code;
        Script_typed_ir.script.arg_type := arg_type;
        Script_typed_ir.script.storage := storage_value;
        Script_typed_ir.script.storage_type := storage_type;
        Script_typed_ir.script.views := views;
        Script_typed_ir.script.root_name := root_name;
        Script_typed_ir.script.code_size := code_size |} in
  let '(size_value, cost) := Script_ir_translator.script_size script in
  let? ctxt := Alpha_context.Gas.consume ctxt cost in
  return? (unparsed_storage, ops, ctxt, lazy_storage_diff, script, size_value).

Module execution_result.
  Record record : Set := Build {
    ctxt : Alpha_context.context;
    storage : Alpha_context.Script.expr;
    lazy_storage_diff : option Alpha_context.Lazy_storage.diffs;
    operations : list Alpha_context.packed_internal_operation }.
  Definition with_ctxt ctxt (r : record) :=
    Build ctxt r.(storage) r.(lazy_storage_diff) r.(operations).
  Definition with_storage storage (r : record) :=
    Build r.(ctxt) storage r.(lazy_storage_diff) r.(operations).
  Definition with_lazy_storage_diff lazy_storage_diff (r : record) :=
    Build r.(ctxt) r.(storage) lazy_storage_diff r.(operations).
  Definition with_operations operations (r : record) :=
    Build r.(ctxt) r.(storage) r.(lazy_storage_diff) operations.
End execution_result.
Definition execution_result := execution_result.record.

Definition execute
  (logger : option Script_typed_ir.logger) (ctxt : Alpha_context.context)
  (cached_script : option Script_ir_translator.ex_script)
  (mode : Script_ir_translator.unparsing_mode) (step_constants : step_constants)
  (script : Alpha_context.Script.t) (entrypoint : string)
  (parameter : Micheline.canonical Alpha_context.Script.prim) (internal : bool)
  : M? (execution_result * (Script_ir_translator.ex_script * int)) :=
  let?
    '(storage_value, operations, ctxt, lazy_storage_diff, ex_script, approx_size) :=
    raw_execute logger ctxt mode step_constants entrypoint internal script
      cached_script (Micheline.root parameter) in
  return?
    ({| execution_result.ctxt := ctxt;
      execution_result.storage := storage_value;
      execution_result.lazy_storage_diff := lazy_storage_diff;
      execution_result.operations := operations |}, (ex_script, approx_size)).

Module Internals.
  Definition local_gas_counter : Set := Local_gas_counter.local_gas_counter.
  
  Definition outdated_context : Set := Local_gas_counter.outdated_context.
  
  Definition next {A B C D : Set}
    (logger : option Script_typed_ir.logger)
    (g_value :
      Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
    (gas : Local_gas_counter.local_gas_counter)
    (ks : Script_typed_ir.continuation) (accu_value : A) (stack_value : B)
    : M?
      (C * D * Local_gas_counter.outdated_context *
        Local_gas_counter.local_gas_counter) :=
    let ks :=
      match logger with
      | None => ks
      | Some logger => Script_typed_ir.KLog ks logger
      end in
    next g_value gas ks accu_value stack_value.
  
  Definition step {A B C D : Set}
    (function_parameter :
      Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
    : Local_gas_counter.local_gas_counter -> Script_typed_ir.kinstr -> A -> B ->
    M?
      (C * D * Local_gas_counter.outdated_context *
        Local_gas_counter.local_gas_counter) :=
    let '(ctxt, step_constants) := function_parameter in
    fun (gas : Local_gas_counter.local_gas_counter) =>
      fun (ks : Script_typed_ir.kinstr) =>
        fun (accu_value : A) =>
          fun (stack_value : B) =>
            internal_step ctxt step_constants gas ks accu_value stack_value.
End Internals.
