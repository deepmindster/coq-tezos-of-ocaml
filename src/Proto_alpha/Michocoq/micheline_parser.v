  Require Import Coq.Strings.String ZArith.
Require Import micheline_syntax location.


Require Coq.Lists.List.
From Coq.PArith Require Import BinPos.
From Coq.NArith Require Import BinNat.
Require MenhirLib.Main.
From MenhirLib Require Version.
Import List.ListNotations.

Definition version_check : unit := MenhirLib.Version.require_20211012.

Unset Elimination Schemes.

Inductive token : Type :=
| STRt :        (location * location * string)%type -> token
| SEMICOLON :        (location * location)%type -> token
| RPAREN :        (location * location)%type -> token
| RBRACE :        (location * location)%type -> token
| PRIMt :        (location * location * string)%type -> token
| NUMBERt :        (location * location * Z)%type -> token
| LPAREN :        (location * location)%type -> token
| LBRACE :        (location * location)%type -> token
| EOF :        (location * location)%type -> token
| BYTESt :        (location * location * string)%type -> token
| ANNOTATIONt :        (location * location * string)%type -> token.

Module Import Gram <: MenhirLib.Grammar.T.

Local Obligation Tactic := let x := fresh in intro x; case x; reflexivity.

Inductive terminal' : Set :=
| ANNOTATIONt't
| BYTESt't
| EOF't
| LBRACE't
| LPAREN't
| NUMBERt't
| PRIMt't
| RBRACE't
| RPAREN't
| SEMICOLON't
| STRt't.
Definition terminal := terminal'.

Program Instance terminalNum : MenhirLib.Alphabet.Numbered terminal :=
  { inj := fun x => match x return _ with
    | ANNOTATIONt't => 1%positive
    | BYTESt't => 2%positive
    | EOF't => 3%positive
    | LBRACE't => 4%positive
    | LPAREN't => 5%positive
    | NUMBERt't => 6%positive
    | PRIMt't => 7%positive
    | RBRACE't => 8%positive
    | RPAREN't => 9%positive
    | SEMICOLON't => 10%positive
    | STRt't => 11%positive
    end;
    surj := (fun n => match n return _ with
    | 1%positive => ANNOTATIONt't
    | 2%positive => BYTESt't
    | 3%positive => EOF't
    | 4%positive => LBRACE't
    | 5%positive => LPAREN't
    | 6%positive => NUMBERt't
    | 7%positive => PRIMt't
    | 8%positive => RBRACE't
    | 9%positive => RPAREN't
    | 10%positive => SEMICOLON't
    | 11%positive => STRt't
    | _ => ANNOTATIONt't
    end)%Z;
    inj_bound := 11%positive }.
Instance TerminalAlph : MenhirLib.Alphabet.Alphabet terminal := _.

Inductive nonterminal' : Set :=
| annotation'nt
| app'nt
| argument'nt
| arguments'nt
| atom'nt
| file'nt
| micheline'nt
| seq'nt
| seq_file'nt.
Definition nonterminal := nonterminal'.

Program Instance nonterminalNum : MenhirLib.Alphabet.Numbered nonterminal :=
  { inj := fun x => match x return _ with
    | annotation'nt => 1%positive
    | app'nt => 2%positive
    | argument'nt => 3%positive
    | arguments'nt => 4%positive
    | atom'nt => 5%positive
    | file'nt => 6%positive
    | micheline'nt => 7%positive
    | seq'nt => 8%positive
    | seq_file'nt => 9%positive
    end;
    surj := (fun n => match n return _ with
    | 1%positive => annotation'nt
    | 2%positive => app'nt
    | 3%positive => argument'nt
    | 4%positive => arguments'nt
    | 5%positive => atom'nt
    | 6%positive => file'nt
    | 7%positive => micheline'nt
    | 8%positive => seq'nt
    | 9%positive => seq_file'nt
    | _ => annotation'nt
    end)%Z;
    inj_bound := 9%positive }.
Instance NonTerminalAlph : MenhirLib.Alphabet.Alphabet nonterminal := _.

Include MenhirLib.Grammar.Symbol.

Definition terminal_semantic_type (t:terminal) : Type:=
  match t with
  | STRt't =>        (location * location * string)%type
  | SEMICOLON't =>        (location * location)%type
  | RPAREN't =>        (location * location)%type
  | RBRACE't =>        (location * location)%type
  | PRIMt't =>        (location * location * string)%type
  | NUMBERt't =>        (location * location * Z)%type
  | LPAREN't =>        (location * location)%type
  | LBRACE't =>        (location * location)%type
  | EOF't =>        (location * location)%type
  | BYTESt't =>        (location * location * string)%type
  | ANNOTATIONt't =>        (location * location * string)%type
  end.

Definition nonterminal_semantic_type (nt:nonterminal) : Type:=
  match nt with
  | seq_file'nt =>        (list (loc_micheline))%type
  | seq'nt =>       (list (loc_micheline) )%type
  | micheline'nt =>       (loc_micheline)%type
  | file'nt =>        (loc_micheline)%type
  | atom'nt =>       (loc_micheline)%type
  | arguments'nt =>       (location * location * list annotation * list loc_micheline )%type
  | argument'nt =>       (location * location * list annotation * list loc_micheline )%type
  | app'nt =>       (loc_micheline)%type
  | annotation'nt =>       (annotation)%type
  end.

Definition symbol_semantic_type (s:symbol) : Type:=
  match s with
  | T t => terminal_semantic_type t
  | NT nt => nonterminal_semantic_type nt
  end.

Definition token := token.

Definition token_term (tok : token) : terminal :=
  match tok with
  | STRt _ => STRt't
  | SEMICOLON _ => SEMICOLON't
  | RPAREN _ => RPAREN't
  | RBRACE _ => RBRACE't
  | PRIMt _ => PRIMt't
  | NUMBERt _ => NUMBERt't
  | LPAREN _ => LPAREN't
  | LBRACE _ => LBRACE't
  | EOF _ => EOF't
  | BYTESt _ => BYTESt't
  | ANNOTATIONt _ => ANNOTATIONt't
  end.

Definition token_sem (tok : token) : symbol_semantic_type (T (token_term tok)) :=
  match tok with
  | STRt x => x
  | SEMICOLON x => x
  | RPAREN x => x
  | RBRACE x => x
  | PRIMt x => x
  | NUMBERt x => x
  | LPAREN x => x
  | LBRACE x => x
  | EOF x => x
  | BYTESt x => x
  | ANNOTATIONt x => x
  end.

Inductive production' : Set :=
| Prod'seq_file'0
| Prod'seq'2
| Prod'seq'1
| Prod'seq'0
| Prod'micheline'1
| Prod'micheline'0
| Prod'file'0
| Prod'atom'6
| Prod'atom'5
| Prod'atom'4
| Prod'atom'3
| Prod'atom'2
| Prod'atom'1
| Prod'atom'0
| Prod'arguments'1
| Prod'arguments'0
| Prod'argument'1
| Prod'argument'0
| Prod'app'0
| Prod'annotation'0.
Definition production := production'.

Program Instance productionNum : MenhirLib.Alphabet.Numbered production :=
  { inj := fun x => match x return _ with
    | Prod'seq_file'0 => 1%positive
    | Prod'seq'2 => 2%positive
    | Prod'seq'1 => 3%positive
    | Prod'seq'0 => 4%positive
    | Prod'micheline'1 => 5%positive
    | Prod'micheline'0 => 6%positive
    | Prod'file'0 => 7%positive
    | Prod'atom'6 => 8%positive
    | Prod'atom'5 => 9%positive
    | Prod'atom'4 => 10%positive
    | Prod'atom'3 => 11%positive
    | Prod'atom'2 => 12%positive
    | Prod'atom'1 => 13%positive
    | Prod'atom'0 => 14%positive
    | Prod'arguments'1 => 15%positive
    | Prod'arguments'0 => 16%positive
    | Prod'argument'1 => 17%positive
    | Prod'argument'0 => 18%positive
    | Prod'app'0 => 19%positive
    | Prod'annotation'0 => 20%positive
    end;
    surj := (fun n => match n return _ with
    | 1%positive => Prod'seq_file'0
    | 2%positive => Prod'seq'2
    | 3%positive => Prod'seq'1
    | 4%positive => Prod'seq'0
    | 5%positive => Prod'micheline'1
    | 6%positive => Prod'micheline'0
    | 7%positive => Prod'file'0
    | 8%positive => Prod'atom'6
    | 9%positive => Prod'atom'5
    | 10%positive => Prod'atom'4
    | 11%positive => Prod'atom'3
    | 12%positive => Prod'atom'2
    | 13%positive => Prod'atom'1
    | 14%positive => Prod'atom'0
    | 15%positive => Prod'arguments'1
    | 16%positive => Prod'arguments'0
    | 17%positive => Prod'argument'1
    | 18%positive => Prod'argument'0
    | 19%positive => Prod'app'0
    | 20%positive => Prod'annotation'0
    | _ => Prod'seq_file'0
    end)%Z;
    inj_bound := 20%positive }.
Instance ProductionAlph : MenhirLib.Alphabet.Alphabet production := _.

Definition prod_contents (p:production) :
  { p:nonterminal * list symbol &
    MenhirLib.Grammar.arrows_right
      (symbol_semantic_type (NT (fst p)))
      (List.map symbol_semantic_type (snd p)) }
 :=
  let box := existT (fun p =>
    MenhirLib.Grammar.arrows_right
      (symbol_semantic_type (NT (fst p)))
      (List.map symbol_semantic_type (snd p)) )
  in
  match p with
  | Prod'annotation'0 => box
    (annotation'nt, [T ANNOTATIONt't]%list)
    (fun _1 =>
                (
      let '((b, e), s) := _1 in
      Mk_annot (b, e, s)
    )
)
  | Prod'app'0 => box
    (app'nt, [NT arguments'nt; T PRIMt't]%list)
    (fun _2 _1 =>
                    (
      let '((b, _), _) := _1 in
      let '((_, e), l1, l2) := _2 in
      Mk_loc_micheline (b, e, PRIM _1 l1 l2)
    )
)
  | Prod'argument'0 => box
    (argument'nt, [NT atom'nt]%list)
    (fun _1 =>
         ( let '(Mk_loc_micheline ((b, e), n)) := _1 in (b, e, nil, cons _1 nil) )
)
  | Prod'argument'1 => box
    (argument'nt, [NT annotation'nt]%list)
    (fun _1 =>
               ( let '(Mk_annot ((b, e), a)) := _1 in (b, e, cons _1 nil, nil) )
)
  | Prod'arguments'0 => box
    (arguments'nt, [NT argument'nt]%list)
    (fun _1 =>
             ( _1 )
)
  | Prod'arguments'1 => box
    (arguments'nt, [NT arguments'nt; NT argument'nt]%list)
    (fun _2 _1 =>
                       (
      let '((b, _), l1, l2) := _1 in
      let '((_, e), l3, l4) := _2 in
      (b, e, List.app l1 l3, List.app l2 l4)
    )
)
  | Prod'atom'0 => box
    (atom'nt, [T STRt't]%list)
    (fun _1 =>
         (
      let '((b, e), s) := _1 in
      Mk_loc_micheline (b, e, STR s)
    )
)
  | Prod'atom'1 => box
    (atom'nt, [T BYTESt't]%list)
    (fun _1 =>
           (
      let '((b, e), s) := _1 in
      Mk_loc_micheline (b, e, BYTES s)
    )
)
  | Prod'atom'2 => box
    (atom'nt, [T NUMBERt't]%list)
    (fun _1 =>
            (
      let '((b, e), z) := _1 in
      Mk_loc_micheline (b, e, NUMBER z) )
)
  | Prod'atom'3 => box
    (atom'nt, [T PRIMt't]%list)
    (fun _1 =>
          (
      let '((b, e), _) := _1 in
      Mk_loc_micheline (b, e, PRIM _1 nil nil)
    )
)
  | Prod'atom'4 => box
    (atom'nt, [T RPAREN't; T PRIMt't; T LPAREN't]%list)
    (fun _3 _2 _1 =>
                        (
      let '((b, e), _) := _2 in
      Mk_loc_micheline (b, e, PRIM _2 nil nil)
    )
)
  | Prod'atom'5 => box
    (atom'nt, [T RPAREN't; NT app'nt; T LPAREN't]%list)
    (fun _3 _2 _1 =>
                      ( _2 )
)
  | Prod'atom'6 => box
    (atom'nt, [T RBRACE't; NT seq'nt; T LBRACE't]%list)
    (fun _3 _2 _1 =>
                      (
      let '(b, _) := _1 in
      let '(_, e) := _3 in
      Mk_loc_micheline (b, e, SEQ _2) )
)
  | Prod'file'0 => box
    (file'nt, [T EOF't; NT micheline'nt]%list)
    (fun _2 _1 =>
                    ( _1 )
)
  | Prod'micheline'0 => box
    (micheline'nt, [NT atom'nt]%list)
    (fun _1 =>
         ( _1 )
)
  | Prod'micheline'1 => box
    (micheline'nt, [NT app'nt]%list)
    (fun _1 =>
        ( _1 )
)
  | Prod'seq'0 => box
    (seq'nt, []%list)
    (
    ( nil )
)
  | Prod'seq'1 => box
    (seq'nt, [NT micheline'nt]%list)
    (fun _1 =>
              ( cons _1 nil )
)
  | Prod'seq'2 => box
    (seq'nt, [NT seq'nt; T SEMICOLON't; NT micheline'nt]%list)
    (fun _3 _2 _1 =>
                            ( cons _1 _3 )
)
  | Prod'seq_file'0 => box
    (seq_file'nt, [T EOF't; NT seq'nt]%list)
    (fun _2 _1 =>
                  ( _1 )
)
  end.

Definition prod_lhs (p:production) :=
  fst (projT1 (prod_contents p)).
Definition prod_rhs_rev (p:production) :=
  snd (projT1 (prod_contents p)).
Definition prod_action (p:production) :=
  projT2 (prod_contents p).

Include MenhirLib.Grammar.Defs.

End Gram.

Module Aut <: MenhirLib.Automaton.T.

Local Obligation Tactic := let x := fresh in intro x; case x; reflexivity.

Module Gram := Gram.
Module GramDefs := Gram.

Definition nullable_nterm (nt:nonterminal) : bool :=
  match nt with
  | seq_file'nt => false
  | seq'nt => true
  | micheline'nt => false
  | file'nt => false
  | atom'nt => false
  | arguments'nt => false
  | argument'nt => false
  | app'nt => false
  | annotation'nt => false
  end.

Definition first_nterm (nt:nonterminal) : list terminal :=
  match nt with
  | seq_file'nt => [STRt't; PRIMt't; NUMBERt't; LPAREN't; LBRACE't; EOF't; BYTESt't]%list
  | seq'nt => [STRt't; PRIMt't; NUMBERt't; LPAREN't; LBRACE't; BYTESt't]%list
  | micheline'nt => [STRt't; PRIMt't; NUMBERt't; LPAREN't; LBRACE't; BYTESt't]%list
  | file'nt => [STRt't; PRIMt't; NUMBERt't; LPAREN't; LBRACE't; BYTESt't]%list
  | atom'nt => [STRt't; PRIMt't; NUMBERt't; LPAREN't; LBRACE't; BYTESt't]%list
  | arguments'nt => [STRt't; PRIMt't; NUMBERt't; LPAREN't; LBRACE't; BYTESt't; ANNOTATIONt't]%list
  | argument'nt => [STRt't; PRIMt't; NUMBERt't; LPAREN't; LBRACE't; BYTESt't; ANNOTATIONt't]%list
  | app'nt => [PRIMt't]%list
  | annotation'nt => [ANNOTATIONt't]%list
  end.

Inductive noninitstate' : Set :=
| Nis'31
| Nis'30
| Nis'26
| Nis'25
| Nis'24
| Nis'23
| Nis'22
| Nis'21
| Nis'20
| Nis'19
| Nis'18
| Nis'17
| Nis'16
| Nis'15
| Nis'14
| Nis'13
| Nis'12
| Nis'11
| Nis'10
| Nis'9
| Nis'8
| Nis'7
| Nis'6
| Nis'5
| Nis'4
| Nis'3
| Nis'2
| Nis'1.
Definition noninitstate := noninitstate'.

Program Instance noninitstateNum : MenhirLib.Alphabet.Numbered noninitstate :=
  { inj := fun x => match x return _ with
    | Nis'31 => 1%positive
    | Nis'30 => 2%positive
    | Nis'26 => 3%positive
    | Nis'25 => 4%positive
    | Nis'24 => 5%positive
    | Nis'23 => 6%positive
    | Nis'22 => 7%positive
    | Nis'21 => 8%positive
    | Nis'20 => 9%positive
    | Nis'19 => 10%positive
    | Nis'18 => 11%positive
    | Nis'17 => 12%positive
    | Nis'16 => 13%positive
    | Nis'15 => 14%positive
    | Nis'14 => 15%positive
    | Nis'13 => 16%positive
    | Nis'12 => 17%positive
    | Nis'11 => 18%positive
    | Nis'10 => 19%positive
    | Nis'9 => 20%positive
    | Nis'8 => 21%positive
    | Nis'7 => 22%positive
    | Nis'6 => 23%positive
    | Nis'5 => 24%positive
    | Nis'4 => 25%positive
    | Nis'3 => 26%positive
    | Nis'2 => 27%positive
    | Nis'1 => 28%positive
    end;
    surj := (fun n => match n return _ with
    | 1%positive => Nis'31
    | 2%positive => Nis'30
    | 3%positive => Nis'26
    | 4%positive => Nis'25
    | 5%positive => Nis'24
    | 6%positive => Nis'23
    | 7%positive => Nis'22
    | 8%positive => Nis'21
    | 9%positive => Nis'20
    | 10%positive => Nis'19
    | 11%positive => Nis'18
    | 12%positive => Nis'17
    | 13%positive => Nis'16
    | 14%positive => Nis'15
    | 15%positive => Nis'14
    | 16%positive => Nis'13
    | 17%positive => Nis'12
    | 18%positive => Nis'11
    | 19%positive => Nis'10
    | 20%positive => Nis'9
    | 21%positive => Nis'8
    | 22%positive => Nis'7
    | 23%positive => Nis'6
    | 24%positive => Nis'5
    | 25%positive => Nis'4
    | 26%positive => Nis'3
    | 27%positive => Nis'2
    | 28%positive => Nis'1
    | _ => Nis'31
    end)%Z;
    inj_bound := 28%positive }.
Instance NonInitStateAlph : MenhirLib.Alphabet.Alphabet noninitstate := _.

Definition last_symb_of_non_init_state (noninitstate:noninitstate) : symbol :=
  match noninitstate with
  | Nis'1 => T STRt't
  | Nis'2 => T PRIMt't
  | Nis'3 => T PRIMt't
  | Nis'4 => T NUMBERt't
  | Nis'5 => T LPAREN't
  | Nis'6 => T PRIMt't
  | Nis'7 => T RPAREN't
  | Nis'8 => T LBRACE't
  | Nis'9 => T BYTESt't
  | Nis'10 => NT seq'nt
  | Nis'11 => T RBRACE't
  | Nis'12 => NT micheline'nt
  | Nis'13 => T SEMICOLON't
  | Nis'14 => NT seq'nt
  | Nis'15 => NT atom'nt
  | Nis'16 => NT app'nt
  | Nis'17 => T ANNOTATIONt't
  | Nis'18 => NT atom'nt
  | Nis'19 => NT arguments'nt
  | Nis'20 => NT argument'nt
  | Nis'21 => NT arguments'nt
  | Nis'22 => NT annotation'nt
  | Nis'23 => NT app'nt
  | Nis'24 => T RPAREN't
  | Nis'25 => NT micheline'nt
  | Nis'26 => T EOF't
  | Nis'30 => NT seq'nt
  | Nis'31 => T EOF't
  end.

Inductive initstate' : Set :=
| Init'28
| Init'0.
Definition initstate := initstate'.

Program Instance initstateNum : MenhirLib.Alphabet.Numbered initstate :=
  { inj := fun x => match x return _ with
    | Init'28 => 1%positive
    | Init'0 => 2%positive
    end;
    surj := (fun n => match n return _ with
    | 1%positive => Init'28
    | 2%positive => Init'0
    | _ => Init'28
    end)%Z;
    inj_bound := 2%positive }.
Instance InitStateAlph : MenhirLib.Alphabet.Alphabet initstate := _.

Include MenhirLib.Automaton.Types.

Definition start_nt (init:initstate) : nonterminal :=
  match init with
  | Init'0 => file'nt
  | Init'28 => seq_file'nt
  end.

Definition action_table (state:state) : action :=
  match state with
  | Init Init'0 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | STRt't => Shift_act Nis'1 (eq_refl _)
    | PRIMt't => Shift_act Nis'2 (eq_refl _)
    | NUMBERt't => Shift_act Nis'4 (eq_refl _)
    | LPAREN't => Shift_act Nis'5 (eq_refl _)
    | LBRACE't => Shift_act Nis'8 (eq_refl _)
    | BYTESt't => Shift_act Nis'9 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'1 => Default_reduce_act Prod'atom'0
  | Ninit Nis'2 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | STRt't => Shift_act Nis'1 (eq_refl _)
    | SEMICOLON't => Reduce_act Prod'atom'3
    | RBRACE't => Reduce_act Prod'atom'3
    | PRIMt't => Shift_act Nis'3 (eq_refl _)
    | NUMBERt't => Shift_act Nis'4 (eq_refl _)
    | LPAREN't => Shift_act Nis'5 (eq_refl _)
    | LBRACE't => Shift_act Nis'8 (eq_refl _)
    | EOF't => Reduce_act Prod'atom'3
    | BYTESt't => Shift_act Nis'9 (eq_refl _)
    | ANNOTATIONt't => Shift_act Nis'17 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'3 => Default_reduce_act Prod'atom'3
  | Ninit Nis'4 => Default_reduce_act Prod'atom'2
  | Ninit Nis'5 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | PRIMt't => Shift_act Nis'6 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'6 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | STRt't => Shift_act Nis'1 (eq_refl _)
    | RPAREN't => Shift_act Nis'7 (eq_refl _)
    | PRIMt't => Shift_act Nis'3 (eq_refl _)
    | NUMBERt't => Shift_act Nis'4 (eq_refl _)
    | LPAREN't => Shift_act Nis'5 (eq_refl _)
    | LBRACE't => Shift_act Nis'8 (eq_refl _)
    | BYTESt't => Shift_act Nis'9 (eq_refl _)
    | ANNOTATIONt't => Shift_act Nis'17 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'7 => Default_reduce_act Prod'atom'4
  | Ninit Nis'8 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | STRt't => Shift_act Nis'1 (eq_refl _)
    | RBRACE't => Reduce_act Prod'seq'0
    | PRIMt't => Shift_act Nis'2 (eq_refl _)
    | NUMBERt't => Shift_act Nis'4 (eq_refl _)
    | LPAREN't => Shift_act Nis'5 (eq_refl _)
    | LBRACE't => Shift_act Nis'8 (eq_refl _)
    | BYTESt't => Shift_act Nis'9 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'9 => Default_reduce_act Prod'atom'1
  | Ninit Nis'10 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | RBRACE't => Shift_act Nis'11 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'11 => Default_reduce_act Prod'atom'6
  | Ninit Nis'12 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | SEMICOLON't => Shift_act Nis'13 (eq_refl _)
    | RBRACE't => Reduce_act Prod'seq'1
    | EOF't => Reduce_act Prod'seq'1
    | _ => Fail_act
    end)
  | Ninit Nis'13 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | STRt't => Shift_act Nis'1 (eq_refl _)
    | RBRACE't => Reduce_act Prod'seq'0
    | PRIMt't => Shift_act Nis'2 (eq_refl _)
    | NUMBERt't => Shift_act Nis'4 (eq_refl _)
    | LPAREN't => Shift_act Nis'5 (eq_refl _)
    | LBRACE't => Shift_act Nis'8 (eq_refl _)
    | EOF't => Reduce_act Prod'seq'0
    | BYTESt't => Shift_act Nis'9 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'14 => Default_reduce_act Prod'seq'2
  | Ninit Nis'15 => Default_reduce_act Prod'micheline'0
  | Ninit Nis'16 => Default_reduce_act Prod'micheline'1
  | Ninit Nis'17 => Default_reduce_act Prod'annotation'0
  | Ninit Nis'18 => Default_reduce_act Prod'argument'0
  | Ninit Nis'19 => Default_reduce_act Prod'app'0
  | Ninit Nis'20 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | STRt't => Shift_act Nis'1 (eq_refl _)
    | SEMICOLON't => Reduce_act Prod'arguments'0
    | RPAREN't => Reduce_act Prod'arguments'0
    | RBRACE't => Reduce_act Prod'arguments'0
    | PRIMt't => Shift_act Nis'3 (eq_refl _)
    | NUMBERt't => Shift_act Nis'4 (eq_refl _)
    | LPAREN't => Shift_act Nis'5 (eq_refl _)
    | LBRACE't => Shift_act Nis'8 (eq_refl _)
    | EOF't => Reduce_act Prod'arguments'0
    | BYTESt't => Shift_act Nis'9 (eq_refl _)
    | ANNOTATIONt't => Shift_act Nis'17 (eq_refl _)
    end)
  | Ninit Nis'21 => Default_reduce_act Prod'arguments'1
  | Ninit Nis'22 => Default_reduce_act Prod'argument'1
  | Ninit Nis'23 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | RPAREN't => Shift_act Nis'24 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'24 => Default_reduce_act Prod'atom'5
  | Ninit Nis'25 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | EOF't => Shift_act Nis'26 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'26 => Default_reduce_act Prod'file'0
  | Init Init'28 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | STRt't => Shift_act Nis'1 (eq_refl _)
    | PRIMt't => Shift_act Nis'2 (eq_refl _)
    | NUMBERt't => Shift_act Nis'4 (eq_refl _)
    | LPAREN't => Shift_act Nis'5 (eq_refl _)
    | LBRACE't => Shift_act Nis'8 (eq_refl _)
    | EOF't => Reduce_act Prod'seq'0
    | BYTESt't => Shift_act Nis'9 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'30 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | EOF't => Shift_act Nis'31 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'31 => Default_reduce_act Prod'seq_file'0
  end.

Definition goto_table (state:state) (nt:nonterminal) :=
  match state, nt return option { s:noninitstate | NT nt = last_symb_of_non_init_state s } with
  | Init Init'0, micheline'nt => Some (exist _ Nis'25 (eq_refl _))
  | Init Init'0, file'nt => None  | Init Init'0, atom'nt => Some (exist _ Nis'15 (eq_refl _))
  | Init Init'0, app'nt => Some (exist _ Nis'16 (eq_refl _))
  | Ninit Nis'2, atom'nt => Some (exist _ Nis'18 (eq_refl _))
  | Ninit Nis'2, arguments'nt => Some (exist _ Nis'19 (eq_refl _))
  | Ninit Nis'2, argument'nt => Some (exist _ Nis'20 (eq_refl _))
  | Ninit Nis'2, annotation'nt => Some (exist _ Nis'22 (eq_refl _))
  | Ninit Nis'5, app'nt => Some (exist _ Nis'23 (eq_refl _))
  | Ninit Nis'6, atom'nt => Some (exist _ Nis'18 (eq_refl _))
  | Ninit Nis'6, arguments'nt => Some (exist _ Nis'19 (eq_refl _))
  | Ninit Nis'6, argument'nt => Some (exist _ Nis'20 (eq_refl _))
  | Ninit Nis'6, annotation'nt => Some (exist _ Nis'22 (eq_refl _))
  | Ninit Nis'8, seq'nt => Some (exist _ Nis'10 (eq_refl _))
  | Ninit Nis'8, micheline'nt => Some (exist _ Nis'12 (eq_refl _))
  | Ninit Nis'8, atom'nt => Some (exist _ Nis'15 (eq_refl _))
  | Ninit Nis'8, app'nt => Some (exist _ Nis'16 (eq_refl _))
  | Ninit Nis'13, seq'nt => Some (exist _ Nis'14 (eq_refl _))
  | Ninit Nis'13, micheline'nt => Some (exist _ Nis'12 (eq_refl _))
  | Ninit Nis'13, atom'nt => Some (exist _ Nis'15 (eq_refl _))
  | Ninit Nis'13, app'nt => Some (exist _ Nis'16 (eq_refl _))
  | Ninit Nis'20, atom'nt => Some (exist _ Nis'18 (eq_refl _))
  | Ninit Nis'20, arguments'nt => Some (exist _ Nis'21 (eq_refl _))
  | Ninit Nis'20, argument'nt => Some (exist _ Nis'20 (eq_refl _))
  | Ninit Nis'20, annotation'nt => Some (exist _ Nis'22 (eq_refl _))
  | Init Init'28, seq_file'nt => None  | Init Init'28, seq'nt => Some (exist _ Nis'30 (eq_refl _))
  | Init Init'28, micheline'nt => Some (exist _ Nis'12 (eq_refl _))
  | Init Init'28, atom'nt => Some (exist _ Nis'15 (eq_refl _))
  | Init Init'28, app'nt => Some (exist _ Nis'16 (eq_refl _))
  | _, _ => None
  end.

Definition past_symb_of_non_init_state (noninitstate:noninitstate) : list symbol :=
  match noninitstate with
  | Nis'1 => []%list
  | Nis'2 => []%list
  | Nis'3 => []%list
  | Nis'4 => []%list
  | Nis'5 => []%list
  | Nis'6 => [T LPAREN't]%list
  | Nis'7 => [T PRIMt't; T LPAREN't]%list
  | Nis'8 => []%list
  | Nis'9 => []%list
  | Nis'10 => [T LBRACE't]%list
  | Nis'11 => [NT seq'nt; T LBRACE't]%list
  | Nis'12 => []%list
  | Nis'13 => [NT micheline'nt]%list
  | Nis'14 => [T SEMICOLON't; NT micheline'nt]%list
  | Nis'15 => []%list
  | Nis'16 => []%list
  | Nis'17 => []%list
  | Nis'18 => []%list
  | Nis'19 => [T PRIMt't]%list
  | Nis'20 => []%list
  | Nis'21 => [NT argument'nt]%list
  | Nis'22 => []%list
  | Nis'23 => [T LPAREN't]%list
  | Nis'24 => [NT app'nt; T LPAREN't]%list
  | Nis'25 => []%list
  | Nis'26 => [NT micheline'nt]%list
  | Nis'30 => []%list
  | Nis'31 => [NT seq'nt]%list
  end.
Extract Constant past_symb_of_non_init_state => "fun _ -> assert false".

Definition state_set_1 (s:state) : bool :=
  match s with
  | Init Init'0 | Ninit Nis'2 | Ninit Nis'6 | Ninit Nis'8 | Ninit Nis'13 | Ninit Nis'20 | Init Init'28 => true
  | _ => false
  end.
Extract Inlined Constant state_set_1 => "assert false".

Definition state_set_2 (s:state) : bool :=
  match s with
  | Init Init'0 | Ninit Nis'8 | Ninit Nis'13 | Init Init'28 => true
  | _ => false
  end.
Extract Inlined Constant state_set_2 => "assert false".

Definition state_set_3 (s:state) : bool :=
  match s with
  | Ninit Nis'2 | Ninit Nis'6 | Ninit Nis'20 => true
  | _ => false
  end.
Extract Inlined Constant state_set_3 => "assert false".

Definition state_set_4 (s:state) : bool :=
  match s with
  | Ninit Nis'5 => true
  | _ => false
  end.
Extract Inlined Constant state_set_4 => "assert false".

Definition state_set_5 (s:state) : bool :=
  match s with
  | Ninit Nis'6 => true
  | _ => false
  end.
Extract Inlined Constant state_set_5 => "assert false".

Definition state_set_6 (s:state) : bool :=
  match s with
  | Ninit Nis'8 => true
  | _ => false
  end.
Extract Inlined Constant state_set_6 => "assert false".

Definition state_set_7 (s:state) : bool :=
  match s with
  | Ninit Nis'10 => true
  | _ => false
  end.
Extract Inlined Constant state_set_7 => "assert false".

Definition state_set_8 (s:state) : bool :=
  match s with
  | Ninit Nis'8 | Ninit Nis'13 | Init Init'28 => true
  | _ => false
  end.
Extract Inlined Constant state_set_8 => "assert false".

Definition state_set_9 (s:state) : bool :=
  match s with
  | Ninit Nis'12 => true
  | _ => false
  end.
Extract Inlined Constant state_set_9 => "assert false".

Definition state_set_10 (s:state) : bool :=
  match s with
  | Ninit Nis'13 => true
  | _ => false
  end.
Extract Inlined Constant state_set_10 => "assert false".

Definition state_set_11 (s:state) : bool :=
  match s with
  | Init Init'0 | Ninit Nis'5 | Ninit Nis'8 | Ninit Nis'13 | Init Init'28 => true
  | _ => false
  end.
Extract Inlined Constant state_set_11 => "assert false".

Definition state_set_12 (s:state) : bool :=
  match s with
  | Ninit Nis'2 | Ninit Nis'6 => true
  | _ => false
  end.
Extract Inlined Constant state_set_12 => "assert false".

Definition state_set_13 (s:state) : bool :=
  match s with
  | Ninit Nis'20 => true
  | _ => false
  end.
Extract Inlined Constant state_set_13 => "assert false".

Definition state_set_14 (s:state) : bool :=
  match s with
  | Ninit Nis'23 => true
  | _ => false
  end.
Extract Inlined Constant state_set_14 => "assert false".

Definition state_set_15 (s:state) : bool :=
  match s with
  | Init Init'0 => true
  | _ => false
  end.
Extract Inlined Constant state_set_15 => "assert false".

Definition state_set_16 (s:state) : bool :=
  match s with
  | Ninit Nis'25 => true
  | _ => false
  end.
Extract Inlined Constant state_set_16 => "assert false".

Definition state_set_17 (s:state) : bool :=
  match s with
  | Init Init'28 => true
  | _ => false
  end.
Extract Inlined Constant state_set_17 => "assert false".

Definition state_set_18 (s:state) : bool :=
  match s with
  | Ninit Nis'30 => true
  | _ => false
  end.
Extract Inlined Constant state_set_18 => "assert false".

Definition past_state_of_non_init_state (s:noninitstate) : list (state -> bool) :=
  match s with
  | Nis'1 => [ state_set_1 ]%list
  | Nis'2 => [ state_set_2 ]%list
  | Nis'3 => [ state_set_3 ]%list
  | Nis'4 => [ state_set_1 ]%list
  | Nis'5 => [ state_set_1 ]%list
  | Nis'6 => [ state_set_4; state_set_1 ]%list
  | Nis'7 => [ state_set_5; state_set_4; state_set_1 ]%list
  | Nis'8 => [ state_set_1 ]%list
  | Nis'9 => [ state_set_1 ]%list
  | Nis'10 => [ state_set_6; state_set_1 ]%list
  | Nis'11 => [ state_set_7; state_set_6; state_set_1 ]%list
  | Nis'12 => [ state_set_8 ]%list
  | Nis'13 => [ state_set_9; state_set_8 ]%list
  | Nis'14 => [ state_set_10; state_set_9; state_set_8 ]%list
  | Nis'15 => [ state_set_2 ]%list
  | Nis'16 => [ state_set_2 ]%list
  | Nis'17 => [ state_set_3 ]%list
  | Nis'18 => [ state_set_3 ]%list
  | Nis'19 => [ state_set_12; state_set_11 ]%list
  | Nis'20 => [ state_set_3 ]%list
  | Nis'21 => [ state_set_13; state_set_3 ]%list
  | Nis'22 => [ state_set_3 ]%list
  | Nis'23 => [ state_set_4; state_set_1 ]%list
  | Nis'24 => [ state_set_14; state_set_4; state_set_1 ]%list
  | Nis'25 => [ state_set_15 ]%list
  | Nis'26 => [ state_set_16; state_set_15 ]%list
  | Nis'30 => [ state_set_17 ]%list
  | Nis'31 => [ state_set_18; state_set_17 ]%list
  end.
Extract Constant past_state_of_non_init_state => "fun _ -> assert false".

Definition lookahead_set_1 : list terminal :=
  [EOF't]%list.
Extract Inlined Constant lookahead_set_1 => "assert false".

Definition lookahead_set_2 : list terminal :=
  [STRt't; SEMICOLON't; RPAREN't; RBRACE't; PRIMt't; NUMBERt't; LPAREN't; LBRACE't; EOF't; BYTESt't; ANNOTATIONt't]%list.
Extract Inlined Constant lookahead_set_2 => "assert false".

Definition lookahead_set_3 : list terminal :=
  [STRt't; SEMICOLON't; RBRACE't; PRIMt't; NUMBERt't; LPAREN't; LBRACE't; EOF't; BYTESt't; ANNOTATIONt't]%list.
Extract Inlined Constant lookahead_set_3 => "assert false".

Definition lookahead_set_4 : list terminal :=
  [SEMICOLON't; RBRACE't; EOF't]%list.
Extract Inlined Constant lookahead_set_4 => "assert false".

Definition lookahead_set_5 : list terminal :=
  [RPAREN't]%list.
Extract Inlined Constant lookahead_set_5 => "assert false".

Definition lookahead_set_6 : list terminal :=
  [STRt't; RPAREN't; PRIMt't; NUMBERt't; LPAREN't; LBRACE't; BYTESt't; ANNOTATIONt't]%list.
Extract Inlined Constant lookahead_set_6 => "assert false".

Definition lookahead_set_7 : list terminal :=
  [SEMICOLON't; RBRACE't]%list.
Extract Inlined Constant lookahead_set_7 => "assert false".

Definition lookahead_set_8 : list terminal :=
  [RBRACE't]%list.
Extract Inlined Constant lookahead_set_8 => "assert false".

Definition lookahead_set_9 : list terminal :=
  [RBRACE't; EOF't]%list.
Extract Inlined Constant lookahead_set_9 => "assert false".

Definition lookahead_set_10 : list terminal :=
  [SEMICOLON't; RPAREN't; RBRACE't; EOF't]%list.
Extract Inlined Constant lookahead_set_10 => "assert false".

Definition lookahead_set_11 : list terminal :=
  [SEMICOLON't; EOF't]%list.
Extract Inlined Constant lookahead_set_11 => "assert false".

Definition items_of_state_0 : list item :=
  [ {| prod_item := Prod'app'0; dot_pos_item := 0; lookaheads_item := lookahead_set_1 |};
    {| prod_item := Prod'atom'0; dot_pos_item := 0; lookaheads_item := lookahead_set_1 |};
    {| prod_item := Prod'atom'1; dot_pos_item := 0; lookaheads_item := lookahead_set_1 |};
    {| prod_item := Prod'atom'2; dot_pos_item := 0; lookaheads_item := lookahead_set_1 |};
    {| prod_item := Prod'atom'3; dot_pos_item := 0; lookaheads_item := lookahead_set_1 |};
    {| prod_item := Prod'atom'4; dot_pos_item := 0; lookaheads_item := lookahead_set_1 |};
    {| prod_item := Prod'atom'5; dot_pos_item := 0; lookaheads_item := lookahead_set_1 |};
    {| prod_item := Prod'atom'6; dot_pos_item := 0; lookaheads_item := lookahead_set_1 |};
    {| prod_item := Prod'file'0; dot_pos_item := 0; lookaheads_item := lookahead_set_2 |};
    {| prod_item := Prod'micheline'0; dot_pos_item := 0; lookaheads_item := lookahead_set_1 |};
    {| prod_item := Prod'micheline'1; dot_pos_item := 0; lookaheads_item := lookahead_set_1 |} ]%list.
Extract Inlined Constant items_of_state_0 => "assert false".

Definition items_of_state_1 : list item :=
  [ {| prod_item := Prod'atom'0; dot_pos_item := 1; lookaheads_item := lookahead_set_2 |} ]%list.
Extract Inlined Constant items_of_state_1 => "assert false".

Definition items_of_state_2 : list item :=
  [ {| prod_item := Prod'annotation'0; dot_pos_item := 0; lookaheads_item := lookahead_set_3 |};
    {| prod_item := Prod'app'0; dot_pos_item := 1; lookaheads_item := lookahead_set_4 |};
    {| prod_item := Prod'argument'0; dot_pos_item := 0; lookaheads_item := lookahead_set_3 |};
    {| prod_item := Prod'argument'1; dot_pos_item := 0; lookaheads_item := lookahead_set_3 |};
    {| prod_item := Prod'arguments'0; dot_pos_item := 0; lookaheads_item := lookahead_set_4 |};
    {| prod_item := Prod'arguments'1; dot_pos_item := 0; lookaheads_item := lookahead_set_4 |};
    {| prod_item := Prod'atom'0; dot_pos_item := 0; lookaheads_item := lookahead_set_3 |};
    {| prod_item := Prod'atom'1; dot_pos_item := 0; lookaheads_item := lookahead_set_3 |};
    {| prod_item := Prod'atom'2; dot_pos_item := 0; lookaheads_item := lookahead_set_3 |};
    {| prod_item := Prod'atom'3; dot_pos_item := 0; lookaheads_item := lookahead_set_3 |};
    {| prod_item := Prod'atom'3; dot_pos_item := 1; lookaheads_item := lookahead_set_4 |};
    {| prod_item := Prod'atom'4; dot_pos_item := 0; lookaheads_item := lookahead_set_3 |};
    {| prod_item := Prod'atom'5; dot_pos_item := 0; lookaheads_item := lookahead_set_3 |};
    {| prod_item := Prod'atom'6; dot_pos_item := 0; lookaheads_item := lookahead_set_3 |} ]%list.
Extract Inlined Constant items_of_state_2 => "assert false".

Definition items_of_state_3 : list item :=
  [ {| prod_item := Prod'atom'3; dot_pos_item := 1; lookaheads_item := lookahead_set_2 |} ]%list.
Extract Inlined Constant items_of_state_3 => "assert false".

Definition items_of_state_4 : list item :=
  [ {| prod_item := Prod'atom'2; dot_pos_item := 1; lookaheads_item := lookahead_set_2 |} ]%list.
Extract Inlined Constant items_of_state_4 => "assert false".

Definition items_of_state_5 : list item :=
  [ {| prod_item := Prod'app'0; dot_pos_item := 0; lookaheads_item := lookahead_set_5 |};
    {| prod_item := Prod'atom'4; dot_pos_item := 1; lookaheads_item := lookahead_set_2 |};
    {| prod_item := Prod'atom'5; dot_pos_item := 1; lookaheads_item := lookahead_set_2 |} ]%list.
Extract Inlined Constant items_of_state_5 => "assert false".

Definition items_of_state_6 : list item :=
  [ {| prod_item := Prod'annotation'0; dot_pos_item := 0; lookaheads_item := lookahead_set_6 |};
    {| prod_item := Prod'app'0; dot_pos_item := 1; lookaheads_item := lookahead_set_5 |};
    {| prod_item := Prod'argument'0; dot_pos_item := 0; lookaheads_item := lookahead_set_6 |};
    {| prod_item := Prod'argument'1; dot_pos_item := 0; lookaheads_item := lookahead_set_6 |};
    {| prod_item := Prod'arguments'0; dot_pos_item := 0; lookaheads_item := lookahead_set_5 |};
    {| prod_item := Prod'arguments'1; dot_pos_item := 0; lookaheads_item := lookahead_set_5 |};
    {| prod_item := Prod'atom'0; dot_pos_item := 0; lookaheads_item := lookahead_set_6 |};
    {| prod_item := Prod'atom'1; dot_pos_item := 0; lookaheads_item := lookahead_set_6 |};
    {| prod_item := Prod'atom'2; dot_pos_item := 0; lookaheads_item := lookahead_set_6 |};
    {| prod_item := Prod'atom'3; dot_pos_item := 0; lookaheads_item := lookahead_set_6 |};
    {| prod_item := Prod'atom'4; dot_pos_item := 0; lookaheads_item := lookahead_set_6 |};
    {| prod_item := Prod'atom'4; dot_pos_item := 2; lookaheads_item := lookahead_set_2 |};
    {| prod_item := Prod'atom'5; dot_pos_item := 0; lookaheads_item := lookahead_set_6 |};
    {| prod_item := Prod'atom'6; dot_pos_item := 0; lookaheads_item := lookahead_set_6 |} ]%list.
Extract Inlined Constant items_of_state_6 => "assert false".

Definition items_of_state_7 : list item :=
  [ {| prod_item := Prod'atom'4; dot_pos_item := 3; lookaheads_item := lookahead_set_2 |} ]%list.
Extract Inlined Constant items_of_state_7 => "assert false".

Definition items_of_state_8 : list item :=
  [ {| prod_item := Prod'app'0; dot_pos_item := 0; lookaheads_item := lookahead_set_7 |};
    {| prod_item := Prod'atom'0; dot_pos_item := 0; lookaheads_item := lookahead_set_7 |};
    {| prod_item := Prod'atom'1; dot_pos_item := 0; lookaheads_item := lookahead_set_7 |};
    {| prod_item := Prod'atom'2; dot_pos_item := 0; lookaheads_item := lookahead_set_7 |};
    {| prod_item := Prod'atom'3; dot_pos_item := 0; lookaheads_item := lookahead_set_7 |};
    {| prod_item := Prod'atom'4; dot_pos_item := 0; lookaheads_item := lookahead_set_7 |};
    {| prod_item := Prod'atom'5; dot_pos_item := 0; lookaheads_item := lookahead_set_7 |};
    {| prod_item := Prod'atom'6; dot_pos_item := 0; lookaheads_item := lookahead_set_7 |};
    {| prod_item := Prod'atom'6; dot_pos_item := 1; lookaheads_item := lookahead_set_2 |};
    {| prod_item := Prod'micheline'0; dot_pos_item := 0; lookaheads_item := lookahead_set_7 |};
    {| prod_item := Prod'micheline'1; dot_pos_item := 0; lookaheads_item := lookahead_set_7 |};
    {| prod_item := Prod'seq'0; dot_pos_item := 0; lookaheads_item := lookahead_set_8 |};
    {| prod_item := Prod'seq'1; dot_pos_item := 0; lookaheads_item := lookahead_set_8 |};
    {| prod_item := Prod'seq'2; dot_pos_item := 0; lookaheads_item := lookahead_set_8 |} ]%list.
Extract Inlined Constant items_of_state_8 => "assert false".

Definition items_of_state_9 : list item :=
  [ {| prod_item := Prod'atom'1; dot_pos_item := 1; lookaheads_item := lookahead_set_2 |} ]%list.
Extract Inlined Constant items_of_state_9 => "assert false".

Definition items_of_state_10 : list item :=
  [ {| prod_item := Prod'atom'6; dot_pos_item := 2; lookaheads_item := lookahead_set_2 |} ]%list.
Extract Inlined Constant items_of_state_10 => "assert false".

Definition items_of_state_11 : list item :=
  [ {| prod_item := Prod'atom'6; dot_pos_item := 3; lookaheads_item := lookahead_set_2 |} ]%list.
Extract Inlined Constant items_of_state_11 => "assert false".

Definition items_of_state_12 : list item :=
  [ {| prod_item := Prod'seq'1; dot_pos_item := 1; lookaheads_item := lookahead_set_9 |};
    {| prod_item := Prod'seq'2; dot_pos_item := 1; lookaheads_item := lookahead_set_9 |} ]%list.
Extract Inlined Constant items_of_state_12 => "assert false".

Definition items_of_state_13 : list item :=
  [ {| prod_item := Prod'app'0; dot_pos_item := 0; lookaheads_item := lookahead_set_4 |};
    {| prod_item := Prod'atom'0; dot_pos_item := 0; lookaheads_item := lookahead_set_4 |};
    {| prod_item := Prod'atom'1; dot_pos_item := 0; lookaheads_item := lookahead_set_4 |};
    {| prod_item := Prod'atom'2; dot_pos_item := 0; lookaheads_item := lookahead_set_4 |};
    {| prod_item := Prod'atom'3; dot_pos_item := 0; lookaheads_item := lookahead_set_4 |};
    {| prod_item := Prod'atom'4; dot_pos_item := 0; lookaheads_item := lookahead_set_4 |};
    {| prod_item := Prod'atom'5; dot_pos_item := 0; lookaheads_item := lookahead_set_4 |};
    {| prod_item := Prod'atom'6; dot_pos_item := 0; lookaheads_item := lookahead_set_4 |};
    {| prod_item := Prod'micheline'0; dot_pos_item := 0; lookaheads_item := lookahead_set_4 |};
    {| prod_item := Prod'micheline'1; dot_pos_item := 0; lookaheads_item := lookahead_set_4 |};
    {| prod_item := Prod'seq'0; dot_pos_item := 0; lookaheads_item := lookahead_set_9 |};
    {| prod_item := Prod'seq'1; dot_pos_item := 0; lookaheads_item := lookahead_set_9 |};
    {| prod_item := Prod'seq'2; dot_pos_item := 0; lookaheads_item := lookahead_set_9 |};
    {| prod_item := Prod'seq'2; dot_pos_item := 2; lookaheads_item := lookahead_set_9 |} ]%list.
Extract Inlined Constant items_of_state_13 => "assert false".

Definition items_of_state_14 : list item :=
  [ {| prod_item := Prod'seq'2; dot_pos_item := 3; lookaheads_item := lookahead_set_9 |} ]%list.
Extract Inlined Constant items_of_state_14 => "assert false".

Definition items_of_state_15 : list item :=
  [ {| prod_item := Prod'micheline'0; dot_pos_item := 1; lookaheads_item := lookahead_set_4 |} ]%list.
Extract Inlined Constant items_of_state_15 => "assert false".

Definition items_of_state_16 : list item :=
  [ {| prod_item := Prod'micheline'1; dot_pos_item := 1; lookaheads_item := lookahead_set_4 |} ]%list.
Extract Inlined Constant items_of_state_16 => "assert false".

Definition items_of_state_17 : list item :=
  [ {| prod_item := Prod'annotation'0; dot_pos_item := 1; lookaheads_item := lookahead_set_2 |} ]%list.
Extract Inlined Constant items_of_state_17 => "assert false".

Definition items_of_state_18 : list item :=
  [ {| prod_item := Prod'argument'0; dot_pos_item := 1; lookaheads_item := lookahead_set_2 |} ]%list.
Extract Inlined Constant items_of_state_18 => "assert false".

Definition items_of_state_19 : list item :=
  [ {| prod_item := Prod'app'0; dot_pos_item := 2; lookaheads_item := lookahead_set_10 |} ]%list.
Extract Inlined Constant items_of_state_19 => "assert false".

Definition items_of_state_20 : list item :=
  [ {| prod_item := Prod'annotation'0; dot_pos_item := 0; lookaheads_item := lookahead_set_2 |};
    {| prod_item := Prod'argument'0; dot_pos_item := 0; lookaheads_item := lookahead_set_2 |};
    {| prod_item := Prod'argument'1; dot_pos_item := 0; lookaheads_item := lookahead_set_2 |};
    {| prod_item := Prod'arguments'0; dot_pos_item := 0; lookaheads_item := lookahead_set_10 |};
    {| prod_item := Prod'arguments'0; dot_pos_item := 1; lookaheads_item := lookahead_set_10 |};
    {| prod_item := Prod'arguments'1; dot_pos_item := 0; lookaheads_item := lookahead_set_10 |};
    {| prod_item := Prod'arguments'1; dot_pos_item := 1; lookaheads_item := lookahead_set_10 |};
    {| prod_item := Prod'atom'0; dot_pos_item := 0; lookaheads_item := lookahead_set_2 |};
    {| prod_item := Prod'atom'1; dot_pos_item := 0; lookaheads_item := lookahead_set_2 |};
    {| prod_item := Prod'atom'2; dot_pos_item := 0; lookaheads_item := lookahead_set_2 |};
    {| prod_item := Prod'atom'3; dot_pos_item := 0; lookaheads_item := lookahead_set_2 |};
    {| prod_item := Prod'atom'4; dot_pos_item := 0; lookaheads_item := lookahead_set_2 |};
    {| prod_item := Prod'atom'5; dot_pos_item := 0; lookaheads_item := lookahead_set_2 |};
    {| prod_item := Prod'atom'6; dot_pos_item := 0; lookaheads_item := lookahead_set_2 |} ]%list.
Extract Inlined Constant items_of_state_20 => "assert false".

Definition items_of_state_21 : list item :=
  [ {| prod_item := Prod'arguments'1; dot_pos_item := 2; lookaheads_item := lookahead_set_10 |} ]%list.
Extract Inlined Constant items_of_state_21 => "assert false".

Definition items_of_state_22 : list item :=
  [ {| prod_item := Prod'argument'1; dot_pos_item := 1; lookaheads_item := lookahead_set_2 |} ]%list.
Extract Inlined Constant items_of_state_22 => "assert false".

Definition items_of_state_23 : list item :=
  [ {| prod_item := Prod'atom'5; dot_pos_item := 2; lookaheads_item := lookahead_set_2 |} ]%list.
Extract Inlined Constant items_of_state_23 => "assert false".

Definition items_of_state_24 : list item :=
  [ {| prod_item := Prod'atom'5; dot_pos_item := 3; lookaheads_item := lookahead_set_2 |} ]%list.
Extract Inlined Constant items_of_state_24 => "assert false".

Definition items_of_state_25 : list item :=
  [ {| prod_item := Prod'file'0; dot_pos_item := 1; lookaheads_item := lookahead_set_2 |} ]%list.
Extract Inlined Constant items_of_state_25 => "assert false".

Definition items_of_state_26 : list item :=
  [ {| prod_item := Prod'file'0; dot_pos_item := 2; lookaheads_item := lookahead_set_2 |} ]%list.
Extract Inlined Constant items_of_state_26 => "assert false".

Definition items_of_state_28 : list item :=
  [ {| prod_item := Prod'app'0; dot_pos_item := 0; lookaheads_item := lookahead_set_11 |};
    {| prod_item := Prod'atom'0; dot_pos_item := 0; lookaheads_item := lookahead_set_11 |};
    {| prod_item := Prod'atom'1; dot_pos_item := 0; lookaheads_item := lookahead_set_11 |};
    {| prod_item := Prod'atom'2; dot_pos_item := 0; lookaheads_item := lookahead_set_11 |};
    {| prod_item := Prod'atom'3; dot_pos_item := 0; lookaheads_item := lookahead_set_11 |};
    {| prod_item := Prod'atom'4; dot_pos_item := 0; lookaheads_item := lookahead_set_11 |};
    {| prod_item := Prod'atom'5; dot_pos_item := 0; lookaheads_item := lookahead_set_11 |};
    {| prod_item := Prod'atom'6; dot_pos_item := 0; lookaheads_item := lookahead_set_11 |};
    {| prod_item := Prod'micheline'0; dot_pos_item := 0; lookaheads_item := lookahead_set_11 |};
    {| prod_item := Prod'micheline'1; dot_pos_item := 0; lookaheads_item := lookahead_set_11 |};
    {| prod_item := Prod'seq'0; dot_pos_item := 0; lookaheads_item := lookahead_set_1 |};
    {| prod_item := Prod'seq'1; dot_pos_item := 0; lookaheads_item := lookahead_set_1 |};
    {| prod_item := Prod'seq'2; dot_pos_item := 0; lookaheads_item := lookahead_set_1 |};
    {| prod_item := Prod'seq_file'0; dot_pos_item := 0; lookaheads_item := lookahead_set_2 |} ]%list.
Extract Inlined Constant items_of_state_28 => "assert false".

Definition items_of_state_30 : list item :=
  [ {| prod_item := Prod'seq_file'0; dot_pos_item := 1; lookaheads_item := lookahead_set_2 |} ]%list.
Extract Inlined Constant items_of_state_30 => "assert false".

Definition items_of_state_31 : list item :=
  [ {| prod_item := Prod'seq_file'0; dot_pos_item := 2; lookaheads_item := lookahead_set_2 |} ]%list.
Extract Inlined Constant items_of_state_31 => "assert false".

Definition items_of_state (s:state) : list item :=
  match s with
  | Init Init'0 => items_of_state_0
  | Ninit Nis'1 => items_of_state_1
  | Ninit Nis'2 => items_of_state_2
  | Ninit Nis'3 => items_of_state_3
  | Ninit Nis'4 => items_of_state_4
  | Ninit Nis'5 => items_of_state_5
  | Ninit Nis'6 => items_of_state_6
  | Ninit Nis'7 => items_of_state_7
  | Ninit Nis'8 => items_of_state_8
  | Ninit Nis'9 => items_of_state_9
  | Ninit Nis'10 => items_of_state_10
  | Ninit Nis'11 => items_of_state_11
  | Ninit Nis'12 => items_of_state_12
  | Ninit Nis'13 => items_of_state_13
  | Ninit Nis'14 => items_of_state_14
  | Ninit Nis'15 => items_of_state_15
  | Ninit Nis'16 => items_of_state_16
  | Ninit Nis'17 => items_of_state_17
  | Ninit Nis'18 => items_of_state_18
  | Ninit Nis'19 => items_of_state_19
  | Ninit Nis'20 => items_of_state_20
  | Ninit Nis'21 => items_of_state_21
  | Ninit Nis'22 => items_of_state_22
  | Ninit Nis'23 => items_of_state_23
  | Ninit Nis'24 => items_of_state_24
  | Ninit Nis'25 => items_of_state_25
  | Ninit Nis'26 => items_of_state_26
  | Init Init'28 => items_of_state_28
  | Ninit Nis'30 => items_of_state_30
  | Ninit Nis'31 => items_of_state_31
  end.
Extract Constant items_of_state => "fun _ -> assert false".

Definition N_of_state (s:state) : N :=
  match s with
  | Init Init'0 => 0%N
  | Ninit Nis'1 => 1%N
  | Ninit Nis'2 => 2%N
  | Ninit Nis'3 => 3%N
  | Ninit Nis'4 => 4%N
  | Ninit Nis'5 => 5%N
  | Ninit Nis'6 => 6%N
  | Ninit Nis'7 => 7%N
  | Ninit Nis'8 => 8%N
  | Ninit Nis'9 => 9%N
  | Ninit Nis'10 => 10%N
  | Ninit Nis'11 => 11%N
  | Ninit Nis'12 => 12%N
  | Ninit Nis'13 => 13%N
  | Ninit Nis'14 => 14%N
  | Ninit Nis'15 => 15%N
  | Ninit Nis'16 => 16%N
  | Ninit Nis'17 => 17%N
  | Ninit Nis'18 => 18%N
  | Ninit Nis'19 => 19%N
  | Ninit Nis'20 => 20%N
  | Ninit Nis'21 => 21%N
  | Ninit Nis'22 => 22%N
  | Ninit Nis'23 => 23%N
  | Ninit Nis'24 => 24%N
  | Ninit Nis'25 => 25%N
  | Ninit Nis'26 => 26%N
  | Init Init'28 => 28%N
  | Ninit Nis'30 => 30%N
  | Ninit Nis'31 => 31%N
  end.
End Aut.

Module MenhirLibParser := MenhirLib.Main.Make Aut.
Theorem safe:
  MenhirLibParser.safe_validator tt = true.
Proof eq_refl true<:MenhirLibParser.safe_validator tt = true.

Theorem complete:
  MenhirLibParser.complete_validator tt = true.
Proof eq_refl true<:MenhirLibParser.complete_validator tt = true.

Definition file : nat -> MenhirLibParser.Inter.buffer -> MenhirLibParser.Inter.parse_result        (loc_micheline) := MenhirLibParser.parse safe Aut.Init'0.

Theorem file_correct (log_fuel : nat) (buffer : MenhirLibParser.Inter.buffer):
  match file log_fuel buffer with
  | MenhirLibParser.Inter.Parsed_pr sem buffer_new =>
      exists word (tree : Gram.parse_tree (NT file'nt) word),
        buffer = MenhirLibParser.Inter.app_buf word buffer_new /\
        Gram.pt_sem tree = sem
  | _ => True
  end.
Proof. apply MenhirLibParser.parse_correct with (init:=Aut.Init'0). Qed.

Theorem file_complete (log_fuel : nat) (word : list token) (buffer_end : MenhirLibParser.Inter.buffer) :
  forall tree : Gram.parse_tree (NT file'nt) word,
  match file log_fuel (MenhirLibParser.Inter.app_buf word buffer_end) with
  | MenhirLibParser.Inter.Fail_pr => False
  | MenhirLibParser.Inter.Parsed_pr output_res buffer_end_res =>
      output_res = Gram.pt_sem tree /\
      buffer_end_res = buffer_end /\ (Gram.pt_size tree <= PeanoNat.Nat.pow 2 log_fuel)%nat
  | MenhirLibParser.Inter.Timeout_pr => (PeanoNat.Nat.pow 2 log_fuel < Gram.pt_size tree)%nat
  end.
Proof. apply MenhirLibParser.parse_complete with (init:=Aut.Init'0); exact complete. Qed.
Definition seq_file : nat -> MenhirLibParser.Inter.buffer -> MenhirLibParser.Inter.parse_result        (list (loc_micheline)) := MenhirLibParser.parse safe Aut.Init'28.

Theorem seq_file_correct (log_fuel : nat) (buffer : MenhirLibParser.Inter.buffer):
  match seq_file log_fuel buffer with
  | MenhirLibParser.Inter.Parsed_pr sem buffer_new =>
      exists word (tree : Gram.parse_tree (NT seq_file'nt) word),
        buffer = MenhirLibParser.Inter.app_buf word buffer_new /\
        Gram.pt_sem tree = sem
  | _ => True
  end.
Proof. apply MenhirLibParser.parse_correct with (init:=Aut.Init'28). Qed.

Theorem seq_file_complete (log_fuel : nat) (word : list token) (buffer_end : MenhirLibParser.Inter.buffer) :
  forall tree : Gram.parse_tree (NT seq_file'nt) word,
  match seq_file log_fuel (MenhirLibParser.Inter.app_buf word buffer_end) with
  | MenhirLibParser.Inter.Fail_pr => False
  | MenhirLibParser.Inter.Parsed_pr output_res buffer_end_res =>
      output_res = Gram.pt_sem tree /\
      buffer_end_res = buffer_end /\ (Gram.pt_size tree <= PeanoNat.Nat.pow 2 log_fuel)%nat
  | MenhirLibParser.Inter.Timeout_pr => (PeanoNat.Nat.pow 2 log_fuel < Gram.pt_size tree)%nat
  end.
Proof. apply MenhirLibParser.parse_complete with (init:=Aut.Init'28); exact complete. Qed.



