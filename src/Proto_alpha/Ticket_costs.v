Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Saturation_repr.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.

Module Constants.
  Module S := Saturation_repr.
  
  Definition cost_contains_tickets_step : S.t := S.safe_int 28.
  
  Definition cost_collect_tickets_step : S.t := S.safe_int 360.
  
  Definition cost_has_tickets_of_ty (type_size : S.t) : S.t :=
    S.mul (S.safe_int 20) type_size.
End Constants.

Definition consume_gas_steps
  (ctxt : Alpha_context.context) (step_cost : Saturation_repr.t)
  (num_steps : int) : M? Alpha_context.context :=
  let op_star := Saturation_repr.mul in
  if num_steps <=i 0 then
    Pervasives.Ok ctxt
  else
    let gas :=
      Alpha_context.Gas.atomic_step_cost
        (op_star step_cost (Saturation_repr.safe_int num_steps)) in
    Alpha_context.Gas.consume ctxt gas.

Definition has_tickets_of_ty_cost (ty : Script_typed_ir.ty) : Constants.S.t :=
  Constants.cost_has_tickets_of_ty
    (Script_typed_ir.Type_size.to_int (Script_typed_ir.ty_size ty)).
