Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.

Definition t (a trace : Set) : Set :=
  Alpha_context.context ->
  M? (Pervasives.result a trace * Alpha_context.context).

Definition gas_monad (a trace : Set) : Set := t a trace.

Definition of_result {A B C : Set} (x_value : A) (ctxt : B)
  : Pervasives.result (A * B) C := return? (x_value, ctxt).

Definition _return {A B C D : Set} (x_value : A)
  : B -> Pervasives.result (Pervasives.result A C * B) D :=
  of_result (return? x_value).

Definition op_gtgtdollar {A B C D E F : Set}
  (m_value : A -> Pervasives.result (Pervasives.result B C * D) E)
  (f_value : B -> D -> Pervasives.result (Pervasives.result F C * D) E)
  (ctxt : A) : Pervasives.result (Pervasives.result F C * D) E :=
  let? '(x_value, ctxt) := m_value ctxt in
  match x_value with
  | Pervasives.Ok y_value => f_value y_value ctxt
  | Pervasives.Error err => of_result (Pervasives.Error err) ctxt
  end.

Definition op_gtpipedollar {A B C D E F : Set}
  (m_value : A -> Pervasives.result (Pervasives.result B C * D) E)
  (f_value : B -> F) (ctxt : A)
  : Pervasives.result (Pervasives.result F C * D) E :=
  let? '(x_value, ctxt) := m_value ctxt in
  of_result (Error_monad.op_gtpipequestion x_value f_value) ctxt.

Definition op_gtquestiondollar {A B C D E F : Set}
  (m_value : A -> Pervasives.result (Pervasives.result B C * D) E)
  (f_value : B -> Pervasives.result F C)
  : A -> Pervasives.result (Pervasives.result F C * D) E :=
  op_gtgtdollar m_value (fun (x_value : B) => of_result (f_value x_value)).

Definition op_gtquestionquestiondollar {A B C D E : Set}
  (m_value : A -> Pervasives.result (B * C) D)
  (f_value : B -> C -> Pervasives.result E D) (ctxt : A)
  : Pervasives.result E D :=
  let? '(x_value, ctxt) := m_value ctxt in
  f_value x_value ctxt.

Definition consume_gas {A : Set}
  (cost : Alpha_context.Gas.cost) (ctxt : Alpha_context.context)
  : M? (Pervasives.result unit A * Alpha_context.context) :=
  Error_monad.op_gtgtquestion (Alpha_context.Gas.consume ctxt cost) (_return tt).

Definition run {A B : Set} (ctxt : A) (x_value : A -> B) : B := x_value ctxt.

Definition record_trace_eval {err a : Set}
  (f_value : unit -> err) (m_value : t a (Error_monad.trace err))
  (ctxt : Alpha_context.context) :=
  let? '(x_value, ctxt) := m_value ctxt in
  of_result (Error_monad.record_trace_eval f_value x_value) ctxt.
