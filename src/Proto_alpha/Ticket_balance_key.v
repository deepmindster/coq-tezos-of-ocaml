Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Script_ir_translator_mli.
  Module Script_ir_translator := Script_ir_translator_mli.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.
Require TezosOfOCaml.Proto_alpha.Ticket_scanner.

Definition ticket_balance_key_and_amount
  (ctxt : Alpha_context.context) (owner : Alpha_context.Contract.t)
  (function_parameter : Ticket_scanner.ex_ticket)
  : M? (Alpha_context.Ticket_balance.key_hash * Z.t * Alpha_context.context) :=
  let
    'Ticket_scanner.Ex_ticket comp_ty {|
      Script_typed_ir.ticket.ticketer := ticketer;
        Script_typed_ir.ticket.contents := contents;
        Script_typed_ir.ticket.amount := amount
        |} := function_parameter in
  let loc := Micheline.dummy_location in
  let? '(cont_ty_unstripped, ctxt) :=
    Script_ir_translator.unparse_comparable_ty loc ctxt comp_ty in
  let? ctxt :=
    Alpha_context.Gas.consume ctxt
      (Alpha_context.Script.strip_annotations_cost cont_ty_unstripped) in
  let typ := Alpha_context.Script.strip_annotations cont_ty_unstripped in
  let ticketer_address := (ticketer, "default") in
  let owner_address := (owner, "default") in
  let address_t := Script_typed_ir.address_t None in
  let? '(ticketer, ctxt) :=
    Script_ir_translator.unparse_data ctxt Script_ir_translator.Optimized_legacy
      address_t ticketer_address in
  let? '(contents, ctxt) :=
    Script_ir_translator.unparse_comparable_data loc ctxt
      Script_ir_translator.Optimized_legacy comp_ty contents in
  let? '(owner, ctxt) :=
    Script_ir_translator.unparse_data ctxt Script_ir_translator.Optimized_legacy
      address_t owner_address in
  let? '(hash_value, ctxt) :=
    Alpha_context.Ticket_balance.make_key_hash ctxt ticketer typ contents owner
    in
  return? (hash_value, (Alpha_context.Script_int.to_zint amount), ctxt).
