Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Contract_repr.
Require TezosOfOCaml.Proto_alpha.Raw_context.
Require TezosOfOCaml.Proto_alpha.Stake_storage.
Require TezosOfOCaml.Proto_alpha.Storage.
Require TezosOfOCaml.Proto_alpha.Storage_sigs.
Require TezosOfOCaml.Proto_alpha.Tez_repr.

Definition find
  : Raw_context.t -> Contract_repr.t -> M? (option Signature.public_key_hash) :=
  Storage.Contract.Delegate.(Storage_sigs.Indexed_data_storage.find).

Definition remove_contract_stake
  (ctxt : Raw_context.t) (contract : Contract_repr.t) (amount : Tez_repr.t)
  : M? Raw_context.t :=
  let? function_parameter := find ctxt contract in
  match function_parameter with
  | None => return? ctxt
  | Some delegate => Stake_storage.remove_stake ctxt delegate amount
  end.

Definition add_contract_stake
  (ctxt : Raw_context.t) (contract : Contract_repr.t) (amount : Tez_repr.t)
  : M? Raw_context.t :=
  let? function_parameter := find ctxt contract in
  match function_parameter with
  | None => return? ctxt
  | Some delegate => Stake_storage.add_stake ctxt delegate amount
  end.

Definition registered
  (c_value : Raw_context.t) (delegate : Signature.public_key_hash) : M? bool :=
  let? function_parameter :=
    Storage.Contract.Delegate.(Storage_sigs.Indexed_data_storage.find) c_value
      (Contract_repr.implicit_contract delegate) in
  match function_parameter with
  | Some current_delegate =>
    return?
      (Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.equal) delegate
        current_delegate)
  | None => return? false
  end.

Definition link
  (c_value : Raw_context.t) (contract : Contract_repr.t)
  (delegate : Signature.public_key_hash) : M? Raw_context.t :=
  let? balance :=
    Storage.Contract.Balance.(Storage_sigs.Indexed_data_storage.get) c_value
      contract in
  let? c_value := Stake_storage.add_stake c_value delegate balance in
  Error_monad.op_gtpipeeq
    (Storage.Contract.Delegated.(Storage_sigs.Data_set_storage.add)
      (c_value, (Contract_repr.implicit_contract delegate)) contract)
    Error_monad.ok.

Definition unlink (c_value : Raw_context.t) (contract : Contract_repr.t)
  : M? Raw_context.t :=
  let? function_parameter :=
    Storage.Contract.Delegate.(Storage_sigs.Indexed_data_storage.find) c_value
      contract in
  match function_parameter with
  | None => return? c_value
  | Some delegate =>
    let? balance :=
      Storage.Contract.Balance.(Storage_sigs.Indexed_data_storage.get) c_value
        contract in
    let? c_value := Stake_storage.remove_stake c_value delegate balance in
    Error_monad.op_gtpipeeq
      (Storage.Contract.Delegated.(Storage_sigs.Data_set_storage.remove)
        (c_value, (Contract_repr.implicit_contract delegate)) contract)
      Error_monad.ok
  end.

Definition init_value
  (ctxt : Raw_context.t) (contract : Contract_repr.t)
  (delegate : Signature.public_key_hash) : M? Raw_context.t :=
  let? ctxt :=
    Storage.Contract.Delegate.(Storage_sigs.Indexed_data_storage.init_value)
      ctxt contract delegate in
  link ctxt contract delegate.

Definition delete (ctxt : Raw_context.t) (contract : Contract_repr.t)
  : M? Raw_context.t :=
  let? ctxt := unlink ctxt contract in
  Error_monad.op_gtpipeeq
    (Storage.Contract.Delegate.(Storage_sigs.Indexed_data_storage.remove) ctxt
      contract) Error_monad.ok.

Definition remove (ctxt : Raw_context.t) (contract : Contract_repr.t)
  : M? Raw_context.t := unlink ctxt contract.

Definition set
  (ctxt : Raw_context.t) (contract : Contract_repr.t)
  (delegate : Signature.public_key_hash) : M? Raw_context.t :=
  let? ctxt := unlink ctxt contract in
  let ctxt :=
    Storage.Contract.Delegate.(Storage_sigs.Indexed_data_storage.add) ctxt
      contract delegate in
  link ctxt contract delegate.

Definition delegated_contracts
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  : list Contract_repr.t :=
  let contract := Contract_repr.implicit_contract delegate in
  Storage.Contract.Delegated.(Storage_sigs.Data_set_storage.elements)
    (ctxt, contract).
