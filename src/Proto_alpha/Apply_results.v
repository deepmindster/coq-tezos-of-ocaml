Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Nonce_hash.
Require TezosOfOCaml.Proto_alpha.Script_expr_hash.

Definition error_encoding : Data_encoding.encoding Error_monad._error :=
  (let arg :=
    fun x_1 =>
      Data_encoding.def "error" x_1
        (Some
          "The full list of RPC errors would be too long to include.\nIt is available at RPC `/errors` (GET).\nErrors specific to protocol Alpha have an id that starts with `proto.alpha`.")
    in
  fun (eta : Data_encoding.encoding Error_monad._error) => arg None eta)
    (Data_encoding.splitted
      (Data_encoding.conv
        (fun (err : Error_monad._error) =>
          Data_encoding.Json.construct Error_monad.error_encoding err)
        (fun (json_value : Data_encoding.json) =>
          Data_encoding.Json.destruct Error_monad.error_encoding json_value)
        None Data_encoding.json_value) Error_monad.error_encoding).

Definition trace_encoding
  : Data_encoding.t (Error_monad.trace Error_monad._error) :=
  Error_monad.make_trace_encoding error_encoding.

(** Records for the constructor parameters *)
Module ConstructorRecords_successful_manager_operation_result.
  Module successful_manager_operation_result.
    Module Reveal_result.
      Record record {consumed_gas : Set} : Set := Build {
        consumed_gas : consumed_gas }.
      Arguments record : clear implicits.
      Definition with_consumed_gas {t_consumed_gas} consumed_gas
        (r : record t_consumed_gas) :=
        Build t_consumed_gas consumed_gas.
    End Reveal_result.
    Definition Reveal_result_skeleton := Reveal_result.record.
    
    Module Transaction_result.
      Record record {storage lazy_storage_diff balance_updates
        originated_contracts consumed_gas storage_size paid_storage_size_diff
        allocated_destination_contract : Set} : Set := Build {
        storage : storage;
        lazy_storage_diff : lazy_storage_diff;
        balance_updates : balance_updates;
        originated_contracts : originated_contracts;
        consumed_gas : consumed_gas;
        storage_size : storage_size;
        paid_storage_size_diff : paid_storage_size_diff;
        allocated_destination_contract : allocated_destination_contract }.
      Arguments record : clear implicits.
      Definition with_storage
        {t_storage t_lazy_storage_diff t_balance_updates t_originated_contracts
          t_consumed_gas t_storage_size t_paid_storage_size_diff
          t_allocated_destination_contract} storage
        (r :
          record t_storage t_lazy_storage_diff t_balance_updates
            t_originated_contracts t_consumed_gas t_storage_size
            t_paid_storage_size_diff t_allocated_destination_contract) :=
        Build t_storage t_lazy_storage_diff t_balance_updates
          t_originated_contracts t_consumed_gas t_storage_size
          t_paid_storage_size_diff t_allocated_destination_contract storage
          r.(lazy_storage_diff) r.(balance_updates) r.(originated_contracts)
          r.(consumed_gas) r.(storage_size) r.(paid_storage_size_diff)
          r.(allocated_destination_contract).
      Definition with_lazy_storage_diff
        {t_storage t_lazy_storage_diff t_balance_updates t_originated_contracts
          t_consumed_gas t_storage_size t_paid_storage_size_diff
          t_allocated_destination_contract} lazy_storage_diff
        (r :
          record t_storage t_lazy_storage_diff t_balance_updates
            t_originated_contracts t_consumed_gas t_storage_size
            t_paid_storage_size_diff t_allocated_destination_contract) :=
        Build t_storage t_lazy_storage_diff t_balance_updates
          t_originated_contracts t_consumed_gas t_storage_size
          t_paid_storage_size_diff t_allocated_destination_contract r.(storage)
          lazy_storage_diff r.(balance_updates) r.(originated_contracts)
          r.(consumed_gas) r.(storage_size) r.(paid_storage_size_diff)
          r.(allocated_destination_contract).
      Definition with_balance_updates
        {t_storage t_lazy_storage_diff t_balance_updates t_originated_contracts
          t_consumed_gas t_storage_size t_paid_storage_size_diff
          t_allocated_destination_contract} balance_updates
        (r :
          record t_storage t_lazy_storage_diff t_balance_updates
            t_originated_contracts t_consumed_gas t_storage_size
            t_paid_storage_size_diff t_allocated_destination_contract) :=
        Build t_storage t_lazy_storage_diff t_balance_updates
          t_originated_contracts t_consumed_gas t_storage_size
          t_paid_storage_size_diff t_allocated_destination_contract r.(storage)
          r.(lazy_storage_diff) balance_updates r.(originated_contracts)
          r.(consumed_gas) r.(storage_size) r.(paid_storage_size_diff)
          r.(allocated_destination_contract).
      Definition with_originated_contracts
        {t_storage t_lazy_storage_diff t_balance_updates t_originated_contracts
          t_consumed_gas t_storage_size t_paid_storage_size_diff
          t_allocated_destination_contract} originated_contracts
        (r :
          record t_storage t_lazy_storage_diff t_balance_updates
            t_originated_contracts t_consumed_gas t_storage_size
            t_paid_storage_size_diff t_allocated_destination_contract) :=
        Build t_storage t_lazy_storage_diff t_balance_updates
          t_originated_contracts t_consumed_gas t_storage_size
          t_paid_storage_size_diff t_allocated_destination_contract r.(storage)
          r.(lazy_storage_diff) r.(balance_updates) originated_contracts
          r.(consumed_gas) r.(storage_size) r.(paid_storage_size_diff)
          r.(allocated_destination_contract).
      Definition with_consumed_gas
        {t_storage t_lazy_storage_diff t_balance_updates t_originated_contracts
          t_consumed_gas t_storage_size t_paid_storage_size_diff
          t_allocated_destination_contract} consumed_gas
        (r :
          record t_storage t_lazy_storage_diff t_balance_updates
            t_originated_contracts t_consumed_gas t_storage_size
            t_paid_storage_size_diff t_allocated_destination_contract) :=
        Build t_storage t_lazy_storage_diff t_balance_updates
          t_originated_contracts t_consumed_gas t_storage_size
          t_paid_storage_size_diff t_allocated_destination_contract r.(storage)
          r.(lazy_storage_diff) r.(balance_updates) r.(originated_contracts)
          consumed_gas r.(storage_size) r.(paid_storage_size_diff)
          r.(allocated_destination_contract).
      Definition with_storage_size
        {t_storage t_lazy_storage_diff t_balance_updates t_originated_contracts
          t_consumed_gas t_storage_size t_paid_storage_size_diff
          t_allocated_destination_contract} storage_size
        (r :
          record t_storage t_lazy_storage_diff t_balance_updates
            t_originated_contracts t_consumed_gas t_storage_size
            t_paid_storage_size_diff t_allocated_destination_contract) :=
        Build t_storage t_lazy_storage_diff t_balance_updates
          t_originated_contracts t_consumed_gas t_storage_size
          t_paid_storage_size_diff t_allocated_destination_contract r.(storage)
          r.(lazy_storage_diff) r.(balance_updates) r.(originated_contracts)
          r.(consumed_gas) storage_size r.(paid_storage_size_diff)
          r.(allocated_destination_contract).
      Definition with_paid_storage_size_diff
        {t_storage t_lazy_storage_diff t_balance_updates t_originated_contracts
          t_consumed_gas t_storage_size t_paid_storage_size_diff
          t_allocated_destination_contract} paid_storage_size_diff
        (r :
          record t_storage t_lazy_storage_diff t_balance_updates
            t_originated_contracts t_consumed_gas t_storage_size
            t_paid_storage_size_diff t_allocated_destination_contract) :=
        Build t_storage t_lazy_storage_diff t_balance_updates
          t_originated_contracts t_consumed_gas t_storage_size
          t_paid_storage_size_diff t_allocated_destination_contract r.(storage)
          r.(lazy_storage_diff) r.(balance_updates) r.(originated_contracts)
          r.(consumed_gas) r.(storage_size) paid_storage_size_diff
          r.(allocated_destination_contract).
      Definition with_allocated_destination_contract
        {t_storage t_lazy_storage_diff t_balance_updates t_originated_contracts
          t_consumed_gas t_storage_size t_paid_storage_size_diff
          t_allocated_destination_contract} allocated_destination_contract
        (r :
          record t_storage t_lazy_storage_diff t_balance_updates
            t_originated_contracts t_consumed_gas t_storage_size
            t_paid_storage_size_diff t_allocated_destination_contract) :=
        Build t_storage t_lazy_storage_diff t_balance_updates
          t_originated_contracts t_consumed_gas t_storage_size
          t_paid_storage_size_diff t_allocated_destination_contract r.(storage)
          r.(lazy_storage_diff) r.(balance_updates) r.(originated_contracts)
          r.(consumed_gas) r.(storage_size) r.(paid_storage_size_diff)
          allocated_destination_contract.
    End Transaction_result.
    Definition Transaction_result_skeleton := Transaction_result.record.
    
    Module Origination_result.
      Record record {lazy_storage_diff balance_updates originated_contracts
        consumed_gas storage_size paid_storage_size_diff : Set} : Set := Build {
        lazy_storage_diff : lazy_storage_diff;
        balance_updates : balance_updates;
        originated_contracts : originated_contracts;
        consumed_gas : consumed_gas;
        storage_size : storage_size;
        paid_storage_size_diff : paid_storage_size_diff }.
      Arguments record : clear implicits.
      Definition with_lazy_storage_diff
        {t_lazy_storage_diff t_balance_updates t_originated_contracts
          t_consumed_gas t_storage_size t_paid_storage_size_diff}
        lazy_storage_diff
        (r :
          record t_lazy_storage_diff t_balance_updates t_originated_contracts
            t_consumed_gas t_storage_size t_paid_storage_size_diff) :=
        Build t_lazy_storage_diff t_balance_updates t_originated_contracts
          t_consumed_gas t_storage_size t_paid_storage_size_diff
          lazy_storage_diff r.(balance_updates) r.(originated_contracts)
          r.(consumed_gas) r.(storage_size) r.(paid_storage_size_diff).
      Definition with_balance_updates
        {t_lazy_storage_diff t_balance_updates t_originated_contracts
          t_consumed_gas t_storage_size t_paid_storage_size_diff}
        balance_updates
        (r :
          record t_lazy_storage_diff t_balance_updates t_originated_contracts
            t_consumed_gas t_storage_size t_paid_storage_size_diff) :=
        Build t_lazy_storage_diff t_balance_updates t_originated_contracts
          t_consumed_gas t_storage_size t_paid_storage_size_diff
          r.(lazy_storage_diff) balance_updates r.(originated_contracts)
          r.(consumed_gas) r.(storage_size) r.(paid_storage_size_diff).
      Definition with_originated_contracts
        {t_lazy_storage_diff t_balance_updates t_originated_contracts
          t_consumed_gas t_storage_size t_paid_storage_size_diff}
        originated_contracts
        (r :
          record t_lazy_storage_diff t_balance_updates t_originated_contracts
            t_consumed_gas t_storage_size t_paid_storage_size_diff) :=
        Build t_lazy_storage_diff t_balance_updates t_originated_contracts
          t_consumed_gas t_storage_size t_paid_storage_size_diff
          r.(lazy_storage_diff) r.(balance_updates) originated_contracts
          r.(consumed_gas) r.(storage_size) r.(paid_storage_size_diff).
      Definition with_consumed_gas
        {t_lazy_storage_diff t_balance_updates t_originated_contracts
          t_consumed_gas t_storage_size t_paid_storage_size_diff} consumed_gas
        (r :
          record t_lazy_storage_diff t_balance_updates t_originated_contracts
            t_consumed_gas t_storage_size t_paid_storage_size_diff) :=
        Build t_lazy_storage_diff t_balance_updates t_originated_contracts
          t_consumed_gas t_storage_size t_paid_storage_size_diff
          r.(lazy_storage_diff) r.(balance_updates) r.(originated_contracts)
          consumed_gas r.(storage_size) r.(paid_storage_size_diff).
      Definition with_storage_size
        {t_lazy_storage_diff t_balance_updates t_originated_contracts
          t_consumed_gas t_storage_size t_paid_storage_size_diff} storage_size
        (r :
          record t_lazy_storage_diff t_balance_updates t_originated_contracts
            t_consumed_gas t_storage_size t_paid_storage_size_diff) :=
        Build t_lazy_storage_diff t_balance_updates t_originated_contracts
          t_consumed_gas t_storage_size t_paid_storage_size_diff
          r.(lazy_storage_diff) r.(balance_updates) r.(originated_contracts)
          r.(consumed_gas) storage_size r.(paid_storage_size_diff).
      Definition with_paid_storage_size_diff
        {t_lazy_storage_diff t_balance_updates t_originated_contracts
          t_consumed_gas t_storage_size t_paid_storage_size_diff}
        paid_storage_size_diff
        (r :
          record t_lazy_storage_diff t_balance_updates t_originated_contracts
            t_consumed_gas t_storage_size t_paid_storage_size_diff) :=
        Build t_lazy_storage_diff t_balance_updates t_originated_contracts
          t_consumed_gas t_storage_size t_paid_storage_size_diff
          r.(lazy_storage_diff) r.(balance_updates) r.(originated_contracts)
          r.(consumed_gas) r.(storage_size) paid_storage_size_diff.
    End Origination_result.
    Definition Origination_result_skeleton := Origination_result.record.
    
    Module Delegation_result.
      Record record {consumed_gas : Set} : Set := Build {
        consumed_gas : consumed_gas }.
      Arguments record : clear implicits.
      Definition with_consumed_gas {t_consumed_gas} consumed_gas
        (r : record t_consumed_gas) :=
        Build t_consumed_gas consumed_gas.
    End Delegation_result.
    Definition Delegation_result_skeleton := Delegation_result.record.
    
    Module Register_global_constant_result.
      Record record {balance_updates consumed_gas size_of_constant
        global_address : Set} : Set := Build {
        balance_updates : balance_updates;
        consumed_gas : consumed_gas;
        size_of_constant : size_of_constant;
        global_address : global_address }.
      Arguments record : clear implicits.
      Definition with_balance_updates
        {t_balance_updates t_consumed_gas t_size_of_constant t_global_address}
        balance_updates
        (r :
          record t_balance_updates t_consumed_gas t_size_of_constant
            t_global_address) :=
        Build t_balance_updates t_consumed_gas t_size_of_constant
          t_global_address balance_updates r.(consumed_gas) r.(size_of_constant)
          r.(global_address).
      Definition with_consumed_gas
        {t_balance_updates t_consumed_gas t_size_of_constant t_global_address}
        consumed_gas
        (r :
          record t_balance_updates t_consumed_gas t_size_of_constant
            t_global_address) :=
        Build t_balance_updates t_consumed_gas t_size_of_constant
          t_global_address r.(balance_updates) consumed_gas r.(size_of_constant)
          r.(global_address).
      Definition with_size_of_constant
        {t_balance_updates t_consumed_gas t_size_of_constant t_global_address}
        size_of_constant
        (r :
          record t_balance_updates t_consumed_gas t_size_of_constant
            t_global_address) :=
        Build t_balance_updates t_consumed_gas t_size_of_constant
          t_global_address r.(balance_updates) r.(consumed_gas) size_of_constant
          r.(global_address).
      Definition with_global_address
        {t_balance_updates t_consumed_gas t_size_of_constant t_global_address}
        global_address
        (r :
          record t_balance_updates t_consumed_gas t_size_of_constant
            t_global_address) :=
        Build t_balance_updates t_consumed_gas t_size_of_constant
          t_global_address r.(balance_updates) r.(consumed_gas)
          r.(size_of_constant) global_address.
    End Register_global_constant_result.
    Definition Register_global_constant_result_skeleton :=
      Register_global_constant_result.record.
    
    Module Set_deposits_limit_result.
      Record record {consumed_gas : Set} : Set := Build {
        consumed_gas : consumed_gas }.
      Arguments record : clear implicits.
      Definition with_consumed_gas {t_consumed_gas} consumed_gas
        (r : record t_consumed_gas) :=
        Build t_consumed_gas consumed_gas.
    End Set_deposits_limit_result.
    Definition Set_deposits_limit_result_skeleton :=
      Set_deposits_limit_result.record.
  End successful_manager_operation_result.
End ConstructorRecords_successful_manager_operation_result.
Import ConstructorRecords_successful_manager_operation_result.

Reserved Notation "'successful_manager_operation_result.Reveal_result".
Reserved Notation "'successful_manager_operation_result.Transaction_result".
Reserved Notation "'successful_manager_operation_result.Origination_result".
Reserved Notation "'successful_manager_operation_result.Delegation_result".
Reserved Notation
  "'successful_manager_operation_result.Register_global_constant_result".
Reserved Notation
  "'successful_manager_operation_result.Set_deposits_limit_result".

Inductive successful_manager_operation_result : Set :=
| Reveal_result :
  'successful_manager_operation_result.Reveal_result ->
  successful_manager_operation_result
| Transaction_result :
  'successful_manager_operation_result.Transaction_result ->
  successful_manager_operation_result
| Origination_result :
  'successful_manager_operation_result.Origination_result ->
  successful_manager_operation_result
| Delegation_result :
  'successful_manager_operation_result.Delegation_result ->
  successful_manager_operation_result
| Register_global_constant_result :
  'successful_manager_operation_result.Register_global_constant_result ->
  successful_manager_operation_result
| Set_deposits_limit_result :
  'successful_manager_operation_result.Set_deposits_limit_result ->
  successful_manager_operation_result

where "'successful_manager_operation_result.Reveal_result" :=
  (successful_manager_operation_result.Reveal_result_skeleton
    Alpha_context.Gas.Arith.fp)
and "'successful_manager_operation_result.Transaction_result" :=
  (successful_manager_operation_result.Transaction_result_skeleton
    (option Alpha_context.Script.expr) (option Alpha_context.Lazy_storage.diffs)
    Alpha_context.Receipt.balance_updates (list Alpha_context.Contract.t)
    Alpha_context.Gas.Arith.fp Z.t Z.t bool)
and "'successful_manager_operation_result.Origination_result" :=
  (successful_manager_operation_result.Origination_result_skeleton
    (option Alpha_context.Lazy_storage.diffs)
    Alpha_context.Receipt.balance_updates (list Alpha_context.Contract.t)
    Alpha_context.Gas.Arith.fp Z.t Z.t)
and "'successful_manager_operation_result.Delegation_result" :=
  (successful_manager_operation_result.Delegation_result_skeleton
    Alpha_context.Gas.Arith.fp)
and "'successful_manager_operation_result.Register_global_constant_result" :=
  (successful_manager_operation_result.Register_global_constant_result_skeleton
    Alpha_context.Receipt.balance_updates Alpha_context.Gas.Arith.fp Z.t
    Script_expr_hash.t)
and "'successful_manager_operation_result.Set_deposits_limit_result" :=
  (successful_manager_operation_result.Set_deposits_limit_result_skeleton
    Alpha_context.Gas.Arith.fp).

Module successful_manager_operation_result.
  Include ConstructorRecords_successful_manager_operation_result.successful_manager_operation_result.
  Definition Reveal_result :=
    'successful_manager_operation_result.Reveal_result.
  Definition Transaction_result :=
    'successful_manager_operation_result.Transaction_result.
  Definition Origination_result :=
    'successful_manager_operation_result.Origination_result.
  Definition Delegation_result :=
    'successful_manager_operation_result.Delegation_result.
  Definition Register_global_constant_result :=
    'successful_manager_operation_result.Register_global_constant_result.
  Definition Set_deposits_limit_result :=
    'successful_manager_operation_result.Set_deposits_limit_result.
End successful_manager_operation_result.

Definition migration_origination_result_to_successful_manager_operation_result
  (function_parameter : Alpha_context.Migration.origination_result)
  : successful_manager_operation_result :=
  let '{|
    Alpha_context.Migration.origination_result.balance_updates := balance_updates;
      Alpha_context.Migration.origination_result.originated_contracts :=
        originated_contracts;
      Alpha_context.Migration.origination_result.storage_size := storage_size;
      Alpha_context.Migration.origination_result.paid_storage_size_diff :=
        paid_storage_size_diff
      |} := function_parameter in
  Origination_result
    {|
      successful_manager_operation_result.Origination_result.lazy_storage_diff
        := None;
      successful_manager_operation_result.Origination_result.balance_updates :=
        balance_updates;
      successful_manager_operation_result.Origination_result.originated_contracts
        := originated_contracts;
      successful_manager_operation_result.Origination_result.consumed_gas :=
        Alpha_context.Gas.Arith.zero;
      successful_manager_operation_result.Origination_result.storage_size :=
        storage_size;
      successful_manager_operation_result.Origination_result.paid_storage_size_diff
        := paid_storage_size_diff |}.

Inductive packed_successful_manager_operation_result : Set :=
| Successful_manager_result :
  successful_manager_operation_result ->
  packed_successful_manager_operation_result.

Definition pack_migration_operation_results
  (results : list Alpha_context.Migration.origination_result)
  : list packed_successful_manager_operation_result :=
  List.map
    (fun (el : Alpha_context.Migration.origination_result) =>
      Successful_manager_result
        (migration_origination_result_to_successful_manager_operation_result el))
    results.

Inductive manager_operation_result : Set :=
| Applied : successful_manager_operation_result -> manager_operation_result
| Backtracked :
  successful_manager_operation_result ->
  option (Error_monad.trace Error_monad._error) -> manager_operation_result
| Failed :
  Alpha_context.Kind.manager -> Error_monad.trace Error_monad._error ->
  manager_operation_result
| Skipped : Alpha_context.Kind.manager -> manager_operation_result.

Inductive packed_internal_operation_result : Set :=
| Internal_operation_result :
  Alpha_context.internal_operation -> manager_operation_result ->
  packed_internal_operation_result.

Module Manager_result.
  (** Records for the constructor parameters *)
  Module ConstructorRecords_case.
    Module case.
      Module MCase.
        Record record {op_case encoding kind iselect select proj inj t : Set} :
          Set := Build {
          op_case : op_case;
          encoding : encoding;
          kind : kind;
          iselect : iselect;
          select : select;
          proj : proj;
          inj : inj;
          t : t }.
        Arguments record : clear implicits.
        Definition with_op_case
          {t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj t_t}
          op_case
          (r :
            record t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj
              t_t) :=
          Build t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj t_t
            op_case r.(encoding) r.(kind) r.(iselect) r.(select) r.(proj)
            r.(inj) r.(t).
        Definition with_encoding
          {t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj t_t}
          encoding
          (r :
            record t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj
              t_t) :=
          Build t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj t_t
            r.(op_case) encoding r.(kind) r.(iselect) r.(select) r.(proj)
            r.(inj) r.(t).
        Definition with_kind
          {t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj t_t} kind
          (r :
            record t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj
              t_t) :=
          Build t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj t_t
            r.(op_case) r.(encoding) kind r.(iselect) r.(select) r.(proj)
            r.(inj) r.(t).
        Definition with_iselect
          {t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj t_t}
          iselect
          (r :
            record t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj
              t_t) :=
          Build t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj t_t
            r.(op_case) r.(encoding) r.(kind) iselect r.(select) r.(proj)
            r.(inj) r.(t).
        Definition with_select
          {t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj t_t}
          select
          (r :
            record t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj
              t_t) :=
          Build t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj t_t
            r.(op_case) r.(encoding) r.(kind) r.(iselect) select r.(proj)
            r.(inj) r.(t).
        Definition with_proj
          {t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj t_t} proj
          (r :
            record t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj
              t_t) :=
          Build t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj t_t
            r.(op_case) r.(encoding) r.(kind) r.(iselect) r.(select) proj
            r.(inj) r.(t).
        Definition with_inj
          {t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj t_t} inj
          (r :
            record t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj
              t_t) :=
          Build t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj t_t
            r.(op_case) r.(encoding) r.(kind) r.(iselect) r.(select) r.(proj)
            inj r.(t).
        Definition with_t
          {t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj t_t} t
          (r :
            record t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj
              t_t) :=
          Build t_op_case t_encoding t_kind t_iselect t_select t_proj t_inj t_t
            r.(op_case) r.(encoding) r.(kind) r.(iselect) r.(select) r.(proj)
            r.(inj) t.
      End MCase.
      Definition MCase_skeleton := MCase.record.
    End case.
  End ConstructorRecords_case.
  Import ConstructorRecords_case.
  
  Reserved Notation "'case.MCase".
  
  Inductive case (kind : Set) : Set :=
  | MCase : forall {a : Set}, 'case.MCase a -> case kind
  
  where "'case.MCase" :=
    (fun (t_a : Set) => case.MCase_skeleton
      Alpha_context.Operation.Encoding.Manager_operations.case
      (Data_encoding.t t_a) Alpha_context.Kind.manager
      (packed_internal_operation_result ->
      option (Alpha_context.internal_operation * manager_operation_result))
      (packed_successful_manager_operation_result ->
      option successful_manager_operation_result)
      (successful_manager_operation_result -> t_a)
      (t_a -> successful_manager_operation_result)
      (Data_encoding.t manager_operation_result)).
  
  Module case.
    Include ConstructorRecords_case.case.
    Definition MCase := 'case.MCase.
  End case.
  
  Arguments MCase {_ _}.
  
  Definition make {A B : Set}
    (op_case : Alpha_context.Operation.Encoding.Manager_operations.case)
    (encoding : Data_encoding.encoding A)
    (kind_value : Alpha_context.Kind.manager)
    (iselect :
      packed_internal_operation_result ->
      option (Alpha_context.internal_operation * manager_operation_result))
    (select :
      packed_successful_manager_operation_result ->
      option successful_manager_operation_result)
    (proj : successful_manager_operation_result -> A)
    (inj : A -> successful_manager_operation_result) : case B :=
    let
      'Alpha_context.Operation.Encoding.Manager_operations.MCase {|
        Alpha_context.Operation.Encoding.Manager_operations.case.MCase.name := name
          |} := op_case in
    let t_value :=
      (let arg :=
        Data_encoding.def
          (Format.asprintf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal
                "operation.alpha.operation_result."
                (CamlinternalFormatBasics.String
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.End_of_format))
              "operation.alpha.operation_result.%s") name) in
      fun (eta : Data_encoding.encoding manager_operation_result) =>
        arg None None eta)
        (Data_encoding.union (Some Data_encoding.Uint8)
          [
            Data_encoding.case_value "Applied" None (Data_encoding.Tag 0)
              (Data_encoding.merge_objs
                (Data_encoding.obj1
                  (Data_encoding.req None None "status"
                    (Data_encoding.constant
                      "applied")))
                encoding)
              (fun (o_value : manager_operation_result) =>
                match o_value with
                | (Skipped _ | Failed _ _ | Backtracked _ _) =>
                  None
                | Applied o_value =>
                  match
                    select
                      (Successful_manager_result
                        o_value)
                    with
                  | None => None
                  | Some o_value =>
                    Some (tt, (proj o_value))
                  end
                end)
              (fun (function_parameter : unit * A) =>
                let '(_, x_value) := function_parameter in
                Applied (inj x_value));
            Data_encoding.case_value "Failed" None (Data_encoding.Tag 1)
              (Data_encoding.obj2
                (Data_encoding.req None None "status"
                  (Data_encoding.constant "failed"))
                (Data_encoding.req None None "errors"
                  trace_encoding))
              (fun (function_parameter : manager_operation_result) =>
                match function_parameter with
                | Failed _ errs => Some (tt, errs)
                | _ => None
                end)
              (fun (function_parameter :
                unit * Error_monad.trace Error_monad._error) =>
                let '(_, errs) := function_parameter in
                Failed kind_value errs);
            Data_encoding.case_value "Skipped" None (Data_encoding.Tag 2)
              (Data_encoding.obj1
                (Data_encoding.req None None "status"
                  (Data_encoding.constant "skipped")))
              (fun (function_parameter : manager_operation_result) =>
                match function_parameter with
                | Skipped _ => Some tt
                | _ => None
                end)
              (fun (function_parameter : unit) =>
                let '_ := function_parameter in
                Skipped kind_value);
            Data_encoding.case_value "Backtracked" None (Data_encoding.Tag 3)
              (Data_encoding.merge_objs
                (Data_encoding.obj2
                  (Data_encoding.req None None "status"
                    (Data_encoding.constant
                      "backtracked"))
                  (Data_encoding.opt None None "errors"
                    trace_encoding)) encoding)
              (fun (o_value : manager_operation_result) =>
                match o_value with
                | (Skipped _ | Failed _ _ | Applied _) => None
                | Backtracked o_value errs =>
                  match
                    select
                      (Successful_manager_result
                        o_value)
                    with
                  | None => None
                  | Some o_value =>
                    Some
                      ((tt, errs),
                        (proj
                          o_value))
                  end
                end)
              (fun (function_parameter :
                (unit *
                  option
                    (Error_monad.trace
                      Error_monad._error))
                  * A) =>
                let '((_, errs), x_value) := function_parameter
                  in
                Backtracked (inj x_value) errs)
          ]) in
    MCase
      {| case.MCase.op_case := op_case; case.MCase.encoding := encoding;
        case.MCase.kind := kind_value; case.MCase.iselect := iselect;
        case.MCase.select := select; case.MCase.proj := proj;
        case.MCase.inj := inj; case.MCase.t := t_value |}.
  
  Axiom reveal_case : case Alpha_context.Kind.reveal.
  
  Axiom transaction_case : case Alpha_context.Kind.transaction.
  
  Axiom origination_case : case Alpha_context.Kind.origination.
  
  Axiom register_global_constant_case :
    case Alpha_context.Kind.register_global_constant.
  
  Definition delegation_case : case Alpha_context.Kind.delegation :=
    make Alpha_context.Operation.Encoding.Manager_operations.delegation_case
      (Data_encoding.obj2
        (Data_encoding.dft None None "consumed_gas"
          Alpha_context.Gas.Arith.n_integral_encoding
          Alpha_context.Gas.Arith.zero)
        (Data_encoding.dft None None "consumed_milligas"
          Alpha_context.Gas.Arith.n_fp_encoding Alpha_context.Gas.Arith.zero))
      Alpha_context.Kind.Delegation_manager_kind
      (fun (function_parameter : packed_internal_operation_result) =>
        match function_parameter with
        |
          Internal_operation_result
            ({|
              Alpha_context.internal_operation.operation := Alpha_context.Delegation _
                |} as op) res => Some (op, res)
        | _ => None
        end)
      (fun (function_parameter : packed_successful_manager_operation_result) =>
        match function_parameter with
        | Successful_manager_result ((Delegation_result _) as op) => Some op
        | _ => None
        end)
      (fun (function_parameter : successful_manager_operation_result) =>
        match function_parameter with
        |
          Delegation_result {|
            successful_manager_operation_result.Delegation_result.consumed_gas :=
              consumed_gas
              |} => ((Alpha_context.Gas.Arith.ceil consumed_gas), consumed_gas)
        | _ => unreachable_gadt_branch
        end)
      (fun (function_parameter :
        Alpha_context.Gas.Arith.integral * Alpha_context.Gas.Arith.fp) =>
        let '(consumed_gas, consumed_milligas) := function_parameter in
        let '_ :=
          (* ❌ Assert instruction is not handled. *)
          assert unit
            (Alpha_context.Gas.Arith.equal
              (Alpha_context.Gas.Arith.ceil consumed_milligas) consumed_gas) in
        Delegation_result
          {|
            successful_manager_operation_result.Delegation_result.consumed_gas
              := consumed_milligas |}).
  
  Definition set_deposits_limit_case
    : case Alpha_context.Kind.set_deposits_limit :=
    make
      Alpha_context.Operation.Encoding.Manager_operations.set_deposits_limit_case
      (Data_encoding.obj2
        (Data_encoding.dft None None "consumed_gas"
          Alpha_context.Gas.Arith.n_integral_encoding
          Alpha_context.Gas.Arith.zero)
        (Data_encoding.dft None None "consumed_milligas"
          Alpha_context.Gas.Arith.n_fp_encoding Alpha_context.Gas.Arith.zero))
      Alpha_context.Kind.Set_deposits_limit_manager_kind
      (fun (function_parameter : packed_internal_operation_result) =>
        match function_parameter with
        |
          Internal_operation_result
            ({|
              Alpha_context.internal_operation.operation :=
                Alpha_context.Set_deposits_limit _
                |} as op) res => Some (op, res)
        | _ => None
        end)
      (fun (function_parameter : packed_successful_manager_operation_result) =>
        match function_parameter with
        | Successful_manager_result ((Set_deposits_limit_result _) as op) =>
          Some op
        | _ => None
        end)
      (fun (function_parameter : successful_manager_operation_result) =>
        match function_parameter with
        |
          Set_deposits_limit_result {|
            successful_manager_operation_result.Set_deposits_limit_result.consumed_gas :=
              consumed_gas
              |} => ((Alpha_context.Gas.Arith.ceil consumed_gas), consumed_gas)
        | _ => unreachable_gadt_branch
        end)
      (fun (function_parameter :
        Alpha_context.Gas.Arith.integral * Alpha_context.Gas.Arith.fp) =>
        let '(consumed_gas, consumed_milligas) := function_parameter in
        let '_ :=
          (* ❌ Assert instruction is not handled. *)
          assert unit
            (Alpha_context.Gas.Arith.equal
              (Alpha_context.Gas.Arith.ceil consumed_milligas) consumed_gas) in
        Set_deposits_limit_result
          {|
            successful_manager_operation_result.Set_deposits_limit_result.consumed_gas
              := consumed_milligas |}).
End Manager_result.

Definition internal_operation_result_encoding
  : Data_encoding.t packed_internal_operation_result :=
  let make {kind : Set} (function_parameter : Manager_result.case kind)
    : Data_encoding.case packed_internal_operation_result :=
    let 'Manager_result.MCase res_case := function_parameter in
    let 'Alpha_context.Operation.Encoding.Manager_operations.MCase op_case :=
      res_case.(Manager_result.case.MCase.op_case) in
    let 'existT _ __MCase_'a1 op_case :=
      existT (A := Set)
        (fun __MCase_'a1 =>
          Alpha_context.Operation.Encoding.Manager_operations.case.MCase
            __MCase_'a1) _ op_case in
    Data_encoding.case_value
      op_case.(Alpha_context.Operation.Encoding.Manager_operations.case.MCase.name)
      None
      (Data_encoding.Tag
        op_case.(Alpha_context.Operation.Encoding.Manager_operations.case.MCase.tag))
      (Data_encoding.merge_objs
        (Data_encoding.obj3
          (Data_encoding.req None None "kind"
            (Data_encoding.constant
              op_case.(Alpha_context.Operation.Encoding.Manager_operations.case.MCase.name)))
          (Data_encoding.req None None "source" Alpha_context.Contract.encoding)
          (Data_encoding.req None None "nonce" Data_encoding.uint16))
        (Data_encoding.merge_objs
          op_case.(Alpha_context.Operation.Encoding.Manager_operations.case.MCase.encoding)
          (Data_encoding.obj1
            (Data_encoding.req None None "result"
              res_case.(Manager_result.case.MCase.t)))))
      (fun (op : packed_internal_operation_result) =>
        match res_case.(Manager_result.case.MCase.iselect) op with
        | Some (op, res) =>
          Some
            ((tt, op.(Alpha_context.internal_operation.source),
              op.(Alpha_context.internal_operation.nonce)),
              ((op_case.(Alpha_context.Operation.Encoding.Manager_operations.case.MCase.proj)
                op.(Alpha_context.internal_operation.operation)), res))
        | None => None
        end)
      (fun (function_parameter :
        (unit * Alpha_context.Contract.t * int) *
          (__MCase_'a1 * manager_operation_result)) =>
        let '((_, source, nonce_value), (op, res)) := function_parameter in
        let op :=
          {| Alpha_context.internal_operation.source := source;
            Alpha_context.internal_operation.operation :=
              op_case.(Alpha_context.Operation.Encoding.Manager_operations.case.MCase.inj)
                op; Alpha_context.internal_operation.nonce := nonce_value |} in
        Internal_operation_result op res) in
  (let arg := Data_encoding.def "operation.alpha.internal_operation_result" in
  fun (eta : Data_encoding.encoding packed_internal_operation_result) =>
    arg None None eta)
    (Data_encoding.union None
      [
        make Manager_result.reveal_case;
        make Manager_result.transaction_case;
        make Manager_result.origination_case;
        make Manager_result.delegation_case;
        make Manager_result.register_global_constant_case;
        make Manager_result.set_deposits_limit_case
      ]).

Definition successful_manager_operation_result_encoding
  : Data_encoding.t packed_successful_manager_operation_result :=
  let make {kind : Set} (mcase : Manager_result.case kind)
    : Data_encoding.case packed_successful_manager_operation_result :=
    let 'Manager_result.MCase res_case := mcase in
    let 'existT _ __MCase_'a res_case :=
      existT (A := Set) (fun __MCase_'a => Manager_result.case.MCase __MCase_'a)
        _ res_case in
    let 'Alpha_context.Operation.Encoding.Manager_operations.MCase op_case :=
      res_case.(Manager_result.case.MCase.op_case) in
    Data_encoding.case_value
      op_case.(Alpha_context.Operation.Encoding.Manager_operations.case.MCase.name)
      None
      (Data_encoding.Tag
        op_case.(Alpha_context.Operation.Encoding.Manager_operations.case.MCase.tag))
      (Data_encoding.merge_objs
        (Data_encoding.obj1
          (Data_encoding.req None None "kind"
            (Data_encoding.constant
              op_case.(Alpha_context.Operation.Encoding.Manager_operations.case.MCase.name))))
        res_case.(Manager_result.case.MCase.encoding))
      (fun (res : packed_successful_manager_operation_result) =>
        match res_case.(Manager_result.case.MCase.select) res with
        | Some res => Some (tt, (res_case.(Manager_result.case.MCase.proj) res))
        | None => None
        end)
      (fun (function_parameter : unit * __MCase_'a) =>
        let '(_, res) := function_parameter in
        Successful_manager_result (res_case.(Manager_result.case.MCase.inj) res))
    in
  (let arg :=
    Data_encoding.def "operation.alpha.successful_manager_operation_result" in
  fun (eta : Data_encoding.encoding packed_successful_manager_operation_result)
    => arg None None eta)
    (Data_encoding.union None
      [
        make Manager_result.reveal_case;
        make Manager_result.transaction_case;
        make Manager_result.origination_case;
        make Manager_result.delegation_case;
        make Manager_result.set_deposits_limit_case
      ]).

(** Records for the constructor parameters *)
Module ConstructorRecords_contents_result.
  Module contents_result.
    Module Preendorsement_result.
      Record record {balance_updates delegate preendorsement_power : Set} : Set := Build {
        balance_updates : balance_updates;
        delegate : delegate;
        preendorsement_power : preendorsement_power }.
      Arguments record : clear implicits.
      Definition with_balance_updates
        {t_balance_updates t_delegate t_preendorsement_power} balance_updates
        (r : record t_balance_updates t_delegate t_preendorsement_power) :=
        Build t_balance_updates t_delegate t_preendorsement_power
          balance_updates r.(delegate) r.(preendorsement_power).
      Definition with_delegate
        {t_balance_updates t_delegate t_preendorsement_power} delegate
        (r : record t_balance_updates t_delegate t_preendorsement_power) :=
        Build t_balance_updates t_delegate t_preendorsement_power
          r.(balance_updates) delegate r.(preendorsement_power).
      Definition with_preendorsement_power
        {t_balance_updates t_delegate t_preendorsement_power}
        preendorsement_power
        (r : record t_balance_updates t_delegate t_preendorsement_power) :=
        Build t_balance_updates t_delegate t_preendorsement_power
          r.(balance_updates) r.(delegate) preendorsement_power.
    End Preendorsement_result.
    Definition Preendorsement_result_skeleton := Preendorsement_result.record.
    
    Module Endorsement_result.
      Record record {balance_updates delegate endorsement_power : Set} : Set := Build {
        balance_updates : balance_updates;
        delegate : delegate;
        endorsement_power : endorsement_power }.
      Arguments record : clear implicits.
      Definition with_balance_updates
        {t_balance_updates t_delegate t_endorsement_power} balance_updates
        (r : record t_balance_updates t_delegate t_endorsement_power) :=
        Build t_balance_updates t_delegate t_endorsement_power balance_updates
          r.(delegate) r.(endorsement_power).
      Definition with_delegate
        {t_balance_updates t_delegate t_endorsement_power} delegate
        (r : record t_balance_updates t_delegate t_endorsement_power) :=
        Build t_balance_updates t_delegate t_endorsement_power
          r.(balance_updates) delegate r.(endorsement_power).
      Definition with_endorsement_power
        {t_balance_updates t_delegate t_endorsement_power} endorsement_power
        (r : record t_balance_updates t_delegate t_endorsement_power) :=
        Build t_balance_updates t_delegate t_endorsement_power
          r.(balance_updates) r.(delegate) endorsement_power.
    End Endorsement_result.
    Definition Endorsement_result_skeleton := Endorsement_result.record.
    
    Module Manager_operation_result.
      Record record {balance_updates operation_result internal_operation_results
        : Set} : Set := Build {
        balance_updates : balance_updates;
        operation_result : operation_result;
        internal_operation_results : internal_operation_results }.
      Arguments record : clear implicits.
      Definition with_balance_updates
        {t_balance_updates t_operation_result t_internal_operation_results}
        balance_updates
        (r :
          record t_balance_updates t_operation_result
            t_internal_operation_results) :=
        Build t_balance_updates t_operation_result t_internal_operation_results
          balance_updates r.(operation_result) r.(internal_operation_results).
      Definition with_operation_result
        {t_balance_updates t_operation_result t_internal_operation_results}
        operation_result
        (r :
          record t_balance_updates t_operation_result
            t_internal_operation_results) :=
        Build t_balance_updates t_operation_result t_internal_operation_results
          r.(balance_updates) operation_result r.(internal_operation_results).
      Definition with_internal_operation_results
        {t_balance_updates t_operation_result t_internal_operation_results}
        internal_operation_results
        (r :
          record t_balance_updates t_operation_result
            t_internal_operation_results) :=
        Build t_balance_updates t_operation_result t_internal_operation_results
          r.(balance_updates) r.(operation_result) internal_operation_results.
    End Manager_operation_result.
    Definition Manager_operation_result_skeleton :=
      Manager_operation_result.record.
  End contents_result.
End ConstructorRecords_contents_result.
Import ConstructorRecords_contents_result.

Reserved Notation "'contents_result.Preendorsement_result".
Reserved Notation "'contents_result.Endorsement_result".
Reserved Notation "'contents_result.Manager_operation_result".

Inductive contents_result : Set :=
| Preendorsement_result :
  'contents_result.Preendorsement_result -> contents_result
| Endorsement_result : 'contents_result.Endorsement_result -> contents_result
| Seed_nonce_revelation_result :
  Alpha_context.Receipt.balance_updates -> contents_result
| Double_endorsement_evidence_result :
  Alpha_context.Receipt.balance_updates -> contents_result
| Double_preendorsement_evidence_result :
  Alpha_context.Receipt.balance_updates -> contents_result
| Double_baking_evidence_result :
  Alpha_context.Receipt.balance_updates -> contents_result
| Activate_account_result :
  Alpha_context.Receipt.balance_updates -> contents_result
| Proposals_result : contents_result
| Ballot_result : contents_result
| Manager_operation_result :
  'contents_result.Manager_operation_result -> contents_result

where "'contents_result.Preendorsement_result" :=
  (contents_result.Preendorsement_result_skeleton
    Alpha_context.Receipt.balance_updates Signature.public_key_hash int)
and "'contents_result.Endorsement_result" :=
  (contents_result.Endorsement_result_skeleton
    Alpha_context.Receipt.balance_updates Signature.public_key_hash int)
and "'contents_result.Manager_operation_result" :=
  (contents_result.Manager_operation_result_skeleton
    Alpha_context.Receipt.balance_updates manager_operation_result
    (list packed_internal_operation_result)).

Module contents_result.
  Include ConstructorRecords_contents_result.contents_result.
  Definition Preendorsement_result := 'contents_result.Preendorsement_result.
  Definition Endorsement_result := 'contents_result.Endorsement_result.
  Definition Manager_operation_result :=
    'contents_result.Manager_operation_result.
End contents_result.

Inductive packed_contents_result : Set :=
| Contents_result : contents_result -> packed_contents_result.

Inductive packed_contents_and_result : Set :=
| Contents_and_result :
  Alpha_context.Operation.contents -> contents_result ->
  packed_contents_and_result.

Inductive eq : Set :=
| Eq : eq.

Definition equal_manager_kind
  (ka : Alpha_context.Kind.manager) (kb : Alpha_context.Kind.manager)
  : option eq :=
  match (ka, kb) with
  |
    (Alpha_context.Kind.Reveal_manager_kind,
      Alpha_context.Kind.Reveal_manager_kind) => Some Eq
  | (Alpha_context.Kind.Reveal_manager_kind, _) => None
  |
    (Alpha_context.Kind.Transaction_manager_kind,
      Alpha_context.Kind.Transaction_manager_kind) => Some Eq
  | (Alpha_context.Kind.Transaction_manager_kind, _) => None
  |
    (Alpha_context.Kind.Origination_manager_kind,
      Alpha_context.Kind.Origination_manager_kind) => Some Eq
  | (Alpha_context.Kind.Origination_manager_kind, _) => None
  |
    (Alpha_context.Kind.Delegation_manager_kind,
      Alpha_context.Kind.Delegation_manager_kind) => Some Eq
  | (Alpha_context.Kind.Delegation_manager_kind, _) => None
  |
    (Alpha_context.Kind.Register_global_constant_manager_kind,
      Alpha_context.Kind.Register_global_constant_manager_kind) => Some Eq
  | (Alpha_context.Kind.Register_global_constant_manager_kind, _) => None
  |
    (Alpha_context.Kind.Set_deposits_limit_manager_kind,
      Alpha_context.Kind.Set_deposits_limit_manager_kind) => Some Eq
  | (Alpha_context.Kind.Set_deposits_limit_manager_kind, _) => None
  end.

Module Encoding.
  (** Records for the constructor parameters *)
  Module ConstructorRecords_case.
    Module case.
      Module Case.
        Record record {op_case encoding select mselect proj inj : Set} : Set := Build {
          op_case : op_case;
          encoding : encoding;
          select : select;
          mselect : mselect;
          proj : proj;
          inj : inj }.
        Arguments record : clear implicits.
        Definition with_op_case
          {t_op_case t_encoding t_select t_mselect t_proj t_inj} op_case
          (r : record t_op_case t_encoding t_select t_mselect t_proj t_inj) :=
          Build t_op_case t_encoding t_select t_mselect t_proj t_inj op_case
            r.(encoding) r.(select) r.(mselect) r.(proj) r.(inj).
        Definition with_encoding
          {t_op_case t_encoding t_select t_mselect t_proj t_inj} encoding
          (r : record t_op_case t_encoding t_select t_mselect t_proj t_inj) :=
          Build t_op_case t_encoding t_select t_mselect t_proj t_inj r.(op_case)
            encoding r.(select) r.(mselect) r.(proj) r.(inj).
        Definition with_select
          {t_op_case t_encoding t_select t_mselect t_proj t_inj} select
          (r : record t_op_case t_encoding t_select t_mselect t_proj t_inj) :=
          Build t_op_case t_encoding t_select t_mselect t_proj t_inj r.(op_case)
            r.(encoding) select r.(mselect) r.(proj) r.(inj).
        Definition with_mselect
          {t_op_case t_encoding t_select t_mselect t_proj t_inj} mselect
          (r : record t_op_case t_encoding t_select t_mselect t_proj t_inj) :=
          Build t_op_case t_encoding t_select t_mselect t_proj t_inj r.(op_case)
            r.(encoding) r.(select) mselect r.(proj) r.(inj).
        Definition with_proj
          {t_op_case t_encoding t_select t_mselect t_proj t_inj} proj
          (r : record t_op_case t_encoding t_select t_mselect t_proj t_inj) :=
          Build t_op_case t_encoding t_select t_mselect t_proj t_inj r.(op_case)
            r.(encoding) r.(select) r.(mselect) proj r.(inj).
        Definition with_inj
          {t_op_case t_encoding t_select t_mselect t_proj t_inj} inj
          (r : record t_op_case t_encoding t_select t_mselect t_proj t_inj) :=
          Build t_op_case t_encoding t_select t_mselect t_proj t_inj r.(op_case)
            r.(encoding) r.(select) r.(mselect) r.(proj) inj.
      End Case.
      Definition Case_skeleton := Case.record.
    End case.
  End ConstructorRecords_case.
  Import ConstructorRecords_case.
  
  Reserved Notation "'case.Case".
  
  Inductive case (kind : Set) : Set :=
  | Case : forall {a : Set}, 'case.Case kind a -> case kind
  
  where "'case.Case" :=
    (fun (t_kind t_a : Set) => case.Case_skeleton
      (Alpha_context.Operation.Encoding.case t_kind) (Data_encoding.t t_a)
      (packed_contents_result -> option contents_result)
      (packed_contents_and_result ->
      option (Alpha_context.contents * contents_result))
      (contents_result -> t_a) (t_a -> contents_result)).
  
  Module case.
    Include ConstructorRecords_case.case.
    Definition Case := 'case.Case.
  End case.
  
  Arguments Case {_ _}.
  
  Definition tagged_case {A B : Set}
    (tag : Data_encoding.case_tag) (name : string)
    (args : Data_encoding.encoding A) (proj : B -> option A) (inj : A -> B)
    : Data_encoding.case B :=
    Data_encoding.case_value (String.capitalize_ascii name) None tag
      (Data_encoding.merge_objs
        (Data_encoding.obj1
          (Data_encoding.req None None "kind" (Data_encoding.constant name)))
        args)
      (fun (x_value : B) =>
        match proj x_value with
        | None => None
        | Some x_value => Some (tt, x_value)
        end)
      (fun (function_parameter : unit * A) =>
        let '(_, x_value) := function_parameter in
        inj x_value).
  
  Axiom preendorsement_case : case Alpha_context.Kind.preendorsement.
  
  Axiom endorsement_case : case Alpha_context.Kind.endorsement.
  
  Axiom seed_nonce_revelation_case :
    case Alpha_context.Kind.seed_nonce_revelation.
  
  Axiom double_endorsement_evidence_case :
    case Alpha_context.Kind.double_endorsement_evidence.
  
  Axiom double_preendorsement_evidence_case :
    case Alpha_context.Kind.double_preendorsement_evidence.
  
  Axiom double_baking_evidence_case :
    case Alpha_context.Kind.double_baking_evidence.
  
  Axiom activate_account_case : case Alpha_context.Kind.activate_account.
  
  Axiom proposals_case : case Alpha_context.Kind.proposals.
  
  Axiom ballot_case : case Alpha_context.Kind.ballot.
  
  Axiom make_manager_case : forall {kind : Set},
    Alpha_context.Operation.Encoding.case Alpha_context.Kind.manager ->
    Manager_result.case kind ->
    (packed_contents_and_result ->
    option (Alpha_context.contents * contents_result)) ->
    case Alpha_context.Kind.manager.
  
  Axiom reveal_case : case Alpha_context.Kind.manager.
  
  Axiom transaction_case : case Alpha_context.Kind.manager.
  
  Axiom origination_case : case Alpha_context.Kind.manager.
  
  Axiom delegation_case : case Alpha_context.Kind.manager.
  
  Axiom register_global_constant_case : case Alpha_context.Kind.manager.
  
  Axiom set_deposits_limit_case : case Alpha_context.Kind.manager.
End Encoding.

Definition contents_result_encoding
  : Data_encoding.encoding packed_contents_result :=
  let make {A : Set} (case_description : Encoding.case A)
    : Data_encoding.case packed_contents_result :=
    let
      'Encoding.Case {|
        Encoding.case.Case.op_case :=
          Alpha_context.Operation.Encoding.Case {|
            Alpha_context.Operation.Encoding.case.Case.tag := tag;
              Alpha_context.Operation.Encoding.case.Case.name :=
                name
              |};
          Encoding.case.Case.encoding := encoding;
          Encoding.case.Case.select := select;
          Encoding.case.Case.mselect := _;
          Encoding.case.Case.proj := proj;
          Encoding.case.Case.inj := inj
          |} := case_description in
    let 'existT _ __Case_'a [inj, proj, select, encoding, name, tag] :=
      existT (A := Set)
        (fun __Case_'a =>
          [__Case_'a -> contents_result ** contents_result -> __Case_'a **
            packed_contents_result -> option contents_result **
            Data_encoding.t __Case_'a ** string ** int]) _
        [inj, proj, select, encoding, name, tag] in
    let proj (x_value : packed_contents_result) : option __Case_'a :=
      match select x_value with
      | None => None
      | Some x_value => Some (proj x_value)
      end in
    let inj (x_value : __Case_'a) : packed_contents_result :=
      Contents_result (inj x_value) in
    Encoding.tagged_case (Data_encoding.Tag tag) name encoding proj inj in
  (let arg := Data_encoding.def "operation.alpha.contents_result" in
  fun (eta : Data_encoding.encoding packed_contents_result) => arg None None eta)
    (Data_encoding.union None
      [
        make Encoding.seed_nonce_revelation_case;
        make Encoding.endorsement_case;
        make Encoding.preendorsement_case;
        make Encoding.double_preendorsement_evidence_case;
        make Encoding.double_endorsement_evidence_case;
        make Encoding.double_baking_evidence_case;
        make Encoding.activate_account_case;
        make Encoding.proposals_case;
        make Encoding.ballot_case;
        make Encoding.reveal_case;
        make Encoding.transaction_case;
        make Encoding.origination_case;
        make Encoding.delegation_case;
        make Encoding.register_global_constant_case;
        make Encoding.set_deposits_limit_case
      ]).

Definition contents_and_result_encoding
  : Data_encoding.encoding packed_contents_and_result :=
  let make {A : Set} (case_description : Encoding.case A)
    : Data_encoding.case packed_contents_and_result :=
    let
      'Encoding.Case {|
        Encoding.case.Case.op_case :=
          Alpha_context.Operation.Encoding.Case {|
            Alpha_context.Operation.Encoding.case.Case.tag := tag;
              Alpha_context.Operation.Encoding.case.Case.name :=
                name;
              Alpha_context.Operation.Encoding.case.Case.encoding :=
                encoding;
              Alpha_context.Operation.Encoding.case.Case.proj :=
                proj;
              Alpha_context.Operation.Encoding.case.Case.inj := inj
              |};
          Encoding.case.Case.encoding := meta_encoding;
          Encoding.case.Case.mselect := mselect;
          Encoding.case.Case.proj := meta_proj;
          Encoding.case.Case.inj := meta_inj
          |} := case_description in
    let 'existT _ [__Case_'a, __Case_'a1]
      [meta_inj, meta_proj, mselect, meta_encoding, inj, proj, encoding, name,
        tag] :=
      existT (A := [Set ** Set])
        (fun '[__Case_'a, __Case_'a1] =>
          [__Case_'a -> contents_result ** contents_result -> __Case_'a **
            packed_contents_and_result ->
            option (Alpha_context.contents * contents_result) **
            Data_encoding.t __Case_'a **
            __Case_'a1 -> Alpha_context.Operation.contents **
            Alpha_context.Operation.contents -> __Case_'a1 **
            Data_encoding.t __Case_'a1 ** string ** int]) [_, _]
        [meta_inj, meta_proj, mselect, meta_encoding, inj, proj, encoding, name,
          tag] in
    let proj (c_value : packed_contents_and_result)
      : option (__Case_'a1 * __Case_'a) :=
      match mselect c_value with
      | Some (op, res) => Some ((proj op), (meta_proj res))
      | _ => None
      end in
    let inj (function_parameter : __Case_'a1 * __Case_'a)
      : packed_contents_and_result :=
      let '(op, res) := function_parameter in
      Contents_and_result (inj op) (meta_inj res) in
    let encoding :=
      Data_encoding.merge_objs encoding
        (Data_encoding.obj1
          (Data_encoding.req None None "metadata" meta_encoding)) in
    Encoding.tagged_case (Data_encoding.Tag tag) name encoding proj inj in
  (let arg := Data_encoding.def "operation.alpha.operation_contents_and_result"
    in
  fun (eta : Data_encoding.encoding packed_contents_and_result) =>
    arg None None eta)
    (Data_encoding.union None
      [
        make Encoding.seed_nonce_revelation_case;
        make Encoding.endorsement_case;
        make Encoding.preendorsement_case;
        make Encoding.double_preendorsement_evidence_case;
        make Encoding.double_endorsement_evidence_case;
        make Encoding.double_baking_evidence_case;
        make Encoding.activate_account_case;
        make Encoding.proposals_case;
        make Encoding.ballot_case;
        make Encoding.reveal_case;
        make Encoding.transaction_case;
        make Encoding.origination_case;
        make Encoding.delegation_case;
        make Encoding.register_global_constant_case;
        make Encoding.set_deposits_limit_case
      ]).

Inductive contents_result_list : Set :=
| Single_result : contents_result -> contents_result_list
| Cons_result : contents_result -> contents_result_list -> contents_result_list.

Inductive packed_contents_result_list : Set :=
| Contents_result_list : contents_result_list -> packed_contents_result_list.

Definition contents_result_list_encoding
  : Data_encoding.encoding packed_contents_result_list :=
  let fix to_list (function_parameter : packed_contents_result_list)
    : list packed_contents_result :=
    match function_parameter with
    | Contents_result_list (Single_result o_value) =>
      [ Contents_result o_value ]
    | Contents_result_list (Cons_result o_value os) =>
      cons (Contents_result o_value) (to_list (Contents_result_list os))
    end in
  let fix of_list (function_parameter : list packed_contents_result)
    : Pervasives.result packed_contents_result_list string :=
    match function_parameter with
    | [] => Pervasives.Error "cannot decode empty operation result"
    | cons (Contents_result o_value) [] =>
      Pervasives.Ok (Contents_result_list (Single_result o_value))
    | cons (Contents_result o_value) os =>
      let? 'Contents_result_list os := of_list os in
      match (o_value, os) with
      | (Manager_operation_result _, Single_result (Manager_operation_result _))
        => Pervasives.Ok (Contents_result_list (Cons_result o_value os))
      | (Manager_operation_result _, Cons_result _ _) =>
        Pervasives.Ok (Contents_result_list (Cons_result o_value os))
      | _ => Pervasives.Error "cannot decode ill-formed operation result"
      end
    end in
  (let arg := Data_encoding.def "operation.alpha.contents_list_result" in
  fun (eta : Data_encoding.encoding packed_contents_result_list) =>
    arg None None eta)
    (Data_encoding.conv_with_guard to_list of_list None
      (Data_encoding.list_value None contents_result_encoding)).

Inductive contents_and_result_list : Set :=
| Single_and_result :
  Alpha_context.contents -> contents_result -> contents_and_result_list
| Cons_and_result :
  Alpha_context.contents -> contents_result -> contents_and_result_list ->
  contents_and_result_list.

Inductive packed_contents_and_result_list : Set :=
| Contents_and_result_list :
  contents_and_result_list -> packed_contents_and_result_list.

Definition contents_and_result_list_encoding
  : Data_encoding.encoding packed_contents_and_result_list :=
  let fix to_list (function_parameter : packed_contents_and_result_list)
    : list packed_contents_and_result :=
    match function_parameter with
    | Contents_and_result_list (Single_and_result op res) =>
      [ Contents_and_result op res ]
    | Contents_and_result_list (Cons_and_result op res rest) =>
      cons (Contents_and_result op res)
        (to_list (Contents_and_result_list rest))
    end in
  let fix of_list (function_parameter : list packed_contents_and_result)
    : Pervasives.result packed_contents_and_result_list string :=
    match function_parameter with
    | [] => Pervasives.Error "cannot decode empty combined operation result"
    | cons (Contents_and_result op res) [] =>
      Pervasives.Ok (Contents_and_result_list (Single_and_result op res))
    | cons (Contents_and_result op res) rest =>
      let? 'Contents_and_result_list rest := of_list rest in
      match (op, rest) with
      |
        (Alpha_context.Manager_operation _,
          Single_and_result (Alpha_context.Manager_operation _) _) =>
        Pervasives.Ok (Contents_and_result_list (Cons_and_result op res rest))
      | (Alpha_context.Manager_operation _, Cons_and_result _ _ _) =>
        Pervasives.Ok (Contents_and_result_list (Cons_and_result op res rest))
      | _ =>
        Pervasives.Error "cannot decode ill-formed combined operation result"
      end
    end in
  Data_encoding.conv_with_guard to_list of_list None
    (Data_encoding._Variable.list_value None contents_and_result_encoding).

Module operation_metadata.
  Record record : Set := Build {
    contents : contents_result_list }.
  Definition with_contents contents (r : record) :=
    Build contents.
End operation_metadata.
Definition operation_metadata := operation_metadata.record.

Inductive packed_operation_metadata : Set :=
| Operation_metadata : operation_metadata -> packed_operation_metadata
| No_operation_metadata : packed_operation_metadata.

Definition operation_metadata_encoding
  : Data_encoding.encoding packed_operation_metadata :=
  (let arg := Data_encoding.def "operation.alpha.result" in
  fun (eta : Data_encoding.encoding packed_operation_metadata) =>
    arg None None eta)
    (Data_encoding.union None
      [
        Data_encoding.case_value "Operation_metadata" None (Data_encoding.Tag 0)
          contents_result_list_encoding
          (fun (function_parameter : packed_operation_metadata) =>
            match function_parameter with
            |
              Operation_metadata {|
                operation_metadata.contents := contents
                  |} => Some (Contents_result_list contents)
            | _ => None
            end)
          (fun (function_parameter : packed_contents_result_list) =>
            let 'Contents_result_list contents := function_parameter in
            Operation_metadata
              {| operation_metadata.contents := contents |});
        Data_encoding.case_value "No_operation_metadata" None
          (Data_encoding.Tag 1) Data_encoding.empty
          (fun (function_parameter : packed_operation_metadata) =>
            match function_parameter with
            | No_operation_metadata => Some tt
            | _ => None
            end)
          (fun (function_parameter : unit) =>
            let '_ := function_parameter in
            No_operation_metadata)
      ]).

Definition kind_equal (op : Alpha_context.contents) (res : contents_result)
  : option eq :=
  match (op, res) with
  | (Alpha_context.Endorsement _, Endorsement_result _) => Some Eq
  | (Alpha_context.Endorsement _, _) => None
  | (Alpha_context.Preendorsement _, Preendorsement_result _) => Some Eq
  | (Alpha_context.Preendorsement _, _) => None
  | (Alpha_context.Seed_nonce_revelation _, Seed_nonce_revelation_result _) =>
    Some Eq
  | (Alpha_context.Seed_nonce_revelation _, _) => None
  |
    (Alpha_context.Double_preendorsement_evidence _,
      Double_preendorsement_evidence_result _) => Some Eq
  | (Alpha_context.Double_preendorsement_evidence _, _) => None
  |
    (Alpha_context.Double_endorsement_evidence _,
      Double_endorsement_evidence_result _) => Some Eq
  | (Alpha_context.Double_endorsement_evidence _, _) => None
  | (Alpha_context.Double_baking_evidence _, Double_baking_evidence_result _) =>
    Some Eq
  | (Alpha_context.Double_baking_evidence _, _) => None
  | (Alpha_context.Activate_account _, Activate_account_result _) => Some Eq
  | (Alpha_context.Activate_account _, _) => None
  | (Alpha_context.Proposals _, Proposals_result) => Some Eq
  | (Alpha_context.Proposals _, _) => None
  | (Alpha_context.Ballot _, Ballot_result) => Some Eq
  | (Alpha_context.Ballot _, _) => None
  | (Alpha_context.Failing_noop _, _) => None
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation := Alpha_context.Reveal _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Applied (Reveal_result _)
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation := Alpha_context.Reveal _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Backtracked (Reveal_result _) _
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation := Alpha_context.Reveal _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Failed Alpha_context.Kind.Reveal_manager_kind _
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation := Alpha_context.Reveal _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Skipped Alpha_context.Kind.Reveal_manager_kind
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation := Alpha_context.Reveal _
        |}, _) => None
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Transaction _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Applied (Transaction_result _)
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Transaction _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Backtracked (Transaction_result _) _
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Transaction _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Failed Alpha_context.Kind.Transaction_manager_kind _
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Transaction _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Skipped Alpha_context.Kind.Transaction_manager_kind
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Transaction _
        |}, _) => None
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Origination _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Applied (Origination_result _)
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Origination _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Backtracked (Origination_result _) _
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Origination _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Failed Alpha_context.Kind.Origination_manager_kind _
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Origination _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Skipped Alpha_context.Kind.Origination_manager_kind
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Origination _
        |}, _) => None
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Delegation _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Applied (Delegation_result _)
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Delegation _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Backtracked (Delegation_result _) _
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Delegation _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Failed Alpha_context.Kind.Delegation_manager_kind _
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Delegation _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Skipped Alpha_context.Kind.Delegation_manager_kind
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Delegation _
        |}, _) => None
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Register_global_constant _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Applied (Register_global_constant_result _)
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Register_global_constant _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Backtracked (Register_global_constant_result _) _
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Register_global_constant _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Failed Alpha_context.Kind.Register_global_constant_manager_kind _
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Register_global_constant _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Skipped Alpha_context.Kind.Register_global_constant_manager_kind
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Register_global_constant _
        |}, _) => None
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Set_deposits_limit _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Applied (Set_deposits_limit_result _)
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Set_deposits_limit _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Backtracked (Set_deposits_limit_result _) _
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Set_deposits_limit _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Failed Alpha_context.Kind.Set_deposits_limit_manager_kind _
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Set_deposits_limit _
        |},
      Manager_operation_result {|
        contents_result.Manager_operation_result.operation_result :=
          Skipped Alpha_context.Kind.Set_deposits_limit_manager_kind
          |}) => Some Eq
  |
    (Alpha_context.Manager_operation {|
      Alpha_context.contents.Manager_operation.operation :=
        Alpha_context.Set_deposits_limit _
        |}, _) => None
  end.

Fixpoint kind_equal_list
  (contents : Alpha_context.contents_list) (res : contents_result_list)
  : option eq :=
  match (contents, res) with
  | (Alpha_context.Single op, Single_result res) =>
    match kind_equal op res with
    | None => None
    | Some Eq => Some Eq
    end
  | (Alpha_context.Cons op ops, Cons_result res ress) =>
    match kind_equal op res with
    | None => None
    | Some Eq =>
      match kind_equal_list ops ress with
      | None => None
      | Some Eq => Some Eq
      end
    end
  | _ => None
  end.

Axiom pack_contents_list :
  Alpha_context.contents_list -> contents_result_list ->
  contents_and_result_list.

Fixpoint unpack_contents_list (function_parameter : contents_and_result_list)
  : Alpha_context.contents_list * contents_result_list :=
  match function_parameter with
  | Single_and_result op res => ((Alpha_context.Single op), (Single_result res))
  | Cons_and_result op res rest =>
    let '(ops, ress) := unpack_contents_list rest in
    ((Alpha_context.Cons op ops), (Cons_result res ress))
  end.

Fixpoint to_list (function_parameter : packed_contents_result_list)
  : list packed_contents_result :=
  match function_parameter with
  | Contents_result_list (Single_result o_value) => [ Contents_result o_value ]
  | Contents_result_list (Cons_result o_value os) =>
    cons (Contents_result o_value) (to_list (Contents_result_list os))
  end.

Definition operation_data_and_metadata_encoding
  : Data_encoding.encoding
    (Alpha_context.packed_protocol_data * packed_operation_metadata) :=
  (let arg := Data_encoding.def "operation.alpha.operation_with_metadata" in
  fun (eta :
    Data_encoding.encoding
      (Alpha_context.packed_protocol_data * packed_operation_metadata)) =>
    arg None None eta)
    (Data_encoding.union None
      [
        Data_encoding.case_value "Operation_with_metadata" None
          (Data_encoding.Tag 0)
          (Data_encoding.obj2
            (Data_encoding.req None None "contents"
              (Data_encoding.dynamic_size None
                contents_and_result_list_encoding))
            (Data_encoding.opt None None "signature" Signature.encoding))
          (fun (function_parameter :
            Alpha_context.packed_protocol_data *
              packed_operation_metadata) =>
            match function_parameter with
            | (Alpha_context.Operation_data _, No_operation_metadata) =>
              None
            | (Alpha_context.Operation_data op, Operation_metadata res)
              =>
              match
                kind_equal_list
                  op.(Alpha_context.protocol_data.contents)
                  res.(operation_metadata.contents) with
              | None =>
                Pervasives.failwith
                  "cannot decode inconsistent combined operation result"
              | Some Eq =>
                Some
                  ((Contents_and_result_list
                    (pack_contents_list
                      op.(Alpha_context.protocol_data.contents)
                      res.(operation_metadata.contents))),
                    op.(Alpha_context.protocol_data.signature))
              end
            end)
          (fun (function_parameter :
            packed_contents_and_result_list * option Signature.t) =>
            let '(Contents_and_result_list contents, signature) :=
              function_parameter in
            let '(op_contents, res_contents) :=
              unpack_contents_list contents in
            ((Alpha_context.Operation_data
              {|
                Alpha_context.protocol_data.contents :=
                  op_contents;
                Alpha_context.protocol_data.signature :=
                  signature |}),
              (Operation_metadata
                {| operation_metadata.contents := res_contents
                  |})));
        Data_encoding.case_value "Operation_without_metadata" None
          (Data_encoding.Tag 1)
          (Data_encoding.obj2
            (Data_encoding.req None None "contents"
              (Data_encoding.dynamic_size None
                Alpha_context.Operation.contents_list_encoding))
            (Data_encoding.opt None None "signature" Signature.encoding))
          (fun (function_parameter :
            Alpha_context.packed_protocol_data *
              packed_operation_metadata) =>
            match function_parameter with
            | (Alpha_context.Operation_data op, No_operation_metadata)
              =>
              Some
                ((Alpha_context.Contents_list
                  op.(Alpha_context.protocol_data.contents)),
                  op.(Alpha_context.protocol_data.signature))
            | (Alpha_context.Operation_data _, Operation_metadata _) =>
              None
            end)
          (fun (function_parameter :
            Alpha_context.packed_contents_list * option Signature.t) =>
            let '(Alpha_context.Contents_list contents, signature) :=
              function_parameter in
            ((Alpha_context.Operation_data
              {| Alpha_context.protocol_data.contents := contents;
                Alpha_context.protocol_data.signature :=
                  signature |}), No_operation_metadata))
      ]).

Module block_metadata.
  Record record : Set := Build {
    proposer : Signature.public_key_hash;
    baker : Signature.public_key_hash;
    level_info : Alpha_context.Level.t;
    voting_period_info : Alpha_context.Voting_period.info;
    nonce_hash : option Nonce_hash.t;
    consumed_gas : Alpha_context.Gas.Arith.fp;
    deactivated : list Signature.public_key_hash;
    balance_updates : Alpha_context.Receipt.balance_updates;
    liquidity_baking_escape_ema : Alpha_context.Liquidity_baking.escape_ema;
    implicit_operations_results :
      list packed_successful_manager_operation_result }.
  Definition with_proposer proposer (r : record) :=
    Build proposer r.(baker) r.(level_info) r.(voting_period_info)
      r.(nonce_hash) r.(consumed_gas) r.(deactivated) r.(balance_updates)
      r.(liquidity_baking_escape_ema) r.(implicit_operations_results).
  Definition with_baker baker (r : record) :=
    Build r.(proposer) baker r.(level_info) r.(voting_period_info)
      r.(nonce_hash) r.(consumed_gas) r.(deactivated) r.(balance_updates)
      r.(liquidity_baking_escape_ema) r.(implicit_operations_results).
  Definition with_level_info level_info (r : record) :=
    Build r.(proposer) r.(baker) level_info r.(voting_period_info)
      r.(nonce_hash) r.(consumed_gas) r.(deactivated) r.(balance_updates)
      r.(liquidity_baking_escape_ema) r.(implicit_operations_results).
  Definition with_voting_period_info voting_period_info (r : record) :=
    Build r.(proposer) r.(baker) r.(level_info) voting_period_info
      r.(nonce_hash) r.(consumed_gas) r.(deactivated) r.(balance_updates)
      r.(liquidity_baking_escape_ema) r.(implicit_operations_results).
  Definition with_nonce_hash nonce_hash (r : record) :=
    Build r.(proposer) r.(baker) r.(level_info) r.(voting_period_info)
      nonce_hash r.(consumed_gas) r.(deactivated) r.(balance_updates)
      r.(liquidity_baking_escape_ema) r.(implicit_operations_results).
  Definition with_consumed_gas consumed_gas (r : record) :=
    Build r.(proposer) r.(baker) r.(level_info) r.(voting_period_info)
      r.(nonce_hash) consumed_gas r.(deactivated) r.(balance_updates)
      r.(liquidity_baking_escape_ema) r.(implicit_operations_results).
  Definition with_deactivated deactivated (r : record) :=
    Build r.(proposer) r.(baker) r.(level_info) r.(voting_period_info)
      r.(nonce_hash) r.(consumed_gas) deactivated r.(balance_updates)
      r.(liquidity_baking_escape_ema) r.(implicit_operations_results).
  Definition with_balance_updates balance_updates (r : record) :=
    Build r.(proposer) r.(baker) r.(level_info) r.(voting_period_info)
      r.(nonce_hash) r.(consumed_gas) r.(deactivated) balance_updates
      r.(liquidity_baking_escape_ema) r.(implicit_operations_results).
  Definition with_liquidity_baking_escape_ema liquidity_baking_escape_ema
    (r : record) :=
    Build r.(proposer) r.(baker) r.(level_info) r.(voting_period_info)
      r.(nonce_hash) r.(consumed_gas) r.(deactivated) r.(balance_updates)
      liquidity_baking_escape_ema r.(implicit_operations_results).
  Definition with_implicit_operations_results implicit_operations_results
    (r : record) :=
    Build r.(proposer) r.(baker) r.(level_info) r.(voting_period_info)
      r.(nonce_hash) r.(consumed_gas) r.(deactivated) r.(balance_updates)
      r.(liquidity_baking_escape_ema) implicit_operations_results.
End block_metadata.
Definition block_metadata := block_metadata.record.

Definition block_metadata_encoding : Data_encoding.encoding block_metadata :=
  (let arg := Data_encoding.def "block_header.alpha.metadata" in
  fun (eta : Data_encoding.encoding block_metadata) => arg None None eta)
    (Data_encoding.conv
      (fun (function_parameter : block_metadata) =>
        let '{|
          block_metadata.proposer := proposer;
            block_metadata.baker := baker;
            block_metadata.level_info := level_info;
            block_metadata.voting_period_info := voting_period_info;
            block_metadata.nonce_hash := nonce_hash;
            block_metadata.consumed_gas := consumed_gas;
            block_metadata.deactivated := deactivated;
            block_metadata.balance_updates := balance_updates;
            block_metadata.liquidity_baking_escape_ema :=
              liquidity_baking_escape_ema;
            block_metadata.implicit_operations_results :=
              implicit_operations_results
            |} := function_parameter in
        (proposer, baker, level_info, voting_period_info, nonce_hash,
          consumed_gas, deactivated, balance_updates,
          liquidity_baking_escape_ema, implicit_operations_results))
      (fun (function_parameter :
        Signature.public_key_hash * Signature.public_key_hash *
          Alpha_context.Level.t * Alpha_context.Voting_period.info *
          option Nonce_hash.t * Alpha_context.Gas.Arith.fp *
          list Signature.public_key_hash * Alpha_context.Receipt.balance_updates
          * Alpha_context.Liquidity_baking.escape_ema *
          list packed_successful_manager_operation_result) =>
        let
          '(proposer, baker, level_info, voting_period_info, nonce_hash,
            consumed_gas, deactivated, balance_updates,
            liquidity_baking_escape_ema, implicit_operations_results) :=
          function_parameter in
        {| block_metadata.proposer := proposer; block_metadata.baker := baker;
          block_metadata.level_info := level_info;
          block_metadata.voting_period_info := voting_period_info;
          block_metadata.nonce_hash := nonce_hash;
          block_metadata.consumed_gas := consumed_gas;
          block_metadata.deactivated := deactivated;
          block_metadata.balance_updates := balance_updates;
          block_metadata.liquidity_baking_escape_ema :=
            liquidity_baking_escape_ema;
          block_metadata.implicit_operations_results :=
            implicit_operations_results |}) None
      (Data_encoding.obj10
        (Data_encoding.req None None "proposer"
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
        (Data_encoding.req None None "baker"
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
        (Data_encoding.req None None "level_info" Alpha_context.Level.encoding)
        (Data_encoding.req None None "voting_period_info"
          Alpha_context.Voting_period.info_encoding)
        (Data_encoding.req None None "nonce_hash"
          (Data_encoding.option_value Nonce_hash.encoding))
        (Data_encoding.req None None "consumed_gas"
          Alpha_context.Gas.Arith.n_fp_encoding)
        (Data_encoding.req None None "deactivated"
          (Data_encoding.list_value None
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
        (Data_encoding.req None None "balance_updates"
          Alpha_context.Receipt.balance_updates_encoding)
        (Data_encoding.req None None "liquidity_baking_escape_ema"
          Data_encoding.int32_value)
        (Data_encoding.req None None "implicit_operations_results"
          (Data_encoding.list_value None
            successful_manager_operation_result_encoding)))).

Module precheck_result.
  Record record : Set := Build {
    consumed_gas : Alpha_context.Gas.Arith.fp;
    balance_updates : Alpha_context.Receipt.balance_updates }.
  Definition with_consumed_gas consumed_gas (r : record) :=
    Build consumed_gas r.(balance_updates).
  Definition with_balance_updates balance_updates (r : record) :=
    Build r.(consumed_gas) balance_updates.
End precheck_result.
Definition precheck_result := precheck_result.record.

Module prechecked_contents.
  Record record : Set := Build {
    contents : Alpha_context.contents;
    result : precheck_result }.
  Definition with_contents contents (r : record) :=
    Build contents r.(result).
  Definition with_result result (r : record) :=
    Build r.(contents) result.
End prechecked_contents.
Definition prechecked_contents := prechecked_contents.record.

Inductive prechecked_contents_list : Set :=
| PrecheckedSingle : prechecked_contents -> prechecked_contents_list
| PrecheckedCons :
  prechecked_contents -> prechecked_contents_list -> prechecked_contents_list.
