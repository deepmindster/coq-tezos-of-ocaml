Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Tez_repr.

Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Lemma encoding_is_valid : Data_encoding.Valid.t (fun _ => True) Tez_repr.encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.
