Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Proofs.Tez_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Blinded_public_key_hash.

Require TezosOfOCaml.Proto_alpha.Commitment_repr.

Lemma encoding_is_valid : 
  Data_encoding.Valid.t (fun _ => True) Commitment_repr.encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
