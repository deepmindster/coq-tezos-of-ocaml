Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Require Import Lia.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Script_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Pervasives.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Micheline.
Require TezosOfOCaml.Proto_alpha.Proofs.Utils.
Require TezosOfOCaml.Proto_alpha.Proofs.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Michelson_v1_primitives.

Lemma expr_encoding_is_valid : Data_encoding.Valid.t (fun _ => True) Script_repr.expr_encoding.
  apply Micheline.canonical_encoding_is_valid.
  apply Michelson_v1_primitives.prim_encoding_is_valid.
Qed.
#[global] Hint Resolve expr_encoding_is_valid : Data_encoding_db.

Lemma location_encoding_is_valid : Data_encoding.Valid.t (fun _ => True) Script_repr.location_encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve expr_encoding_is_valid : Data_encoding_db.

Lemma lazy_expr_encoding_is_valid : Data_encoding.Valid.t (fun _ => True) Script_repr.lazy_expr_encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve lazy_expr_encoding_is_valid : Data_encoding_db.

Lemma encoding_is_valid : Data_encoding.Valid.t (fun _ => True) Script_repr.encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

Lemma deserialization_cost_estimated_from_bytes_is_valid length
  : Gas_limit_repr.Cost.Valid.t
    (Script_repr.deserialization_cost_estimated_from_bytes length).
  unfold Script_repr.deserialization_cost_estimated_from_bytes.
  apply Saturation_repr.mul_is_valid.
  reflexivity.
  apply Saturation_repr.safe_int_is_valid.
Qed.

Lemma force_decode_cost_is_valid {a : Set} (lexpr : Data_encoding.lazy_t a)
  : Gas_limit_repr.Cost.Valid.t (Script_repr.force_decode_cost lexpr).
  unfold Script_repr.force_decode_cost, Data_encoding.apply_lazy.
  destruct lexpr; try reflexivity.
  apply deserialization_cost_estimated_from_bytes_is_valid.
Qed.

(** A simpler implementation of [Script_repr.micheline_nodes]. *)
Module Reference_micheline_nodes.
  Reserved Notation "'micheline_nodes_list".

  Fixpoint micheline_nodes {l} (node : Script_repr.michelson_node l) : int :=
    let micheline_nodes_list := 'micheline_nodes_list l in
    match node with
    | Micheline.Int _ _ | Micheline.String _ _ | Micheline.Bytes _ _ => 1
    | Micheline.Prim _ _ subterms _ | Micheline.Seq _ subterms =>
      micheline_nodes_list subterms +i 1
    end

  where "'micheline_nodes_list" :=
    (fun (l : Set) => fix micheline_nodes_list
      (subterms : list (Script_repr.michelson_node l)) {struct subterms} : int :=
      match subterms with
      | [] => 0
      | cons n nodes => micheline_nodes n +i micheline_nodes_list nodes
      end).

  Definition micheline_nodes_list {l : Set} := 'micheline_nodes_list l.
End Reference_micheline_nodes.

(** How to unfold the continuation of [Script_repr.micheline_fold_aux]. *)
Fixpoint micheline_fold_aux_continuation {A l : Set}
  (node : Script_repr.michelson_node l)
  (f : A -> Script_repr.michelson_node l -> A) {struct node}
  : forall (acc_value : A) {B : Set} (k : A -> B),
    Script_repr.micheline_fold_aux node f acc_value k =
    k (Script_repr.micheline_fold_aux node f acc_value id).
  assert (micheline_fold_nodes_continuation :
    forall (subterms : list (Script_repr.michelson_node l)) (acc_value : A)
      {B : Set} (k : A -> B),
    Script_repr.micheline_fold_nodes subterms f acc_value k =
    k (Script_repr.micheline_fold_nodes subterms f acc_value id)
  ).
  { induction subterms; intros; try reflexivity; simpl.
    match goal with
    | [H : _ |- _] => rewrite H
    end.
    rewrite micheline_fold_aux_continuation.
    f_equal.
    match goal with
    | [H : _ |- _ = Script_repr.micheline_fold_nodes _ _ _ ?k] =>
      now rewrite (H _ _ k)
    end.
  }
  { intros.
    destruct node; try reflexivity.
    all:
      simpl;
      fold (@Script_repr.micheline_fold_nodes l A B);
      now rewrite micheline_fold_nodes_continuation.
  }
Qed.

(** How to unfold the continuation of [Script_repr.micheline_fold_nodes]. *)
Fixpoint micheline_fold_nodes_continuation {A B l : Set}
  (nodes : list (Script_repr.michelson_node l))
  (f : A -> Script_repr.michelson_node l -> A)
  (acc_value : A) (k : A -> B) {struct nodes}
  : Script_repr.micheline_fold_nodes nodes f acc_value k =
    k (Script_repr.micheline_fold_nodes nodes f acc_value id).
  destruct nodes; try reflexivity; simpl.
  rewrite micheline_fold_nodes_continuation.
  rewrite micheline_fold_aux_continuation.
  f_equal.
  match goal with
  | [|- _ = Script_repr.micheline_fold_nodes _ _ _ ?k'] =>
    now rewrite micheline_fold_nodes_continuation with (k := k')
  end.
Qed.

Fixpoint micheline_nodes_like_reference_aux {l}
  (node : Script_repr.michelson_node l) acc
  : Pervasives.Int.Valid.t acc ->
    Script_repr.micheline_fold_aux node (fun acc '_ => acc +i 1) acc id =
    Reference_micheline_nodes.micheline_nodes node +i acc.
  assert (micheline_nodes_list_like_reference :
    forall (subterms : list (Script_repr.michelson_node l)) (acc' : int),
      Pervasives.Int.Valid.t acc' ->
      Script_repr.micheline_fold_nodes subterms (fun n '_ => n +i 1) acc' id =
      Reference_micheline_nodes.micheline_nodes_list subterms +i acc'
  ).
  { unfold Pervasives.Int.Valid.t, Pervasives.max_int, Pervasives.min_int.
    induction subterms as [|x xs IHl]; simpl; intros.
    { unfold "+i", normalize, Pervasives.two_pow_63, two_pow_62.
      unfold id.
      lia. }
    { rewrite micheline_fold_nodes_continuation.
      rewrite micheline_fold_aux_continuation.
      unfold id; simpl.
      match goal with
      | [H : _ |- _] => rewrite H
      end.
      rewrite micheline_nodes_like_reference_aux.
      { Utils.tezos_z_auto. }
      { Utils.tezos_z_auto. }
      { apply H. } } }
  { 
    unfold Pervasives.Int.Valid.t, Pervasives.max_int, Pervasives.min_int in micheline_nodes_list_like_reference;
    autounfold with tezos_z in micheline_nodes_list_like_reference;
    unfold Script_repr.micheline_fold_aux, "+i";
    assert (Hid : forall {A : Set} {x : A}, id x = x) by reflexivity;
    destruct node; autounfold with tezos_z; try rewrite Hid.

    all: 
      try rewrite micheline_nodes_list_like_reference;
      try unfold Reference_micheline_nodes.micheline_nodes_list,
        Reference_micheline_nodes.micheline_nodes,
        Script_repr.michelson_node, Michelson_v1_primitives; 
      try Utils.tezos_z_auto.

    all:
      unfold Reference_micheline_nodes.micheline_nodes_list,
        Reference_micheline_nodes.micheline_nodes,
        Script_repr.michelson_node;
      Utils.tezos_z_auto.
   }
Qed.

Lemma reference_micheline_nodes_micheline_nodes_is_valid : forall (node : Script_repr.node),
  Pervasives.Int.Valid.t (Reference_micheline_nodes.micheline_nodes node). 
  unfold Script_repr.node, Script_repr.michelson_node,
    Script_repr.location, Reference_micheline_nodes.micheline_nodes.
  destruct node; Utils.tezos_z_auto.
Qed.

(** [Script_repr.micheline_nodes] behaves like its reference implementation. *)
Lemma micheline_nodes_like_reference (node : Script_repr.node)
  : Script_repr.micheline_nodes node =
    Reference_micheline_nodes.micheline_nodes node.

  unfold Script_repr.micheline_nodes, Script_repr.fold.
  rewrite micheline_nodes_like_reference_aux.
  rewrite Pervasives.Int.Valid.int_plus_0_r_eq.
  { reflexivity. }
  { apply reference_micheline_nodes_micheline_nodes_is_valid. }
  { Utils.tezos_z_auto. }
Qed.
