Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Require Import Lia.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Saturation_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Option.

(** We put the saturation operations which can be trivially converted to their
    Z equivalent into the [tezos_z] hint database. *)
Module Unfold.
  Import Saturation_repr.

  Global Hint Unfold may_saturate_value to_int op_lt op_lteq op_gt op_gteq op_eq
    equal op_ltgt compare to_z zero one : tezos_z.
End Unfold.

Module Valid.
  Definition check (x : Saturation_repr.t) : bool :=
    Saturation_repr.op_gteq x 0 &&
      Saturation_repr.op_lteq x Saturation_repr.saturated.

  Definition t (x : Saturation_repr.t) : Prop :=
    check x = true.
End Valid.

Lemma max_is_valid (x y : Saturation_repr.t)
  : Valid.t x -> Valid.t y -> Valid.t (Saturation_repr.max x y).
  intros.
  unfold Saturation_repr.max.
  now destruct (Saturation_repr.op_gteq _ _).
Qed.

Lemma max_eq x y : Saturation_repr.max x y = Z.max x y.
  unfold Saturation_repr.max.
  autounfold with tezos_z; cbn; autounfold with tezos_z.
  match goal with
  | [|- context[if ?e then _ else _]] => destruct e eqn:H
  end; lia.
Qed.

Lemma min_is_valid (x y : Saturation_repr.t)
  : Valid.t x -> Valid.t y -> Valid.t (Saturation_repr.min x y).
  intros.
  unfold Saturation_repr.min.
  now destruct (Saturation_repr.op_gteq _ _).
Qed.

Lemma min_eq x y : Saturation_repr.min x y = Z.min x y.
  unfold Saturation_repr.min.
  autounfold with tezos_z; cbn; autounfold with tezos_z.
  match goal with
  | [|- context[if ?e then _ else _]] => destruct e eqn:H
  end; lia.
Qed.

Lemma saturated_is_valid : Valid.t Saturation_repr.saturated.
  reflexivity.
Qed.

Lemma of_int_opt_is_valid (i : int)
  : Option.post_when_success (Saturation_repr.of_int_opt i) Valid.t.
  unfold Saturation_repr.of_int_opt, Valid.t, Valid.check, "&&".
  autounfold with tezos_z.
  destruct (_ && _)%bool eqn:H_eq; simpl; trivial.
  cbn in *; autounfold with tezos_z in *.
  lia.
Qed.

Lemma safe_int_is_valid x : Valid.t (Saturation_repr.safe_int x).
  unfold Saturation_repr.safe_int, Saturation_repr.saturate_if_undef.
  assert (H := of_int_opt_is_valid x).
  destruct (Saturation_repr.of_int_opt _); trivial.
  exact saturated_is_valid.
Qed.

Axiom small_enough_eq :
  forall x, Saturation_repr.small_enough x = (x <=i 2147483647).

Axiom mul_is_valid : forall (x y : Saturation_repr.t),
  Valid.t x -> Valid.t y -> Valid.t (Saturation_repr.mul x y).
