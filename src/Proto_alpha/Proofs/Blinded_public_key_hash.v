Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Blinded_public_key_hash.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Blake2B.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Proofs.Storage_description.

Lemma H_is_valid : S.HASH.Valid.t (fun _ => True) Blinded_public_key_hash.H.
  apply Blake2B.Make_is_valid.
Qed.
#[global] Hint Extern 1 => apply H_is_valid : Data_encoding_db.

Lemma Index_is_valid
  : Storage_description.INDEX.Valid.t Blinded_public_key_hash.Index.
  constructor;
    try (apply Path_encoding.Make_hex_is_valid; constructor);
    apply H_is_valid.
Qed.

Module Activation_code.
  Module Valid.
    Definition t (activation_code : Blinded_public_key_hash.activation_code)
      : Prop :=
      Bytes.length activation_code =
      Blinded_public_key_hash.activation_code_size.
  End Valid.

  Lemma encoding_is_valid
    : Data_encoding.Valid.t
      Valid.t Blinded_public_key_hash.activation_code_encoding.
    apply Data_encoding.Valid.Fixed.bytes_value.
  Qed.
End Activation_code.
