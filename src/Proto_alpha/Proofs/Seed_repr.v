Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Seed_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Module Nonce.
  Module Valid.
    Definition t (nonce : Seed_repr.nonce) : Prop :=
      Bytes.length nonce = Constants_repr.nonce_length.
  End Valid.

  Lemma encoding_is_valid
    : Data_encoding.Valid.t Valid.t Seed_repr.nonce_encoding.
    apply Data_encoding.Valid.Fixed.bytes_value.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.
End Nonce.

Module State_hash.
  Module Valid.
    Definition t (s : State_hash.t) : Prop :=
      Bytes.length (State_hash.to_bytes s) = Nonce_hash.size_value /\
      State_hash.of_bytes_exn (State_hash.to_bytes s) = s.
  End Valid.
End State_hash.

Lemma seed_state_hash_encoding_is_valid: Data_encoding.Valid.t State_hash.Valid.t Seed_repr.state_hash_encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve seed_state_hash_encoding_is_valid : Data_encoding_db.

Lemma seeds_encoding_is_valid : 
    Data_encoding.Valid.t 
    (fun x => State_hash.Valid.t (let 'Seed_repr.B b_value := x in b_value))
    Seed_repr.seed_encoding.
  Data_encoding.Valid.data_encoding_auto.
  destruct x.
  intro. split.
  { assumption. }
  { reflexivity. }
Qed.
#[global] Hint Resolve seeds_encoding_is_valid : Data_encoding_db.
