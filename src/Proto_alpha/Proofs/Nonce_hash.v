Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Nonce_hash.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Blake2B.
Require TezosOfOCaml.Proto_alpha.Proofs.Path_encoding.

Lemma H_is_valid : S.HASH.Valid.t (fun _ => True) Nonce_hash.H.
  apply Blake2B.Make_is_valid.
Qed.
#[global] Hint Extern 1 => apply H_is_valid : Data_encoding_db.

Lemma Path_encoding_Make_hex_include_is_valid
  : Path_encoding.S.Valid.t Nonce_hash.Path_encoding_Make_hex_include.
  apply Path_encoding.Make_hex_is_valid.
  constructor; apply H_is_valid.
Qed.
#[global] Hint Resolve Path_encoding_Make_hex_include_is_valid : Data_encoding_db.
