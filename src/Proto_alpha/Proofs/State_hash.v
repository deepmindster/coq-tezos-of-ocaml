Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.State_hash.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Blake2B.
Require TezosOfOCaml.Proto_alpha.Proofs.Path_encoding.

Lemma H_is_valid : S.HASH.Valid.t (fun _ => True) State_hash.H.
  apply Blake2B.Make_is_valid.
Qed.

Lemma Path_encoding_Make_hex_include_is_valid
  : Path_encoding.S.Valid.t State_hash.Path_encoding_Make_hex_include.
  apply Path_encoding.Make_hex_is_valid.
  constructor; apply H_is_valid.
Qed.
