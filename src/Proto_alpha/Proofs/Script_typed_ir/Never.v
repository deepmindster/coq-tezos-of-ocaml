Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Compare.

Definition compare (x y : Script_typed_ir.never) : int :=
  0.

Lemma compare_is_valid : Compare.Valid.t (Compare.wrap_compare compare).
  easy.
Qed.
