Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.

Require TezosOfOCaml.Proto_alpha.Michocoq.syntax_type.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_typed_ir.Comparable_ty.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_typed_ir.Ty_metadata.

Module Family.
  Inductive t : Set :=
  | Unit_t : t
  | Int_t : t
  | Nat_t : t
  | Signature_t : t
  | String_t : t
  | Bytes_t : t
  | Mutez_t : t
  | Key_hash_t : t
  | Key_t : t
  | Timestamp_t : t
  | Address_t : t
  | Bool_t : t
  | Pair_t : t -> t -> t
  | Union_t : t -> t -> t
  | Lambda_t : t -> t -> t
  | Option_t : t -> t
  | List_t : t -> t
  | Set_t : Comparable_ty.Family.t -> t
  | Map_t : Comparable_ty.Family.t -> t -> t
  | Big_map_t : Comparable_ty.Family.t -> t -> t
  | Contract_t : t -> t
  | Sapling_transaction_t : t
  | Sapling_state_t : t
  | Operation_t : t
  | Chain_id_t : t
  | Never_t : t
  | Bls12_381_g1_t : t
  | Bls12_381_g2_t : t
  | Bls12_381_fr_t : t
  | Ticket_t : Comparable_ty.Family.t -> t
  | Chest_key_t : t
  | Chest_t : t.
End Family.

(** Set the metadata of a type to a default value. *)
Fixpoint get_canonical (ty : Script_typed_ir.ty) : Script_typed_ir.ty :=
  match ty with
  | Script_typed_ir.Unit_t _ =>
    Script_typed_ir.Unit_t Ty_metadata.default
  | Script_typed_ir.Int_t _ =>
    Script_typed_ir.Int_t Ty_metadata.default
  | Script_typed_ir.Nat_t _ =>
    Script_typed_ir.Nat_t Ty_metadata.default
  | Script_typed_ir.Signature_t _ =>
    Script_typed_ir.Signature_t Ty_metadata.default
  | Script_typed_ir.String_t _ =>
    Script_typed_ir.String_t Ty_metadata.default
  | Script_typed_ir.Bytes_t _ =>
    Script_typed_ir.Bytes_t Ty_metadata.default
  | Script_typed_ir.Mutez_t _ =>
    Script_typed_ir.Mutez_t Ty_metadata.default
  | Script_typed_ir.Key_hash_t _ =>
    Script_typed_ir.Key_hash_t Ty_metadata.default
  | Script_typed_ir.Key_t _ =>
    Script_typed_ir.Key_t Ty_metadata.default
  | Script_typed_ir.Timestamp_t _ =>
    Script_typed_ir.Timestamp_t Ty_metadata.default
  | Script_typed_ir.Address_t _ =>
    Script_typed_ir.Address_t Ty_metadata.default
  | Script_typed_ir.Bool_t _ =>
    Script_typed_ir.Bool_t Ty_metadata.default
  | Script_typed_ir.Pair_t (ty1, _, _) (ty2, _, _) _ =>
    let ty1 := get_canonical ty1 in
    let ty2 := get_canonical ty2 in
    Script_typed_ir.Pair_t
      (ty1, None, None) (ty2, None, None) Ty_metadata.default
  | Script_typed_ir.Union_t (ty1, _) (ty2, _) _ =>
    let ty1 := get_canonical ty1 in
    let ty2 := get_canonical ty2 in
    Script_typed_ir.Union_t
      (ty1, None) (ty2, None) Ty_metadata.default
  | Script_typed_ir.Lambda_t ty1 ty2 _ =>
    let ty1 := get_canonical ty1 in
    let ty2 := get_canonical ty2 in
    Script_typed_ir.Lambda_t ty1 ty2 Ty_metadata.default
  | Script_typed_ir.Option_t ty _ =>
    let ty := get_canonical ty in
    Script_typed_ir.Option_t ty Ty_metadata.default
  | Script_typed_ir.List_t ty _ =>
    let ty := get_canonical ty in
    Script_typed_ir.List_t ty Ty_metadata.default
  | Script_typed_ir.Set_t ty _ =>
    let ty := Comparable_ty.get_canonical ty in
    Script_typed_ir.Set_t ty Ty_metadata.default
  | Script_typed_ir.Map_t ty1 ty2 _ =>
    let ty1 := Comparable_ty.get_canonical ty1 in
    let ty2 := get_canonical ty2 in
    Script_typed_ir.Map_t ty1 ty2 Ty_metadata.default
  | Script_typed_ir.Big_map_t ty1 ty2 _ =>
    let ty1 := Comparable_ty.get_canonical ty1 in
    let ty2 := get_canonical ty2 in
    Script_typed_ir.Big_map_t ty1 ty2 Ty_metadata.default
  | Script_typed_ir.Contract_t ty _ =>
    let ty := get_canonical ty in
    Script_typed_ir.Contract_t ty Ty_metadata.default
  | Script_typed_ir.Sapling_transaction_t memo_size _ =>
    Script_typed_ir.Sapling_transaction_t memo_size Ty_metadata.default
  | Script_typed_ir.Sapling_state_t memo_size _ =>
    Script_typed_ir.Sapling_state_t memo_size Ty_metadata.default
  | Script_typed_ir.Operation_t _ =>
    Script_typed_ir.Operation_t Ty_metadata.default
  | Script_typed_ir.Chain_id_t _ =>
    Script_typed_ir.Chain_id_t Ty_metadata.default
  | Script_typed_ir.Never_t _ =>
    Script_typed_ir.Never_t Ty_metadata.default
  | Script_typed_ir.Bls12_381_g1_t _ =>
    Script_typed_ir.Bls12_381_g1_t Ty_metadata.default
  | Script_typed_ir.Bls12_381_g2_t _ =>
    Script_typed_ir.Bls12_381_g2_t Ty_metadata.default
  | Script_typed_ir.Bls12_381_fr_t _ =>
    Script_typed_ir.Bls12_381_fr_t Ty_metadata.default
  | Script_typed_ir.Ticket_t ty _ =>
    let ty := Comparable_ty.get_canonical ty in
    Script_typed_ir.Ticket_t ty Ty_metadata.default
  | Script_typed_ir.Chest_key_t _ =>
    Script_typed_ir.Chest_key_t Ty_metadata.default
  | Script_typed_ir.Chest_t _ =>
    Script_typed_ir.Chest_t Ty_metadata.default
  end.

(** Getting the canonical value of a type is involutive. *)
Fixpoint get_canonical_is_involutive ty
  : get_canonical (get_canonical ty) = get_canonical ty.
  destruct ty; try reflexivity; simpl;
    repeat match goal with
    | [x : _ * _ |- _] => destruct x
    end; simpl;
    repeat rewrite Comparable_ty.get_canonical_is_involutive;
    repeat rewrite get_canonical_is_involutive;
    reflexivity.
Qed.

(** Convert a type to Mi-Cho-Coq. *)
Fixpoint to_Michocoq (ty : Script_typed_ir.ty) : option syntax_type.type :=
  match ty with
  | Script_typed_ir.Unit_t _ => Some syntax_type.unit
  | Script_typed_ir.Int_t _ =>
    Some (syntax_type.Comparable_type syntax_type.int)
  | Script_typed_ir.Nat_t _ =>
    Some (syntax_type.Comparable_type syntax_type.nat)
  | Script_typed_ir.Signature_t _ => Some syntax_type.signature
  | Script_typed_ir.String_t _ =>
    Some (syntax_type.Comparable_type syntax_type.string)
  | Script_typed_ir.Bytes_t _ =>
    Some (syntax_type.Comparable_type syntax_type.bytes)
  | Script_typed_ir.Mutez_t _ =>
    Some (syntax_type.Comparable_type syntax_type.mutez)
  | Script_typed_ir.Key_hash_t _ =>
    Some (syntax_type.Comparable_type syntax_type.key_hash)
  | Script_typed_ir.Key_t _ => Some syntax_type.key
  | Script_typed_ir.Timestamp_t _ =>
    Some (syntax_type.Comparable_type syntax_type.timestamp)
  | Script_typed_ir.Address_t _ =>
    Some (syntax_type.Comparable_type syntax_type.address)
  | Script_typed_ir.Bool_t _ =>
    Some (syntax_type.Comparable_type syntax_type.bool)
  | Script_typed_ir.Pair_t (ty1, _, _) (ty2, _, _) _ =>
    let* ty1 := to_Michocoq ty1 in
    let* ty2 := to_Michocoq ty2 in
    Some (syntax_type.pair ty1 ty2)
  | Script_typed_ir.Union_t (ty1, _) (ty2, _) _ =>
    let* ty1 := to_Michocoq ty1 in
    let* ty2 := to_Michocoq ty2 in
    Some (syntax_type.or ty1 ty2)
  | Script_typed_ir.Lambda_t ty1 ty2 _ =>
    let* ty1 := to_Michocoq ty1 in
    let* ty2 := to_Michocoq ty2 in
    Some (syntax_type.lambda ty1 ty2)
  | Script_typed_ir.Option_t ty _ =>
    let* ty := to_Michocoq ty in
    Some (syntax_type.option ty)
  | Script_typed_ir.List_t ty _ =>
    let* ty := to_Michocoq ty in
    Some (syntax_type.list ty)
  | Script_typed_ir.Set_t ty _ =>
    let* ty := Comparable_ty.to_Michocoq ty in
    Some (syntax_type.set ty)
  | Script_typed_ir.Map_t ty1 ty2 _ =>
    let* ty1 := Comparable_ty.to_Michocoq ty1 in
    let* ty2 := to_Michocoq ty2 in
    Some (syntax_type.map ty1 ty2)
  | Script_typed_ir.Big_map_t ty1 ty2 _ =>
    let* ty1 := Comparable_ty.to_Michocoq ty1 in
    let* ty2 := to_Michocoq ty2 in
    Some (syntax_type.big_map ty1 ty2)
  | Script_typed_ir.Contract_t ty _ =>
    let* ty := to_Michocoq ty in
    Some (syntax_type.contract ty)
  | Script_typed_ir.Sapling_transaction_t _ _ => None
  | Script_typed_ir.Sapling_state_t _ _ => None
  | Script_typed_ir.Operation_t _ => Some syntax_type.operation
  | Script_typed_ir.Chain_id_t _ => Some syntax_type.chain_id
  | Script_typed_ir.Never_t _ => None
  | Script_typed_ir.Bls12_381_g1_t _ => None
  | Script_typed_ir.Bls12_381_g2_t _ => None
  | Script_typed_ir.Bls12_381_fr_t _ => None
  | Script_typed_ir.Ticket_t _ _ => None
  | Script_typed_ir.Chest_key_t _ => None
  | Script_typed_ir.Chest_t _ => None
  end.

(** The [to_Michocoq] function does not depend on the metadata. *)
Fixpoint to_Michocoq_canonical_eq ty
  : to_Michocoq (get_canonical ty) = to_Michocoq ty.
  destruct ty; try reflexivity; simpl;
    repeat match goal with
    | [x : _ * _ |- _] => destruct x
    end; simpl;
    repeat rewrite Comparable_ty.to_Michocoq_canonical_eq;
    repeat rewrite to_Michocoq_canonical_eq;
    reflexivity.
Qed.

(** Import a type from Mi-Cho-Coq. *)
Fixpoint of_Michocoq (ty : syntax_type.type) : Script_typed_ir.ty :=
  match ty with
  | syntax_type.Comparable_type ty =>
    match ty with
    | syntax_type.string => Script_typed_ir.String_t Ty_metadata.default
    | syntax_type.nat => Script_typed_ir.Nat_t Ty_metadata.default
    | syntax_type.int => Script_typed_ir.Int_t Ty_metadata.default
    | syntax_type.bytes => Script_typed_ir.Bytes_t Ty_metadata.default
    | syntax_type.bool => Script_typed_ir.Bool_t Ty_metadata.default
    | syntax_type.mutez => Script_typed_ir.Mutez_t Ty_metadata.default
    | syntax_type.address => Script_typed_ir.Address_t Ty_metadata.default
    | syntax_type.key_hash => Script_typed_ir.Key_hash_t Ty_metadata.default
    | syntax_type.timestamp => Script_typed_ir.Timestamp_t Ty_metadata.default
    end
  | syntax_type.key => Script_typed_ir.Key_t Ty_metadata.default
  | syntax_type.unit => Script_typed_ir.Unit_t Ty_metadata.default
  | syntax_type.signature => Script_typed_ir.Signature_t Ty_metadata.default
  | syntax_type.option ty =>
    let ty := of_Michocoq ty in
    Script_typed_ir.Option_t ty Ty_metadata.default
  | syntax_type.list ty =>
    let ty := of_Michocoq ty in
    Script_typed_ir.List_t ty Ty_metadata.default
  | syntax_type.set ty =>
    let ty := Comparable_ty.of_Michocoq ty in
    Script_typed_ir.Set_t ty Ty_metadata.default
  | syntax_type.contract ty =>
    let ty := of_Michocoq ty in
    Script_typed_ir.Contract_t ty Ty_metadata.default
  | syntax_type.operation => Script_typed_ir.Operation_t Ty_metadata.default
  | syntax_type.pair ty1 ty2 =>
    let ty1 := of_Michocoq ty1 in
    let ty2 := of_Michocoq ty2 in
    Script_typed_ir.Pair_t
      (ty1, None, None) (ty2, None, None) Ty_metadata.default
  | syntax_type.or ty1 ty2 =>
    let ty1 := of_Michocoq ty1 in
    let ty2 := of_Michocoq ty2 in
    Script_typed_ir.Union_t (ty1, None) (ty2, None) Ty_metadata.default
  | syntax_type.lambda ty1 ty2 =>
    let ty1 := of_Michocoq ty1 in
    let ty2 := of_Michocoq ty2 in
    Script_typed_ir.Lambda_t ty1 ty2 Ty_metadata.default
  | syntax_type.map ty1 ty2 =>
    let ty1 := Comparable_ty.of_Michocoq ty1 in
    let ty2 := of_Michocoq ty2 in
    Script_typed_ir.Map_t ty1 ty2 Ty_metadata.default
  | syntax_type.big_map ty1 ty2 =>
    let ty1 := Comparable_ty.of_Michocoq ty1 in
    let ty2 := of_Michocoq ty2 in
    Script_typed_ir.Big_map_t ty1 ty2 Ty_metadata.default
  | syntax_type.chain_id => Script_typed_ir.Chain_id_t Ty_metadata.default
  end.

(** The function [of_Michocoq] produces terms which are already in a
    canonical form. *)
Fixpoint of_Michocoq_is_canonical ty
  : of_Michocoq ty = get_canonical (of_Michocoq ty).
  destruct ty; simpl;
    repeat rewrite <- Comparable_ty.of_Michocoq_is_canonical;
    repeat rewrite <- of_Michocoq_is_canonical;
    try reflexivity.
  destruct_all syntax_type.simple_comparable_type; reflexivity.
Qed.

(** [to_Michocoq] is the inverse of [of_Michocoq]. *)
Fixpoint to_Michocoq_of_Michocoq ty
  : to_Michocoq (of_Michocoq ty) = Some ty.
  destruct ty; simpl;
    repeat rewrite Comparable_ty.to_Michocoq_of_Michocoq;
    repeat rewrite to_Michocoq_of_Michocoq;
    try reflexivity.
  destruct_all syntax_type.simple_comparable_type; reflexivity.
Qed.

(** [of_Michocoq] is the inverse of [to_Michocoq]. *)
Fixpoint of_Michocoq_to_Michocoq ty
  : match to_Michocoq ty with
    | Some ty' => of_Michocoq ty' = get_canonical ty
    | None => True
    end.
  destruct ty; simpl; trivial;
    repeat match goal with
    | [x : _ * _ |- _] => destruct x
    end; simpl;
    repeat match goal with
    | [ty : _ |- _] =>
      let H := fresh "H" in
      (
        (assert (H := Comparable_ty.of_Michocoq_to_Michocoq ty);
        destruct (Comparable_ty.to_Michocoq ty)) ||
        (assert (H := of_Michocoq_to_Michocoq ty);
        destruct (to_Michocoq ty))
      );
      revert ty H;
      simpl; trivial
    end;
    congruence.
Qed.
