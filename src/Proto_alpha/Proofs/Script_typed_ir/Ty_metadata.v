Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.

(** A default empty metadata we use to make a canonical form of a type. *)
Definition default : Script_typed_ir.ty_metadata :=
  {|
    Script_typed_ir.ty_metadata.annot := None;
    Script_typed_ir.ty_metadata.size := 0;
  |}.
