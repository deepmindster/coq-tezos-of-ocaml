Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.

Require Export TezosOfOCaml.Proto_alpha.Proofs.Script_typed_ir.Ty_metadata.
Require Export TezosOfOCaml.Proto_alpha.Proofs.Script_typed_ir.Comparable_ty.
Require Export TezosOfOCaml.Proto_alpha.Proofs.Script_typed_ir.Ty.

Module With_family.
  Reserved Notation "'kdescr".
  Reserved Notation "'kinfo".

  Inductive instr :
    Script_typed_ir.kinstr -> list Ty.Family.t -> list Ty.Family.t -> Set :=
  (*
    Stack
    -----
  *)
  | IDrop {a b s r f info k} :
    'kinfo info (a :: b :: s) ->
    instr k (b :: s) (r :: f) ->
    instr (Script_typed_ir.IDrop info k) (a :: b :: s) (r :: f)
  | IDup {a s r f info k} :
    'kinfo info (a :: s) ->
    instr k (a :: a :: s) (r :: f) ->
    instr (Script_typed_ir.IDup info k) (a :: s) (r :: f)
  | ISwap {a b s r f info k} :
    'kinfo info (a :: b :: s) ->
    instr k (b :: a :: s) (r :: f) ->
    instr (Script_typed_ir.ISwap info k) (a :: b :: s) (r :: f)
  | IConst {a s ty r f info} (v : bool(*Ty.Family.to_Set ty TODO*)) {k} :
    'kinfo info (a :: s) ->
    instr k (ty :: a :: s) (r :: f) ->
    instr (Script_typed_ir.IConst info v k) (a :: s) (r :: f)
  (*
    Pairs
    -----
  *)
  | ICons_pair {a b s r f info k} :
    'kinfo info (a :: b :: s) ->
    instr k (Ty.Family.Pair_t a b :: s) (r :: f) ->
    instr (Script_typed_ir.ICons_pair info k) (a :: b :: s) (r :: f)
  | ICar {a b s r f info k} :
    'kinfo info (a :: b :: s) ->
    instr k (a :: s) (r :: f) ->
    instr (Script_typed_ir.ICar info k) (Ty.Family.Pair_t a b :: s) (r :: f)
  | ICdr {a b s r f info k} :
    'kinfo info (Ty.Family.Pair_t a b :: s) ->
    instr k (b :: s) (r :: f) ->
    instr (Script_typed_ir.ICdr info k) (Ty.Family.Pair_t a b :: s) (r :: f)
  | IUnpair {a b s r f info k} :
    'kinfo info (Ty.Family.Pair_t a b :: s) ->
    instr k (a :: b :: s) (r :: f) ->
    instr (Script_typed_ir.IUnpair info k) (Ty.Family.Pair_t a b :: s) (r :: f)
  (*
    Options
    -------
  *)
  | ICons_some {v s r f info k} :
    'kinfo info (v :: s) ->
    instr k (Ty.Family.Option_t v :: s) (r :: f) ->
    instr (Script_typed_ir.ICons_some info k) (v :: s) (r :: f)
  | ICons_none {a s b r f info k} :
    'kinfo info (a :: s) ->
    instr k (Ty.Family.Option_t b :: a :: s) (r :: f) ->
    instr (Script_typed_ir.ICons_none info k) (a :: s) (r :: f)
  | IIf_none {a b s c t r f info branch_if_none branch_if_some k} :
    'kinfo info (Ty.Family.Option_t a :: b :: s) ->
    instr branch_if_none (b :: s) (c :: t) ->
    instr branch_if_some (a :: b :: s) (c :: t) ->
    instr k (c :: t) (r :: f) ->
    instr
      (Script_typed_ir.IIf_none {|
        Script_typed_ir.kinstr.IIf_none.kinfo := info;
        Script_typed_ir.kinstr.IIf_none.branch_if_none := branch_if_none;
        Script_typed_ir.kinstr.IIf_none.branch_if_some := branch_if_some;
        Script_typed_ir.kinstr.IIf_none.k := k;
      |})
      (Ty.Family.Option_t a :: b :: s) (r :: f)
  | IOpt_map {a b c s t info body k} :
    'kinfo info (Ty.Family.Option_t a :: s) ->
    instr body (a :: s) (b :: s) ->
    instr k (Ty.Family.Option_t b :: s) (c :: t) ->
    instr
      (Script_typed_ir.IOpt_map {|
        Script_typed_ir.kinstr.IOpt_map.kinfo := info;
        Script_typed_ir.kinstr.IOpt_map.body := body;
        Script_typed_ir.kinstr.IOpt_map.k := k;
      |})
      (Ty.Family.Option_t a :: s) (c :: t)
  (*
    Unions
    ------
  *)
  | ICons_left {a b s r f info k} :
    'kinfo info (a :: s) ->
    instr k (Ty.Family.Union_t a b :: s) (r :: f) ->
    instr (Script_typed_ir.ICons_left info k) (a :: s) (r :: f)
  | ICons_right {a b s r f info k} :
    'kinfo info (b :: s) ->
    instr k (Ty.Family.Union_t a b :: s) (r :: f) ->
    instr (Script_typed_ir.ICons_right info k) (b :: s) (r :: f)
  | IIf_left {a b c s t r f info branch_if_left branch_if_right k} :
    'kinfo info (Ty.Family.Union_t a b :: s) ->
    instr branch_if_left (a :: s) (c :: t) ->
    instr branch_if_right (b :: s) (c :: t) ->
    instr k (c :: t) (r :: f) ->
    instr
      (Script_typed_ir.IIf_left {|
        Script_typed_ir.kinstr.IIf_left.kinfo := info;
        Script_typed_ir.kinstr.IIf_left.branch_if_left := branch_if_left;
        Script_typed_ir.kinstr.IIf_left.branch_if_right := branch_if_right;
        Script_typed_ir.kinstr.IIf_left.k := k;
      |})
      (Ty.Family.Union_t a b :: s) (r :: f)
  (*
    Lists
    -----
  *)
  | ICons_list {a s r f info k} :
    'kinfo info (a :: Ty.Family.List_t a :: s) ->
    instr k (Ty.Family.List_t a :: s) (r :: f) ->
    instr (Script_typed_ir.ICons_list info k)
      (a :: Ty.Family.List_t a :: s) (r :: f)
  | INil {a b s r f info k} :
    'kinfo info (a :: s) ->
    instr k (Ty.Family.List_t b :: a :: s) (r :: f) ->
    instr (Script_typed_ir.INil info k) (a :: s) (r :: f)
  | IIf_cons {a b c s r f t info branch_if_cons branch_if_nil k} :
    'kinfo info (Ty.Family.List_t a :: b :: s) ->
    instr branch_if_cons (a :: Ty.Family.List_t a :: b :: s) (c :: t) ->
    instr branch_if_nil (b :: s) (c :: t) ->
    instr k (c :: t) (r :: f) ->
    instr
      (Script_typed_ir.IIf_cons {|
        Script_typed_ir.kinstr.IIf_cons.kinfo := info;
        Script_typed_ir.kinstr.IIf_cons.branch_if_cons := branch_if_cons;
        Script_typed_ir.kinstr.IIf_cons.branch_if_nil := branch_if_nil;
        Script_typed_ir.kinstr.IIf_cons.k := k;
      |})
      (Ty.Family.List_t a :: b :: s) (r :: f)
  | IList_map {a b c s r f info body k} :
    'kinfo info (Ty.Family.List_t a :: c :: s) ->
    instr body (a :: c :: s) (b :: c :: s) ->
    instr k (Ty.Family.List_t b :: c :: s) (r :: f) ->
    instr (Script_typed_ir.IList_map info body k)
        (Ty.Family.List_t a :: c :: s) (r :: f)
  | IList_iter {a b s r f info body k} :
    'kinfo info (Ty.Family.List_t a :: b :: s) ->
    instr body (a :: b :: s) (b :: s) ->
    instr k (b :: s) (r :: f) ->
    instr (Script_typed_ir.IList_iter info body k)
      (Ty.Family.List_t a :: b :: s) (r :: f)
  | IList_size {a s r f info k} :
    'kinfo info (Ty.Family.List_t a :: s) ->
    instr k (Ty.Family.Nat_t :: s) (r :: f) ->
    instr (Script_typed_ir.IList_size info k) (Ty.Family.List_t a :: s) (r :: f)
(*(*
  Sets
  ----
*)
| IEmpty_set :
    ('a, 's) kinfo * 'b comparable_ty * ('b set, 'a * 's, 'r, 'f) kinstr
    -> ('a, 's, 'r, 'f) kinstr
| ISet_iter :
    ('a set, 'b * 's) kinfo
    * ('a, 'b * 's, 'b, 's) kinstr
    * ('b, 's, 'r, 'f) kinstr
    -> ('a set, 'b * 's, 'r, 'f) kinstr
| ISet_mem :
    ('a, 'a set * 's) kinfo * (bool, 's, 'r, 'f) kinstr
    -> ('a, 'a set * 's, 'r, 'f) kinstr
| ISet_update :
    ('a, bool * ('a set * 's)) kinfo * ('a set, 's, 'r, 'f) kinstr
    -> ('a, bool * ('a set * 's), 'r, 'f) kinstr
| ISet_size :
    ('a set, 's) kinfo * (n num, 's, 'r, 'f) kinstr
    -> ('a set, 's, 'r, 'f) kinstr
(*
   Maps
   ----
 *)
| IEmpty_map :
    ('a, 's) kinfo * 'b comparable_ty * (('b, 'c) map, 'a * 's, 'r, 'f) kinstr
    -> ('a, 's, 'r, 'f) kinstr
| IMap_map :
    (('a, 'b) map, 'd * 's) kinfo
    * ('a * 'b, 'd * 's, 'c, 'd * 's) kinstr
    * (('a, 'c) map, 'd * 's, 'r, 'f) kinstr
    -> (('a, 'b) map, 'd * 's, 'r, 'f) kinstr
| IMap_iter :
    (('a, 'b) map, 'c * 's) kinfo
    * ('a * 'b, 'c * 's, 'c, 's) kinstr
    * ('c, 's, 'r, 'f) kinstr
    -> (('a, 'b) map, 'c * 's, 'r, 'f) kinstr
| IMap_mem :
    ('a, ('a, 'b) map * 's) kinfo * (bool, 's, 'r, 'f) kinstr
    -> ('a, ('a, 'b) map * 's, 'r, 'f) kinstr
| IMap_get :
    ('a, ('a, 'b) map * 's) kinfo * ('b option, 's, 'r, 'f) kinstr
    -> ('a, ('a, 'b) map * 's, 'r, 'f) kinstr
| IMap_update :
    ('a, 'b option * (('a, 'b) map * 's)) kinfo
    * (('a, 'b) map, 's, 'r, 'f) kinstr
    -> ('a, 'b option * (('a, 'b) map * 's), 'r, 'f) kinstr
| IMap_get_and_update :
    ('a, 'b option * (('a, 'b) map * 's)) kinfo
    * ('b option, ('a, 'b) map * 's, 'r, 'f) kinstr
    -> ('a, 'b option * (('a, 'b) map * 's), 'r, 'f) kinstr
| IMap_size :
    (('a, 'b) map, 's) kinfo * (n num, 's, 'r, 'f) kinstr
    -> (('a, 'b) map, 's, 'r, 'f) kinstr
(*
   Big maps
   --------
*)
| IEmpty_big_map :
    ('a, 's) kinfo
    * 'b comparable_ty
    * 'c ty
    * (('b, 'c) big_map, 'a * 's, 'r, 'f) kinstr
    -> ('a, 's, 'r, 'f) kinstr
| IBig_map_mem :
    ('a, ('a, 'b) big_map * 's) kinfo * (bool, 's, 'r, 'f) kinstr
    -> ('a, ('a, 'b) big_map * 's, 'r, 'f) kinstr
| IBig_map_get :
    ('a, ('a, 'b) big_map * 's) kinfo * ('b option, 's, 'r, 'f) kinstr
    -> ('a, ('a, 'b) big_map * 's, 'r, 'f) kinstr
| IBig_map_update :
    ('a, 'b option * (('a, 'b) big_map * 's)) kinfo
    * (('a, 'b) big_map, 's, 'r, 'f) kinstr
    -> ('a, 'b option * (('a, 'b) big_map * 's), 'r, 'f) kinstr
| IBig_map_get_and_update :
    ('a, 'b option * (('a, 'b) big_map * 's)) kinfo
    * ('b option, ('a, 'b) big_map * 's, 'r, 'f) kinstr
    -> ('a, 'b option * (('a, 'b) big_map * 's), 'r, 'f) kinstr
(*
   Strings
   -------
*)
| IConcat_string :
    (Script_string.t boxed_list, 's) kinfo
    * (Script_string.t, 's, 'r, 'f) kinstr
    -> (Script_string.t boxed_list, 's, 'r, 'f) kinstr
| IConcat_string_pair :
    (Script_string.t, Script_string.t * 's) kinfo
    * (Script_string.t, 's, 'r, 'f) kinstr
    -> (Script_string.t, Script_string.t * 's, 'r, 'f) kinstr
| ISlice_string :
    (n num, n num * (Script_string.t * 's)) kinfo
    * (Script_string.t option, 's, 'r, 'f) kinstr
    -> (n num, n num * (Script_string.t * 's), 'r, 'f) kinstr
| IString_size :
    (Script_string.t, 's) kinfo * (n num, 's, 'r, 'f) kinstr
    -> (Script_string.t, 's, 'r, 'f) kinstr
(*
   Bytes
   -----
*)
| IConcat_bytes :
    (bytes boxed_list, 's) kinfo * (bytes, 's, 'r, 'f) kinstr
    -> (bytes boxed_list, 's, 'r, 'f) kinstr
| IConcat_bytes_pair :
    (bytes, bytes * 's) kinfo * (bytes, 's, 'r, 'f) kinstr
    -> (bytes, bytes * 's, 'r, 'f) kinstr
| ISlice_bytes :
    (n num, n num * (bytes * 's)) kinfo * (bytes option, 's, 'r, 'f) kinstr
    -> (n num, n num * (bytes * 's), 'r, 'f) kinstr
| IBytes_size :
    (bytes, 's) kinfo * (n num, 's, 'r, 'f) kinstr
    -> (bytes, 's, 'r, 'f) kinstr
(*
   Timestamps
   ----------
 *)
| IAdd_seconds_to_timestamp :
    (z num, Script_timestamp.t * 's) kinfo
    * (Script_timestamp.t, 's, 'r, 'f) kinstr
    -> (z num, Script_timestamp.t * 's, 'r, 'f) kinstr
| IAdd_timestamp_to_seconds :
    (Script_timestamp.t, z num * 's) kinfo
    * (Script_timestamp.t, 's, 'r, 'f) kinstr
    -> (Script_timestamp.t, z num * 's, 'r, 'f) kinstr
| ISub_timestamp_seconds :
    (Script_timestamp.t, z num * 's) kinfo
    * (Script_timestamp.t, 's, 'r, 'f) kinstr
    -> (Script_timestamp.t, z num * 's, 'r, 'f) kinstr
| IDiff_timestamps :
    (Script_timestamp.t, Script_timestamp.t * 's) kinfo
    * (z num, 's, 'r, 'f) kinstr
    -> (Script_timestamp.t, Script_timestamp.t * 's, 'r, 'f) kinstr
(*
   Tez
   ---
  *)
| IAdd_tez :
    (Tez.t, Tez.t * 's) kinfo * (Tez.t, 's, 'r, 'f) kinstr
    -> (Tez.t, Tez.t * 's, 'r, 'f) kinstr
| ISub_tez :
    (Tez.t, Tez.t * 's) kinfo * (Tez.t option, 's, 'r, 'f) kinstr
    -> (Tez.t, Tez.t * 's, 'r, 'f) kinstr
| ISub_tez_legacy :
    (Tez.t, Tez.t * 's) kinfo * (Tez.t, 's, 'r, 'f) kinstr
    -> (Tez.t, Tez.t * 's, 'r, 'f) kinstr
| IMul_teznat :
    (Tez.t, n num * 's) kinfo * (Tez.t, 's, 'r, 'f) kinstr
    -> (Tez.t, n num * 's, 'r, 'f) kinstr
| IMul_nattez :
    (n num, Tez.t * 's) kinfo * (Tez.t, 's, 'r, 'f) kinstr
    -> (n num, Tez.t * 's, 'r, 'f) kinstr
| IEdiv_teznat :
    (Tez.t, n num * 's) kinfo
    * ((Tez.t, Tez.t) pair option, 's, 'r, 'f) kinstr
    -> (Tez.t, n num * 's, 'r, 'f) kinstr
| IEdiv_tez :
    (Tez.t, Tez.t * 's) kinfo
    * ((n num, Tez.t) pair option, 's, 'r, 'f) kinstr
    -> (Tez.t, Tez.t * 's, 'r, 'f) kinstr
(*
   Booleans
   --------
 *)*)
  | IOr {s r f info k} :
    'kinfo info (Ty.Family.Bool_t :: Ty.Family.Bool_t :: s) ->
    instr k (Ty.Family.Bool_t :: s) (r :: f) ->
    instr (Script_typed_ir.IOr info k)
      (Ty.Family.Bool_t :: Ty.Family.Bool_t :: s) (r :: f)
  | IAnd {s r f info k} :
    'kinfo info (Ty.Family.Bool_t :: Ty.Family.Bool_t :: s) ->
    instr k (Ty.Family.Bool_t :: s) (r :: f) ->
    instr (Script_typed_ir.IAnd info k)
      (Ty.Family.Bool_t :: Ty.Family.Bool_t :: s) (r :: f)
  | IXor {s r f info k} :
    'kinfo info (Ty.Family.Bool_t :: Ty.Family.Bool_t :: s) ->
    instr k (Ty.Family.Bool_t :: s) (r :: f) ->
    instr (Script_typed_ir.IXor info k)
      (Ty.Family.Bool_t :: Ty.Family.Bool_t :: s) (r :: f)
  | INot {s r f info k} :
    'kinfo info (Ty.Family.Bool_t :: s) ->
    instr k (Ty.Family.Bool_t :: s) (r :: f) ->
    instr (Script_typed_ir.IOr info k) (Ty.Family.Bool_t :: s) (r :: f)
(*(*
   Integers
   --------
*)
| IIs_nat :
    (z num, 's) kinfo * (n num option, 's, 'r, 'f) kinstr
    -> (z num, 's, 'r, 'f) kinstr
| INeg :
    ('a num, 's) kinfo * (z num, 's, 'r, 'f) kinstr
    -> ('a num, 's, 'r, 'f) kinstr
| IAbs_int :
    (z num, 's) kinfo * (n num, 's, 'r, 'f) kinstr
    -> (z num, 's, 'r, 'f) kinstr
| IInt_nat :
    (n num, 's) kinfo * (z num, 's, 'r, 'f) kinstr
    -> (n num, 's, 'r, 'f) kinstr
| IAdd_int :
    ('a num, 'b num * 's) kinfo * (z num, 's, 'r, 'f) kinstr
    -> ('a num, 'b num * 's, 'r, 'f) kinstr
| IAdd_nat :
    (n num, n num * 's) kinfo * (n num, 's, 'r, 'f) kinstr
    -> (n num, n num * 's, 'r, 'f) kinstr
| ISub_int :
    ('a num, 'b num * 's) kinfo * (z num, 's, 'r, 'f) kinstr
    -> ('a num, 'b num * 's, 'r, 'f) kinstr
| IMul_int :
    ('a num, 'b num * 's) kinfo * (z num, 's, 'r, 'f) kinstr
    -> ('a num, 'b num * 's, 'r, 'f) kinstr
| IMul_nat :
    (n num, 'a num * 's) kinfo * ('a num, 's, 'r, 'f) kinstr
    -> (n num, 'a num * 's, 'r, 'f) kinstr
| IEdiv_int :
    ('a num, 'b num * 's) kinfo
    * ((z num, n num) pair option, 's, 'r, 'f) kinstr
    -> ('a num, 'b num * 's, 'r, 'f) kinstr
| IEdiv_nat :
    (n num, 'a num * 's) kinfo
    * (('a num, n num) pair option, 's, 'r, 'f) kinstr
    -> (n num, 'a num * 's, 'r, 'f) kinstr
| ILsl_nat :
    (n num, n num * 's) kinfo * (n num, 's, 'r, 'f) kinstr
    -> (n num, n num * 's, 'r, 'f) kinstr
| ILsr_nat :
    (n num, n num * 's) kinfo * (n num, 's, 'r, 'f) kinstr
    -> (n num, n num * 's, 'r, 'f) kinstr
| IOr_nat :
    (n num, n num * 's) kinfo * (n num, 's, 'r, 'f) kinstr
    -> (n num, n num * 's, 'r, 'f) kinstr
| IAnd_nat :
    (n num, n num * 's) kinfo * (n num, 's, 'r, 'f) kinstr
    -> (n num, n num * 's, 'r, 'f) kinstr
| IAnd_int_nat :
    (z num, n num * 's) kinfo * (n num, 's, 'r, 'f) kinstr
    -> (z num, n num * 's, 'r, 'f) kinstr
| IXor_nat :
    (n num, n num * 's) kinfo * (n num, 's, 'r, 'f) kinstr
    -> (n num, n num * 's, 'r, 'f) kinstr
| INot_int :
    ('a num, 's) kinfo * (z num, 's, 'r, 'f) kinstr
    -> ('a num, 's, 'r, 'f) kinstr *)
  (*
    Control
    -------
  *)
  | IIf {a b s u r f info branch_if_true branch_if_false k} :
    'kinfo info (Ty.Family.Bool_t :: a :: s) ->
    instr branch_if_true (a :: s) (b :: u) ->
    instr branch_if_false (a :: s) (b :: u) ->
    instr k (b :: u) (r :: f) ->
    instr
      (Script_typed_ir.IIf {|
        Script_typed_ir.kinstr.IIf.kinfo := info;
        Script_typed_ir.kinstr.IIf.branch_if_true := branch_if_true;
        Script_typed_ir.kinstr.IIf.branch_if_false := branch_if_false;
        Script_typed_ir.kinstr.IIf.k := k;
      |})
      (Ty.Family.Bool_t :: a :: s) (r :: f)
  | ILoop {a s r f info body k} :
    'kinfo info (Ty.Family.Bool_t :: a :: s) ->
    instr body (a :: s) (Ty.Family.Bool_t :: a :: s) ->
    instr k (a :: s) (r :: f) ->
    instr (Script_typed_ir.ILoop info body k)
      (Ty.Family.Bool_t :: a :: s) (r :: f)
  | ILoop_left {a b s r f info body k} :
    'kinfo info (Ty.Family.Union_t a b :: s) ->
    instr body (a :: s) (Ty.Family.Union_t a b :: s) ->
    instr k (b :: s) (r :: f) ->
    instr (Script_typed_ir.ILoop_left info body k)
      (Ty.Family.Union_t a b :: s) (r :: f)
  | IDip {a b c s t r f info body k} :
    'kinfo info (a :: b :: s) ->
    instr body (b :: s) (c :: t) ->
    instr k (a :: c :: t) (r :: f) ->
    instr (Script_typed_ir.IDip info body k) (a :: b :: s) (r :: f)
  | IExec {a b s r f info k} :
    'kinfo info (a :: Ty.Family.Lambda_t a b :: s) ->
    instr k (b :: s) (r :: f) ->
    instr (Script_typed_ir.IExec info k)
      (a :: Ty.Family.Lambda_t a b :: s) (r :: f)
  | IApply {a b c s r f info v_a k} :
    'kinfo info (a :: Ty.Family.Lambda_t (Ty.Family.Pair_t a b) c :: s) ->
    ty v_a a ->
    instr k (Ty.Family.Lambda_t b c :: s) (r :: f) ->
    instr (Script_typed_ir.IApply info v_a k)
      (a :: Ty.Family.Lambda_t (Ty.Family.Pair_t a b) c :: s) (r :: f)
  | ILambda {a s b c r f info l k} :
    'kinfo info (a :: s) ->
    lambda l b c ->
    instr k (Ty.Family.Lambda_t b c :: a :: s) (r :: f) ->
    instr (Script_typed_ir.ILambda info l k) (a :: s) (r :: f)
  | IFailwith {v_a a s r f info} loc :
    'kinfo info (a :: s) ->
    ty v_a a ->
    instr (Script_typed_ir.IFailwith info loc v_a) (a :: s) (r :: f)
(*
   Comparison
   ----------
*)
(* | ICompare :
    ('a, 'a * 's) kinfo * 'a comparable_ty * (z num, 's, 'r, 'f) kinstr
    -> ('a, 'a * 's, 'r, 'f) kinstr *)
(*
   Comparators
   -----------
*)
(* | IEq :
    (z num, 's) kinfo * (bool, 's, 'r, 'f) kinstr
    -> (z num, 's, 'r, 'f) kinstr
| INeq :
    (z num, 's) kinfo * (bool, 's, 'r, 'f) kinstr
    -> (z num, 's, 'r, 'f) kinstr
| ILt :
    (z num, 's) kinfo * (bool, 's, 'r, 'f) kinstr
    -> (z num, 's, 'r, 'f) kinstr
| IGt :
    (z num, 's) kinfo * (bool, 's, 'r, 'f) kinstr
    -> (z num, 's, 'r, 'f) kinstr
| ILe :
    (z num, 's) kinfo * (bool, 's, 'r, 'f) kinstr
    -> (z num, 's, 'r, 'f) kinstr
| IGe :
    (z num, 's) kinfo * (bool, 's, 'r, 'f) kinstr
    -> (z num, 's, 'r, 'f) kinstr
(*
   Protocol
   --------
*)
| IAddress :
    ('a typed_contract, 's) kinfo * (address, 's, 'r, 'f) kinstr
    -> ('a typed_contract, 's, 'r, 'f) kinstr
| IContract :
    (address, 's) kinfo
    * 'a ty
    * string
    * ('a typed_contract option, 's, 'r, 'f) kinstr
    -> (address, 's, 'r, 'f) kinstr
| IView :
    ('a, address * 's) kinfo
    * ('a, 'b) view_signature
    * ('b option, 's, 'r, 'f) kinstr
    -> ('a, address * 's, 'r, 'f) kinstr
| ITransfer_tokens :
    ('a, Tez.t * ('a typed_contract * 's)) kinfo
    * (operation, 's, 'r, 'f) kinstr
    -> ('a, Tez.t * ('a typed_contract * 's), 'r, 'f) kinstr
| IImplicit_account :
    (public_key_hash, 's) kinfo * (unit typed_contract, 's, 'r, 'f) kinstr
    -> (public_key_hash, 's, 'r, 'f) kinstr
| ICreate_contract : {
    kinfo : (public_key_hash option, Tez.t * ('a * 's)) kinfo;
    storage_type : 'a ty;
    arg_type : 'b ty;
    lambda : ('b * 'a, operation boxed_list * 'a) lambda;
    views : view SMap.t;
    root_name : field_annot option;
    k : (operation, address * 's, 'r, 'f) kinstr;
  }
    -> (public_key_hash option, Tez.t * ('a * 's), 'r, 'f) kinstr
| ISet_delegate :
    (public_key_hash option, 's) kinfo * (operation, 's, 'r, 'f) kinstr
    -> (public_key_hash option, 's, 'r, 'f) kinstr
| INow :
    ('a, 's) kinfo * (Script_timestamp.t, 'a * 's, 'r, 'f) kinstr
    -> ('a, 's, 'r, 'f) kinstr
| IBalance :
    ('a, 's) kinfo * (Tez.t, 'a * 's, 'r, 'f) kinstr
    -> ('a, 's, 'r, 'f) kinstr
| ILevel :
    ('a, 's) kinfo * (n num, 'a * 's, 'r, 'f) kinstr
    -> ('a, 's, 'r, 'f) kinstr
| ICheck_signature :
    (public_key, signature * (bytes * 's)) kinfo * (bool, 's, 'r, 'f) kinstr
    -> (public_key, signature * (bytes * 's), 'r, 'f) kinstr
| IHash_key :
    (public_key, 's) kinfo * (public_key_hash, 's, 'r, 'f) kinstr
    -> (public_key, 's, 'r, 'f) kinstr
| IPack :
    ('a, 's) kinfo * 'a ty * (bytes, 's, 'r, 'f) kinstr
    -> ('a, 's, 'r, 'f) kinstr
| IUnpack :
    (bytes, 's) kinfo * 'a ty * ('a option, 's, 'r, 'f) kinstr
    -> (bytes, 's, 'r, 'f) kinstr
| IBlake2b :
    (bytes, 's) kinfo * (bytes, 's, 'r, 'f) kinstr
    -> (bytes, 's, 'r, 'f) kinstr
| ISha256 :
    (bytes, 's) kinfo * (bytes, 's, 'r, 'f) kinstr
    -> (bytes, 's, 'r, 'f) kinstr
| ISha512 :
    (bytes, 's) kinfo * (bytes, 's, 'r, 'f) kinstr
    -> (bytes, 's, 'r, 'f) kinstr
| ISource :
    ('a, 's) kinfo * (address, 'a * 's, 'r, 'f) kinstr
    -> ('a, 's, 'r, 'f) kinstr
| ISender :
    ('a, 's) kinfo * (address, 'a * 's, 'r, 'f) kinstr
    -> ('a, 's, 'r, 'f) kinstr
| ISelf :
    ('a, 's) kinfo
    * 'b ty
    * string
    * ('b typed_contract, 'a * 's, 'r, 'f) kinstr
    -> ('a, 's, 'r, 'f) kinstr
| ISelf_address :
    ('a, 's) kinfo * (address, 'a * 's, 'r, 'f) kinstr
    -> ('a, 's, 'r, 'f) kinstr
| IAmount :
    ('a, 's) kinfo * (Tez.t, 'a * 's, 'r, 'f) kinstr
    -> ('a, 's, 'r, 'f) kinstr
| ISapling_empty_state :
    ('a, 's) kinfo
    * Sapling.Memo_size.t
    * (Sapling.state, 'a * 's, 'b, 'f) kinstr
    -> ('a, 's, 'b, 'f) kinstr
| ISapling_verify_update :
    (Sapling.transaction, Sapling.state * 's) kinfo
    * ((z num, Sapling.state) pair option, 's, 'r, 'f) kinstr
    -> (Sapling.transaction, Sapling.state * 's, 'r, 'f) kinstr
| IDig :
    ('a, 's) kinfo
    (*
       There is a prefix of length [n] common to the input stack
       of type ['a * 's] and an intermediary stack of type ['d * 'u].
    *)
    * int
      (*
       Under this common prefix, the input stack has type ['b * 'c * 't] and
       the intermediary stack type ['c * 't] because we removed the ['b] from
       the input stack. This value of type ['b] is pushed on top of the
       stack passed to the continuation.
    *)
    * ('b, 'c * 't, 'c, 't, 'a, 's, 'd, 'u) stack_prefix_preservation_witness
    * ('b, 'd * 'u, 'r, 'f) kinstr
    -> ('a, 's, 'r, 'f) kinstr
| IDug :
    ('a, 'b * 's) kinfo
    (*
       The input stack has type ['a * 'b * 's].

       There is a prefix of length [n] common to its substack
       of type ['b * 's] and the output stack of type ['d * 'u].
    *)
    * int
      (*
       Under this common prefix, the first stack has type ['c * 't]
       and the second has type ['a * 'c * 't] because we have pushed
       the topmost element of this input stack under the common prefix.
    *)
    * ('c, 't, 'a, 'c * 't, 'b, 's, 'd, 'u) stack_prefix_preservation_witness
    * ('d, 'u, 'r, 'f) kinstr
    -> ('a, 'b * 's, 'r, 'f) kinstr
| IDipn :
    ('a, 's) kinfo
    (*
       The body of Dipn is applied under a prefix of size [n]...
    *)
    * int
      (*
       ... the relation between the types of the input and output stacks
       is characterized by the following witness.
       (See forthcoming comments about [stack_prefix_preservation_witness].)
    *)
    * ('c, 't, 'd, 'v, 'a, 's, 'b, 'u) stack_prefix_preservation_witness
    * ('c, 't, 'd, 'v) kinstr
    * ('b, 'u, 'r, 'f) kinstr
    -> ('a, 's, 'r, 'f) kinstr
| IDropn :
    ('a, 's) kinfo
    (*
       The input stack enjoys a prefix of length [n]...
    *)
    * int
      (*
       ... and the following value witnesses that under this prefix
       the stack has type ['b * 'u].
    *)
    * ('b, 'u, 'b, 'u, 'a, 's, 'a, 's) stack_prefix_preservation_witness
    (*
       This stack is passed to the continuation since we drop the
       entire prefix.
    *)
    * ('b, 'u, 'r, 'f) kinstr
    -> ('a, 's, 'r, 'f) kinstr
| IChainId :
    ('a, 's) kinfo * (Chain_id.t, 'a * 's, 'r, 'f) kinstr
    -> ('a, 's, 'r, 'f) kinstr
| INever : (never, 's) kinfo -> (never, 's, 'r, 'f) kinstr
| IVoting_power :
    (public_key_hash, 's) kinfo * (n num, 's, 'r, 'f) kinstr
    -> (public_key_hash, 's, 'r, 'f) kinstr
| ITotal_voting_power :
    ('a, 's) kinfo * (n num, 'a * 's, 'r, 'f) kinstr
    -> ('a, 's, 'r, 'f) kinstr
| IKeccak :
    (bytes, 's) kinfo * (bytes, 's, 'r, 'f) kinstr
    -> (bytes, 's, 'r, 'f) kinstr
| ISha3 :
    (bytes, 's) kinfo * (bytes, 's, 'r, 'f) kinstr
    -> (bytes, 's, 'r, 'f) kinstr
| IAdd_bls12_381_g1 :
    (Bls12_381.G1.t, Bls12_381.G1.t * 's) kinfo
    * (Bls12_381.G1.t, 's, 'r, 'f) kinstr
    -> (Bls12_381.G1.t, Bls12_381.G1.t * 's, 'r, 'f) kinstr
| IAdd_bls12_381_g2 :
    (Bls12_381.G2.t, Bls12_381.G2.t * 's) kinfo
    * (Bls12_381.G2.t, 's, 'r, 'f) kinstr
    -> (Bls12_381.G2.t, Bls12_381.G2.t * 's, 'r, 'f) kinstr
| IAdd_bls12_381_fr :
    (Bls12_381.Fr.t, Bls12_381.Fr.t * 's) kinfo
    * (Bls12_381.Fr.t, 's, 'r, 'f) kinstr
    -> (Bls12_381.Fr.t, Bls12_381.Fr.t * 's, 'r, 'f) kinstr
| IMul_bls12_381_g1 :
    (Bls12_381.G1.t, Bls12_381.Fr.t * 's) kinfo
    * (Bls12_381.G1.t, 's, 'r, 'f) kinstr
    -> (Bls12_381.G1.t, Bls12_381.Fr.t * 's, 'r, 'f) kinstr
| IMul_bls12_381_g2 :
    (Bls12_381.G2.t, Bls12_381.Fr.t * 's) kinfo
    * (Bls12_381.G2.t, 's, 'r, 'f) kinstr
    -> (Bls12_381.G2.t, Bls12_381.Fr.t * 's, 'r, 'f) kinstr
| IMul_bls12_381_fr :
    (Bls12_381.Fr.t, Bls12_381.Fr.t * 's) kinfo
    * (Bls12_381.Fr.t, 's, 'r, 'f) kinstr
    -> (Bls12_381.Fr.t, Bls12_381.Fr.t * 's, 'r, 'f) kinstr
| IMul_bls12_381_z_fr :
    (Bls12_381.Fr.t, 'a num * 's) kinfo * (Bls12_381.Fr.t, 's, 'r, 'f) kinstr
    -> (Bls12_381.Fr.t, 'a num * 's, 'r, 'f) kinstr
| IMul_bls12_381_fr_z :
    ('a num, Bls12_381.Fr.t * 's) kinfo * (Bls12_381.Fr.t, 's, 'r, 'f) kinstr
    -> ('a num, Bls12_381.Fr.t * 's, 'r, 'f) kinstr
| IInt_bls12_381_fr :
    (Bls12_381.Fr.t, 's) kinfo * (z num, 's, 'r, 'f) kinstr
    -> (Bls12_381.Fr.t, 's, 'r, 'f) kinstr
| INeg_bls12_381_g1 :
    (Bls12_381.G1.t, 's) kinfo * (Bls12_381.G1.t, 's, 'r, 'f) kinstr
    -> (Bls12_381.G1.t, 's, 'r, 'f) kinstr
| INeg_bls12_381_g2 :
    (Bls12_381.G2.t, 's) kinfo * (Bls12_381.G2.t, 's, 'r, 'f) kinstr
    -> (Bls12_381.G2.t, 's, 'r, 'f) kinstr
| INeg_bls12_381_fr :
    (Bls12_381.Fr.t, 's) kinfo * (Bls12_381.Fr.t, 's, 'r, 'f) kinstr
    -> (Bls12_381.Fr.t, 's, 'r, 'f) kinstr
| IPairing_check_bls12_381 :
    ((Bls12_381.G1.t, Bls12_381.G2.t) pair boxed_list, 's) kinfo
    * (bool, 's, 'r, 'f) kinstr
    -> ((Bls12_381.G1.t, Bls12_381.G2.t) pair boxed_list, 's, 'r, 'f) kinstr
| IComb :
    ('a, 's) kinfo
    * int
    * ('a * 's, 'b * 'u) comb_gadt_witness
    * ('b, 'u, 'r, 'f) kinstr
    -> ('a, 's, 'r, 'f) kinstr
| IUncomb :
    ('a, 's) kinfo
    * int
    * ('a * 's, 'b * 'u) uncomb_gadt_witness
    * ('b, 'u, 'r, 'f) kinstr
    -> ('a, 's, 'r, 'f) kinstr
| IComb_get :
    ('t, 's) kinfo
    * int
    * ('t, 'v) comb_get_gadt_witness
    * ('v, 's, 'r, 'f) kinstr
    -> ('t, 's, 'r, 'f) kinstr
| IComb_set :
    ('a, 'b * 's) kinfo
    * int
    * ('a, 'b, 'c) comb_set_gadt_witness
    * ('c, 's, 'r, 'f) kinstr
    -> ('a, 'b * 's, 'r, 'f) kinstr
| IDup_n :
    ('a, 's) kinfo
    * int
    * ('a * 's, 't) dup_n_gadt_witness
    * ('t, 'a * 's, 'r, 'f) kinstr
    -> ('a, 's, 'r, 'f) kinstr
| ITicket :
    ('a, n num * 's) kinfo * ('a ticket, 's, 'r, 'f) kinstr
    -> ('a, n num * 's, 'r, 'f) kinstr
| IRead_ticket :
    ('a ticket, 's) kinfo
    * (address * ('a * n num), 'a ticket * 's, 'r, 'f) kinstr
    -> ('a ticket, 's, 'r, 'f) kinstr
| ISplit_ticket :
    ('a ticket, (n num * n num) * 's) kinfo
    * (('a ticket * 'a ticket) option, 's, 'r, 'f) kinstr
    -> ('a ticket, (n num * n num) * 's, 'r, 'f) kinstr
| IJoin_tickets :
    ('a ticket * 'a ticket, 's) kinfo
    * 'a comparable_ty
    * ('a ticket option, 's, 'r, 'f) kinstr
    -> ('a ticket * 'a ticket, 's, 'r, 'f) kinstr
| IOpen_chest :
    (Timelock.chest_key, Timelock.chest * (n num * 's)) kinfo
    * ((bytes, bool) union, 's, 'r, 'f) kinstr
    -> (Timelock.chest_key, Timelock.chest * (n num * 's), 'r, 'f) kinstr*)
  (*
    Internal control instructions
    -----------------------------
    The following instructions are not available in the source language.
    They are used by the internals of the interpreter.
  *)
  | IHalt {a s r f info} :
    'kinfo info (a :: s) ->
    instr (Script_typed_ir.IHalt info) (a :: s) (r :: f)
  | ILog {a s r f info} event logger {k} :
    'kinfo info (a :: s) ->
    instr k (a :: s) (r :: f) ->
    instr (Script_typed_ir.ILog info event logger k) (a :: s) (r :: f)

  with lambda : Script_typed_ir.lambda -> Ty.Family.t -> Ty.Family.t -> Set :=
  | Lam {arg ret descr} node :
    'kdescr descr [arg] [ret] ->
    lambda (Script_typed_ir.Lam descr node) arg ret

  with ty : Script_typed_ir.ty -> Ty.Family.t -> Set :=
  | Unit_t metadata :
    ty (Script_typed_ir.Unit_t metadata) Ty.Family.Unit_t
  | Int_t metadata :
    ty (Script_typed_ir.Int_t metadata) Ty.Family.Int_t
  | Nat_t metadata :
    ty (Script_typed_ir.Nat_t metadata) Ty.Family.Nat_t
  | Signature_t metadata :
    ty (Script_typed_ir.Signature_t metadata) Ty.Family.Signature_t
  | String_t metadata :
    ty (Script_typed_ir.String_t metadata) Ty.Family.String_t
  | Bytes_t metadata :
    ty (Script_typed_ir.Bytes_t metadata) Ty.Family.Bytes_t
  | Mutez_t metadata :
    ty (Script_typed_ir.Mutez_t metadata) Ty.Family.Mutez_t
  | Key_hash_t metadata :
    ty (Script_typed_ir.Key_hash_t metadata) Ty.Family.Key_hash_t
  | Key_t metadata :
    ty (Script_typed_ir.Key_t metadata) Ty.Family.Key_t
  | Timestamp_t metadata :
    ty (Script_typed_ir.Timestamp_t metadata) Ty.Family.Timestamp_t
  | Address_t metadata :
    ty (Script_typed_ir.Address_t metadata) Ty.Family.Address_t
  | Bool_t metadata :
    ty (Script_typed_ir.Bool_t metadata) Ty.Family.Bool_t
  | Pair_t {v1 m1} f_annot1 v_annot1 {v2 m2} f_annot2 v_annot2 metadata :
    ty v1 m1 ->
    ty v2 m2 ->
    ty
      (Script_typed_ir.Pair_t
        (v1, f_annot1, v_annot1) (v2, f_annot2, v_annot2) metadata)
      (Family.Pair_t m1 m2)
  | Union_t {v1 m1} f_annot1 {v2 m2} f_annot2 metadata :
    ty v1 m1 ->
    ty v2 m2 ->
    ty
      (Script_typed_ir.Union_t
        (v1, f_annot1) (v2, f_annot2) metadata)
      (Family.Union_t m1 m2)
  | Lambda_t {v_arg m_arg v_ret m_ret} metadata :
    ty v_arg m_arg ->
    ty v_ret m_ret ->
    ty (Script_typed_ir.Lambda_t v_arg v_ret metadata)
      (Family.Lambda_t m_arg m_arg)
  | Option_t {v m} metadata :
    ty v m ->
    ty (Script_typed_ir.Option_t v metadata) (Family.Option_t m)
  | List_t {v m} metadata :
    ty v m ->
    ty (Script_typed_ir.List_t v metadata) (Family.List_t m)
  | Set_t {v m} metadata :
    Comparable_ty.With_family.t v m ->
    ty (Script_typed_ir.Set_t v metadata) (Family.Set_t m)
  | Map_t {v_key m_key v_value m_value} metadata :
    Comparable_ty.With_family.t v_key m_key ->
    ty v_value m_value ->
    ty (Script_typed_ir.Map_t v_key v_value metadata)
      (Family.Map_t m_key m_value)
  | Big_map_t {v_key m_key v_value m_value} metadata :
    Comparable_ty.With_family.t v_key m_key ->
    ty v_value m_value ->
    ty (Script_typed_ir.Big_map_t v_key v_value metadata)
      (Family.Big_map_t m_key m_value)
  | Contract_t {v m} metadata :
    ty v m ->
    ty (Script_typed_ir.Contract_t v metadata) (Family.Contract_t m)
  | Sapling_transaction_t memo_size metadata :
    ty (Script_typed_ir.Sapling_transaction_t memo_size metadata)
      Ty.Family.Sapling_transaction_t
  | Sapling_state_t memo_size metadata :
    ty (Script_typed_ir.Sapling_state_t memo_size metadata)
      Ty.Family.Sapling_state_t
  | Operation_t metadata :
    ty (Script_typed_ir.Operation_t metadata) Ty.Family.Operation_t
  | Chain_id_t metadata :
    ty (Script_typed_ir.Chain_id_t metadata) Ty.Family.Chain_id_t
  | Never_t metadata :
    ty (Script_typed_ir.Never_t metadata) Ty.Family.Never_t
  | Bls12_381_g1_t metadata :
    ty (Script_typed_ir.Bls12_381_g1_t metadata) Ty.Family.Bls12_381_g1_t
  | Bls12_381_g2_t metadata :
    ty (Script_typed_ir.Bls12_381_g2_t metadata) Ty.Family.Bls12_381_g2_t
  | Bls12_381_fr_t metadata :
    ty (Script_typed_ir.Bls12_381_fr_t metadata) Ty.Family.Bls12_381_fr_t
  | Ticket_t {v m} metadata :
    Comparable_ty.With_family.t v m ->
    ty (Script_typed_ir.Ticket_t v metadata) (Family.Ticket_t m)
  | Chest_key_t metadata :
    ty (Script_typed_ir.Chest_key_t metadata) Ty.Family.Chest_key_t
  | Chest_t metadata :
    ty (Script_typed_ir.Chest_t metadata) Ty.Family.Chest_t

  with stack_ty : Script_typed_ir.stack_ty -> list Ty.Family.t -> Set :=
  | Item_t {v_ty m_ty v_rest m_rest} annot :
    ty v_ty m_ty ->
    stack_ty v_rest m_rest ->
    stack_ty (Script_typed_ir.Item_t v_ty v_rest annot) (m_ty :: m_rest)
  | Bot_t :
    stack_ty Script_typed_ir.Bot_t []

  where "'kdescr" := (fun (v : Script_typed_ir.kdescr) (s f : list Ty.Family.t) =>
    stack_ty v.(Script_typed_ir.kdescr.kbef) s *
    stack_ty v.(Script_typed_ir.kdescr.kaft) f *
    instr v.(Script_typed_ir.kdescr.kinstr) s f
  )

  and "'kinfo" := (fun (v : Script_typed_ir.kinfo) (m : list Ty.Family.t) =>
    stack_ty v.(Script_typed_ir.kinfo.kstack_ty) m).

  Definition kdescr := 'kdescr.
  Definition kinfo := 'kinfo.
End With_family.
