Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Vote_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Lemma ballot_encoding_is_valid
  : Data_encoding.Valid.t (fun _ => True) Vote_repr.ballot_encoding.
  eapply Data_encoding.Valid.implies.
  eapply Data_encoding.Valid.splitted.
  { apply Data_encoding.Valid.string_enum; reflexivity. }
  { eapply Data_encoding.Valid.conv_with_guard.
    apply Data_encoding.Valid.int8.
  }
  { intro x; destruct x; simpl; tauto. }
Qed.
