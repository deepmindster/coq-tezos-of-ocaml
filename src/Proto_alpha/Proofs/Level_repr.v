Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Level_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Compare.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

(* See https://nomadic-labs.gitlab.io/coq-tezos-of-ocaml/docs/proofs/contract_repr
   for another proof on comparisons. *)
Axiom compare_is_valid :
  Compare.Valid.t (Compare.wrap_compare Level_repr.compare).

(* See https://nomadic-labs.gitlab.io/coq-tezos-of-ocaml/docs/proofs/constants_repr
   to have a file with many proofs on the data-encodings. *)
Axiom encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Level_repr.encoding.

Axiom diff_value_eq_zero : forall l, Level_repr.diff_value l l = 0.

Axiom cycle_era_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Level_repr.cycle_era_encoding.

Axiom cycle_eras_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Level_repr.cycle_eras_encoding.
