Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Constants_repr.
Require TezosOfOCaml.Proto_alpha.Gas_limit_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.List.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Signature.
Require TezosOfOCaml.Proto_alpha.Proofs.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Nonce_hash.
Require TezosOfOCaml.Proto_alpha.Proofs.Round_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Seed_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Tez_repr.

Module Ratio.
  Module Valid.
    Definition t (ratio : Constants_repr.ratio) : Prop :=
      ratio.(Constants_repr.ratio.denominator) >i 0 = true.
  End Valid.
End Ratio.

Lemma ratio_encoding_is_valid
  : Data_encoding.Valid.t Ratio.Valid.t Constants_repr.ratio_encoding.
  Data_encoding.Valid.data_encoding_auto.
  intros x H; repeat split.
  now rewrite H.
Qed.
#[global] Hint Resolve ratio_encoding_is_valid : Data_encoding_db.

Lemma fixed_encoding_is_valid
  : Data_encoding.Valid.t (fun _ => True) Constants_repr.fixed_encoding.
  Data_encoding.Valid.data_encoding_auto.
  intros []; repeat split.
  repeat constructor.
Qed.
#[global] Hint Resolve fixed_encoding_is_valid : Data_encoding_db.

Lemma delegate_selection_encoding_is_valid
  : Data_encoding.Valid.t (fun _ => True) Constants_repr.delegate_selection_encoding.
  Data_encoding.Valid.data_encoding_auto;
    intros []; try tauto.
  auto using List.Forall_True.
Qed.
#[global] Hint Resolve delegate_selection_encoding_is_valid : Data_encoding_db.

Module Parametric.
  Module Valid.
    Record t {c : Constants_repr.parametric} : Prop := {
      minimal_participation_ratio : Ratio.Valid.t
        c.(Constants_repr.parametric.minimal_participation_ratio);
      ratio_of_frozen_deposits_slashed_per_double_endorsement : Ratio.Valid.t
        c.(Constants_repr.parametric.ratio_of_frozen_deposits_slashed_per_double_endorsement);
      round_durations : Round_repr.Durations.Valid.t
        c.(Constants_repr.parametric.round_durations);
    }.
    Arguments t : clear implicits.
  End Valid.
End Parametric.

Lemma parametric_encoding_is_valid
  : Data_encoding.Valid.t
      Parametric.Valid.t Constants_repr.parametric_encoding.
  Data_encoding.Valid.data_encoding_auto.
  intros x H; simpl; destruct H, round_durations; repeat split; trivial.
Qed.
#[global] Hint Resolve parametric_encoding_is_valid : Data_encoding_db.

Module Valid.
  Definition t (c : Constants_repr.t) : Prop :=
    Parametric.Valid.t c.(Constants_repr.t.parametric).
End Valid.

Lemma encoding_is_valid
  : Data_encoding.Valid.t Valid.t Constants_repr.encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

Module Proto_previous.
  Module Valid.
    Record t {c : Constants_repr.Proto_previous.parametric} : Prop := {
      delay_per_missing_endorsement :
        Period_repr.Valid.t
          c.(Constants_repr.Proto_previous.parametric.delay_per_missing_endorsement);
      minimal_block_delay :
        Period_repr.Valid.t
          c.(Constants_repr.Proto_previous.parametric.minimal_block_delay);
      time_between_blocks :
        List.Forall Period_repr.Valid.t
          c.(Constants_repr.Proto_previous.parametric.time_between_blocks);
    }.
    Arguments t : clear implicits.
  End Valid.

  Lemma parametric_encoding_is_valid 
    : Data_encoding.Valid.t Valid.t Constants_repr.Proto_previous.parametric_encoding.
    Data_encoding.Valid.data_encoding_auto.
    intros ? H; destruct H; intuition; now apply List.Forall_True.
  Qed.
  #[global] Hint Resolve parametric_encoding_is_valid : Data_encoding_db.
End Proto_previous.
