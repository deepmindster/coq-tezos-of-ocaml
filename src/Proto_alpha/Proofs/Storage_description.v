Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Storage_description.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Compare.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.RPC_arg.
Require TezosOfOCaml.Proto_alpha.Proofs.Path_encoding.

Module INDEX.
  Definition to_Path {t : Set} (I : Storage_description.INDEX (t := t))
    : Path_encoding.S (t := t) := {|
      Path_encoding.S.to_path := I.(Storage_description.INDEX.to_path);
      Path_encoding.S.of_path := I.(Storage_description.INDEX.of_path);
      Path_encoding.S.path_length := I.(Storage_description.INDEX.path_length);
    |}.

  Module Valid_on.
    Record t {t : Set} {domain : t -> Prop}
      {I : Storage_description.INDEX (t := t)} : Prop := {
      Path_is_valid : Path_encoding.S.Valid.t (to_Path I);
      rpc_arg : RPC_arg.Valid.t domain I.(Storage_description.INDEX.rpc_arg);
      encoding_is_valid :
        Data_encoding.Valid.t domain I.(Storage_description.INDEX.encoding);
      compare_is_valid :
        Compare.Valid.t
          (Compare.wrap_compare I.(Storage_description.INDEX.compare));
    }.
    Arguments t {_}.
  End Valid_on.

  Module Valid.
    Definition t {t : Set} (I : Storage_description.INDEX (t := t)) : Prop :=
      Valid_on.t (fun '_ => True) I.
  End Valid.
End INDEX.
