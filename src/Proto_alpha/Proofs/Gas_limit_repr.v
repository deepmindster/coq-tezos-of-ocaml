Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Require Import Lia.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Gas_limit_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Pervasives.
Require TezosOfOCaml.Proto_alpha.Proofs.Saturation_repr.

Module Arith.
  (** TODO This should be proved but some of the functions (land for ex) are
      still opaque. *)
  Axiom integral_exn_integral_to_z : forall {v_a},
    Gas_limit_repr.Arith.integral_exn (Gas_limit_repr.Arith.integral_to_z v_a) =
    v_a.

  Lemma z_integral_encoding_is_valid
    : Data_encoding.Valid.t (fun _ => True) Gas_limit_repr.Arith.z_integral_encoding.
    eapply Data_encoding.Valid.implies.
    eapply
      Data_encoding.Valid.conv,
      Data_encoding.Valid.z_value.
    intuition; now rewrite integral_exn_integral_to_z.
  Qed.
  #[global] Hint Resolve z_integral_encoding_is_valid : Data_encoding_db.
End Arith.

Module Cost.
  Module Valid.
    Definition t (x : Gas_limit_repr.cost) : Prop :=
      Saturation_repr.Valid.t x.
  End Valid.
End Cost.

Lemma raw_consume_is_valid gas_counter cost
  : gas_counter >= cost ->
    Cost.Valid.t cost ->
    Saturation_repr.Valid.t gas_counter ->
    Option.post_when_success (Gas_limit_repr.raw_consume gas_counter cost)
      (fun gas_counter' =>
        Gas_limit_repr.Arith.op_lteq gas_counter' gas_counter = true).
  unfold Option.post_when_success. 
  unfold Gas_limit_repr.raw_consume.
  unfold Gas_limit_repr.Arith.sub_opt.
  unfold Gas_limit_repr.cost_to_milligas.
  unfold Gas_limit_repr.S.sub_opt.
  unfold Cost.Valid.t. 
  unfold Saturation_repr.Valid.t, Saturation_repr.Valid.check.
  unfold Saturation_repr.saturated, Pervasives.max_int.
  cbn; autounfold with tezos_z.
  unfold normalize, two_pow_62, two_pow_63, two_pow_62.
  repeat rewrite Zminus_0_r.
  intros.
  match goal with
  | [|- context[if ?e then _ else _]] => destruct e eqn:H_eq
  end; try lia. 
Qed.
