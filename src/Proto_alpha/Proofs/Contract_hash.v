Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Contract_hash.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Blake2B.
Require TezosOfOCaml.Proto_alpha.Proofs.Path_encoding.

Lemma H_is_valid : S.HASH.Valid.t (fun _ => True) Contract_hash.H.
  apply Blake2B.Make_is_valid.
Qed.
(* Do not replace this by Hint Resolve because the `auto` tactic would not be
   able to apply this lemma for encodings. *)
#[global] Hint Extern 1 => apply H_is_valid : Data_encoding_db.

Lemma Path_encoding_Make_hex_include_is_valid
  : Path_encoding.S.Valid.t Contract_hash.Path_encoding_Make_hex_include.
  apply Path_encoding.Make_hex_is_valid.
  constructor; apply H_is_valid.
Qed.
