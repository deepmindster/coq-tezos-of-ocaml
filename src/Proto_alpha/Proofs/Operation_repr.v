Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Operation_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Operation_hash.

Lemma equal_manager_operation_kind_refl op
  : Operation_repr.equal_manager_operation_kind op op = Some Operation_repr.Eq.
  destruct op; reflexivity.
Qed.

Lemma equal_manager_operation_kind_implies_eq op1 op2
  : Operation_repr.equal_manager_operation_kind op1 op2 = Some Operation_repr.Eq ->
  Operation_repr.manager_kind op1 = Operation_repr.manager_kind op2.
  destruct op1, op2; cbv; congruence.
Qed.

Lemma equal_contents_kind_refl op
  : Operation_repr.equal_contents_kind op op = Some Operation_repr.Eq.
  destruct op; try reflexivity.
  unfold Operation_repr.equal_contents_kind.
  now rewrite equal_manager_operation_kind_refl.
Qed.

Fixpoint equal_contents_kind_list_refl op
  : Operation_repr.equal_contents_kind_list op op = Some Operation_repr.Eq.
  destruct op; unfold Operation_repr.equal_contents_kind_list.
  { apply equal_contents_kind_refl. }
  { rewrite equal_contents_kind_refl.
    fold Operation_repr.equal_contents_kind_list.
    now rewrite equal_contents_kind_list_refl.
  }
Qed.

Lemma equal_refl op
  : Operation_repr.equal op op = Some Operation_repr.Eq.
  unfold Operation_repr.equal.
  assert (valid_compare :=
    Operation_hash.Included_HASH_is_valid
      .(S.HASH.Valid.MINIMAL_HASH)
      .(S.MINIMAL_HASH.Valid.Compare_S)).
  rewrite valid_compare.(Compare.S.Valid.rest).(Compare.S.Eq.equal).
  simpl.
  rewrite (Compare.Pre_valid.refl_wrap (f := id)).
  { now rewrite equal_contents_kind_list_refl. }
  { apply valid_compare.(Compare.S.Valid.compare). }
Qed.
