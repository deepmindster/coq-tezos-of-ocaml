Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Contract_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Compare.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Operation_hash.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Signature.
Require TezosOfOCaml.Proto_alpha.Proofs.Contract_hash.
Require TezosOfOCaml.Proto_alpha.Proofs.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Proofs.Saturation_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Storage_description.

Lemma compare_is_valid
  : Compare.Valid.t (Compare.wrap_compare Contract_repr.compare).
  constructor; unfold Compare.wrap_compare, Contract_repr.compare; simpl;
    unfold id; change (_.(Compare.COMPARABLE.t)) with Contract_repr.t;
    intros;
    try match goal with
    | [H : _ = _ |- _] => now rewrite H
    end;
    destruct_all Contract_repr.t; simpl in *; try easy;
    try f_equal;
    try apply Signature.Public_key_hash_compare_is_valid;
    try apply Contract_hash.H_is_valid;
    try easy;
    try eapply
      (Signature.Public_key_hash_compare_is_valid.(Compare.Pre_valid.trans));
    try eapply Contract_hash.H_is_valid;
    match goal with
    | [H : _ = 1 |- _] => exact H
    end.
Qed.

Lemma blake2b_hash_size_is_valid
  : Saturation_repr.Valid.t Contract_repr.blake2b_hash_size.
  reflexivity.
Qed.

Lemma public_key_hash_in_memory_size_is_valid
  : Saturation_repr.Valid.t Contract_repr.public_key_hash_in_memory_size.
  reflexivity.
Qed.

Lemma in_memory_size_is_valid : forall contract,
  Saturation_repr.Valid.t (Contract_repr.in_memory_size contract).
  intros.
  destruct contract; easy.
Qed.

Axiom of_b58_to_b58_eq : forall contract,
  Contract_repr.of_b58check (Contract_repr.to_b58check contract) =
  return? contract.

Axiom to_b58_of_b58_eq : forall b58,
  match Contract_repr.of_b58check b58 with
  | Pervasives.Ok contract => Contract_repr.to_b58check contract = b58
  | Pervasives.Error _ => True
  end.

Lemma implicit_contract_is_implicit : forall id,
  Contract_repr.is_implicit (Contract_repr.implicit_contract id) = Some id.
  reflexivity.
Qed.

Lemma implicit_contract_is_not_originated : forall id,
  Contract_repr.is_originated (Contract_repr.implicit_contract id) = None.
  reflexivity.
Qed.

Lemma origination_nonce_encoding_is_valid
  : Data_encoding.Valid.t (fun _ => True) Contract_repr.origination_nonce_encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve origination_nonce_encoding_is_valid : Data_encoding_db.

Axiom originated_contracts_length : forall first_nonce last_nonce,
  List.length (Contract_repr.originated_contracts first_nonce last_nonce) =
  Compare.Int.(Compare.S.max) 0 (
    last_nonce.(Contract_repr.origination_nonce.origination_index) -
    first_nonce.(Contract_repr.origination_nonce.origination_index)
  ).

Lemma rpc_arg_valid : RPC_arg.Valid.t (fun _ => True) Contract_repr.rpc_arg.
  constructor; intros; simpl.
  { rewrite of_b58_to_b58_eq.
    simpl.
    reflexivity.
  }
  { assert (H := to_b58_of_b58_eq s).
    now destruct (Contract_repr.of_b58check _).
  }
Qed.

Lemma encoding_is_valid : Data_encoding.Valid.t (fun _ => True) Contract_repr.encoding.
  Data_encoding.Valid.data_encoding_auto;
    try now intros [].
  intros [] ?; try (repeat apply conj; try tauto);
    try rewrite of_b58_to_b58_eq; reflexivity.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

Lemma index_path_encoding_is_valid
  : Path_encoding.S.Valid.t
    (Storage_description.INDEX.to_Path Contract_repr.Index).
  constructor; intros; simpl;
    unfold Contract_repr.Index.to_path, Contract_repr.Index.of_path.
  - destruct Hex.of_bytes. reflexivity.
  - destruct Hex.of_bytes eqn:H0.
    rewrite <- H0.
    rewrite Hex.to_bytes_of_bytes; simpl.
    unfold Binary.to_bytes_exn.
    destruct Binary.to_bytes_opt; apply axiom.
  - destruct path as [|s[]]; trivial.
    destruct (Hex.to_bytes (Hex s)) eqn:H_s; simpl; trivial.
    destruct (Binary.of_bytes_opt _ _) eqn:H_b; trivial.
    eassert (H_encoding_to_of :=
      encoding_is_valid.(Data_encoding.Valid.to_bytes_opt_of_bytes_opt) _).
    rewrite H_b in H_encoding_to_of.
    unfold Binary.to_bytes_exn.
    rewrite H_encoding_to_of.
    eassert (H_hex_of_to := Hex.of_bytes_to_bytes _).
    rewrite H_s in H_hex_of_to.
    now rewrite H_hex_of_to.
  - now destruct (Hex.of_bytes _ _).
Qed.

Lemma index_is_valid : Storage_description.INDEX.Valid.t Contract_repr.Index.
  constructor;
    try apply index_path_encoding_is_valid;
    try apply encoding_is_valid;
    try apply rpc_arg_valid;
    try apply compare_is_valid.
Qed.
