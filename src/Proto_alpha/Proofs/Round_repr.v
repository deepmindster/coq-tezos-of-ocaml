Require Import Lia.
Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Round_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Proofs.Period_repr.

Module Valid.
  Definition t (n : Round_repr.t) : Prop :=
    n >=i32 0 = true.
End Valid.

Lemma encoding_is_valid : Data_encoding.Valid.t Valid.t Round_repr.encoding.
  eapply Data_encoding.Valid.implies.
  eapply Data_encoding.Valid.conv_with_guard;
    apply Data_encoding.Valid.int32_value.
  unfold Valid.t, Round_repr.of_int32, Round_repr.encoding, Round_repr.op_gteq.
  intuition.
  now destruct (_ >=i32 0).
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

Module Durations.
  Module Valid.
    Record t {durations : Round_repr.Durations.t} : Prop := {
      durations_list :=
        durations.(Round_repr.Durations.t.round0) ::
        durations.(Round_repr.Durations.t.round1) ::
        durations.(Round_repr.Durations.t.other_rounds);
      minimal_duration :
        Period_repr.to_seconds durations.(Round_repr.Durations.t.round0)
          <i64 1 = false;
      ordered :
        match Round_repr.Durations.check_ordered durations_list with
        | Pervasives.Ok _ => True
        | Pervasives.Error _ => False
        end;
      periods :
        List.Forall Period_repr.Valid.t durations_list;
    }.
    Arguments t : clear implicits.
  End Valid.

  Lemma create_succeeds_when_valid {rest d1 d2}
    : let durations := Round_repr.Durations.t.Build d1 d2 rest in
      let rest_opt :=
        match rest with
        | [] => None
        | _ :: _ => Some rest
        end in
      Valid.t durations ->
      Round_repr.Durations.create rest_opt d1 d2 tt =
      Pervasives.Ok durations.
    intros durations rest_opt H; unfold Round_repr.Durations.create.
    destruct H; simpl in *.
    rewrite minimal_duration; simpl.
    destruct (Period_repr.op_gt _ _); simpl in *; try contradiction.
    destruct rest; simpl; trivial.
    destruct (Period_repr.op_gt _ _); simpl in *; try contradiction.
    now match goal with
    | [|- (let? '_ := ?e in _) = _] => destruct e
    end.
  Qed.

  Lemma encoding_is_valid
    : Data_encoding.Valid.t Valid.t Round_repr.Durations.encoding.
    Data_encoding.Valid.data_encoding_auto.
    intros x H; destruct H.
    assert (round0_valid := List.Forall_inv periods).
    assert (periods_0_valid := List.Forall_inv_tail periods).
    assert (round1_valid := List.Forall_inv periods_0_valid).
    assert (other_rounds_valid := List.Forall_inv_tail periods_0_valid).
    repeat split; trivial.
    { destruct x, other_rounds; trivial. }
    { unfold Round_repr.Durations.create_opt.
      now rewrite create_succeeds_when_valid.
    }
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.
End Durations.
