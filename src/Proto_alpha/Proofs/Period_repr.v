Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Period_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Module Valid.
  Definition t (period : Period_repr.t) : Prop :=
    period >=i64 0 = true.
End Valid.

Module Internal.
  Lemma encoding_is_valid
    : Data_encoding.Valid.t Valid.t Period_repr.Internal.encoding.
    Data_encoding.Valid.data_encoding_auto.
    unfold Valid.t.
    intros.
    now replace (_ >=i64 _) with true.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.
End Internal.

Lemma encoding_is_valid
  : Data_encoding.Valid.t Valid.t Period_repr.encoding.
  exact Internal.encoding_is_valid.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.
