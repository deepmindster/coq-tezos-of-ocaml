Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_services.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.RPC_service.
Require TezosOfOCaml.Proto_alpha.Proofs.Nonce_hash.
Require TezosOfOCaml.Proto_alpha.Proofs.Seed_repr.

Module Seed.
  Module S.
    Axiom seed_value_is_valid :
      RPC_service.Valid.t Alpha_services.Seed.S.seed_value.
  End S.
End Seed.

Module Nonce.
  Module Info.
    Module Valid.
      Definition t (info : Alpha_services.Nonce.info) : Prop :=
        match info with
        | Alpha_services.Nonce.Revealed nonce => Seed_repr.Nonce.Valid.t nonce
        | _ => True
        end.
    End Valid.

    Lemma encoding_is_valid
      : Data_encoding.Valid.t Valid.t Alpha_services.Nonce.info_encoding.
      Data_encoding.Valid.data_encoding_auto;
        intros []; tauto.
    Qed.
    #[global] Hint Resolve encoding_is_valid : Data_encoding_db.
  End Info.

  Module S.
    Axiom get_is_valid : RPC_service.Valid.t Alpha_services.Nonce.S.get.
  End S.
End Nonce.

Module Liquidity_baking.
  Module S.
    Axiom get_cpmm_address_is_valid :
      RPC_service.Valid.t Alpha_services.Liquidity_baking.S.get_cpmm_address.
  End S.
End Liquidity_baking.

Module Cache.
  Module S.
    Axiom cached_contracts_is_valid :
      RPC_service.Valid.t Alpha_services.Cache.S.cached_contracts.

    Axiom contract_cache_size_is_valid :
      RPC_service.Valid.t Alpha_services.Cache.S.contract_cache_size.

    Axiom contract_cache_size_limit_is_valid :
      RPC_service.Valid.t Alpha_services.Cache.S.contract_cache_size_limit.

    Axiom contract_rank_is_valid :
      RPC_service.Valid.t Alpha_services.Cache.S.contract_rank.
  End S.
End Cache.
