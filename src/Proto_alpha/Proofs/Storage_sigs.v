Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require Import TezosOfOCaml.Proto_alpha.Storage_sigs.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Context.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Error_monad.
Require TezosOfOCaml.Proto_alpha.Proofs.Raw_context.

Module Single_data_storage.
  Module Eq.
    Import Proto_alpha.Storage_sigs.Single_data_storage.

    Record t {t value : Set}
      {S1 S2 : Single_data_storage (t := t) (value := value)}
      : Prop := {
      mem ctxt : S1.(mem) ctxt = S2.(mem) ctxt;
      get ctxt : S1.(get) ctxt = S2.(get) ctxt;
      find ctxt : S1.(find) ctxt = S2.(find) ctxt;
      init_value ctxt value :
        S1.(init_value) ctxt value = S2.(init_value) ctxt value;
      update ctxt value :
        S1.(update) ctxt value = S2.(update) ctxt value;
      add ctxt value :
        S1.(add) ctxt value = S2.(add) ctxt value;
      add_or_remove ctxt value :
        S1.(add_or_remove) ctxt value = S2.(add_or_remove) ctxt value;
      remove_existing ctxt :
        S1.(remove_existing) ctxt = S2.(remove_existing) ctxt;
      remove ctxt : S1.(remove) ctxt = S2.(remove) ctxt;
    }.
    Arguments t {_ _ }.
  End Eq.

  (** The type of a simple store. We add a special case for a state with
      an invalid encoding. It should not be possible to create such a state,
      but we may not control what is already in the input store. *)
  Module State.
    Inductive t (a : Set) : Set :=
    | Empty : t a
    | Invalid_encoding : t a
    | Value : a -> t a.
    Arguments Empty {_}.
    Arguments Invalid_encoding {_}.
    Arguments Value {_}.

    Definition parse {a : Set} (encoding : Data_encoding.t a)
      (value : option Context.value) : State.t a :=
      match value with
      | Some bytes =>
        match Data_encoding.Binary.of_bytes_opt encoding bytes with
        | Some value => State.Value value
        | None => State.Invalid_encoding
        end
      | None => State.Empty
      end.
  End State.

  Module Change.
    Inductive t (a : Set) : Set :=
    | Empty : t a
    | Value : a -> t a.
    Arguments Empty {_}.
    Arguments Value {_}.

    Definition apply {a : Set} (ctxt : Raw_context.t) (path : Context.key)
      (encoding : Data_encoding.t a) (change : t a) : Raw_context.t :=
      match change with
      | Empty => Raw_context.remove ctxt path
      | Value value =>
        let bytes :=
          match Data_encoding.Binary.to_bytes_opt None encoding value with
          | Some bytes => bytes
          | None => Bytes.empty
          end in
        Raw_context.add ctxt path bytes
      end.
  End Change.

  Definition reduce {a : Set} (_ : State.t a) (change : Change.t a)
    : State.t a :=
    match change with
    | Change.Empty => State.Empty
    | Change.Value value => State.Value value
    end.

  Module Op.
    Definition mem {a : Set} (state : State.t a) : bool :=
      match state with
      | State.Empty => false
      | State.Invalid_encoding | State.Value _ => true
      end.

    Definition get {a : Set} (absolute_key : key) (state : State.t a) : M? a :=
      match state with
      | State.Empty =>
        let error := Raw_context.Missing_key absolute_key Raw_context.Get in
        Raw_context.storage_error_value error
      | State.Invalid_encoding =>
        let error :=
          Build_extensible "Storage_error" Raw_context.storage_error
            (Raw_context.Corrupted_data absolute_key) in
        Error_monad.error_value error
      | State.Value value => return? value
      end.

    Definition find {a : Set} (absolute_key : key) (state : State.t a)
      : M? (option a) :=
      match state with
      | State.Empty => return? None
      | State.Invalid_encoding =>
        let error :=
          Build_extensible "Storage_error" Raw_context.storage_error
            (Raw_context.Corrupted_data absolute_key) in
        Error_monad.error_value error
      | State.Value value => return? Some value
      end.

    Definition init_value {a : Set} (absolute_key : key) (state : State.t a)
      (value : a) : M? (Change.t a) :=
      match state with
      | State.Empty => return? Change.Value value
      | State.Invalid_encoding | State.Value _ =>
        Raw_context.storage_error_value (Raw_context.Existing_key absolute_key)
      end.

    Definition update {a : Set} (absolute_key : key) (state : State.t a)
      (value : a) : M? (Change.t a) :=
      match state with
      | State.Empty =>
        Raw_context.storage_error_value
          (Raw_context.Missing_key absolute_key Raw_context._Set)
      | State.Invalid_encoding | State.Value _ => return? Change.Value value
      end.

    Definition add {a : Set} (state : State.t a)
      (value : a) : Change.t a :=
      Change.Value value.

    Definition add_or_remove {a : Set} (state : State.t a) (ovalue : option a)
      : Change.t a :=
      match ovalue with
      | Some value => Change.Value value
      | None => Change.Empty
      end.

    Definition remove_existing {a : Set} (absolute_key : key) (state : State.t a)
      : M? (Change.t a) :=
      match state with
      | State.Empty =>
        Raw_context.storage_error_value
          (Raw_context.Missing_key absolute_key Raw_context.Del)
      | State.Invalid_encoding | State.Value _ =>
        return? Change.Empty
      end.

    Definition remove {a : Set} (state : State.t a) : Change.t a :=
      Change.Empty.
  End Op.

  Definition Make {a : Set}
    (parse : Raw_context.t -> State.t a)
    (apply : Raw_context.t -> Change.t a -> Raw_context.t)
    (absolute_key : key)
    : Single_data_storage (t := Raw_context.t) (value := a) :=
    let apply_e ctxt change :=
      let? change := change in
      return? apply ctxt change in
    {|
      Single_data_storage.mem ctxt :=
        Op.mem (parse ctxt);
      Single_data_storage.get ctxt :=
        Op.get absolute_key (parse ctxt);
      Single_data_storage.find ctxt :=
        Op.find absolute_key (parse ctxt);
      Single_data_storage.init_value ctxt value :=
        apply_e ctxt
          (Op.init_value absolute_key (parse ctxt) value);
      Single_data_storage.update ctxt value :=
        apply_e ctxt
          (Op.update absolute_key (parse ctxt) value);
      Single_data_storage.add ctxt value :=
        let change := Op.add (parse ctxt) value in
        apply ctxt change;
      Single_data_storage.add_or_remove ctxt ovalue :=
        let change := Op.add_or_remove (parse ctxt) ovalue in
        apply ctxt change;
      Single_data_storage.remove_existing ctxt :=
        apply_e ctxt
          (Op.remove_existing absolute_key (parse ctxt));
      Single_data_storage.remove ctxt :=
        apply ctxt (Op.remove (parse ctxt));
    |}.
End Single_data_storage.

Module Data_set_storage.
  Module Eq.
    Import Proto_alpha.Storage_sigs.Data_set_storage.

    Record t {t elt : Set}
      {S1 S2 : Data_set_storage (t := t) (elt := elt)}
      : Prop := {
      mem ctxt value : S1.(mem) ctxt value = S2.(mem) ctxt value;
      add ctxt value : S1.(add) ctxt value = S2.(add) ctxt value;
      remove ctxt value : S1.(remove) ctxt value = S2.(remove) ctxt value;
      elements ctxt : S1.(elements) ctxt = S2.(elements) ctxt;
      clear ctxt : S1.(clear) ctxt = S2.(clear) ctxt;
    }.
    Arguments t {_ _}.
  End Eq.

  Module State.
    Definition _Set {elt : Set} (compare : elt -> elt -> int)
      : _Set.S (elt := elt) (t := _) :=
      let Ord := {| Compare.COMPARABLE.compare := compare |} in
      _Set.Make Ord.

    Definition t {elt : Set} (compare : elt -> elt -> int) : Set :=
      (_Set compare).(_Set.S.t).
  End State.

  Module Change.
    Inductive t (elt : Set) : Set :=
    | Add : elt -> t elt
    | Clear : t elt
    | Remove : elt -> t elt.
    Arguments Add {_}.
    Arguments Clear {_}.
    Arguments Remove {_}.
  End Change.

  Definition reduce {elt : Set} {compare : elt -> elt -> int}
    (state : State.t compare) (change : Change.t elt) : State.t compare :=
    match change with
    | Change.Add value => (State._Set compare).(_Set.S.add) value state
    | Change.Clear => (State._Set compare).(_Set.S.empty)
    | Change.Remove value => (State._Set compare).(_Set.S.remove) value state
    end.

  Module Op.
    Definition mem {elt : Set} {compare : elt -> elt -> int}
      (state : State.t compare) (value : elt) : bool :=
      (State._Set compare).(_Set.S.mem) value state.

    Definition add {elt : Set} (value : elt) : Change.t elt :=
      Change.Add value.

    Definition remove {elt : Set} (value : elt) : Change.t elt :=
      Change.Remove value.

    Definition elements {elt : Set} {compare : elt -> elt -> int}
      (state : State.t compare) : list elt :=
      (State._Set compare).(_Set.S.elements) state.

    Definition clear {elt : Set} : Change.t elt :=
      Change.Clear.
  End Op.

  Definition Make {elt : Set} {compare : elt -> elt -> int}
    (parse : Raw_context.t -> State.t compare)
    (apply : Raw_context.t -> Change.t elt -> Raw_context.t)
    : Data_set_storage (t := Raw_context.t) (elt := elt) :=
    {|
      Data_set_storage.mem ctxt value :=
        Op.mem (parse ctxt) value;
      Data_set_storage.add ctxt value :=
        apply ctxt (Op.add value);
      Data_set_storage.remove ctxt value :=
        apply ctxt (Op.remove value);
      Data_set_storage.elements ctxt :=
        Op.elements (parse ctxt);
      Data_set_storage.fold := axiom;
      Data_set_storage.clear ctxt :=
        apply ctxt Op.clear;
    |}.
End Data_set_storage.

Module Indexed_data_storage.
  Module Eq.
    Import Proto_alpha.Storage_sigs.Indexed_data_storage.

    Record t {t key value : Set}
      {S1 S2 : Indexed_data_storage (t := t) (key := key) (value := value)}
      : Prop := {
      find ctxt key : S1.(find) ctxt key = S2.(find) ctxt key;
      add ctxt key value : S1.(add) ctxt key value = S2.(add) ctxt key value;
    }.
    Arguments t {_ _ _}.
  End Eq.

  Module State.
    Definition Map {key : Set} (compare : key -> key -> int)
      : Map.S (key := key) (t := _) :=
      let Ord := {| Compare.COMPARABLE.compare := compare |} in
      Map.Make Ord.

    Definition t {key : Set} (compare : key -> key -> int) (value : Set)
      : Set :=
      (Map compare).(Map.S.t) value.

    Axiom parse : forall {key value : Set} (compare : key -> key -> int),
      Data_encoding.t key -> option Context.value -> State.t compare value.
  End State.

  Module Change.
    Inductive t (key value : Set) : Set :=
    | Add : key -> value -> t key value.
    Arguments Add {_ _}.
  End Change.

  Definition reduce {key value : Set} {compare : key -> key -> int}
    (state : State.t compare value) (change : Change.t key value)
    : State.t compare value :=
    match change with
    | Change.Add key value => (State.Map compare).(Map.S.add) key value state
    end.

  Module Op.
    Definition find {key value : Set} {compare : key -> key -> int}
      (state : State.t compare value) (k : key) : option value :=
      (State.Map compare).(Map.S.find) k state.

    Definition add {key value : Set} (k : key) (v : value)
      : Change.t key value :=
      Change.Add k v.
  End Op.

  Definition Make {key value : Set} {compare : key -> key -> int}
    (parse : Raw_context.t -> State.t compare value)
    (apply : Raw_context.t -> Change.t key value -> Raw_context.t)
    : Indexed_data_storage (t := Raw_context.t) (key := key) :=
    {|
      Indexed_data_storage.mem := axiom;
      Indexed_data_storage.get := axiom;
      Indexed_data_storage.find ctxt k :=
        return? Op.find (parse ctxt) k;
      Indexed_data_storage.update := axiom;
      Indexed_data_storage.init_value := axiom;
      Indexed_data_storage.add ctxt k v :=
        apply ctxt (Op.add k v);
      Indexed_data_storage.add_or_remove := axiom;
      Indexed_data_storage.remove_existing := axiom;
      Indexed_data_storage.remove := axiom;
      Indexed_data_storage.clear := axiom;
      Indexed_data_storage.keys := axiom;
      Indexed_data_storage.bindings := axiom;
      Indexed_data_storage.fold := axiom;
      Indexed_data_storage.fold_keys := axiom;
    |}.
End Indexed_data_storage.
