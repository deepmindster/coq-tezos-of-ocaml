Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Contract_services.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.RPC_service.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Signature.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Micheline.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Contract_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Michelson_v1_primitives.
Require TezosOfOCaml.Proto_alpha.Proofs.Tez_repr.

Lemma info_encoding_is_valid
  : Data_encoding.Valid.t (fun _ => True) Contract_services.info_encoding.
  Data_encoding.Valid.data_encoding_auto.
  intros x H; simpl; repeat split;
    match goal with
      [|- match ?x with | _ => _ end] => now destruct x
    end.
Qed.

Module S.
  Lemma balance_is_valid : RPC_service.Valid.t Contract_services.S.balance.
    RPC_service.rpc_auto.
  Qed.

  Lemma manager_key_is_valid
    : RPC_service.Valid.t Contract_services.S.manager_key.
    RPC_service.rpc_auto.
    now destruct x.
  Qed.

  Lemma delegate_is_valid : RPC_service.Valid.t Contract_services.S.delegate.
    RPC_service.rpc_auto.
  Qed.

  Lemma counter_is_valid : RPC_service.Valid.t Contract_services.S.counter.
    RPC_service.rpc_auto.
  Qed.

  Lemma script_is_valid : RPC_service.Valid.t Contract_services.S.script.
    RPC_service.rpc_auto.
  Qed.

  Lemma storage_value_is_valid
    : RPC_service.Valid.t Contract_services.S.storage_value.
    RPC_service.rpc_auto.
  Qed.

  Lemma entrypoint_type_is_valid
    : RPC_service.Valid.t Contract_services.S.entrypoint_type.
    RPC_service.rpc_auto.
  Qed.

  Lemma list_entrypoints_is_valid
    : RPC_service.Valid.t Contract_services.S.list_entrypoints.
    RPC_service.rpc_auto.
    intros x H. simpl. destruct x. split.
    { apply List.Forall_True. intro x. 
      apply List.Forall_True. constructor. }
    { apply List.Forall_True. constructor. }
  Qed.

  Lemma contract_big_map_get_opt_is_valid
    : RPC_service.Valid.t Contract_services.S.contract_big_map_get_opt.
    RPC_service.rpc_auto.
    destruct x; intuition.
  Qed.

  Lemma big_map_get_is_valid
    : RPC_service.Valid.t Contract_services.S.big_map_get.
    RPC_service.rpc_auto.
  Qed.

  Lemma big_map_get_all_is_valid
    : RPC_service.Valid.t Contract_services.S.big_map_get_all.
    RPC_service.rpc_auto.
    intros x H. apply List.Forall_True; intuition.
  Qed.

  Lemma info_value_is_valid
    : RPC_service.Valid.t Contract_services.S.info_value.
    RPC_service.rpc_auto.
    destruct x. repeat split;
    match goal with
      [|- match ?x with | _ => _ end] => destruct x; constructor
    end.
  Qed.

  Lemma list_value_is_valid
    : RPC_service.Valid.t Contract_services.S.list_value.
    RPC_service.rpc_auto.
    intros x H. apply List.Forall_True. intuition.
  Qed.
End S.
