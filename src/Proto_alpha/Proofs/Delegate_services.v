Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.Signature.
Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.List.
Require TezosOfOCaml.Proto_alpha.Delegate_services.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.RPC_service.
Require TezosOfOCaml.Proto_alpha.Proofs.Contract_hash.
Require TezosOfOCaml.Proto_alpha.Proofs.Contract_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Tez_repr.

Module S.
  Lemma info_encoding_is_valid
    : Data_encoding.Valid.t (fun _ => True) Delegate_services.info_encoding.
    Data_encoding.Valid.data_encoding_auto.
    intros []; repeat split; first [
      now destruct frozen_deposits_limit |
      now apply List.Forall_True
    ].
  Qed.
  #[global] Hint Resolve info_encoding_is_valid : Data_encoding_db.

  Lemma list_delegate_is_valid
    : RPC_service.Valid.t Delegate_services.S.list_delegate.
    constructor; trivial with Data_encoding_db.
    Data_encoding.Valid.data_encoding_auto.
    intros; now apply List.Forall_True.
  Qed.

  Lemma info_value_is_valid
    : RPC_service.Valid.t Delegate_services.S.info_value.
    constructor; trivial with Data_encoding_db.
  Qed.

  Lemma full_balance_is_valid
    : RPC_service.Valid.t Delegate_services.S.full_balance.
    constructor; trivial with Data_encoding_db.
  Qed.

  Lemma frozen_deposits_is_valid
    : RPC_service.Valid.t Delegate_services.S.frozen_deposits.
    constructor; trivial with Data_encoding_db.
  Qed.

  Lemma staking_balance_is_valid
    : RPC_service.Valid.t Delegate_services.S.staking_balance.
    constructor; trivial with Data_encoding_db.
  Qed.

  Lemma frozen_deposits_limit_is_valid
    : RPC_service.Valid.t Delegate_services.S.frozen_deposits_limit.
    constructor; trivial with Data_encoding_db.
      Data_encoding.Valid.data_encoding_auto.
     { intros []; intuition. }
  Qed.

  Lemma delegated_contracts_is_valid
    : RPC_service.Valid.t Delegate_services.S.delegated_contracts.
    constructor; trivial with Data_encoding_db.
    Data_encoding.Valid.data_encoding_auto.
    intros; now apply List.Forall_True.
  Qed.

  Lemma delegated_balance_is_valid
    : RPC_service.Valid.t Delegate_services.S.delegated_balance.
    constructor; trivial with Data_encoding_db.
  Qed.

  Lemma deactivated_is_valid
    : RPC_service.Valid.t Delegate_services.S.deactivated.
    constructor; trivial with Data_encoding_db.
  Qed.

  Lemma grace_period_is_valid
    : RPC_service.Valid.t Delegate_services.S.grace_period.
    constructor; trivial with Data_encoding_db.
  Qed.

  Lemma voting_power_is_valid
    : RPC_service.Valid.t Delegate_services.S.voting_power.
    constructor; trivial with Data_encoding_db.
  Qed.

  Lemma participation_is_valid
    : RPC_service.Valid.t Delegate_services.S.participation.
    constructor; trivial with Data_encoding_db.
    Data_encoding.Valid.data_encoding_auto.
  Qed.
End S.
