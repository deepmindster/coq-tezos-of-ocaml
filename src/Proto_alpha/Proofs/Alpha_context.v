Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Error_monad.
Require TezosOfOCaml.Proto_alpha.Proofs.Raw_context.
Require TezosOfOCaml.Proto_alpha.Proofs.Contract_hash.

Module Script.
  Lemma force_decode_in_context_gas_consume ctxt lexpr
    : Error_monad.post_when_success
        (Alpha_context.Script.force_decode_in_context
          Alpha_context.Script.When_needed ctxt lexpr)
        (fun '(_, ctxt') =>
          Raw_context.Consume_gas.Valid.t ctxt ctxt'
            [Script_repr.force_decode_cost lexpr]
        ).
    unfold Alpha_context.Script.force_decode_in_context.
    eapply Error_monad.post_when_success_bind;
      [apply Raw_context.Consume_gas.Valid.consume_gas|]; simpl.
    intros ctxt' H.
    now destruct (Script_repr.force_decode _).
  Qed.
End Script.

(* encoding_is_valid can be found at Proofs.Tez_repr. Alpha_context.Tez is a
 * symnonym Tez_repr, so all properties of Tez_repr applies to this one too. 
 *)
