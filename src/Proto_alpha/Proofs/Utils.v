Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Require Import Lia.
(* This is needed for lia work with rem and mod
   https://coq.inria.fr/refman/addendum/micromega.html#coq:tacn.zify *)
Ltac Zify.zify_post_hook ::= Z.to_euclidean_division_equations.

Require Import TezosOfOCaml.Proto_alpha.Environment.

(** Document the proof state by checking that a certain expression is there,
    either in the goal or the hypothesis. *)
Ltac step e :=
  match goal with
  | |- context [e] => idtac
  | _ : context [e] |- _ => idtac
  | _ => fail "expression not found in the current goal"
  end.

Ltac tezos_z_autounfold :=
  autounfold with tezos_z;
  unfold normalize, Pervasives.two_pow_63, Pervasives.two_pow_62.

Ltac tezos_z_auto :=
  tezos_z_autounfold; intros; lia.
