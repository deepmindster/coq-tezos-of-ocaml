Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Period_repr.
Require TezosOfOCaml.Proto_alpha.Slot_repr.
Require TezosOfOCaml.Proto_alpha.Time_repr.

Definition round : Set := int32.

Definition t : Set := round.

Definition Map :=
  Map.Make
    {|
      Compare.COMPARABLE.compare := Int32.compare
    |}.

(** Inclusion of the module [Compare.Int32] *)
Definition op_eq := Compare.Int32.(Compare.S.op_eq).

Definition op_ltgt := Compare.Int32.(Compare.S.op_ltgt).

Definition op_lt := Compare.Int32.(Compare.S.op_lt).

Definition op_lteq := Compare.Int32.(Compare.S.op_lteq).

Definition op_gteq := Compare.Int32.(Compare.S.op_gteq).

Definition op_gt := Compare.Int32.(Compare.S.op_gt).

Definition compare := Compare.Int32.(Compare.S.compare).

Definition equal := Compare.Int32.(Compare.S.equal).

Definition max := Compare.Int32.(Compare.S.max).

Definition min := Compare.Int32.(Compare.S.min).

Definition zero : int32 := 0.

Definition succ : int32 -> int32 := Int32.succ.

Definition pp (fmt : Format.formatter) (i_value : int32) : unit :=
  Format.fprintf fmt
    (CamlinternalFormatBasics.Format
      (CamlinternalFormatBasics.Int32 CamlinternalFormatBasics.Int_d
        CamlinternalFormatBasics.No_padding
        CamlinternalFormatBasics.No_precision
        CamlinternalFormatBasics.End_of_format) "%ld") i_value.

(** Init function; without side-effects in Coq *)
Definition init_module1 : unit :=
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent "negative_round"
      "Negative round" "Round cannot be built out of negative integers."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (i_value : int64) =>
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Negative round cannot be built out of negative integers ("
                  (CamlinternalFormatBasics.Int64 CamlinternalFormatBasics.Int_d
                    CamlinternalFormatBasics.No_padding
                    CamlinternalFormatBasics.No_precision
                    (CamlinternalFormatBasics.Char_literal ")" % char
                      CamlinternalFormatBasics.End_of_format)))
                "Negative round cannot be built out of negative integers (%Ld)")
              i_value))
      (Data_encoding.obj1
        (Data_encoding.req None None "Negative_round" Data_encoding.int64_value))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Negative_round" then
            let 'i_value := cast int payload in
            Some (Int64.of_int i_value)
          else None
        end)
      (fun (i_value : int64) =>
        Build_extensible "Negative_round" int (Int64.to_int i_value)) in
  Error_monad.register_error_kind Error_monad.Permanent "round_overflow"
    "Round overflow"
    "Round cannot be built out of integer greater than maximum int32 value."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (i_value : int64) =>
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal
                "Round cannot be built out of integer greater than maximum int32 value ("
                (CamlinternalFormatBasics.Int64 CamlinternalFormatBasics.Int_d
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.No_precision
                  (CamlinternalFormatBasics.Char_literal ")" % char
                    CamlinternalFormatBasics.End_of_format)))
              "Round cannot be built out of integer greater than maximum int32 value (%Ld)")
            i_value))
    (Data_encoding.obj1
      (Data_encoding.req None None "Negative_round" Data_encoding.int64_value))
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Round_overflow" then
          let 'i_value := cast int payload in
          Some (Int64.of_int i_value)
        else None
      end)
    (fun (i_value : int64) =>
      Build_extensible "Round_overflow" int (Int64.to_int i_value)).

Definition of_int32 (i_value : t) : M? t :=
  if op_gteq i_value 0 then
    Pervasives.Ok i_value
  else
    Error_monad.error_value
      (Build_extensible "Negative_round" int (Int32.to_int i_value)).

Definition pred (r_value : int32) : M? t :=
  let p_value := Int32.pred r_value in
  of_int32 p_value.

Definition of_int (i_value : int) : M? int32 :=
  if i_value <i 0 then
    Error_monad.error_value (Build_extensible "Negative_round" int i_value)
  else
    let i32 := Int32.of_int i_value in
    if (Int32.to_int i32) =i i_value then
      Pervasives.Ok i32
    else
      Error_monad.error_value (Build_extensible "Round_overflow" int i_value).

Definition to_int (i32 : Int32.t) : M? int :=
  let i_value := Int32.to_int i32 in
  if Int32.equal (Int32.of_int i_value) i32 then
    return? i_value
  else
    Error_monad.error_value (Build_extensible "Round_overflow" int i_value).

Definition to_int32 {A : Set} (t_value : A) : A := t_value.

Definition to_slot (round : Int32.t) (committee_size : int) : M? Slot_repr.t :=
  let? r_value := to_int round in
  let slot := Pervasives._mod r_value committee_size in
  return? (Slot_repr.of_int_exn slot).

Definition encoding : Data_encoding.encoding t :=
  Data_encoding.conv_with_guard (fun (i_value : t) => i_value)
    (fun (i_value : t) =>
      match of_int32 i_value with
      | Pervasives.Ok round => Pervasives.Ok round
      | Pervasives.Error _ =>
        Pervasives.Error "Round_repr.encoding: negative round"
      end) None Data_encoding.int32_value.

Module Durations.
  Module Non_increasing_rounds.
    Record record : Set := Build {
      round : Period_repr.t;
      next_round : Period_repr.t }.
    Definition with_round round (r : record) :=
      Build round r.(next_round).
    Definition with_next_round next_round (r : record) :=
      Build r.(round) next_round.
  End Non_increasing_rounds.
  Definition Non_increasing_rounds := Non_increasing_rounds.record.
  
  Module Round_durations_must_be_at_least_one_second.
    Record record : Set := Build {
      round : Period_repr.t }.
    Definition with_round round (r : record) :=
      Build round.
  End Round_durations_must_be_at_least_one_second.
  Definition Round_durations_must_be_at_least_one_second :=
    Round_durations_must_be_at_least_one_second.record.
  
  (** Init function; without side-effects in Coq *)
  Definition init_module2 : unit :=
    Error_monad.register_error_kind Error_monad.Permanent
      "durations.non_increasing_rounds" "Non increasing round"
      "The provided rounds are not increasing."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : Period_repr.period * Period_repr.period) =>
            let '(round, next_round) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "The provided rounds are not increasing (round: "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal ", next round: "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.Char_literal ")" % char
                          CamlinternalFormatBasics.End_of_format)))))
                "The provided rounds are not increasing (round: %a, next round: %a)")
              Period_repr.pp round Period_repr.pp next_round))
      (Data_encoding.obj2
        (Data_encoding.req None None "round" Period_repr.encoding)
        (Data_encoding.req None None "next_round" Period_repr.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Non_increasing_rounds" then
            let '{|
              Non_increasing_rounds.round := round;
                Non_increasing_rounds.next_round := next_round
                |} := cast Non_increasing_rounds payload in
            Some (round, next_round)
          else None
        end)
      (fun (function_parameter : Period_repr.period * Period_repr.period) =>
        let '(round, next_round) := function_parameter in
        Build_extensible "Non_increasing_rounds" Non_increasing_rounds
          {| Non_increasing_rounds.round := round;
            Non_increasing_rounds.next_round := next_round |}).
  
  Module t.
    Record record : Set := Build {
      round0 : Period_repr.t;
      round1 : Period_repr.t;
      other_rounds : list Period_repr.t }.
    Definition with_round0 round0 (r : record) :=
      Build round0 r.(round1) r.(other_rounds).
    Definition with_round1 round1 (r : record) :=
      Build r.(round0) round1 r.(other_rounds).
    Definition with_other_rounds other_rounds (r : record) :=
      Build r.(round0) r.(round1) other_rounds.
  End t.
  Definition t := t.record.
  
  Definition pp (fmt : Format.formatter) (t_value : t) : unit :=
    Format.fprintf fmt
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.Alpha
          (CamlinternalFormatBasics.Char_literal "," % char
            (CamlinternalFormatBasics.Formatting_lit
              (CamlinternalFormatBasics.Break "@ " 1 0)
              (CamlinternalFormatBasics.Alpha
                (CamlinternalFormatBasics.Char_literal "," % char
                  (CamlinternalFormatBasics.Formatting_lit
                    (CamlinternalFormatBasics.Break "@ " 1 0)
                    (CamlinternalFormatBasics.Alpha
                      CamlinternalFormatBasics.End_of_format)))))))
        "%a,@ %a,@ %a") Period_repr.pp t_value.(t.round0) Period_repr.pp
      t_value.(t.round1)
      (Format.pp_print_list
        (Some
          (fun (fmt : Format.formatter) =>
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              Format.fprintf fmt
                (CamlinternalFormatBasics.Format
                  (CamlinternalFormatBasics.Char_literal "," % char
                    (CamlinternalFormatBasics.Formatting_lit
                      (CamlinternalFormatBasics.Break "@ " 1 0)
                      CamlinternalFormatBasics.End_of_format)) ",@ ")))
        Period_repr.pp) t_value.(t.other_rounds).
  
  Fixpoint check_ordered (function_parameter : list Period_repr.t) : M? unit :=
    match function_parameter with
    | (cons _ [] | []) => Result.return_unit
    | cons r0 ((cons r1 _) as rs) =>
      let? '_ :=
        Error_monad.error_when (Period_repr.op_gt r0 r1)
          (Build_extensible "Non_increasing_rounds" Non_increasing_rounds
            {| Non_increasing_rounds.round := r0;
              Non_increasing_rounds.next_round := r1 |}) in
      check_ordered rs
    end.
  
  Definition create (op_staroptstar : option (list Period_repr.t))
    : Period_repr.period -> Period_repr.t -> unit -> M? t :=
    let other_rounds :=
      match op_staroptstar with
      | Some op_starsthstar => op_starsthstar
      | None => nil
      end in
    fun (round0 : Period_repr.period) =>
      fun (round1 : Period_repr.t) =>
        fun (function_parameter : unit) =>
          let '_ := function_parameter in
          let? '_ :=
            Error_monad.error_when ((Period_repr.to_seconds round0) <i64 1)
              (Build_extensible "Round_durations_must_be_at_least_one_second"
                Round_durations_must_be_at_least_one_second
                {| Round_durations_must_be_at_least_one_second.round := round0
                  |}) in
          let? '_ := check_ordered (cons round0 (cons round1 other_rounds)) in
          return?
            {| t.round0 := round0; t.round1 := round1;
              t.other_rounds := other_rounds |}.
  
  Definition create_opt
    (other_rounds : option (list Period_repr.t)) (round0 : Period_repr.period)
    (round1 : Period_repr.t) (function_parameter : unit) : option t :=
    let '_ := function_parameter in
    match create other_rounds round0 round1 tt with
    | Pervasives.Ok v_value => Some v_value
    | Pervasives.Error _ => None
    end.
  
  Definition encoding : Data_encoding.encoding t :=
    Data_encoding.conv_with_guard
      (fun (function_parameter : t) =>
        let '{|
          t.round0 := round0;
            t.round1 := round1;
            t.other_rounds := other_rounds
            |} := function_parameter in
        (round0, round1,
          match other_rounds with
          | [] => None
          | cons _ _ => Some other_rounds
          end))
      (fun (function_parameter :
        Period_repr.t * Period_repr.t * option (list Period_repr.t)) =>
        let '(round0, round1, other_rounds) := function_parameter in
        match create_opt other_rounds round0 round1 tt with
        | None =>
          Pervasives.Error "The provided round durations are not increasing."
        | Some rounds => Pervasives.Ok rounds
        end) None
      (Data_encoding.obj3
        (Data_encoding.req None None "round0" Period_repr.encoding)
        (Data_encoding.req None None "round1" Period_repr.encoding)
        (Data_encoding.opt None None "other_rounds"
          (Data_encoding.list_value None Period_repr.encoding))).
  
  Definition round_duration (function_parameter : t) : int32 -> Period_repr.t :=
    let '{|
      t.round0 := round0;
        t.round1 := round1;
        t.other_rounds := other_rounds
        |} := function_parameter in
    fun (round : int32) =>
      let '_ :=
        (* ❌ Assert instruction is not handled. *)
        assert unit (round >=i32 0) in
      if round =i32 0 then
        round0
      else
        if round =i32 1 then
          round1
        else
          let fix loop
            (i_value : int32) (ultimate : Period_repr.period)
            (penultimate : Period_repr.period)
            (function_parameter : list Period_repr.period)
            : Period_repr.period :=
            match function_parameter with
            | cons d_value ds =>
              if i_value =i32 0 then
                d_value
              else
                loop (Int32.pred i_value) d_value ultimate ds
            | [] =>
              let last := Period_repr.to_seconds ultimate in
              let last_but_one := Period_repr.to_seconds penultimate in
              let diff_value := last -i64 last_but_one in
              let '_ :=
                (* ❌ Assert instruction is not handled. *)
                assert unit (diff_value >=i64 0) in
              let offset := Int32.succ i_value in
              let duration :=
                last +i64 (diff_value *i64 (Int64.of_int32 offset)) in
              Period_repr.of_seconds_exn duration
            end in
          loop (round -i32 2) round1 round0 other_rounds.
  
  Definition first (function_parameter : t) : Period_repr.t :=
    let '{| t.round0 := round0 |} := function_parameter in
    round0.
  
  Definition length (function_parameter : t) : int :=
    let '{| t.other_rounds := other_rounds |} := function_parameter in
    (List.length other_rounds) +i 2.
End Durations.

(** Init function; without side-effects in Coq *)
Definition init_module3 : unit :=
  Error_monad.register_error_kind Error_monad.Permanent "round_too_high"
    "round too high" "The block's round is too high."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (round : int32) =>
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal
                "The block's round is too high: "
                (CamlinternalFormatBasics.Int32 CamlinternalFormatBasics.Int_d
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.No_precision
                  CamlinternalFormatBasics.End_of_format))
              "The block's round is too high: %ld") round))
    (Data_encoding.obj1
      (Data_encoding.req None None "level_offset_too_high"
        Data_encoding.int32_value))
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Round_too_high" then
          let 'round := cast int32 payload in
          Some round
        else None
      end)
    (fun (round : int32) => Build_extensible "Round_too_high" int32 round).

(** [raw_level_offset_of_round round] returns the time period between the
    start of round 0 and the start of round [round]. That is, the sum
    of the duration of rounds [0] to [round-1]. Note that [round] is
    necessarily a positive number. *)
Definition raw_level_offset_of_round (round_durations : Durations.t) (round : t)
  : M? int64 :=
  let last_and_sum_loop (round_durations : Durations.t) (round : int32)
    : Period_repr.t * int64 :=
    if round =i32 Int32.zero then
      (round_durations.(Durations.t.round0), Int64.zero)
    else
      let last : Period_repr.t :=
        round_durations.(Durations.t.round1)
      in let sum_acc : int64 :=
        Period_repr.to_seconds round_durations.(Durations.t.round0) in
      let fix loop
        (round_durations : list Period_repr.period) (round : int32)
        (last : Period_repr.period) (sum_acc : int64)
        : Period_repr.period * int64 :=
        if round =i32 Int32.zero then
          (last, sum_acc)
        else
          let sum_acc := sum_acc +i64 (Period_repr.to_seconds last) in
          match round_durations with
          | [] => (last, sum_acc)
          | cons d_value round_durations' =>
            loop round_durations' (Int32.pred round) d_value sum_acc
          end in
      loop round_durations.(Durations.t.other_rounds) (Int32.pred round) last
        sum_acc in
  let parameters_len := Int32.of_int (Durations.length round_durations) in
  if op_lteq round parameters_len then
    return? (Pervasives.snd (last_and_sum_loop round_durations round))
  else
    let '(round_durations_last, sum_round_durations) :=
      last_and_sum_loop round_durations (Int32.pred parameters_len) in
    let sum_after_round_durations :=
      let round_durations_last : int64 :=
        Period_repr.to_seconds round_durations_last
      in let after_round_durations_last : int64 :=
        Period_repr.to_seconds
          (Durations.round_duration round_durations (Int32.pred round)) in
      ((round_durations_last +i64 after_round_durations_last) *i64
      (Int64.succ ((Int64.of_int32 round) -i64 (Int64.of_int32 parameters_len))))
      /i64 (Int64.of_int 2) in
    if sum_after_round_durations <i64 0 then
      Error_monad.error_value (Build_extensible "Round_too_high" t round)
    else
      return? (sum_round_durations +i64 sum_after_round_durations).

(** Init function; without side-effects in Coq *)
Definition init_module4 : unit :=
  Error_monad.register_error_kind Error_monad.Permanent "level_offset_too_high"
    "level offset too high" "The block's level offset is too high."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (offset : Period_repr.period) =>
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal
                "The block's level offset is too high: "
                (CamlinternalFormatBasics.Alpha
                  CamlinternalFormatBasics.End_of_format))
              "The block's level offset is too high: %a") Period_repr.pp offset))
    (Data_encoding.obj1
      (Data_encoding.req None None "level_offset_too_high" Period_repr.encoding))
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Level_offset_too_high" then
          let 'offset := cast Period_repr.t payload in
          Some offset
        else None
      end)
    (fun (offset : Period_repr.period) =>
      Build_extensible "Level_offset_too_high" Period_repr.period offset).

Module round_and_offset.
  Record record : Set := Build {
    round : int32;
    offset : Period_repr.t }.
  Definition with_round round (r : record) :=
    Build round r.(offset).
  Definition with_offset offset (r : record) :=
    Build r.(round) offset.
End round_and_offset.
Definition round_and_offset := round_and_offset.record.

(** Complexity: in the worst case, O(log max_int)*O(|round_durations|^2).
    Normally, [level_offset] is small enough for [check_first] to
    return the searched round, thus the binary search will not be performed.
    [check_first] can be made to run in O(round_duration) but it is kept
    this way for simplicity given that currently |round_durations| = 2. *)
Definition round_and_offset_value
  (round_durations : Durations.t) (level_offset : Period_repr.period)
  : M? round_and_offset :=
  let level_offset_in_seconds := Period_repr.to_seconds level_offset in
  let fix check_first (max_round : int) (round : t) {struct round}
    : M? (option round_and_offset) :=
    if (Int32.to_int round) >=i max_round then
      return? None
    else
      let? next_level_offset :=
        raw_level_offset_of_round round_durations (Int32.succ round) in
      if level_offset_in_seconds <i64 next_level_offset then
        let? current_level_offset :=
          raw_level_offset_of_round round_durations round in
        return?
          (Some
            {| round_and_offset.round := round;
              round_and_offset.offset :=
                Period_repr.of_seconds_exn
                  ((Period_repr.to_seconds level_offset) -i64
                  current_level_offset) |})
      else
        check_first max_round (Int32.succ round) in
  let right_bound :=
    if level_offset_in_seconds <i64 (Int64.of_int32 Int32.max_int) then
      Int32.of_int ((Int64.to_int level_offset_in_seconds) +i 1)
    else
      Int32.max_int in
  let fix bin_search (min_r : int32) (max_r : int32) {struct max_r}
    : M? round_and_offset :=
    if min_r >=i32 right_bound then
      Error_monad.error_value
        (Build_extensible "Level_offset_too_high" Period_repr.period
          level_offset)
    else
      let round := min_r +i32 ((max_r -i32 min_r) /i32 2) in
      let? next_level_offset :=
        raw_level_offset_of_round round_durations (Int32.succ round) in
      if (Period_repr.to_seconds level_offset) >=i64 next_level_offset then
        bin_search (Int32.succ round) max_r
      else
        let? current_level_offset :=
          raw_level_offset_of_round round_durations round in
        if
          (Period_repr.to_seconds level_offset) <i64 current_level_offset
        then
          bin_search min_r round
        else
          return?
            {| round_and_offset.round := round;
              round_and_offset.offset :=
                Period_repr.of_seconds_exn
                  ((Period_repr.to_seconds level_offset) -i64
                  current_level_offset) |} in
  let n_value := Durations.length round_durations in
  let? res := check_first n_value 0 in
  match res with
  | Some result_value => return? result_value
  | None => bin_search (Int32.of_int n_value) right_bound
  end.

(** Complexity: O(|round_durations|). *)
Definition timestamp_of_round
  (round_durations : Durations.t) (predecessor_timestamp : Time_repr.time)
  (predecessor_round : int32) (round : t) : M? Time_repr.time :=
  let pred_round_duration :=
    Durations.round_duration round_durations predecessor_round in
  let? start_of_current_level :=
    Time_repr.op_plusquestion predecessor_timestamp pred_round_duration in
  let? level_offset := raw_level_offset_of_round round_durations round in
  let level_offset := Period_repr.of_seconds_exn level_offset in
  Time_repr.op_plusquestion start_of_current_level level_offset.

(** Unlike [timestamp_of_round], this function gets the starting time
    of a given round, given the timestamp and the round of a proposal
    at the same level.

    We compute the starting time of [considered_round] from a given
    [round_durations] description, some [current_round], and its
    starting time [current_timestamp].

    Complexity: O(|round_durations|). *)
Definition timestamp_of_another_round_same_level
  (round_durations : Durations.t) (current_timestamp : Time_repr.t)
  (current_round : t) (considered_round : t) : M? Time_repr.t :=
  let? target_offset :=
    raw_level_offset_of_round round_durations considered_round in
  let? current_offset := raw_level_offset_of_round round_durations current_round
    in
  return?
    (Time_repr.of_seconds
      (((Time_repr.to_seconds current_timestamp) -i64 current_offset) +i64
      target_offset)).

Module Round_of_past_timestamp.
  Record record : Set := Build {
    provided_timestamp : Time.t;
    predecessor_timestamp : Time.t;
    predecessor_round : t }.
  Definition with_provided_timestamp provided_timestamp (r : record) :=
    Build provided_timestamp r.(predecessor_timestamp) r.(predecessor_round).
  Definition with_predecessor_timestamp predecessor_timestamp (r : record) :=
    Build r.(provided_timestamp) predecessor_timestamp r.(predecessor_round).
  Definition with_predecessor_round predecessor_round (r : record) :=
    Build r.(provided_timestamp) r.(predecessor_timestamp) predecessor_round.
End Round_of_past_timestamp.
Definition Round_of_past_timestamp := Round_of_past_timestamp.record.

(** Init function; without side-effects in Coq *)
Definition init_module5 : unit :=
  Error_monad.register_error_kind Error_monad.Permanent
    "round_of_past_timestamp" "Round_of_timestamp for past timestamp"
    "Provided timestamp is before the expected level start."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (function_parameter : Time.t * Time.t * t) =>
          let '(provided_ts, predecessor_ts, round) := function_parameter in
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal "Provided timestamp ("
                (CamlinternalFormatBasics.Alpha
                  (CamlinternalFormatBasics.String_literal
                    ") is before the expected level start (computed based on predecessor_ts "
                    (CamlinternalFormatBasics.Alpha
                      (CamlinternalFormatBasics.String_literal " at round "
                        (CamlinternalFormatBasics.Alpha
                          (CamlinternalFormatBasics.String_literal ")."
                            CamlinternalFormatBasics.End_of_format)))))))
              "Provided timestamp (%a) is before the expected level start (computed based on predecessor_ts %a at round %a).")
            Time.pp_hum provided_ts Time.pp_hum predecessor_ts pp round))
    (Data_encoding.obj3
      (Data_encoding.req None None "provided_timestamp" Time.encoding)
      (Data_encoding.req None None "predecessor_timestamp" Time.encoding)
      (Data_encoding.req None None "predecessor_round" encoding))
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Round_of_past_timestamp" then
          let '{|
            Round_of_past_timestamp.provided_timestamp := provided_timestamp;
              Round_of_past_timestamp.predecessor_timestamp :=
                predecessor_timestamp;
              Round_of_past_timestamp.predecessor_round := predecessor_round
              |} := cast Round_of_past_timestamp payload in
          Some (provided_timestamp, predecessor_timestamp, predecessor_round)
        else None
      end)
    (fun (function_parameter : Time.t * Time.t * t) =>
      let '(provided_timestamp, predecessor_timestamp, predecessor_round) :=
        function_parameter in
      Build_extensible "Round_of_past_timestamp" Round_of_past_timestamp
        {| Round_of_past_timestamp.provided_timestamp := provided_timestamp;
          Round_of_past_timestamp.predecessor_timestamp := predecessor_timestamp;
          Round_of_past_timestamp.predecessor_round := predecessor_round |}).

Definition round_of_timestamp
  (round_durations : Durations.t) (predecessor_timestamp : Time_repr.time)
  (predecessor_round : int32) (timestamp : Time_repr.t) : M? int32 :=
  let round_duration :=
    Durations.round_duration round_durations predecessor_round in
  let? start_of_current_level :=
    Time_repr.op_plusquestion predecessor_timestamp round_duration in
  let? diff_value :=
    Error_monad.record_trace
      (Build_extensible "Round_of_past_timestamp" Round_of_past_timestamp
        {| Round_of_past_timestamp.provided_timestamp := timestamp;
          Round_of_past_timestamp.predecessor_timestamp := predecessor_timestamp;
          Round_of_past_timestamp.predecessor_round := predecessor_round |})
      (Period_repr.of_seconds
        (Time_repr.diff_value timestamp start_of_current_level)) in
  let? round_and_offset_value :=
    round_and_offset_value round_durations diff_value in
  return? round_and_offset_value.(round_and_offset.round).

Definition level_offset_of_round (round_durations : Durations.t) (round : t)
  : M? Period_repr.period :=
  let? offset := raw_level_offset_of_round round_durations round in
  return? (Period_repr.of_seconds_exn offset).
