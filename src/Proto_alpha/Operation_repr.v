Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Blinded_public_key_hash.
Require TezosOfOCaml.Proto_alpha.Block_header_repr.
Require TezosOfOCaml.Proto_alpha.Block_payload_hash.
Require TezosOfOCaml.Proto_alpha.Cache_memory_helpers.
Require TezosOfOCaml.Proto_alpha.Contract_repr.
Require TezosOfOCaml.Proto_alpha.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Michelson_v1_primitives.
Require TezosOfOCaml.Proto_alpha.Raw_level_repr.
Require TezosOfOCaml.Proto_alpha.Round_repr.
Require TezosOfOCaml.Proto_alpha.Saturation_repr.
Require TezosOfOCaml.Proto_alpha.Script_repr.
Require TezosOfOCaml.Proto_alpha.Seed_repr.
Require TezosOfOCaml.Proto_alpha.Slot_repr.
Require TezosOfOCaml.Proto_alpha.Tez_repr.
Require TezosOfOCaml.Proto_alpha.Vote_repr.

Module Kind.
  Inductive preendorsement_consensus_kind : Set :=
  | Preendorsement_consensus_kind : preendorsement_consensus_kind.
  
  Inductive endorsement_consensus_kind : Set :=
  | Endorsement_consensus_kind : endorsement_consensus_kind.
  
  Inductive consensus : Set :=
  | Preendorsement_kind : consensus
  | Endorsement_kind : consensus.
  
  Definition preendorsement : Set := consensus.
  
  Definition endorsement : Set := consensus.
  
  Inductive seed_nonce_revelation : Set :=
  | Seed_nonce_revelation_kind : seed_nonce_revelation.
  
  Inductive double_consensus_operation_evidence (a : Set) : Set :=
  | Double_consensus_operation_evidence : double_consensus_operation_evidence a.
  
  Arguments Double_consensus_operation_evidence {_}.
  
  Definition double_endorsement_evidence : Set :=
    double_consensus_operation_evidence endorsement_consensus_kind.
  
  Definition double_preendorsement_evidence : Set :=
    double_consensus_operation_evidence preendorsement_consensus_kind.
  
  Inductive double_baking_evidence : Set :=
  | Double_baking_evidence_kind : double_baking_evidence.
  
  Inductive activate_account : Set :=
  | Activate_account_kind : activate_account.
  
  Inductive proposals : Set :=
  | Proposals_kind : proposals.
  
  Inductive ballot : Set :=
  | Ballot_kind : ballot.
  
  Inductive reveal : Set :=
  | Reveal_kind : reveal.
  
  Inductive transaction : Set :=
  | Transaction_kind : transaction.
  
  Inductive origination : Set :=
  | Origination_kind : origination.
  
  Inductive delegation : Set :=
  | Delegation_kind : delegation.
  
  Inductive set_deposits_limit : Set :=
  | Set_deposits_limit_kind : set_deposits_limit.
  
  Inductive failing_noop : Set :=
  | Failing_noop_kind : failing_noop.
  
  Inductive register_global_constant : Set :=
  | Register_global_constant_kind : register_global_constant.
  
  Inductive manager : Set :=
  | Reveal_manager_kind : manager
  | Transaction_manager_kind : manager
  | Origination_manager_kind : manager
  | Delegation_manager_kind : manager
  | Register_global_constant_manager_kind : manager
  | Set_deposits_limit_manager_kind : manager.
End Kind.

Module Consensus_operation_type.
  Inductive t : Set :=
  | Endorsement : t
  | Preendorsement : t.
  
  Definition pp (ppf : Format.formatter) (operation_kind : t) : unit :=
    match operation_kind with
    | Endorsement =>
      Format.fprintf ppf
        (CamlinternalFormatBasics.Format
          (CamlinternalFormatBasics.String_literal "Endorsement"
            CamlinternalFormatBasics.End_of_format) "Endorsement")
    | Preendorsement =>
      Format.fprintf ppf
        (CamlinternalFormatBasics.Format
          (CamlinternalFormatBasics.String_literal "Preendorsement"
            CamlinternalFormatBasics.End_of_format) "Preendorsement")
    end.
End Consensus_operation_type.

Module consensus_content.
  Record record : Set := Build {
    slot : Slot_repr.t;
    level : Raw_level_repr.t;
    round : Round_repr.t;
    block_payload_hash : Block_payload_hash.t }.
  Definition with_slot slot (r : record) :=
    Build slot r.(level) r.(round) r.(block_payload_hash).
  Definition with_level level (r : record) :=
    Build r.(slot) level r.(round) r.(block_payload_hash).
  Definition with_round round (r : record) :=
    Build r.(slot) r.(level) round r.(block_payload_hash).
  Definition with_block_payload_hash block_payload_hash (r : record) :=
    Build r.(slot) r.(level) r.(round) block_payload_hash.
End consensus_content.
Definition consensus_content := consensus_content.record.

Definition consensus_content_encoding
  : Data_encoding.encoding consensus_content :=
  Data_encoding.conv
    (fun (function_parameter : consensus_content) =>
      let '{|
        consensus_content.slot := slot;
          consensus_content.level := level;
          consensus_content.round := round;
          consensus_content.block_payload_hash := block_payload_hash
          |} := function_parameter in
      (slot, level, round, block_payload_hash))
    (fun (function_parameter :
      Slot_repr.t * Raw_level_repr.t * Round_repr.t * Block_payload_hash.t) =>
      let '(slot, level, round, block_payload_hash) := function_parameter in
      {| consensus_content.slot := slot; consensus_content.level := level;
        consensus_content.round := round;
        consensus_content.block_payload_hash := block_payload_hash |}) None
    (Data_encoding.obj4 (Data_encoding.req None None "slot" Slot_repr.encoding)
      (Data_encoding.req None None "level" Raw_level_repr.encoding)
      (Data_encoding.req None None "round" Round_repr.encoding)
      (Data_encoding.req None None "block_payload_hash"
        Block_payload_hash.encoding)).

Definition pp_consensus_content
  (ppf : Format.formatter) (content : consensus_content) : unit :=
  Format.fprintf ppf
    (CamlinternalFormatBasics.Format
      (CamlinternalFormatBasics.Char_literal "(" % char
        (CamlinternalFormatBasics.Int32 CamlinternalFormatBasics.Int_d
          CamlinternalFormatBasics.No_padding
          CamlinternalFormatBasics.No_precision
          (CamlinternalFormatBasics.String_literal ", "
            (CamlinternalFormatBasics.Alpha
              (CamlinternalFormatBasics.String_literal ", "
                (CamlinternalFormatBasics.Alpha
                  (CamlinternalFormatBasics.String_literal ", "
                    (CamlinternalFormatBasics.Alpha
                      (CamlinternalFormatBasics.Char_literal ")" % char
                        CamlinternalFormatBasics.End_of_format)))))))))
      "(%ld, %a, %a, %a)")
    (Raw_level_repr.to_int32 content.(consensus_content.level)) Round_repr.pp
    content.(consensus_content.round) Slot_repr.pp
    content.(consensus_content.slot) Block_payload_hash.pp_short
    content.(consensus_content.block_payload_hash).

Module Consensus_watermark.
  Inductive t : Set :=
  | Endorsement : Chain_id.t -> t
  | Preendorsement : Chain_id.t -> t.
End Consensus_watermark.

Definition bytes_of_consensus_watermark
  (function_parameter : Consensus_watermark.t) : bytes :=
  match function_parameter with
  | Consensus_watermark.Preendorsement chain_id =>
    Bytes.cat (Bytes.of_string "\018") (Chain_id.to_bytes chain_id)
  | Consensus_watermark.Endorsement chain_id =>
    Bytes.cat (Bytes.of_string "\019") (Chain_id.to_bytes chain_id)
  end.

Definition to_watermark (w_value : Consensus_watermark.t)
  : Signature.watermark :=
  Signature.Custom (bytes_of_consensus_watermark w_value).

Definition of_watermark (function_parameter : Signature.watermark)
  : option Consensus_watermark.t :=
  match function_parameter with
  | Signature.Custom b_value =>
    if (Bytes.length b_value) >i 0 then
      match Bytes.get b_value 0 with
      | "018" % char =>
        Option.map
          (fun (chain_id : Chain_id.t) =>
            Consensus_watermark.Endorsement chain_id)
          (Chain_id.of_bytes_opt
            (Bytes.sub b_value 1 ((Bytes.length b_value) -i 1)))
      | "019" % char =>
        Option.map
          (fun (chain_id : Chain_id.t) =>
            Consensus_watermark.Preendorsement chain_id)
          (Chain_id.of_bytes_opt
            (Bytes.sub b_value 1 ((Bytes.length b_value) -i 1)))
      | _ => None
      end
    else
      None
  | _ => None
  end.

Definition raw : Set := Operation.t.

Definition raw_encoding : Data_encoding.t Operation.t := Operation.encoding.

(** Records for the constructor parameters *)
Module ConstructorRecords_contents_manager_operation.
  Module contents.
    Module Seed_nonce_revelation.
      Record record {level nonce : Set} : Set := Build {
        level : level;
        nonce : nonce }.
      Arguments record : clear implicits.
      Definition with_level {t_level t_nonce} level
        (r : record t_level t_nonce) :=
        Build t_level t_nonce level r.(nonce).
      Definition with_nonce {t_level t_nonce} nonce
        (r : record t_level t_nonce) :=
        Build t_level t_nonce r.(level) nonce.
    End Seed_nonce_revelation.
    Definition Seed_nonce_revelation_skeleton := Seed_nonce_revelation.record.
    
    Module Double_preendorsement_evidence.
      Record record {op1 op2 : Set} : Set := Build {
        op1 : op1;
        op2 : op2 }.
      Arguments record : clear implicits.
      Definition with_op1 {t_op1 t_op2} op1 (r : record t_op1 t_op2) :=
        Build t_op1 t_op2 op1 r.(op2).
      Definition with_op2 {t_op1 t_op2} op2 (r : record t_op1 t_op2) :=
        Build t_op1 t_op2 r.(op1) op2.
    End Double_preendorsement_evidence.
    Definition Double_preendorsement_evidence_skeleton :=
      Double_preendorsement_evidence.record.
    
    Module Double_endorsement_evidence.
      Record record {op1 op2 : Set} : Set := Build {
        op1 : op1;
        op2 : op2 }.
      Arguments record : clear implicits.
      Definition with_op1 {t_op1 t_op2} op1 (r : record t_op1 t_op2) :=
        Build t_op1 t_op2 op1 r.(op2).
      Definition with_op2 {t_op1 t_op2} op2 (r : record t_op1 t_op2) :=
        Build t_op1 t_op2 r.(op1) op2.
    End Double_endorsement_evidence.
    Definition Double_endorsement_evidence_skeleton :=
      Double_endorsement_evidence.record.
    
    Module Double_baking_evidence.
      Record record {bh1 bh2 : Set} : Set := Build {
        bh1 : bh1;
        bh2 : bh2 }.
      Arguments record : clear implicits.
      Definition with_bh1 {t_bh1 t_bh2} bh1 (r : record t_bh1 t_bh2) :=
        Build t_bh1 t_bh2 bh1 r.(bh2).
      Definition with_bh2 {t_bh1 t_bh2} bh2 (r : record t_bh1 t_bh2) :=
        Build t_bh1 t_bh2 r.(bh1) bh2.
    End Double_baking_evidence.
    Definition Double_baking_evidence_skeleton := Double_baking_evidence.record.
    
    Module Activate_account.
      Record record {id activation_code : Set} : Set := Build {
        id : id;
        activation_code : activation_code }.
      Arguments record : clear implicits.
      Definition with_id {t_id t_activation_code} id
        (r : record t_id t_activation_code) :=
        Build t_id t_activation_code id r.(activation_code).
      Definition with_activation_code {t_id t_activation_code} activation_code
        (r : record t_id t_activation_code) :=
        Build t_id t_activation_code r.(id) activation_code.
    End Activate_account.
    Definition Activate_account_skeleton := Activate_account.record.
    
    Module Proposals.
      Record record {source period proposals : Set} : Set := Build {
        source : source;
        period : period;
        proposals : proposals }.
      Arguments record : clear implicits.
      Definition with_source {t_source t_period t_proposals} source
        (r : record t_source t_period t_proposals) :=
        Build t_source t_period t_proposals source r.(period) r.(proposals).
      Definition with_period {t_source t_period t_proposals} period
        (r : record t_source t_period t_proposals) :=
        Build t_source t_period t_proposals r.(source) period r.(proposals).
      Definition with_proposals {t_source t_period t_proposals} proposals
        (r : record t_source t_period t_proposals) :=
        Build t_source t_period t_proposals r.(source) r.(period) proposals.
    End Proposals.
    Definition Proposals_skeleton := Proposals.record.
    
    Module Ballot.
      Record record {source period proposal ballot : Set} : Set := Build {
        source : source;
        period : period;
        proposal : proposal;
        ballot : ballot }.
      Arguments record : clear implicits.
      Definition with_source {t_source t_period t_proposal t_ballot} source
        (r : record t_source t_period t_proposal t_ballot) :=
        Build t_source t_period t_proposal t_ballot source r.(period)
          r.(proposal) r.(ballot).
      Definition with_period {t_source t_period t_proposal t_ballot} period
        (r : record t_source t_period t_proposal t_ballot) :=
        Build t_source t_period t_proposal t_ballot r.(source) period
          r.(proposal) r.(ballot).
      Definition with_proposal {t_source t_period t_proposal t_ballot} proposal
        (r : record t_source t_period t_proposal t_ballot) :=
        Build t_source t_period t_proposal t_ballot r.(source) r.(period)
          proposal r.(ballot).
      Definition with_ballot {t_source t_period t_proposal t_ballot} ballot
        (r : record t_source t_period t_proposal t_ballot) :=
        Build t_source t_period t_proposal t_ballot r.(source) r.(period)
          r.(proposal) ballot.
    End Ballot.
    Definition Ballot_skeleton := Ballot.record.
    
    Module Manager_operation.
      Record record {source fee counter operation gas_limit storage_limit : Set} :
        Set := Build {
        source : source;
        fee : fee;
        counter : counter;
        operation : operation;
        gas_limit : gas_limit;
        storage_limit : storage_limit }.
      Arguments record : clear implicits.
      Definition with_source
        {t_source t_fee t_counter t_operation t_gas_limit t_storage_limit}
        source
        (r :
          record t_source t_fee t_counter t_operation t_gas_limit
            t_storage_limit) :=
        Build t_source t_fee t_counter t_operation t_gas_limit t_storage_limit
          source r.(fee) r.(counter) r.(operation) r.(gas_limit)
          r.(storage_limit).
      Definition with_fee
        {t_source t_fee t_counter t_operation t_gas_limit t_storage_limit} fee
        (r :
          record t_source t_fee t_counter t_operation t_gas_limit
            t_storage_limit) :=
        Build t_source t_fee t_counter t_operation t_gas_limit t_storage_limit
          r.(source) fee r.(counter) r.(operation) r.(gas_limit)
          r.(storage_limit).
      Definition with_counter
        {t_source t_fee t_counter t_operation t_gas_limit t_storage_limit}
        counter
        (r :
          record t_source t_fee t_counter t_operation t_gas_limit
            t_storage_limit) :=
        Build t_source t_fee t_counter t_operation t_gas_limit t_storage_limit
          r.(source) r.(fee) counter r.(operation) r.(gas_limit)
          r.(storage_limit).
      Definition with_operation
        {t_source t_fee t_counter t_operation t_gas_limit t_storage_limit}
        operation
        (r :
          record t_source t_fee t_counter t_operation t_gas_limit
            t_storage_limit) :=
        Build t_source t_fee t_counter t_operation t_gas_limit t_storage_limit
          r.(source) r.(fee) r.(counter) operation r.(gas_limit)
          r.(storage_limit).
      Definition with_gas_limit
        {t_source t_fee t_counter t_operation t_gas_limit t_storage_limit}
        gas_limit
        (r :
          record t_source t_fee t_counter t_operation t_gas_limit
            t_storage_limit) :=
        Build t_source t_fee t_counter t_operation t_gas_limit t_storage_limit
          r.(source) r.(fee) r.(counter) r.(operation) gas_limit
          r.(storage_limit).
      Definition with_storage_limit
        {t_source t_fee t_counter t_operation t_gas_limit t_storage_limit}
        storage_limit
        (r :
          record t_source t_fee t_counter t_operation t_gas_limit
            t_storage_limit) :=
        Build t_source t_fee t_counter t_operation t_gas_limit t_storage_limit
          r.(source) r.(fee) r.(counter) r.(operation) r.(gas_limit)
          storage_limit.
    End Manager_operation.
    Definition Manager_operation_skeleton := Manager_operation.record.
  End contents.
  Module manager_operation.
    Module Transaction.
      Record record {amount parameters entrypoint destination : Set} : Set := Build {
        amount : amount;
        parameters : parameters;
        entrypoint : entrypoint;
        destination : destination }.
      Arguments record : clear implicits.
      Definition with_amount {t_amount t_parameters t_entrypoint t_destination}
        amount (r : record t_amount t_parameters t_entrypoint t_destination) :=
        Build t_amount t_parameters t_entrypoint t_destination amount
          r.(parameters) r.(entrypoint) r.(destination).
      Definition with_parameters
        {t_amount t_parameters t_entrypoint t_destination} parameters
        (r : record t_amount t_parameters t_entrypoint t_destination) :=
        Build t_amount t_parameters t_entrypoint t_destination r.(amount)
          parameters r.(entrypoint) r.(destination).
      Definition with_entrypoint
        {t_amount t_parameters t_entrypoint t_destination} entrypoint
        (r : record t_amount t_parameters t_entrypoint t_destination) :=
        Build t_amount t_parameters t_entrypoint t_destination r.(amount)
          r.(parameters) entrypoint r.(destination).
      Definition with_destination
        {t_amount t_parameters t_entrypoint t_destination} destination
        (r : record t_amount t_parameters t_entrypoint t_destination) :=
        Build t_amount t_parameters t_entrypoint t_destination r.(amount)
          r.(parameters) r.(entrypoint) destination.
    End Transaction.
    Definition Transaction_skeleton := Transaction.record.
    
    Module Origination.
      Record record {delegate script credit preorigination : Set} : Set := Build {
        delegate : delegate;
        script : script;
        credit : credit;
        preorigination : preorigination }.
      Arguments record : clear implicits.
      Definition with_delegate {t_delegate t_script t_credit t_preorigination}
        delegate (r : record t_delegate t_script t_credit t_preorigination) :=
        Build t_delegate t_script t_credit t_preorigination delegate r.(script)
          r.(credit) r.(preorigination).
      Definition with_script {t_delegate t_script t_credit t_preorigination}
        script (r : record t_delegate t_script t_credit t_preorigination) :=
        Build t_delegate t_script t_credit t_preorigination r.(delegate) script
          r.(credit) r.(preorigination).
      Definition with_credit {t_delegate t_script t_credit t_preorigination}
        credit (r : record t_delegate t_script t_credit t_preorigination) :=
        Build t_delegate t_script t_credit t_preorigination r.(delegate)
          r.(script) credit r.(preorigination).
      Definition with_preorigination
        {t_delegate t_script t_credit t_preorigination} preorigination
        (r : record t_delegate t_script t_credit t_preorigination) :=
        Build t_delegate t_script t_credit t_preorigination r.(delegate)
          r.(script) r.(credit) preorigination.
    End Origination.
    Definition Origination_skeleton := Origination.record.
    
    Module Register_global_constant.
      Record record {value : Set} : Set := Build {
        value : value }.
      Arguments record : clear implicits.
      Definition with_value {t_value} value (r : record t_value) :=
        Build t_value value.
    End Register_global_constant.
    Definition Register_global_constant_skeleton :=
      Register_global_constant.record.
  End manager_operation.
End ConstructorRecords_contents_manager_operation.
Import ConstructorRecords_contents_manager_operation.

Module protocol_data.
  Record record {contents signature : Set} : Set := Build {
    contents : contents;
    signature : signature }.
  Arguments record : clear implicits.
  Definition with_contents {t_contents t_signature} contents
    (r : record t_contents t_signature) :=
    Build t_contents t_signature contents r.(signature).
  Definition with_signature {t_contents t_signature} signature
    (r : record t_contents t_signature) :=
    Build t_contents t_signature r.(contents) signature.
End protocol_data.
Definition protocol_data_skeleton := protocol_data.record.

Module operation.
  Record record {shell protocol_data : Set} : Set := Build {
    shell : shell;
    protocol_data : protocol_data }.
  Arguments record : clear implicits.
  Definition with_shell {t_shell t_protocol_data} shell
    (r : record t_shell t_protocol_data) :=
    Build t_shell t_protocol_data shell r.(protocol_data).
  Definition with_protocol_data {t_shell t_protocol_data} protocol_data
    (r : record t_shell t_protocol_data) :=
    Build t_shell t_protocol_data r.(shell) protocol_data.
End operation.
Definition operation_skeleton := operation.record.

Reserved Notation "'contents.Seed_nonce_revelation".
Reserved Notation "'contents.Double_preendorsement_evidence".
Reserved Notation "'contents.Double_endorsement_evidence".
Reserved Notation "'contents.Double_baking_evidence".
Reserved Notation "'contents.Activate_account".
Reserved Notation "'contents.Proposals".
Reserved Notation "'contents.Ballot".
Reserved Notation "'contents.Manager_operation".
Reserved Notation "'manager_operation.Transaction".
Reserved Notation "'manager_operation.Origination".
Reserved Notation "'manager_operation.Register_global_constant".
Reserved Notation "'operation".
Reserved Notation "'protocol_data".
Reserved Notation "'counter".

Inductive contents_list : Set :=
| Single : contents -> contents_list
| Cons : contents -> contents_list -> contents_list

with contents : Set :=
| Preendorsement : consensus_content -> contents
| Endorsement : consensus_content -> contents
| Seed_nonce_revelation : 'contents.Seed_nonce_revelation -> contents
| Double_preendorsement_evidence :
  'contents.Double_preendorsement_evidence -> contents
| Double_endorsement_evidence :
  'contents.Double_endorsement_evidence -> contents
| Double_baking_evidence : 'contents.Double_baking_evidence -> contents
| Activate_account : 'contents.Activate_account -> contents
| Proposals : 'contents.Proposals -> contents
| Ballot : 'contents.Ballot -> contents
| Failing_noop : string -> contents
| Manager_operation : 'contents.Manager_operation -> contents

with manager_operation : Set :=
| Reveal : Signature.public_key -> manager_operation
| Transaction : 'manager_operation.Transaction -> manager_operation
| Origination : 'manager_operation.Origination -> manager_operation
| Delegation : option Signature.public_key_hash -> manager_operation
| Register_global_constant :
  'manager_operation.Register_global_constant -> manager_operation
| Set_deposits_limit : option Tez_repr.t -> manager_operation

where "'protocol_data" :=
  (protocol_data_skeleton contents_list (option Signature.t))
and "'counter" := (Z.t)
and "'operation" := (operation_skeleton Operation.shell_header 'protocol_data)
and "'contents.Seed_nonce_revelation" :=
  (contents.Seed_nonce_revelation_skeleton Raw_level_repr.t Seed_repr.nonce)
and "'contents.Double_preendorsement_evidence" :=
  (contents.Double_preendorsement_evidence_skeleton 'operation 'operation)
and "'contents.Double_endorsement_evidence" :=
  (contents.Double_endorsement_evidence_skeleton 'operation 'operation)
and "'contents.Double_baking_evidence" :=
  (contents.Double_baking_evidence_skeleton Block_header_repr.t
    Block_header_repr.t)
and "'contents.Activate_account" :=
  (contents.Activate_account_skeleton
    Ed25519.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.t)
    Blinded_public_key_hash.activation_code)
and "'contents.Proposals" :=
  (contents.Proposals_skeleton Signature.public_key_hash int32
    (list Protocol_hash.t))
and "'contents.Ballot" :=
  (contents.Ballot_skeleton Signature.public_key_hash int32 Protocol_hash.t
    Vote_repr.ballot)
and "'contents.Manager_operation" :=
  (contents.Manager_operation_skeleton Signature.public_key_hash Tez_repr.tez
    'counter manager_operation Gas_limit_repr.Arith.integral Z.t)
and "'manager_operation.Transaction" :=
  (manager_operation.Transaction_skeleton Tez_repr.tez Script_repr.lazy_expr
    string Contract_repr.contract)
and "'manager_operation.Origination" :=
  (manager_operation.Origination_skeleton (option Signature.public_key_hash)
    Script_repr.t Tez_repr.tez (option Contract_repr.t))
and "'manager_operation.Register_global_constant" :=
  (manager_operation.Register_global_constant_skeleton Script_repr.lazy_expr).

Module contents.
  Include ConstructorRecords_contents_manager_operation.contents.
  Definition Seed_nonce_revelation := 'contents.Seed_nonce_revelation.
  Definition Double_preendorsement_evidence :=
    'contents.Double_preendorsement_evidence.
  Definition Double_endorsement_evidence :=
    'contents.Double_endorsement_evidence.
  Definition Double_baking_evidence := 'contents.Double_baking_evidence.
  Definition Activate_account := 'contents.Activate_account.
  Definition Proposals := 'contents.Proposals.
  Definition Ballot := 'contents.Ballot.
  Definition Manager_operation := 'contents.Manager_operation.
End contents.
Module manager_operation.
  Include ConstructorRecords_contents_manager_operation.manager_operation.
  Definition Transaction := 'manager_operation.Transaction.
  Definition Origination := 'manager_operation.Origination.
  Definition Register_global_constant :=
    'manager_operation.Register_global_constant.
End manager_operation.

Definition operation := 'operation.
Definition protocol_data := 'protocol_data.
Definition counter := 'counter.

Definition manager_kind (function_parameter : manager_operation)
  : Kind.manager :=
  match function_parameter with
  | Reveal _ => Kind.Reveal_manager_kind
  | Transaction _ => Kind.Transaction_manager_kind
  | Origination _ => Kind.Origination_manager_kind
  | Delegation _ => Kind.Delegation_manager_kind
  | Register_global_constant _ => Kind.Register_global_constant_manager_kind
  | Set_deposits_limit _ => Kind.Set_deposits_limit_manager_kind
  end.

Module internal_operation.
  Record record : Set := Build {
    source : Contract_repr.contract;
    operation : manager_operation;
    nonce : int }.
  Definition with_source source (r : record) :=
    Build source r.(operation) r.(nonce).
  Definition with_operation operation (r : record) :=
    Build r.(source) operation r.(nonce).
  Definition with_nonce nonce (r : record) :=
    Build r.(source) r.(operation) nonce.
End internal_operation.
Definition internal_operation := internal_operation.record.

Inductive packed_manager_operation : Set :=
| Manager : manager_operation -> packed_manager_operation.

Inductive packed_contents : Set :=
| Contents : contents -> packed_contents.

Inductive packed_contents_list : Set :=
| Contents_list : contents_list -> packed_contents_list.

Inductive packed_protocol_data : Set :=
| Operation_data : protocol_data -> packed_protocol_data.

Module packed_operation.
  Record record : Set := Build {
    shell : Operation.shell_header;
    protocol_data : packed_protocol_data }.
  Definition with_shell shell (r : record) :=
    Build shell r.(protocol_data).
  Definition with_protocol_data protocol_data (r : record) :=
    Build r.(shell) protocol_data.
End packed_operation.
Definition packed_operation := packed_operation.record.

Definition _pack (function_parameter : operation) : packed_operation :=
  let '{|
    operation.shell := shell; operation.protocol_data := protocol_data |} :=
    function_parameter in
  {| packed_operation.shell := shell;
    packed_operation.protocol_data := Operation_data protocol_data |}.

Inductive packed_internal_operation : Set :=
| Internal_operation : internal_operation -> packed_internal_operation.

Fixpoint to_list (function_parameter : packed_contents_list)
  : list packed_contents :=
  match function_parameter with
  | Contents_list (Single o_value) => [ Contents o_value ]
  | Contents_list (Cons o_value os) =>
    cons (Contents o_value) (to_list (Contents_list os))
  end.

Fixpoint of_list_internal (function_parameter : list packed_contents)
  : Pervasives.result packed_contents_list string :=
  match function_parameter with
  | [] => Pervasives.Error "Operation lists should not be empty."
  | cons (Contents o_value) [] => Pervasives.Ok (Contents_list (Single o_value))
  | cons (Contents o_value) os =>
    let? 'Contents_list os := of_list_internal os in
    match (o_value, os) with
    | (Manager_operation _, Single (Manager_operation _)) =>
      Pervasives.Ok (Contents_list (Cons o_value os))
    | (Manager_operation _, Cons _ _) =>
      Pervasives.Ok (Contents_list (Cons o_value os))
    | _ =>
      Pervasives.Error
        "Operation list of length > 1 should only contains manager operations."
    end
  end.

Definition of_list (l_value : list packed_contents) : M? packed_contents_list :=
  match of_list_internal l_value with
  | Pervasives.Ok contents => Pervasives.Ok contents
  | Pervasives.Error s_value =>
    Error_monad.error_value
      (Build_extensible "Contents_list_error" string s_value)
  end.

Module Encoding.
  Definition case_value {A B : Set}
    (tag : Data_encoding.case_tag) (name : string)
    (args : Data_encoding.encoding A) (proj : B -> option A) (inj : A -> B)
    : Data_encoding.case B :=
    Data_encoding.case_value (String.capitalize_ascii name) None tag
      (Data_encoding.merge_objs
        (Data_encoding.obj1
          (Data_encoding.req None None "kind" (Data_encoding.constant name)))
        args)
      (fun (x_value : B) =>
        match proj x_value with
        | None => None
        | Some x_value => Some (tt, x_value)
        end)
      (fun (function_parameter : unit * A) =>
        let '(_, x_value) := function_parameter in
        inj x_value).
  
  Module Manager_operations.
    (** Records for the constructor parameters *)
    Module ConstructorRecords_case.
      Module case.
        Module MCase.
          Record record {tag name encoding select proj inj : Set} : Set := Build {
            tag : tag;
            name : name;
            encoding : encoding;
            select : select;
            proj : proj;
            inj : inj }.
          Arguments record : clear implicits.
          Definition with_tag {t_tag t_name t_encoding t_select t_proj t_inj}
            tag (r : record t_tag t_name t_encoding t_select t_proj t_inj) :=
            Build t_tag t_name t_encoding t_select t_proj t_inj tag r.(name)
              r.(encoding) r.(select) r.(proj) r.(inj).
          Definition with_name {t_tag t_name t_encoding t_select t_proj t_inj}
            name (r : record t_tag t_name t_encoding t_select t_proj t_inj) :=
            Build t_tag t_name t_encoding t_select t_proj t_inj r.(tag) name
              r.(encoding) r.(select) r.(proj) r.(inj).
          Definition with_encoding
            {t_tag t_name t_encoding t_select t_proj t_inj} encoding
            (r : record t_tag t_name t_encoding t_select t_proj t_inj) :=
            Build t_tag t_name t_encoding t_select t_proj t_inj r.(tag) r.(name)
              encoding r.(select) r.(proj) r.(inj).
          Definition with_select {t_tag t_name t_encoding t_select t_proj t_inj}
            select (r : record t_tag t_name t_encoding t_select t_proj t_inj) :=
            Build t_tag t_name t_encoding t_select t_proj t_inj r.(tag) r.(name)
              r.(encoding) select r.(proj) r.(inj).
          Definition with_proj {t_tag t_name t_encoding t_select t_proj t_inj}
            proj (r : record t_tag t_name t_encoding t_select t_proj t_inj) :=
            Build t_tag t_name t_encoding t_select t_proj t_inj r.(tag) r.(name)
              r.(encoding) r.(select) proj r.(inj).
          Definition with_inj {t_tag t_name t_encoding t_select t_proj t_inj}
            inj (r : record t_tag t_name t_encoding t_select t_proj t_inj) :=
            Build t_tag t_name t_encoding t_select t_proj t_inj r.(tag) r.(name)
              r.(encoding) r.(select) r.(proj) inj.
        End MCase.
        Definition MCase_skeleton := MCase.record.
      End case.
    End ConstructorRecords_case.
    Import ConstructorRecords_case.
    
    Reserved Notation "'case.MCase".
    
    Inductive case : Set :=
    | MCase : forall {a : Set}, 'case.MCase a -> case
    
    where "'case.MCase" :=
      (fun (t_a : Set) => case.MCase_skeleton int string (Data_encoding.t t_a)
        (packed_manager_operation -> option manager_operation)
        (manager_operation -> t_a) (t_a -> manager_operation)).
    
    Module case.
      Include ConstructorRecords_case.case.
      Definition MCase := 'case.MCase.
    End case.
    
    Axiom reveal_case : case.
    
    Definition entrypoint_encoding : Data_encoding.encoding string :=
      Data_encoding.def "entrypoint" (Some "entrypoint")
        (Some "Named entrypoint to a Michelson smart contract")
        (let builtin_case (tag : int) (name : string)
          : Data_encoding.case string :=
          Data_encoding.case_value name None (Data_encoding.Tag tag)
            (Data_encoding.constant name)
            (fun (n_value : string) =>
              if Compare.String.(Compare.S.op_eq) n_value name then
                Some tt
              else
                None)
            (fun (function_parameter : unit) =>
              let '_ := function_parameter in
              name) in
        Data_encoding.union None
          [
            builtin_case 0 "default";
            builtin_case 1 "root";
            builtin_case 2 "do";
            builtin_case 3 "set_delegate";
            builtin_case 4 "remove_delegate";
            Data_encoding.case_value "named" None (Data_encoding.Tag 255)
              (Data_encoding.Bounded.string_value 31)
              (fun (s_value : string) => Some s_value)
              (fun (s_value : string) => s_value)
          ]).
    
    Axiom transaction_case : case.
    
    Axiom origination_case : case.
    
    Axiom delegation_case : case.
    
    Axiom register_global_constant_case : case.
    
    Axiom set_deposits_limit_case : case.
    
    Definition encoding : Data_encoding.encoding packed_manager_operation :=
      let make (mcase : case) : Data_encoding.case packed_manager_operation :=
        let
          'MCase {|
            case.MCase.tag := tag;
              case.MCase.name := name;
              case.MCase.encoding := encoding;
              case.MCase.select := select;
              case.MCase.proj := proj;
              case.MCase.inj := inj
              |} := mcase in
        let 'existT _ __MCase_'a [inj, proj, select, encoding, name, tag] :=
          existT (A := Set)
            (fun __MCase_'a =>
              [__MCase_'a -> manager_operation **
                manager_operation -> __MCase_'a **
                packed_manager_operation -> option manager_operation **
                Data_encoding.t __MCase_'a ** string ** int]) _
            [inj, proj, select, encoding, name, tag] in
        case_value (Data_encoding.Tag tag) name encoding
          (fun (o_value : packed_manager_operation) =>
            match select o_value with
            | None => None
            | Some o_value => Some (proj o_value)
            end) (fun (x_value : __MCase_'a) => Manager (inj x_value)) in
      Data_encoding.union (Some Data_encoding.Uint8)
        [
          make reveal_case;
          make transaction_case;
          make origination_case;
          make delegation_case;
          make register_global_constant_case;
          make set_deposits_limit_case
        ].
  End Manager_operations.
  
  (** Records for the constructor parameters *)
  Module ConstructorRecords_case.
    Module case.
      Module Case.
        Record record {tag name encoding select proj inj : Set} : Set := Build {
          tag : tag;
          name : name;
          encoding : encoding;
          select : select;
          proj : proj;
          inj : inj }.
        Arguments record : clear implicits.
        Definition with_tag {t_tag t_name t_encoding t_select t_proj t_inj} tag
          (r : record t_tag t_name t_encoding t_select t_proj t_inj) :=
          Build t_tag t_name t_encoding t_select t_proj t_inj tag r.(name)
            r.(encoding) r.(select) r.(proj) r.(inj).
        Definition with_name {t_tag t_name t_encoding t_select t_proj t_inj}
          name (r : record t_tag t_name t_encoding t_select t_proj t_inj) :=
          Build t_tag t_name t_encoding t_select t_proj t_inj r.(tag) name
            r.(encoding) r.(select) r.(proj) r.(inj).
        Definition with_encoding {t_tag t_name t_encoding t_select t_proj t_inj}
          encoding (r : record t_tag t_name t_encoding t_select t_proj t_inj) :=
          Build t_tag t_name t_encoding t_select t_proj t_inj r.(tag) r.(name)
            encoding r.(select) r.(proj) r.(inj).
        Definition with_select {t_tag t_name t_encoding t_select t_proj t_inj}
          select (r : record t_tag t_name t_encoding t_select t_proj t_inj) :=
          Build t_tag t_name t_encoding t_select t_proj t_inj r.(tag) r.(name)
            r.(encoding) select r.(proj) r.(inj).
        Definition with_proj {t_tag t_name t_encoding t_select t_proj t_inj}
          proj (r : record t_tag t_name t_encoding t_select t_proj t_inj) :=
          Build t_tag t_name t_encoding t_select t_proj t_inj r.(tag) r.(name)
            r.(encoding) r.(select) proj r.(inj).
        Definition with_inj {t_tag t_name t_encoding t_select t_proj t_inj} inj
          (r : record t_tag t_name t_encoding t_select t_proj t_inj) :=
          Build t_tag t_name t_encoding t_select t_proj t_inj r.(tag) r.(name)
            r.(encoding) r.(select) r.(proj) inj.
      End Case.
      Definition Case_skeleton := Case.record.
    End case.
  End ConstructorRecords_case.
  Import ConstructorRecords_case.
  
  Reserved Notation "'case.Case".
  
  Inductive case (b : Set) : Set :=
  | Case : forall {a : Set}, 'case.Case a -> case b
  
  where "'case.Case" :=
    (fun (t_a : Set) => case.Case_skeleton int string (Data_encoding.t t_a)
      (packed_contents -> option contents) (contents -> t_a) (t_a -> contents)).
  
  Module case.
    Include ConstructorRecords_case.case.
    Definition Case := 'case.Case.
  End case.
  
  Arguments Case {_ _}.
  
  Definition preendorsement_case : case Kind.preendorsement :=
    Case
      {| case.Case.tag := 20; case.Case.name := "preendorsement";
        case.Case.encoding := consensus_content_encoding;
        case.Case.select :=
          fun (function_parameter : packed_contents) =>
            match function_parameter with
            | Contents ((Preendorsement _) as op) => Some op
            | _ => None
            end;
        case.Case.proj :=
          fun (function_parameter : contents) =>
            match function_parameter with
            | Preendorsement preendorsement => preendorsement
            | _ => unreachable_gadt_branch
            end;
        case.Case.inj :=
          fun (preendorsement : consensus_content) =>
            Preendorsement preendorsement |}.
  
  Axiom preendorsement_encoding : Data_encoding.encoding operation.
  
  Definition endorsement_case : case Kind.endorsement :=
    Case
      {| case.Case.tag := 21; case.Case.name := "endorsement";
        case.Case.encoding :=
          Data_encoding.obj4
            (Data_encoding.req None None "slot" Slot_repr.encoding)
            (Data_encoding.req None None "level" Raw_level_repr.encoding)
            (Data_encoding.req None None "round" Round_repr.encoding)
            (Data_encoding.req None None "block_payload_hash"
              Block_payload_hash.encoding);
        case.Case.select :=
          fun (function_parameter : packed_contents) =>
            match function_parameter with
            | Contents ((Endorsement _) as op) => Some op
            | _ => None
            end;
        case.Case.proj :=
          fun (function_parameter : contents) =>
            match function_parameter with
            | Endorsement consensus_content =>
              (consensus_content.(consensus_content.slot),
                consensus_content.(consensus_content.level),
                consensus_content.(consensus_content.round),
                consensus_content.(consensus_content.block_payload_hash))
            | _ => unreachable_gadt_branch
            end;
        case.Case.inj :=
          fun (function_parameter :
            Slot_repr.t * Raw_level_repr.raw_level * Round_repr.t *
              Block_payload_hash.t) =>
            let '(slot, level, round, block_payload_hash) := function_parameter
              in
            Endorsement
              {| consensus_content.slot := slot;
                consensus_content.level := level;
                consensus_content.round := round;
                consensus_content.block_payload_hash := block_payload_hash |} |}.
  
  Axiom endorsement_encoding : Data_encoding.encoding operation.
  
  Axiom seed_nonce_revelation_case : case Kind.seed_nonce_revelation.
  
  Axiom double_preendorsement_evidence_case :
    case Kind.double_preendorsement_evidence.
  
  Axiom double_endorsement_evidence_case :
    case Kind.double_endorsement_evidence.
  
  Axiom double_baking_evidence_case : case Kind.double_baking_evidence.
  
  Axiom activate_account_case : case Kind.activate_account.
  
  Axiom proposals_case : case Kind.proposals.
  
  Axiom ballot_case : case Kind.ballot.
  
  Definition failing_noop_case : case Kind.failing_noop :=
    Case
      {| case.Case.tag := 17; case.Case.name := "failing_noop";
        case.Case.encoding :=
          Data_encoding.obj1
            (Data_encoding.req None None "arbitrary" Data_encoding.string_value);
        case.Case.select :=
          fun (function_parameter : packed_contents) =>
            match function_parameter with
            | Contents ((Failing_noop _) as op) => Some op
            | _ => None
            end;
        case.Case.proj :=
          fun (function_parameter : contents) =>
            match function_parameter with
            | Failing_noop message => message
            | _ => unreachable_gadt_branch
            end; case.Case.inj := fun (message : string) => Failing_noop message
        |}.
  
  Definition manager_encoding
    : Data_encoding.encoding
      (Signature.public_key_hash * Tez_repr.t * Z.t *
        Gas_limit_repr.Arith.integral * Z.t) :=
    Data_encoding.obj5
      (Data_encoding.req None None "source"
        Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
      (Data_encoding.req None None "fee" Tez_repr.encoding)
      (Data_encoding.req None None "counter"
        (Data_encoding.check_size 10 Data_encoding.n_value))
      (Data_encoding.req None None "gas_limit"
        (Data_encoding.check_size 10 Gas_limit_repr.Arith.n_integral_encoding))
      (Data_encoding.req None None "storage_limit"
        (Data_encoding.check_size 10 Data_encoding.n_value)).
  
  Definition extract (function_parameter : contents)
    : Signature.public_key_hash * Tez_repr.tez * counter *
      Gas_limit_repr.Arith.integral * Z.t :=
    match function_parameter with
    |
      Manager_operation {|
        contents.Manager_operation.source := source;
          contents.Manager_operation.fee := fee;
          contents.Manager_operation.counter := counter;
          contents.Manager_operation.operation := _;
          contents.Manager_operation.gas_limit := gas_limit;
          contents.Manager_operation.storage_limit := storage_limit
          |} => (source, fee, counter, gas_limit, storage_limit)
    | _ => unreachable_gadt_branch
    end.
  
  Definition rebuild
    (function_parameter :
      Signature.public_key_hash * Tez_repr.tez * counter *
        Gas_limit_repr.Arith.integral * Z.t) : manager_operation -> contents :=
    let '(source, fee, counter, gas_limit, storage_limit) := function_parameter
      in
    fun (operation : manager_operation) =>
      Manager_operation
        {| contents.Manager_operation.source := source;
          contents.Manager_operation.fee := fee;
          contents.Manager_operation.counter := counter;
          contents.Manager_operation.operation := operation;
          contents.Manager_operation.gas_limit := gas_limit;
          contents.Manager_operation.storage_limit := storage_limit |}.
  
  Axiom make_manager_case : int -> Manager_operations.case -> case Kind.manager.
  
  Definition reveal_case : case Kind.manager :=
    make_manager_case 107 Manager_operations.reveal_case.
  
  Definition transaction_case : case Kind.manager :=
    make_manager_case 108 Manager_operations.transaction_case.
  
  Definition origination_case : case Kind.manager :=
    make_manager_case 109 Manager_operations.origination_case.
  
  Definition delegation_case : case Kind.manager :=
    make_manager_case 110 Manager_operations.delegation_case.
  
  Definition register_global_constant_case : case Kind.manager :=
    make_manager_case 111 Manager_operations.register_global_constant_case.
  
  Definition set_deposits_limit_case : case Kind.manager :=
    make_manager_case 112 Manager_operations.set_deposits_limit_case.
  
  Definition contents_encoding : Data_encoding.encoding packed_contents :=
    let make {A : Set} (case_description : case A)
      : Data_encoding.case packed_contents :=
      let
        'Case {|
          case.Case.tag := tag;
            case.Case.name := name;
            case.Case.encoding := encoding;
            case.Case.select := select;
            case.Case.proj := proj;
            case.Case.inj := inj
            |} := case_description in
      let 'existT _ __Case_'a [inj, proj, select, encoding, name, tag] :=
        existT (A := Set)
          (fun __Case_'a =>
            [__Case_'a -> contents ** contents -> __Case_'a **
              packed_contents -> option contents ** Data_encoding.t __Case_'a **
              string ** int]) _ [inj, proj, select, encoding, name, tag] in
      case_value (Data_encoding.Tag tag) name encoding
        (fun (o_value : packed_contents) =>
          match select o_value with
          | None => None
          | Some o_value => Some (proj o_value)
          end) (fun (x_value : __Case_'a) => Contents (inj x_value)) in
    (let arg := Data_encoding.def "operation.alpha.contents" in
    fun (eta : Data_encoding.encoding packed_contents) => arg None None eta)
      (Data_encoding.union None
        [
          make endorsement_case;
          make preendorsement_case;
          make seed_nonce_revelation_case;
          make double_endorsement_evidence_case;
          make double_preendorsement_evidence_case;
          make double_baking_evidence_case;
          make activate_account_case;
          make proposals_case;
          make ballot_case;
          make reveal_case;
          make transaction_case;
          make origination_case;
          make delegation_case;
          make set_deposits_limit_case;
          make failing_noop_case;
          make register_global_constant_case
        ]).
  
  Definition contents_list_encoding
    : Data_encoding.encoding packed_contents_list :=
    Data_encoding.conv_with_guard to_list of_list_internal None
      (Data_encoding._Variable.list_value None contents_encoding).
  
  Definition optional_signature_encoding
    : Data_encoding.encoding (option Signature.t) :=
    Data_encoding.conv
      (fun (function_parameter : option Signature.t) =>
        match function_parameter with
        | Some s_value => s_value
        | None => Signature.zero
        end)
      (fun (s_value : Signature.t) =>
        if Signature.equal s_value Signature.zero then
          None
        else
          Some s_value) None Signature.encoding.
  
  Definition protocol_data_encoding
    : Data_encoding.encoding packed_protocol_data :=
    (let arg := Data_encoding.def "operation.alpha.contents_and_signature" in
    fun (eta : Data_encoding.encoding packed_protocol_data) => arg None None eta)
      (Data_encoding.conv
        (fun (function_parameter : packed_protocol_data) =>
          let
            'Operation_data {|
              protocol_data.contents := contents;
                protocol_data.signature := signature
                |} := function_parameter in
          ((Contents_list contents), signature))
        (fun (function_parameter : packed_contents_list * option Signature.t) =>
          let '(Contents_list contents, signature) := function_parameter in
          Operation_data
            {| protocol_data.contents := contents;
              protocol_data.signature := signature |}) None
        (Data_encoding.obj2
          (Data_encoding.req None None "contents" contents_list_encoding)
          (Data_encoding.req None None "signature" optional_signature_encoding))).
  
  Definition operation_encoding : Data_encoding.encoding packed_operation :=
    Data_encoding.conv
      (fun (function_parameter : packed_operation) =>
        let '{|
          packed_operation.shell := shell;
            packed_operation.protocol_data := protocol_data
            |} := function_parameter in
        (shell, protocol_data))
      (fun (function_parameter : Operation.shell_header * packed_protocol_data)
        =>
        let '(shell, protocol_data) := function_parameter in
        {| packed_operation.shell := shell;
          packed_operation.protocol_data := protocol_data |}) None
      (Data_encoding.merge_objs Operation.shell_header_encoding
        protocol_data_encoding).
  
  Definition unsigned_operation_encoding
    : Data_encoding.encoding (Operation.shell_header * packed_contents_list) :=
    (let arg := Data_encoding.def "operation.alpha.unsigned_operation" in
    fun (eta :
      Data_encoding.encoding (Operation.shell_header * packed_contents_list)) =>
      arg None None eta)
      (Data_encoding.merge_objs Operation.shell_header_encoding
        (Data_encoding.obj1
          (Data_encoding.req None None "contents" contents_list_encoding))).
  
  Definition internal_operation_encoding
    : Data_encoding.encoding packed_internal_operation :=
    (let arg := Data_encoding.def "operation.alpha.internal_operation" in
    fun (eta : Data_encoding.encoding packed_internal_operation) =>
      arg None None eta)
      (Data_encoding.conv
        (fun (function_parameter : packed_internal_operation) =>
          let
            'Internal_operation {|
              internal_operation.source := source;
                internal_operation.operation := operation;
                internal_operation.nonce := nonce_value
                |} := function_parameter in
          ((source, nonce_value), (Manager operation)))
        (fun (function_parameter :
          (Contract_repr.contract * int) * packed_manager_operation) =>
          let '((source, nonce_value), Manager operation) := function_parameter
            in
          Internal_operation
            {| internal_operation.source := source;
              internal_operation.operation := operation;
              internal_operation.nonce := nonce_value |}) None
        (Data_encoding.merge_objs
          (Data_encoding.obj2
            (Data_encoding.req None None "source" Contract_repr.encoding)
            (Data_encoding.req None None "nonce" Data_encoding.uint16))
          Manager_operations.encoding)).
End Encoding.

Definition encoding : Data_encoding.encoding packed_operation :=
  Encoding.operation_encoding.

Definition contents_encoding : Data_encoding.encoding packed_contents :=
  Encoding.contents_encoding.

Definition contents_list_encoding
  : Data_encoding.encoding packed_contents_list :=
  Encoding.contents_list_encoding.

Definition protocol_data_encoding
  : Data_encoding.encoding packed_protocol_data :=
  Encoding.protocol_data_encoding.

Definition unsigned_operation_encoding
  : Data_encoding.encoding (Operation.shell_header * packed_contents_list) :=
  Encoding.unsigned_operation_encoding.

Definition internal_operation_encoding
  : Data_encoding.encoding packed_internal_operation :=
  Encoding.internal_operation_encoding.

Definition raw_value (function_parameter : operation) : Operation.t :=
  let '{|
    operation.shell := shell; operation.protocol_data := protocol_data |} :=
    function_parameter in
  let proto :=
    Data_encoding.Binary.to_bytes_exn None protocol_data_encoding
      (Operation_data protocol_data) in
  {| Operation.t.shell := shell; Operation.t.proto := proto |}.

Definition acceptable_passes (op : packed_operation) : list int :=
  let 'Operation_data protocol_data := op.(packed_operation.protocol_data) in
  match protocol_data.(protocol_data.contents) with
  | Single (Failing_noop _) => nil
  | Single (Preendorsement _) => [ 0 ]
  | Single (Endorsement _) => [ 0 ]
  | Single (Proposals _) => [ 1 ]
  | Single (Ballot _) => [ 1 ]
  | Single (Seed_nonce_revelation _) => [ 2 ]
  | Single (Double_endorsement_evidence _) => [ 2 ]
  | Single (Double_preendorsement_evidence _) => [ 2 ]
  | Single (Double_baking_evidence _) => [ 2 ]
  | Single (Activate_account _) => [ 2 ]
  | Single (Manager_operation _) => [ 3 ]
  | Cons (Manager_operation _) _ops => [ 3 ]
  | _ => unreachable_gadt_branch
  end.

(** Init function; without side-effects in Coq *)
Definition init_module_repr : unit :=
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "operation.invalid_signature" "Invalid operation signature"
      "The operation signature is ill-formed or has been made with the wrong public key"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "The operation signature is invalid"
                  CamlinternalFormatBasics.End_of_format)
                "The operation signature is invalid"))) Data_encoding.unit_value
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_signature" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Invalid_signature" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "operation.missing_signature" "Missing operation signature"
      "The operation is of a kind that must be signed, but the signature is missing"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "The operation requires a signature"
                  CamlinternalFormatBasics.End_of_format)
                "The operation requires a signature"))) Data_encoding.unit_value
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Missing_signature" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Missing_signature" unit tt) in
  Error_monad.register_error_kind Error_monad.Permanent
    "operation.contents_list_error" "Invalid list of operation contents."
    "An operation contents list has an unexpected shape; it should be either a single operation or a non-empty list of manager operations"
    (Some
      (fun (ppf : Format.formatter) =>
        fun (s_value : string) =>
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal
                "An operation contents list has an unexpected shape: "
                (CamlinternalFormatBasics.String
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.End_of_format))
              "An operation contents list has an unexpected shape: %s") s_value))
    (Data_encoding.obj1
      (Data_encoding.req None None "message" Data_encoding.string_value))
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Contents_list_error" then
          let 's_value := cast string payload in
          Some s_value
        else None
      end)
    (fun (s_value : string) =>
      Build_extensible "Contents_list_error" string s_value).

Definition check_signature
  (key_value : Signature.public_key) (chain_id : Chain_id.t)
  (function_parameter : operation) : M? unit :=
  let '{|
    operation.shell := shell; operation.protocol_data := protocol_data |} :=
    function_parameter in
  let check
    (watermark : Signature.watermark) (contents : packed_contents_list)
    (signature : Signature.t) : M? unit :=
    let unsigned_operation :=
      Data_encoding.Binary.to_bytes_exn None unsigned_operation_encoding
        (shell, contents) in
    if
      Signature.check (Some watermark) key_value signature unsigned_operation
    then
      Pervasives.Ok tt
    else
      Error_monad.error_value (Build_extensible "Invalid_signature" unit tt) in
  match protocol_data.(protocol_data.signature) with
  | None =>
    Error_monad.error_value (Build_extensible "Missing_signature" unit tt)
  | Some signature =>
    match protocol_data.(protocol_data.contents) with
    | (Single (Preendorsement _)) as contents =>
      check (to_watermark (Consensus_watermark.Preendorsement chain_id))
        (Contents_list contents) signature
    | (Single (Endorsement _)) as contents =>
      check (to_watermark (Consensus_watermark.Endorsement chain_id))
        (Contents_list contents) signature
    |
      Single
        (Failing_noop _ | Proposals _ | Ballot _ | Seed_nonce_revelation _ |
        Double_endorsement_evidence _ | Double_preendorsement_evidence _ |
        Double_baking_evidence _ | Activate_account _ | Manager_operation _) =>
      check Signature.Generic_operation
        (Contents_list protocol_data.(protocol_data.contents)) signature
    | Cons (Manager_operation _) _ops =>
      check Signature.Generic_operation
        (Contents_list protocol_data.(protocol_data.contents)) signature
    | _ => unreachable_gadt_branch
    end
  end.

Definition hash_raw : Operation.t -> Operation_hash.t := Operation.hash_value.

Definition hash_value (o_value : operation) : Operation_hash.t :=
  let proto :=
    Data_encoding.Binary.to_bytes_exn None protocol_data_encoding
      (Operation_data o_value.(operation.protocol_data)) in
  Operation.hash_value
    {| Operation.t.shell := o_value.(operation.shell);
      Operation.t.proto := proto |}.

Definition hash_packed (o_value : packed_operation) : Operation_hash.t :=
  let proto :=
    Data_encoding.Binary.to_bytes_exn None protocol_data_encoding
      o_value.(packed_operation.protocol_data) in
  Operation.hash_value
    {| Operation.t.shell := o_value.(packed_operation.shell);
      Operation.t.proto := proto |}.

Inductive eq : Set :=
| Eq : eq.

Definition equal_manager_operation_kind
  (op1 : manager_operation) (op2 : manager_operation) : option eq :=
  match (op1, op2) with
  | (Reveal _, Reveal _) => Some Eq
  | (Reveal _, _) => None
  | (Transaction _, Transaction _) => Some Eq
  | (Transaction _, _) => None
  | (Origination _, Origination _) => Some Eq
  | (Origination _, _) => None
  | (Delegation _, Delegation _) => Some Eq
  | (Delegation _, _) => None
  | (Register_global_constant _, Register_global_constant _) => Some Eq
  | (Register_global_constant _, _) => None
  | (Set_deposits_limit _, Set_deposits_limit _) => Some Eq
  | (Set_deposits_limit _, _) => None
  end.

Definition equal_contents_kind (op1 : contents) (op2 : contents) : option eq :=
  match (op1, op2) with
  | (Preendorsement _, Preendorsement _) => Some Eq
  | (Preendorsement _, _) => None
  | (Endorsement _, Endorsement _) => Some Eq
  | (Endorsement _, _) => None
  | (Seed_nonce_revelation _, Seed_nonce_revelation _) => Some Eq
  | (Seed_nonce_revelation _, _) => None
  | (Double_endorsement_evidence _, Double_endorsement_evidence _) => Some Eq
  | (Double_endorsement_evidence _, _) => None
  | (Double_preendorsement_evidence _, Double_preendorsement_evidence _) =>
    Some Eq
  | (Double_preendorsement_evidence _, _) => None
  | (Double_baking_evidence _, Double_baking_evidence _) => Some Eq
  | (Double_baking_evidence _, _) => None
  | (Activate_account _, Activate_account _) => Some Eq
  | (Activate_account _, _) => None
  | (Proposals _, Proposals _) => Some Eq
  | (Proposals _, _) => None
  | (Ballot _, Ballot _) => Some Eq
  | (Ballot _, _) => None
  | (Failing_noop _, Failing_noop _) => Some Eq
  | (Failing_noop _, _) => None
  | (Manager_operation op1, Manager_operation op2) =>
    match
      equal_manager_operation_kind op1.(contents.Manager_operation.operation)
        op2.(contents.Manager_operation.operation) with
    | None => None
    | Some Eq => Some Eq
    end
  | (Manager_operation _, _) => None
  end.

Fixpoint equal_contents_kind_list (op1 : contents_list) (op2 : contents_list)
  : option eq :=
  match (op1, op2) with
  | (Single op1, Single op2) => equal_contents_kind op1 op2
  | (Single _, Cons _ _) => None
  | (Cons _ _, Single _) => None
  | (Cons op1 ops1, Cons op2 ops2) =>
    match equal_contents_kind op1 op2 with
    | None => None
    | Some Eq =>
      match equal_contents_kind_list ops1 ops2 with
      | None => None
      | Some Eq => Some Eq
      end
    end
  end.

Definition equal (op1 : operation) (op2 : operation) : option eq :=
  if
    Pervasives.not (Operation_hash.equal (hash_value op1) (hash_value op2))
  then
    None
  else
    equal_contents_kind_list
      op1.(operation.protocol_data).(protocol_data.contents)
      op2.(operation.protocol_data).(protocol_data.contents).

Definition script_lazy_expr_size (expr : Script_repr.lazy_expr)
  : int * Saturation_repr.t :=
  let fun_value (expr : Micheline.canonical Michelson_v1_primitives.prim)
    : int * Saturation_repr.t :=
    Cache_memory_helpers.ret_adding (Cache_memory_helpers.expr_size expr)
      Cache_memory_helpers.word_size in
  let fun_bytes (bytes_value : bytes) : int * Saturation_repr.t :=
    (Cache_memory_helpers.Nodes.(Cache_memory_helpers.SNodes.zero),
      (Cache_memory_helpers.op_plusexclamation Cache_memory_helpers.word_size
        (Cache_memory_helpers.bytes_size bytes_value))) in
  let fun_combine
    (expr_size : int * Saturation_repr.t) (bytes_size : int * Saturation_repr.t)
    : int * Saturation_repr.t :=
    Cache_memory_helpers.op_plusplus expr_size bytes_size in
  Cache_memory_helpers.ret_adding
    (Data_encoding.apply_lazy fun_value fun_bytes fun_combine expr)
    Cache_memory_helpers.header_size.

Definition script_repr_size (function_parameter : Script_repr.t)
  : int * Saturation_repr.t :=
  let '{|
    Script_repr.t.code := code; Script_repr.t.storage := storage_value |} :=
    function_parameter in
  Cache_memory_helpers.ret_adding
    (Cache_memory_helpers.op_plusplus (script_lazy_expr_size code)
      (script_lazy_expr_size storage_value)) Cache_memory_helpers.h2w.

Definition internal_manager_operation_size (op : manager_operation)
  : int * Saturation_repr.t :=
  match op with
  |
    Transaction {|
      manager_operation.Transaction.amount := _;
        manager_operation.Transaction.parameters := parameters;
        manager_operation.Transaction.entrypoint := entrypoint;
        manager_operation.Transaction.destination := destination
        |} =>
    Cache_memory_helpers.ret_adding (script_lazy_expr_size parameters)
      (Cache_memory_helpers.op_plusexclamation
        (Cache_memory_helpers.op_plusexclamation
          (Cache_memory_helpers.op_plusexclamation Cache_memory_helpers.h4w
            Cache_memory_helpers.int64_size)
          (Cache_memory_helpers.string_size_gen (String.length entrypoint)))
        (Contract_repr.in_memory_size destination))
  |
    Origination {|
      manager_operation.Origination.delegate := delegate;
        manager_operation.Origination.script := script;
        manager_operation.Origination.credit := _;
        manager_operation.Origination.preorigination := preorigination
        |} =>
    Cache_memory_helpers.ret_adding (script_repr_size script)
      (Cache_memory_helpers.op_plusexclamation
        (Cache_memory_helpers.op_plusexclamation
          (Cache_memory_helpers.op_plusexclamation Cache_memory_helpers.h4w
            (Cache_memory_helpers.option_size
              (fun (function_parameter : Signature.public_key_hash) =>
                let '_ := function_parameter in
                Contract_repr.public_key_hash_in_memory_size) delegate))
          Cache_memory_helpers.int64_size)
        (Cache_memory_helpers.option_size Contract_repr.in_memory_size
          preorigination))
  | Delegation pkh_opt =>
    (Cache_memory_helpers.Nodes.(Cache_memory_helpers.SNodes.zero),
      (Cache_memory_helpers.op_plusexclamation Cache_memory_helpers.h1w
        (Cache_memory_helpers.option_size
          (fun (function_parameter : Signature.public_key_hash) =>
            let '_ := function_parameter in
            Contract_repr.public_key_hash_in_memory_size) pkh_opt)))
  | Reveal _ =>
    (* ❌ Assert instruction is not handled. *)
    assert (int * Saturation_repr.t) false
  | Register_global_constant _ =>
    (* ❌ Assert instruction is not handled. *)
    assert (int * Saturation_repr.t) false
  | Set_deposits_limit _ =>
    (* ❌ Assert instruction is not handled. *)
    assert (int * Saturation_repr.t) false
  end.

Definition packed_internal_operation_in_memory_size
  (function_parameter : packed_internal_operation)
  : Cache_memory_helpers.nodes_and_size :=
  let 'Internal_operation iop := function_parameter in
  let '{|
    internal_operation.source := source;
      internal_operation.operation := operation;
      internal_operation.nonce := _
      |} := iop in
  let source_size := Contract_repr.in_memory_size source in
  let nonce_size := Cache_memory_helpers.word_size in
  Cache_memory_helpers.ret_adding (internal_manager_operation_size operation)
    (Cache_memory_helpers.op_plusexclamation
      (Cache_memory_helpers.op_plusexclamation Cache_memory_helpers.h2w
        source_size) nonce_size).
