Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Script_int_repr.
Require TezosOfOCaml.Proto_alpha.Time_repr.

Definition t : Set := Z.t.

Definition compare : Z.t -> Z.t -> int := Z.compare.

Definition of_int64 : int64 -> Z.t := Z.of_int64.

Definition of_string (x_value : string) : option Z.t :=
  match Time_repr.of_notation x_value with
  | None =>
    Option.catch None
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Z.of_string x_value)
  | Some time => Some (of_int64 (Time_repr.to_seconds time))
  end.

Definition to_notation (x_value : Z.t) : option string :=
  Option.catch None
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Time_repr.to_notation (Time.of_seconds (Z.to_int64 x_value))).

Definition to_num_str : Z.t -> string := Z.to_string.

Definition to_string (x_value : Z.t) : string :=
  match to_notation x_value with
  | None => to_num_str x_value
  | Some s_value => s_value
  end.

Definition diff_value (x_value : Z.t) (y_value : Z.t) : Script_int_repr.num :=
  Script_int_repr.of_zint (x_value -Z y_value).

Definition sub_delta (t_value : Z.t) (delta : Script_int_repr.num) : Z.t :=
  t_value -Z (Script_int_repr.to_zint delta).

Definition add_delta (t_value : Z.t) (delta : Script_int_repr.num) : Z.t :=
  t_value +Z (Script_int_repr.to_zint delta).

Definition to_zint {A : Set} (x_value : A) : A := x_value.

Definition of_zint {A : Set} (x_value : A) : A := x_value.

Definition encoding : Data_encoding.encoding t := Data_encoding.z_value.
