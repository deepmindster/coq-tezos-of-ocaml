Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Blinded_public_key_hash.
Require TezosOfOCaml.Proto_alpha.Contract_repr.
Require TezosOfOCaml.Proto_alpha.Cycle_repr.
Require TezosOfOCaml.Proto_alpha.Tez_repr.

Inductive balance : Set :=
| Contract : Contract_repr.t -> balance
| Legacy_rewards : Signature.public_key_hash -> Cycle_repr.t -> balance
| Block_fees : balance
| Legacy_deposits : Signature.public_key_hash -> Cycle_repr.t -> balance
| Deposits : Signature.public_key_hash -> balance
| Nonce_revelation_rewards : balance
| Double_signing_evidence_rewards : balance
| Endorsing_rewards : balance
| Baking_rewards : balance
| Baking_bonuses : balance
| Legacy_fees : Signature.public_key_hash -> Cycle_repr.t -> balance
| Storage_fees : balance
| Double_signing_punishments : balance
| Lost_endorsing_rewards : Signature.public_key_hash -> bool -> bool -> balance
| Liquidity_baking_subsidies : balance
| Burned : balance
| Commitments : Blinded_public_key_hash.t -> balance
| Bootstrap : balance
| Invoice : balance
| Initial_commitments : balance
| Minted : balance.

Definition balance_encoding : Data_encoding.encoding balance :=
  (let arg := Data_encoding.def "operation_metadata.alpha.balance" in
  fun (eta : Data_encoding.encoding balance) => arg None None eta)
    (Data_encoding.union None
      [
        Data_encoding.case_value "Contract" None (Data_encoding.Tag 0)
          (Data_encoding.obj2
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "contract"))
            (Data_encoding.req None None "contract"
              Contract_repr.encoding))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Contract c_value => Some (tt, c_value)
            | _ => None
            end)
          (fun (function_parameter : unit * Contract_repr.contract) =>
            let '(_, c_value) := function_parameter in
            Contract c_value);
        Data_encoding.case_value "Legacy_rewards" None (Data_encoding.Tag 1)
          (Data_encoding.obj4
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "freezer"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "legacy_rewards"))
            (Data_encoding.req None None "delegate"
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
            (Data_encoding.req None None "cycle" Cycle_repr.encoding))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Legacy_rewards d_value l_value =>
              Some (tt, tt, d_value, l_value)
            | _ => None
            end)
          (fun (function_parameter :
            unit * unit * Signature.public_key_hash * Cycle_repr.cycle)
            =>
            let '(_, _, d_value, l_value) := function_parameter in
            Legacy_rewards d_value l_value);
        Data_encoding.case_value "Block_fees" None (Data_encoding.Tag 2)
          (Data_encoding.obj2
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "accumulator"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "block fees")))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Block_fees => Some (tt, tt)
            | _ => None
            end)
          (fun (function_parameter : unit * unit) =>
            let '(_, _) := function_parameter in
            Block_fees);
        Data_encoding.case_value "Legacy_deposits" None (Data_encoding.Tag 3)
          (Data_encoding.obj4
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "freezer"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "legacy_deposits"))
            (Data_encoding.req None None "delegate"
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
            (Data_encoding.req None None "cycle" Cycle_repr.encoding))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Legacy_deposits d_value l_value =>
              Some (tt, tt, d_value, l_value)
            | _ => None
            end)
          (fun (function_parameter :
            unit * unit * Signature.public_key_hash * Cycle_repr.cycle)
            =>
            let '(_, _, d_value, l_value) := function_parameter in
            Legacy_deposits d_value l_value);
        Data_encoding.case_value "Deposits" None (Data_encoding.Tag 4)
          (Data_encoding.obj3
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "freezer"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "deposits"))
            (Data_encoding.req None None "delegate"
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Deposits d_value => Some (tt, tt, d_value)
            | _ => None
            end)
          (fun (function_parameter :
            unit * unit * Signature.public_key_hash) =>
            let '(_, _, d_value) := function_parameter in
            Deposits d_value);
        Data_encoding.case_value "Nonce_revelation_rewards" None
          (Data_encoding.Tag 5)
          (Data_encoding.obj2
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "minted"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "nonce revelation rewards")))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Nonce_revelation_rewards => Some (tt, tt)
            | _ => None
            end)
          (fun (function_parameter : unit * unit) =>
            let '(_, _) := function_parameter in
            Nonce_revelation_rewards);
        Data_encoding.case_value "Double_signing_evidence_rewards" None
          (Data_encoding.Tag 6)
          (Data_encoding.obj2
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "minted"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant
                "double signing evidence rewards")))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Double_signing_evidence_rewards => Some (tt, tt)
            | _ => None
            end)
          (fun (function_parameter : unit * unit) =>
            let '(_, _) := function_parameter in
            Double_signing_evidence_rewards);
        Data_encoding.case_value "Endorsing_rewards" None (Data_encoding.Tag 7)
          (Data_encoding.obj2
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "minted"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "endorsing rewards")))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Endorsing_rewards => Some (tt, tt)
            | _ => None
            end)
          (fun (function_parameter : unit * unit) =>
            let '(_, _) := function_parameter in
            Endorsing_rewards);
        Data_encoding.case_value "Baking_rewards" None (Data_encoding.Tag 8)
          (Data_encoding.obj2
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "minted"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "baking rewards")))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Baking_rewards => Some (tt, tt)
            | _ => None
            end)
          (fun (function_parameter : unit * unit) =>
            let '(_, _) := function_parameter in
            Baking_rewards);
        Data_encoding.case_value "Baking_bonuses" None (Data_encoding.Tag 9)
          (Data_encoding.obj2
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "minted"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "baking bonuses")))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Baking_bonuses => Some (tt, tt)
            | _ => None
            end)
          (fun (function_parameter : unit * unit) =>
            let '(_, _) := function_parameter in
            Baking_bonuses);
        Data_encoding.case_value "Legacy_fees" None (Data_encoding.Tag 10)
          (Data_encoding.obj4
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "freezer"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "legacy_fees"))
            (Data_encoding.req None None "delegate"
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
            (Data_encoding.req None None "cycle" Cycle_repr.encoding))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Legacy_fees d_value l_value =>
              Some (tt, tt, d_value, l_value)
            | _ => None
            end)
          (fun (function_parameter :
            unit * unit * Signature.public_key_hash * Cycle_repr.cycle)
            =>
            let '(_, _, d_value, l_value) := function_parameter in
            Legacy_fees d_value l_value);
        Data_encoding.case_value "Storage_fees" None (Data_encoding.Tag 11)
          (Data_encoding.obj2
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "burned"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "storage fees")))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Storage_fees => Some (tt, tt)
            | _ => None
            end)
          (fun (function_parameter : unit * unit) =>
            let '(_, _) := function_parameter in
            Storage_fees);
        Data_encoding.case_value "Double_signing_punishments" None
          (Data_encoding.Tag 12)
          (Data_encoding.obj2
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "burned"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "punishments")))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Double_signing_punishments => Some (tt, tt)
            | _ => None
            end)
          (fun (function_parameter : unit * unit) =>
            let '(_, _) := function_parameter in
            Double_signing_punishments);
        Data_encoding.case_value "Lost_endorsing_rewards" None
          (Data_encoding.Tag 13)
          (Data_encoding.obj5
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "burned"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "lost endorsing rewards"))
            (Data_encoding.req None None "delegate"
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
            (Data_encoding.req None None "participation"
              Data_encoding.bool_value)
            (Data_encoding.req None None "revelation"
              Data_encoding.bool_value))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Lost_endorsing_rewards d_value p_value r_value =>
              Some (tt, tt, d_value, p_value, r_value)
            | _ => None
            end)
          (fun (function_parameter :
            unit * unit * Signature.public_key_hash * bool * bool) =>
            let '(_, _, d_value, p_value, r_value) := function_parameter
              in
            Lost_endorsing_rewards d_value p_value r_value);
        Data_encoding.case_value "Liquidity_baking_subsidies" None
          (Data_encoding.Tag 14)
          (Data_encoding.obj2
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "minted"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "subsidy")))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Liquidity_baking_subsidies => Some (tt, tt)
            | _ => None
            end)
          (fun (function_parameter : unit * unit) =>
            let '(_, _) := function_parameter in
            Liquidity_baking_subsidies);
        Data_encoding.case_value "Burned" None (Data_encoding.Tag 15)
          (Data_encoding.obj2
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "burned"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "burned")))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Burned => Some (tt, tt)
            | _ => None
            end)
          (fun (function_parameter : unit * unit) =>
            let '(_, _) := function_parameter in
            Burned);
        Data_encoding.case_value "Commitments" None (Data_encoding.Tag 16)
          (Data_encoding.obj3
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "commitment"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "commitment"))
            (Data_encoding.req None None "committer"
              Blinded_public_key_hash.encoding))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Commitments bpkh => Some (tt, tt, bpkh)
            | _ => None
            end)
          (fun (function_parameter :
            unit * unit * Blinded_public_key_hash.t) =>
            let '(_, _, bpkh) := function_parameter in
            Commitments bpkh);
        Data_encoding.case_value "Bootstrap" None (Data_encoding.Tag 17)
          (Data_encoding.obj2
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "minted"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "bootstrap")))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Bootstrap => Some (tt, tt)
            | _ => None
            end)
          (fun (function_parameter : unit * unit) =>
            let '(_, _) := function_parameter in
            Bootstrap);
        Data_encoding.case_value "Invoice" None (Data_encoding.Tag 18)
          (Data_encoding.obj2
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "minted"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "invoice")))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Invoice => Some (tt, tt)
            | _ => None
            end)
          (fun (function_parameter : unit * unit) =>
            let '(_, _) := function_parameter in
            Invoice);
        Data_encoding.case_value "Initial_commitments" None
          (Data_encoding.Tag 19)
          (Data_encoding.obj2
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "minted"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "commitment")))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Initial_commitments => Some (tt, tt)
            | _ => None
            end)
          (fun (function_parameter : unit * unit) =>
            let '(_, _) := function_parameter in
            Initial_commitments);
        Data_encoding.case_value "Minted" None (Data_encoding.Tag 20)
          (Data_encoding.obj2
            (Data_encoding.req None None "kind"
              (Data_encoding.constant "minted"))
            (Data_encoding.req None None "category"
              (Data_encoding.constant "minted")))
          (fun (function_parameter : balance) =>
            match function_parameter with
            | Minted => Some (tt, tt)
            | _ => None
            end)
          (fun (function_parameter : unit * unit) =>
            let '(_, _) := function_parameter in
            Minted)
      ]).

Definition is_not_zero (c_value : int) : bool :=
  Pervasives.not (Compare.Int.(Compare.S.equal) c_value 0).

Definition compare_balance (ba : balance) (bb : balance) : int :=
  match (ba, bb) with
  | (Contract ca, Contract cb) => Contract_repr.compare ca cb
  | (Legacy_rewards pkha ca, Legacy_rewards pkhb cb) =>
    let c_value :=
      Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.compare) pkha pkhb
      in
    if is_not_zero c_value then
      c_value
    else
      Cycle_repr.compare ca cb
  | (Legacy_deposits pkha ca, Legacy_deposits pkhb cb) =>
    let c_value :=
      Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.compare) pkha pkhb
      in
    if is_not_zero c_value then
      c_value
    else
      Cycle_repr.compare ca cb
  | (Deposits pkha, Deposits pkhb) =>
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.compare) pkha pkhb
  | (Lost_endorsing_rewards pkha pa ra, Lost_endorsing_rewards pkhb pb rb) =>
    let c_value :=
      Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.compare) pkha pkhb
      in
    if is_not_zero c_value then
      c_value
    else
      let c_value := Compare.Bool.(Compare.S.compare) pa pb in
      if is_not_zero c_value then
        c_value
      else
        Compare.Bool.(Compare.S.compare) ra rb
  | (Commitments bpkha, Commitments bpkhb) =>
    Blinded_public_key_hash.compare bpkha bpkhb
  | (Legacy_fees pkha ca, Legacy_fees pkhb cb) =>
    let c_value :=
      Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.compare) pkha pkhb
      in
    if is_not_zero c_value then
      c_value
    else
      Cycle_repr.compare ca cb
  | (_, _) =>
    let index (b_value : balance) : int :=
      match b_value with
      | Contract _ => 0
      | Legacy_rewards _ _ => 1
      | Block_fees => 2
      | Legacy_deposits _ _ => 3
      | Deposits _ => 4
      | Nonce_revelation_rewards => 5
      | Double_signing_evidence_rewards => 6
      | Endorsing_rewards => 7
      | Baking_rewards => 8
      | Baking_bonuses => 9
      | Legacy_fees _ _ => 10
      | Storage_fees => 11
      | Double_signing_punishments => 12
      | Lost_endorsing_rewards _ _ _ => 13
      | Liquidity_baking_subsidies => 14
      | Burned => 15
      | Commitments _ => 16
      | Bootstrap => 17
      | Invoice => 18
      | Initial_commitments => 19
      | Minted => 20
      end in
    Compare.Int.(Compare.S.compare) (index ba) (index bb)
  end.

Inductive balance_update : Set :=
| Debited : Tez_repr.t -> balance_update
| Credited : Tez_repr.t -> balance_update.

Definition balance_update_encoding : Data_encoding.encoding balance_update :=
  (let arg := Data_encoding.def "operation_metadata.alpha.balance_update" in
  fun (eta : Data_encoding.encoding balance_update) => arg None None eta)
    (Data_encoding.obj1
      (Data_encoding.req None None "change"
        (Data_encoding.conv
          (fun (function_parameter : balance_update) =>
            match function_parameter with
            | Credited v_value => Tez_repr.to_mutez v_value
            | Debited v_value => Int64.neg (Tez_repr.to_mutez v_value)
            end)
          (Data_encoding.Json.wrap_error
            (fun (v_value : int64) =>
              if v_value <i64 0 then
                match Tez_repr.of_mutez (Int64.neg v_value) with
                | Some v_value => Debited v_value
                | None =>
                  (* ❌ Assert instruction is not handled. *)
                  assert balance_update false
                end
              else
                match Tez_repr.of_mutez v_value with
                | Some v_value => Credited v_value
                | None =>
                  (* ❌ Assert instruction is not handled. *)
                  assert balance_update false
                end)) None Data_encoding.int64_value))).

Inductive update_origin : Set :=
| Block_application : update_origin
| Protocol_migration : update_origin
| Subsidy : update_origin
| Simulation : update_origin.

Definition compare_update_origin (oa : update_origin) (ob : update_origin)
  : int :=
  let index (o_value : update_origin) : int :=
    match o_value with
    | Block_application => 0
    | Protocol_migration => 1
    | Subsidy => 2
    | Simulation => 3
    end in
  Compare.Int.(Compare.S.compare) (index oa) (index ob).

Definition update_origin_encoding : Data_encoding.encoding update_origin :=
  (let arg := Data_encoding.def "operation_metadata.alpha.update_origin" in
  fun (eta : Data_encoding.encoding update_origin) => arg None None eta)
    (Data_encoding.obj1
      (Data_encoding.req None None "origin"
        (Data_encoding.union None
          [
            Data_encoding.case_value "Block_application" None
              (Data_encoding.Tag 0) (Data_encoding.constant "block")
              (fun (function_parameter : update_origin) =>
                match function_parameter with
                | Block_application => Some tt
                | _ => None
                end)
              (fun (function_parameter : unit) =>
                let '_ := function_parameter in
                Block_application);
            Data_encoding.case_value "Protocol_migration" None
              (Data_encoding.Tag 1) (Data_encoding.constant "migration")
              (fun (function_parameter : update_origin) =>
                match function_parameter with
                | Protocol_migration => Some tt
                | _ => None
                end)
              (fun (function_parameter : unit) =>
                let '_ := function_parameter in
                Protocol_migration);
            Data_encoding.case_value "Subsidy" None (Data_encoding.Tag 2)
              (Data_encoding.constant "subsidy")
              (fun (function_parameter : update_origin) =>
                match function_parameter with
                | Subsidy => Some tt
                | _ => None
                end)
              (fun (function_parameter : unit) =>
                let '_ := function_parameter in
                Subsidy);
            Data_encoding.case_value "Simulation" None (Data_encoding.Tag 3)
              (Data_encoding.constant "simulation")
              (fun (function_parameter : update_origin) =>
                match function_parameter with
                | Simulation => Some tt
                | _ => None
                end)
              (fun (function_parameter : unit) =>
                let '_ := function_parameter in
                Simulation)
          ]))).

Definition balance_updates : Set :=
  list (balance * balance_update * update_origin).

Definition balance_updates_encoding
  : Data_encoding.encoding (list (balance * balance_update * update_origin)) :=
  (let arg := Data_encoding.def "operation_metadata.alpha.balance_updates" in
  fun (eta :
    Data_encoding.encoding (list (balance * balance_update * update_origin))) =>
    arg None None eta)
    (Data_encoding.list_value None
      (Data_encoding.conv
        (fun (function_parameter : balance * balance_update * update_origin) =>
          let '(balance, balance_update, update_origin) := function_parameter in
          ((balance, balance_update), update_origin))
        (fun (function_parameter : (balance * balance_update) * update_origin)
          =>
          let '((balance, balance_update), update_origin) := function_parameter
            in
          (balance, balance_update, update_origin)) None
        (Data_encoding.merge_objs
          (Data_encoding.merge_objs balance_encoding balance_update_encoding)
          update_origin_encoding))).

Definition BalanceMap :=
  Map.Make
    (let t : Set := balance * update_origin in
    let compare (function_parameter : balance * update_origin)
      : balance * update_origin -> int :=
      let '(ba, ua) := function_parameter in
      fun (function_parameter : balance * update_origin) =>
        let '(bb, ub) := function_parameter in
        let c_value := compare_balance ba bb in
        if is_not_zero c_value then
          c_value
        else
          compare_update_origin ua ub in
    {|
      Compare.COMPARABLE.compare := compare
    |}).

Definition group_balance_updates
  (balance_updates : list (balance * balance_update * update_origin))
  : M? (list (balance * balance_update * update_origin)) :=
  let? map :=
    List.fold_left_e
      (fun (acc_value : BalanceMap.(Map.S.t) balance_update) =>
        fun (function_parameter : balance * balance_update * update_origin) =>
          let '(b_value, update, o_value) := function_parameter in
          let? function_parameter :=
            match BalanceMap.(Map.S.find) (b_value, o_value) acc_value with
            | None => return? update
            | Some present =>
              match (present, update) with
              |
                ((Credited a_value, Debited b_value) |
                (Debited b_value, Credited a_value)) =>
                if Tez_repr.op_gteq a_value b_value then
                  let? update := Tez_repr.op_minusquestion a_value b_value in
                  return? (Credited update)
                else
                  let? update := Tez_repr.op_minusquestion b_value a_value in
                  return? (Debited update)
              | (Credited a_value, Credited b_value) =>
                let? update := Tez_repr.op_plusquestion a_value b_value in
                return? (Credited update)
              | (Debited a_value, Debited b_value) =>
                let? update := Tez_repr.op_plusquestion a_value b_value in
                return? (Debited update)
              end
            end in
          match
            (function_parameter,
              match function_parameter with
              | Credited update => Tez_repr.op_eq update Tez_repr.zero
              | _ => false
              end) with
          | (Credited update, true) =>
            return? (BalanceMap.(Map.S.remove) (b_value, o_value) acc_value)
          | (update, _) =>
            return? (BalanceMap.(Map.S.add) (b_value, o_value) update acc_value)
          end) BalanceMap.(Map.S.empty) balance_updates in
  return?
    (BalanceMap.(Map.S.fold)
      (fun (function_parameter : balance * update_origin) =>
        let '(b_value, o_value) := function_parameter in
        fun (u_value : balance_update) =>
          fun (acc_value : list (balance * balance_update * update_origin)) =>
            cons (b_value, u_value, o_value) acc_value) map nil).
