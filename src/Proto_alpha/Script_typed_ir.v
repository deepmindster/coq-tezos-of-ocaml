Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Positivity Checking.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Cache_memory_helpers.
Require TezosOfOCaml.Proto_alpha.Saturation_repr.
Require TezosOfOCaml.Proto_alpha.Script_expr_hash.
Require TezosOfOCaml.Proto_alpha.Script_ir_annot.

Module step_constants.
  Record record : Set := Build {
    source : Alpha_context.Contract.t;
    payer : Alpha_context.Contract.t;
    self : Alpha_context.Contract.t;
    amount : Alpha_context.Tez.t;
    chain_id : Chain_id.t;
    now : Alpha_context.Script_timestamp.t;
    level : Alpha_context.Script_int.num }.
  Definition with_source source (r : record) :=
    Build source r.(payer) r.(self) r.(amount) r.(chain_id) r.(now) r.(level).
  Definition with_payer payer (r : record) :=
    Build r.(source) payer r.(self) r.(amount) r.(chain_id) r.(now) r.(level).
  Definition with_self self (r : record) :=
    Build r.(source) r.(payer) self r.(amount) r.(chain_id) r.(now) r.(level).
  Definition with_amount amount (r : record) :=
    Build r.(source) r.(payer) r.(self) amount r.(chain_id) r.(now) r.(level).
  Definition with_chain_id chain_id (r : record) :=
    Build r.(source) r.(payer) r.(self) r.(amount) chain_id r.(now) r.(level).
  Definition with_now now (r : record) :=
    Build r.(source) r.(payer) r.(self) r.(amount) r.(chain_id) now r.(level).
  Definition with_level level (r : record) :=
    Build r.(source) r.(payer) r.(self) r.(amount) r.(chain_id) r.(now) level.
End step_constants.
Definition step_constants := step_constants.record.

Inductive never : Set :=.

Definition address : Set := Alpha_context.Contract.t * string.

Definition pair (a b : Set) : Set := a * b.

Inductive union (a b : Set) : Set :=
| L : a -> union a b
| R : b -> union a b.

Arguments L {_ _}.
Arguments R {_ _}.

Definition operation : Set :=
  Alpha_context.packed_internal_operation *
    option Alpha_context.Lazy_storage.diffs.

Module ticket.
  Record record {a : Set} : Set := Build {
    ticketer : Alpha_context.Contract.t;
    contents : a;
    amount : Alpha_context.Script_int.num }.
  Arguments record : clear implicits.
  Definition with_ticketer {t_a} ticketer (r : record t_a) :=
    Build t_a ticketer r.(contents) r.(amount).
  Definition with_contents {t_a} contents (r : record t_a) :=
    Build t_a r.(ticketer) contents r.(amount).
  Definition with_amount {t_a} amount (r : record t_a) :=
    Build t_a r.(ticketer) r.(contents) amount.
End ticket.
Definition ticket := ticket.record.

Module TYPE_SIZE.
  Record signature {t : Set} : Set := {
    t := t;
    merge : t -> t -> M? t;
    to_int : t -> Saturation_repr.t;
    one : t;
    two : t;
    three : t;
    four : t;
    compound1 : Alpha_context.Script.location -> t -> M? t;
    compound2 : Alpha_context.Script.location -> t -> t -> M? t;
  }.
End TYPE_SIZE.
Definition TYPE_SIZE := @TYPE_SIZE.signature.
Arguments TYPE_SIZE {_}.

Module Type_size.
  Definition t : Set := int.
  
  (** Init function; without side-effects in Coq *)
  Definition init_module : unit :=
    let '_ :=
      Saturation_repr.mul_safe_of_int_exn
        Alpha_context.Constants.michelson_maximum_type_size in
    tt.
  
  Definition to_int : int -> Saturation_repr.t :=
    Saturation_repr.mul_safe_of_int_exn.
  
  Definition one : int := 1.
  
  Definition two : int := 2.
  
  Definition three : int := 3.
  
  Definition four : int := 4.
  
  Definition merge (x_value : int) (y_value : int) : M? int :=
    if x_value =i y_value then
      return? x_value
    else
      Error_monad.error_value
        (Build_extensible "Inconsistent_type_sizes" (int * int)
          (x_value, y_value)).
  
  Definition of_int (loc : Alpha_context.Script.location) (size_value : int)
    : M? int :=
    let max_size := Alpha_context.Constants.michelson_maximum_type_size in
    if size_value <=i max_size then
      return? size_value
    else
      Error_monad.error_value
        (Build_extensible "Type_too_large" (Alpha_context.Script.location * int)
          (loc, max_size)).
  
  Definition compound1 (loc : Alpha_context.Script.location) (size_value : int)
    : M? int := of_int loc (1 +i size_value).
  
  Definition compound2
    (loc : Alpha_context.Script.location) (size1 : int) (size2 : int)
    : M? int := of_int loc ((1 +i size1) +i size2).
  
  Definition module :=
    {|
      TYPE_SIZE.merge := merge;
      TYPE_SIZE.to_int := to_int;
      TYPE_SIZE.one := one;
      TYPE_SIZE.two := two;
      TYPE_SIZE.three := three;
      TYPE_SIZE.four := four;
      TYPE_SIZE.compound1 := compound1;
      TYPE_SIZE.compound2 := compound2
    |}.
End Type_size.
Definition Type_size : TYPE_SIZE (t := _) := Type_size.module.

Inductive empty_cell : Set :=
| EmptyCell : empty_cell.

Definition end_of_stack : Set := empty_cell * empty_cell.

Module ty_metadata.
  Record record : Set := Build {
    annot : option Script_ir_annot.type_annot;
    size : Type_size.(TYPE_SIZE.t) }.
  Definition with_annot annot (r : record) :=
    Build annot r.(size).
  Definition with_size size (r : record) :=
    Build r.(annot) size.
End ty_metadata.
Definition ty_metadata := ty_metadata.record.

Inductive comparable_ty : Set :=
| Unit_key : ty_metadata -> comparable_ty
| Never_key : ty_metadata -> comparable_ty
| Int_key : ty_metadata -> comparable_ty
| Nat_key : ty_metadata -> comparable_ty
| Signature_key : ty_metadata -> comparable_ty
| String_key : ty_metadata -> comparable_ty
| Bytes_key : ty_metadata -> comparable_ty
| Mutez_key : ty_metadata -> comparable_ty
| Bool_key : ty_metadata -> comparable_ty
| Key_hash_key : ty_metadata -> comparable_ty
| Key_key : ty_metadata -> comparable_ty
| Timestamp_key : ty_metadata -> comparable_ty
| Chain_id_key : ty_metadata -> comparable_ty
| Address_key : ty_metadata -> comparable_ty
| Pair_key :
  comparable_ty * option Script_ir_annot.field_annot ->
  comparable_ty * option Script_ir_annot.field_annot -> ty_metadata ->
  comparable_ty
| Union_key :
  comparable_ty * option Script_ir_annot.field_annot ->
  comparable_ty * option Script_ir_annot.field_annot -> ty_metadata ->
  comparable_ty
| Option_key : comparable_ty -> ty_metadata -> comparable_ty.

Definition comparable_ty_metadata (function_parameter : comparable_ty)
  : ty_metadata :=
  match function_parameter with
  | Unit_key meta => meta
  | Never_key meta => meta
  | Int_key meta => meta
  | Nat_key meta => meta
  | Signature_key meta => meta
  | String_key meta => meta
  | Bytes_key meta => meta
  | Mutez_key meta => meta
  | Bool_key meta => meta
  | Key_hash_key meta => meta
  | Key_key meta => meta
  | Timestamp_key meta => meta
  | Chain_id_key meta => meta
  | Address_key meta => meta
  | Pair_key _ _ meta => meta
  | Union_key _ _ meta => meta
  | Option_key _ meta => meta
  end.

Definition comparable_ty_size (t_value : comparable_ty)
  : Type_size.(TYPE_SIZE.t) :=
  (comparable_ty_metadata t_value).(ty_metadata.size).

Definition unit_key (annot : option Script_ir_annot.type_annot)
  : comparable_ty :=
  Unit_key
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition never_key (annot : option Script_ir_annot.type_annot)
  : comparable_ty :=
  Never_key
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition int_key (annot : option Script_ir_annot.type_annot)
  : comparable_ty :=
  Int_key
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition nat_key (annot : option Script_ir_annot.type_annot)
  : comparable_ty :=
  Nat_key
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition signature_key (annot : option Script_ir_annot.type_annot)
  : comparable_ty :=
  Signature_key
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition string_key (annot : option Script_ir_annot.type_annot)
  : comparable_ty :=
  String_key
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition bytes_key (annot : option Script_ir_annot.type_annot)
  : comparable_ty :=
  Bytes_key
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition mutez_key (annot : option Script_ir_annot.type_annot)
  : comparable_ty :=
  Mutez_key
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition bool_key (annot : option Script_ir_annot.type_annot)
  : comparable_ty :=
  Bool_key
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition key_hash_key (annot : option Script_ir_annot.type_annot)
  : comparable_ty :=
  Key_hash_key
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition key_key (annot : option Script_ir_annot.type_annot)
  : comparable_ty :=
  Key_key
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition timestamp_key (annot : option Script_ir_annot.type_annot)
  : comparable_ty :=
  Timestamp_key
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition chain_id_key (annot : option Script_ir_annot.type_annot)
  : comparable_ty :=
  Chain_id_key
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition address_key (annot : option Script_ir_annot.type_annot)
  : comparable_ty :=
  Address_key
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition pair_key
  (loc : Alpha_context.Script.location)
  (function_parameter : comparable_ty * option Script_ir_annot.field_annot)
  : comparable_ty * option Script_ir_annot.field_annot ->
  option Script_ir_annot.type_annot -> M? comparable_ty :=
  let '(l_value, fannot_l) := function_parameter in
  fun (function_parameter : comparable_ty * option Script_ir_annot.field_annot)
    =>
    let '(r_value, fannot_r) := function_parameter in
    fun (annot : option Script_ir_annot.type_annot) =>
      let? size_value :=
        Type_size.(TYPE_SIZE.compound2) loc (comparable_ty_size l_value)
          (comparable_ty_size r_value) in
      return?
        (Pair_key (l_value, fannot_l) (r_value, fannot_r)
          {| ty_metadata.annot := annot; ty_metadata.size := size_value |}).

Definition pair_3_key
  (loc : Alpha_context.Script.location)
  (l_value : comparable_ty * option Script_ir_annot.field_annot)
  (m_value : comparable_ty * option Script_ir_annot.field_annot)
  (r_value : comparable_ty * option Script_ir_annot.field_annot)
  : M? comparable_ty :=
  let? r_value := pair_key loc m_value r_value None in
  pair_key loc l_value (r_value, None) None.

Definition union_key
  (loc : Alpha_context.Script.location)
  (function_parameter : comparable_ty * option Script_ir_annot.field_annot)
  : comparable_ty * option Script_ir_annot.field_annot ->
  option Script_ir_annot.type_annot -> M? comparable_ty :=
  let '(l_value, fannot_l) := function_parameter in
  fun (function_parameter : comparable_ty * option Script_ir_annot.field_annot)
    =>
    let '(r_value, fannot_r) := function_parameter in
    fun (annot : option Script_ir_annot.type_annot) =>
      let? size_value :=
        Type_size.(TYPE_SIZE.compound2) loc (comparable_ty_size l_value)
          (comparable_ty_size r_value) in
      return?
        (Union_key (l_value, fannot_l) (r_value, fannot_r)
          {| ty_metadata.annot := annot; ty_metadata.size := size_value |}).

Definition option_key
  (loc : Alpha_context.Script.location) (t_value : comparable_ty)
  (annot : option Script_ir_annot.type_annot) : M? comparable_ty :=
  let? size_value :=
    Type_size.(TYPE_SIZE.compound1) loc (comparable_ty_size t_value) in
  return?
    (Option_key t_value
      {| ty_metadata.annot := annot; ty_metadata.size := size_value |}).

Module Boxed_set_OPS.
  Record signature {t elt : Set} : Set := {
    t := t;
    elt := elt;
    empty : t;
    add : elt -> t -> t;
    mem : elt -> t -> bool;
    remove : elt -> t -> t;
    fold : forall {a : Set}, (elt -> a -> a) -> t -> a -> a;
  }.
End Boxed_set_OPS.
Definition Boxed_set_OPS := @Boxed_set_OPS.signature.
Arguments Boxed_set_OPS {_ _}.

Module Boxed_set.
  Record signature {elt OPS_t : Set} : Set := {
    elt := elt;
    elt_ty : comparable_ty;
    OPS : Boxed_set_OPS (t := OPS_t) (elt := elt);
    boxed : OPS.(Boxed_set_OPS.t);
    size_value : int;
  }.
End Boxed_set.
Definition Boxed_set := @Boxed_set.signature.
Arguments Boxed_set {_ _}.

Definition set (elt : Set) : Set :=
  {OPS_t : Set @ Boxed_set (elt := elt) (OPS_t := OPS_t)}.

Module Boxed_map_OPS.
  Record signature {t key value : Set} : Set := {
    t := t;
    key := key;
    value := value;
    empty : t;
    add : key -> value -> t -> t;
    remove : key -> t -> t;
    find : key -> t -> option value;
    fold : forall {a : Set}, (key -> value -> a -> a) -> t -> a -> a;
  }.
End Boxed_map_OPS.
Definition Boxed_map_OPS := @Boxed_map_OPS.signature.
Arguments Boxed_map_OPS {_ _ _}.

Module Boxed_map.
  Record signature {key value OPS_t : Set} : Set := {
    key := key;
    value := value;
    key_ty : comparable_ty;
    OPS : Boxed_map_OPS (t := OPS_t) (key := key) (value := value);
    boxed : OPS.(Boxed_map_OPS.t);
    size_value : int;
  }.
End Boxed_map.
Definition Boxed_map := @Boxed_map.signature.
Arguments Boxed_map {_ _ _}.

Definition map (key value : Set) : Set :=
  {OPS_t : Set @ Boxed_map (key := key) (value := value) (OPS_t := OPS_t)}.

Definition Big_map_overlay :=
  Map.Make
    (let t : Set := Script_expr_hash.t in
    let compare := Script_expr_hash.compare in
    {|
      Compare.COMPARABLE.compare := compare
    |}).

Module big_map_overlay.
  Record record {key value : Set} : Set := Build {
    map : Big_map_overlay.(Map.S.t) (key * option value);
    size : int }.
  Arguments record : clear implicits.
  Definition with_map {t_key t_value} map (r : record t_key t_value) :=
    Build t_key t_value map r.(size).
  Definition with_size {t_key t_value} size (r : record t_key t_value) :=
    Build t_key t_value r.(map) size.
End big_map_overlay.
Definition big_map_overlay := big_map_overlay.record.

Module boxed_list.
  Record record {elt : Set} : Set := Build {
    elements : list elt;
    length : int }.
  Arguments record : clear implicits.
  Definition with_elements {t_elt} elements (r : record t_elt) :=
    Build t_elt elements r.(length).
  Definition with_length {t_elt} length (r : record t_elt) :=
    Build t_elt r.(elements) length.
End boxed_list.
Definition boxed_list := boxed_list.record.

Definition SMap :=
  Map.Make
    {|
      Compare.COMPARABLE.compare := Alpha_context.Script_string.compare
    |}.

Module view.
  Record record : Set := Build {
    input_ty : Alpha_context.Script.node;
    output_ty : Alpha_context.Script.node;
    view_code : Alpha_context.Script.node }.
  Definition with_input_ty input_ty (r : record) :=
    Build input_ty r.(output_ty) r.(view_code).
  Definition with_output_ty output_ty (r : record) :=
    Build r.(input_ty) output_ty r.(view_code).
  Definition with_view_code view_code (r : record) :=
    Build r.(input_ty) r.(output_ty) view_code.
End view.
Definition view := view.record.

(** Records for the constructor parameters *)
Module ConstructorRecords_kinstr_view_signature.
  Module kinstr.
    Module IIf_none.
      Record record {kinfo branch_if_none branch_if_some k : Set} : Set := Build {
        kinfo : kinfo;
        branch_if_none : branch_if_none;
        branch_if_some : branch_if_some;
        k : k }.
      Arguments record : clear implicits.
      Definition with_kinfo {t_kinfo t_branch_if_none t_branch_if_some t_k}
        kinfo (r : record t_kinfo t_branch_if_none t_branch_if_some t_k) :=
        Build t_kinfo t_branch_if_none t_branch_if_some t_k kinfo
          r.(branch_if_none) r.(branch_if_some) r.(k).
      Definition with_branch_if_none
        {t_kinfo t_branch_if_none t_branch_if_some t_k} branch_if_none
        (r : record t_kinfo t_branch_if_none t_branch_if_some t_k) :=
        Build t_kinfo t_branch_if_none t_branch_if_some t_k r.(kinfo)
          branch_if_none r.(branch_if_some) r.(k).
      Definition with_branch_if_some
        {t_kinfo t_branch_if_none t_branch_if_some t_k} branch_if_some
        (r : record t_kinfo t_branch_if_none t_branch_if_some t_k) :=
        Build t_kinfo t_branch_if_none t_branch_if_some t_k r.(kinfo)
          r.(branch_if_none) branch_if_some r.(k).
      Definition with_k {t_kinfo t_branch_if_none t_branch_if_some t_k} k
        (r : record t_kinfo t_branch_if_none t_branch_if_some t_k) :=
        Build t_kinfo t_branch_if_none t_branch_if_some t_k r.(kinfo)
          r.(branch_if_none) r.(branch_if_some) k.
    End IIf_none.
    Definition IIf_none_skeleton := IIf_none.record.
    
    Module IOpt_map.
      Record record {kinfo body k : Set} : Set := Build {
        kinfo : kinfo;
        body : body;
        k : k }.
      Arguments record : clear implicits.
      Definition with_kinfo {t_kinfo t_body t_k} kinfo
        (r : record t_kinfo t_body t_k) :=
        Build t_kinfo t_body t_k kinfo r.(body) r.(k).
      Definition with_body {t_kinfo t_body t_k} body
        (r : record t_kinfo t_body t_k) :=
        Build t_kinfo t_body t_k r.(kinfo) body r.(k).
      Definition with_k {t_kinfo t_body t_k} k
        (r : record t_kinfo t_body t_k) :=
        Build t_kinfo t_body t_k r.(kinfo) r.(body) k.
    End IOpt_map.
    Definition IOpt_map_skeleton := IOpt_map.record.
    
    Module IIf_left.
      Record record {kinfo branch_if_left branch_if_right k : Set} : Set := Build {
        kinfo : kinfo;
        branch_if_left : branch_if_left;
        branch_if_right : branch_if_right;
        k : k }.
      Arguments record : clear implicits.
      Definition with_kinfo {t_kinfo t_branch_if_left t_branch_if_right t_k}
        kinfo (r : record t_kinfo t_branch_if_left t_branch_if_right t_k) :=
        Build t_kinfo t_branch_if_left t_branch_if_right t_k kinfo
          r.(branch_if_left) r.(branch_if_right) r.(k).
      Definition with_branch_if_left
        {t_kinfo t_branch_if_left t_branch_if_right t_k} branch_if_left
        (r : record t_kinfo t_branch_if_left t_branch_if_right t_k) :=
        Build t_kinfo t_branch_if_left t_branch_if_right t_k r.(kinfo)
          branch_if_left r.(branch_if_right) r.(k).
      Definition with_branch_if_right
        {t_kinfo t_branch_if_left t_branch_if_right t_k} branch_if_right
        (r : record t_kinfo t_branch_if_left t_branch_if_right t_k) :=
        Build t_kinfo t_branch_if_left t_branch_if_right t_k r.(kinfo)
          r.(branch_if_left) branch_if_right r.(k).
      Definition with_k {t_kinfo t_branch_if_left t_branch_if_right t_k} k
        (r : record t_kinfo t_branch_if_left t_branch_if_right t_k) :=
        Build t_kinfo t_branch_if_left t_branch_if_right t_k r.(kinfo)
          r.(branch_if_left) r.(branch_if_right) k.
    End IIf_left.
    Definition IIf_left_skeleton := IIf_left.record.
    
    Module IIf_cons.
      Record record {kinfo branch_if_cons branch_if_nil k : Set} : Set := Build {
        kinfo : kinfo;
        branch_if_cons : branch_if_cons;
        branch_if_nil : branch_if_nil;
        k : k }.
      Arguments record : clear implicits.
      Definition with_kinfo {t_kinfo t_branch_if_cons t_branch_if_nil t_k} kinfo
        (r : record t_kinfo t_branch_if_cons t_branch_if_nil t_k) :=
        Build t_kinfo t_branch_if_cons t_branch_if_nil t_k kinfo
          r.(branch_if_cons) r.(branch_if_nil) r.(k).
      Definition with_branch_if_cons
        {t_kinfo t_branch_if_cons t_branch_if_nil t_k} branch_if_cons
        (r : record t_kinfo t_branch_if_cons t_branch_if_nil t_k) :=
        Build t_kinfo t_branch_if_cons t_branch_if_nil t_k r.(kinfo)
          branch_if_cons r.(branch_if_nil) r.(k).
      Definition with_branch_if_nil
        {t_kinfo t_branch_if_cons t_branch_if_nil t_k} branch_if_nil
        (r : record t_kinfo t_branch_if_cons t_branch_if_nil t_k) :=
        Build t_kinfo t_branch_if_cons t_branch_if_nil t_k r.(kinfo)
          r.(branch_if_cons) branch_if_nil r.(k).
      Definition with_k {t_kinfo t_branch_if_cons t_branch_if_nil t_k} k
        (r : record t_kinfo t_branch_if_cons t_branch_if_nil t_k) :=
        Build t_kinfo t_branch_if_cons t_branch_if_nil t_k r.(kinfo)
          r.(branch_if_cons) r.(branch_if_nil) k.
    End IIf_cons.
    Definition IIf_cons_skeleton := IIf_cons.record.
    
    Module IIf.
      Record record {kinfo branch_if_true branch_if_false k : Set} : Set := Build {
        kinfo : kinfo;
        branch_if_true : branch_if_true;
        branch_if_false : branch_if_false;
        k : k }.
      Arguments record : clear implicits.
      Definition with_kinfo {t_kinfo t_branch_if_true t_branch_if_false t_k}
        kinfo (r : record t_kinfo t_branch_if_true t_branch_if_false t_k) :=
        Build t_kinfo t_branch_if_true t_branch_if_false t_k kinfo
          r.(branch_if_true) r.(branch_if_false) r.(k).
      Definition with_branch_if_true
        {t_kinfo t_branch_if_true t_branch_if_false t_k} branch_if_true
        (r : record t_kinfo t_branch_if_true t_branch_if_false t_k) :=
        Build t_kinfo t_branch_if_true t_branch_if_false t_k r.(kinfo)
          branch_if_true r.(branch_if_false) r.(k).
      Definition with_branch_if_false
        {t_kinfo t_branch_if_true t_branch_if_false t_k} branch_if_false
        (r : record t_kinfo t_branch_if_true t_branch_if_false t_k) :=
        Build t_kinfo t_branch_if_true t_branch_if_false t_k r.(kinfo)
          r.(branch_if_true) branch_if_false r.(k).
      Definition with_k {t_kinfo t_branch_if_true t_branch_if_false t_k} k
        (r : record t_kinfo t_branch_if_true t_branch_if_false t_k) :=
        Build t_kinfo t_branch_if_true t_branch_if_false t_k r.(kinfo)
          r.(branch_if_true) r.(branch_if_false) k.
    End IIf.
    Definition IIf_skeleton := IIf.record.
    
    Module ICreate_contract.
      Record record {kinfo storage_type arg_type lambda views root_name k : Set} :
        Set := Build {
        kinfo : kinfo;
        storage_type : storage_type;
        arg_type : arg_type;
        lambda : lambda;
        views : views;
        root_name : root_name;
        k : k }.
      Arguments record : clear implicits.
      Definition with_kinfo
        {t_kinfo t_storage_type t_arg_type t_lambda t_views t_root_name t_k}
        kinfo
        (r :
          record t_kinfo t_storage_type t_arg_type t_lambda t_views t_root_name
            t_k) :=
        Build t_kinfo t_storage_type t_arg_type t_lambda t_views t_root_name t_k
          kinfo r.(storage_type) r.(arg_type) r.(lambda) r.(views) r.(root_name)
          r.(k).
      Definition with_storage_type
        {t_kinfo t_storage_type t_arg_type t_lambda t_views t_root_name t_k}
        storage_type
        (r :
          record t_kinfo t_storage_type t_arg_type t_lambda t_views t_root_name
            t_k) :=
        Build t_kinfo t_storage_type t_arg_type t_lambda t_views t_root_name t_k
          r.(kinfo) storage_type r.(arg_type) r.(lambda) r.(views) r.(root_name)
          r.(k).
      Definition with_arg_type
        {t_kinfo t_storage_type t_arg_type t_lambda t_views t_root_name t_k}
        arg_type
        (r :
          record t_kinfo t_storage_type t_arg_type t_lambda t_views t_root_name
            t_k) :=
        Build t_kinfo t_storage_type t_arg_type t_lambda t_views t_root_name t_k
          r.(kinfo) r.(storage_type) arg_type r.(lambda) r.(views) r.(root_name)
          r.(k).
      Definition with_lambda
        {t_kinfo t_storage_type t_arg_type t_lambda t_views t_root_name t_k}
        lambda
        (r :
          record t_kinfo t_storage_type t_arg_type t_lambda t_views t_root_name
            t_k) :=
        Build t_kinfo t_storage_type t_arg_type t_lambda t_views t_root_name t_k
          r.(kinfo) r.(storage_type) r.(arg_type) lambda r.(views) r.(root_name)
          r.(k).
      Definition with_views
        {t_kinfo t_storage_type t_arg_type t_lambda t_views t_root_name t_k}
        views
        (r :
          record t_kinfo t_storage_type t_arg_type t_lambda t_views t_root_name
            t_k) :=
        Build t_kinfo t_storage_type t_arg_type t_lambda t_views t_root_name t_k
          r.(kinfo) r.(storage_type) r.(arg_type) r.(lambda) views r.(root_name)
          r.(k).
      Definition with_root_name
        {t_kinfo t_storage_type t_arg_type t_lambda t_views t_root_name t_k}
        root_name
        (r :
          record t_kinfo t_storage_type t_arg_type t_lambda t_views t_root_name
            t_k) :=
        Build t_kinfo t_storage_type t_arg_type t_lambda t_views t_root_name t_k
          r.(kinfo) r.(storage_type) r.(arg_type) r.(lambda) r.(views) root_name
          r.(k).
      Definition with_k
        {t_kinfo t_storage_type t_arg_type t_lambda t_views t_root_name t_k} k
        (r :
          record t_kinfo t_storage_type t_arg_type t_lambda t_views t_root_name
            t_k) :=
        Build t_kinfo t_storage_type t_arg_type t_lambda t_views t_root_name t_k
          r.(kinfo) r.(storage_type) r.(arg_type) r.(lambda) r.(views)
          r.(root_name) k.
    End ICreate_contract.
    Definition ICreate_contract_skeleton := ICreate_contract.record.
  End kinstr.
  Module view_signature.
    Module View_signature.
      Record record {name input_ty output_ty : Set} : Set := Build {
        name : name;
        input_ty : input_ty;
        output_ty : output_ty }.
      Arguments record : clear implicits.
      Definition with_name {t_name t_input_ty t_output_ty} name
        (r : record t_name t_input_ty t_output_ty) :=
        Build t_name t_input_ty t_output_ty name r.(input_ty) r.(output_ty).
      Definition with_input_ty {t_name t_input_ty t_output_ty} input_ty
        (r : record t_name t_input_ty t_output_ty) :=
        Build t_name t_input_ty t_output_ty r.(name) input_ty r.(output_ty).
      Definition with_output_ty {t_name t_input_ty t_output_ty} output_ty
        (r : record t_name t_input_ty t_output_ty) :=
        Build t_name t_input_ty t_output_ty r.(name) r.(input_ty) output_ty.
    End View_signature.
    Definition View_signature_skeleton := View_signature.record.
  End view_signature.
End ConstructorRecords_kinstr_view_signature.
Import ConstructorRecords_kinstr_view_signature.

Module kinfo.
  Record record {iloc kstack_ty : Set} : Set := Build {
    iloc : iloc;
    kstack_ty : kstack_ty }.
  Arguments record : clear implicits.
  Definition with_iloc {t_iloc t_kstack_ty} iloc
    (r : record t_iloc t_kstack_ty) :=
    Build t_iloc t_kstack_ty iloc r.(kstack_ty).
  Definition with_kstack_ty {t_iloc t_kstack_ty} kstack_ty
    (r : record t_iloc t_kstack_ty) :=
    Build t_iloc t_kstack_ty r.(iloc) kstack_ty.
End kinfo.
Definition kinfo_skeleton := kinfo.record.

Module kdescr.
  Record record {kloc kbef kaft kinstr : Set} : Set := Build {
    kloc : kloc;
    kbef : kbef;
    kaft : kaft;
    kinstr : kinstr }.
  Arguments record : clear implicits.
  Definition with_kloc {t_kloc t_kbef t_kaft t_kinstr} kloc
    (r : record t_kloc t_kbef t_kaft t_kinstr) :=
    Build t_kloc t_kbef t_kaft t_kinstr kloc r.(kbef) r.(kaft) r.(kinstr).
  Definition with_kbef {t_kloc t_kbef t_kaft t_kinstr} kbef
    (r : record t_kloc t_kbef t_kaft t_kinstr) :=
    Build t_kloc t_kbef t_kaft t_kinstr r.(kloc) kbef r.(kaft) r.(kinstr).
  Definition with_kaft {t_kloc t_kbef t_kaft t_kinstr} kaft
    (r : record t_kloc t_kbef t_kaft t_kinstr) :=
    Build t_kloc t_kbef t_kaft t_kinstr r.(kloc) r.(kbef) kaft r.(kinstr).
  Definition with_kinstr {t_kloc t_kbef t_kaft t_kinstr} kinstr
    (r : record t_kloc t_kbef t_kaft t_kinstr) :=
    Build t_kloc t_kbef t_kaft t_kinstr r.(kloc) r.(kbef) r.(kaft) kinstr.
End kdescr.
Definition kdescr_skeleton := kdescr.record.

Module big_map.
  Record record {id diff key_type value_type : Set} : Set := Build {
    id : id;
    diff : diff;
    key_type : key_type;
    value_type : value_type }.
  Arguments record : clear implicits.
  Definition with_id {t_id t_diff t_key_type t_value_type} id
    (r : record t_id t_diff t_key_type t_value_type) :=
    Build t_id t_diff t_key_type t_value_type id r.(diff) r.(key_type)
      r.(value_type).
  Definition with_diff {t_id t_diff t_key_type t_value_type} diff
    (r : record t_id t_diff t_key_type t_value_type) :=
    Build t_id t_diff t_key_type t_value_type r.(id) diff r.(key_type)
      r.(value_type).
  Definition with_key_type {t_id t_diff t_key_type t_value_type} key_type
    (r : record t_id t_diff t_key_type t_value_type) :=
    Build t_id t_diff t_key_type t_value_type r.(id) r.(diff) key_type
      r.(value_type).
  Definition with_value_type {t_id t_diff t_key_type t_value_type} value_type
    (r : record t_id t_diff t_key_type t_value_type) :=
    Build t_id t_diff t_key_type t_value_type r.(id) r.(diff) r.(key_type)
      value_type.
End big_map.
Definition big_map_skeleton := big_map.record.

Module logger.
  Record record {log_interp log_entry log_control log_exit get_log : Set} : Set := Build {
    log_interp : log_interp;
    log_entry : log_entry;
    log_control : log_control;
    log_exit : log_exit;
    get_log : get_log }.
  Arguments record : clear implicits.
  Definition with_log_interp
    {t_log_interp t_log_entry t_log_control t_log_exit t_get_log} log_interp
    (r : record t_log_interp t_log_entry t_log_control t_log_exit t_get_log) :=
    Build t_log_interp t_log_entry t_log_control t_log_exit t_get_log log_interp
      r.(log_entry) r.(log_control) r.(log_exit) r.(get_log).
  Definition with_log_entry
    {t_log_interp t_log_entry t_log_control t_log_exit t_get_log} log_entry
    (r : record t_log_interp t_log_entry t_log_control t_log_exit t_get_log) :=
    Build t_log_interp t_log_entry t_log_control t_log_exit t_get_log
      r.(log_interp) log_entry r.(log_control) r.(log_exit) r.(get_log).
  Definition with_log_control
    {t_log_interp t_log_entry t_log_control t_log_exit t_get_log} log_control
    (r : record t_log_interp t_log_entry t_log_control t_log_exit t_get_log) :=
    Build t_log_interp t_log_entry t_log_control t_log_exit t_get_log
      r.(log_interp) r.(log_entry) log_control r.(log_exit) r.(get_log).
  Definition with_log_exit
    {t_log_interp t_log_entry t_log_control t_log_exit t_get_log} log_exit
    (r : record t_log_interp t_log_entry t_log_control t_log_exit t_get_log) :=
    Build t_log_interp t_log_entry t_log_control t_log_exit t_get_log
      r.(log_interp) r.(log_entry) r.(log_control) log_exit r.(get_log).
  Definition with_get_log
    {t_log_interp t_log_entry t_log_control t_log_exit t_get_log} get_log
    (r : record t_log_interp t_log_entry t_log_control t_log_exit t_get_log) :=
    Build t_log_interp t_log_entry t_log_control t_log_exit t_get_log
      r.(log_interp) r.(log_entry) r.(log_control) r.(log_exit) get_log.
End logger.
Definition logger_skeleton := logger.record.

Module script.
  Record record {code arg_type storage storage_type views root_name code_size :
    Set} : Set := Build {
    code : code;
    arg_type : arg_type;
    storage : storage;
    storage_type : storage_type;
    views : views;
    root_name : root_name;
    code_size : code_size }.
  Arguments record : clear implicits.
  Definition with_code
    {t_code t_arg_type t_storage t_storage_type t_views t_root_name t_code_size}
    code
    (r :
      record t_code t_arg_type t_storage t_storage_type t_views t_root_name
        t_code_size) :=
    Build t_code t_arg_type t_storage t_storage_type t_views t_root_name
      t_code_size code r.(arg_type) r.(storage) r.(storage_type) r.(views)
      r.(root_name) r.(code_size).
  Definition with_arg_type
    {t_code t_arg_type t_storage t_storage_type t_views t_root_name t_code_size}
    arg_type
    (r :
      record t_code t_arg_type t_storage t_storage_type t_views t_root_name
        t_code_size) :=
    Build t_code t_arg_type t_storage t_storage_type t_views t_root_name
      t_code_size r.(code) arg_type r.(storage) r.(storage_type) r.(views)
      r.(root_name) r.(code_size).
  Definition with_storage
    {t_code t_arg_type t_storage t_storage_type t_views t_root_name t_code_size}
    storage
    (r :
      record t_code t_arg_type t_storage t_storage_type t_views t_root_name
        t_code_size) :=
    Build t_code t_arg_type t_storage t_storage_type t_views t_root_name
      t_code_size r.(code) r.(arg_type) storage r.(storage_type) r.(views)
      r.(root_name) r.(code_size).
  Definition with_storage_type
    {t_code t_arg_type t_storage t_storage_type t_views t_root_name t_code_size}
    storage_type
    (r :
      record t_code t_arg_type t_storage t_storage_type t_views t_root_name
        t_code_size) :=
    Build t_code t_arg_type t_storage t_storage_type t_views t_root_name
      t_code_size r.(code) r.(arg_type) r.(storage) storage_type r.(views)
      r.(root_name) r.(code_size).
  Definition with_views
    {t_code t_arg_type t_storage t_storage_type t_views t_root_name t_code_size}
    views
    (r :
      record t_code t_arg_type t_storage t_storage_type t_views t_root_name
        t_code_size) :=
    Build t_code t_arg_type t_storage t_storage_type t_views t_root_name
      t_code_size r.(code) r.(arg_type) r.(storage) r.(storage_type) views
      r.(root_name) r.(code_size).
  Definition with_root_name
    {t_code t_arg_type t_storage t_storage_type t_views t_root_name t_code_size}
    root_name
    (r :
      record t_code t_arg_type t_storage t_storage_type t_views t_root_name
        t_code_size) :=
    Build t_code t_arg_type t_storage t_storage_type t_views t_root_name
      t_code_size r.(code) r.(arg_type) r.(storage) r.(storage_type) r.(views)
      root_name r.(code_size).
  Definition with_code_size
    {t_code t_arg_type t_storage t_storage_type t_views t_root_name t_code_size}
    code_size
    (r :
      record t_code t_arg_type t_storage t_storage_type t_views t_root_name
        t_code_size) :=
    Build t_code t_arg_type t_storage t_storage_type t_views t_root_name
      t_code_size r.(code) r.(arg_type) r.(storage) r.(storage_type) r.(views)
      r.(root_name) code_size.
End script.
Definition script_skeleton := script.record.

Reserved Notation "'kinstr.IIf_none".
Reserved Notation "'kinstr.IOpt_map".
Reserved Notation "'kinstr.IIf_left".
Reserved Notation "'kinstr.IIf_cons".
Reserved Notation "'kinstr.IIf".
Reserved Notation "'kinstr.ICreate_contract".
Reserved Notation "'view_signature.View_signature".
Reserved Notation "'script".
Reserved Notation "'typed_contract".
Reserved Notation "'logging_function".
Reserved Notation "'execution_trace".
Reserved Notation "'logger".
Reserved Notation "'big_map".
Reserved Notation "'kdescr".
Reserved Notation "'kinfo".

Inductive kinstr : Set :=
| IDrop : 'kinfo -> kinstr -> kinstr
| IDup : 'kinfo -> kinstr -> kinstr
| ISwap : 'kinfo -> kinstr -> kinstr
| IConst : forall {ty : Set}, 'kinfo -> ty -> kinstr -> kinstr
| ICons_pair : 'kinfo -> kinstr -> kinstr
| ICar : 'kinfo -> kinstr -> kinstr
| ICdr : 'kinfo -> kinstr -> kinstr
| IUnpair : 'kinfo -> kinstr -> kinstr
| ICons_some : 'kinfo -> kinstr -> kinstr
| ICons_none : 'kinfo -> kinstr -> kinstr
| IIf_none : 'kinstr.IIf_none -> kinstr
| IOpt_map : 'kinstr.IOpt_map -> kinstr
| ICons_left : 'kinfo -> kinstr -> kinstr
| ICons_right : 'kinfo -> kinstr -> kinstr
| IIf_left : 'kinstr.IIf_left -> kinstr
| ICons_list : 'kinfo -> kinstr -> kinstr
| INil : 'kinfo -> kinstr -> kinstr
| IIf_cons : 'kinstr.IIf_cons -> kinstr
| IList_map : 'kinfo -> kinstr -> kinstr -> kinstr
| IList_iter : 'kinfo -> kinstr -> kinstr -> kinstr
| IList_size : 'kinfo -> kinstr -> kinstr
| IEmpty_set : 'kinfo -> comparable_ty -> kinstr -> kinstr
| ISet_iter : 'kinfo -> kinstr -> kinstr -> kinstr
| ISet_mem : 'kinfo -> kinstr -> kinstr
| ISet_update : 'kinfo -> kinstr -> kinstr
| ISet_size : 'kinfo -> kinstr -> kinstr
| IEmpty_map : 'kinfo -> comparable_ty -> kinstr -> kinstr
| IMap_map : 'kinfo -> kinstr -> kinstr -> kinstr
| IMap_iter : 'kinfo -> kinstr -> kinstr -> kinstr
| IMap_mem : 'kinfo -> kinstr -> kinstr
| IMap_get : 'kinfo -> kinstr -> kinstr
| IMap_update : 'kinfo -> kinstr -> kinstr
| IMap_get_and_update : 'kinfo -> kinstr -> kinstr
| IMap_size : 'kinfo -> kinstr -> kinstr
| IEmpty_big_map : 'kinfo -> comparable_ty -> ty -> kinstr -> kinstr
| IBig_map_mem : 'kinfo -> kinstr -> kinstr
| IBig_map_get : 'kinfo -> kinstr -> kinstr
| IBig_map_update : 'kinfo -> kinstr -> kinstr
| IBig_map_get_and_update : 'kinfo -> kinstr -> kinstr
| IConcat_string : 'kinfo -> kinstr -> kinstr
| IConcat_string_pair : 'kinfo -> kinstr -> kinstr
| ISlice_string : 'kinfo -> kinstr -> kinstr
| IString_size : 'kinfo -> kinstr -> kinstr
| IConcat_bytes : 'kinfo -> kinstr -> kinstr
| IConcat_bytes_pair : 'kinfo -> kinstr -> kinstr
| ISlice_bytes : 'kinfo -> kinstr -> kinstr
| IBytes_size : 'kinfo -> kinstr -> kinstr
| IAdd_seconds_to_timestamp : 'kinfo -> kinstr -> kinstr
| IAdd_timestamp_to_seconds : 'kinfo -> kinstr -> kinstr
| ISub_timestamp_seconds : 'kinfo -> kinstr -> kinstr
| IDiff_timestamps : 'kinfo -> kinstr -> kinstr
| IAdd_tez : 'kinfo -> kinstr -> kinstr
| ISub_tez : 'kinfo -> kinstr -> kinstr
| ISub_tez_legacy : 'kinfo -> kinstr -> kinstr
| IMul_teznat : 'kinfo -> kinstr -> kinstr
| IMul_nattez : 'kinfo -> kinstr -> kinstr
| IEdiv_teznat : 'kinfo -> kinstr -> kinstr
| IEdiv_tez : 'kinfo -> kinstr -> kinstr
| IOr : 'kinfo -> kinstr -> kinstr
| IAnd : 'kinfo -> kinstr -> kinstr
| IXor : 'kinfo -> kinstr -> kinstr
| INot : 'kinfo -> kinstr -> kinstr
| IIs_nat : 'kinfo -> kinstr -> kinstr
| INeg : 'kinfo -> kinstr -> kinstr
| IAbs_int : 'kinfo -> kinstr -> kinstr
| IInt_nat : 'kinfo -> kinstr -> kinstr
| IAdd_int : 'kinfo -> kinstr -> kinstr
| IAdd_nat : 'kinfo -> kinstr -> kinstr
| ISub_int : 'kinfo -> kinstr -> kinstr
| IMul_int : 'kinfo -> kinstr -> kinstr
| IMul_nat : 'kinfo -> kinstr -> kinstr
| IEdiv_int : 'kinfo -> kinstr -> kinstr
| IEdiv_nat : 'kinfo -> kinstr -> kinstr
| ILsl_nat : 'kinfo -> kinstr -> kinstr
| ILsr_nat : 'kinfo -> kinstr -> kinstr
| IOr_nat : 'kinfo -> kinstr -> kinstr
| IAnd_nat : 'kinfo -> kinstr -> kinstr
| IAnd_int_nat : 'kinfo -> kinstr -> kinstr
| IXor_nat : 'kinfo -> kinstr -> kinstr
| INot_int : 'kinfo -> kinstr -> kinstr
| IIf : 'kinstr.IIf -> kinstr
| ILoop : 'kinfo -> kinstr -> kinstr -> kinstr
| ILoop_left : 'kinfo -> kinstr -> kinstr -> kinstr
| IDip : 'kinfo -> kinstr -> kinstr -> kinstr
| IExec : 'kinfo -> kinstr -> kinstr
| IApply : 'kinfo -> ty -> kinstr -> kinstr
| ILambda : 'kinfo -> lambda -> kinstr -> kinstr
| IFailwith : 'kinfo -> Alpha_context.Script.location -> ty -> kinstr
| ICompare : 'kinfo -> comparable_ty -> kinstr -> kinstr
| IEq : 'kinfo -> kinstr -> kinstr
| INeq : 'kinfo -> kinstr -> kinstr
| ILt : 'kinfo -> kinstr -> kinstr
| IGt : 'kinfo -> kinstr -> kinstr
| ILe : 'kinfo -> kinstr -> kinstr
| IGe : 'kinfo -> kinstr -> kinstr
| IAddress : 'kinfo -> kinstr -> kinstr
| IContract : 'kinfo -> ty -> string -> kinstr -> kinstr
| IView : 'kinfo -> view_signature -> kinstr -> kinstr
| ITransfer_tokens : 'kinfo -> kinstr -> kinstr
| IImplicit_account : 'kinfo -> kinstr -> kinstr
| ICreate_contract : 'kinstr.ICreate_contract -> kinstr
| ISet_delegate : 'kinfo -> kinstr -> kinstr
| INow : 'kinfo -> kinstr -> kinstr
| IBalance : 'kinfo -> kinstr -> kinstr
| ILevel : 'kinfo -> kinstr -> kinstr
| ICheck_signature : 'kinfo -> kinstr -> kinstr
| IHash_key : 'kinfo -> kinstr -> kinstr
| IPack : 'kinfo -> ty -> kinstr -> kinstr
| IUnpack : 'kinfo -> ty -> kinstr -> kinstr
| IBlake2b : 'kinfo -> kinstr -> kinstr
| ISha256 : 'kinfo -> kinstr -> kinstr
| ISha512 : 'kinfo -> kinstr -> kinstr
| ISource : 'kinfo -> kinstr -> kinstr
| ISender : 'kinfo -> kinstr -> kinstr
| ISelf : 'kinfo -> ty -> string -> kinstr -> kinstr
| ISelf_address : 'kinfo -> kinstr -> kinstr
| IAmount : 'kinfo -> kinstr -> kinstr
| ISapling_empty_state :
  'kinfo -> Alpha_context.Sapling.Memo_size.t -> kinstr -> kinstr
| ISapling_verify_update : 'kinfo -> kinstr -> kinstr
| IDig : 'kinfo -> int -> stack_prefix_preservation_witness -> kinstr -> kinstr
| IDug : 'kinfo -> int -> stack_prefix_preservation_witness -> kinstr -> kinstr
| IDipn :
  'kinfo -> int -> stack_prefix_preservation_witness -> kinstr -> kinstr ->
  kinstr
| IDropn :
  'kinfo -> int -> stack_prefix_preservation_witness -> kinstr -> kinstr
| IChainId : 'kinfo -> kinstr -> kinstr
| INever : 'kinfo -> kinstr
| IVoting_power : 'kinfo -> kinstr -> kinstr
| ITotal_voting_power : 'kinfo -> kinstr -> kinstr
| IKeccak : 'kinfo -> kinstr -> kinstr
| ISha3 : 'kinfo -> kinstr -> kinstr
| IAdd_bls12_381_g1 : 'kinfo -> kinstr -> kinstr
| IAdd_bls12_381_g2 : 'kinfo -> kinstr -> kinstr
| IAdd_bls12_381_fr : 'kinfo -> kinstr -> kinstr
| IMul_bls12_381_g1 : 'kinfo -> kinstr -> kinstr
| IMul_bls12_381_g2 : 'kinfo -> kinstr -> kinstr
| IMul_bls12_381_fr : 'kinfo -> kinstr -> kinstr
| IMul_bls12_381_z_fr : 'kinfo -> kinstr -> kinstr
| IMul_bls12_381_fr_z : 'kinfo -> kinstr -> kinstr
| IInt_bls12_381_fr : 'kinfo -> kinstr -> kinstr
| INeg_bls12_381_g1 : 'kinfo -> kinstr -> kinstr
| INeg_bls12_381_g2 : 'kinfo -> kinstr -> kinstr
| INeg_bls12_381_fr : 'kinfo -> kinstr -> kinstr
| IPairing_check_bls12_381 : 'kinfo -> kinstr -> kinstr
| IComb : 'kinfo -> int -> comb_gadt_witness -> kinstr -> kinstr
| IUncomb : 'kinfo -> int -> uncomb_gadt_witness -> kinstr -> kinstr
| IComb_get : 'kinfo -> int -> comb_get_gadt_witness -> kinstr -> kinstr
| IComb_set : 'kinfo -> int -> comb_set_gadt_witness -> kinstr -> kinstr
| IDup_n : 'kinfo -> int -> dup_n_gadt_witness -> kinstr -> kinstr
| ITicket : 'kinfo -> kinstr -> kinstr
| IRead_ticket : 'kinfo -> kinstr -> kinstr
| ISplit_ticket : 'kinfo -> kinstr -> kinstr
| IJoin_tickets : 'kinfo -> comparable_ty -> kinstr -> kinstr
| IOpen_chest : 'kinfo -> kinstr -> kinstr
| IHalt : 'kinfo -> kinstr
| ILog : 'kinfo -> logging_event -> 'logger -> kinstr -> kinstr

with logging_event : Set :=
| LogEntry : logging_event
| LogExit : 'kinfo -> logging_event

with lambda : Set :=
| Lam : 'kdescr -> Alpha_context.Script.node -> lambda

with continuation : Set :=
| KNil : continuation
| KCons : kinstr -> continuation -> continuation
| KReturn : forall {s : Set}, s -> continuation -> continuation
| KMap_head : forall {a b : Set}, (a -> b) -> continuation -> continuation
| KUndip : forall {b : Set}, b -> continuation -> continuation
| KLoop_in : kinstr -> continuation -> continuation
| KLoop_in_left : kinstr -> continuation -> continuation
| KIter : forall {a : Set}, kinstr -> list a -> continuation -> continuation
| KList_enter_body : forall {a b : Set},
  kinstr -> list a -> list b -> int -> continuation -> continuation
| KList_exit_body : forall {a b : Set},
  kinstr -> list a -> list b -> int -> continuation -> continuation
| KMap_enter_body : forall {a b c : Set},
  kinstr -> list (a * b) -> map a c -> continuation -> continuation
| KMap_exit_body : forall {a b c : Set},
  kinstr -> list (a * b) -> map a c -> a -> continuation -> continuation
| KView_exit : step_constants -> continuation -> continuation
| KLog : continuation -> 'logger -> continuation

with ty : Set :=
| Unit_t : ty_metadata -> ty
| Int_t : ty_metadata -> ty
| Nat_t : ty_metadata -> ty
| Signature_t : ty_metadata -> ty
| String_t : ty_metadata -> ty
| Bytes_t : ty_metadata -> ty
| Mutez_t : ty_metadata -> ty
| Key_hash_t : ty_metadata -> ty
| Key_t : ty_metadata -> ty
| Timestamp_t : ty_metadata -> ty
| Address_t : ty_metadata -> ty
| Bool_t : ty_metadata -> ty
| Pair_t :
  ty * option Script_ir_annot.field_annot * option Script_ir_annot.var_annot ->
  ty * option Script_ir_annot.field_annot * option Script_ir_annot.var_annot ->
  ty_metadata -> ty
| Union_t :
  ty * option Script_ir_annot.field_annot ->
  ty * option Script_ir_annot.field_annot -> ty_metadata -> ty
| Lambda_t : ty -> ty -> ty_metadata -> ty
| Option_t : ty -> ty_metadata -> ty
| List_t : ty -> ty_metadata -> ty
| Set_t : comparable_ty -> ty_metadata -> ty
| Map_t : comparable_ty -> ty -> ty_metadata -> ty
| Big_map_t : comparable_ty -> ty -> ty_metadata -> ty
| Contract_t : ty -> ty_metadata -> ty
| Sapling_transaction_t : Alpha_context.Sapling.Memo_size.t -> ty_metadata -> ty
| Sapling_state_t : Alpha_context.Sapling.Memo_size.t -> ty_metadata -> ty
| Operation_t : ty_metadata -> ty
| Chain_id_t : ty_metadata -> ty
| Never_t : ty_metadata -> ty
| Bls12_381_g1_t : ty_metadata -> ty
| Bls12_381_g2_t : ty_metadata -> ty
| Bls12_381_fr_t : ty_metadata -> ty
| Ticket_t : comparable_ty -> ty_metadata -> ty
| Chest_key_t : ty_metadata -> ty
| Chest_t : ty_metadata -> ty

with stack_ty : Set :=
| Item_t : ty -> stack_ty -> option Script_ir_annot.var_annot -> stack_ty
| Bot_t : stack_ty

with stack_prefix_preservation_witness : Set :=
| KPrefix :
  'kinfo -> stack_prefix_preservation_witness ->
  stack_prefix_preservation_witness
| KRest : stack_prefix_preservation_witness

with comb_gadt_witness : Set :=
| Comb_one : comb_gadt_witness
| Comb_succ : comb_gadt_witness -> comb_gadt_witness

with uncomb_gadt_witness : Set :=
| Uncomb_one : uncomb_gadt_witness
| Uncomb_succ : uncomb_gadt_witness -> uncomb_gadt_witness

with comb_get_gadt_witness : Set :=
| Comb_get_zero : comb_get_gadt_witness
| Comb_get_one : comb_get_gadt_witness
| Comb_get_plus_two : comb_get_gadt_witness -> comb_get_gadt_witness

with comb_set_gadt_witness : Set :=
| Comb_set_zero : comb_set_gadt_witness
| Comb_set_one : comb_set_gadt_witness
| Comb_set_plus_two : comb_set_gadt_witness -> comb_set_gadt_witness

with dup_n_gadt_witness : Set :=
| Dup_n_zero : dup_n_gadt_witness
| Dup_n_succ : dup_n_gadt_witness -> dup_n_gadt_witness

with view_signature : Set :=
| View_signature : 'view_signature.View_signature -> view_signature

where "'script" :=
  (fun (t_storage : Set) => script_skeleton lambda ty t_storage ty
    (SMap.(Map.S.t) view) (option Script_ir_annot.field_annot)
    Cache_memory_helpers.sint)
and "'typed_contract" := (ty * address)
and "'logging_function" :=
  (fun (t_c t_u : Set) => kinstr -> Alpha_context.context ->
  Alpha_context.Script.location -> stack_ty -> t_c * t_u -> unit)
and "'execution_trace" :=
  (list
    (Alpha_context.Script.location * Alpha_context.Gas.t *
      list (Alpha_context.Script.expr * option string)))
and "'big_map" :=
  (fun (t_key t_value : Set) => big_map_skeleton
    (option Alpha_context.Big_map.Id.t) (big_map_overlay t_key t_value)
    comparable_ty ty)
and "'kdescr" :=
  (kdescr_skeleton Alpha_context.Script.location stack_ty stack_ty kinstr)
and "'kinfo" := (kinfo_skeleton Alpha_context.Script.location stack_ty)
and "'logger" :=
  (logger_skeleton (forall {c u : Set}, 'logging_function c u)
    (forall {a s : Set}, 'logging_function a s) (continuation -> unit)
    (forall {c u : Set}, 'logging_function c u)
    (unit -> M? (option 'execution_trace)))
and "'kinstr.IIf_none" := (kinstr.IIf_none_skeleton 'kinfo kinstr kinstr kinstr)
and "'kinstr.IOpt_map" := (kinstr.IOpt_map_skeleton 'kinfo kinstr kinstr)
and "'kinstr.IIf_left" := (kinstr.IIf_left_skeleton 'kinfo kinstr kinstr kinstr)
and "'kinstr.IIf_cons" := (kinstr.IIf_cons_skeleton 'kinfo kinstr kinstr kinstr)
and "'kinstr.IIf" := (kinstr.IIf_skeleton 'kinfo kinstr kinstr kinstr)
and "'kinstr.ICreate_contract" :=
  (kinstr.ICreate_contract_skeleton 'kinfo ty ty lambda (SMap.(Map.S.t) view)
    (option Script_ir_annot.field_annot) kinstr)
and "'view_signature.View_signature" :=
  (view_signature.View_signature_skeleton Alpha_context.Script_string.t ty ty).

Module kinstr.
  Include ConstructorRecords_kinstr_view_signature.kinstr.
  Definition IIf_none := 'kinstr.IIf_none.
  Definition IOpt_map := 'kinstr.IOpt_map.
  Definition IIf_left := 'kinstr.IIf_left.
  Definition IIf_cons := 'kinstr.IIf_cons.
  Definition IIf := 'kinstr.IIf.
  Definition ICreate_contract := 'kinstr.ICreate_contract.
End kinstr.
Module view_signature.
  Include ConstructorRecords_kinstr_view_signature.view_signature.
  Definition View_signature := 'view_signature.View_signature.
End view_signature.

Definition script := 'script.
Definition typed_contract := 'typed_contract.
Definition logging_function := 'logging_function.
Definition execution_trace := 'execution_trace.
Definition logger := 'logger.
Definition big_map := 'big_map.
Definition kdescr := 'kdescr.
Definition kinfo := 'kinfo.

Definition kinfo_of_kinstr (i_value : kinstr) : kinfo :=
  match i_value with
  | IDrop kinfo_value _ => kinfo_value
  | IDup kinfo_value _ => kinfo_value
  | ISwap kinfo_value _ => kinfo_value
  | IConst kinfo_value _ _ => kinfo_value
  | ICons_pair kinfo_value _ => kinfo_value
  | ICar kinfo_value _ => kinfo_value
  | ICdr kinfo_value _ => kinfo_value
  | IUnpair kinfo_value _ => kinfo_value
  | ICons_some kinfo_value _ => kinfo_value
  | ICons_none kinfo_value _ => kinfo_value
  | IIf_none {| kinstr.IIf_none.kinfo := kinfo_value |} => kinfo_value
  | IOpt_map {| kinstr.IOpt_map.kinfo := kinfo_value |} => kinfo_value
  | ICons_left kinfo_value _ => kinfo_value
  | ICons_right kinfo_value _ => kinfo_value
  | IIf_left {| kinstr.IIf_left.kinfo := kinfo_value |} => kinfo_value
  | ICons_list kinfo_value _ => kinfo_value
  | INil kinfo_value _ => kinfo_value
  | IIf_cons {| kinstr.IIf_cons.kinfo := kinfo_value |} => kinfo_value
  | IList_map kinfo_value _ _ => kinfo_value
  | IList_iter kinfo_value _ _ => kinfo_value
  | IList_size kinfo_value _ => kinfo_value
  | IEmpty_set kinfo_value _ _ => kinfo_value
  | ISet_iter kinfo_value _ _ => kinfo_value
  | ISet_mem kinfo_value _ => kinfo_value
  | ISet_update kinfo_value _ => kinfo_value
  | ISet_size kinfo_value _ => kinfo_value
  | IEmpty_map kinfo_value _ _ => kinfo_value
  | IMap_map kinfo_value _ _ => kinfo_value
  | IMap_iter kinfo_value _ _ => kinfo_value
  | IMap_mem kinfo_value _ => kinfo_value
  | IMap_get kinfo_value _ => kinfo_value
  | IMap_update kinfo_value _ => kinfo_value
  | IMap_get_and_update kinfo_value _ => kinfo_value
  | IMap_size kinfo_value _ => kinfo_value
  | IEmpty_big_map kinfo_value _ _ _ => kinfo_value
  | IBig_map_mem kinfo_value _ => kinfo_value
  | IBig_map_get kinfo_value _ => kinfo_value
  | IBig_map_update kinfo_value _ => kinfo_value
  | IBig_map_get_and_update kinfo_value _ => kinfo_value
  | IConcat_string kinfo_value _ => kinfo_value
  | IConcat_string_pair kinfo_value _ => kinfo_value
  | ISlice_string kinfo_value _ => kinfo_value
  | IString_size kinfo_value _ => kinfo_value
  | IConcat_bytes kinfo_value _ => kinfo_value
  | IConcat_bytes_pair kinfo_value _ => kinfo_value
  | ISlice_bytes kinfo_value _ => kinfo_value
  | IBytes_size kinfo_value _ => kinfo_value
  | IAdd_seconds_to_timestamp kinfo_value _ => kinfo_value
  | IAdd_timestamp_to_seconds kinfo_value _ => kinfo_value
  | ISub_timestamp_seconds kinfo_value _ => kinfo_value
  | IDiff_timestamps kinfo_value _ => kinfo_value
  | IAdd_tez kinfo_value _ => kinfo_value
  | ISub_tez kinfo_value _ => kinfo_value
  | ISub_tez_legacy kinfo_value _ => kinfo_value
  | IMul_teznat kinfo_value _ => kinfo_value
  | IMul_nattez kinfo_value _ => kinfo_value
  | IEdiv_teznat kinfo_value _ => kinfo_value
  | IEdiv_tez kinfo_value _ => kinfo_value
  | IOr kinfo_value _ => kinfo_value
  | IAnd kinfo_value _ => kinfo_value
  | IXor kinfo_value _ => kinfo_value
  | INot kinfo_value _ => kinfo_value
  | IIs_nat kinfo_value _ => kinfo_value
  | INeg kinfo_value _ => kinfo_value
  | IAbs_int kinfo_value _ => kinfo_value
  | IInt_nat kinfo_value _ => kinfo_value
  | IAdd_int kinfo_value _ => kinfo_value
  | IAdd_nat kinfo_value _ => kinfo_value
  | ISub_int kinfo_value _ => kinfo_value
  | IMul_int kinfo_value _ => kinfo_value
  | IMul_nat kinfo_value _ => kinfo_value
  | IEdiv_int kinfo_value _ => kinfo_value
  | IEdiv_nat kinfo_value _ => kinfo_value
  | ILsl_nat kinfo_value _ => kinfo_value
  | ILsr_nat kinfo_value _ => kinfo_value
  | IOr_nat kinfo_value _ => kinfo_value
  | IAnd_nat kinfo_value _ => kinfo_value
  | IAnd_int_nat kinfo_value _ => kinfo_value
  | IXor_nat kinfo_value _ => kinfo_value
  | INot_int kinfo_value _ => kinfo_value
  | IIf {| kinstr.IIf.kinfo := kinfo_value |} => kinfo_value
  | ILoop kinfo_value _ _ => kinfo_value
  | ILoop_left kinfo_value _ _ => kinfo_value
  | IDip kinfo_value _ _ => kinfo_value
  | IExec kinfo_value _ => kinfo_value
  | IApply kinfo_value _ _ => kinfo_value
  | ILambda kinfo_value _ _ => kinfo_value
  | IFailwith kinfo_value _ _ => kinfo_value
  | ICompare kinfo_value _ _ => kinfo_value
  | IEq kinfo_value _ => kinfo_value
  | INeq kinfo_value _ => kinfo_value
  | ILt kinfo_value _ => kinfo_value
  | IGt kinfo_value _ => kinfo_value
  | ILe kinfo_value _ => kinfo_value
  | IGe kinfo_value _ => kinfo_value
  | IAddress kinfo_value _ => kinfo_value
  | IContract kinfo_value _ _ _ => kinfo_value
  | ITransfer_tokens kinfo_value _ => kinfo_value
  | IView kinfo_value _ _ => kinfo_value
  | IImplicit_account kinfo_value _ => kinfo_value
  | ICreate_contract {| kinstr.ICreate_contract.kinfo := kinfo_value |} =>
    kinfo_value
  | ISet_delegate kinfo_value _ => kinfo_value
  | INow kinfo_value _ => kinfo_value
  | IBalance kinfo_value _ => kinfo_value
  | ILevel kinfo_value _ => kinfo_value
  | ICheck_signature kinfo_value _ => kinfo_value
  | IHash_key kinfo_value _ => kinfo_value
  | IPack kinfo_value _ _ => kinfo_value
  | IUnpack kinfo_value _ _ => kinfo_value
  | IBlake2b kinfo_value _ => kinfo_value
  | ISha256 kinfo_value _ => kinfo_value
  | ISha512 kinfo_value _ => kinfo_value
  | ISource kinfo_value _ => kinfo_value
  | ISender kinfo_value _ => kinfo_value
  | ISelf kinfo_value _ _ _ => kinfo_value
  | ISelf_address kinfo_value _ => kinfo_value
  | IAmount kinfo_value _ => kinfo_value
  | ISapling_empty_state kinfo_value _ _ => kinfo_value
  | ISapling_verify_update kinfo_value _ => kinfo_value
  | IDig kinfo_value _ _ _ => kinfo_value
  | IDug kinfo_value _ _ _ => kinfo_value
  | IDipn kinfo_value _ _ _ _ => kinfo_value
  | IDropn kinfo_value _ _ _ => kinfo_value
  | IChainId kinfo_value _ => kinfo_value
  | INever kinfo_value => kinfo_value
  | IVoting_power kinfo_value _ => kinfo_value
  | ITotal_voting_power kinfo_value _ => kinfo_value
  | IKeccak kinfo_value _ => kinfo_value
  | ISha3 kinfo_value _ => kinfo_value
  | IAdd_bls12_381_g1 kinfo_value _ => kinfo_value
  | IAdd_bls12_381_g2 kinfo_value _ => kinfo_value
  | IAdd_bls12_381_fr kinfo_value _ => kinfo_value
  | IMul_bls12_381_g1 kinfo_value _ => kinfo_value
  | IMul_bls12_381_g2 kinfo_value _ => kinfo_value
  | IMul_bls12_381_fr kinfo_value _ => kinfo_value
  | IMul_bls12_381_z_fr kinfo_value _ => kinfo_value
  | IMul_bls12_381_fr_z kinfo_value _ => kinfo_value
  | IInt_bls12_381_fr kinfo_value _ => kinfo_value
  | INeg_bls12_381_g1 kinfo_value _ => kinfo_value
  | INeg_bls12_381_g2 kinfo_value _ => kinfo_value
  | INeg_bls12_381_fr kinfo_value _ => kinfo_value
  | IPairing_check_bls12_381 kinfo_value _ => kinfo_value
  | IComb kinfo_value _ _ _ => kinfo_value
  | IUncomb kinfo_value _ _ _ => kinfo_value
  | IComb_get kinfo_value _ _ _ => kinfo_value
  | IComb_set kinfo_value _ _ _ => kinfo_value
  | IDup_n kinfo_value _ _ _ => kinfo_value
  | ITicket kinfo_value _ => kinfo_value
  | IRead_ticket kinfo_value _ => kinfo_value
  | ISplit_ticket kinfo_value _ => kinfo_value
  | IJoin_tickets kinfo_value _ _ => kinfo_value
  | IHalt kinfo_value => kinfo_value
  | ILog kinfo_value _ _ _ => kinfo_value
  | IOpen_chest kinfo_value _ => kinfo_value
  end.

Module kinstr_rewritek.
  Record record : Set := Build {
    apply : kinstr -> kinstr }.
  Definition with_apply apply (r : record) :=
    Build apply.
End kinstr_rewritek.
Definition kinstr_rewritek := kinstr_rewritek.record.

Definition kinstr_rewritek_value (i_value : kinstr) (f_value : kinstr_rewritek)
  : kinstr :=
  match i_value with
  | IDrop kinfo_value k_value =>
    IDrop kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IDup kinfo_value k_value =>
    IDup kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ISwap kinfo_value k_value =>
    ISwap kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IConst kinfo_value x_value k_value =>
    IConst kinfo_value x_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ICons_pair kinfo_value k_value =>
    ICons_pair kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ICar kinfo_value k_value =>
    ICar kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ICdr kinfo_value k_value =>
    ICdr kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IUnpair kinfo_value k_value =>
    IUnpair kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ICons_some kinfo_value k_value =>
    ICons_some kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ICons_none kinfo_value k_value =>
    ICons_none kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  |
    IIf_none {|
      kinstr.IIf_none.kinfo := kinfo_value;
        kinstr.IIf_none.branch_if_none := branch_if_none;
        kinstr.IIf_none.branch_if_some := branch_if_some;
        kinstr.IIf_none.k := k_value
        |} =>
    IIf_none
      {| kinstr.IIf_none.kinfo := kinfo_value;
        kinstr.IIf_none.branch_if_none :=
          f_value.(kinstr_rewritek.apply) branch_if_none;
        kinstr.IIf_none.branch_if_some :=
          f_value.(kinstr_rewritek.apply) branch_if_some;
        kinstr.IIf_none.k := f_value.(kinstr_rewritek.apply) k_value |}
  
  |
    IOpt_map {|
      kinstr.IOpt_map.kinfo := kinfo_value;
        kinstr.IOpt_map.body := body;
        kinstr.IOpt_map.k := k_value
        |} =>
    let body := f_value.(kinstr_rewritek.apply) body in
    let k_value := f_value.(kinstr_rewritek.apply) k_value in
    IOpt_map
      {| kinstr.IOpt_map.kinfo := kinfo_value; kinstr.IOpt_map.body := body;
        kinstr.IOpt_map.k := k_value |}
  
  | ICons_left kinfo_value k_value =>
    ICons_left kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ICons_right kinfo_value k_value =>
    ICons_right kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  |
    IIf_left {|
      kinstr.IIf_left.kinfo := kinfo_value;
        kinstr.IIf_left.branch_if_left := branch_if_left;
        kinstr.IIf_left.branch_if_right := branch_if_right;
        kinstr.IIf_left.k := k_value
        |} =>
    IIf_left
      {| kinstr.IIf_left.kinfo := kinfo_value;
        kinstr.IIf_left.branch_if_left :=
          f_value.(kinstr_rewritek.apply) branch_if_left;
        kinstr.IIf_left.branch_if_right :=
          f_value.(kinstr_rewritek.apply) branch_if_right;
        kinstr.IIf_left.k := f_value.(kinstr_rewritek.apply) k_value |}
  
  | ICons_list kinfo_value k_value =>
    ICons_list kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | INil kinfo_value k_value =>
    INil kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  |
    IIf_cons {|
      kinstr.IIf_cons.kinfo := kinfo_value;
        kinstr.IIf_cons.branch_if_cons := branch_if_cons;
        kinstr.IIf_cons.branch_if_nil := branch_if_nil;
        kinstr.IIf_cons.k := k_value
        |} =>
    IIf_cons
      {| kinstr.IIf_cons.kinfo := kinfo_value;
        kinstr.IIf_cons.branch_if_cons :=
          f_value.(kinstr_rewritek.apply) branch_if_cons;
        kinstr.IIf_cons.branch_if_nil :=
          f_value.(kinstr_rewritek.apply) branch_if_nil;
        kinstr.IIf_cons.k := f_value.(kinstr_rewritek.apply) k_value |}
  
  | IList_map kinfo_value body k_value =>
    IList_map kinfo_value (f_value.(kinstr_rewritek.apply) body)
      (f_value.(kinstr_rewritek.apply) k_value)
  
  | IList_iter kinfo_value body k_value =>
    IList_iter kinfo_value (f_value.(kinstr_rewritek.apply) body)
      (f_value.(kinstr_rewritek.apply) k_value)
  
  | IList_size kinfo_value k_value =>
    IList_size kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IEmpty_set kinfo_value ty k_value =>
    IEmpty_set kinfo_value ty (f_value.(kinstr_rewritek.apply) k_value)
  
  | ISet_iter kinfo_value body k_value =>
    ISet_iter kinfo_value (f_value.(kinstr_rewritek.apply) body)
      (f_value.(kinstr_rewritek.apply) k_value)
  
  | ISet_mem kinfo_value k_value =>
    ISet_mem kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ISet_update kinfo_value k_value =>
    ISet_update kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ISet_size kinfo_value k_value =>
    ISet_size kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IEmpty_map kinfo_value cty k_value =>
    IEmpty_map kinfo_value cty (f_value.(kinstr_rewritek.apply) k_value)
  
  | IMap_map kinfo_value body k_value =>
    IMap_map kinfo_value (f_value.(kinstr_rewritek.apply) body)
      (f_value.(kinstr_rewritek.apply) k_value)
  
  | IMap_iter kinfo_value body k_value =>
    IMap_iter kinfo_value (f_value.(kinstr_rewritek.apply) body)
      (f_value.(kinstr_rewritek.apply) k_value)
  
  | IMap_mem kinfo_value k_value =>
    IMap_mem kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IMap_get kinfo_value k_value =>
    IMap_get kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IMap_update kinfo_value k_value =>
    IMap_update kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IMap_get_and_update kinfo_value k_value =>
    IMap_get_and_update kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IMap_size kinfo_value k_value =>
    IMap_size kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IEmpty_big_map kinfo_value cty ty k_value =>
    IEmpty_big_map kinfo_value cty ty (f_value.(kinstr_rewritek.apply) k_value)
  
  | IBig_map_mem kinfo_value k_value =>
    IBig_map_mem kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IBig_map_get kinfo_value k_value =>
    IBig_map_get kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IBig_map_update kinfo_value k_value =>
    IBig_map_update kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IBig_map_get_and_update kinfo_value k_value =>
    IBig_map_get_and_update kinfo_value
      (f_value.(kinstr_rewritek.apply) k_value)
  
  | IConcat_string kinfo_value k_value =>
    IConcat_string kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IConcat_string_pair kinfo_value k_value =>
    IConcat_string_pair kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ISlice_string kinfo_value k_value =>
    ISlice_string kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IString_size kinfo_value k_value =>
    IString_size kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IConcat_bytes kinfo_value k_value =>
    IConcat_bytes kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IConcat_bytes_pair kinfo_value k_value =>
    IConcat_bytes_pair kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ISlice_bytes kinfo_value k_value =>
    ISlice_bytes kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IBytes_size kinfo_value k_value =>
    IBytes_size kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IAdd_seconds_to_timestamp kinfo_value k_value =>
    IAdd_seconds_to_timestamp kinfo_value
      (f_value.(kinstr_rewritek.apply) k_value)
  
  | IAdd_timestamp_to_seconds kinfo_value k_value =>
    IAdd_timestamp_to_seconds kinfo_value
      (f_value.(kinstr_rewritek.apply) k_value)
  
  | ISub_timestamp_seconds kinfo_value k_value =>
    ISub_timestamp_seconds kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IDiff_timestamps kinfo_value k_value =>
    IDiff_timestamps kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IAdd_tez kinfo_value k_value =>
    IAdd_tez kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ISub_tez kinfo_value k_value =>
    ISub_tez kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ISub_tez_legacy kinfo_value k_value =>
    ISub_tez_legacy kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IMul_teznat kinfo_value k_value =>
    IMul_teznat kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IMul_nattez kinfo_value k_value =>
    IMul_nattez kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IEdiv_teznat kinfo_value k_value =>
    IEdiv_teznat kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IEdiv_tez kinfo_value k_value =>
    IEdiv_tez kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IOr kinfo_value k_value =>
    IOr kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IAnd kinfo_value k_value =>
    IAnd kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IXor kinfo_value k_value =>
    IXor kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | INot kinfo_value k_value =>
    INot kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IIs_nat kinfo_value k_value =>
    IIs_nat kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | INeg kinfo_value k_value =>
    INeg kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IAbs_int kinfo_value k_value =>
    IAbs_int kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IInt_nat kinfo_value k_value =>
    IInt_nat kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IAdd_int kinfo_value k_value =>
    IAdd_int kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IAdd_nat kinfo_value k_value =>
    IAdd_nat kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ISub_int kinfo_value k_value =>
    ISub_int kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IMul_int kinfo_value k_value =>
    IMul_int kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IMul_nat kinfo_value k_value =>
    IMul_nat kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IEdiv_int kinfo_value k_value =>
    IEdiv_int kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IEdiv_nat kinfo_value k_value =>
    IEdiv_nat kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ILsl_nat kinfo_value k_value =>
    ILsl_nat kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ILsr_nat kinfo_value k_value =>
    ILsr_nat kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IOr_nat kinfo_value k_value =>
    IOr_nat kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IAnd_nat kinfo_value k_value =>
    IAnd_nat kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IAnd_int_nat kinfo_value k_value =>
    IAnd_int_nat kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IXor_nat kinfo_value k_value =>
    IXor_nat kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | INot_int kinfo_value k_value =>
    INot_int kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  |
    IIf {|
      kinstr.IIf.kinfo := kinfo_value;
        kinstr.IIf.branch_if_true := branch_if_true;
        kinstr.IIf.branch_if_false := branch_if_false;
        kinstr.IIf.k := k_value
        |} =>
    IIf
      {| kinstr.IIf.kinfo := kinfo_value;
        kinstr.IIf.branch_if_true :=
          f_value.(kinstr_rewritek.apply) branch_if_true;
        kinstr.IIf.branch_if_false :=
          f_value.(kinstr_rewritek.apply) branch_if_false;
        kinstr.IIf.k := f_value.(kinstr_rewritek.apply) k_value |}
  
  | ILoop kinfo_value kbody k_value =>
    ILoop kinfo_value (f_value.(kinstr_rewritek.apply) kbody)
      (f_value.(kinstr_rewritek.apply) k_value)
  
  | ILoop_left kinfo_value kl kr =>
    ILoop_left kinfo_value (f_value.(kinstr_rewritek.apply) kl)
      (f_value.(kinstr_rewritek.apply) kr)
  
  | IDip kinfo_value body k_value =>
    IDip kinfo_value (f_value.(kinstr_rewritek.apply) body)
      (f_value.(kinstr_rewritek.apply) k_value)
  
  | IExec kinfo_value k_value =>
    IExec kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IApply kinfo_value ty k_value =>
    IApply kinfo_value ty (f_value.(kinstr_rewritek.apply) k_value)
  
  | ILambda kinfo_value l_value k_value =>
    ILambda kinfo_value l_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IFailwith kinfo_value i_value ty => IFailwith kinfo_value i_value ty
  
  | ICompare kinfo_value ty k_value =>
    ICompare kinfo_value ty (f_value.(kinstr_rewritek.apply) k_value)
  
  | IEq kinfo_value k_value =>
    IEq kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | INeq kinfo_value k_value =>
    INeq kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ILt kinfo_value k_value =>
    ILt kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IGt kinfo_value k_value =>
    IGt kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ILe kinfo_value k_value =>
    ILe kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IGe kinfo_value k_value =>
    IGe kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IAddress kinfo_value k_value =>
    IAddress kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IContract kinfo_value ty code k_value =>
    IContract kinfo_value ty code (f_value.(kinstr_rewritek.apply) k_value)
  
  | ITransfer_tokens kinfo_value k_value =>
    ITransfer_tokens kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IView kinfo_value view_signature k_value =>
    IView kinfo_value view_signature (f_value.(kinstr_rewritek.apply) k_value)
  
  | IImplicit_account kinfo_value k_value =>
    IImplicit_account kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  |
    ICreate_contract {|
      kinstr.ICreate_contract.kinfo := kinfo_value;
        kinstr.ICreate_contract.storage_type := storage_type;
        kinstr.ICreate_contract.arg_type := arg_type;
        kinstr.ICreate_contract.lambda := lambda;
        kinstr.ICreate_contract.views := views;
        kinstr.ICreate_contract.root_name := root_name;
        kinstr.ICreate_contract.k := k_value
        |} =>
    let k_value := f_value.(kinstr_rewritek.apply) k_value in
    ICreate_contract
      {| kinstr.ICreate_contract.kinfo := kinfo_value;
        kinstr.ICreate_contract.storage_type := storage_type;
        kinstr.ICreate_contract.arg_type := arg_type;
        kinstr.ICreate_contract.lambda := lambda;
        kinstr.ICreate_contract.views := views;
        kinstr.ICreate_contract.root_name := root_name;
        kinstr.ICreate_contract.k := k_value |}
  
  | ISet_delegate kinfo_value k_value =>
    ISet_delegate kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | INow kinfo_value k_value =>
    INow kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IBalance kinfo_value k_value =>
    IBalance kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ILevel kinfo_value k_value =>
    ILevel kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ICheck_signature kinfo_value k_value =>
    ICheck_signature kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IHash_key kinfo_value k_value =>
    IHash_key kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IPack kinfo_value ty k_value =>
    IPack kinfo_value ty (f_value.(kinstr_rewritek.apply) k_value)
  
  | IUnpack kinfo_value ty k_value =>
    IUnpack kinfo_value ty (f_value.(kinstr_rewritek.apply) k_value)
  
  | IBlake2b kinfo_value k_value =>
    IBlake2b kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ISha256 kinfo_value k_value =>
    ISha256 kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ISha512 kinfo_value k_value =>
    ISha512 kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ISource kinfo_value k_value =>
    ISource kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ISender kinfo_value k_value =>
    ISender kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ISelf kinfo_value ty s_value k_value =>
    ISelf kinfo_value ty s_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ISelf_address kinfo_value k_value =>
    ISelf_address kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IAmount kinfo_value k_value =>
    IAmount kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ISapling_empty_state kinfo_value s_value k_value =>
    ISapling_empty_state kinfo_value s_value
      (f_value.(kinstr_rewritek.apply) k_value)
  
  | ISapling_verify_update kinfo_value k_value =>
    ISapling_verify_update kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IDig kinfo_value n_value p_value k_value =>
    IDig kinfo_value n_value p_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IDug kinfo_value n_value p_value k_value =>
    IDug kinfo_value n_value p_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IDipn kinfo_value n_value p_value k1 k2 =>
    IDipn kinfo_value n_value p_value (f_value.(kinstr_rewritek.apply) k1)
      (f_value.(kinstr_rewritek.apply) k2)
  
  | IDropn kinfo_value n_value p_value k_value =>
    IDropn kinfo_value n_value p_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IChainId kinfo_value k_value =>
    IChainId kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | INever kinfo_value => INever kinfo_value
  
  | IVoting_power kinfo_value k_value =>
    IVoting_power kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ITotal_voting_power kinfo_value k_value =>
    ITotal_voting_power kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IKeccak kinfo_value k_value =>
    IKeccak kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ISha3 kinfo_value k_value =>
    ISha3 kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IAdd_bls12_381_g1 kinfo_value k_value =>
    IAdd_bls12_381_g1 kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IAdd_bls12_381_g2 kinfo_value k_value =>
    IAdd_bls12_381_g2 kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IAdd_bls12_381_fr kinfo_value k_value =>
    IAdd_bls12_381_fr kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IMul_bls12_381_g1 kinfo_value k_value =>
    IMul_bls12_381_g1 kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IMul_bls12_381_g2 kinfo_value k_value =>
    IMul_bls12_381_g2 kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IMul_bls12_381_fr kinfo_value k_value =>
    IMul_bls12_381_fr kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IMul_bls12_381_z_fr kinfo_value k_value =>
    IMul_bls12_381_z_fr kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IMul_bls12_381_fr_z kinfo_value k_value =>
    IMul_bls12_381_fr_z kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IInt_bls12_381_fr kinfo_value k_value =>
    IInt_bls12_381_fr kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | INeg_bls12_381_g1 kinfo_value k_value =>
    INeg_bls12_381_g1 kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | INeg_bls12_381_g2 kinfo_value k_value =>
    INeg_bls12_381_g2 kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | INeg_bls12_381_fr kinfo_value k_value =>
    INeg_bls12_381_fr kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IPairing_check_bls12_381 kinfo_value k_value =>
    IPairing_check_bls12_381 kinfo_value
      (f_value.(kinstr_rewritek.apply) k_value)
  
  | IComb kinfo_value n_value p_value k_value =>
    IComb kinfo_value n_value p_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IUncomb kinfo_value n_value p_value k_value =>
    IUncomb kinfo_value n_value p_value
      (f_value.(kinstr_rewritek.apply) k_value)
  
  | IComb_get kinfo_value n_value p_value k_value =>
    IComb_get kinfo_value n_value p_value
      (f_value.(kinstr_rewritek.apply) k_value)
  
  | IComb_set kinfo_value n_value p_value k_value =>
    IComb_set kinfo_value n_value p_value
      (f_value.(kinstr_rewritek.apply) k_value)
  
  | IDup_n kinfo_value n_value p_value k_value =>
    IDup_n kinfo_value n_value p_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ITicket kinfo_value k_value =>
    ITicket kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IRead_ticket kinfo_value k_value =>
    IRead_ticket kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | ISplit_ticket kinfo_value k_value =>
    ISplit_ticket kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  
  | IJoin_tickets kinfo_value ty k_value =>
    IJoin_tickets kinfo_value ty (f_value.(kinstr_rewritek.apply) k_value)
  
  | IHalt kinfo_value => IHalt kinfo_value
  
  | ILog kinfo_value event logger k_value =>
    ILog kinfo_value event logger k_value
  
  | IOpen_chest kinfo_value k_value =>
    IOpen_chest kinfo_value (f_value.(kinstr_rewritek.apply) k_value)
  end.

Definition ty_metadata_value (function_parameter : ty) : ty_metadata :=
  match function_parameter with
  | Unit_t meta => meta
  | Never_t meta => meta
  | Int_t meta => meta
  | Nat_t meta => meta
  | Signature_t meta => meta
  | String_t meta => meta
  | Bytes_t meta => meta
  | Mutez_t meta => meta
  | Bool_t meta => meta
  | Key_hash_t meta => meta
  | Key_t meta => meta
  | Timestamp_t meta => meta
  | Chain_id_t meta => meta
  | Address_t meta => meta
  | Pair_t _ _ meta => meta
  | Union_t _ _ meta => meta
  | Option_t _ meta => meta
  | Lambda_t _ _ meta => meta
  | List_t _ meta => meta
  | Set_t _ meta => meta
  | Map_t _ _ meta => meta
  | Big_map_t _ _ meta => meta
  | Ticket_t _ meta => meta
  | Contract_t _ meta => meta
  | Sapling_transaction_t _ meta => meta
  | Sapling_state_t _ meta => meta
  | Operation_t meta => meta
  | Bls12_381_g1_t meta => meta
  | Bls12_381_g2_t meta => meta
  | Bls12_381_fr_t meta => meta
  | Chest_t meta => meta
  | Chest_key_t meta => meta
  end.

Definition ty_size (t_value : ty) : Type_size.(TYPE_SIZE.t) :=
  (ty_metadata_value t_value).(ty_metadata.size).

Definition unit_t (annot : option Script_ir_annot.type_annot) : ty :=
  Unit_t
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition int_t (annot : option Script_ir_annot.type_annot) : ty :=
  Int_t
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition nat_t (annot : option Script_ir_annot.type_annot) : ty :=
  Nat_t
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition signature_t (annot : option Script_ir_annot.type_annot) : ty :=
  Signature_t
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition string_t (annot : option Script_ir_annot.type_annot) : ty :=
  String_t
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition bytes_t (annot : option Script_ir_annot.type_annot) : ty :=
  Bytes_t
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition mutez_t (annot : option Script_ir_annot.type_annot) : ty :=
  Mutez_t
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition key_hash_t (annot : option Script_ir_annot.type_annot) : ty :=
  Key_hash_t
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition key_t (annot : option Script_ir_annot.type_annot) : ty :=
  Key_t
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition timestamp_t (annot : option Script_ir_annot.type_annot) : ty :=
  Timestamp_t
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition address_t (annot : option Script_ir_annot.type_annot) : ty :=
  Address_t
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition bool_t (annot : option Script_ir_annot.type_annot) : ty :=
  Bool_t
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition pair_t
  (loc : Alpha_context.Script.location)
  (function_parameter :
    ty * option Script_ir_annot.field_annot * option Script_ir_annot.var_annot)
  : ty * option Script_ir_annot.field_annot * option Script_ir_annot.var_annot
  -> option Script_ir_annot.type_annot -> M? ty :=
  let '(l_value, fannot_l, vannot_l) := function_parameter in
  fun (function_parameter :
    ty * option Script_ir_annot.field_annot * option Script_ir_annot.var_annot)
    =>
    let '(r_value, fannot_r, vannot_r) := function_parameter in
    fun (annot : option Script_ir_annot.type_annot) =>
      let? size_value :=
        Type_size.(TYPE_SIZE.compound2) loc (ty_size l_value) (ty_size r_value)
        in
      return?
        (Pair_t (l_value, fannot_l, vannot_l) (r_value, fannot_r, vannot_r)
          {| ty_metadata.annot := annot; ty_metadata.size := size_value |}).

Definition union_t
  (loc : Alpha_context.Script.location)
  (function_parameter : ty * option Script_ir_annot.field_annot)
  : ty * option Script_ir_annot.field_annot ->
  option Script_ir_annot.type_annot -> M? ty :=
  let '(l_value, fannot_l) := function_parameter in
  fun (function_parameter : ty * option Script_ir_annot.field_annot) =>
    let '(r_value, fannot_r) := function_parameter in
    fun (annot : option Script_ir_annot.type_annot) =>
      let? size_value :=
        Type_size.(TYPE_SIZE.compound2) loc (ty_size l_value) (ty_size r_value)
        in
      return?
        (Union_t (l_value, fannot_l) (r_value, fannot_r)
          {| ty_metadata.annot := annot; ty_metadata.size := size_value |}).

Definition union_bytes_bool_t : ty :=
  Union_t ((bytes_t None), None) ((bool_t None), None)
    {| ty_metadata.annot := None;
      ty_metadata.size := Type_size.(TYPE_SIZE.three) |}.

Definition lambda_t
  (loc : Alpha_context.Script.location) (l_value : ty) (r_value : ty)
  (annot : option Script_ir_annot.type_annot) : M? ty :=
  let? size_value :=
    Type_size.(TYPE_SIZE.compound2) loc (ty_size l_value) (ty_size r_value) in
  return?
    (Lambda_t l_value r_value
      {| ty_metadata.annot := annot; ty_metadata.size := size_value |}).

Definition option_t
  (loc : Alpha_context.Script.location) (t_value : ty)
  (annot : option Script_ir_annot.type_annot) : M? ty :=
  let? size_value := Type_size.(TYPE_SIZE.compound1) loc (ty_size t_value) in
  return?
    (Option_t t_value
      {| ty_metadata.annot := annot; ty_metadata.size := size_value |}).

Definition option_mutez'_t (meta : ty_metadata) : ty :=
  let '{| ty_metadata.annot := annot; ty_metadata.size := _ |} := meta in
  Option_t (mutez_t annot)
    {| ty_metadata.annot := None; ty_metadata.size := Type_size.(TYPE_SIZE.two)
      |}.

Definition option_string'_t (meta : ty_metadata) : ty :=
  let '{| ty_metadata.annot := annot; ty_metadata.size := _ |} := meta in
  Option_t (string_t annot)
    {| ty_metadata.annot := None; ty_metadata.size := Type_size.(TYPE_SIZE.two)
      |}.

Definition option_bytes'_t (meta : ty_metadata) : ty :=
  let '{| ty_metadata.annot := annot; ty_metadata.size := _ |} := meta in
  Option_t (bytes_t annot)
    {| ty_metadata.annot := None; ty_metadata.size := Type_size.(TYPE_SIZE.two)
      |}.

Definition option_nat_t : ty :=
  Option_t (nat_t None)
    {| ty_metadata.annot := None; ty_metadata.size := Type_size.(TYPE_SIZE.two)
      |}.

Definition option_pair_nat_nat_t : ty :=
  Option_t
    (Pair_t ((nat_t None), None, None) ((nat_t None), None, None)
      {| ty_metadata.annot := None;
        ty_metadata.size := Type_size.(TYPE_SIZE.three) |})
    {| ty_metadata.annot := None; ty_metadata.size := Type_size.(TYPE_SIZE.four)
      |}.

Definition option_pair_nat'_nat'_t (meta : ty_metadata) : ty :=
  let '{| ty_metadata.annot := annot; ty_metadata.size := _ |} := meta in
  Option_t
    (Pair_t ((nat_t annot), None, None) ((nat_t annot), None, None)
      {| ty_metadata.annot := None;
        ty_metadata.size := Type_size.(TYPE_SIZE.three) |})
    {| ty_metadata.annot := None; ty_metadata.size := Type_size.(TYPE_SIZE.four)
      |}.

Definition option_pair_nat_mutez'_t (meta : ty_metadata) : ty :=
  let '{| ty_metadata.annot := annot; ty_metadata.size := _ |} := meta in
  Option_t
    (Pair_t ((nat_t None), None, None) ((mutez_t annot), None, None)
      {| ty_metadata.annot := None;
        ty_metadata.size := Type_size.(TYPE_SIZE.three) |})
    {| ty_metadata.annot := None; ty_metadata.size := Type_size.(TYPE_SIZE.four)
      |}.

Definition option_pair_mutez'_mutez'_t (meta : ty_metadata) : ty :=
  let '{| ty_metadata.annot := annot; ty_metadata.size := _ |} := meta in
  Option_t
    (Pair_t ((mutez_t annot), None, None) ((mutez_t annot), None, None)
      {| ty_metadata.annot := None;
        ty_metadata.size := Type_size.(TYPE_SIZE.three) |})
    {| ty_metadata.annot := None; ty_metadata.size := Type_size.(TYPE_SIZE.four)
      |}.

Definition option_pair_int'_nat_t (meta : ty_metadata) : ty :=
  let '{| ty_metadata.annot := annot; ty_metadata.size := _ |} := meta in
  Option_t
    (Pair_t ((int_t annot), None, None) ((nat_t None), None, None)
      {| ty_metadata.annot := None;
        ty_metadata.size := Type_size.(TYPE_SIZE.three) |})
    {| ty_metadata.annot := None; ty_metadata.size := Type_size.(TYPE_SIZE.four)
      |}.

Definition option_pair_int_nat'_t (meta : ty_metadata) : ty :=
  let '{| ty_metadata.annot := annot; ty_metadata.size := _ |} := meta in
  Option_t
    (Pair_t ((int_t None), None, None) ((nat_t annot), None, None)
      {| ty_metadata.annot := None;
        ty_metadata.size := Type_size.(TYPE_SIZE.three) |})
    {| ty_metadata.annot := None; ty_metadata.size := Type_size.(TYPE_SIZE.four)
      |}.

Definition list_t
  (loc : Alpha_context.Script.location) (t_value : ty)
  (annot : option Script_ir_annot.type_annot) : M? ty :=
  let? size_value := Type_size.(TYPE_SIZE.compound1) loc (ty_size t_value) in
  return?
    (List_t t_value
      {| ty_metadata.annot := annot; ty_metadata.size := size_value |}).

Definition operation_t (annot : option Script_ir_annot.type_annot) : ty :=
  Operation_t
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition list_operation_t : ty :=
  List_t (operation_t None)
    {| ty_metadata.annot := None; ty_metadata.size := Type_size.(TYPE_SIZE.two)
      |}.

Definition set_t
  (loc : Alpha_context.Script.location) (t_value : comparable_ty)
  (annot : option Script_ir_annot.type_annot) : M? ty :=
  let? size_value :=
    Type_size.(TYPE_SIZE.compound1) loc (comparable_ty_size t_value) in
  return?
    (Set_t t_value
      {| ty_metadata.annot := annot; ty_metadata.size := size_value |}).

Definition map_t
  (loc : Alpha_context.Script.location) (l_value : comparable_ty) (r_value : ty)
  (annot : option Script_ir_annot.type_annot) : M? ty :=
  let? size_value :=
    Type_size.(TYPE_SIZE.compound2) loc (comparable_ty_size l_value)
      (ty_size r_value) in
  return?
    (Map_t l_value r_value
      {| ty_metadata.annot := annot; ty_metadata.size := size_value |}).

Definition big_map_t
  (loc : Alpha_context.Script.location) (l_value : comparable_ty) (r_value : ty)
  (annot : option Script_ir_annot.type_annot) : M? ty :=
  let? size_value :=
    Type_size.(TYPE_SIZE.compound2) loc (comparable_ty_size l_value)
      (ty_size r_value) in
  return?
    (Big_map_t l_value r_value
      {| ty_metadata.annot := annot; ty_metadata.size := size_value |}).

Definition contract_t
  (loc : Alpha_context.Script.location) (t_value : ty)
  (annot : option Script_ir_annot.type_annot) : M? ty :=
  let? size_value := Type_size.(TYPE_SIZE.compound1) loc (ty_size t_value) in
  return?
    (Contract_t t_value
      {| ty_metadata.annot := annot; ty_metadata.size := size_value |}).

Definition contract_unit_t : ty :=
  Contract_t (unit_t None)
    {| ty_metadata.annot := None; ty_metadata.size := Type_size.(TYPE_SIZE.two)
      |}.

Definition sapling_transaction_t
  (memo_size : Alpha_context.Sapling.Memo_size.t)
  (annot : option Script_ir_annot.type_annot) : ty :=
  Sapling_transaction_t memo_size
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition sapling_state_t
  (memo_size : Alpha_context.Sapling.Memo_size.t)
  (annot : option Script_ir_annot.type_annot) : ty :=
  Sapling_state_t memo_size
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition chain_id_t (annot : option Script_ir_annot.type_annot) : ty :=
  Chain_id_t
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition never_t (annot : option Script_ir_annot.type_annot) : ty :=
  Never_t
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition bls12_381_g1_t (annot : option Script_ir_annot.type_annot) : ty :=
  Bls12_381_g1_t
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition bls12_381_g2_t (annot : option Script_ir_annot.type_annot) : ty :=
  Bls12_381_g2_t
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition bls12_381_fr_t (annot : option Script_ir_annot.type_annot) : ty :=
  Bls12_381_fr_t
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition ticket_t
  (loc : Alpha_context.Script.location) (t_value : comparable_ty)
  (annot : option Script_ir_annot.type_annot) : M? ty :=
  let? size_value :=
    Type_size.(TYPE_SIZE.compound1) loc (comparable_ty_size t_value) in
  return?
    (Ticket_t t_value
      {| ty_metadata.annot := annot; ty_metadata.size := size_value |}).

Definition chest_key_t (annot : option Script_ir_annot.type_annot) : ty :=
  Chest_key_t
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Definition chest_t (annot : option Script_ir_annot.type_annot) : ty :=
  Chest_t
    {| ty_metadata.annot := annot; ty_metadata.size := Type_size.(TYPE_SIZE.one)
      |}.

Module kinstr_traverse.
  Record record {a : Set} : Set := Build {
    apply : a -> kinstr -> a }.
  Arguments record : clear implicits.
  Definition with_apply {t_a} apply (r : record t_a) :=
    Build t_a apply.
End kinstr_traverse.
Definition kinstr_traverse := kinstr_traverse.record.

Definition kinstr_traverse_value {accu : Set}
  (i_value : kinstr) (init_value : accu) (f_value : kinstr_traverse accu)
  : accu :=
  let fix aux {ret : Set}
    (accu_value : accu) (t_value : kinstr) (continue : accu -> ret) : ret :=
    let accu_value := f_value.(kinstr_traverse.apply) accu_value t_value in
    let next (k_value : kinstr) : ret :=
      aux accu_value k_value (fun (accu_value : accu) => continue accu_value) in
    let next2 (k1 : kinstr) (k2 : kinstr) : ret :=
      aux accu_value k1
        (fun (accu_value : accu) =>
          aux accu_value k2 (fun (accu_value : accu) => continue accu_value)) in
    let next3 (k1 : kinstr) (k2 : kinstr) (k3 : kinstr) : ret :=
      aux accu_value k1
        (fun (accu_value : accu) =>
          aux accu_value k2
            (fun (accu_value : accu) =>
              aux accu_value k3 (fun (accu_value : accu) => continue accu_value)))
      in
    let _return (function_parameter : unit) : ret :=
      let '_ := function_parameter in
      continue accu_value in
    match t_value with
    | IDrop _ k_value => next k_value
    | IDup _ k_value => next k_value
    | ISwap _ k_value => next k_value
    | IConst _ _ k_value => next k_value
    | ICons_pair _ k_value => next k_value
    | ICar _ k_value => next k_value
    | ICdr _ k_value => next k_value
    | IUnpair _ k_value => next k_value
    | ICons_some _ k_value => next k_value
    | ICons_none _ k_value => next k_value
    |
      IIf_none {|
        kinstr.IIf_none.kinfo := _;
          kinstr.IIf_none.branch_if_none := k1;
          kinstr.IIf_none.branch_if_some := k2;
          kinstr.IIf_none.k := k_value
          |} => next3 k1 k2 k_value
    |
      IOpt_map {|
        kinstr.IOpt_map.kinfo := _;
          kinstr.IOpt_map.body := body;
          kinstr.IOpt_map.k := k_value
          |} => next2 body k_value
    | ICons_left _ k_value => next k_value
    | ICons_right _ k_value => next k_value
    |
      IIf_left {|
        kinstr.IIf_left.kinfo := _;
          kinstr.IIf_left.branch_if_left := k1;
          kinstr.IIf_left.branch_if_right := k2;
          kinstr.IIf_left.k := k_value
          |} => next3 k1 k2 k_value
    | ICons_list _ k_value => next k_value
    | INil _ k_value => next k_value
    |
      IIf_cons {|
        kinstr.IIf_cons.kinfo := _;
          kinstr.IIf_cons.branch_if_cons := k2;
          kinstr.IIf_cons.branch_if_nil := k1;
          kinstr.IIf_cons.k := k_value
          |} => next3 k1 k2 k_value
    | IList_map _ k1 k2 => next2 k1 k2
    | IList_iter _ k1 k2 => next2 k1 k2
    | IList_size _ k_value => next k_value
    | IEmpty_set _ _ k_value => next k_value
    | ISet_iter _ k1 k2 => next2 k1 k2
    | ISet_mem _ k_value => next k_value
    | ISet_update _ k_value => next k_value
    | ISet_size _ k_value => next k_value
    | IEmpty_map _ _ k_value => next k_value
    | IMap_map _ k1 k2 => next2 k1 k2
    | IMap_iter _ k1 k2 => next2 k1 k2
    | IMap_mem _ k_value => next k_value
    | IMap_get _ k_value => next k_value
    | IMap_update _ k_value => next k_value
    | IMap_get_and_update _ k_value => next k_value
    | IMap_size _ k_value => next k_value
    | IEmpty_big_map _ _ _ k_value => next k_value
    | IBig_map_mem _ k_value => next k_value
    | IBig_map_get _ k_value => next k_value
    | IBig_map_update _ k_value => next k_value
    | IBig_map_get_and_update _ k_value => next k_value
    | IConcat_string _ k_value => next k_value
    | IConcat_string_pair _ k_value => next k_value
    | ISlice_string _ k_value => next k_value
    | IString_size _ k_value => next k_value
    | IConcat_bytes _ k_value => next k_value
    | IConcat_bytes_pair _ k_value => next k_value
    | ISlice_bytes _ k_value => next k_value
    | IBytes_size _ k_value => next k_value
    | IAdd_seconds_to_timestamp _ k_value => next k_value
    | IAdd_timestamp_to_seconds _ k_value => next k_value
    | ISub_timestamp_seconds _ k_value => next k_value
    | IDiff_timestamps _ k_value => next k_value
    | IAdd_tez _ k_value => next k_value
    | ISub_tez _ k_value => next k_value
    | ISub_tez_legacy _ k_value => next k_value
    | IMul_teznat _ k_value => next k_value
    | IMul_nattez _ k_value => next k_value
    | IEdiv_teznat _ k_value => next k_value
    | IEdiv_tez _ k_value => next k_value
    | IOr _ k_value => next k_value
    | IAnd _ k_value => next k_value
    | IXor _ k_value => next k_value
    | INot _ k_value => next k_value
    | IIs_nat _ k_value => next k_value
    | INeg _ k_value => next k_value
    | IAbs_int _ k_value => next k_value
    | IInt_nat _ k_value => next k_value
    | IAdd_int _ k_value => next k_value
    | IAdd_nat _ k_value => next k_value
    | ISub_int _ k_value => next k_value
    | IMul_int _ k_value => next k_value
    | IMul_nat _ k_value => next k_value
    | IEdiv_int _ k_value => next k_value
    | IEdiv_nat _ k_value => next k_value
    | ILsl_nat _ k_value => next k_value
    | ILsr_nat _ k_value => next k_value
    | IOr_nat _ k_value => next k_value
    | IAnd_nat _ k_value => next k_value
    | IAnd_int_nat _ k_value => next k_value
    | IXor_nat _ k_value => next k_value
    | INot_int _ k_value => next k_value
    |
      IIf {|
        kinstr.IIf.kinfo := _;
          kinstr.IIf.branch_if_true := k1;
          kinstr.IIf.branch_if_false := k2;
          kinstr.IIf.k := k_value
          |} => next3 k1 k2 k_value
    | ILoop _ k1 k2 => next2 k1 k2
    | ILoop_left _ k1 k2 => next2 k1 k2
    | IDip _ k1 k2 => next2 k1 k2
    | IExec _ k_value => next k_value
    | IApply _ _ k_value => next k_value
    | ILambda _ _ k_value => next k_value
    | IFailwith _ _ _ => _return tt
    | ICompare _ _ k_value => next k_value
    | IEq _ k_value => next k_value
    | INeq _ k_value => next k_value
    | ILt _ k_value => next k_value
    | IGt _ k_value => next k_value
    | ILe _ k_value => next k_value
    | IGe _ k_value => next k_value
    | IAddress _ k_value => next k_value
    | IContract _ _ _ k_value => next k_value
    | IView _ _ k_value => next k_value
    | ITransfer_tokens _ k_value => next k_value
    | IImplicit_account _ k_value => next k_value
    | ICreate_contract {| kinstr.ICreate_contract.k := k_value |} =>
      next k_value
    | ISet_delegate _ k_value => next k_value
    | INow _ k_value => next k_value
    | IBalance _ k_value => next k_value
    | ILevel _ k_value => next k_value
    | ICheck_signature _ k_value => next k_value
    | IHash_key _ k_value => next k_value
    | IPack _ _ k_value => next k_value
    | IUnpack _ _ k_value => next k_value
    | IBlake2b _ k_value => next k_value
    | ISha256 _ k_value => next k_value
    | ISha512 _ k_value => next k_value
    | ISource _ k_value => next k_value
    | ISender _ k_value => next k_value
    | ISelf _ _ _ k_value => next k_value
    | ISelf_address _ k_value => next k_value
    | IAmount _ k_value => next k_value
    | ISapling_empty_state _ _ k_value => next k_value
    | ISapling_verify_update _ k_value => next k_value
    | IDig _ _ _ k_value => next k_value
    | IDug _ _ _ k_value => next k_value
    | IDipn _ _ _ k1 k2 => next2 k1 k2
    | IDropn _ _ _ k_value => next k_value
    | IChainId _ k_value => next k_value
    | INever _ => _return tt
    | IVoting_power _ k_value => next k_value
    | ITotal_voting_power _ k_value => next k_value
    | IKeccak _ k_value => next k_value
    | ISha3 _ k_value => next k_value
    | IAdd_bls12_381_g1 _ k_value => next k_value
    | IAdd_bls12_381_g2 _ k_value => next k_value
    | IAdd_bls12_381_fr _ k_value => next k_value
    | IMul_bls12_381_g1 _ k_value => next k_value
    | IMul_bls12_381_g2 _ k_value => next k_value
    | IMul_bls12_381_fr _ k_value => next k_value
    | IMul_bls12_381_z_fr _ k_value => next k_value
    | IMul_bls12_381_fr_z _ k_value => next k_value
    | IInt_bls12_381_fr _ k_value => next k_value
    | INeg_bls12_381_g1 _ k_value => next k_value
    | INeg_bls12_381_g2 _ k_value => next k_value
    | INeg_bls12_381_fr _ k_value => next k_value
    | IPairing_check_bls12_381 _ k_value => next k_value
    | IComb _ _ _ k_value => next k_value
    | IUncomb _ _ _ k_value => next k_value
    | IComb_get _ _ _ k_value => next k_value
    | IComb_set _ _ _ k_value => next k_value
    | IDup_n _ _ _ k_value => next k_value
    | ITicket _ k_value => next k_value
    | IRead_ticket _ k_value => next k_value
    | ISplit_ticket _ k_value => next k_value
    | IJoin_tickets _ _ k_value => next k_value
    | IOpen_chest _ k_value => next k_value
    | IHalt _ => _return tt
    | ILog _ _ _ k_value => next k_value
    end in
  aux init_value i_value (fun (accu_value : accu) => accu_value).

Module ty_traverse.
  Record record {a : Set} : Set := Build {
    apply : a -> ty -> a;
    apply_comparable : a -> comparable_ty -> a }.
  Arguments record : clear implicits.
End ty_traverse.
Definition ty_traverse := ty_traverse.record.

Module Ty_traverse.
  Fixpoint aux {accu ret : Set}
    (f_value : ty_traverse accu) (accu_value : accu) (ty : comparable_ty)
    (continue : accu -> ret) : ret :=
    let accu_value := f_value.(ty_traverse.apply_comparable) accu_value ty in
    let next2 (ty1 : comparable_ty) (ty2 : comparable_ty) : ret :=
      aux f_value accu_value ty1
        (fun (accu_value : accu) =>
          aux f_value accu_value ty2
            (fun (accu_value : accu) => continue accu_value)) in
    let next (ty1 : comparable_ty) : ret :=
      aux f_value accu_value ty1
        (fun (accu_value : accu) => continue accu_value) in
    let _return (function_parameter : unit) : ret :=
      let '_ := function_parameter in
      continue accu_value in
    match ty with
    |
      (Unit_key _ | Int_key _ | Nat_key _ | Signature_key _ | String_key _ |
      Bytes_key _ | Mutez_key _ | Key_hash_key _ | Key_key _ | Timestamp_key _ |
      Address_key _ | Bool_key _ | Chain_id_key _ | Never_key _) => _return tt
    | Pair_key (ty1, _) (ty2, _) _ => next2 ty1 ty2
    | Union_key (ty1, _) (ty2, _) _ => next2 ty1 ty2
    | Option_key ty _ => next ty
    end.
  
  Reserved Notation "~next2'".
  Reserved Notation "~next'".
  
  Fixpoint aux' {accu ret : Set}
    (f_value : ty_traverse accu) (accu_value : accu) (ty : ty)
    (continue : accu -> ret) : ret :=
    let next2' {accu ret} := ~next2' accu ret in
    let next' {accu ret} := ~next' accu ret in
    let accu_value := f_value.(ty_traverse.apply) accu_value ty in
    match ty with
    |
      (Unit_t _ | Int_t _ | Nat_t _ | Signature_t _ | String_t _ | Bytes_t _ |
      Mutez_t _ | Key_hash_t _ | Key_t _ | Timestamp_t _ | Address_t _ |
      Bool_t _ | Sapling_transaction_t _ _ | Sapling_state_t _ _ | Operation_t _
      | Chain_id_t _ | Never_t _ | Bls12_381_g1_t _ | Bls12_381_g2_t _ |
      Bls12_381_fr_t _) => continue accu_value
    | Ticket_t cty _ => aux f_value accu_value cty continue
    | (Chest_key_t _ | Chest_t _) => continue accu_value
    | Pair_t (ty1, _, _) (ty2, _, _) _ =>
      next2' f_value accu_value ty1 ty2 continue
    | Union_t (ty1, _) (ty2, _) _ => next2' f_value accu_value ty1 ty2 continue
    | Lambda_t ty1 ty2 _ => next2' f_value accu_value ty1 ty2 continue
    | Option_t ty1 _ => next' f_value accu_value ty1 continue
    | List_t ty1 _ => next' f_value accu_value ty1 continue
    | Set_t cty _ => aux f_value accu_value cty continue
    | Map_t cty ty1 _ =>
      aux f_value accu_value cty
        (fun (accu_value : accu) => next' f_value accu_value ty1 continue)
    | Big_map_t cty ty1 _ =>
      aux f_value accu_value cty
        (fun (accu_value : accu) => next' f_value accu_value ty1 continue)
    | Contract_t ty1 _ => next' f_value accu_value ty1 continue
    end
  
  where "~next2'" :=
    (fun (accu ret : Set) => fun
      (f_value : ty_traverse accu) (accu_value : accu) (ty1 : ty) (ty2 : ty)
      (continue : accu -> ret) =>
      aux' f_value accu_value ty1
        (fun (accu_value : accu) =>
          aux' f_value accu_value ty2
            (fun (accu_value : accu) => continue accu_value)))
  
  and "~next'" :=
    (fun (accu ret : Set) => fun
      (f_value : ty_traverse accu) (accu_value : accu) (ty1 : ty)
      (continue : accu -> ret) =>
      let next2' {accu ret} := ~next2' accu ret in
      aux' f_value accu_value ty1
        (fun (accu_value : accu) => continue accu_value)).
  
  Definition next2' {accu ret : Set} := ~next2' accu ret.
  Definition next' {accu ret : Set} := ~next' accu ret.
End Ty_traverse.

Definition comparable_ty_traverse {A : Set}
  (cty : comparable_ty) (init_value : A) (f_value : ty_traverse A) : A :=
  Ty_traverse.aux f_value init_value cty (fun (accu_value : A) => accu_value).

Definition ty_traverse_value {A : Set}
  (ty : ty) (init_value : A) (f_value : ty_traverse A) : A :=
  Ty_traverse.aux' f_value init_value ty (fun (accu_value : A) => accu_value).

Module stack_ty_traverse.
  Record record {accu : Set} : Set := Build {
    apply : accu -> stack_ty -> accu }.
  Arguments record : clear implicits.
  Definition with_apply {t_accu} apply (r : record t_accu) :=
    Build t_accu apply.
End stack_ty_traverse.
Definition stack_ty_traverse := stack_ty_traverse.record.

Definition stack_ty_traverse_value {accu : Set}
  (sty : stack_ty) (init_value : accu) (f_value : stack_ty_traverse accu)
  : accu :=
  let fix aux (accu_value : accu) (sty : stack_ty) : accu :=
    match sty with
    | Bot_t => f_value.(stack_ty_traverse.apply) accu_value sty
    | Item_t _ sty' _ =>
      aux (f_value.(stack_ty_traverse.apply) accu_value sty) sty'
    end in
  aux init_value sty.

Module value_traverse.
  Record record {a : Set} : Set := Build {
    apply : forall {t : Set}, a -> ty -> t -> a;
    apply_comparable : forall {t : Set}, a -> comparable_ty -> t -> a }.
  Arguments record : clear implicits.
End value_traverse.
Definition value_traverse := value_traverse.record.

Axiom value_traverse_value : forall {t accu : Set},
  union ty comparable_ty -> t -> accu -> value_traverse accu -> accu.

Definition stack_top_ty (function_parameter : stack_ty) : ty :=
  match function_parameter with
  | Item_t ty _ _ => ty
  | _ => unreachable_gadt_branch
  end.
