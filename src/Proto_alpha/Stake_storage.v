Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Cache_repr.
Require TezosOfOCaml.Proto_alpha.Constants_repr.
Require TezosOfOCaml.Proto_alpha.Constants_storage.
Require TezosOfOCaml.Proto_alpha.Contract_repr.
Require TezosOfOCaml.Proto_alpha.Cycle_repr.
Require TezosOfOCaml.Proto_alpha.Delegate_activation_storage.
Require TezosOfOCaml.Proto_alpha.Frozen_deposits_storage.
Require TezosOfOCaml.Proto_alpha.Level_repr.
Require TezosOfOCaml.Proto_alpha.Level_storage.
Require TezosOfOCaml.Proto_alpha.Misc.
Require TezosOfOCaml.Proto_alpha.Raw_context.
Require TezosOfOCaml.Proto_alpha.Sampler.
Require TezosOfOCaml.Proto_alpha.Seed_repr.
Require TezosOfOCaml.Proto_alpha.Storage.
Require TezosOfOCaml.Proto_alpha.Storage_sigs.
Require TezosOfOCaml.Proto_alpha.Tez_repr.

Module Selected_distribution_for_cycle.
  Module Cache_client.
    Definition cached_value : Set :=
      list (Signature.public_key_hash * Tez_repr.t).
    
    Definition namespace_value : Cache_repr.namespace :=
      Cache_repr.create_namespace "stake_distribution".
    
    Definition cache_index : int := 1.
    
    Definition value_of_identifier (ctxt : Raw_context.t) (identifier : string)
      : M?
        Storage.Stake.Selected_distribution_for_cycle.(Storage_sigs.Indexed_data_storage.value) :=
      let cycle := Cycle_repr.of_string_exn identifier in
      Storage.Stake.Selected_distribution_for_cycle.(Storage_sigs.Indexed_data_storage.get)
        ctxt cycle.
    
    Definition module :=
      {|
        Cache_repr.CLIENT.namespace_value := namespace_value;
        Cache_repr.CLIENT.cache_index := cache_index;
        Cache_repr.CLIENT.value_of_identifier := value_of_identifier
      |}.
  End Cache_client.
  Definition Cache_client := Cache_client.module.
  
  Axiom Cache : Cache_repr.INTERFACE (cached_value := Cache_client.cached_value).
  
  Definition identifier_of_cycle (cycle : Cycle_repr.cycle) : string :=
    Format.asprintf
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.Alpha CamlinternalFormatBasics.End_of_format)
        "%a") Cycle_repr.pp cycle.
  
  Definition init_value
    (ctxt : Raw_context.t) (cycle : Cycle_repr.cycle)
    (stakes :
      Storage.Stake.Selected_distribution_for_cycle.(Storage_sigs.Indexed_data_storage.value))
    : M? Raw_context.t :=
    let id := identifier_of_cycle cycle in
    let? ctxt :=
      Storage.Stake.Selected_distribution_for_cycle.(Storage_sigs.Indexed_data_storage.init_value)
        ctxt cycle stakes in
    let size_value := Constants_repr.stake_distribution_size in
    let? ctxt :=
      Cache.(Cache_repr.INTERFACE.update) ctxt id (Some (stakes, size_value)) in
    return? ctxt.
  
  Definition get (ctxt : Raw_context.t) (cycle : Cycle_repr.cycle)
    : M?
      Storage.Stake.Selected_distribution_for_cycle.(Storage_sigs.Indexed_data_storage.value) :=
    let id := identifier_of_cycle cycle in
    let? function_parameter := Cache.(Cache_repr.INTERFACE.find) ctxt id in
    match function_parameter with
    | None =>
      Storage.Stake.Selected_distribution_for_cycle.(Storage_sigs.Indexed_data_storage.get)
        ctxt cycle
    | Some v_value => return? v_value
    end.
  
  Definition remove_existing (ctxt : Raw_context.t) (cycle : Cycle_repr.cycle)
    : M? Raw_context.t :=
    let id := identifier_of_cycle cycle in
    let? function_parameter := Cache.(Cache_repr.INTERFACE.find) ctxt id in
    match function_parameter with
    | None =>
      Storage.Stake.Selected_distribution_for_cycle.(Storage_sigs.Indexed_data_storage.remove_existing)
        ctxt cycle
    | Some _ =>
      let? ctxt := Cache.(Cache_repr.INTERFACE.update) ctxt id None in
      return? ctxt
    end.
End Selected_distribution_for_cycle.

Module Delegate_sampler_state.
  Module Cache_client.
    Definition cached_value : Set :=
      Sampler.t (Signature.public_key * Signature.public_key_hash).
    
    Definition namespace_value : Cache_repr.namespace :=
      Cache_repr.create_namespace "sampler_state".
    
    Definition cache_index : int := 2.
    
    Definition value_of_identifier (ctxt : Raw_context.t) (identifier : string)
      : M?
        Storage.Delegate_sampler_state.(Storage_sigs.Indexed_data_storage.value) :=
      let cycle := Cycle_repr.of_string_exn identifier in
      Storage.Delegate_sampler_state.(Storage_sigs.Indexed_data_storage.get)
        ctxt cycle.
    
    Definition module :=
      {|
        Cache_repr.CLIENT.namespace_value := namespace_value;
        Cache_repr.CLIENT.cache_index := cache_index;
        Cache_repr.CLIENT.value_of_identifier := value_of_identifier
      |}.
  End Cache_client.
  Definition Cache_client := Cache_client.module.
  
  Axiom Cache : Cache_repr.INTERFACE (cached_value := Cache_client.cached_value).
  
  Definition identifier_of_cycle (cycle : Cycle_repr.cycle) : string :=
    Format.asprintf
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.Alpha CamlinternalFormatBasics.End_of_format)
        "%a") Cycle_repr.pp cycle.
  
  Definition init_value
    (ctxt : Raw_context.t) (cycle : Cycle_repr.cycle)
    (sampler_state :
      Storage.Delegate_sampler_state.(Storage_sigs.Indexed_data_storage.value))
    : M? Raw_context.t :=
    let id := identifier_of_cycle cycle in
    let? ctxt :=
      Storage.Delegate_sampler_state.(Storage_sigs.Indexed_data_storage.init_value)
        ctxt cycle sampler_state in
    let size_value := Constants_repr.sampler_state_size in
    let? ctxt :=
      Cache.(Cache_repr.INTERFACE.update) ctxt id
        (Some (sampler_state, size_value)) in
    return? ctxt.
  
  Definition get (ctxt : Raw_context.t) (cycle : Cycle_repr.cycle)
    : M?
      Storage.Delegate_sampler_state.(Storage_sigs.Indexed_data_storage.value) :=
    let id := identifier_of_cycle cycle in
    let? function_parameter := Cache.(Cache_repr.INTERFACE.find) ctxt id in
    match function_parameter with
    | None =>
      Storage.Delegate_sampler_state.(Storage_sigs.Indexed_data_storage.get)
        ctxt cycle
    | Some v_value => return? v_value
    end.
  
  Definition remove_existing (ctxt : Raw_context.t) (cycle : Cycle_repr.cycle)
    : M? Raw_context.t :=
    let id := identifier_of_cycle cycle in
    let? function_parameter := Cache.(Cache_repr.INTERFACE.find) ctxt id in
    match function_parameter with
    | None =>
      Storage.Delegate_sampler_state.(Storage_sigs.Indexed_data_storage.remove_existing)
        ctxt cycle
    | Some _ =>
      let? ctxt := Cache.(Cache_repr.INTERFACE.update) ctxt id None in
      return? ctxt
    end.
End Delegate_sampler_state.

Definition get_staking_balance
  : Raw_context.t -> Signature.public_key_hash -> M? Tez_repr.t :=
  Storage.Stake.Staking_balance.(Storage_sigs.Indexed_data_snapshotable_storage.get).

Definition ensure_stake_inited
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  : M? Raw_context.t :=
  let function_parameter :=
    Storage.Stake.Staking_balance.(Storage_sigs.Indexed_data_snapshotable_storage.mem)
      ctxt delegate in
  match function_parameter with
  | true => return? ctxt
  | false =>
    let? ctxt := Frozen_deposits_storage.init_value ctxt delegate in
    Storage.Stake.Staking_balance.(Storage_sigs.Indexed_data_snapshotable_storage.init_value)
      ctxt delegate Tez_repr.zero
  end.

Definition remove_stake
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  (amount : Tez_repr.t) : M? Raw_context.t :=
  let? ctxt := ensure_stake_inited ctxt delegate in
  let tokens_per_roll := Constants_storage.tokens_per_roll ctxt in
  let? staking_balance_before := get_staking_balance ctxt delegate in
  let? staking_balance :=
    Tez_repr.op_minusquestion staking_balance_before amount in
  let? ctxt :=
    Storage.Stake.Staking_balance.(Storage_sigs.Indexed_data_snapshotable_storage.update)
      ctxt delegate staking_balance in
  let? inactive := Delegate_activation_storage.is_inactive ctxt delegate in
  if
    (Pervasives.not inactive) &&
    (Tez_repr.op_gteq staking_balance_before tokens_per_roll)
  then
    if Tez_repr.op_lt staking_balance tokens_per_roll then
      let ctxt :=
        Storage.Stake.Active_delegate_with_one_roll.(Storage_sigs.Indexed_data_snapshotable_storage.remove)
          ctxt delegate in
      return? ctxt
    else
      return? ctxt
  else
    return? ctxt.

Definition add_stake
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  (amount : Tez_repr.t) : M? Raw_context.t :=
  let? ctxt := ensure_stake_inited ctxt delegate in
  let tokens_per_roll := Constants_storage.tokens_per_roll ctxt in
  let? staking_balance_before := get_staking_balance ctxt delegate in
  let? staking_balance := Tez_repr.op_plusquestion amount staking_balance_before
    in
  let? ctxt :=
    Storage.Stake.Staking_balance.(Storage_sigs.Indexed_data_snapshotable_storage.update)
      ctxt delegate staking_balance in
  if Tez_repr.op_gteq staking_balance tokens_per_roll then
    let? inactive := Delegate_activation_storage.is_inactive ctxt delegate in
    if
      inactive || (Tez_repr.op_gteq staking_balance_before tokens_per_roll)
    then
      return? ctxt
    else
      let ctxt :=
        Storage.Stake.Active_delegate_with_one_roll.(Storage_sigs.Indexed_data_snapshotable_storage.add)
          ctxt delegate tt in
      return? ctxt
  else
    return? ctxt.

Definition deactivate_only_call_from_delegate_storage
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  : Raw_context.t :=
  Storage.Stake.Active_delegate_with_one_roll.(Storage_sigs.Indexed_data_snapshotable_storage.remove)
    ctxt delegate.

Definition activate_only_call_from_delegate_storage
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  : M? Raw_context.t :=
  let? ctxt := ensure_stake_inited ctxt delegate in
  let? staking_balance := get_staking_balance ctxt delegate in
  let tokens_per_roll := Constants_storage.tokens_per_roll ctxt in
  if Tez_repr.op_gteq staking_balance tokens_per_roll then
    let ctxt :=
      Storage.Stake.Active_delegate_with_one_roll.(Storage_sigs.Indexed_data_snapshotable_storage.add)
        ctxt delegate tt in
    return? ctxt
  else
    return? ctxt.

Definition snapshot_value (ctxt : Raw_context.t) : M? Raw_context.t :=
  let? index :=
    Storage.Stake.Last_snapshot.(Storage_sigs.Single_data_storage.get) ctxt in
  let? ctxt :=
    Storage.Stake.Last_snapshot.(Storage_sigs.Single_data_storage.update) ctxt
      (index +i 1) in
  let? ctxt :=
    Storage.Stake.Staking_balance.(Storage_sigs.Indexed_data_snapshotable_storage.snapshot_value)
      ctxt index in
  Storage.Stake.Active_delegate_with_one_roll.(Storage_sigs.Indexed_data_snapshotable_storage.snapshot_value)
    ctxt index.

Definition select_distribution_for_cycle
  (ctxt : Raw_context.t) (cycle : Cycle_repr.t)
  (pubkey :
    Raw_context.t -> Signature.public_key_hash -> M? Signature.public_key)
  : M? Raw_context.t :=
  let? max_index :=
    Storage.Stake.Last_snapshot.(Storage_sigs.Single_data_storage.get) ctxt in
  let? seed_value := Storage.Seed.For_cycle.(Storage.FOR_CYCLE.get) ctxt cycle
    in
  let rd :=
    Seed_repr.initialize_new seed_value [ Bytes.of_string "stake_snapshot" ] in
  let seq := Seed_repr.sequence_value rd 0 in
  let selected_index :=
    Int32.to_int
      (Pervasives.fst (Seed_repr.take_int32 seq (Int32.of_int max_index))) in
  let? ctxt :=
    List.fold_left_es
      (fun (ctxt : Raw_context.t) =>
        fun (index : int) =>
          let? ctxt :=
            if index =i selected_index then
              let? '(stakes, total_stake) :=
                Storage.Stake.Active_delegate_with_one_roll.(Storage_sigs.Indexed_data_snapshotable_storage.fold_snapshot)
                  ctxt index Context.Sorted (nil, Tez_repr.zero)
                  (fun (delegate : Signature.public_key_hash) =>
                    fun (function_parameter : unit) =>
                      let '_ := function_parameter in
                      fun (function_parameter :
                        list (Signature.public_key_hash * Tez_repr.t) *
                          Tez_repr.t) =>
                        let '(acc_value, total_stake) := function_parameter in
                        let? staking_balance :=
                          Storage.Stake.Staking_balance.(Storage_sigs.Indexed_data_snapshotable_storage.Snapshot).(Storage_sigs.Indexed_data_storage.get)
                            ctxt (index, delegate) in
                        let delegate_contract :=
                          Contract_repr.implicit_contract delegate in
                        let? frozen_deposits_limit :=
                          Storage.Contract.Frozen_deposits_limit.(Storage_sigs.Indexed_data_storage.find)
                            ctxt delegate_contract in
                        let? balance :=
                          Storage.Contract.Balance.(Storage_sigs.Indexed_data_storage.get)
                            ctxt delegate_contract in
                        let? frozen_deposits :=
                          Frozen_deposits_storage.get ctxt delegate_contract in
                        let? total_balance :=
                          Tez_repr.op_plusquestion balance
                            frozen_deposits.(Storage.deposits.current_amount) in
                        let frozen_deposits_percentage :=
                          Constants_storage.frozen_deposits_percentage ctxt in
                        let stake_to_consider :=
                          match frozen_deposits_limit with
                          | Some frozen_deposits_limit =>
                            (* ❌ Try-with are not handled *)
                            try_with
                              (fun _ =>
                                let max_mutez :=
                                  Tez_repr.of_mutez_exn Int64.max_int in
                                let frozen_stake_limit :=
                                  if
                                    Tez_repr.op_gt frozen_deposits_limit
                                      (Tez_repr.div_exn
                                        max_mutez
                                        100)
                                  then
                                    max_mutez
                                  else
                                    Tez_repr.div_exn
                                      (Tez_repr.mul_exn frozen_deposits_limit
                                        100) frozen_deposits_percentage in
                                Tez_repr.min staking_balance frozen_stake_limit)
                              (fun _ => staking_balance)
                          | None => staking_balance
                          end in
                        let max_staking_capacity :=
                          Tez_repr.div_exn (Tez_repr.mul_exn total_balance 100)
                            frozen_deposits_percentage in
                        let stake_for_cycle :=
                          Tez_repr.min stake_to_consider max_staking_capacity in
                        let? total_stake :=
                          Tez_repr.op_plusquestion total_stake stake_for_cycle
                          in
                        return?
                          ((cons (delegate, stake_for_cycle) acc_value),
                            total_stake)) in
              let stakes :=
                List.sort
                  (fun (function_parameter :
                    Signature.public_key_hash * Tez_repr.t) =>
                    let '(_, x_value) := function_parameter in
                    fun (function_parameter :
                      Signature.public_key_hash * Tez_repr.t) =>
                      let '(_, y_value) := function_parameter in
                      Tez_repr.compare y_value x_value) stakes in
              let? ctxt :=
                Selected_distribution_for_cycle.init_value ctxt cycle stakes in
              let ctxt :=
                Storage.Total_active_stake.(Storage_sigs.Indexed_data_storage.add)
                  ctxt cycle total_stake in
              let? stakes_pk :=
                List.fold_left_es
                  (fun (acc_value :
                    list
                      ((Signature.public_key * Signature.public_key_hash) *
                        Sampler.mass)) =>
                    fun (function_parameter :
                      Signature.public_key_hash * Tez_repr.t) =>
                      let '(pkh, stake) := function_parameter in
                      let? pk := pubkey ctxt pkh in
                      return?
                        (cons ((pk, pkh), (Tez_repr.to_mutez stake)) acc_value))
                  nil stakes in
              let state := Sampler.create stakes_pk in
              Storage.Delegate_sampler_state.(Storage_sigs.Indexed_data_storage.init_value)
                ctxt cycle state
            else
              return? ctxt in
          let ctxt :=
            Storage.Stake.Staking_balance.(Storage_sigs.Indexed_data_snapshotable_storage.delete_snapshot)
              ctxt index in
          let ctxt :=
            Storage.Stake.Active_delegate_with_one_roll.(Storage_sigs.Indexed_data_snapshotable_storage.delete_snapshot)
              ctxt index in
          return? ctxt) ctxt (Misc.op_minusminusgt 0 (max_index -i 1)) in
  Storage.Stake.Last_snapshot.(Storage_sigs.Single_data_storage.update) ctxt 0.

Definition select_distribution_for_cycle_do_not_call_except_for_migration
  : Raw_context.t -> Cycle_repr.t ->
  (Raw_context.t -> Signature.public_key_hash -> M? Signature.public_key) ->
  M? Raw_context.t := select_distribution_for_cycle.

Definition clear_cycle (ctxt : Raw_context.t) (cycle : Cycle_repr.t)
  : M? Raw_context.t :=
  let? ctxt :=
    Storage.Total_active_stake.(Storage_sigs.Indexed_data_storage.remove_existing)
      ctxt cycle in
  let? ctxt := Selected_distribution_for_cycle.remove_existing ctxt cycle in
  Storage.Delegate_sampler_state.(Storage_sigs.Indexed_data_storage.remove_existing)
    ctxt cycle.

Definition init_first_cycles
  (ctxt : Raw_context.t)
  (pubkey :
    Raw_context.t -> Signature.public_key_hash -> M? Signature.public_key)
  : M? Raw_context.t :=
  let preserved := Constants_storage.preserved_cycles ctxt in
  let? ctxt :=
    List.fold_left_es
      (fun (ctxt : Raw_context.t) =>
        fun (c_value : int) =>
          let cycle := Cycle_repr.of_int32_exn (Int32.of_int c_value) in
          let? ctxt := snapshot_value ctxt in
          select_distribution_for_cycle ctxt cycle pubkey) ctxt
      (Misc.op_minusminusgt 0 preserved) in
  snapshot_value ctxt.

Definition fold {A : Set}
  (ctxt : Raw_context.t)
  (f_value : Signature.public_key_hash * Tez_repr.t -> A -> M? A)
  (order : Context.order) (init_value : A) : M? A :=
  Storage.Stake.Active_delegate_with_one_roll.(Storage_sigs.Indexed_data_snapshotable_storage.fold)
    ctxt order (Pervasives.Ok init_value)
    (fun (delegate : Signature.public_key_hash) =>
      fun (function_parameter : unit) =>
        let '_ := function_parameter in
        fun (acc_value : M? A) =>
          let? acc_value := acc_value in
          let? stake := get_staking_balance ctxt delegate in
          f_value (delegate, stake) acc_value).

Definition select_new_distribution_at_cycle_end
  (ctxt : Raw_context.t) (new_cycle : Cycle_repr.cycle)
  : (Raw_context.t -> Signature.public_key_hash -> M? Signature.public_key) ->
  M? Raw_context.t :=
  let preserved := Constants_storage.preserved_cycles ctxt in
  let for_cycle := Cycle_repr.add new_cycle preserved in
  select_distribution_for_cycle ctxt for_cycle.

Definition clear_at_cycle_end
  (ctxt : Raw_context.t) (new_cycle : Cycle_repr.cycle) : M? Raw_context.t :=
  let max_slashing_period := Constants_storage.max_slashing_period ctxt in
  match Cycle_repr.sub new_cycle max_slashing_period with
  | None => return? ctxt
  | Some cycle_to_clear => clear_cycle ctxt cycle_to_clear
  end.

Definition get (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  : M? Tez_repr.t :=
  let function_parameter :=
    Storage.Stake.Active_delegate_with_one_roll.(Storage_sigs.Indexed_data_snapshotable_storage.mem)
      ctxt delegate in
  match function_parameter with
  | true => get_staking_balance ctxt delegate
  | false => return? Tez_repr.zero
  end.

Definition fold_on_active_delegates_with_rolls {A : Set}
  : Raw_context.t -> Context.order -> A ->
  (Signature.public_key_hash -> unit -> A -> A) -> A :=
  Storage.Stake.Active_delegate_with_one_roll.(Storage_sigs.Indexed_data_snapshotable_storage.fold).

Definition get_selected_distribution
  : Raw_context.t -> Cycle_repr.cycle ->
  M?
    Storage.Stake.Selected_distribution_for_cycle.(Storage_sigs.Indexed_data_storage.value) :=
  Selected_distribution_for_cycle.get.

Definition find_selected_distribution
  : Raw_context.t -> Cycle_repr.t ->
  M?
    (option
      Storage.Stake.Selected_distribution_for_cycle.(Storage_sigs.Indexed_data_storage.value)) :=
  Storage.Stake.Selected_distribution_for_cycle.(Storage_sigs.Indexed_data_storage.find).

Definition prepare_stake_distribution (ctxt : Raw_context.t)
  : M? Raw_context.t :=
  let level := Level_storage.current ctxt in
  let? stakes :=
    Selected_distribution_for_cycle.get ctxt level.(Level_repr.t.cycle) in
  let stake_distribution :=
    List.fold_left
      (fun (map :
        Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
          Tez_repr.t) =>
        fun (function_parameter : Signature.public_key_hash * Tez_repr.t) =>
          let '(pkh, stake) := function_parameter in
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.add)
            pkh stake map)
      Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.empty)
      stakes in
  return?
    (Raw_context.init_stake_distribution_for_current_cycle ctxt
      stake_distribution).

Definition get_total_active_stake
  : Raw_context.t -> Cycle_repr.t -> M? Tez_repr.t :=
  Storage.Total_active_stake.(Storage_sigs.Indexed_data_storage.get).
