$notations = <<END
Module Notations.
  Export Compare.Notations.
  Export Error_monad.Notations.
  Export Int32.Notations.
  Export Int64.Notations.
  Export Lwt.Notations.
  Export Option.Notations.
  Export Pervasives.Notations.
  Export Z.Notations.
End Notations.
Export Notations.
END

def patch_environment(target_path)
  path = "Environment_mli.v"
  content = File.read(path)

  content.gsub!(/^Module (\w+)\.$/) do
    "Module Type #{$1}_signature."
  end
  content.gsub!(/^End (\w+)\.$/) do
    replacement = <<-END
End #{$1}_signature.
Require Export Proto_2021_01.Environment.#{$1}.
Module #{$1}_check : #{$1}_signature := #{$1}.
END
    replacement.chop
  end

  pattern = <<-END
  Module OrderedType.
    Record signature {t : Set} : Set := {
      t := t;
      compare : t -> t -> int;
    }.
  End OrderedType.
  Definition OrderedType := @OrderedType.signature.
  Arguments OrderedType {_}.
  
  Parameter Make_t : forall {Ord_t : Set} (Ord : OrderedType (t := Ord_t)), Set.
  
  Parameter Make :
    forall {Ord_t : Set},
    forall (Ord : OrderedType (t := Ord_t)),
    S.SET (elt := Ord.(OrderedType.t)) (t := Make_t Ord).
  END
  replacement = <<-END
  Parameter Make_t :
    forall {Ord_t : Set} (Ord : Compare.COMPARABLE (t := Ord_t)), Set.
  
  Parameter Make :
    forall {Ord_t : Set},
    forall (Ord : Compare.COMPARABLE (t := Ord_t)),
    S.SET (elt := Ord.(Compare.COMPARABLE.t)) (t := Make_t Ord).
  END
  content = content.gsub(pattern, replacement)

  pattern = <<-END
  Module OrderedType.
    Record signature {t : Set} : Set := {
      t := t;
      compare : t -> t -> int;
    }.
  End OrderedType.
  Definition OrderedType := @OrderedType.signature.
  Arguments OrderedType {_}.
  
  Parameter Make_t :
    forall {Ord_t : Set} (Ord : OrderedType (t := Ord_t)), Set -> Set.
  
  Parameter Make :
    forall {Ord_t : Set},
    forall (Ord : OrderedType (t := Ord_t)),
    S.MAP (key := Ord.(OrderedType.t)) (t := Make_t Ord).
  END
  replacement = <<-END
  Parameter Make_t :
    forall {Ord_t : Set} (Ord : Compare.COMPARABLE (t := Ord_t)), Set -> Set.
  
  Parameter Make :
    forall {Ord_t : Set},
    forall (Ord : Compare.COMPARABLE (t := Ord_t)),
    S.MAP (key := Ord.(Compare.COMPARABLE.t)) (t := Make_t Ord).
  END
  content = content.gsub(pattern, replacement)

  content << "\n"
  content << $notations

  File.open(path, "w") do |file|
    file << content
  end
end

# We add a number to the module init functions to differentiate them.
def index_init_module(content)
  n = 0
  content.gsub("init_module ") { n += 1; "init_module#{n} " }
end

def patch_alpha_context(target_path)
  path = File.join(target_path, "Alpha_context.v")
  content = File.read(path)

  pattern = "Include Fitness."
  replacement = "Include Environment.Fitness."
  content = content.gsub(pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_apply(target_path)
  path = File.join(target_path, "Apply.v")
  content = File.read(path)

  pattern = "Signature.Ed25519 "
  replacement = "Signature.Ed25519Hash "
  content = content.gsub(pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_baking(target_path)
  path = File.join(target_path, "Baking.v")
  content = File.read(path)

  content = index_init_module(content)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_contract_repr(target_path)
  path = File.join(target_path, "Contract_repr.v")
  content = File.read(path)

  pattern = "Signature.Ed25519 "
  replacement = "Signature.Ed25519Hash "
  content = content.gsub(pattern, replacement)
  pattern = "Signature.Secp256k1 "
  replacement = "Signature.Secp256k1Hash "
  content = content.gsub(pattern, replacement)
  pattern = "Signature.P256 "
  replacement = "Signature.P256Hash "
  content = content.gsub(pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_fixed_point_repr(target_path)
  path = File.join(target_path, "Fixed_point_repr.v")
  content = File.read(path)

  pattern = "Set -> Set"
  replacement = "Set"
  content = content.gsub(pattern, replacement)
  2.times do
    pattern = " _ :="
    replacement = " :="
    content = content.gsub(pattern, replacement)
  end
  pattern = "let t (a : Set) : Set := Z.t in"
  replacement = "let t : Set := Z.t in"
  content = content.gsub(pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_lazy_storage_kind(target_path)
  path = File.join(target_path, "Lazy_storage_kind.v")
  content = File.read(path)

  pattern = <<-END
  Definition title := MakeId_include.(TitleWithId.title).
  
  Module alloc.
  END
  replacement = <<-END
  Definition title := MakeId_include.(TitleWithId.title).

  Definition Id := MakeId_include.(TitleWithId.Id).

  Definition IdSet := MakeId_include.(TitleWithId.IdSet).

  Module alloc.
  END
  content = content.gsub(pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_raw_context(target_path)
  path = File.join(target_path, "Raw_context.v")
  content = File.read(path)

  content = index_init_module(content)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_storage(target_path)
  path = File.join(target_path, "Storage.v")
  content = File.read(path)

  pattern = <<-END
:=
    {|
      Storage_functors.INDEX.path_length := path_length;
  END
  replacement = <<-END
: Storage_functors.INDEX.signature (ipath := ipath) := {|
      Storage_functors.INDEX.path_length := path_length;
  END
  content = content.gsub(pattern, replacement)

  pattern = <<-END
:=
      {|
        Storage_functors.INDEX.path_length := path_length;
  END
  replacement = <<-END
: Storage_functors.INDEX.signature (ipath := ipath) := {|
        Storage_functors.INDEX.path_length := path_length;
  END
  content = content.gsub(pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_storage_functors(target_path)
  path = File.join(target_path, "Storage_functors.v")
  content = File.read(path)

  pattern = <<-END
  Definition functor `{FArgs} :=
    {|
      INDEX.path_length := path_length;
  END
  replacement = <<-END
  Definition functor `{FArgs} : INDEX.signature (ipath := ipath) :=
    {|
      INDEX.path_length := path_length;
  END
  content = content.gsub(pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

# We do this change because it generates collisions on include in alpha_context.
def patch_init_module_repr(target_path)
  Dir.glob(File.join(target_path, "*_repr.v")) do |path|
    content = File.read(path)

    pattern = "init_module "
    replacement = "init_module_repr "
    content = content.gsub(pattern, replacement)

    File.open(path, "w") do |file|
      file << content
    end
  end
end

def patch_generated_files(environment_path, protocol_path)
  patch_environment(environment_path)
  patch_alpha_context(protocol_path)
  patch_apply(protocol_path)
  patch_baking(protocol_path)
  patch_contract_repr(protocol_path)
  patch_fixed_point_repr(protocol_path)
  patch_lazy_storage_kind(protocol_path)
  patch_raw_context(protocol_path)
  patch_storage(protocol_path)
  patch_storage_functors(protocol_path)
  patch_init_module_repr(protocol_path)
end
