$notations = <<END
Module Notations.
  Export Compare.Notations.
  Export Int32.Notations.
  Export Int64.Notations.
  Export Option.Notations.
  Export Pervasives.Notations.
  Export Z.Notations.
End Notations.
Export Notations.

Parameter set_record_field : forall {A B : Set}, A -> string -> B -> unit.

Parameter typ_with_with_non_trivial_matching : forall {A}, A.
END

def gsub(content, pattern, replacement)
  init = content.clone
  content.gsub!(pattern, replacement )
  if content == init then
    puts "Useless replacement for #{pattern}"
    puts "Please update the replacement rules"
    puts
    puts caller
  end
end

def patch_environment(target_path)
  path = "Environment_mli.v"
  content = File.read(path, :encoding => 'utf-8')

  content.gsub!(/^Module (\w+)\.$/) do
    "Module Type #{$1}_signature."
  end
  content.gsub!(/^End (\w+)\.$/) do
    if $1 == "Lwt" then
      replacement = <<-END
End #{$1}_signature.
(* We do not implement the Lwt module, as it is the identity monad for us since
   the protocol code is sequential and interactions with the store can be
   implemented in a purely functional way. *)
END
    else
      replacement = <<-END
End #{$1}_signature.
Require Export Proto_alpha.Environment.#{$1}.
Module #{$1}_check : #{$1}_signature := #{$1}.
END
    end
    replacement.chop
  end

  pattern = "Module Error_monad_check : Error_monad_signature := Error_monad."
  replacement = pattern + "\n\n" + "Export Error_monad.Notations."
  gsub(content, pattern, replacement)

  pattern = /^.*Legacy \:.*$\n/
  gsub(content, pattern, "")

  pattern = " (Legacy_elt := elt) (Legacy_t := t)"
  gsub(content, pattern, "")

  pattern = <<-END
 (Legacy_key := key)
          (Legacy_t := fun (a : Set) => t a)
  END
  gsub(content, pattern.chop, "")

  pattern = <<-END
  Reserved Notation "'t".
  Reserved Notation "'node".
  
  
  
  where "'node" := (fun (t_a : Set) => Stdlib.Seq.node t_a)
  and "'t" := (fun (t_a : Set) => unit -> 'node t_a).
  
  Definition t := 't.
  Definition node := 'node.
  END
  replacement = <<-END
  Reserved Notation "'t".
  
  Inductive node (a : Set) : Set :=
  | Nil : node a
  | Cons : a -> 't a -> node a
  
  where "'t" := (fun (t_a : Set) => unit -> node t_a).
  
  Definition t := 't.
  END
  gsub(content, pattern, replacement)

  pattern = "  Parameter Cache_value : Set.\n  \n"
  gsub(content, pattern, "")

  gsub(content, "Cache_value", "cache_value")

  content << "\n"
  content << $notations

  File.open(path, "w") do |file|
    file << content
  end
end

# We add a number to the module init functions to differentiate them.
def index_init_module(content)
  n = 0
  content.gsub("init_module ") { n += 1; "init_module#{n} " }
end

def patch_alpha_context(target_path)
  path = File.join(target_path, "Alpha_context.v")
  content = File.read(path, :encoding => 'utf-8')

  gsub(content, "Fitness.t", "Environment.Fitness.t")
  gsub(content, "List.t", "(list t)")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_apply(target_path)
  path = File.join(target_path, "Apply.v")
  content = File.read(path, :encoding => 'utf-8')

  content = index_init_module(content)

  pattern = "Signature.Ed25519 "
  replacement = "Signature.Ed25519Hash "
  gsub(content, pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_baking(target_path)
  path = File.join(target_path, "Baking.v")
  content = File.read(path, :encoding => 'utf-8')

  content = index_init_module(content)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_cache_repr(target_path)
  path = File.join(target_path, "Cache_repr.v")
  content = File.read(path, :encoding => 'utf-8')

  pattern = <<-END
  Definition list_keys := Raw_context.Cache.(Context.CACHE.list_keys).
  
  Definition key_rank := Raw_context.Cache.(Context.CACHE.key_rank).
  
  END
  gsub(content, pattern, "")

  pattern = <<-END
      fun x_1 =>
        Option.value_value x_1 Pervasives.max_int
  END
  replacement = <<-END
      (fun x_1 =>
        Option.value_value x_1 Pervasives.max_int)
  END
  gsub(content, pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_contract_repr(target_path)
  path = File.join(target_path, "Contract_repr.v")
  content = File.read(path, :encoding => 'utf-8')

  pattern = "Signature.Ed25519 "
  replacement = "Signature.Ed25519Hash "
  gsub(content, pattern, replacement)
  pattern = "Signature.Secp256k1 "
  replacement = "Signature.Secp256k1Hash "
  gsub(content, pattern, replacement)
  pattern = "Signature.P256 "
  replacement = "Signature.P256Hash "
  gsub(content, pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_fixed_point_repr(target_path)
  path = File.join(target_path, "Fixed_point_repr.v")
  content = File.read(path, :encoding => 'utf-8')

  pattern = "Set -> Set"
  replacement = "Set"
  gsub(content, pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_gas_monad(target_path)
  path = File.join(target_path, "Gas_monad.v")
  content = File.read(path, :encoding => 'utf-8')

  gsub(content, ": M? (M? a * Alpha_context.context) ", "")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_lazy_storage_kind(target_path)
  path = File.join(target_path, "Lazy_storage_kind.v")
  content = File.read(path, :encoding => 'utf-8')

  pattern = <<-END
  Definition title := MakeId_include.(TitleWithId.title).
  
  Module alloc.
  END
  replacement = <<-END
  Definition title := MakeId_include.(TitleWithId.title).

  Definition Id := MakeId_include.(TitleWithId.Id).

  Definition Temp_id := MakeId_include.(TitleWithId.Temp_id).

  Definition IdSet := MakeId_include.(TitleWithId.IdSet).

  Module alloc.
  END
  gsub(content, pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_level_repr(target_path)
  path = File.join(target_path, "Level_repr.v")
  content = File.read(path, :encoding => 'utf-8')

  content = index_init_module(content)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_raw_context(target_path)
  path = File.join(target_path, "Raw_context.v")
  content = File.read(path, :encoding => 'utf-8')

  content = index_init_module(content)

  pattern = "  Definition empty := Context.Tree.(Context.TREE.empty)."
  replacement = "  (* Definition empty := Context.Tree.(Context.TREE.empty). *)"
  gsub(content, pattern, replacement)

  pattern = <<-END
(* ❌ This kind of signature (ident) is not handled. *)
(* unhandled_module_type *)
  END
  replacement = <<-END
Definition T {t : Set} : Set :=
  Raw_context_intf.T (root := root) (t := t) (tree := tree).
  END
  gsub(content, pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_round_repr(target_path)
  path = File.join(target_path, "Round_repr.v")
  content = File.read(path, :encoding => 'utf-8')

  content = index_init_module(content)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_sampler(target_path)
  path = File.join(target_path, "Sampler.v")
  content = File.read(path, :encoding => 'utf-8')

  gsub(content, "Record record {a : Set}", "Record record `{FArgs} {a : Set}")
  gsub(content, "Arguments record : clear implicits.", "Arguments record {_ _}.")
  gsub(content, "Build t_a", "Build _ _ t_a")
  gsub(content, "Definition with_total", "Definition with_total `{FArgs}")
  gsub(content, "Definition with_support", "Definition with_support `{FArgs}")
  gsub(content, "Definition with_p", "Definition with_p `{FArgs}")
  gsub(content, "Definition with_alias", "Definition with_alias `{FArgs}")
  gsub(content, "Definition t := t.record.", "Definition t `{FArgs} := t.record.")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_script_cache(target_path)
  path = File.join(target_path, "Script_cache.v")
  content = File.read(path, :encoding => 'utf-8')

  pattern = "(* Cannot unpack first-class modules at top-level due to a universe inconsistency *)"
  replacement = "Axiom Cache : Alpha_context.Cache.INTERFACE (cached_value := cached_contract)."
  gsub(content, pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_script_interpreter(target_path)
  path = File.join(target_path, "Script_interpreter.v")
  content = File.read(path, :encoding => 'utf-8')

  pattern = "                  let fix aux {b t : Set}"
  replacement = "                  let fix aux {a b s t : Set}"
  gsub(content, pattern, replacement)

  pattern = ".(Script_typed_ir.logger.log_interp)"
  replacement = ".(Script_typed_ir.logger.log_interp) _ _"
  gsub(content, pattern, replacement)

  pattern = "with imap_map {e f a b c d : Set}"
  replacement = "with imap_map {e f a b c d g : Set}"
  gsub(content, pattern, replacement)

  pattern = "with ilist_map {e a b c d : Set}"
  replacement = "with ilist_map {e a b c d f : Set}"
  gsub(content, pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_script_interpreter_defs(target_path)
  path = File.join(target_path, "Script_interpreter_defs.v")
  content = File.read(path, :encoding => 'utf-8')

  pattern = ".(Script_typed_ir.logger.log_exit)"
  replacement = ".(Script_typed_ir.logger.log_exit) _ _"
  gsub(content, pattern, replacement)

  pattern = ".(Script_typed_ir.logger.log_entry)"
  replacement = ".(Script_typed_ir.logger.log_entry) _ _"
  gsub(content, pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_script_map(target_path)
  path = File.join(target_path, "Script_map.v")
  content = File.read(path, :encoding => 'utf-8')

  gsub(
    content,
    "let fold := OPS.(Map.S.fold) in",
    "let fold {a : Set} := OPS.(Map.S.fold) (b := a) in"
  )

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_script_typed_ir(target_path)
  path = File.join(target_path, "Script_typed_ir.v")
  content = File.read(path, :encoding => 'utf-8')

  pattern = <<-END
Module TYPE_SIZE.
  Record signature {t : Set -> Set} : Set := {
  END
  replacement = <<-END
Module TYPE_SIZE.
  Record signature {t : Set} : Set := {
  END
  gsub(content, pattern, replacement)

  pattern = <<-END
      TYPE_SIZE.merge _ _ := merge;
      TYPE_SIZE.to_int _ := to_int;
      TYPE_SIZE.one _ := one;
      TYPE_SIZE.two _ := two;
      TYPE_SIZE.three _ := three;
      TYPE_SIZE.four _ _ := four;
      TYPE_SIZE.compound1 _ _ := compound1;
      TYPE_SIZE.compound2 _ _ _ := compound2
  END
  replacement = <<-END
      TYPE_SIZE.merge := merge;
      TYPE_SIZE.to_int := to_int;
      TYPE_SIZE.one := one;
      TYPE_SIZE.two := two;
      TYPE_SIZE.three := three;
      TYPE_SIZE.four := four;
      TYPE_SIZE.compound1 := compound1;
      TYPE_SIZE.compound2 := compound2
  END
  gsub(content, pattern, replacement)

  pattern = <<-END
  Definition with_apply {t_a} apply (r : record t_a) :=
    Build t_a apply r.(apply_comparable).
  Definition with_apply_comparable {t_a} apply_comparable (r : record t_a) :=
    Build t_a r.(apply) apply_comparable.
  END
  gsub(content, pattern, "")

  gsub(content, "'next'", "~next'")
  gsub(content, "'next2'", "~next2'")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_script_typed_ir_size(target_path)
  path = File.join(target_path, "Script_typed_ir_size.v")
  content = File.read(path, :encoding => 'utf-8')

  gsub(content, "Signature.Ed25519", "Signature.Ed25519Hash")
  gsub(content, "Signature.Secp256k1", "Signature.Secp256k1Hash")
  gsub(content, "Signature.P256", "Signature.P256Hash")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_slot_repr(target_path)
  path = File.join(target_path, "Slot_repr.v")
  content = File.read(path, :encoding => 'utf-8')

  gsub(content, "  Definition t : Set := list t.\n  \n", "")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_stake_storage(target_path)
  path = File.join(target_path, "Stake_storage.v")
  content = File.read(path, :encoding => 'utf-8')

  gsub(content,
    "(* Cannot unpack first-class modules at top-level due to a universe inconsistency *)",
    "Axiom Cache : Cache_repr.INTERFACE (cached_value := Cache_client.cached_value)."
  )

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_storage(target_path)
  path = File.join(target_path, "Storage.v")
  content = File.read(path, :encoding => 'utf-8')

  pattern = <<-END
:=
    {|
      Storage_functors.INDEX.to_path := to_path;
  END
  replacement = <<-END
: Storage_functors.INDEX.signature (ipath := ipath) := {|
      Storage_functors.INDEX.to_path := to_path;
  END
  gsub(content, pattern, replacement)

  pattern = <<-END
:=
      {|
        Storage_functors.INDEX.to_path := to_path;
  END
  replacement = <<-END
: Storage_functors.INDEX.signature (ipath := ipath) := {|
        Storage_functors.INDEX.to_path := to_path;
  END
  gsub(content, pattern, replacement)

  gsub(content, "Signature.Ed25519", "Signature.Ed25519Hash")
  gsub(content, "Signature.Secp256k1", "Signature.Secp256k1Hash")
  gsub(content, "Signature.P256", "Signature.P256Hash")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_storage_functors(target_path)
  path = File.join(target_path, "Storage_functors.v")
  content = File.read(path, :encoding => 'utf-8')

  pattern = <<-END
  Definition functor `{FArgs} :=
    {|
      INDEX.to_path := to_path;
  END
  replacement = <<-END
  Definition functor `{FArgs} : INDEX.signature (ipath := ipath) :=
    {|
      INDEX.to_path := to_path;
  END
  gsub(content, pattern, replacement)

  pattern = "C.Tree."
  replacement = "C.(Raw_context_intf.T.Tree)."
  gsub(content, pattern, replacement)

  pattern = "  Definition empty `{FArgs} := C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.empty)."
  replacement = "  (* Definition empty `{FArgs} := C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.empty). *)"
  gsub(content, pattern, replacement)

  pattern = <<-END
Module Make_indexed_carbonated_data_storage :=
  Make_indexed_carbonated_data_storage_INTERNAL.
  END
  replacement = <<-END
Definition Make_indexed_carbonated_data_storage
  {C_t I_t : Set} {I_ipath : Set -> Set} {V_t : Set}
  (C : Raw_context.T (t := C_t)) (I : INDEX (t := I_t) (ipath := I_ipath))
  (V : Storage_sigs.VALUE (t := V_t))
  : Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values
    (t := C.(Raw_context_intf.T.t)) (key := I.(INDEX.t))
    (value := V.(Storage_sigs.VALUE.t)) :=
  let functor_result := Make_indexed_carbonated_data_storage_INTERNAL C I V in
  {|
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.mem :=
      functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.mem);
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.get :=
      functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.get);
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.find :=
      functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.find);
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.update :=
      functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.update);
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.init_value :=
      functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.init_value);
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.add :=
      functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.add);
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.add_or_remove :=
      functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.add_or_remove);
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.remove_existing :=
      functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.remove_existing);
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.remove :=
      functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.remove);
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.list_values :=
      functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.list_values)
  |}.
  END
  gsub(content, pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_ticket_storage(target_path)
  path = File.join(target_path, "Ticket_storage.v")
  content = File.read(path, :encoding => 'utf-8')

  gsub(content, "  let S := Saturation_repr in\n", "")
  gsub(content, "S.add", "Saturation_repr.add")
  gsub(content, "S.safe_int", "Saturation_repr.safe_int")
  gsub(content, "S.shift_right", "Saturation_repr.shift_right")

  File.open(path, "w") do |file|
    file << content
  end
end

# We do this change because it generates collisions on include in alpha_context.
def patch_init_module_repr(target_path)
  Dir.glob(File.join(target_path, "*_repr.v")) do |path|
    content = File.read(path, :encoding => 'utf-8')

    pattern = "init_module "
    replacement = "init_module_repr "
    if content.include?(pattern) then
      gsub(content, pattern, replacement)
    end

    File.open(path, "w") do |file|
      file << content
    end
  end
end

def patch_generated_files(environment_path, protocol_path)
  patch_environment(environment_path)
  patch_alpha_context(protocol_path)
  patch_apply(protocol_path)
  patch_baking(protocol_path)
  patch_cache_repr(protocol_path)
  patch_contract_repr(protocol_path)
  patch_fixed_point_repr(protocol_path)
  patch_gas_monad(protocol_path)
  patch_lazy_storage_kind(protocol_path)
  patch_level_repr(protocol_path)
  patch_raw_context(protocol_path)
  patch_round_repr(protocol_path)
  patch_sampler(protocol_path)
  patch_script_cache(protocol_path)
  patch_script_interpreter(protocol_path)
  patch_script_interpreter_defs(protocol_path)
  patch_script_map(protocol_path)
  patch_script_typed_ir(protocol_path)
  patch_script_typed_ir_size(protocol_path)
  patch_slot_repr(protocol_path)
  patch_stake_storage(protocol_path)
  patch_storage(protocol_path)
  patch_storage_functors(protocol_path)
  patch_ticket_storage(protocol_path)
  patch_init_module_repr(protocol_path)
end

patch_generated_files(ARGV[0], ARGV[1])
